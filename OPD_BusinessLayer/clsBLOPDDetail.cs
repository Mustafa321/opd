using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLOPDDetail
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLOPDDetail()
        {
        }

        public clsBLOPDDetail(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tOPDDetail";
        private string StrErrorMessage = "";

        private string _OPDID = Default;
        private string _PrefID= Default;
        private string _StatusID = Default;
        private string _EnteredOn = Default;
        
        #endregion

        #region "Properties"

        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID= value; }
        }

        public string PrefID
        {
            get { return _PrefID; }
            set { _PrefID = value; }
        }

        public string StatusID
        {
            get { return _StatusID; }
            set { _StatusID = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }
        
        public bool UpdateStatus()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objdbOPD.Query = "Update cm_tOPDDetail set ";

                    if (!_StatusID.Equals(Default))
                    {
                        objdbOPD.Query += "StatusID=" + _StatusID + ",";
                    }
                    objdbOPD.Query += "EnteredOn=str_to_date('" + _EnteredOn + "','%d/%m/%Y %h:%i %p') Where OPDID=" + _OPDID + " and PrefID=" + _PrefID + "";
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }
        
        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT c.`OPDID`, c.`PrefID` FROM " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "Delete  FROM " + _TableName + " where OPDID = " + OPDID ;
                    break;
                case 3:
                    objdbOPD.Query = "Select OPDID,PrefID FROM cm_tOPDDetail Where OPDID=" + _OPDID + " and PrefID=" + _PrefID + ";";
                    break;
                case 4:
                    objdbOPD.Query = "Select OPDID,PrefID FROM cm_tOPDDetail Where OPDID=" + _OPDID + ";";
                    break;
                case 5:
                    objdbOPD.Query = "SELECT s.name,cast(date_format(od.enteredon,'%b %d, %Y %h:%i %p') as char) as enteredOn FROM cm_topddetail od ,cm_tstatus s where od.statusid=s.statusid and OPDID=" + _OPDID + " and PrefID=" + _PrefID + ";";
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        public bool Delete()
        {
                try
                {
                    objdbOPD.Query = "Delete  FROM " + _TableName + " where OPDID = " + OPDID;
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            
        }
       
        public bool DeleteFromPrefID()
        {
            try
            {
                objdbOPD.Query = "Delete  FROM " + _TableName + " where OPDID = " + OPDID + " and PrefID=" + _PrefID;
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }

        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[4, 3];

            if (!this._OPDID.Equals(Default))
            {
                strarr[0, 0] = "OPDID";
                strarr[0, 1] = _OPDID;
                strarr[0, 2] = "int";
            }

            if (!this._PrefID.Equals(Default))
            {
                strarr[1, 0] = "PrefID";
                strarr[1, 1] = _PrefID;
                strarr[1, 2] = "int";
            }

            if (!this._StatusID.Equals(Default))
            {
                strarr[2, 0] = "StatusID";
                strarr[2, 1] = _StatusID;
                strarr[2, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[3, 0] = "EnteredOn";
                strarr[3, 1] = _EnteredOn;
                strarr[3, 2] = "datetime";
            }
            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion

    }
}
