using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLMedicine
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLMedicine()
        {
        }

        public clsBLMedicine(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tMedicine";
        private string StrErrorMessage = "";

        private string _MedicineID = Default;
        private string _Name= Default;
        private string _UrduDosage= Default;
        private string _EngDosage = Default;
        private string _PatientInfo= Default;
        private string _Active= Default;
        private string _PersonID= Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _NumTime= Default;
        private string _FarsiDosage= Default;
        private string _MedIDRef= Default;
        private string _Morning = Default;
        private string _AfterNoon = Default;
        private string _Evening = Default;
        private string _Night = Default;
        private string _DayCount = Default;
        private string _Instruction = Default;
        private string _Quantity = Default;
        #endregion

        #region "Properties"
        public string Morning
        {
            get { return _Morning; }
            set { _Morning = value; }
        }
        public string Afternoon
        {
            get { return _AfterNoon; }
            set { _AfterNoon = value; }
        }
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public string Evening
        {
            get { return _Evening; }
            set { _Evening = value; }
        }
        public string Night
        {
            get { return _Night; }
            set { _Night = value; }
        }
        public string Instruction
        {
            get { return _Instruction; }
            set { _Instruction = value; }
        }
        public string DaysCount
        {
            get { return _DayCount; }
            set { _DayCount = value; }
        }
        public string MedicineID
        {
            get { return _MedicineID; }
            set { _MedicineID= value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string UrduDosage
        {
            get { return _UrduDosage; }
            set { _UrduDosage= value; }
        }

        public string PatientInfo
        {
            get { return _PatientInfo; }
            set { _PatientInfo= value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active= value; }
        }

        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }
        
        public string EngDosage
        {
            get { return _EngDosage; }
            set { _EngDosage = value; }
        }

        public string NumTime
        {
            get { return _NumTime; }
            set { _NumTime = value; }
        }

        public string FarsiDosage
        {
            get { return _FarsiDosage; }
            set { _FarsiDosage = value; }
        }

        public string MedIDRef
        {
            get { return _MedIDRef; }
            set { _MedIDRef = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Delete()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBDelete("Medicineid", MedicineID, _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
					objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name`, c.`UrduDosage`, c.`PatientInfo`, cast(c.Active as char) as Active, c.`PersonID`, c.`EnteredBy`, c.`EnteredOn`, c.`EngDosage`, c.`NumTime` FROM " + _TableName + " c";
                    break;

                case 2:
                    objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name`, c.`UrduDosage`  as dosage ,MedIDRef FROM " + _TableName + " c where c.personID = " + _PersonID + " and c.Active=1 order by c.Name ";
                    break;
                case 3:
                    objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name` FROM " + _TableName + " c where c.personID = " + _PersonID + " and name = '" + _Name + "'";
                    break;
                case 4:
                    objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name`, c.`EngDosage`  as dosage ,MedIDRef FROM " + _TableName + " c where .c.personID = " + _PersonID + " and c.Active=1 order by c.Name";
                    break;
                case 5://Select Number of time for  Selected Medicine 
                    objdbOPD.Query = "SELECT numtime FROM cm_tmedicine where medicineid" + _MedicineID ;
                    break;
                case 6:
                    objdbOPD.Query = "SELECT Dosageid,Acronym FROM cm_tDosage where Active ='Y' order by Acronym";
                    break;
                case 7:
                    objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name`, c.`FarsiDosage`  as dosage,MedIDRef  FROM " + _TableName + " c where c.personID = " + _PersonID + " and c.Active=1 order by c.`Name`";
                    break;
                case 8:
                    objdbOPD.Query = "SELECT MedicineID, Concat( Med_name , ' ' , n.name , ifnull(concat(' ' , mm.Brand_Qty,' '),'') , u.name ) as Med_Name FROM ph_tmed_master mm ,ph_tnature n, ph_tunit u where mm.unitid=u.unitid and mm.natureid=n.natureid and mm.MedicineId not in (Select m.MedIDRef from cm_tmedicine m where m.MedIDRef is not null) order by Med_Name";
                    break;
                case 9://Medicine Information
                    objdbOPD.Query = "SELECT mm.MedicineID,mm.MEd_Name,mm.Brand_Qty,n.name as NatureName,u.name as UnitName,dc.Name as DrugCatName,ms.name as SystemName,tg.name as GroupName,p.PackName,r.name as RootName,mm.PatientInfo,mm.RENALDOSAGE_ADJUST,mm.DESCRIPTION, mm.MEDICAL_NOTES,mm.ItemNo,mm.LACTATION_STATUS, mm.PEDIATRIC_SAFETY, mm.IV_ADMINISTRATION, mm.OVERDOSAGE_TREAT FROM ph_tmed_master mm  left outer join  ph_tRoots r on mm.RootID=r.RootID, ph_tNature n,ph_tUnit u,ph_tDrug_Category dc, cl_tmed_System ms, ph_tTherapeutic_group tg,ph_tpPacking p where mm.MedicineID=" + _MedicineID + " and mm.natureid=n.natureiD and mm.unitID=u.unitID and mm.CategoryID=dc.categoryID and mm.systemid=ms.systemID and mm.groupID=tg.groupid and mm.packingID= p.packingid";
                    break;
                case 10://Generic Information
                    objdbOPD.Query = "SELECT mg.GenericID, g.GCName,mg.Generic_Qty,u.name as UnitName FROM ph_tmed_generic mg,ph_tGeneric g,ph_tUnit u where mg.medicineID=" + _MedicineID + " and mg.GenericID=g.genericID  and mg.unitID=u.unitid;";
                    break;
                case 11://Generic Detail Information
                    objdbOPD.Query = "SELECT g.genericID,g.GCname,g.GC_ContraIndication,g.GC_PreCaution, g.GC_ADVERSEEFFECT, g.GC_INGREDIENTS FROM ph_tgeneric g where g.GenericID=" + _MedIDRef;
                    break;
                case 12://Get Manufacturer and Supplier Information
                    objdbOPD.Query = "SELECT DIstinct(case when type='M' then ConCat((SELECT M_Name FROM hr_tp_manufacturer where ManufacturerID=ReferenceID  ), ' (Manufacturer)') else  ConCat( (SELECT S_Name FROM hr_tp_supplier where SupplierID=ReferenceID ),' (Supplier)') end) as MS_Name  FROM pe_titemhistory p where Itemno=  " + _MedIDRef + " order by type;";
                    break;
                case 13://Get Drug Reaction Information
                    objdbOPD.Query = "SELECT g.genericID, g.GCName,dr.Description ,dr.Management FROM ph_tpdrug_reaction dr , ph_tGeneric g where  dr.Generic_1= " + _MedIDRef + " and  g.GenericID= dr.generic_2  ;";
                    break;
                case 14://Age wise Dosage
                    objdbOPD.Query = "SELECT ad.MinAge, ad.MaxAge, ad.Weight, u.name as UnitName FROM ph_tmed_agewise_doasge ad ,ph_tUnit u where ad.unitid=u.unitid and ad.medicineID = " + _MedicineID;
                    break;
                case 15:
                    objdbOPD.Query = "SELECT Medicineid, Name,MedIDRef as RefMedID ,'0' as Mode ,replace(UrduDosage ,'Ju','\\\\Ju') as UrduDosage,EngDosage  FROM " + _TableName + " order by Name";
                    break;
                case 16:
                    objdbOPD.Query = "SELECT Medicineid as MedID, Name as MedName FROM " + _TableName + " where Active=1";
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and Name like '" + _Name  +"%'";
                    }
                    break;
                    case 17:
                        objdbOPD.Query = "SELECT mg.GenericID, g.GCName,mm.medicineid, mm.med_name,n.natureid,n.name as NatureName FROM ph_tmed_master mm , ph_tmed_generic mg,ph_tGeneric g,ph_tnature n where mg.GenericID=g.genericID and mm.Natureid=n.natureid and mm.medicineid=mg.medicineid and mm.medicineid!=" + _MedicineID + " and mm.natureid in (SELECT p.Natureid FROM ph_tmed_master p where medicineid=" + _MedicineID + ") and g.genericid in (SELECT p.genericid FROM ph_tmed_generic p where medicineid=" +_MedicineID + ");";
                    break;

                    
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[22, 3];

            if (!this._MedicineID.Equals(Default))
            {
                strarr[0, 0] = "MedicineID";
                strarr[0, 1] = _MedicineID;
                strarr[0, 2] = "int";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[1, 0] = "Name";
                strarr[1, 1] = _Name;
                strarr[1, 2] = "string";
            }

            if (!this._UrduDosage.Equals(Default))
            {
                strarr[2, 0] = "UrduDosage";
                strarr[2, 1] = _UrduDosage;
                strarr[2, 2] = "string";
            }

            if (!this._PatientInfo.Equals(Default))
            {
                strarr[3, 0] = "PatientInfo";
                strarr[3, 1] = _PatientInfo;
                strarr[3, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[4, 0] = "Active";
                strarr[4, 1] = _Active;
                strarr[4, 2] = "int";
            }

            if (!this._PersonID.Equals(Default))
            {
                strarr[5, 0] = "PersonID";
                strarr[5, 1] = _PersonID;
                strarr[5, 2] = "int";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[6, 0] = "EnteredBy";
                strarr[6, 1] = _EnteredBy;
                strarr[6, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[7, 0] = "EnteredOn";
                strarr[7, 1] = _EnteredOn;
                strarr[7, 2] = "datetime";
            }
            if (!this._EngDosage.Equals(Default))
            {
                strarr[8, 0] = "EngDosage";
                strarr[8, 1] = _EngDosage;
                strarr[8, 2] = "string";
            }
            if (!this._NumTime.Equals(Default))
            {
                strarr[9, 0] = "NumTime";
                strarr[9, 1] = _NumTime;
                strarr[9, 2] = "int";
            }
            if (!this._FarsiDosage.Equals(Default))
            {
                strarr[10, 0] = "FarsiDosage";
                strarr[10, 1] = _FarsiDosage;
                strarr[10, 2] = "string";
            }
            if (!this._MedIDRef.Equals(Default))
            {
                strarr[11, 0] = "MedIDRef";
                strarr[11, 1] = _MedIDRef;
                strarr[11, 2] = "int";
            }
            if (!this._Morning.Equals(Default))
            {
                strarr[12, 0] = "Morning";
                strarr[12, 1] = _Morning;
                strarr[12, 2] = "string";
            }
            if (!this._AfterNoon.Equals(Default))
            {
                strarr[13, 0] = "Afternoon";
                strarr[13, 1] = _AfterNoon;
                strarr[13, 2] = "string";
            }
            if (!this._Evening.Equals(Default))
            {
                strarr[14, 0] = "Evening";
                strarr[14, 1] = _Evening;
                strarr[14, 2] = "string";
            }
            if (!this._Night.Equals(Default))
            {
                strarr[15, 0] = "Night";
                strarr[15, 1] = _Night;
                strarr[15, 2] = "string";
            }
            if (!this._DayCount.Equals(Default))
            {
                strarr[16, 0] = "DaysCount";
                strarr[16, 1] = _DayCount;
                strarr[16, 2] = "string";
            }
            if (!this._Instruction.Equals(Default))
            {
                strarr[17, 0] = "Instruction";
                strarr[17, 1] = _Instruction;
                strarr[17, 2] = "string";
            }
            if (!this.Morning.Equals(Default)&& !this.Morning.Equals(""))
            {
                strarr[18, 0] = "QuantityForMorning";
                strarr[18, 1] = _Quantity;
                strarr[18, 2] = "string";
            }
            else
            {
                strarr[18, 0] = "QuantityForMorning";
                strarr[18, 1] = "0";
                strarr[18, 2] = "string";
            }
            if (!this.Afternoon.Equals(Default) && !this.Afternoon.Equals(""))
            {
                strarr[19, 0] = "QuantityForAfterNoon";
                strarr[19, 1] = _Quantity;
                strarr[19, 2] = "string";
            }
            else
            {
                strarr[19, 0] = "QuantityForAfterNoon";
                strarr[19, 1] = "0";
                strarr[19, 2] = "string";
            }
            if (!this.Evening.Equals(Default) && !this.Evening.Equals(""))
            {
                strarr[20, 0] = "QuantityForEvening";
                strarr[20, 1] = _Quantity;
                strarr[20, 2] = "string";
            }
            else
            {
                strarr[20, 0] = "QuantityForEvening";
                strarr[20, 1] = "0";
                strarr[20, 2] = "string";
            }
            if (!this.Night.Equals(Default) && !this.Night.Equals(""))
            {
                strarr[21, 0] = "QuantityNight";
                strarr[21, 1] = _Quantity;
                strarr[21, 2] = "string";
            }
            else
            {
                strarr[21, 0] = "QuantityNight";
                strarr[21, 1] = "0";
                strarr[21, 2] = "string";
            }
            
            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if(!validateName())
            {
                return false;
            }
            //else if (!validateDosage())
            //{
            //    return false;
            //}
            return true;
        }

        private bool validateName()
        {
            if (!_Name.Equals(Default))
            {
                if (_Name.Equals(""))
                {
                    StrErrorMessage = "Please Enter Medicine Name";
                    return false;
                }
                DataView dv = this.GetAll(3);
                if (!MedicineID.Equals(Default))
                {
                    dv.RowFilter="MedicineID <> " + _MedicineID ;
                }

                if (dv.Count != 0)
                {
                    StrErrorMessage = "Medicine Already Save ";
                    return false;
                }
            }
            return true;
        }
       
        private bool validateDosage()
        {
            //if (!_Dosage.Equals(Default))
            //{
                //if (_Dosage.Equals(""))
                //{
                //    StrErrorMessage = "Please Select Dosage ";
                //    return false;
                //}
            //}
            return true;
        }

        #endregion

    }
}
