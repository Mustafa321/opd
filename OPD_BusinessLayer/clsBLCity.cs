﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD_BL
{
   public class clsBLCity
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objCity = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();
        public clsBLCity()
        {
        }
        public clsBLCity(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }
        #region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tcity";
        private string StrErrorMessage = "";
        private string _CityId = Default;
        private string _CityName = Default;
        private string _EnteredOn = Default;
        private string _EnteredBy = Default;


        #endregion
        #region "Properties"

        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }
        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }
        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }
        public string CityId
        {
            get { return _CityId; }
            set { _CityId = value; }
        }
        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion
        #region "Method"
        public bool CheckCityExist()
        {
            if (ValidateData())
            {
                try
                {
                    //_OPDID = objQB.QBGetMax("OPDIF", _TableName);
                    objCity.Query = objQB.QBGet(MakeArray1(), _TableName);
                    var n = _objConnection._objOperation.DataTrigger_Get_Single(objCity);

                    if (n.Count>0)
                    {
                        StrErrorMessage = "City Name Already Exist";
                        return true;
                       
                    }
                    return false;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
   
        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    //_OPDID = objQB.QBGetMax("OPDIF", _TableName);
                    objCity.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objCity);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objCity.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objCity);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void Delete()
        {
            objCity.Query = "Delete from " + _TableName + " where CITYID=" + _CityId;
            StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objCity);
        }

        //public void DeleteAll()
        //{
        //    objCity.Query = "Delete from " + _TableName + " where EnteredBy=" + _EnteredBy + " and visitDate = '" + _VisitDate + "'";
        //    StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objCity);
        //}

        //public DataView GetAll(int flag)
        //{
        //    switch (flag)
        //    {
        //        case 1:
        //            objCity.Query = "SELECT `OPDID`,parientID `PRNO`, `Complaints`, `Diagnosis`, `Signs`, `BP`, `Pulse`, `EyeSight`, `Temprature`, `Weight`, `Height`, cast(Active as char) as Active, `DisposalPlan`, `Investigation`, `HeadCircumferences`, `History`, `Instruction`, `EnteredBy`, `EnteredOn`,status,BMI,BSA,FollowUpDate,Payment,visitdate,HeartRate,RespiratoryRate FROM  " + _TableName;
        //            break;

        //        case 2:
        //            objCity.Query = "SELECT max(OPDID) FROM  " + _TableName;
        //            break;

        //        case 3://get seleted visit Information
        //            //objCity.Query = "SELECT c.`OPDID`,c.patientID , c.`PRNO`, c.`PresentingComplaints`, c.`Diagnosis`, c.`DisposalPlan`, c.`Investigation`, c.`History`,c.`Signs`, EnteredOn FROM " + _TableName + " c where Active=1 and opdid=" + _OPDID ;
        //            objCity.Query = "SELECT c.`OPDID`,c.patientID , c.`PRNO`, c.`PresentingComplaints`, c.`Diagnosis`, c.`DisposalPlan`, c.`Investigation`, c.`History`,c.`Signs`, EnteredOn, c.`BP`, c.`Pulse`, c.`Temprature`, c.`Weight`, c.`Height`, c.`HeadCircumferences`, c.`Instruction`,c.BMI,c.BSA,c.FollowUpDate,c.Payment,visitdate,c.notes ,c.HeartRate,c.RespiratoryRate FROM " + _TableName + " c where Active=1 and opdid= " + _OPDID;
        //            break;

        //        case 4://get last visit OPDID for seleted patient
        //            //objCity.Query = "SELECT c.`OPDID`  FROM " + _TableName + " c where enteredon in (select  max(enteredon) from cm_TOPD where prno='" + _PRNO + "' group by PRNO)";
        //            objCity.Query = "SELECT c.`OPDID` , visitdate FROM cm_topd c where prno='" + _PRNO + "' and Status!='W' and visitdate in (select  max(visitdate) from cm_TOPD where Status!='W' and prno='" + _PRNO + "' group by PRNO)";

        //            break;

        //        case 5:
        //            objCity.Query = "SELECT c.`History`,c.notes FROM " + _TableName + " c where Active=1 and PRNO= '" + _PRNO + "' order by opdid";
        //            break;

        //        case 6://all visit information for for selected patient
        //            objCity.Query = "SELECT OPDID,cast(date_format(date(visitdate),'%b %d, %Y') as char) as visitdate,History FROM " + _TableName + "  where Active=1 and PRNO= '" + _PRNO + "' ";
        //            if (!_Status.Equals("") && !_Status.Equals(Default))
        //            {
        //                objCity.Query += " and status = '" + _Status + "' ";
        //            }
        //            if (!_OPDID.Equals(""))
        //            {
        //                objCity.Query += " and opdID <>'" + _OPDID + "' ";
        //            }
        //            objCity.Query += "  order by date_format(date(visitdate),'%y %m, %d') Desc";
        //            break;

        //        case 7:
        //            //objCity.Query = "SELECT count(patientID) as TotalPatient,sum(payment) as TotalAmount FROM "   + _TableName  + " where  date(enteredon) = date(now());";s
        //            objCity.Query = "SELECT payment FROM " + _TableName + " where  date(visitdate) = date(now())";
        //            if (!_Status.Equals("") && !_Status.Equals(Default))
        //            {
        //                objCity.Query += " and status = '" + _Status + "' ";
        //            }
        //            break;

        //        case 8:
        //            objCity.Query = "SELECT distinct(year(visitdate)) as Visityear FROM cm_topd c;";
        //            //objCity.Query = "SELECT min(visitdate)as visitdate FROM cm_topd  order by visitdate";
        //            break;
        //        case 9://,pr.name as ItemName
        //            #region For Fill Grid
        //            if (!_Weight.Equals("") && !_Weight.Equals(Default))
        //            {
        //                objCity.Query = _Weight;//Add Selection critaria for DOB
        //            }
        //            if (!_Temprature.Equals("") && !_Temprature.Equals(Default))
        //            {
        //                objCity.Query = _Temprature;//for visit date
        //            }
        //            if (!_Pulse.Equals("") && !_Pulse.Equals(Default))
        //            {
        //                objCity.Query = _Pulse;//for location aggregates(city count, and city name)
        //            }
        //            objCity.Query += " SELECT o.PRNO, p.Name As PatientName,Group_ConCat(Distinct pr.name SEPARATOR ' , ')as itemName,o.visitdate ";
        //            if (!_PresentingComplaints.Equals("") && !_PresentingComplaints.Equals(Default))
        //            {
        //                objCity.Query += _PresentingComplaints;//for cityid and cityname in selection area for sub query
        //            }
        //            if (!_BP.Equals("") && !_BP.Equals(Default))
        //            {
        //                objCity.Query += _BP;//Add DOB For Age Aggregate
        //            }
        //            objCity.Query += " FROM cm_topd o,cm_tPatientReg p,cm_topdDetail od,cm_tPreference pr,cm_ttype t ";
        //            if (!_Signs.Equals("") && !_Signs.Equals(Default))
        //            {
        //                objCity.Query += _Signs;//set cm_tcity table in from table for location only
        //            }
        //            objCity.Query += "where p.patientid=o.patientid and o. opdid=od.opdid and pr.prefid=od.prefid and pr.typeid=t.typeid ";
        //            if (!_Payment.Equals("") && !_Payment.Equals(Default))
        //            {
        //                objCity.Query += " and " + _Payment;//for join condition b/w cm_tpatientReg and cm_tcity
        //            }
        //            if (!_VisitDate.Equals("") && !_VisitDate.Equals(Default))
        //            {
        //                objCity.Query += " and " + _VisitDate;//selection for visit date 
        //            }
        //            if (!_History.Equals("") && !_History.Equals(Default))
        //            {
        //                objCity.Query += " and t.name= '" + _History + "'";//join condition for cm_tType and Cm_TPreeference 
        //            }
        //            if (!_Instuction.Equals("") && !_Instuction.Equals(Default))
        //            {
        //                objCity.Query += " and pr.name= '" + _Instuction + "'";//join condition for cm_tPreference and Cm_TOPDDetail
        //            }
        //            if (!_BMI.Equals("") && !_BMI.Equals(Default))
        //            {
        //                if (_BMI.Equals(""))
        //                {
        //                    objCity.Query += " and  p.cityid in(-1)";//filter for selected cities
        //                }
        //                else
        //                {
        //                    objCity.Query += " and  p.cityid in(" + _BMI + ")";//filter for selected cities
        //                }
        //            }
        //            if (!_Diagnosis.Equals(Default) && !_DisposalPlan.Equals(Default))//for selected ages(DOB)
        //            {
        //                objCity.Query += " and date_format(p.DOB,'%Y/%m/%d') between '" + Convert.ToDateTime(_Diagnosis).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_DisposalPlan).ToString("yyyy/MM/dd") + "'";
        //            }
        //            objCity.Query += "  group by o.opdid order by o.OPDID";
        //            if (!_Investigation.Equals("") && !_Investigation.Equals(Default))
        //            {
        //                objCity.Query += _Investigation;//for group by location to find location agregates
        //            }
        //            if (!_Status.Equals("") && !_Status.Equals(Default))
        //            {
        //                objCity.Query += _Status;//for group by visitdate to find visitdate agregates
        //            }
        //            if (!_Height.Equals("") && !_Height.Equals(Default))
        //            {
        //                objCity.Query += _Height;//for group by DOB to find DOB agregates
        //            }
        //            break;
        //        #endregion
        //        case 10:
        //            objCity.Query = "SELECT pr.Patientid,o.opdid,pr.prno,pr.name as PatientName,pr.gender,pr.dob,pr.phoneno,pr.cellno,pr.address,c.name as CityName,pr.email,pn.panelname,ap.tokkenno,ap.AppointmentID FROM " + _TableName + " o, cm_tappointment ap ,cm_tpatientreg pr left outer join  CM_tPanel Pn  on pr.panelID=pn.PanelID,cm_tcity c where ap.prno=pr.prno and pr.patientid=o.patientid  and Appointmentdate='" + _EnteredOn + "' and pr.cityID=c.cityID and visitdate='" + _VisitDate + "' and o.status='w' and ap.personid=" + _EnteredBy + " and pr.prno='" + _PRNO + "'";
        //            break;
        //        case 11:
        //            objCity.Query = "SELECT opdid FROM cm_topd c WHERE DATE(C.visitdate)=DATE(Now()) AND PRNO='" + PRNO + "'";
        //            //objCity.Query = "SELECT min(visitdate)as visitdate FROM cm_topd  order by visitdate";
        //            break;
        //        case 12:
        //            objCity.Query = "SELECT pr.Patientid,o.opdid,pr.prno,pr.name as PatientName,pr.gender,pr.dob,pr.phoneno,pr.cellno,pr.address,c.name as CityName,pr.email,pn.panelname,o.payment FROM " + _TableName + " o, cm_tpatientreg pr left outer join  CM_tPanel Pn  on pr.panelID=pn.PanelID,cm_tcity c where pr.patientid=o.patientid  and pr.cityID=c.cityID and visitdate='" + _VisitDate + "' and o.status='w' and pr.prno='" + _PRNO + "'";
        //            break;

        //        case 13://all visit information for for selected patient
        //            objCity.Query = "SELECT PresentingComplaints as dgComplaints, Diagnosis as dgDiagnosis, DisposalPlan as dgPlans, Investigation as dgInvestigation, History as dgHistory, Signs as dgSigns FROM " + _TableName + "  where opdID =" + _OPDID;
        //            break;
        //        case 14://all vital sign information for selected patient
        //            objCity.Query = "SELECT BP, Pulse, Temprature, Weight, Height, HeadCircumferences,VisitDate,HeartRate,RespiratoryRate FROM cm_topd where PRNo='" + _PRNO + "'";
        //            break;
        //        case 15://Maximum OPDID
        //            objCity.Query = "select max(OPDID) from cm_tOPD";
        //            break;
        //        case 16://Min Year 
        //            objCity.Query = "select cast(min(date_format(visitdate,'%Y' )) as char)  as MinYear from cm_tOPD";
        //            break;
        //        case 17:
        //            objCity.Query = "SELECT count(opdid) as PatientCount,cast(date_format(visitdate,'%d') as char) as visitDay FROM cm_topd where date_format(visitdate,'%m/%Y')='" + _VisitDate + "' and EnteredBy= " + _EnteredBy + " group by date_format(visitdate,'%d/%m/%Y') order by date_format(visitdate,'%Y/%m/%d') Asc";
        //            break;
        //        case 18:
        //            objCity.Query = "Select pr.PatientID,pr.PRNo, pr.Email from cm_tOPD o,cm_tpatientreg pr where pr.patientid=o.patientid and  o.EnteredBy=" + _EnteredBy + " and visitDate = '" + _VisitDate + "'";
        //            break;
        //        case 19:
        //            objCity.Query = "SELECT c.BP, c.Pulse, c.Temprature, c.Weight, c.Height, c.HeadCircumferences, c.BMI, c.BSA,HeartRate,RespiratoryRate FROM cm_topd c where opdid=" + _OPDID;
        //            break;
        //        case 20:
        //            objCity.Query = "SELECT cast(date_format(date(visitdate),'%b %d, %Y') as char) as visitdate,Notes FROM cm_topd where notes is not null and notes<>'' and Patientid=" + _PatientID;
        //            break;

        //    }
        //    return _objConnection._objOperation.DataTrigger_Get_All(objCity);
        //}

        public bool UpdatePK(string query)
        {
            try
            {
                //_OPDID = objQB.QBGetMax("OPDIF", _TableName);
                objCity.Query = query;

                StrErrorMessage = _objConnection._objOperation.DataTrigger_Ora_Update(objCity);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public DataView GetAllOra(int flag)
        //{
        //  switch (flag)
        //  {
        //    case 1:
        //      objCity.Query = "select Hospitalid from hr_thospital  where Name='Rehman Medical College'";
        //      break;
        //  }
        //  return _objConnection._objOperation.DataTrigger_Ora_Get_All(objCity);
        //}
        private string[,] MakeArray1()
        {
            string[,] strarr = new string[1, 3];

            if (!this._CityName.Equals(Default))
            {
                strarr[0, 0] = "NAME";
                strarr[0, 1] = _CityName;
                strarr[0, 2] = "string";
            }
            

            return strarr;
        }
        private string[,] MakeArray()
        {
            string[,] strarr = new string[4, 3];

            if (!this._CityName.Equals(Default))
            {
                strarr[0, 0] = "NAME";
                strarr[0, 1] = _CityName;
                strarr[0, 2] = "string";
            }
            if (!this._EnteredBy.Equals(Default))
            {
                strarr[1, 0] = "ENTEREDBY";
                strarr[1, 1] = _EnteredBy;
                strarr[1, 2] = "string";
            }
            if (!this._EnteredOn.Equals(Default))
            {
                strarr[2, 0] = "ENTEREDON";
                strarr[2, 1] = _EnteredOn;
                strarr[2, 2] = "string";
            }
            if (!this._EnteredOn.Equals(Default))
            {
                strarr[3, 0] = "ACTIVE";
                strarr[3, 1] = "Y";
                strarr[3, 2] = "string";
            }


            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateCityName())
            {
                return false;
            }
            return true;
        }

        private bool ValidateCityName()
        {
            if (!this.CityName.Equals(Default))
            {


                return true;
            }
            else
            {
                Validation objValid = new Validation();


                this.StrErrorMessage = "Please Enter Valid City Name";
                return false;
            }
           
        }

        #endregion
    }
}
