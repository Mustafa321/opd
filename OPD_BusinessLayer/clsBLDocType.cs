using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using System.Data;

namespace OPD_BL
{
    public class clsBLDocType
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLDocType()
        {
        }

        public clsBLDocType(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tDocType";
        private string StrErrorMessage = "";

        private string _DocTypeID = Default;
        private string _Name = Default;
        private string _Description = Default;
        private string _Type= Default;
        private string _Active= Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        
        #endregion

        #region "Properties"

        public string DocTypeID
        {
            get { return _DocTypeID; }
            set { _DocTypeID = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active= value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT c.DocTypeID, c.Name, c.Description, c.EnteredBy, c.Active, c.EnteredOn, (case when type='D' then 'Document' else case when type='V' then 'Video' else 'Picture' end end) as Type FROM cm_tdoctype c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT c.DocTypeID, c.Name FROM cm_tdoctype c where c.Active='1' and Type='" + _Type + "'";
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[7, 3];

            if (!this._DocTypeID.Equals(Default))
            {
                strarr[0, 0] = "DocTypeID";
                strarr[0, 1] = _DocTypeID;
                strarr[0, 2] = "int";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[1, 0] = "Name";
                strarr[1, 1] = _Name;
                strarr[1, 2] = "string";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[2, 0] = "Description";
                strarr[2, 1] = _Description;
                strarr[2, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[3, 0] = "EnteredBy";
                strarr[3, 1] = _EnteredBy;
                strarr[3, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[4, 0] = "EnteredOn";
                strarr[4, 1] = _EnteredOn;
                strarr[4, 2] = "datetime";
            }

            if (!this._Type.Equals(Default))
            {
                strarr[5, 0] = "Type";
                strarr[5, 1] = _Type;
                strarr[5, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[6, 0] = "Active";
                strarr[6 ,1] = _Active;
                strarr[6, 2] = "string";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion
    }
}
