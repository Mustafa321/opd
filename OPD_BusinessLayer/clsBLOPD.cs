using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLOPD
    {

        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLOPD()
        {
        }

        public clsBLOPD(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tOPD";
        private string StrErrorMessage = "";

        private string _OPDID = Default;
        private string _PatientID = Default;
        private string _PRNO = Default;
        private string _MRNO = Default;
        private string _PresentingComplaints = Default;
        private string _Diagnosis= Default;
        private string _Signs = Default;
        private string _BP = Default;
        private string _Pulse= Default;
        private string _Temprature= Default;
        private string _Weight = Default;
        private string _Height = Default;
        private string _Active = Default;
        private string _DisposalPlan = Default;
        private string _Investigation = Default;
        private string _HeadCircumferences= Default;
        private string _Instuction = Default;
        private string _History = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _Status = Default;
        private string _BMI= Default;
        private string _BSA= Default;
        private string _FollowUpDate = Default;
        private string _Payment= Default;
        private string _VisitDate= Default;
        private string _temVDate = Default;
        private string _Notes= Default;
        private string _PaymentMode = Default;
        private string _RefNo = Default;
        private string _HeartRate = Default;
        private string _RespiratoryRate = Default;
        private string _Video = Default;

#endregion

        #region "Properties"

        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID = value; }
        }
        public string MRNO
        {
            get { return _MRNO; }
            set { _MRNO = value; }
        }
        public string TempVisitDate
        {
            get { return _temVDate; }
            set { _temVDate = value; }
        }

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID= value; }
        }

        public string PRNO
        {
            get { return _PRNO; }
            set { _PRNO = value; }
        }

        public string PresentingComplaints
        {
            get { return _PresentingComplaints; }
            set { _PresentingComplaints = value; }
        }

        public string Diagnosis
        {
            get { return _Diagnosis; }
            set { _Diagnosis= value; }
        }

        public string Signs
        {
            get { return _Signs; }
            set { _Signs = value; }
        }
        public string Video
        {
            get { return _Video; }
            set { _Video = value; }
        }


        public string BP
        {
            get { return _BP; }
            set { _BP = value; }
        }

        public string Pulse
        {
            get { return _Pulse ; }
            set { _Pulse= value; }
        }

        public string Temprature
        {
            get { return _Temprature ; }
            set { _Temprature = value; }
        }

        public string Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        public string Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public string DisposalPlan
        {
            get { return _DisposalPlan ; }
            set { _DisposalPlan = value; }
        }

        public string Investigation
        {
            get { return _Investigation; }
            set { _Investigation = value; }
        }

        public string HeadCircumferences
        {
            get { return _HeadCircumferences; }
            set { _HeadCircumferences = value; }
        }

        public string Instruction
        {
            get { return _Instuction; }
            set { _Instuction= value; }
        }

        public string History
        {
            get { return _History; }
            set { _History= value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public string BMI
        {
            get { return _BMI; }
            set { _BMI= value; }
        }

        public string BSA
        {
            get { return _BSA; }
            set { _BSA= value; }
        }

        public string FollowUpDate
        {
            get { return _FollowUpDate; }
            set { _FollowUpDate = value; }
        }

        public string Payment
        {
            get { return _Payment; }
            set { _Payment = value; }
        }

        public string VisitDate
        {
            get { return _VisitDate; }
            set { _VisitDate = value; }
        }

        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        public string PaymentMode
        {
            get { return _PaymentMode; }
            set { _PaymentMode = value; }
        }

        public string RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        public string RespiratoryRate
        {
            get { return _RespiratoryRate; }
            set { _RespiratoryRate = value; }
        }

        public string HeartRate
        {
            get { return _HeartRate; }
            set { _HeartRate = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    DataView dv = new DataView();

                    //_OPDID = objQB.QBGetMax("OPDIF", _TableName);
                    dv = GetAll(22);
                    if(dv.Table.Rows.Count>0)
                    {
                        StrErrorMessage = "Appointment on this date already exist";
                        return false;
                       
                    }
                    else
                    {
                        objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                        StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                        if (StrErrorMessage.Equals("Error"))
                        {
                            StrErrorMessage = _objConnection._objOperation.OperationError;
                            return false;
                        }
                        return true;

                    }
                   
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
       
        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArrayforUPDate(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool UpdateOPD()
        {
            try
            {
                objdbOPD.Query = "update "+ _TableName + " set PRNO='" + _PRNO + "' where PatientID=" + _PatientID;
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }
        }
        public bool UpdateSendStatus()
        {
          
                try
                {
                    objdbOPD.Query = "Update " + _TableName + " set IsSend=1";
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
           
        }
        public bool UpdateSendStatus(string opdId)
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = "Update " + _TableName + " set IsSend=1 where opdid='"+opdId+"'";
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool SetOPDFileName(string opdId,string FileName,int isSend)
        {
            try
            {
                objdbOPD.Query = "Update " + _TableName + " set FileName='" + FileName + "',IsFileSend=" + isSend + " where opdid='" + opdId + "'";
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }
        }

        public void Delete()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where OPDID=" + _OPDID;
            StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        public void DeleteAll()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where EnteredBy=" + _EnteredBy + " and visitDate = '" + _VisitDate + "'";
            StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        public DataView GetAll(int flag)
        {
           
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT `OPDID`,parientID `PRNO`, `Complaints`, `Diagnosis`, `Signs`, `BP`, `Pulse`, `EyeSight`, `Temprature`, `Weight`, `Height`, cast(Active as char) as Active, `DisposalPlan`, `Investigation`, `HeadCircumferences`, `History`, `Instruction`, `EnteredBy`, `EnteredOn`,status,BMI,BSA,FollowUpDate,Payment,visitdate,HeartRate,RespiratoryRate FROM  " + _TableName;
                    break;

                case 2:
                    objdbOPD.Query = "SELECT max(OPDID) FROM  " + _TableName;
                    break;

                case 3://get seleted visit Information
                    //objdbOPD.Query = "SELECT c.`OPDID`,c.patientID , c.`PRNO`, c.`PresentingComplaints`, c.`Diagnosis`, c.`DisposalPlan`, c.`Investigation`, c.`History`,c.`Signs`, EnteredOn FROM " + _TableName + " c where Active=1 and opdid=" + _OPDID ;
                    objdbOPD.Query = "SELECT c.`OPDID`,c.patientID , c.`PRNO`, c.`PresentingComplaints`, c.`Diagnosis`, c.`DisposalPlan`, c.`Investigation`, c.`History`,c.`Signs`, EnteredOn, c.`BP`, c.`Pulse`, c.`Temprature`, c.`Weight`, c.`Height`, c.`HeadCircumferences`, c.`Instruction`,c.BMI,c.BSA,c.FollowUpDate,c.Payment,visitdate,c.notes ,c.HeartRate,c.RespiratoryRate FROM " + _TableName + " c where Active=1 and opdid= " + _OPDID;
                    break;

                case 4://get last visit OPDID for seleted patient
                    //objdbOPD.Query = "SELECT c.`OPDID`  FROM " + _TableName + " c where enteredon in (select  max(enteredon) from cm_TOPD where prno='" + _PRNO + "' group by PRNO)";
                    objdbOPD.Query = "SELECT c.`OPDID` , visitdate FROM cm_topd c where prno='" + _PRNO + "' and Status!='W' and visitdate in (select  max(visitdate) from cm_TOPD where Status!='W' and prno='" + _PRNO + "' group by PRNO)";
                    //objdbOPD.Query += "  order by date_format(date(visitdate),'%y %m, %d') Desc";

                    break;

                case 5:
                    objdbOPD.Query = "SELECT c.`History`,c.notes FROM " + _TableName + " c where Active=1 and PRNO= '" + _PRNO + "' order by opdid";
                    break;

                case 6://all visit information for for selected patient
                    objdbOPD.Query = "SELECT OPDID,cast(date_format(date(visitdate),'%b %d, %Y') as char) as visitdate,History FROM " + _TableName + "  where Active=1 and PRNO= '" + _PRNO + "' ";
                    if (!_Status.Equals("") && !_Status.Equals(Default))
                    {
                        objdbOPD.Query += " and status = '" + _Status + "' ";
                    }
                    if (!_OPDID.Equals(""))
                    {
                        objdbOPD.Query += " and opdID <>'" + _OPDID + "' ";
                    }
                    objdbOPD.Query += "  order by date_format(date(visitdate),'%y %m, %d') Desc";
                    break;

                case 7:
                    //objdbOPD.Query = "SELECT count(patientID) as TotalPatient,sum(payment) as TotalAmount FROM "   + _TableName  + " where  date(enteredon) = date(now());";s
                    objdbOPD.Query = "SELECT payment FROM " + _TableName + " where  date(visitdate) = date(now())";
                    if (!_Status.Equals("") && !_Status.Equals(Default))
                    {
                        objdbOPD.Query += " and status = '" + _Status + "' ";
                    }
                    break;

                case 8:
                    objdbOPD.Query = "SELECT distinct(year(visitdate)) as Visityear FROM cm_topd c;";
                    //objdbOPD.Query = "SELECT min(visitdate)as visitdate FROM cm_topd  order by visitdate";
                    break;
                case 9://,pr.name as ItemName
                    #region For Fill Grid
                    if (!_Weight.Equals("") && !_Weight.Equals(Default))
                    {
                        objdbOPD.Query = _Weight;//Add Selection critaria for DOB
                    }
                    if (!_Temprature.Equals("") && !_Temprature.Equals(Default))
                    {
                        objdbOPD.Query = _Temprature;//for visit date
                    }
                    if (!_Pulse.Equals("") && !_Pulse.Equals(Default))
                    {
                        objdbOPD.Query = _Pulse;//for location aggregates(city count, and city name)
                    }
                    objdbOPD.Query += " SELECT o.PRNO, p.Name As PatientName,Group_ConCat(Distinct pr.name SEPARATOR ' , ')as itemName,o.visitdate ";
                    if (!_PresentingComplaints.Equals("") && !_PresentingComplaints.Equals(Default))
                    {
                        objdbOPD.Query += _PresentingComplaints;//for cityid and cityname in selection area for sub query
                    }
                    if (!_BP.Equals("") && !_BP.Equals(Default))
                    {
                        objdbOPD.Query += _BP;//Add DOB For Age Aggregate
                    }
                    objdbOPD.Query += " FROM cm_topd o,cm_tPatientReg p,cm_topdDetail od,cm_tPreference pr,cm_ttype t ";
                    if (!_Signs.Equals("") && !_Signs.Equals(Default))
                    {
                        objdbOPD.Query += _Signs;//set cm_tcity table in from table for location only
                    }
                    objdbOPD.Query += "where p.patientid=o.patientid and o. opdid=od.opdid and pr.prefid=od.prefid and pr.typeid=t.typeid ";
                    if (!_Payment.Equals("") && !_Payment.Equals(Default))
                    {
                        objdbOPD.Query += " and " + _Payment;//for join condition b/w cm_tpatientReg and cm_tcity
                    }
                    if (!_VisitDate.Equals("") && !_VisitDate.Equals(Default))
                    {
                        objdbOPD.Query += " and " + _VisitDate;//selection for visit date 
                    }
                    if (!_History.Equals("") && !_History.Equals(Default))
                    {
                        objdbOPD.Query += " and t.name= '" + _History + "'";//join condition for cm_tType and Cm_TPreeference 
                    }
                    if (!_Instuction.Equals("") && !_Instuction.Equals(Default))
                    {
                        objdbOPD.Query += " and pr.name= '" + _Instuction + "'";//join condition for cm_tPreference and Cm_TOPDDetail
                    }
                    if (!_BMI.Equals("") && !_BMI.Equals(Default))
                    {
                        if (_BMI.Equals(""))
                        {
                            objdbOPD.Query += " and  p.cityid in(-1)";//filter for selected cities
                        }
                        else
                        {
                            objdbOPD.Query += " and  p.cityid in(" + _BMI + ")";//filter for selected cities
                        }
                    }
                    if (!_Diagnosis.Equals(Default) && !_DisposalPlan.Equals(Default))//for selected ages(DOB)
                    {
                        objdbOPD.Query += " and date_format(p.DOB,'%Y/%m/%d') between '" + Convert.ToDateTime(_Diagnosis).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_DisposalPlan).ToString("yyyy/MM/dd") + "'";
                    }
                    objdbOPD.Query += "  group by o.opdid order by o.OPDID";
                    if (!_Investigation.Equals("") && !_Investigation.Equals(Default))
                    {
                        objdbOPD.Query += _Investigation;//for group by location to find location agregates
                    }
                    if (!_Status.Equals("") && !_Status.Equals(Default))
                    {
                        objdbOPD.Query += _Status;//for group by visitdate to find visitdate agregates
                    }
                    if (!_Height.Equals("") && !_Height.Equals(Default))
                    {
                        objdbOPD.Query += _Height;//for group by DOB to find DOB agregates
                    }
                    break;
                    #endregion
                case 10:
                    objdbOPD.Query = "SELECT pr.Patientid,o.opdid,pr.prno,pr.name as PatientName,pr.gender,pr.dob,pr.phoneno,pr.cellno,pr.address,c.name as CityName,pr.email,pn.panelname,ap.tokkenno,ap.AppointmentID FROM " + _TableName + " o, cm_tappointment ap ,cm_tpatientreg pr left outer join  CM_tPanel Pn  on pr.panelID=pn.PanelID,cm_tcity c where ap.prno=pr.prno and pr.patientid=o.patientid  and Appointmentdate='" + _EnteredOn + "' and pr.cityID=c.cityID and visitdate='" + _VisitDate + "' and o.status='w' and ap.personid=" + _EnteredBy + " and pr.prno='" + _PRNO + "'";
                    break;
                case 11:
                    objdbOPD.Query = "SELECT opdid FROM cm_topd c WHERE DATE(C.visitdate)=DATE(Now()) AND PRNO='" + PRNO + "'";
                    //objdbOPD.Query = "SELECT min(visitdate)as visitdate FROM cm_topd  order by visitdate";
                    break;
                case 12:
                    objdbOPD.Query = "SELECT pr.Patientid,o.opdid,pr.prno,pr.name as PatientName,pr.gender,pr.dob,pr.phoneno,pr.cellno,pr.address,c.name as CityName,pr.email,pn.panelname,o.payment FROM " + _TableName + " o, cm_tpatientreg pr left outer join  CM_tPanel Pn  on pr.panelID=pn.PanelID,cm_tcity c where pr.patientid=o.patientid  and pr.cityID=c.cityID and visitdate='" + _VisitDate + "' and o.status='w' and pr.prno='" + _PRNO + "'";
                    break;

                case 13://all visit information for for selected patient
                    objdbOPD.Query = "SELECT PresentingComplaints as dgComplaints, Diagnosis as dgDiagnosis, DisposalPlan as dgPlans, Investigation as dgInvestigation, History as dgHistory, Signs as dgSigns FROM " + _TableName + "  where opdID =" + _OPDID;
                    break;
                case 14://all vital sign information for selected patient
                    objdbOPD.Query = "SELECT BP, Pulse, Temprature, Weight, Height, HeadCircumferences,VisitDate,HeartRate,RespiratoryRate FROM cm_topd where PRNo='" + _PRNO + "'";
                    break;
                case 15://Maximum OPDID
                    objdbOPD.Query = "select max(OPDID) from cm_tOPD";
                    break;
                case 16://Min Year 
                    objdbOPD.Query = "select cast(min(date_format(visitdate,'%Y' )) as char)  as MinYear from cm_tOPD";
                    break;
                case 17:
                    objdbOPD.Query = "SELECT count(opdid) as PatientCount,cast(date_format(visitdate,'%d') as char) as visitDay FROM cm_topd where date_format(visitdate,'%m/%Y')='" + _VisitDate + "' and EnteredBy= " + _EnteredBy + " group by date_format(visitdate,'%d/%m/%Y') order by date_format(visitdate,'%Y/%m/%d') Asc";
                    break;
                case 18:
                    objdbOPD.Query = "Select pr.PatientID,pr.PRNo, pr.Email from cm_tOPD o,cm_tpatientreg pr where pr.patientid=o.patientid and  o.EnteredBy=" + _EnteredBy + " and visitDate = '" + _VisitDate + "'";
                    break;
                    case 19:
                        objdbOPD.Query = "SELECT c.BP, c.Pulse, c.Temprature, c.Weight, c.Height, c.HeadCircumferences, c.BMI, c.BSA,HeartRate,RespiratoryRate FROM cm_topd c where opdid=" + _OPDID;
                    break;
                    case 20:
                        objdbOPD.Query = "SELECT cast(date_format(date(visitdate),'%b %d, %Y') as char) as visitdate,Notes FROM cm_topd where notes is not null and notes<>'' and Patientid=" + _PatientID;
                    break;
                case 21:
                    objdbOPD.Query = "SELECT pr.Patientid,pr.UserName,pr.Password,o.opdid,o.PresentingComplaints,o.Diagnosis,o.Signs,o.BP,o.Pulse,o.Temprature,o.Weight,o.Height,o.Active,o.DisposalPlan,o.Investigation,o.HeadCircumferences,o.Instruction,o.History,o.EnteredOn,o.Status,o.BMI,o.BSA,o.FollowUpDate,o.VisitDate,o.Notes,o.HeartRate,o.RespiratoryRate FROM " + _TableName + " o inner join  cm_tpatientreg pr on  pr.patientid=o.patientid where IsSend=0";
                    break;
                case 22:
                    objdbOPD.Query = "select * from " + _TableName + " where PatientID=" + _PatientID + "  and visitdate='" + _temVDate + "'";
                    break;
                case 23://all visit information for for selected patient
                    objdbOPD.Query = "SELECT OPDID,cast(date_format(date(visitdate),'%b %d, %Y') as char) as visitdate,History FROM " + _TableName + "  where Active=1 and  Status!='W'and PatientID= '" + _PatientID + "' ";
                    if (!_Status.Equals("") && !_Status.Equals(Default))
                    {
                        objdbOPD.Query += " and status = '" + _Status + "' ";
                    }
                    //if (!_OPDID.Equals(Default))
                    //{
                    //    objdbOPD.Query += " and opdID <>'" + _OPDID + "' ";
                    //}
                    objdbOPD.Query += "  order by date_format(date(visitdate),'%y %m, %d') Desc";
                    break;
                case 24://get last visit OPDID for seleted patient
                    //objdbOPD.Query = "SELECT c.`OPDID`  FROM " + _TableName + " c where enteredon in (select  max(enteredon) from cm_TOPD where prno='" + _PRNO + "' group by PRNO)";
                    objdbOPD.Query = "SELECT c.`OPDID` , visitdate FROM cm_topd c where prno='" + _PRNO + "'  and visitdate in (select  max(visitdate) from cm_TOPD where  prno='" + _PRNO + "' group by PRNO)";
                    //objdbOPD.Query += "  order by date_format(date(visitdate),'%y %m, %d') Desc";

                    break;
            }
            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

	 	public bool UpdatePK (string query )
		{
			try
			{
				//_OPDID = objQB.QBGetMax("OPDIF", _TableName);
				objdbOPD.Query = query;

				StrErrorMessage = _objConnection._objOperation.DataTrigger_Ora_Update(objdbOPD);

				if (StrErrorMessage.Equals("Error"))
				{
					StrErrorMessage = _objConnection._objOperation.OperationError;
					return false;
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

	  //public DataView GetAllOra(int flag)
	  //{
	  //  switch (flag)
	  //  {
	  //    case 1:
	  //      objdbOPD.Query = "select Hospitalid from hr_thospital  where Name='Rehman Medical College'";
	  //      break;
	  //  }
	  //  return _objConnection._objOperation.DataTrigger_Ora_Get_All(objdbOPD);
	  //}

        private string[,] MakeArray()
        {
            string[,] strarr = new string[32, 3];

            if (!this._OPDID.Equals(Default))
            {
                strarr[0, 0] = "OPDID";
                strarr[0, 1] = _OPDID;
                strarr[0, 2] = "int";
            }

            if (!this._PatientID.Equals(Default))
            {
                strarr[1, 0] = "PatientID";
                strarr[1, 1] = _PatientID;
                strarr[1, 2] = "int";
            }

            if (!this._PRNO.Equals(Default))
            {
                strarr[2, 0] = "PRNO";
                strarr[2, 1] = _PRNO;
                strarr[2, 2] = "string";
            }

            if (!this._PresentingComplaints.Equals(Default))
            {
                strarr[3, 0] = "PresentingComplaints";
                strarr[3, 1] = _PresentingComplaints;
                strarr[3, 2] = "string";
            }

            if (!this._Diagnosis.Equals(Default))
            {
                strarr[4, 0] = "Diagnosis";
                strarr[4, 1] = _Diagnosis;
                strarr[4, 2] = "string";
            }

            if (!this._Signs.Equals(Default))
            {
                strarr[5, 0] = "Signs";
                strarr[5, 1] = _Signs;
                strarr[5, 2] = "string";
            }

            if (!this._BP.Equals(Default))
            {
                strarr[6, 0] = "BP";
                strarr[6, 1] = _BP;
                strarr[6, 2] = "string";
            }

            if (!this._Pulse.Equals(Default))
            {
                strarr[7, 0] = "Pulse";
                strarr[7, 1] = _Pulse;
                strarr[7, 2] = "string";
            }

            if (!this._Temprature.Equals(Default))
            {
                strarr[8, 0] = "Temprature";
                strarr[8, 1] = _Temprature;
                strarr[8, 2] = "string";
            }

            if (!this._Weight.Equals(Default))
            {
                strarr[9, 0] = "Weight";
                strarr[9, 1] = _Weight;
                strarr[9, 2] = "string";
            }

            if (!this._Height.Equals(Default))
            {
                strarr[10, 0] = "Height";
                strarr[10, 1] = _Height;
                strarr[10, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[11, 0] = "Active";
                strarr[11, 1] = _Active;
                strarr[11, 2] = "int";
            }

            if (!this._DisposalPlan.Equals(Default))
            {
                strarr[12, 0] = "DisposalPlan";
                strarr[12, 1] = _DisposalPlan;
                strarr[12, 2] = "string";
            }

            if (!this._Investigation.Equals(Default))
            {
                strarr[13, 0] = "Investigation";
                strarr[13, 1] = _Investigation ;
                strarr[13, 2] = "string";
            }


            if (!this._HeadCircumferences.Equals(Default))
            {
                strarr[14, 0] = "HeadCircumferences";
                strarr[14, 1] = _HeadCircumferences;
                strarr[14, 2] = "string";
            }

            if (!this._Instuction.Equals(Default))
            {
                strarr[15, 0] = "Instruction";
                strarr[15, 1] = _Instuction;
                strarr[15, 2] = "string";
            }

            if (!this._History.Equals(Default))
            {
                strarr[16, 0] = "History";
                strarr[16, 1] = _History;
                strarr[16, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[17, 0] = "EnteredBy";
                strarr[17, 1] = _EnteredBy;
                strarr[17, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[18, 0] = "EnteredOn";
                strarr[18, 1] = _EnteredOn;
                strarr[18, 2] = "datetime";
            }

            if (!this._Status.Equals(Default))
            {
                strarr[19, 0] = "Status";
                strarr[19, 1] = _Status;
                strarr[19, 2] = "string";
            }

            if (!this._BMI.Equals(Default))
            {
                strarr[20, 0] = "BMI";
                strarr[20, 1] = _BMI;
                strarr[20, 2] = "string";
            }

            if (!this._BSA.Equals(Default))
            {
                strarr[21, 0] = "BSA";
                strarr[21, 1] = _BSA;
                strarr[21, 2] = "string";
            }

            if (!this._FollowUpDate.Equals(Default))
            {
                strarr[22, 0] = "FollowUpDate";
                strarr[22, 1] = _FollowUpDate;
                strarr[22, 2] = "datetime";
            }

            if (!this._Payment.Equals(Default))
            {
                strarr[23, 0] = "Payment";
                strarr[23, 1] = _Payment;
                strarr[23, 2] = "int";
            }
            
            if (!this._VisitDate.Equals(Default))
            {
                strarr[24, 0] = "VisitDate";
                strarr[24, 1] = _VisitDate;
                strarr[24, 2] = "datetime";
            }
            if (!this._Notes.Equals(Default))
            {
                strarr[25, 0] = "Notes";
                strarr[25, 1] = _Notes;
                strarr[25, 2] = "string";
            }
            if (!this._PaymentMode.Equals(Default))
            {
                strarr[26, 0] = "PaymentMode";
                strarr[26, 1] = _PaymentMode;
                strarr[26, 2] = "string";
            }

            if (!this.RefNo.Equals(Default))
            {
                strarr[27, 0] = "ReferenceNo";
                strarr[27, 1] = _RefNo;
                strarr[27, 2] = "string";
            }

            if (!this._HeartRate.Equals(Default))
            {
                strarr[28, 0] = "HeartRate";
                strarr[28, 1] = _HeartRate;
                strarr[28, 2] = "string";
            }

            if (!this._RespiratoryRate.Equals(Default))
            {
                strarr[29, 0] = "RespiratoryRate";
                strarr[29, 1] = _RespiratoryRate;
                strarr[29, 2] = "string";
            }
            if (!this.Video.Equals(Default))
            {
                strarr[29, 0] = "Video";
                strarr[29, 1] = Video;
                strarr[29, 2] = "string";
            }

            strarr[30, 0] = "IsSend";
            strarr[30, 1] = "0";
            strarr[30, 2] = "string";


            strarr[31, 0] = "MRNo";
            strarr[31, 1] = _MRNO;
            strarr[31, 2] = "string";


            return strarr;
        }
        private string[,] MakeArrayforUPDate()
        {
            string[,] strarr = new string[30, 3];

            if (!this._OPDID.Equals(Default))
            {
                strarr[0, 0] = "OPDID";
                strarr[0, 1] = _OPDID;
                strarr[0, 2] = "int";
            }

            if (!this._PatientID.Equals(Default))
            {
                strarr[1, 0] = "PatientID";
                strarr[1, 1] = _PatientID;
                strarr[1, 2] = "int";
            }

            //if (!this._PRNO.Equals(Default))
            //{
            //    strarr[2, 0] = "PRNO";
            //    strarr[2, 1] = _PRNO;
            //    strarr[2, 2] = "string";
            //}

            if (!this._PresentingComplaints.Equals(Default))
            {
                strarr[3, 0] = "PresentingComplaints";
                strarr[3, 1] = _PresentingComplaints;
                strarr[3, 2] = "string";
            }

            if (!this._Diagnosis.Equals(Default))
            {
                strarr[4, 0] = "Diagnosis";
                strarr[4, 1] = _Diagnosis;
                strarr[4, 2] = "string";
            }

            if (!this._Signs.Equals(Default))
            {
                strarr[5, 0] = "Signs";
                strarr[5, 1] = _Signs;
                strarr[5, 2] = "string";
            }

            if (!this._BP.Equals(Default))
            {
                strarr[6, 0] = "BP";
                strarr[6, 1] = _BP;
                strarr[6, 2] = "string";
            }

            if (!this._Pulse.Equals(Default))
            {
                strarr[7, 0] = "Pulse";
                strarr[7, 1] = _Pulse;
                strarr[7, 2] = "string";
            }

            if (!this._Temprature.Equals(Default))
            {
                strarr[8, 0] = "Temprature";
                strarr[8, 1] = _Temprature;
                strarr[8, 2] = "string";
            }

            if (!this._Weight.Equals(Default))
            {
                strarr[9, 0] = "Weight";
                strarr[9, 1] = _Weight;
                strarr[9, 2] = "string";
            }

            if (!this._Height.Equals(Default))
            {
                strarr[10, 0] = "Height";
                strarr[10, 1] = _Height;
                strarr[10, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[11, 0] = "Active";
                strarr[11, 1] = _Active;
                strarr[11, 2] = "int";
            }

            if (!this._DisposalPlan.Equals(Default))
            {
                strarr[12, 0] = "DisposalPlan";
                strarr[12, 1] = _DisposalPlan;
                strarr[12, 2] = "string";
            }

            if (!this._Investigation.Equals(Default))
            {
                strarr[13, 0] = "Investigation";
                strarr[13, 1] = _Investigation;
                strarr[13, 2] = "string";
            }


            if (!this._HeadCircumferences.Equals(Default))
            {
                strarr[14, 0] = "HeadCircumferences";
                strarr[14, 1] = _HeadCircumferences;
                strarr[14, 2] = "string";
            }

            if (!this._Instuction.Equals(Default))
            {
                strarr[15, 0] = "Instruction";
                strarr[15, 1] = _Instuction;
                strarr[15, 2] = "string";
            }

            if (!this._History.Equals(Default))
            {
                strarr[16, 0] = "History";
                strarr[16, 1] = _History;
                strarr[16, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[17, 0] = "EnteredBy";
                strarr[17, 1] = _EnteredBy;
                strarr[17, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[18, 0] = "EnteredOn";
                strarr[18, 1] = _EnteredOn;
                strarr[18, 2] = "datetime";
            }

            if (!this._Status.Equals(Default))
            {
                strarr[19, 0] = "Status";
                strarr[19, 1] = _Status;
                strarr[19, 2] = "string";
            }

            if (!this._BMI.Equals(Default))
            {
                strarr[20, 0] = "BMI";
                strarr[20, 1] = _BMI;
                strarr[20, 2] = "string";
            }

            if (!this._BSA.Equals(Default))
            {
                strarr[21, 0] = "BSA";
                strarr[21, 1] = _BSA;
                strarr[21, 2] = "string";
            }

            if (!this._FollowUpDate.Equals(Default))
            {
                strarr[22, 0] = "FollowUpDate";
                strarr[22, 1] = _FollowUpDate;
                strarr[22, 2] = "datetime";
            }

            if (!this._Payment.Equals(Default))
            {
                strarr[23, 0] = "Payment";
                strarr[23, 1] = _Payment;
                strarr[23, 2] = "int";
            }

            if (!this._VisitDate.Equals(Default))
            {
                strarr[24, 0] = "VisitDate";
                strarr[24, 1] = _VisitDate;
                strarr[24, 2] = "datetime";
            }
            if (!this._Notes.Equals(Default))
            {
                strarr[25, 0] = "Notes";
                strarr[25, 1] = _Notes;
                strarr[25, 2] = "string";
            }
            if (!this._PaymentMode.Equals(Default))
            {
                strarr[26, 0] = "PaymentMode";
                strarr[26, 1] = _PaymentMode;
                strarr[26, 2] = "string";
            }

            if (!this.RefNo.Equals(Default))
            {
                strarr[27, 0] = "ReferenceNo";
                strarr[27, 1] = _RefNo;
                strarr[27, 2] = "string";
            }

            if (!this._HeartRate.Equals(Default))
            {
                strarr[28, 0] = "HeartRate";
                strarr[28, 1] = _HeartRate;
                strarr[28, 2] = "string";
            }

            if (!this._RespiratoryRate.Equals(Default))
            {
                strarr[29, 0] = "RespiratoryRate";
                strarr[29, 1] = _RespiratoryRate;
                strarr[29, 2] = "string";
            }
            if (!this.Video.Equals(Default))
            {
                strarr[29, 0] = "Video";
                strarr[29, 1] = Video;
                strarr[29, 2] = "string";
            }

            strarr[2, 0] = "IsSend";
            strarr[2, 1] = "0";
            strarr[2, 2] = "string";


      


            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidatePayment())
            {
                return false;
            }
            return true;
        }

        private bool ValidatePayment()
        {
            if (!this._Payment.Equals(Default))
            {
                Validation objValid = new Validation();

                if (!objValid.IsNumber(this._Payment) && !this._Payment.Equals(""))
                {
                    this.StrErrorMessage = "Payment has some invalid characters..";
                    return false;
                }
                objValid = null;
            }
            return true;
        }

        #endregion
    }
}
