using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using DataLayer;

namespace OPD_BL
{
  public class clsBLDBConnection
  {
    public clsoperation _objOperation;

    public clsBLDBConnection()
    {
      _objOperation = new clsoperation();
    }

    public void Connection_Open()
    {
      _objOperation.Open_Connection();
    }

    public void Transaction_Begin()
    {
      _objOperation.Start_Transaction();
    }

    public void Transaction_ComRoll()
    {
      _objOperation.End_Transaction();
    }

    public void Connection_Close()
    {
      _objOperation.Close_Connection();
    }


    /// <summary>
    /// For Oracle
    /// </summary>
    //public void Ora_Connection_Open (string flag)
    //{
    //    _objOperation.Open_Ora_Connection(flag);
    //}

	public void Ora_Transaction_Begin ( )
	{
		_objOperation.Start_Ora_Transaction();
	}

	public void Ora_Transaction_ComRoll ( )
	{
		_objOperation.End_Ora_Transaction();
	}

	public void Ora_Connection_Close ( )
	{
		_objOperation.Close_Ora_Connection();
	}
    ////////////
    public void SetRollBackStatus()
    {
      _objOperation.StrMsg = "Error";
    }
  }
}
