using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLType
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLType()
        {
        }

        public clsBLType(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tType";
        private string StrErrorMessage = "";

        private string _TypeID = Default;
        private string _Name= Default;
        private string _Description = Default;
        private string _Active = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _ClientID = Default;
        private string _PersonID = Default;
        private string _DefaultVal = Default;

        #endregion

        #region "Properties"

        public string TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Description
        {
            get { return _Description ; }
            set { _Description= value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active  = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID= value; }
        }
        public string DefaultVal
        {
            get { return _DefaultVal; }
            set { _DefaultVal = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    //objdbOPD.Query = "SELECT c.`TypeID`, c.`Name`, c.`Description`, c.`Active` FROM " + _TableName + " c";
                    objdbOPD.Query = "SELECT distinct (t.TypeID), t.Name, p.Description FROM cm_ttype t,cm_tPreference p where t.typeid=p.typeid and p.personID= " + _PersonID;
                    break;
                case 2:
                    objdbOPD.Query = "SELECT TypeID,Name FROM cm_ttype";
                    break;
                case 3:
					objdbOPD.Query = "SELECT c.`TypeID`, c.`Name`, c.`Description`, cast(c.Active as char) as Active, case when DefaultSel ='' then 'N'  else   DefaultSel end as DefaultSel  FROM " + _TableName + " c  where Name= '" + _Name + "' and Active='1' and  personid = " + PersonID;

                    
                    
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[9, 3];

            if (!this._TypeID.Equals(Default))
            {
                strarr[0, 0] = "TypeID";
                strarr[0, 1] = _TypeID;
                strarr[0, 2] = "int";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[1, 0] = "Name";
                strarr[1, 1] = _Name;
                strarr[1, 2] = "string";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[2, 0] = "Description";
                strarr[2, 1] = _Description;
                strarr[2, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[3, 0] = "Active";
                strarr[3, 1] = _Active;
                strarr[3, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[4, 0] = "EnteredBy";
                strarr[4, 1] = _EnteredBy;
                strarr[4, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[5, 0] = "EnteredOn";
                strarr[5, 1] = _EnteredOn;
                strarr[5, 2] = "datetime";
            }

            if (!this._ClientID.Equals(Default))
            {
                strarr[6, 0] = "ClientID";
                strarr[6, 1] = _ClientID;
                strarr[6, 2] = "int";
            }
            
            if (!this._PersonID.Equals(Default))
            {
                strarr[7, 0] = "PersonID";
                strarr[7, 1] = _PersonID;
                strarr[7, 2] = "int";
            }

            if (!this._DefaultVal.Equals(Default))
            {
                strarr[8, 0] = "DefaultSel";
                strarr[8, 1] = _DefaultVal;
                strarr[8, 2] = "string";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateInstructions())
            {
                return false;
            }

            return true;
        }

        private bool ValidateInstructions()
        {
            if (!_Description.Equals(Default))
            {
                if(_Description.Equals(""))
                {
                    StrErrorMessage = "Please Enter Instructions";
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
