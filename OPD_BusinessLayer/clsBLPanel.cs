using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLPanel
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLPanel()
        {
        }

        public clsBLPanel(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tPanel";
        private string StrErrorMessage = "";

        private string _PanelID = Default;
        private string _PanelName = Default;
        private string _Active= Default;
        private string _DefaultFee = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _ClientID = Default;
        
        #endregion

        #region "Properties"

        public string PanelID
        {
            get { return _PanelID; }
            set { _PanelID = value; }
        }

        public string PanelName
        {
            get { return _PanelName; }
            set { _PanelName = value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public string DefaultFee
        {
            get { return _DefaultFee; }
            set { _DefaultFee = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
					objdbOPD.Query = "SELECT `PanelID`, `PanelName`, cast(Active as char) as Active, `DefaultFee`, `EnteredBy`, `EnteredOn`, `ClientID` from " + _TableName + " where 1=1 ";// and PanelName != 'Select'";
                    if(!_PanelID.Equals(Default))
                    {
                        objdbOPD.Query += " and PanelID = " + _PanelID ;
                    }
                    break;
                case 2:
                    objdbOPD.Query = "select t.* from pr_tpatientreg t";// and PanelName != 'Select'";
                    break;
                case 3:
                    objdbOPD.Query = "SELECT c.`Medicineid`, c.`Name`, c.`UrduDosage`, c.`PatientInfo`, cast(c.Active as char) as Active, c.`PersonID`, c.`EnteredBy`, c.`EnteredOn`, c.`EngDosage`, c.`NumTime` FROM  cm_tMedicine c";
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[7, 3];

            if (!this._PanelID.Equals(Default))
            {
                strarr[0, 0] = "PanelID";
                strarr[0, 1] = _PanelID;
                strarr[0, 2] = "int";
            }

            if (!this._PanelName.Equals(Default))
            {
                strarr[1, 0] = "PanelName";
                strarr[1, 1] = _PanelName;
                strarr[1, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[2, 0] = "Active";
                strarr[2, 1] = _Active;
                strarr[2, 2] = "string";
            }

            if (!this._DefaultFee.Equals(Default))
            {
                strarr[3, 0] = "DefaultFee";
                strarr[3, 1] = _DefaultFee;
                strarr[3, 2] = "int";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[4, 0] = "EnteredBy";
                strarr[4, 1] = _EnteredBy;
                strarr[4, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[5, 0] = "EnteredOn";
                strarr[5, 1] = _EnteredOn;
                strarr[5, 2] = "datetime";
            }

            if (!this._ClientID.Equals(Default))
            {
                strarr[6, 0] = "ClientID";
                strarr[6, 1] = _ClientID;
                strarr[6, 2] = "int";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }
        
        #endregion
    }
}
