using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
	public class clsBLDosageInstruction
	{
		clsBLDBConnection objConnection = new clsBLDBConnection();
		clsdbOPD objDbOPD = new clsdbOPD();
		QueryBuilder objQB = new QueryBuilder();

		public clsBLDosageInstruction()
		{
 
		}

		public clsBLDosageInstruction(clsBLDBConnection objConnection)
		{
			this.objConnection = objConnection;
		}

		#region Class Variable

		private const string StrTable = "cm_tDosageInstruction";
		private const string Default = "~!@";
		private string StrErrorMsg = "";

		private string StrDosageInstructionID = Default;
		private string StrDescription = Default;
		private string StrActive = Default;
		private string StrPersonID = Default;
		private string StrClientID = Default;
		private string StrEnteredBy = Default;
		private string StrEnteredOn = Default;

		#endregion

		#region Properties

		public string DosageInstructionID
		{
			get { return StrDosageInstructionID; }
			set { StrDosageInstructionID = value; }
		}

		public string Description
		{
			get { return StrDescription; }
			set { StrDescription = value; }
		}

		public string Active
		{
			get { return StrActive; }
			set { StrActive = value;}
		}

		public string PersonID
		{
			get { return StrPersonID; }
			set { StrPersonID = value; }
		}

		public string EnteredBy
		{
			get { return StrEnteredBy; }
			set { StrEnteredBy = value; }
		}

		public string EnteredOn
		{
			get { return EnteredOn; }
			set { StrEnteredOn = value; }
		}

		public string ClientID
		{
			get { return StrClientID; }
			set { StrClientID = value; }
		}

		public string ErrorMsg
		{
			get { return StrErrorMsg; }
			set { StrErrorMsg = value; }
		}
		#endregion

		#region Method

		public bool Insert()
		{
			if (Validate())
			{
				try
				{
					objDbOPD.Query = objQB.QBInsert(MakeArray(), StrTable);
					StrErrorMsg = this.objConnection._objOperation.DataTrigger_Insert(objDbOPD);

					if (StrErrorMsg.Equals("Error"))
					{

						StrErrorMsg = objConnection._objOperation.OperationError;
						return false;
					}
					return true;
				}
				catch (Exception exc)
				{
					StrErrorMsg = exc.Message;
					return false;
				}
			}
			return false;
		}

		public bool Update()
		{
			if (Validate())
			{
				try 
				{
					objDbOPD.Query = objQB.QBUpdate(MakeArray(), StrTable);
					StrErrorMsg = objConnection._objOperation.DataTrigger_Update(objDbOPD);

					if (StrErrorMsg.Equals("Error"))
					{
						StrErrorMsg = objConnection._objOperation.OperationError;
						return false;
					}
					return true;
				}
				catch (Exception exc)
				{
					StrErrorMsg = exc.Message;
					return false;
				}
			}
			return false;
		}

		public DataView GetAll(int Flag)
		{
			switch (Flag)
			{
				case 1:
					objDbOPD.Query = "SELECT DosageInstructionID, Description, cast(Active as char) as Active,PersonID, EnteredBy, EnteredOn, ClientID FROM cm_tdosageinstruction  where personid=" + StrPersonID;
					break;
				case 2:
					objDbOPD.Query = "SELECT DosageInstructionID, Description FROM cm_tdosageinstruction  where Active='Y' and language = 'u' and Personid = " + StrPersonID;
					break;
                case 3:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=1 and language = 'p' order by dorder";
                    break;
                case 4:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=2 and language = 'p' order by dorder";
                    break;
                case 6:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=4 and language = 'p' order by dorder";
                    break;
                case 7:
                    objDbOPD.Query = "SELECT DosageInstructionID, Description FROM cm_tdosageinstruction  where Active='Y' and language = 'p' and Personid = " + StrPersonID; 
                    break;
                case 8:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=1 and language = 'u' order by dorder";
                    break;
                case 9:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=2 and language = 'u' order by dorder";
                    break;
                case 10:
                    objDbOPD.Query = "SELECT Name FROM cm_tmedicinepreference c where level=4 and language = 'u' order by dorder";
                    break;

			}
			return objConnection._objOperation.DataTrigger_Get_All(objDbOPD);
		}

		private string[,] MakeArray()
		{
			string[,] strArr = new string[7, 3];

			if(!StrDosageInstructionID.Equals(Default))
			{
				strArr[0, 0] = "DosageInstructionID";
				strArr[0, 1] = StrDosageInstructionID;
				strArr[0, 2] = "int";
			}
			if (!StrDescription.Equals(Default))
			{
				strArr[1, 0] = "Description";
				strArr[1, 1] = StrDescription;
				strArr[1, 2] = "string";
			}
			if (!StrActive.Equals(Default))
			{
				strArr[2, 0] = "Active";
				strArr[2, 1] = StrActive;
				strArr[2, 2] = "string";
			}
			if (!StrPersonID.Equals(Default))
			{
				strArr[3, 0] = "PersonID";
				strArr[3, 1] = StrPersonID;
				strArr[3, 2] = "int";
			}
			if (!StrEnteredBy.Equals(Default))
			{
				strArr[4, 0] = "EnteredBy";
				strArr[4, 1] = StrEnteredBy;
				strArr[4, 2] = "int";
			}
			if (!StrEnteredOn.Equals(Default))
			{
				strArr[5, 0] = "EnteredOn";
				strArr[5, 1] = StrEnteredOn;
				strArr[5, 2] = "datetime";
			}
			if (!StrClientID.Equals(Default))
			{
				strArr[6, 0] = "ClientID";
				strArr[6, 1] = StrClientID;
				strArr[6, 2] = "int";
			}

			return strArr;
		}

		#endregion

		#region Validate

		private bool Validate()
		{
			if (!ValidateDescription())
			{
				return false;
			}

			return true;
		}

		private bool ValidateDescription()
		{
			if (StrDescription.Equals(Default) || StrDescription.Equals(""))
			{
				StrErrorMsg = "Description is Empty";
				return false;
			}
			return true;
		}

		#endregion
	}
}
