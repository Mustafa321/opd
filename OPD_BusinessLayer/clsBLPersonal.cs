using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DataLayer;
using System.Globalization;

namespace OPD_BL
{
    public class clsBLPersonal
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLPersonal()
        {
        }

        public clsBLPersonal(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tPersonal";
        private string StrErrorMessage = "";

        private string _PersonID = Default;
        private string _Title= Default;
        private string _Name = Default;
        private string _Gender = Default;
        private string _MaritalStatus = Default;
        private string _DOB = Default;
        private string _PhoneNo = Default;
        private string _CellNo = Default;
        private string _NIC = Default;
        private string _Address = Default;
        private string _BloodGroup = Default;
        private string _Designation = Default;
        private string _Rank = Default;
        private string _Education= Default;
        private string _LoginID = Default;
        private string _Pasword = Default;
        private string _RCPasword = Default;
        private string _Hint = Default;
        private string _RegistrationDate = Default;
        private string _Active = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _ClientID = Default;
        private string _Specialty = Default;
        private string _Type = Default;
        private string _Email = Default;
        private string _EmailText = Default;
        private string _Charges = Default;
        private string _RevisitCharges = Default;
        private string _RevisitDays = Default;
        private string _MaxSlot = Default;
        private string _Expire = Default;
        #endregion

        #region "Properties"

        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID = value; }
        }
        public string Expire
        {
            get { return _Expire; }
            set { _Expire = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public string MaritalStatus
        {
            get { return _MaritalStatus ; }
            set { _MaritalStatus  = value; }
        }

        public string DOB
        {
            get { return _DOB ; }
            set { _DOB= value; }
        }

        public string PhoneNo
        {
            get { return _PhoneNo; }
            set { _PhoneNo = value; }
        }

        public string CellNo
        {
            get { return _CellNo; }
            set { _CellNo = value; }
        }

        public string NIC
        {
            get { return _NIC; }
            set { _NIC= value; }
        }

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string BloodGroup
        {
            get { return _BloodGroup ; }
            set { _BloodGroup = value; }
        }

        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }

        public string Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }

        public string Education
        {
            get { return _Education; }
            set { _Education = value; }
        }

        public string LoginID
        {
            get { return _LoginID; }
            set { _LoginID = value; }
        }

        public string Pasword
        {
            get { return _Pasword; }
            set { _Pasword = value; }
        }

        public string RCPasword
        {
            get { return _RCPasword; }
            set { _RCPasword = value; }
        }

        public string Hint
        {
            get { return _Hint; }
            set { _Hint = value; }
        }

        public string RegistrationCode
        {
            get { return _RegistrationDate; }
            set { _RegistrationDate = value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        public string Specialty
        {
            get { return _Specialty; }
            set { _Specialty = value; }
        }

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email= value; }
        }

        public string EmailText
        {
            get { return _EmailText; }
            set { _EmailText = value; }
        }

        public string Charges
        {
            get { return _Charges; }
            set { _Charges = value; }
        }

        public string RevisitCharges
        {
            get { return _RevisitCharges; }
            set { _RevisitCharges = value; }
        }

        public string RevisitDays
        {
            get { return _RevisitDays; }
            set { _RevisitDays = value; }
        }

        public string maxSlot
        {
            get { return _MaxSlot; }
            set { _MaxSlot = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT c.PersonID, c.Title, c.Name, c.Gender, c.MaritalStatus, c.DOB, c.PhoneNo, c.CellNo, c.NIC, c.Address, c.BloodGroup, c.Education, c.Designation, c.Rank, c.LoginID, c.Pasword, c.Hint, cast(c.Active as char) as Active, c.Specialty,c.Type,Email,EmailText, MaxSlot, Charges, ReVisitCharges, RevisitDays FROM " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT Bloodgroup FROM cm_bloodgroup;";
                    break;
                  case 3:
                    objdbOPD.Query = "SELECT PersonID,Title ,Name,loginID,pasword,Hint,cast(Active as char) as Active,Type,RefPersonID,Expire FROM " + _TableName + " where LoginID = '" + _LoginID + "' and Active='1'";
                    break;
                case 4:
                    objdbOPD.Query = "SELECT PersonID,NIC FROM " + _TableName + " where NIC= '" + _NIC + "'";
                    break;
                case 5:
                    objdbOPD.Query = "SELECT p.Email as fromMessage ,pr.Email as toMessage FROM cm_tpersonal p,cm_tpatientreg pr where p.personid = " + _Name + " and pr.patientid = " + _Address;
                     break;
                case 6:
                    objdbOPD.Query = "SELECT PersonID,name FROM " + _TableName + " where Type='C' and active =1 and PersonId!=1";
                    break;
                case 7:
                    objdbOPD.Query = "SELECT maxslot FROM " + _TableName + " where Active=1 and PersonId=" + _PersonID;
                    break;
                case 8:
                    objdbOPD.Query = "SELECT PersonID, EmailText, MaxSlot, Charges, ReVisitCharges, RevisitDays FROM " + _TableName + " where Active=1 and PersonId=" + _PersonID;
                    break;
                case 9:
                    objdbOPD.Query = "    SELECT cast(date_format(visitDate,'%Y%m%d') as char) as VisitDate FROM cm_topd where patientID= " + _Type + " and EnteredBy=" + _PersonID + " order by date_format(visitDate,'%Y/%m/%d') desc ";
                    break;
            }
            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[31, 3];

            if (!this._PersonID.Equals(Default))
            {
                strarr[0, 0] = "PersonID";
                strarr[0, 1] = _PersonID;
                strarr[0, 2] = "int";
            }

            if (!this._Title.Equals(Default))
            {
                strarr[1, 0] = "Title";
                strarr[1, 1] = _Title;
                strarr[1, 2] = "string";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[2, 0] = "Name";
                strarr[2, 1] = _Name;
                strarr[2, 2] = "string";
            }

            if (!this._Gender.Equals(Default))
            {
                strarr[3, 0] = "Gender";
                strarr[3, 1] = _Gender;
                strarr[3, 2] = "string";
            }

            if (!this._MaritalStatus.Equals(Default))
            {
                strarr[4, 0] = "MaritalStatus";
                strarr[4, 1] = _MaritalStatus;
                strarr[4, 2] = "string";
            }

            if (!this._DOB.Equals(Default))
            {
                strarr[5, 0] = "DOB";
                strarr[5, 1] = _DOB;
                strarr[5, 2] = "datetime";
            }

            if (!this._PhoneNo.Equals(Default))
            {
                strarr[6, 0] = "PhoneNo";
                strarr[6, 1] = _PhoneNo;
                strarr[6, 2] = "string";
            }

            if (!this._CellNo.Equals(Default))
            {
                strarr[7, 0] = "CellNo";
                strarr[7, 1] = _CellNo;
                strarr[7, 2] = "string";
            }

            if (!this._NIC.Equals(Default))
            {
                strarr[8, 0] = "NIC";
                strarr[8, 1] = _NIC;
                strarr[8, 2] = "string";
            }

            if (!this._Address.Equals(Default))
            {
                strarr[9, 0] = "Address";
                strarr[9, 1] = _Address;
                strarr[9, 2] = "string";
            }

            if (!this._BloodGroup.Equals(Default))
            {
                strarr[10, 0] = "BloodGroup";
                strarr[10, 1] = _BloodGroup;
                strarr[10, 2] = "string";
            }

            if (!this._Designation.Equals(Default))
            {
                strarr[11, 0] = "Designation";
                strarr[11, 1] = _Designation;
                strarr[11, 2] = "string";
            }

            if (!this._Rank.Equals(Default))
            {
                strarr[12, 0] = "Rank";
                strarr[12, 1] = _Rank;
                strarr[12, 2] = "string";
            }

            if (!this._Education.Equals(Default))
            {
                strarr[13, 0] = "Education";
                strarr[13, 1] = _Education;
                strarr[13, 2] = "string";
            }

            if (!this._LoginID.Equals(Default))
            {
                strarr[14, 0] = "LoginID";
                strarr[14, 1] = _LoginID;
                strarr[14, 2] = "string";
            }

            if (!this._Pasword.Equals(Default))
            {
                strarr[15, 0] = "Pasword";
                strarr[15, 1] = _Pasword;
                strarr[15, 2] = "string";
            }

            if (!this._Hint.Equals(Default))
            {
                strarr[16, 0] = "Hint";
                strarr[16, 1] = _Hint;
                strarr[16, 2] = "string";
            }

            if (!this._RegistrationDate.Equals(Default))
            {
                strarr[17, 0] = "registrationDate";
                strarr[17, 1] = _RegistrationDate;
                strarr[17, 2] = "datetime";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[18, 0] = "Active";
                strarr[18, 1] = _Active;
                strarr[18, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[19, 0] = "EnteredBy";
                strarr[19, 1] = _EnteredBy;
                strarr[19, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[20, 0] = "EnteredOn";
                strarr[20, 1] = _EnteredOn;
                strarr[20, 2] = "datetime";
            }

            if (!this._ClientID.Equals(Default))
            {
                strarr[21, 0] = "ClientID";
                strarr[21, 1] = _ClientID;
                strarr[21, 2] = "int";
            }


            if (!this._Specialty.Equals(Default))
            {
                strarr[22, 0] = "Specialty";
                strarr[22, 1] = _Specialty;
                strarr[22, 2] = "string";
            }

            if (!this._Type.Equals(Default))
            {
                strarr[23, 0] = "Type";
                strarr[23, 1] = _Type;
                strarr[23, 2] = "string";
            }

            if (!this._Email.Equals(Default))
            {
                strarr[24, 0] = "Email";
                strarr[24, 1] = _Email;
                strarr[24, 2] = "string";
            }
            if (!_EmailText.Equals(Default))
            {
                strarr[25, 0] = "EmailText";
                strarr[25, 1] = _EmailText;
                strarr[25, 2] = "string";
            }

            if (!this._Charges.Equals(Default))
            {
                strarr[26, 0] = "Charges";
                strarr[26, 1] = _Charges;
                strarr[26, 2] = "int";
            }

            if (!_RevisitCharges.Equals(Default))
            {
                strarr[27, 0] = "RevisitCharges";
                strarr[27, 1] = _RevisitCharges;
                strarr[27, 2] = "int";
            }

            if (!_RevisitDays.Equals(Default))
            {
                strarr[28, 0] = "RevisitDays";
                strarr[28, 1] = _RevisitDays;
                strarr[28, 2] = "int";
            }

            if (!_MaxSlot.Equals(Default))
            {
                strarr[29, 0] = "MaxSlot";
                strarr[29, 1] = _MaxSlot;
                strarr[29, 2] = "int";
            }
            if (!_Expire.Equals(Default))
            {
                strarr[30, 0] = "Expire";
                strarr[30, 1] = _Expire;
                strarr[30, 2] = "string";
            }
            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateName())
            {
                return false;
            }
            else if (!ValidateDOB())
            {
                return false;
            }
            else if (!ValidatePhoneNo())
            {
                return false;
            }
            else if (!ValidatCellNo())
            {
                return false;
            }
            else if (!ValidateNIC())
            {
                return false;
            }
            else if (!ValidatLoginID())
            {
                return false;
            }
            else if (!ValidatPassword())
            {
                return false;
            }
            else if (!ValidateEmail())
            {
                return false;
            }
            if (!_Expire.ToString().Equals(Default))
            {
                if (_Expire.ToString().Equals(""))
                {
                    StrErrorMessage = "Expire Date is not allowed to be Empty";
                   
                    return false;
                }

               

            }

            return true;
        }

        private bool ValidateName()
        {
            Validation objValid = new Validation();

            if (!_Name.ToString().Equals(Default))
            {
                if (_Name.ToString().Equals(""))
                {
                    StrErrorMessage = "Name is not allowed to be Empty";
                    objValid = null;
                    return false;
                }

                if (!objValid.IsName(_Name))
                {
                    StrErrorMessage = "Invalid Character in Name field";
                    objValid = null;
                    return false;
                }
 
            }
            objValid = null;
            return true;
        }

        private bool ValidateDOB()
        {
              CultureInfo objCultureInfo = new CultureInfo("ur-pk");
            if (!this._DOB.Equals(Default))
            {
                if (DateTime.Compare(Convert.ToDateTime(Convert.ToDateTime(_DOB, objCultureInfo).ToString("MM/dd/yyyy")), DateTime.Now.Date) >= 0)
                {
                    this.StrErrorMessage = "Date of Birth is not more then current date";
                    return false;
                }
            }
            return true;
        }

        private bool ValidatePhoneNo()
        {
            if (!this._PhoneNo.Equals(Default))
            {
                Validation objValid = new Validation();

                if (!objValid.IsNumber(this._PhoneNo) && !this._PhoneNo.Equals(""))
                {
                    this.StrErrorMessage = "Phone Number has some invalid characters..";
                    return false;
                }
                objValid = null;
            }
            return true;
        }

        private bool ValidatCellNo()
        {
            if (!this._CellNo.Equals(Default))
            {
                Validation objValid = new Validation();

                if (!objValid.IsNumber(this._CellNo) && !this._CellNo.Equals(""))
                {
                    this.StrErrorMessage = "Cell number has some invalid characters.";
                    return false;
                }

                objValid = null;
            }
            return true;
        }

        private bool ValidateNIC()
        {
            Validation objValid = new Validation();

            if (!this._NIC.Equals(Default))
            {

                if (!objValid.IsNIC(_NIC))
                {
                    StrErrorMessage = "Invalid NIC format";
                    return false;
                }

                DataView dv = this.GetAll(4);

                if (!this._PersonID.Equals(Default))
                {
                    dv.RowFilter = "PersonID <> " + this._PersonID;
                }
                if (dv.Count > 0)
                {
                    StrErrorMessage = "Duplicate NIC";
                    return false;
                }
            }
            return true;
        }

        private bool ValidatLoginID()
        {
            if (!this._LoginID.Equals(Default))
            {
                DataView dv = this.GetAll(3);

                if (!this._PersonID.Equals(Default))
                {
                    dv.RowFilter = "PersonID <> " + this._PersonID ;
                }
                if (dv.Count >0)
                {
                    StrErrorMessage= "User already Exist";
                    return false;
                }
            }
            return true;
        }

        private bool ValidatPassword()
        {
            if (!this._Pasword.Equals(Default))
            {
                
                if (!_Pasword.Equals(_RCPasword))
                {
                    StrErrorMessage = "Password did not match";
                    return false;
                }
            }
            return true;
        }

        private bool ValidateEmail()
        {
            Validation objValid = new Validation();

            if (!_Email.ToString().Equals(Default))
            {
                if (!objValid.IsEmail(_Email))
                {
                    StrErrorMessage = "Invalid Email Address";
                    objValid = null;
                    return false;
                }
            }
            objValid = null;
            return true;
        }

        #endregion
    }
}
