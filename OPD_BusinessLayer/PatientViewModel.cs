﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD_BL
{
   public class PatientViewModel
    {
        public decimal Id { get; set; }
        public int PatientId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MRNo { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string WhatsappNumber { get; set; }
        public string Address { get; set; }
        public string cityName { get; set; }
        public string Email { get; set; }
        public string MartialStatus { get; set; }
        public string ClinicName { get; set; }
        public decimal ClinicId { get; set; }
        public decimal DoctorId { get; set; }
        public decimal CityId { get; set; }
    }
    public class PatientOPDDetails
    {
        public decimal? Id { get; set; }
        public decimal? DoctorId { get; set; }
        public string Patientid { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string opdid { get; set; }
        public string PresentingComplaints { get; set; }
        public string Diagnosis { get; set; }
        public string Signs { get; set; }
        public string BP { get; set; }
        public string Pulse { get; set; }
        public string Temprature { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Active { get; set; }
        public string DisposalPlan { get; set; }
        public string Investigation { get; set; }
        public string HeadCircumferences { get; set; }
        public string Instruction { get; set; }
        public string History { get; set; }
        public string EnteredOn { get; set; }
        public string Status { get; set; }
        public string BMI { get; set; }
        public string BSA { get; set; }
        public string FollowUpDate { get; set; }
        public string VisitDate { get; set; }
        public string Notes { get; set; }
        public string HeartRate { get; set; }
        public string RespiratoryRate { get; set; }
        public List<PatientMedicineMedicine> Medicinelist { get; set; }
    }
    public class PatientMedicineMedicine
    {
        public string MedicineId { get; set; }
        public string OpdId { get; set; }
        public string MedicineName { get; set; }
        public string Dosage { get; set; }
        public string UrduDosage { get; set; }
        public string FarsiDosage { get; set; }
        public string Morning { get; set; }
        public string Night { get; set; }
        public string Evening { get; set; }
        public string Afternoon { get; set; }
        public string DaysCount { get; set; }
        public string Instruction { get; set; }
      

    }
}
