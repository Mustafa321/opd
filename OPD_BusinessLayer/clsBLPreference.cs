using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLPreference
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLPreference()
        {
        }

        public clsBLPreference(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tPreference";
        private string StrErrorMessage = "";

        private string _PrefID = Default;
        private string _Name= Default;
        private string _TypeID = Default;
        private string _Description = Default;
        private string _Active = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _ClientID = Default;
        private string _PersonID = Default;
        private string _NumTime = Default;
        private string _DiseaseIDRef = "0";   

        #endregion

        #region "Properties"

        public string PrefID
        {
            get { return _PrefID; }
            set { _PrefID = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public string Description
        {
            get { return _Description ; }
            set { _Description= value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active  = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID = value; }
        }

        public string NumTime   
        {
            get { return _NumTime; }
            set { _NumTime = value; }
        }

        public string DiseaseIDRef
        {
            get { return _DiseaseIDRef; }
            set { _DiseaseIDRef = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                  _PrefID = Default;
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public bool UpdateDescription()
        {
            try
            {
                objdbOPD.Query = "update " + _TableName + " set Description ='" + _Description + "' where TypeID =" + _TypeID + " and PersonID = " + _PersonID;
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
					objdbOPD.Query = "SELECT c.`PrefID`, c.`Name`, c.`TypeID`, c.`Description`, cast(c.Active as char) as Active FROM " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT c.PrefID,c.Name,c.DiseaseIDref  FROM " + _TableName + " c , cm_tType t where c.typeID=t.typeid and c.active=1 and c.PersonID = " + _PersonID + " and  t.typeid= " + _TypeID;
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and c.name = '" + _Name + "'";
                    }
                    objdbOPD.Query += " order by c.Name ";
                    break;
                case 3:
                    objdbOPD.Query = "SELECT maX(PrefID) FROM " + _TableName;
                    break;
                case 4:
                    objdbOPD.Query = "SELECT c.PrefID,c.Name,c.DiseaseIDref  FROM " + _TableName + " c , cm_tType t where c.typeID=t.typeid and c.active=1 and t.typeid= " + _TypeID;
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and c.name = '" + _Name + "'";
                    }
                    break;
                case 5:
                    objdbOPD.Query = "SELECT PrefID FROM " + _TableName + " where typeID= " + _TypeID + " and active=1 and UPPER(name) = UPPER('" + _Name + "')";
                    break;
                case 6:
                    objdbOPD.Query = "SELECT p.*,t.name as tname  FROM cm_tpreference p,cm_ttype t  where p.typeid=t.typeid and p.personid = " + _PersonID + " order by t.name,p.name"  ;
                    break;
                case 7:
                    objdbOPD.Query = "SELECT distinct(typeid) as TypeID, Description FROM cm_tpreference where TypeID=" + _TypeID;
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[11, 3];

            if (!this._PrefID.Equals(Default))
            {
                strarr[0, 0] = "PrefID";
                strarr[0, 1] = _PrefID;
                strarr[0, 2] = "int";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[1, 0] = "Name";
                strarr[1, 1] = _Name;
                strarr[1, 2] = "string";
            }

            if (!this._TypeID.Equals(Default))
            {
                strarr[2, 0] = "TypeID";
                strarr[2, 1] = _TypeID;
                strarr[2, 2] = "int";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[3, 0] = "Description";
                strarr[3, 1] = _Description;
                strarr[3, 2] = "string";
            }

            if (!this._Active.Equals(Default))
            {
                strarr[4, 0] = "Active";
                strarr[4, 1] = _Active;
                strarr[4, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[5, 0] = "EnteredBy";
                strarr[5, 1] = _EnteredBy;
                strarr[5, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[6, 0] = "EnteredOn";
                strarr[6, 1] = _EnteredOn;
                strarr[6, 2] = "datetime";
            }

            if (!this._ClientID.Equals(Default))
            {
                strarr[7, 0] = "ClientID";
                strarr[7, 1] = _ClientID;
                strarr[7, 2] = "int";
            }

            if (!this._PersonID.Equals(Default))
            {
                strarr[8, 0] = "PersonID";
                strarr[8, 1] = _PersonID;
                strarr[8, 2] = "int";
            }
            if (!this._NumTime.Equals(Default))
            {
                strarr[9, 0] = "NumTime";
                strarr[9, 1] = _NumTime;
                strarr[9, 2] = "int";
            }
            if (!this._DiseaseIDRef.Equals(Default))
            {
                strarr[10, 0] = "DISEASEIDRef";
                strarr[10, 1] = _DiseaseIDRef;
                strarr[10, 2] = "int";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!validateName())
            {
                return false;
            }
            return true;
        }

        private bool validateName()
        {
            DataView dv = new DataView();
            if (!_Name.Equals(Default) )
            {
                dv=this.GetAll(2);
                if (!_PrefID.Equals(Default))
                {
                    dv.RowFilter=" PrefID <> " + _PrefID;
                }
                if (dv.Count >= 1)
                {
                    StrErrorMessage = "Type is already entered ";
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
