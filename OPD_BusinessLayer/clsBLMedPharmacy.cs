using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using System.Data;

namespace OPD_BL
{
  public class clsBLMedPharmacy
  {
    clsBLDBConnection _objConnection;
    clsdbOPD objdbOPD= new clsdbOPD();
    
    public clsBLMedPharmacy()
    {

    }
    
    public clsBLMedPharmacy(clsBLDBConnection objConnection)
    {
      _objConnection=objConnection;
    }

    #region Class Variable

    private const string _Table = "cm_tMedPharmacy";
    private const string Default = "~!@";
    private string StrErrorMsg = "";

    private string _OPDID = Default;
    private string _MedID = Default;
    private string _Status = Default;
    private string _QtyDemanded = Default;
    private string _QtyIssued = Default;
    private string _IssuedBy = Default;
    private string _IssuedOn = Default;
    private string _Price = Default;
    private string _StoreID = Default;

    #endregion

    #region Properties

    public string OPDID
    {
      get { return _OPDID; }
      set { _OPDID = value; }
    }

    public string MedID
    {
      get { return _MedID; }
      set { _MedID = value; }
    }

    public string Status
    {
      get { return _Status; }
      set { _Status = value; }
    }

    public string QtyDemand
    {
      get { return _QtyDemanded; }
      set { _QtyDemanded = value; }
    }

    public string QtyIssued
    {
      get { return _QtyIssued; }
      set { _QtyIssued = value; }
    }

    public string IssuedBy
    {
      get { return _IssuedBy; }
      set { _IssuedBy = value; }
    }

    public string IssuedOn
    {
      get { return _IssuedOn; }
      set { _IssuedOn = value; }
    }

    public string Price
    {
      get { return _Price; }
      set { _Price = value; }
    }

    public string StoreID
    {
      get { return _StoreID; }
      set { _StoreID = value; }
    }

    #endregion

    #region Method

    public bool Insert()
    {
      QueryBuilder objQuery = new QueryBuilder();
      if (Validate())
      {
        try
        {
          objdbOPD.Query = objQuery.QBInsert(MakeArray(), _Table);
          StrErrorMsg = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

          if (StrErrorMsg.Equals("Error"))
          {
            StrErrorMsg = _objConnection._objOperation.OperationError;
            return false;
          }
          return true;
        }
        catch (Exception exc)
        {
          StrErrorMsg = exc.Message;
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    public bool Update()
    {
      QueryBuilder objQueruy = new QueryBuilder();

      if (Validate())
      {
        try
        {
          objdbOPD.Query = objQueruy.QBUpdate(MakeArray(), _Table);
          StrErrorMsg = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

          if (StrErrorMsg.Equals("Error"))
          {
            StrErrorMsg = _objConnection._objOperation.OperationError;
            return false;
          }
          return true;
        }
        catch (Exception exc)
        {
          StrErrorMsg = exc.Message;
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    public DataView Getall(int Flag)
    {
      QueryBuilder objQuery = new QueryBuilder();
      //switch (Flag)
      //{
    
      
      //}
      return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
    }

    private string[,] MakeArray()
    {
      string[,] strarr = new string[9, 3];

      if (!_OPDID.Equals(Default))
      {
        strarr[0, 0] = "OPDID";
        strarr[0, 1] = _OPDID;
        strarr[0, 2] = "int";
      }
      
      if (!_MedID.Equals(Default))
      {
        strarr[1, 0] = "MedID";
        strarr[1, 1] = _MedID;
        strarr[1, 2] = "int";
      }

      if (!_Status.Equals(Default))
      {
        strarr[2, 0] = "Status";
        strarr[2, 1] = _Status;
        strarr[2, 2] = "string";
      }

      if (!_QtyDemanded.Equals(Default))
      {
        strarr[3, 0] = "QtyDemand";
        strarr[3, 1] = _QtyDemanded;
        strarr[3, 2] = "int";
      }

      if(!_QtyIssued.Equals(Default))
      {
        strarr[4, 0] = "QtyIssued";
        strarr[4, 1] = _QtyIssued;
        strarr[4, 2] = "int";
      }

      if (_IssuedBy.Equals(Default))
      {
        strarr[5, 0] = "IssuedBy";
        strarr[5, 1] = _IssuedBy;
        strarr[5, 2] = "int";
      }

      if (_IssuedOn.Equals(Default))
      {
        strarr[6, 0] = "issuedOn";
        strarr[6, 1] = _IssuedOn;
        strarr[6, 2] = "datetime";
      }

      if(_StoreID.Equals(Default))
      {
        strarr[7, 0] = "StoreID";
        strarr[7, 1] = _StoreID;
        strarr[7, 2] = "int";
      }

      if (!_Price.Equals(Default))
      {
        strarr[8, 0] = "Price";
        strarr[8, 1] = _Price;
        strarr[8, 2] = "int";
      }

      return strarr;
    }

    #endregion

    #region Validation

    private bool Validate()
    {
      return true;
    }

    #endregion


  }
}

