using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using System.Data;

namespace OPD_BL
{
    public class clsBLAdmissionProc
    {
          clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLAdmissionProc()
        {
        }

        public clsBLAdmissionProc(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tpatientadmissionproc";
        private string StrErrorMessage = "";

        private string _AdmissionProcID = Default;
        private string _Name = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _Type= Default;
        private string _Active = Default;
        private string _PatientID = Default;
        
        #endregion

        #region "Properties"

        public string AdmissionProcID
        {
            get { return _AdmissionProcID; }
            set { _AdmissionProcID = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        
        public string Active
        {
            get { return _Active; }
            set { _Active= value; }
        }
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT AdmissionProcID, Name, EnteredOn, EnteredBy, Type,active FROM cm_tpatientadmissionproc where patientid= " +_PatientID;
                    if (!_Type.Equals(Default))
                    {
                        objdbOPD.Query += " and type ='" + _Type+ "'";
                    }
                    break;
                    case 2:
                    objdbOPD.Query = "SELECT cast(Date_format(enteredon,'%b %d, %Y') as char) as Enteredon, name as ProcAdmName FROM cm_tpatientadmissionproc where Type = '"+ _Type +"' and PatientID="+ _PatientID ;
                    break;
                    
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[7, 3];

            if (!this._AdmissionProcID.Equals(Default))
            {
                strarr[0, 0] = "AdmissionProcID";
                strarr[0, 1] = _AdmissionProcID;
                strarr[0, 2] = "int";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[1, 0] = "Name";
                strarr[1, 1] = _Name;
                strarr[1, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[2, 0] = "EnteredBy";
                strarr[2, 1] = _EnteredBy;
                strarr[2, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[3, 0] = "EnteredOn";
                strarr[3, 1] = _EnteredOn;
                strarr[3, 2] = "datetime";
            }

            if (!this._Type.Equals(Default))
            {
                strarr[4, 0] = "type";
                strarr[4, 1] = _Type;
                strarr[4, 2] = "string";
            }
            if (!this._Active.Equals(Default))
            {
                strarr[5, 0] = "Active";
                strarr[5, 1] = _Active;
                strarr[5, 2] = "string";
            }
            if (!this._PatientID.Equals(Default))
            {
                strarr[6, 0] = "patientID";
                strarr[6, 1] = _PatientID;
                strarr[6, 2] = "int";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion
    }
}
