using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;
using System.Globalization;

namespace OPD_BL
{
    public class clsBLPatientReg
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLPatientReg()
        {
        }

        public clsBLPatientReg(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        #region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tPatientReg";
        private string StrErrorMessage = "";
        private string _UserName = Default;
        private string _Password = Default;
        private string _PatientID = Default;
        private string _PRNO = Default;
        private string _Name = Default;
        private string _Gender = Default;
        private string _DOB = Default;
        private string _PhoneNo = Default;
        private string _CellNo = Default;
        private string _Address = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _FromDate = Default;
        private string _ToDate = Default;
        private string _CityID = Default;
        private string _Email = Default;
        private string _PanelID = Default;
        private string _TempDOB = Default;
        private string _MRNo = Default;
        private string _MaritalStatus = Default;
        private string _EmpNo = Default;
        private string _RefPRNO = Default;

        private string _OPDID = Default;
        private string _SynchedToServer = Default;
        private string _Status = Default; //Hold and Consulted

        #endregion

        #region "Properties"

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string MRNo
        {
            get { return _MRNo; }
            set { _MRNo = value; }
        }
        public string SynchedToServer
        {
            get { return _SynchedToServer; }
            set { _SynchedToServer = value; }
        }
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string PRNO
        {
            get { return _PRNO; }
            set { _PRNO = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public string DOB
        {
            get { return _DOB; }
            set { _DOB = value; }
        }
        public string TempDOB
        {
            get { return _TempDOB; }
            set { _TempDOB = value; }
        }

        public string PhoneNo
        {
            get { return _PhoneNo; }
            set { _PhoneNo = value; }
        }

        public string CellNo
        {
            get { return _CellNo; }
            set { _CellNo = value; }
        }

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string CityID
        {
            get { return _CityID; }
            set { _CityID = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        public string FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public string ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string PanelID
        {
            get { return _PanelID; }
            set { _PanelID = value; }
        }

        public string MaritalStatus
        {
            get { return _MaritalStatus; }
            set { _MaritalStatus = value; }
        }

        public string EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID = value; }
        }

        public string RefPRNO
        {
            get { return _RefPRNO; }
            set { _RefPRNO = value; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool UpdatePatientsStatus()
        {

            try
            {
                objdbOPD.Query = "Update " + _TableName + " set SynchedToServer=1";
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }

        }
        public bool UpdatePatientsStatus(int PatientId)
        {
           
                try
                {
                    objdbOPD.Query = "Update " + _TableName + " set SynchedToServer=1 where PatientID='"+PatientId+"'";
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
          
        }
        public bool UpdateUserNameAndPassword(string username,string password,int patientID)
        {

            try
            {
                objdbOPD.Query = "Update " + _TableName + " set UserName='"+username+ "' ,Password='"+password+ "' where PatientID ="+ patientID + "";
                StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                if (StrErrorMessage.Equals("Error"))
                {
                    StrErrorMessage = _objConnection._objOperation.OperationError;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                StrErrorMessage = e.Message;
                return false;
            }

        }
        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArrayForUpdate(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            var mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            var n = mask.Replace("a", " ").TrimStart();
            n = n.TrimEnd();
            CultureInfo objCultureInfo = new CultureInfo("ur-pk");

            switch (flag)
            {
                //case 1:
                //    objdbOPD.Query = "SELECT `PatientID`, `PRNO`, p.Name, case when Gender ='M' then 'Male'  else   'Female' end as Gender, `DOB`,case when (month(dob) <= month(now()) )then  (year(now())-year( DOB)) else year(now())-year(DOB)-1 end as Age,case when (month(dob) = month(now()) && day(dob) >= day(now())) then  '11' when(month(dob) = month(now()) && day(dob) <= day(now())) THEN '0' when (month(dob) <  month(now()) && DAY(dob) <=  DAY(now())) then (month(now())-month( DOB)) when (month(dob) <  month(now()) && DAY(dob) >  DAY(now())) then (month(now())-month( DOB))-1 when (month(dob) >  month(now()) && DAY(dob) <  DAY(now())) then 12 -(month( DOB)-month(now())) when (month(dob) >  month(now()) && DAY(dob) >=  DAY(now())) then 11 -(month( DOB)-month(now())) end as month, case when (day(dob) = day(now())) then  '0' when (day(NOW()) >  DAY(DOB)) then (day(NOW())-day( dob)) when (day(NOW()) <  DAY(DOB)) then CASE WHEN (MONTH(DOB)=2) THEN case when (year(DoB)%4=0) then (29 - day(DOB)) + (day(NOW())) else (28 - day(DOB)) + (day(NOW())) end WHEN ( MONTH(DOB)=4 or MONTH(DOB)=6 or MONTH(DOB)=9 or MONTH(DOB)=11) THEN (30 - day(DOB)) + (day(NOW())) else  (31 - day(DOB)) + (day(NOW())) END end as days, `PhoneNo`, `CellNo`, `Address`,c.name as cityname,Email ,pn.panelName FROM  " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID, cm_tcity c  where p.cityID=c.cityID ";
                //    if (!_PRNO.Equals(Default) && !_PRNO.Equals("") && !_PRNO.Equals("lblPRNO"))
                //    {
                //        objdbOPD.Query += " and Prno = '" + _PRNO + "'";
                //    }
                //    break;
                case 1:
                    objdbOPD.Query = "SELECT `PatientID`, `PRNO`, p.Name, case when Gender ='M' then 'Male'  else   'Female' end as Gender, `DOB`, `PhoneNo`, `CellNo`, `Address`,c.name as cityname,Email ,pn.panelName,cast(case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end as char) AS age,MaritalStatus,EmployNo  FROM  " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID, cm_tcity c  where p.cityID=c.cityID ";
                    if (!_PRNO.Equals(Default) && !_PRNO.Equals("") && !_PRNO.Equals("lblPRNO"))
                    {
                        objdbOPD.Query += " and Prno = '" + _PRNO + "'";
                    }
                    break;
                case 2:
                    //objdbOPD.Query = "SELECT `PatientID`, `PRNO`, p.Name as PatientName, case when Gender ='M' then 'Male'  else   'Female' end as Gender, `DOB`, `PhoneNo`, `CellNo`, `Address`,c.name as cityname,p.EnteredBy,p.Email,pn.panelName FROM " + _TableName + " p, cm_tcity c,CM_tPanel Pn where p.cityID=c.cityID and pn.panelID=p.PanelID and p.EnteredOn between '" + Convert.ToDateTime(_ToDate, objCultureInfo).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_FromDate, objCultureInfo).ToString("yyyy/MM/dd") + "' ";

                    objdbOPD.Query = "SELECT `PatientID`, `PRNO`, p.Name as PatientName, case when Gender ='M' then 'Male'  else   'Female' end as Gender,p.DOB,cast( case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end   as char) AS age , `PhoneNo`, `CellNo`, `Address`,c.name as cityname,p.EnteredBy,p.Email,pn.panelName FROM " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID,cm_TCITY c where p.cityID=c.cityID";

                    if (!_ToDate.Equals(Default) && !_ToDate.Equals(""))
                    {
                        objdbOPD.Query += " and p.EnteredOn between '" + Convert.ToDateTime(_ToDate, objCultureInfo).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_FromDate, objCultureInfo).ToString("yyyy/MM/dd") + "' ";
                    }

                    if (!_PRNO.Equals(Default) && !_PRNO.Equals(mask))
                    {
                        objdbOPD.Query += " and (MRNo = '" + _MRNo + "' or PRNO = '" + _PRNO + "')";
                    }
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and p.Name like '%" + _Name + "%' ";
                    }
                    if (!_PhoneNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.PhoneNo like '%" + _PhoneNo + "%' ";
                    }
                    if (!_CellNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.CellNo like '%" + _CellNo + "%' ";
                    }
                    objdbOPD.Query += "  order by p.Name";
                    break;

                case 3:
                    objdbOPD.Query = "SELECT c.`CITYID`, c.`NAME` FROM cm_tcity c where Active='Y' order by c.name";
                    break;

                case 4:
                    //objdbOPD.Query = "SELECT p.PatientID, p.PRNO , p.Name as PatientName, case when p.Gender ='M' then 'Male'  else   'Female' end as Gender, p.DOB, p.PhoneNo, p.CellNo, p.Address,p.EnteredBy,c.name as cityname,o.opdid,o.visitdate,p.email,pn.panelName from " + _TableName + " p,cm_TOPD o,cm_tcity c,CM_tPanel Pn where p.cityID=c.cityID and pn.panelID=p.PanelID and p.PRNO=o.prno and o.status= '" + _Status + "' and o.visitdate between '" + Convert.ToDateTime(_ToDate, objCultureInfo).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_FromDate, objCultureInfo).ToString("yyyy/MM/dd") + "' ";
                    objdbOPD.Query = "SELECT p.PatientID, p.PRNO , p.Name as PatientName, case when p.Gender ='M' then 'Male'  else   'Female' end as Gender, p.DOB,cast(case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end as Char) AS age ,o.visitdate, p.PhoneNo, p.CellNo, p.Address,p.EnteredBy,c.name as cityname,o.opdid,p.email,pn.panelName from " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID,cm_TOPD o,cm_tcity c where p.cityID=c.cityID and p.patientID=o.patientid and o.status= '" + _Status + "' ";
                    if (!_FromDate.Equals(Default) && !_ToDate.Equals(""))
                    {
                        objdbOPD.Query += " and o.visitdate between '" + Convert.ToDateTime(_FromDate, objCultureInfo).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_ToDate, objCultureInfo).ToString("yyyy/MM/dd") + "' ";
                    }

                    if (!_PRNO.Equals(Default) && !_PRNO.Equals(n))
                    {
                        objdbOPD.Query += " and (MRNo = '" + _MRNo + "' or PRNO = '" + _PRNO + "')";
                    }
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and p.Name like '%" + _Name + "%' ";
                    }
                    if (!_PhoneNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.PhoneNo like '%" + _PhoneNo + "%' ";
                    }
                    if (!_CellNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.CellNo like '%" + _CellNo + "%' ";
                    }
                    objdbOPD.Query += " order by VisitDate DESC";

                    break;
                case 5:
                    objdbOPD.Query = "SELECT PatientID, PRNO, p.Name, Gender, DOB, CellNo, PhoneNo, Address, p.EnteredBy, p.EnteredOn, c.name as cityname FROM " + _TableName + " p, cm_tcity c where p.cityID=c.cityID and patientID in (select max(patientID) from " + _TableName + ")";
                    break;
                case 6:
                    objdbOPD.Query = "SELECT max(patientid) as PatientID FROM cm_tpatientreg";
                    break;
                case 7:
                    objdbOPD.Query = "SELECT patientid FROM cm_tpatientreg where PRNo = '" + _PRNO + "'";
                    break;
                case 8:
                    objdbOPD.Query = "SELECT p.PatientID, p.PRNO , p.Name as PatientName, case when p.Gender ='M' then 'Male'  else   'Female' end as Gender, p.DOB,cast(case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end as char)AS age ,o.visitdate, p.PhoneNo, p.CellNo, p.Address,p.EnteredBy,c.name as cityname,o.opdid,p.email,pn.panelName from " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID,cm_tcity c,cm_topd o where p.cityID=c.cityID and p.patientid=o.patientid and o.status= '" + _Status + "' and DATE(o.visitdate)=DATE(Now()) ";
                    objdbOPD.Query += " order by VisitDate DESC";
                    break;
                case 9:
                    objdbOPD.Query = "SELECT p.PatientID, p.PRNO, p.Name as PatientName, case when Gender ='M' then 'Male' else 'Female' end as Gender,cast(case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end as char) AS age ,p.dob as  DOB, `PhoneNo`, `CellNo`, `Address`, c.name as cityname,o.status,o.OPDID as ROPDID ,p.Email,Pn.PanelName,p.enteredby  FROM " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID, cm_tcity c,cm_topd o where o.status='W' and p.cityID=c.cityID and p.patientID=o.patientid and DATE(o.visitdate)=DATE(Now()) ";
                    break;
                case 10:
                    objdbOPD.Query = "SELECT count(p.cityid) as CountCity, c.name FROM cm_tcity c,cm_tPatientReg p where c.cityid=p.cityid group by p.cityid order by CountCity desc";
                    break;

                case 11://,(year(now())-year( DOB)) as age  
                    objdbOPD.Query = "SELECT `PatientID`, `PRNO`, p.Name, case when Gender ='M' then 'Male'  else   'Female' end as Gender, `DOB`, `PhoneNo`, `CellNo`, `Address`,c.name as cityname,Email,pn.PanelName,cast(case when DATEDIFF(CURRENT_DATE,dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/365),' Years') else case when DATEDIFF(CURRENT_DATE,dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,dob)/30),' Months') else concat(DATEDIFF(CURRENT_DATE,dob),' Days') end end as char) AS age,MaritalStatus,EmployNo  FROM  " + _TableName + " p left outer join  CM_tPanel Pn  on p.panelID=pn.PanelID, cm_tcity c where p.cityID=c.cityID  and p.EnteredOn between '" + Convert.ToDateTime(_ToDate, objCultureInfo).ToString("yyyy/MM/dd") + "' and '" + Convert.ToDateTime(_FromDate, objCultureInfo).ToString("yyyy/MM/dd") + "' ";

                    if (!_PRNO.Equals(Default) && !_PRNO.Equals(mask))
                    {
                        //objdbOPD.Query += " and MRNo = '" + _MRNo + "' ";
                        objdbOPD.Query += " and (MRNo = '" + _MRNo + "' or PRNO = '" + _PRNO + "')";

                    }
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and p.Name like '%" + _Name + "%' ";
                    }
                    if (!_PhoneNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.PhoneNo like '%" + _PhoneNo + "%' ";
                    }
                    if (!_CellNo.Equals(Default))
                    {
                        objdbOPD.Query += " and p.CellNo like '%" + _CellNo + "%' ";
                    }
                    break;
                case 12:
                    objdbOPD.Query = "SELECT c.`CITYID`,c.`NAME` FROM cm_tPatientReg p  ,cm_tcity c  where p.cityid=c.cityid and c.Active='Y' group by cityid order by count(p.cityid)desc";
                    break;

                case 13:
                    objdbOPD.Query = "SELECT OPDID from cm_tOPD where PatientID = " + _PatientID;
                    break;
                case 14://for Pharmacy Patient Info Selection
                    objdbOPD.Query = "SELECT distinct(pr.PatientID),pr.PRNo,o.OPDID ,pr.Name,pr.PhoneNO,pr.CellNo , concat('Dr. ', p.Name) as ConsultantName,concat(case when pr.Gender ='M' then 'Male'  else   'Female' end,' / ',cast( case when DATEDIFF(CURRENT_DATE,pr.dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/365),' Y') else case when DATEDIFF(CURRENT_DATE,pr.dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/30),' M') else concat(DATEDIFF(CURRENT_DATE,pr.dob),' D') end end as char)) AS ageGender,pr.Address FROM cm_tOPd o ,cm_tPatientreg pr ,cm_tPersonal p,cm_tOPDMedicine om where date(o.visitdate ) = date(now()) and  o.PatientID=pr.PatientID and p.personID=o.EnteredBY and o.status='C' and o.OPDID=om.OPDID and om.status is null ";
                    if (!_PRNO.Equals(Default) && !_PRNO.Equals(mask))
                    {
                        objdbOPD.Query += " and Pr.PRNO = '" + _PRNO + "' ";
                    }
                    if (!_Name.Equals(Default))
                    {
                        objdbOPD.Query += " and pr.Name like '%" + _Name + "%' ";
                    }
                    if (!_PhoneNo.Equals(Default))
                    {
                        objdbOPD.Query += " and pr.PhoneNo like '%" + _PhoneNo + "%' ";
                    }
                    if (!_CellNo.Equals(Default))
                    {
                        objdbOPD.Query += " and pr.CellNo like '%" + _CellNo + "%' ";
                    }
                    if (!_EnteredBy.Equals(Default))
                    {
                        objdbOPD.Query += " and o.EnteredBy =" + _EnteredBy;
                    }
                    objdbOPD.Query += " order by o.OPDID desc;";
                    break;
                case 15:
                    objdbOPD.Query = "SELECT o.OPDID, o.Video, pr.prno,pr.name as PName,status,cast( case when DATEDIFF(CURRENT_DATE,pr.dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/365),' Y') else case when DATEDIFF(CURRENT_DATE,pr.dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/30),' M') else concat(DATEDIFF(CURRENT_DATE,pr.dob),' D') end end as char) AS age,p.name as DocName FROM cm_tPersonal p, cm_tPatientReg pr, cm_topd o where p.Personid=o.enteredby and  pr.patientid= o.patientid and DATE(o.visitdate)=DATE(Now()) order by o.enteredby,status desc";
                    break;
                case 16:
                    objdbOPD.Query = "SELECT pr.prno,pr.name as PName,cast( case when DATEDIFF(CURRENT_DATE,pr.dob) > 365 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/365),' Y') else case when DATEDIFF(CURRENT_DATE,pr.dob) > 30 then concat(Floor(DATEDIFF(CURRENT_DATE,pr.dob)/30),' M') else concat(DATEDIFF(CURRENT_DATE,pr.dob),' D') end end as char) AS age,pr.gender,RefPrno FROM cm_tPatientReg pr where pr.patientid=" + _PatientID;
                    break;
                case 17:
                    objdbOPD.Query = "SELECT * FROM cm_tPatientReg pr where pr.SynchedToServer=" + 0;
                    break;
                case 18:
                    objdbOPD.Query = "SELECT * FROM cm_tPatientReg pr where pr.PatientID=" + _PatientID;
                    break;
                case 19:
                    objdbOPD.Query = "SELECT * FROM cm_topd pr where pr.PatientID=" + _PatientID;
                    break;
                case 20:
                    objdbOPD.Query = "SELECT * FROM cm_topd pr where pr.PRNO=" + _PRNO;
                    break;
                case 21:
                    objdbOPD.Query = "SELECT * FROM cm_tPatientReg pr where pr.PatientID=" + _PatientID;
                    break;
            }
            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        //public DataView Ora_GetAll(int flag)
        //{
        //  switch (flag)
        //  {//case when (year(now())-year( DOB)) <> 0 then case when (month(dob) <= month(now())) then concat((year(now())-year( DOB)),' Yrs') else concat(year(now())-year(DOB)-1 , ' Yrs') end else concat(month(now())-month(DOB), ' Mon') end as Age,
        //    case 1://FromDate is used to represent RefPersonid in Cm_tPersonal
        //      objdbOPD.Query = "select pvd.TransNo,pvm.VisitNo,pr.PRNo as PRNo,pr.fname || ' ' || NVL(pr.mname, '') || ' ' || NVL(pr.lname, '') as PatientName,case pr.sex when 'M' then 'Male' else 'Female' end as Gender, to_char( (to_number(to_char(sysdate,'yyyy'))) - to_number(to_char(DOB,'yyyy'))) || ' Yrs'  as Age,DOB as DOB, pr.hphone as PhoneNo, pr.cellphone as CellNo, pr.haddress as Address, pr.email as Email, case pvd.status when 'N' then 'H' when 'Y' then 'C' else 'W' end as Status, ct.name as CityName from pr_tpatientvisitm pvm, pr_tpatientvisitd pvd, Pr_Tpatientreg    pr, hr_tcity ct, hr_tservice sr, hr_tsubdepartment sub where pvm.visitno = pvd.visitno and pvm.prno=pr.prno  and pvd.serviceid = sr.serviceid and pr.cityid=ct.cityid  and sr.subdepartmentid = sub.subdepartmentid and to_char(pvm.visitdatetime, 'dd-mm-yy') = to_char(sysdate, 'dd-mm-yy') and pvd.departmentid = '001' and pvd.paidno is not null and sr. name = 'Consultation' and sub.personid = '" + _FromDate + "'";

        //      //objdbOPD.Query = "select pvd.TransNo,pvd.VisitNo,pr.PRNo as PRNo, pr.fname || ' ' || NVL(pr.mname, '') || ' ' || NVL(pr.lname, '') as PatientName, case pr.sex when 'M' then 'Male' else 'Female' end as Gender, pr.dob as DOB, pr.hphone as PhoneNo, pr.cellphone as CellNo, pr.haddress as Address, ct.name as CityName, pr.email as Email, case pvd.status when 'N' then 'H' when 'Y' then 'C' else 'W' end as Status from Pr_Tpatientreg  pr, pr_tpatientvisitd pvd, hr_tcity ct, hr_tservice sr, hr_tsubdepartment sub where pr.prno = pvd.prno and ct.cityid = pr.cityid and pvd.serviceid = sr.serviceid and pvd.departmentid = '001' and pvd.paidno is not null and sr.subdepartmentid = sub.subdepartmentid and sr. name = 'Consultation' and sub.personid = '" + _FromDate  + "' and pvd.cmid is  null and to_date(pvd.enteredat, 'dd-mm-yy') = to_date('02-07-2007', 'dd-mm-yy')";
        //      break;
        //    case 2:
        //      objdbOPD.Query = "select t.Prno from pr_tpatientreg t where Prno='" + _PRNO + "'";
        //      break;
        //    case 3://FromDate is used to represent RefPersonid in Cm_tPersonal
        //      objdbOPD.Query = "select pvd.TransNo,pvm.VisitNo,pr.PRNo as PRNo,pr.fname || ' ' || NVL(pr.mname, '') || ' ' || NVL(pr.lname, '') as PatientName,case pr.sex when 'M' then 'Male' else 'Female' end as Gender, to_char( (to_number(to_char(sysdate,'yyyy'))) - to_number(to_char(DOB,'yyyy'))) || ' Yrs'  as Age,DOB as DOB, pr.hphone as PhoneNo, pr.cellphone as CellNo, pr.haddress as Address, pr.email as Email, case pvd.status when 'N' then 'H' when 'Y' then 'C' else 'W' end as Status, ct.name as CityName from pr_tpatientvisitm pvm, pr_tpatientvisitd pvd, Pr_Tpatientreg    pr, hr_tcity ct, hr_tservice sr, hr_tsubdepartment sub where pvm.visitno = pvd.visitno and pvm.prno=pr.prno  and pvd.serviceid = sr.serviceid and pr.cityid=ct.cityid  and sr.subdepartmentid = sub.subdepartmentid and to_char(pvm.visitdatetime, 'dd-mm-yy') = to_char(sysdate, 'dd-mm-yy') and pvd.departmentid = '001' and pvd.paidno is not null and sr. name = 'Consultation' and pr.PRNo = '" + _PRNO+ "'";
        //      break;
        //  }
        //  return _objConnection._objOperation.DataTrigger_Ora_Get_All(objdbOPD);
        //}

        private string[,] MakeArray()
        {
            string[,] strarr = new string[19, 3];

            if (!this._PatientID.Equals(Default))
            {
                strarr[0, 0] = "PatientID";
                strarr[0, 1] = _PatientID;
                strarr[0, 2] = "int";
            }

            if (!this._PRNO.Equals(Default))
            {
                strarr[1, 0] = "PRNO";
                strarr[1, 1] = _PRNO;
                strarr[1, 2] = "string";
            }

            if (!this._Name.Equals(Default))
            {
                strarr[2, 0] = "Name";
                strarr[2, 1] = _Name;
                strarr[2, 2] = "string";
            }

            if (!this._Gender.Equals(Default))
            {
                strarr[3, 0] = "Gender";
                strarr[3, 1] = _Gender;
                strarr[3, 2] = "string";
            }

            if (!this._DOB.Equals(Default))
            {
                strarr[4, 0] = "DOB";
                strarr[4, 1] = _DOB;
                strarr[4, 2] = "datetime";
            }

            if (!this._PhoneNo.Equals(Default))
            {
                strarr[5, 0] = "PhoneNo";
                strarr[5, 1] = _PhoneNo;
                strarr[5, 2] = "string";
            }

            if (!this._CellNo.Equals(Default))
            {
                strarr[6, 0] = "CellNo";
                strarr[6, 1] = _CellNo;
                strarr[6, 2] = "string";
            }

            if (!this._Address.Equals(Default))
            {
                strarr[7, 0] = "Address";
                strarr[7, 1] = _Address;
                strarr[7, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[8, 0] = "EnteredBy";
                strarr[8, 1] = _EnteredBy;
                strarr[8, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[9, 0] = "EnteredOn";
                strarr[9, 1] = _EnteredOn;
                strarr[9, 2] = "datetime";
            }

            if (!this._CityID.Equals(Default))
            {
                strarr[10, 0] = "CityID";
                strarr[10, 1] = _CityID;
                strarr[10, 2] = "int";
            }

            if (!this._Email.Equals(Default))
            {
                strarr[11, 0] = "Email";
                strarr[11, 1] = _Email;
                strarr[11, 2] = "string";
            }

            if (!this._PanelID.Equals(Default))
            {
                strarr[12, 0] = "PanelID";
                strarr[12, 1] = _PanelID;
                strarr[12, 2] = "int";
            }
            if (!_MaritalStatus.Equals(Default))
            {
                strarr[13, 0] = "MaritalStatus";
                strarr[13, 1] = _MaritalStatus;
                strarr[13, 2] = "string";
            }

            if (!_EmpNo.Equals(Default))
            {
                strarr[14, 0] = "EmployNo";
                strarr[14, 1] = _EmpNo;
                strarr[14, 2] = "string";
            }
            if (!_RefPRNO.Equals(Default))
            {
                strarr[14, 0] = "RefPRNO";
                strarr[14, 1] = _RefPRNO;
                strarr[14, 2] = "string";
            }
            if (!_UserName.Equals(Default))
            {
                strarr[15, 0] = "UserName";
                strarr[15, 1] = _UserName;
                strarr[15, 2] = "string";
            }
            if (!_Password.Equals(Default))
            {
                strarr[16, 0] = "Password";
                strarr[16, 1] = _Password;
                strarr[16, 2] = "string";
            }
            if (!_SynchedToServer.Equals(Default))
            {
                strarr[17, 0] = "SynchedToServer";
                strarr[17, 1] = _SynchedToServer;
                strarr[17, 2] = "string";
            }
            if (!_MRNo.Equals(Default))
            {
                strarr[18, 0] = "MrNo";
                strarr[18, 1] = _MRNo;
                strarr[18, 2] = "string";
            }

            return strarr;
        }
        private string[,] MakeArrayForUpdate()
        {
            string[,] strarr = new string[16, 3];

            if (!this._PatientID.Equals(Default))
            {
                strarr[0, 0] = "PatientID";
                strarr[0, 1] = _PatientID;
                strarr[0, 2] = "int";
            }

            if (!this._PRNO.Equals(Default))
            {
                strarr[1, 0] = "PRNO";
                strarr[1, 1] = _PRNO;
                strarr[1, 2] = "string";
            }

            //if (!this._Name.Equals(Default))
            //{
            //    strarr[2, 0] = "Name";
            //    strarr[2, 1] = _Name;
            //    strarr[2, 2] = "string";
            //}

            if (!this._Gender.Equals(Default))
            {
                strarr[3, 0] = "Gender";
                strarr[3, 1] = _Gender;
                strarr[3, 2] = "string";
            }

            if (!this._DOB.Equals(Default))
            {
                strarr[4, 0] = "DOB";
                strarr[4, 1] = _DOB;
                strarr[4, 2] = "datetime";
            }

            if (!this._PhoneNo.Equals(Default))
            {
                strarr[5, 0] = "PhoneNo";
                strarr[5, 1] = _PhoneNo;
                strarr[5, 2] = "string";
            }

            if (!this._CellNo.Equals(Default))
            {
                strarr[6, 0] = "CellNo";
                strarr[6, 1] = _CellNo;
                strarr[6, 2] = "string";
            }

            if (!this._Address.Equals(Default))
            {
                strarr[7, 0] = "Address";
                strarr[7, 1] = _Address;
                strarr[7, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[8, 0] = "EnteredBy";
                strarr[8, 1] = _EnteredBy;
                strarr[8, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[9, 0] = "EnteredOn";
                strarr[9, 1] = _EnteredOn;
                strarr[9, 2] = "datetime";
            }

            if (!this._CityID.Equals(Default))
            {
                strarr[10, 0] = "CityID";
                strarr[10, 1] = _CityID;
                strarr[10, 2] = "int";
            }

            if (!this._Email.Equals(Default))
            {
                strarr[11, 0] = "Email";
                strarr[11, 1] = _Email;
                strarr[11, 2] = "string";
            }

            if (!this._PanelID.Equals(Default))
            {
                strarr[12, 0] = "PanelID";
                strarr[12, 1] = _PanelID;
                strarr[12, 2] = "int";
            }
            if (!_MaritalStatus.Equals(Default))
            {
                strarr[13, 0] = "MaritalStatus";
                strarr[13, 1] = _MaritalStatus;
                strarr[13, 2] = "string";
            }

            if (!_EmpNo.Equals(Default))
            {
                strarr[14, 0] = "EmployNo";
                strarr[14, 1] = _EmpNo;
                strarr[14, 2] = "string";
            }
            if (!_RefPRNO.Equals(Default))
            {
                strarr[14, 0] = "RefPRNO";
                strarr[14, 1] = _RefPRNO;
                strarr[14, 2] = "string";
            }
            if (!_SynchedToServer.Equals(Default))
            {
                strarr[15, 0] = "SynchedToServer";
                strarr[15, 1] = "0";
                strarr[15, 2] = "string";
            }
          


            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateName())
            {
                return false;
            }
            else if (!ValidatePRNo())
            {
                return false;
            }
            else if (!ValidateDOB())
            {
                return false;
            }
        
            else if (!ValidatCellNo())
            {
                return false;
            }
            return true;
        }

        private bool ValidateName()
        {
            Validation objValid = new Validation();

            if (!_Name.ToString().Equals(Default))
            {
                if (_Name.ToString().Equals(""))
                {
                    StrErrorMessage = "Name is not allowed to be Empty";
                    objValid = null;
                    return false;
                }

                if (!objValid.IsName(_Name))
                {
                    StrErrorMessage = "Invalid Character in Name field";
                    objValid = null;
                    return false;
                }

            }
            objValid = null;
            return true;
        }

        private bool ValidatePRNo()
        {
            if (_PRNO.Trim().Equals("-  -") || _PRNO.Trim().Equals(""))
            {
                StrErrorMessage = "PRNo is not allowed to be Empty";
                return false;
            }
            return true;
        }

        private bool ValidateDOB()
        {
           
            if (!this._DOB.Equals(Default))
            {
               
                //if (DateTime.Compare(Convert.ToDateTime(_TempDOB), DateTime.Now.Date) >= 0)
                //{
                //    this.StrErrorMessage = "Date of Birth is not more then current date";
                //    return false;
                //}
            }
            return true;
        }

        private bool ValidatePhoneNo()
        {
            if (!this._PhoneNo.Equals(Default))
            {
                Validation objValid = new Validation();

                if (!objValid.IsNumber(this._PhoneNo) && !this._PhoneNo.Equals(""))
                {
                    this.StrErrorMessage = "Phone Number has some invalid characters..";
                    return false;
                }
                objValid = null;
            }
            return true;
        }

        private bool ValidatCellNo()
        {
            //if (!this._CellNo.Equals(Default))
            //{
            //  Validation objValid = new Validation();

            //  if (!objValid.IsNumber(this._CellNo) && !this._CellNo.Equals(""))
            //  {
            //    this.StrErrorMessage = "Cell number has some invalid characters.";
            //    return false;
            //  }

            //  objValid = null;
            //}
            return true;
        }

        #endregion
    }
}
