using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using System.Data;

namespace OPD_BL
{
    public class clsBLOPDMedAlerAdver
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLOPDMedAlerAdver()
        {
        }

        public clsBLOPDMedAlerAdver(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_topdmedaleradver";
        private string StrErrorMessage = "";

        private string _ID = Default;
        private string _AlergAdversID = Default;
        private string _PatientID= Default;
        private string _MedID= Default;
        private string _Description = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _Active= Default;

        private string _Type= Default;
        
        #endregion

        #region "Properties"

        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string AlergAdversID
        {
            get { return _AlergAdversID; }
            set { _AlergAdversID = value; }
        }

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }

        public string MedID
        {
            get { return _MedID; }
            set { _MedID = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT * FROM cm_topdmedaleradver";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT count(ID) as Calculate FROM cm_topdmedaleradver o,cm_tallergadvers a where o.AlergAdversID=a.AlergAdversID and a.type='" + _Type + "' and o.patientid=" + _PatientID;
                    break;
                case 3:
                    objdbOPD.Query = "SELECT o.ID,aa.AlergAdversID,aa.Name as AlergARName, m.Medicineid , m.Name as MedName,cast(Date_format(o.enteredon,'%b %d, %Y') as char) as Enteredon,'I' as Mode  FROM (cm_topdmedaleradver o, cm_tallergadvers aa) left outer join cm_tmedicine m on o.medid=m.Medicineid where o.AlergAdversID=aa.AlergAdversID and aa.type='" + _Type + "' and o.patientid=" + _PatientID;
                    break;
                case 4:
                    objdbOPD.Query = "SELECT cast(Date_format(o.enteredon,'%b %d, %Y') as char) as Enteredon,a.Name as AllergARName  FROM cm_topdmedaleradver o,cm_tallergadvers a where o.AlergAdversID=a.AlergAdversID and a.type='" + _Type + "' and o.patientid=" + _PatientID;
                    break;
                case 5:
                    objdbOPD.Query = "SELECT ID FROM cm_topdmedaleradver where Medid=" + _MedID + " and PatientID=" + _PatientID;
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[8, 3];

            if (!this._ID.Equals(Default))
            {
                strarr[0, 0] = "ID";
                strarr[0, 1] = _ID;
                strarr[0, 2] = "int";
            }

            if (!this._AlergAdversID.Equals(Default))
            {
                strarr[1, 0] = "AlergAdversID";
                strarr[1, 1] = _AlergAdversID;
                strarr[1, 2] = "int";
            }

            if (!this._PatientID.Equals(Default))
            {
                strarr[2, 0] = "PatientID";
                strarr[2, 1] = _PatientID;
                strarr[2, 2] = "int";
            }

            if (!this._MedID.Equals(Default))
            {
                strarr[3, 0] = "MedID";
                strarr[3, 1] = _MedID;
                strarr[3, 2] = "int";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[4, 0] = "Description";
                strarr[4, 1] = _Description;
                strarr[4, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[5, 0] = "EnteredBy";
                strarr[5, 1] = _EnteredBy;
                strarr[5, 2] = "int";
            }
            if (!this._EnteredOn.Equals(Default))
            {
                strarr[6, 0] = "EnteredON";
                strarr[6, 1] = _EnteredOn;
                strarr[6, 2] = "datetime";
            }
            if (!this._Active.Equals(Default))
            {
                strarr[7, 0] = "Active";
                strarr[7, 1] = _Active;
                strarr[7, 2] = "string";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion
    }
}
