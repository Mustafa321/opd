using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using System.Data;

namespace OPD_BL
{
    public class clsBLVoiceNotes
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLVoiceNotes()
        {
        }

        public clsBLVoiceNotes(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tVoiceNotes";
        private string StrErrorMessage = "";

        private string _VoiceNotesID = Default;
        private string _PatientID= Default;
        private string _Description = Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _Path = Default;

        #endregion

        #region "Properties"

        public string VoiceNotesID
        {
            get { return _VoiceNotesID; }
            set { _VoiceNotesID = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string Path
        {
            get { return _Path; }
            set { _Path= value; }
        }

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID= value; }
        }
        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT VoiceNotesID,Path,Description,cast(date_format(date(EnteredOn),'%b %d, %Y') as char) as EnteredOn FROM cm_tvoiceNotes where PatientID=" + _PatientID;
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[6, 3];

            if (!this._VoiceNotesID.Equals(Default))
            {
                strarr[0, 0] = "VoiceNotesID";
                strarr[0, 1] = _VoiceNotesID;
                strarr[0, 2] = "int";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[1, 0] = "Description";
                strarr[1, 1] = _Description;
                strarr[1, 2] = "string";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[2, 0] = "EnteredBy";
                strarr[2, 1] = _EnteredBy;
                strarr[2, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[3, 0] = "EnteredOn";
                strarr[3, 1] = _EnteredOn;
                strarr[3, 2] = "datetime";
            }

            if (!this._Path.Equals(Default))
            {
                strarr[4, 0] = "Path";
                strarr[4, 1] = _Path;
                strarr[4, 2] = "string";
            }
            if (!this._PatientID.Equals(Default))
            {
                strarr[5, 0] = "PatientID";
                strarr[5, 1] = _PatientID;
                strarr[5, 2] = "int";
            }
            
            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion

    }
}
