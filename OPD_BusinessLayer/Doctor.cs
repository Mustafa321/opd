﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD_BL
{
   public class Doctor
    {
        public decimal Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string DoctorName { get; set; }
        public string Email { get; set; }
        public Nullable< decimal> RevisitDays { get; set; }
        public string RevisitCharges { get; set; }
        public decimal Charges { get; set; }
        public string CNIC { get; set; }
        public string Designation { get; set; }
        public string WhatsappNumber { get; set; }
        public string Address { get; set; }
        public DateTime DOB { get; set; }
        public string BloodGroup { get; set; }
        public string MartialStatus { get; set; }
        public string Gender { get; set; }
        public bool Active { get; set; }
        public Nullable< DateTime> LicenceExpireDate { get; set; }

    }
}
