using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLPatientPicture
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLPatientPicture()
        {
        }

        public clsBLPatientPicture(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tPatientPicture";
        private string StrErrorMessage = "";

        private string _PictureID = Default;
        private string _PatientID = Default;
        private string _SmallPicture = Default;
        private string _Comment = Default;
        private string _DocTypeID = Default;
        private string _SavedMethod = Default;
        private string _EnteredOn= Default;

        #endregion

        #region "Properties"

        public string PictureID
        {
            get { return _PictureID; }
            set { _PictureID = value; }
        }

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }

        public string SmallPicture
        {
            get { return _SmallPicture; }
            set { _SmallPicture = value; }
        }

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public string DocTypeID
        {
            get { return _DocTypeID; }
            set { _DocTypeID = value; }
        }

        public string SavedMethod
        {
            get { return _SavedMethod; }
            set { _SavedMethod = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);
                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool UpdatePatientPuicture()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = "update " + _TableName + " set SmallPicture= '" + _SmallPicture + "'where comment='Pic :PatientPicture' and  PatientID =" + _PatientID;
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    //objdbOPD.Query = "SELECT `PatientID` , CAST(UNCOMPRESS(SmallPicture) as char) as SmallPicture FROM  " + _TableName;
                    objdbOPD.Query = "SELECT cast(date_format(pp.EnteredOn,'%b %d, %Y %h:%i %p')as char) as Enteredon, PictureID, `PatientID` , CAST(SmallPicture as char) as SmallPicture,Comment,pp.Doctypeid,dt.name as DocName FROM  " + _TableName + " pp left outer join cm_tdoctype dt on pp.Doctypeid=dt.doctypeid  where pp.SavedMethod='PS' and  PatientID = " + _PatientID;
                    if (!_DocTypeID.Equals(Default))
                    {
                        objdbOPD.Query += " and dt.DoctypeId=" + _DocTypeID;
                    }
                    break;

                case 2://Get selected patient Videos
                    objdbOPD.Query = "SELECT cast(date_format(pp.EnteredOn,'%b %d, %Y %h:%i %p')as char) as Enteredon,PictureID,PatientID, CAST(SmallPicture as char) as SmallPicture,Comment, pp.DocTypeID, SavedMethod ,dt.type,dt.Name as DocTypeName FROM  cm_tPatientPicture pp left outer join cm_tdoctype dt on pp.DocTypeID=dt.DocTypeID where (pp.SavedMethod='VS' or pp.SavedMethod='VP') and  PatientID =" + _PatientID;
                    if (!_DocTypeID.Equals(Default))
                    {
                        objdbOPD.Query += " and dt.DoctypeId=" + _DocTypeID;
                    }
                    break;
                case 3:
                    objdbOPD.Query = "SELECT CAST(SmallPicture as char) as SmallPicture FROM " + _TableName + " where substring(comment,2,8)='video :' and  PatientID =" + _PatientID + " and CAST(SmallPicture as char) = '" + _SmallPicture + "'";
                    break;
                case 4:
                    //objdbOPD.Query = "SELECT PictureID, `PatientID` , CAST(SmallPicture as char) as SmallPicture,Comment FROM  " + _TableName + " where substring(comment,2,5)='Doc :' and  PatientID =" + _PatientID;
                    objdbOPD.Query = "SELECT cast(date_format(pp.EnteredOn,'%b %d, %Y %h:%i %p') as char) as Enteredon,PictureID,PatientID, CAST(SmallPicture as char) as SmallPicture,Comment, pp.DocTypeID, SavedMethod ,dt.type,dt.Name as DocTypeName FROM  " + _TableName + " pp left outer join cm_tdoctype dt on pp.DocTypeID=dt.DocTypeID where (pp.SavedMethod='DS' or pp.SavedMethod='DP') and  PatientID =" + _PatientID;
                    if (!_DocTypeID.Equals(Default))
                    {
                        objdbOPD.Query += " and dt.DoctypeId=" + _DocTypeID;
                    }
                    break;
                case 5:
                    objdbOPD.Query = "SELECT PictureID,CAST(SmallPicture as char) as SmallPicture FROM  " + _TableName + " where savedMethod ='PP' and  PatientID =" + _PatientID;//PP is PatientPicture
                    break;
                case 6:
                    objdbOPD.Query = "SELECT PatientID , cast(sum(if(savedMethod='VP' or savedMethod='VS' ,1,'0'))as chaR) as VideoCount, cast(sum(if(savedMethod='DP' or savedMethod='DS' ,1,'0'))as char) as DocCount, cast(sum(if(savedMethod='PS' ,1,'0')) as char) as PicCount from cm_tpatientpicture where PatientID = " + _PatientID + " group by PatientID";
                    //objdbOPD.Query = "SELECT distinct PatientID , (SELECT count(pictureID)  as Picture  from cm_tpatientpicture where substring(comment,2,7)='Video : ' and PatientID = " + _PatientID + " ) as VideoCount, (SELECT count(pictureID) as Picture  from cm_tpatientpicture where substring(comment,1,5) ='Pic :' and PatientID = " + _PatientID + " ) as PicCount, (SELECT count(pictureID) as Picture  from cm_tpatientpicture where substring(comment,2,5)='Doc :' and PatientID = " + _PatientID + " ) as DocCount from cm_tpatientpicture where PatientID = " + _PatientID;
                    break;
            }
            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        public void Delete()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where pictureid =  " + _PictureID; _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[7, 3];

            if (!this._PictureID.Equals(Default))
            {
                strarr[0, 0] = "PictureID";
                strarr[0, 1] = _PictureID;
                strarr[0, 2] = "int";
            }

            if (!this._PatientID.Equals(Default))
            {
                strarr[1, 0] = "PatientID";
                strarr[1, 1] = _PatientID;
                strarr[1, 2] = "int";
            }

            if (!this._SmallPicture.ToString().Equals(Default))
            {
                strarr[2, 0] = "SmallPicture";
                strarr[2, 1] = _SmallPicture;
                strarr[2, 2] = "string";
            }
            if (!this._Comment.Equals(Default))
            {
                strarr[3, 0] = "Comment";
                strarr[3, 1] = _Comment;
                strarr[3, 2] = "string";
            }
            if (!this._DocTypeID.ToString().Equals(Default))
            {
                strarr[4, 0] = "DocTypeID";
                strarr[4, 1] = _DocTypeID;
                strarr[4, 2] = "int";
            }
            if (!this._SavedMethod.Equals(Default))
            {
                strarr[5, 0] = "SavedMethod";
                strarr[5, 1] = _SavedMethod;
                strarr[5, 2] = "string";
            }
            if (!this._EnteredOn.Equals(Default))
            {
                strarr[6, 0] = "EnteredOn";
                strarr[6, 1] = _EnteredOn;
                strarr[6, 2] = "datetime";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateVideoName())
            {
                return false;
            }
            return true;
        }

        private bool ValidateVideoName()
        {
            if (!_SmallPicture.ToString().Equals(Default))
            {
                //if (GetAll(3).Table.Rows.Count>=1)
                //{
                //    StrErrorMessage = "Already Added "; 
                //    return false;
                //}

            }
            //objValid = null;
            return true;
        }

        #endregion
    }
}
