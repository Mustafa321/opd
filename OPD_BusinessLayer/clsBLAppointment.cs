using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLAppointment
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLAppointment()
        {
        }

        public clsBLAppointment(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tAppointment";
        private string StrErrorMessage = "";

        private string _AppointmentID = Default;
        private string _PRNO= Default;
        private string _PersonID= Default;
        private string _AppointmentDate = Default;
        private string _TokkenNo= Default;
        private string _MaxSlot= Default;
        
        #endregion

        #region "Properties"

        public string AppointmentID
        {
            get { return _AppointmentID; }
            set { _AppointmentID= value; }
        }

        public string PRNO
        {
            get { return _PRNO; }
            set { _PRNO = value; }
        }

        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID= value; }
        }

        public string AppointmentDate
        {
            get { return _AppointmentDate  ; }
            set { _AppointmentDate= value; }
        }

        public string TokkenNo
        {
            get { return _TokkenNo; }
            set { _TokkenNo= value; }
        }

        public string MaxSlot
        {
            get { return _MaxSlot; }
            set { _MaxSlot= value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public void Delete()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where AppointmentId =" + _AppointmentID;
            StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        public void DeleteAll()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where PersonID=" + _PersonID + " and AppointmentDate='" + _AppointmentDate + "'";
            StrErrorMessage = _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT `AppointmentID`, `PRNO`, `PersonID`, `AppointmentDate`, `TokkenNo` FROM  " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT count(AppointmentID) as FillSlot FROM  " + _TableName + " c, Cm_topd o where AppointmentDate= '" + _AppointmentDate + "' and o.prno=c.prno and o.status='W' and o.visitdate='" + _AppointmentDate + "' and personid = " + _PersonID;
                    break;
                case 3:
                    objdbOPD.Query = "SELECT tokkenno+1 as tokkenno FROM " + _TableName + " where  AppointmentDate= '" + _AppointmentDate + "' and personid = " + _PersonID +  "  order by tokkenno desc";
                    break;
                case 4:
                    objdbOPD.Query = "SELECT o.opdid, pr.PatientID,pa.Prno, pr.name,pa.Appointmentid,pa.AppointmentDate,pa.personid,pa.tokkenno,'Vital Sign' as vitalsign  FROM " + _TableName + " pa,cm_tpatientreg pr ,cm_topd o where  o.patientid=pr.patientid and pr.prno=pa.prno and AppointmentDate= '" + _AppointmentDate + "' and o.visitdate='" + _AppointmentDate + "' and personid = " + _PersonID + "  and o.status = 'W' order by tokkenno Asc";
                    break;
                case 5:
                  objdbOPD.Query = "SELECT AppointmentID FROM " + _TableName + " where  AppointmentDate= '" + _AppointmentDate + "' and personid = " + _PersonID + "  and Prno = '" + _PRNO +"'" ;
                    break;
                  case 6:
                    objdbOPD.Query = "SELECT  pr.prno,pr.name FROM cm_tpatientreg pr ,cm_topd o where o.patientid= pr.patientid and o.visitdate='" + _AppointmentDate + "' and o.status = 'W'";
                    break;
                case 7:
                    //objdbOPD.Query = "SELECT date_format(c.AppointmentDate,'%m/%d/%Y') as AppointmentDate,count(Appointmentid) as  case 6:
                    //objdbOPD.Query = "SELECT date_format(c.AppointmentDate,'%m/%d/%Y') as AppointmentDate,count(Appointmentid) as Count FROM cm_tappointment c, cm_Topd o where date_format(c.AppointmentDate,'%m/%Y')='" + AppointmentDate + "' and personid = " + _PersonID + " and o.visitdate=c.appointmentdate and c.appointmentdate >= current_Date and  o.visitdate >= current_Date  and  c.prno=o.prno and o.status='W' group by c.AppointmentDate";

                    //   objdbOPD.Query = "SELECT cast(date_format(c.AppointmentDate,'%m/%d/%Y') as char)as AppointmentDate,count(Appointmentid) as Count FROM cm_tappointment c where date_format(c.AppointmentDate,'%m/%Y')='" + AppointmentDate + "' and personid = " + _PersonID + " and c.appointmentdate >= current_Date and c.prno in (select o.Prno from cm_tOPD o where date_format(o.visitdate,'%m/%Y')= '" + _AppointmentDate + "' and o.enteredBy = " + _PersonID + ") group by c.AppointmentDate";

                    objdbOPD.Query = "SELECT cast(date_format(c.AppointmentDate,'%m/%d/%Y') as char)as AppointmentDate,count(Appointmentid) as Count FROM cm_tappointment c where date_format(c.AppointmentDate,'%m/%Y')='" + AppointmentDate + "' and personid = " + _PersonID + " and c.appointmentdate <= current_Date and c.prno in (select o.Prno from cm_tOPD o where date_format(o.visitdate,'%m/%Y')= '" + _AppointmentDate + "' and o.enteredBy = " + _PersonID + ") group by c.AppointmentDate";


                    break;
                case 8://Email for Tommorow reminder
                    objdbOPD.Query = "SELECT pr.Email,pr.patientid,pr.Prno FROM cm_tappointment c ,cm_tpatientReg pr where c.PRNo=pr.PRNo and Personid=" + _PersonID + " and Appointmentdate='" + _AppointmentDate + "';";
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[5, 3];

            if (!this._AppointmentID.Equals(Default))
            {
                strarr[0, 0] = "AppointmentID";
                strarr[0, 1] = _AppointmentID;
                strarr[0, 2] = "int";
            }

            if (!this._PRNO.Equals(Default))
            {
                strarr[1, 0] = "PRNO";
                strarr[1, 1] = _PRNO;
                strarr[1, 2] = "string";
            }
            
            if (!this._AppointmentDate.Equals(Default))
            {
                strarr[2, 0] = "AppointmentDate";
                strarr[2, 1] = _AppointmentDate ;
                strarr[2, 2] = "datetime";
            }

            if (!this._PersonID.Equals(Default))
            {
                strarr[3, 0] = "PersonID";
                strarr[3, 1] = _PersonID;
                strarr[3, 2] = "int";
            }

            if (!this.TokkenNo.Equals(Default))
            {
                strarr[4, 0] = "TokkenNo";
                strarr[4, 1] = _TokkenNo;
                strarr[4, 2] = "int";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            if (!ValidateMaxAppointment())
            {
                return false;
            }
           
            return true;
        }

        private bool ValidateMaxAppointment()
        {
            if (!_TokkenNo.ToString().Equals(Default))
            {
                if ( Convert.ToInt16( _TokkenNo) > Convert.ToInt16(_MaxSlot) )
                {
                    StrErrorMessage = "You Exceed Maximum Patient/Day";
                    return false;
                }
            }
            
            return true;
        }

        #endregion

    }
}

