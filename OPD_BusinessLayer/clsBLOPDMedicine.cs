using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;
namespace OPD_BL
{
    public class clsBLOPDMedicine
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLOPDMedicine()
        { 
        }

        public clsBLOPDMedicine(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tOPDMedicine";
        private string StrErrorMessage = "";

        private string _OPDID = Default;
        private string _MedID= Default;
        private string _MedName = Default;
        private string _MedDosage = Default;
        
        #endregion

        #region "Properties"

        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID= value; }
        }

        public string MedID
        {
            get { return _MedID; }
            set { _MedID = value; }
        }

        public string MedName
        {
            get { return _MedName; }
            set { _MedName= value; }
        }

        public string MedDosage
        {
            get { return _MedDosage; }
            set { _MedDosage= value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT c.`OPDID`, c.`MedicineID`, c.`MedicineName`, c.`MedicineDosage` FROM " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT c.`OPDID`, c.`MedicineID`, c.`MedicineName`, c.`MedicineDosage` FROM " + _TableName + " c where OPDID = " + _OPDID;
                    break;
                  case 3:
                      objdbOPD.Query = "SELECT o.visitdate,om.OPDID, om.MedicineID, om.MedicineName, om.medicineDosage FROM cm_topdmedicine om ,cm_topd o where o.opdid=om.opdid and o.patientid = " + _MedID + " and o.opdid <> " + _OPDID + " order by o.visitdate desc;";
                    break;
                case 4:
                    objdbOPD.Query = "SELECT om.OPDID,om.MedicineID,om.MedicineName,om.MedicineDosage,'1' as Deliver ,'1' as Avail FROM cm_topdmedicine om  where om.OPDID=" + _OPDID + " and om.Status is null";
                    break;
                case 5:
                    objdbOPD.Query = "SELECT _pMed.MedicineName,_pMed.MedicineDosage as Dosage ,_m.QuantityForMorning,_m.QuantityForAfterNoon,_m.QuantityForEvening,_m.QuantityNight,_m.Name,_m.UrduDosage,_m.FarsiDosage,_m.Morning,_m.Night,_m.Evening,_m.Afternoon,_m.DaysCount as Days,_m.Instruction FROM cm_topdmedicine as _pMed LEFT JOIN cm_tmedicine _m on _m.Medicineid= _pMed.MedicineID where _pMed.OPDID='" + _OPDID+"'";
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        public void Delete()
        {
            objdbOPD.Query = "Delete from " + _TableName + " where OPDID = " + _OPDID ;
            StrErrorMessage= _objConnection._objOperation.DataTrigger_Delete(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[4, 3];

            if (!this._OPDID.Equals(Default))
            {
                strarr[0, 0] = "OPDID";
                strarr[0, 1] = _OPDID;
                strarr[0, 2] = "int";
            }

            if (!this._MedID.Equals(Default))
            {
                strarr[1, 0] = "MedicineID";
                strarr[1, 1] = _MedID;
                strarr[1, 2] = "int";
            }

            if (!this._MedName.Equals(Default))
            {
                strarr[2, 0] = "MedicineName";
                strarr[2, 1] = _MedName;
                strarr[2, 2] = "string";
            }

            if (!this._MedDosage.Equals(Default))
            {
                strarr[3, 0] = "MedicineDosage";
                strarr[3, 1] = _MedDosage;
                strarr[3, 2] = "string";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion
    }

}
