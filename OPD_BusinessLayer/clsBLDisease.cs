using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataLayer;

namespace OPD_BL
{
    public class clsBLDisease
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLDisease()
        {
        }

        public clsBLDisease(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tDisease";
        private string StrErrorMessage = "";

        private string _OPDID = Default;
        private string _DiseaseID= Default;
        private string _EnteredBy = Default;
        private string _EnteredOn = Default;
        private string _DiseaseName = Default;

        #endregion

        #region "Properties"

        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID= value; }
        }

        public string DiseaseID
        {
            get { return _DiseaseID; }
            set { _DiseaseID = value; }
        }

        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        public string EnteredOn
        {
            get { return _EnteredOn; }
            set { _EnteredOn = value; }
        }

        public string DiseaseName
        {
            get { return _DiseaseName; }
            set { _DiseaseName = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT c.`OPDID`, c.`DiseaseID`, c.`EnteredBy`, c.`EnteredOn` FROM " + _TableName + " c";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT DISEASEID,DISEASECODE, DISEASENAME,'0' as History,'0' as Complaints,'0' as Signs,'0' as Diagnosis FROM icd_tdisease where ACTIVE='Y'";

                    if (!DiseaseName.Equals(Default) && !DiseaseName.Equals(""))
                    {
                        objdbOPD.Query += " and diseasename like '%" + _DiseaseName + "%'";
                    }
                    if (!EnteredBy.Equals(Default))
                    {
                        objdbOPD.Query += " and BLOCKID = " + _EnteredBy;
                    }
                    if (!EnteredOn.Equals(Default))
                    {
                        objdbOPD.Query += " and CHAPTERID = " + _EnteredOn;
                    }

                    objdbOPD.Query += " order by DISEASENAME";
                    break;

                case 3:
                    //objdbOPD.Query = "SELECT PrefID, p.Name as prefName,(SELECT Case when count(*)=0 then '0' else '1' end  FROM cm_tpreference ph where ph.name=p.name and typeid=1) as History, (SELECT Case when count(*)=0 then '0' else '1' end  FROM cm_tpreference ph where ph.name=p.name and typeid=2) as Diagnosis, (SELECT Case when count(*)=0 then '0' else '1' end  FROM cm_tpreference ph where ph.name=p.name and typeid=3) as Complaints, (SELECT Case when count(*)=0 then '0' else '1' end  FROM cm_tpreference ph where ph.name=p.name and typeid=4) as Signs,DISEASEIDRef ,  '0' as Mode FROM cm_tpreference p where p.TypeID<> 5 and p.TypeID<>6 group by p.name order by p.name";
                    objdbOPD.Query = "SELECT d.diseasecode,PrefID, p.Name as prefName,t.name as Typename,diseaseIDRef , '0' as Mode FROM cm_tpreference p left outer join icd_tdisease d on p.diseaseIDRef=d.DiseaseID, cm_ttype t where t.typeid=p.typeid and p.TypeID<> 5 and p.TypeID<>6 ";

                    if (!_DiseaseName.Equals(Default))
                    {
                        objdbOPD.Query += " and p.name like '%" + _DiseaseName + "%' ";
                    }
                    if (!_DiseaseID.Equals(Default))
                    {
                        objdbOPD.Query += " and p.diseaseIDRef= " + _DiseaseID;//group by p.name 
                    }
                    objdbOPD.Query += " order by p.name";//group by p.name 
                    break;
                case 4:
                    objdbOPD.Query = "SELECT CHAPBLOCKID,CHAPBLOCKTITLE FROM icd_tchapblock where Active ='Y' and Type ='" + _EnteredBy + "' ";
                    if (!_OPDID.Equals(Default))
                    {
                        objdbOPD.Query += " and RefChapter = " + _OPDID;
                    }
                    objdbOPD.Query += " order By CHAPBLOCKTITLE";
                    break;
                case 5:
                    objdbOPD.Query = "SELECT concat(DISEASECODE,' : ', DISEASENAME) as DISEASENAME FROM icd_tdisease where DiseaseID=" + _DiseaseID;
                    break;
                case 6:
                    objdbOPD.Query = "select 0 as SNO,d.DISEASECODE, d.DISEASENAME,g.gcname, i.name as Med_name, n.Name as NatureName from ((((((icd_tdisease d left outer join ph_tgeneric_indication gi on d.diseaseID=gi.diseaseID) left outer join ph_tgeneric g on  gi.genericID=g.genericid ) left outer join ph_tmed_generic mg on mg.genericid=g.genericid) left outer join  ph_tmed_master mm   on mg.medicineid=mm.medicineid) left outer join   ph_tnature n on mm.natureid=n.natureid) left outer join pe_titem i on  i.itemno=mm.itemno) where mm.medicineid is not null and  d.Diseaseid=" + _DiseaseID;
                    break;
            }

            return _objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[4, 3];

            if (!this._OPDID.Equals(Default))
            {
                strarr[0, 0] = "OPDID";
                strarr[0, 1] = _OPDID;
                strarr[0, 2] = "int";
            }

            if (!this._DiseaseID.Equals(Default))
            {
                strarr[1, 0] = "DiseaseID";
                strarr[1, 1] = _DiseaseID;
                strarr[1, 2] = "int";
            }

            if (!this._EnteredBy.Equals(Default))
            {
                strarr[2, 0] = "EnteredBy";
                strarr[2, 1] = _EnteredBy;
                strarr[2, 2] = "int";
            }

            if (!this._EnteredOn.Equals(Default))
            {
                strarr[3, 0] = "EnteredOn";
                strarr[3, 1] = _EnteredOn;
                strarr[3, 2] = "datetime";
            }

            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
            return true;
        }

        #endregion
    }
}
