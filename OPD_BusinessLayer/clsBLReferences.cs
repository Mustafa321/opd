using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DataLayer;
using System.Globalization;

namespace OPD_BL
{
    public class clsBLReferences
    {
        clsBLDBConnection _objConnection;
        clsdbOPD objdbOPD = new clsdbOPD();
        QueryBuilder objQB = new QueryBuilder();

        public clsBLReferences()
        {
        }

        public clsBLReferences(clsBLDBConnection objConnection)
        {
            _objConnection = objConnection;
        }

        # region "Class variables"

        private const string Default = "~!@";
        private const string _TableName = "cm_tReference";
        private string StrErrorMessage = "";
    
        private string _ReportRefrence = Default;
        private string _Description= Default;
        private string _ReportTitle1 = Default;
        private string _ReportTitle2 = Default;
        private string _ReportTitle3= Default;
        private string _TreesFooter1= Default;
        private string _TreesFooter2= Default;
        private string _ClientWebAddress= Default;
        
        #endregion

        #region "Properties"

        public string ReportRefrence
        {
            get { return _ReportRefrence; }
            set { _ReportRefrence = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string ReportTitle1
        {
            get { return _ReportTitle1; }
            set { _ReportTitle1 = value; }
        }

        public string ReportTitle2
        {
            get { return _ReportTitle2; }
            set { _ReportTitle2 = value; }
        }

        public string ReportTitle3
        {
            get { return _ReportTitle3; }
            set { _ReportTitle3 = value; }
        }

        public string TreesFooter1
        {
            get { return _TreesFooter1; }
            set { _TreesFooter1 = value; }
        }

        public string TreesFooter2
        {
            get { return _TreesFooter2; }
            set { _TreesFooter2 = value; }
        }

        public string ClientWebAddress
        {
            get { return _ClientWebAddress; }
            set { _ClientWebAddress = value; }
        }

        public string ErrorMessage
        {
            get { return StrErrorMessage; }
        }

        #endregion

        #region "Method"

        public bool Insert()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBInsert(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Insert(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (ValidateData())
            {
                try
                {
                    objdbOPD.Query = objQB.QBUpdate(MakeArray(), _TableName);
                    StrErrorMessage = _objConnection._objOperation.DataTrigger_Update(objdbOPD);

                    if (StrErrorMessage.Equals("Error"))
                    {
                        StrErrorMessage = _objConnection._objOperation.OperationError;
                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    StrErrorMessage = e.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public DataView GetAll(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbOPD.Query = "SELECT ReportRefrence, Description, ReportTitle1, ReportTitle2, ReportTitle3, TreesFooter1, TreesFooter2, ClientWebAddress FROM "+  _TableName + "  where ReportRefrence = '" + ReportRefrence + "'";
                    break;
                case 2:
                    objdbOPD.Query = "SELECT * FROM cm_tReference where ReportRefrence='" + _ReportRefrence + "'";
                    break;
                case 3:
                    objdbOPD.Query = "SELECT * FROM cm_tReference";
                    break;
            }
           var t=_objConnection._objOperation.DataTrigger_Get_All(objdbOPD);
            return t;
        }

        private string[,] MakeArray()
        {
            string[,] strarr = new string[8, 3];

            if (!this._ReportRefrence.Equals(Default))
            {
                strarr[0, 0] = "ReportRefrence";
                strarr[0, 1] = _ReportRefrence;
                strarr[0, 2] = "string";
            }

            if (!this._Description.Equals(Default))
            {
                strarr[1, 0] = "Description";
                strarr[1, 1] = _Description;
                strarr[1, 2] = "string";
            }

            if (!this._ReportTitle1.Equals(Default))
            {
                strarr[2, 0] = "ReportTitle1";
                strarr[2, 1] = _ReportTitle1;
                strarr[2, 2] = "string";
            }

            if (!this._ReportTitle2.Equals(Default))
            {
                strarr[3, 0] = "ReportTitle2";
                strarr[3, 1] = _ReportTitle2;
                strarr[3, 2] = "string";
            }

            if (!this._ReportTitle3.Equals(Default))
            {
                strarr[4, 0] = "ReportTitle3";
                strarr[4, 1] = _ReportTitle3;
                strarr[4, 2] = "string";
            }

            if (!this._TreesFooter1.Equals(Default))
            {
                strarr[5, 0] = "TreesFooter1";
                strarr[5, 1] = _TreesFooter1;
                strarr[5, 2] = "string";
            }

            if (!this._TreesFooter2.Equals(Default))
            {
                strarr[6, 0] = "TreesFooter2";
                strarr[6, 1] = _TreesFooter2;
                strarr[6, 2] = "string";
            }

            if (!this._ClientWebAddress.Equals(Default))
            {
                strarr[7, 0] = "ClientWebAddress";
                strarr[7, 1] = _ClientWebAddress;
                strarr[7, 2] = "string";
            }
            return strarr;
        }

        #endregion

        #region "Validation"

        private bool ValidateData()
        {
         
            return true;
        }
        #endregion
    }
}
