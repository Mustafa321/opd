using System;
using System.Data;
using LimsDataLayer;

namespace BusinessLayer
{
    /// <summary>
    /// Summary description for clsBLLogin.
    /// </summary>
    public class clsLogin
    {
        private string StrErrorMessage = "";
        private string _PersonId = "";
        private string _Loginid = "";
        private string _Password = "";
        private string _Active = "";
        private string _OptionID = "";

        clsoperation objTrans = new clsoperation();
        clsdbhims objdbhims = new clsdbhims();

        #region Properties
        public string PersonId
        {
            get { return _PersonId; }
            set { _PersonId = value; }
        }
        public string Loginid
        {
            get { return _Loginid; }
            set { _Loginid = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public string ErrorMessage
        {
            get { return StrErrorMessage; }
            set { StrErrorMessage = value; }
        }
        public string OptionId
        {
            get { return  _OptionID;}
            set { _OptionID = value; }
        }
        #endregion

        #region Method
        public DataView GetLogin(int flag)
        {
            switch (flag)
            {
                case 1:
                    objdbhims.Query = "Select p.PersonId,concat(p.salutation,p.FName,' ',p.MName,' ',p.LName) as PersonName,p.OrgId,p.subdepartmentid,ifnull(crossdept,'N') as crossdept,p.departmentid,u.defaultpage from dc_tp_personnel p left outer join dc_tu_userright u on u.personid=p.personid Where p.Active='Y' and p.LoginId = '" + _Loginid.ToString() + "' and p.Pasword ='" + _Password.ToString() + "'";
                    break;
                case 2: // user matrix
                    objdbhims.Query = "select gr.OptionId from dc_tu_userright m,dc_tu_accessgroup ag,dc_tu_groupright gr where ag.GroupId = gr.GroupId and m.GroupId = gr.GroupId and ag.Active = 'Y' and m.Active = 'Y' and m.personid = "+_PersonId+" and gr.OptionId = "+_OptionID+"";
                    break;
                case 3: // access option user wise
                    objdbhims.Query = "SELECT distinct o.optionid,o.optionname FROM dc_tu_accessoption o left outer join dc_tu_groupright r on o.optionid=r.optionid left outer join dc_tu_userright u on r.groupid = u.groupid where u.personid="+_PersonId+" order by o.optionid";
                    break;
            }
            return objTrans.DataTrigger_Get_All(objdbhims);
        }

        #endregion

        public clsLogin()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
