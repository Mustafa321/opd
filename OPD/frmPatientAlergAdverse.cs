using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmPatientAlergAdverse : Form
    {
        private string _Type = "";
        private string _PatientID = "";
        private frmOPD objOPD = null;
        private bool _Flag = false;
        private string _MedID = "";
        private string _MedName = "";

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string gMedID
        {
            get { return _MedID; }
            set { _MedID = value; }
        }
        public string gMedName
        {
            get { return _MedName; }
            set { _MedName = value; }
        }

        public frmPatientAlergAdverse()
        {
            InitializeComponent();
        }

        public frmPatientAlergAdverse(frmOPD objOPD)
        {
            InitializeComponent();
            this.objOPD = objOPD;
        }

        private void frmPatientAlergAdverse_Load(object sender, EventArgs e)
        {
            this.txtPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);

            FillAllAllergAR();
            FillAllMed();
            FillPatientInfo();
            FillSelect();

            if (_Type.Equals("A"))
            {
                lblAllergARHeading.Text = "Allergies";
                dgSelect.Columns["AlergARName"].HeaderText = "Allergies";
            }
            else if (_Type.Equals("R"))
            {
                lblAllergARHeading.Text = "Adverse Reaction";
                dgSelect.Columns["AlergARName"].HeaderText = "Adverse Reaction";
            }

            cmbMed.Text = _MedName;

            _Flag = true;
            objComp = null;
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPatientAlergAdverse_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objOPD != null)
            {
                objOPD.Show();
                this.Dispose();
            }
        }

        private void FillAllAllergAR()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAllergAdvers objAllergAdvers = new clsBLAllergAdvers(objConnection);
            SComponents objComp = new SComponents();

            objAllergAdvers.Type = _Type;
            objConnection.Connection_Open();
            objComp.FillComboBox(cmbAllergAR, objAllergAdvers.GetAll(1), "Name", "AlergAdversID", true);
            objConnection.Connection_Close();


            objAllergAdvers = null;
            objConnection = null;
            objComp = null;
        }

        private void FillAllMed()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            SComponents objComp = new SComponents();

            objMed.PersonID = clsSharedVariables.UserID;
            objConnection.Connection_Open();
            objComp.FillComboBox(cmbMed, objMed.GetAll(16), "MedName", "MedID", true);
            objConnection.Connection_Close();

            objMed = null;
            objConnection = null;
            objComp = null;
        }

        private void FillPatientInfo()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();

            objPatientReg.PatientID = _PatientID;
            objConnection.Connection_Open();
            dv = objPatientReg.GetAll(16);
            objConnection.Connection_Close();

            if (dv.Table.Rows.Count != 0)
            {
                txtPRNo.Text = dv.Table.Rows[0]["prno"].ToString();
                txtPName.Text = dv.Table.Rows[0]["PName"].ToString();
                txtGender.Text = dv.Table.Rows[0]["gender"].ToString().Equals("M") ? "Male" : "Female";
                txtAge.Text = dv.Table.Rows[0]["age"].ToString();
            }

            objPatientReg = null;
            objConnection = null;
        }

        private void FillSelect()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedAlerAdver objOPDMedAlerAdver = new clsBLOPDMedAlerAdver(objConnection);

            dgSelect.SuspendLayout();
            objOPDMedAlerAdver.Type = _Type;
            objConnection.Connection_Open();
            objOPDMedAlerAdver.PatientID = _PatientID;

            dgSelect.DataSource = objOPDMedAlerAdver.GetAll(3);
            objConnection.Connection_Close();

            dgSelect.ResumeLayout();
            objOPDMedAlerAdver = null;
        }

        private void ClearGridRows(DataGridView dg)
        {
            try
            {
                DataView dv = (DataView)dg.DataSource;

                if (dv != null)
                {
                    dv.Table.Rows.Clear();
                    dg.DataSource = dv;
                }
            }
            catch
            {
                DataTable dt = (DataTable)dg.DataSource;

                if (dt != null)
                {
                    dt.Rows.Clear();
                    dg.DataSource = dt;
                }
            }
            //while (dg.Rows.Count > 0)
            //{
            //  dg.Rows.RemoveAt(0);
            //}
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            cmbAllergAR.SelectedIndex = 0;
            cmbMed.Text = _MedName;
            FillSelect();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (cmbAllergAR.SelectedIndex != 0)
            {
                AddAllergAR();
            }
        }

        private void miSelectedDelete_Click(object sender, EventArgs e)
        {
            dgSelect.Rows[Convert.ToInt16(cmsSelected.Items[0].Tag)].Cells["mode"].Value = "D";
            formatGrid();
        }

        private void dgSelect_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;

            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dg.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dg.ContextMenuStrip.Items[0].Enabled = true;
                dg.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dg.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dg.ContextMenuStrip.Items[0].Enabled = false;
                dg.ContextMenuStrip.Items[0].Tag = null;
            }

            hi = null;
        }

        private void AddAllergAR()
        {
            int Flag = 0;
            DataView dt = (DataView)dgSelect.DataSource;

            for (int i = 0; i < dgSelect.Rows.Count; i++)
            {
                if (cmbMed.SelectedValue.ToString().Equals(dgSelect.Rows[i].Cells["Medicineid"].Value.ToString()) && cmbAllergAR.SelectedValue.ToString().Equals(dgSelect.Rows[i].Cells["AlergAdversID"].Value.ToString()))
                {
                    Flag = 1;
                    //MessageBox.Show("Disease Already Added ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                }
            }

            if (Flag == 0)
            {
                DataRow dr = dt.Table.NewRow();

                dr[0] = 0;
                dr[1] = Convert.ToInt16(cmbAllergAR.SelectedValue);
                dr[2] = cmbAllergAR.Text;
                if (cmbMed.SelectedIndex == 0)
                {
                    dr[3] = 0;
                    dr[4] = "";
                }
                else
                {
                    dr[3] = Convert.ToInt16(cmbMed.SelectedValue);
                    dr[4] = cmbMed.Text;
                }
                dr[5] = "";
                dr[6] = "A";
                dt.Table.Rows.Add(dr);

                dgSelect.DataSource = dt;
                formatGrid();
            }
        }

        private void formatGrid()
        {
            for (int i = 0; i < dgSelect.Rows.Count; i++)
            {
                dgSelect.Rows[0].Selected = true;

                if (dgSelect.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                {
                    dgSelect.Rows[i].DefaultCellStyle.BackColor = Color.BurlyWood;
                }
                else if (dgSelect.Rows[i].Cells["Mode"].Value.ToString().Equals("D"))
                {
                    dgSelect.Rows[i].DefaultCellStyle.BackColor = Color.Cornsilk;
                }
                else  if (dgSelect.Rows[i].Cells["Mode"].Value.ToString().Equals("A"))
                {
                    dgSelect.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                }
            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedAlerAdver objOPDMedAlerAdver = new clsBLOPDMedAlerAdver(objConnection);

            try
            {
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                for (int i = 0; i < dgSelect.Rows.Count; i++)
                {
                    if (dgSelect.Rows[i].Cells["Mode"].Value.ToString().Equals("D"))
                    {
                        Update(objOPDMedAlerAdver, i);
                    }
                    else if (dgSelect.Rows[i].Cells["Mode"].Value.ToString().Equals("A"))
                    {
                        Insert(objOPDMedAlerAdver, i);
                    }
                }

                objConnection.Transaction_ComRoll();

                FillSelect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Insertion Successfully", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objOPDMedAlerAdver = null;
            }
        }

        private void Insert(clsBLOPDMedAlerAdver objOPDMedAlerAdver, int RowIdx)
        {

            objOPDMedAlerAdver.AlergAdversID = dgSelect.Rows[RowIdx].Cells["AlergAdversID"].Value.ToString();
            objOPDMedAlerAdver.PatientID = _PatientID;
            if (!dgSelect.Rows[RowIdx].Cells["Medicineid"].Value.ToString().Equals("0"))
            {
                objOPDMedAlerAdver.MedID = dgSelect.Rows[RowIdx].Cells["Medicineid"].Value.ToString();
            }
            objOPDMedAlerAdver.Description = "";
            objOPDMedAlerAdver.EnteredBy = clsSharedVariables.UserID;
            objOPDMedAlerAdver.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            objOPDMedAlerAdver.Active = "Y";

            objOPDMedAlerAdver.Insert();
        }

        private void Update(clsBLOPDMedAlerAdver objOPDMedAlerAdver, int RowIdx)
        {
            objOPDMedAlerAdver.Active= "N";
            objOPDMedAlerAdver.ID = dgSelect.Rows[RowIdx].Cells["PatientAllergAR"].Value.ToString();
            objOPDMedAlerAdver.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

            objOPDMedAlerAdver.Update();
        }
    }
}