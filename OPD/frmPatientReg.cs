using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;
using System.IO;
using System.Net.Mail;
using System.Net.Configuration;
using System.Threading;
using Pabo.Calendar;

namespace OPD
{
    public partial class frmPatientReg : Form
    {
        private string _Path = "";
        private int _dgAppointRowIdx = 0;
        private int _dgRegPatientRowIdx = 0;


        private string strHistory = "";
        private string strPresentingComplaints = "";
        private string strSigns = "";
        private string strDiagnosis = "";
        private string strInvestigation = "";
        private string strPlans = "";


        public frmPatientReg()
        {
            InitializeComponent();
        }

        private void frmPatientReg_Load(object sender, EventArgs e)
        {
           
                this.txtRPRNo.ReadOnly = clsSharedVariables.AutoPRNO;
           
            this.txtRPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtVSPRNO.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            SComponents objComp = new SComponents();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            clsBLPanel objPnl = new clsBLPanel(objConnection);
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);

            try
            {
                objComp.ApplyStyleToControls(this);
                objConnection.Connection_Open();

                FillGridView(objConnection, objPatientReg, 1);
                objComp.FillComboBox(cboCity, objPatientReg.GetAll(3), "Name", "CityID", false);
                if (objPatientReg.GetAll(10).Table.Rows.Count != 0)
                {
                    cboCity.Text = objPatientReg.GetAll(10).Table.Rows[0]["Name"].ToString();
                }
                objComp.FillComboBox(cboPanel, objPnl.GetAll(1), "PanelName", "PanelID", true);
                dgPatientReg.ClearSelection();
                dtpFrom.MaxDate = DateTime.Now.Date;
                dtpTo.Value = Convert.ToDateTime("2004/01/01");
                dtpDateBirth.MaxDate = DateTime.Now;
                //lblTotalPatient.Text = "Total Record Found : " + dgPatientReg.Rows.Count.ToString();
                this.Text = this.Text + "(" + clsSharedVariables.UserName + ")";

                objComp.FillComboBox(cboConsultent, objPerson.GetAll(6), "Name", "Personid", false);
                objComp.FillComboBox(cboChangeConultant, objPerson.GetAll(6), "Name", "Personid", false);

                mcAppointment.SelectDate(DateTime.Now.Date);
                if (cboConsultent.Items.Count != 0)
                {
                    objPerson.PersonID = cboConsultent.SelectedValue.ToString();

                    cboConsultent.Tag = objPerson.GetAll(7).Table.Rows[0]["MaxSlot"].ToString();
                    //mcAppointment1.SelectionStart = DateTime.Now.Date;
                    //mcAppointment1.MinDate = DateTime.Today;
                }
                else
                {
                    panel5.Enabled = false;
                }
                FillTodaySummary();
                //GetAppointment(objConnection);
                cboHeightUnit.SelectedIndex = 0;
                cboTempUnit.SelectedIndex = 0;
                cboWeightUnit.SelectedIndex = 0;
                dtpFrom.MinDate = DateTime.Now.AddYears(-25);
               
                dtpFrom.MaxDate = DateTime.Now;
              
                dtpTo.MinDate = DateTime.Now.AddYears(-25);

                dtpTo.MaxDate = DateTime.Now;
               
                lblWait.BackColor = Color.White;
                lblConsulted.BackColor = Color.SeaShell;
                lblHold.BackColor = Color.Azure;
                //FillAppointmenthCal();
                mcAppointment.MinDate = DateTime.Now.AddDays(-1);
                dtpAppointDate.MinDate = DateTime.Now;
                dtpAppointDate.Value = DateTime.Now;
                //FormatCal();
                FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objComp = null;
                objPatientReg = null;
                objPnl = null;
                objPerson = null;
            }
        }
        private int DuplicatePRNO()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();

            objPatient.PRNO = txtRPRNo.Text;
            objConnection.Connection_Open();
            dv = objPatient.GetAll(7);

            if (!btnSave.Text.Equals("&Save"))
            {
                dv.RowFilter = "PatientID <>" + lblPatientID.Text;
             
            }
            if (dv.Count == 0)
            {
                objConnection.Connection_Close();
                return 1;
            }
            else
            {
                objConnection.Connection_Close();
                return 0;
            }

        }
        private clsBLPatientReg SetBLValues(clsBLPatientReg objPatient, clsBLDBConnection objConnection)
        {
            objPatient.Name = clsSharedVariables.InItCaps(txtName.Text.Trim());
        
            if (!clsSharedVariables.AutoPRNO)
            {
                int Flag = 0;
               
                Flag = DuplicatePRNO();
                if (Flag == 0)
                {
                    MessageBox.Show("" + clsSharedVariables.PRNOHeading + " already assign", "" + clsSharedVariables.PRNOHeading + " Duplication", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objPatient.PRNO = "Duplicate";
                    return objPatient;
                }
                else
                {
                    objPatient.PRNO = txtRPRNo.Text.ToString();
                }
               
            }
            else if (btnSave.Text.Equals("&Save"))
            {
                int Flag = 0;
                while (Flag == 0)
                {
                    Flag = DuplicatePRNO(objPatient);
                }
            }
            if (rbMale.Checked)
            {
                objPatient.Gender = "M";
            }
            else
            {
                objPatient.Gender = "F";
            }
            objPatient.MaritalStatus = rbSingle.Checked ? "S" : "M";
            objPatient.DOB = dtpDateBirth.Value.Date.ToString("dd/MM/yyyy");
            objPatient.TempDOB = dtpDateBirth.Value.Date.ToShortDateString();
            objPatient.PhoneNo = txtPhone.Text.Trim();
            objPatient.CellNo = txtCell.Text.Trim();
            objPatient.Address = clsSharedVariables.InItCaps(txtAddress.Text.Trim());
            objPatient.CityID = clsSharedVariables.InItCaps(cboCity.SelectedValue.ToString());
            objPatient.EnteredBy = clsSharedVariables.UserID; ;
            objPatient.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPatient.Email = txtEmail.Text;
            objPatient.MRNo = objPatient.PRNO.Replace("-", string.Empty);
           
            if (objPatient.RefPRNO != "")
                if (!cboPanel.SelectedValue.ToString().Equals("-1"))
                {
                    objPatient.PanelID = cboPanel.SelectedValue.ToString();
                    objPatient.EmpNo = txtEmpNo.Text.Trim();
                }
                else
                {
                    objPatient.PanelID = "0";
                }

            return objPatient;
        }

        private int DuplicatePRNO(clsBLPatientReg objPatient)
        {
            int RndVal = 0;
            Random rnd = new Random();

            RndVal = rnd.Next(0, 999999);

            lblPRNO.Text = objPatient.PRNO = DateTime.Now.ToString("yy") + "-" + DateTime.Now.ToString("MM") + "-" + RndVal.ToString("000000");
            if (objPatient.GetAll(7).Table.Rows.Count == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private bool Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            try
            {
                objConnection.Connection_Open();

                if (!cboPanel.SelectedValue.ToString().Equals("-1"))
                {
                    if (txtEmpNo.Text.Trim().Equals(""))
                    {
                        MessageBox.Show("Employ No. is Must in case of Panel");
                        txtEmpNo.Focus();
                        return false;
                    }
                }
                objPatient = SetBLValues(objPatient, objConnection);
                if(objPatient.PRNO!= "Duplicate")
                {
                    objConnection.Transaction_Begin();
                    if (objPatient.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                        objPatient.PRNO = "";
                        FillGridView(objConnection, objPatient, 1);
                        lblPatientID.Text = objPatient.GetAll(6).Table.Rows[0][0].ToString();
                        //MessageBox.Show("Patient Registration complete Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
              
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                objConnection.Connection_Close();
                objPatient = null;
                objConnection = null;
            }
            return true;
        }

        private bool UpdateData()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            clsBLDBConnection objConnection1 = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection1);
            try
            {
                objConnection.Connection_Open();
                if (!cboPanel.SelectedValue.ToString().Equals("-1"))
                {
                    if (txtEmpNo.Text.Trim().Equals(""))
                    {
                        MessageBox.Show("Employ No. is Must in case of Panel");
                        txtEmpNo.Focus();
                        return false;
                    }
                }
                objPatient.PatientID = this.lblPatientID.Text;

                objPatient = SetBLValues(objPatient, objConnection);
                objConnection.Transaction_Begin();
                if (objPatient.Update())
                {
                    objOPD.PRNO = txtRPRNo.Text.ToString();
                    objOPD.PatientID = this.lblPatientID.Text;
                    objOPD.UpdateOPD();
                    objConnection.Transaction_ComRoll();
                    objPatient.PRNO = "";
                    FillGridView(objConnection, objPatient, 1);

                    MessageBox.Show("Patient Registration updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                objConnection.Connection_Close();
                objPatient = null;
                objConnection = null;
            }
            return true;
        }

        private void FillGridView(clsBLDBConnection objConnection, clsBLPatientReg objPatient, int Flag)
        {
            DataView dvPatient = objPatient.GetAll(Flag);
            dvPatient.Sort = "Name";
            this.dgPatientReg.DataSource = dvPatient;
            GenrateSerialNo();

            dvPatient = null;
        }

        private void GetAppointment(clsBLDBConnection objConnection)
        {
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dvAppointment;
            //int x = 0, y = 5;
            //int AppointCount = 0;
            try
            {
                //objAppoint.AppointmentDate = mcAppointment1.SelectionStart.ToString("yyyy-MM-dd");
                if (mcAppointment.SelectedDates.Count != 0)
                {
                    objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");
                }
                else
                {
                    objAppoint.AppointmentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                }
                objAppoint.PersonID = cboConsultent.SelectedValue.ToString();

                //pnlAppointList.Controls.Clear();

                dvAppointment = objAppoint.GetAll(4);
                dgAppointment.DataSource = dvAppointment;

                //int i = 0;
                //foreach (DataRow dr in dvAppointment.Table.Rows)
                //{
                //    this.pnlAppointList.Controls.Add(llblProperties(x, y, "llbl" + AppointCount.ToString(), dr["PRNO"].ToString(), dr["AppointmentID"].ToString()));

                //    this.pnlAppointList.Controls.Add(llblProperties(x + 85, y, "llblName" + AppointCount.ToString(), dr["Name"].ToString(), dr["PRNO"].ToString()));

                //    this.pnlAppointList.Controls.Add(lblProperties(x + 208, y, "lbl" + AppointCount.ToString(), dr["TokkenNo"].ToString()));
                //    y = y + 17;
                //    i++;
                //}
                lblAppointment.Text = "Appointments " + dvAppointment.Table.Rows.Count.ToString() + " / " + cboConsultent.Tag.ToString();
                dgAppointment.ClearSelection();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objAppoint = null;
            }
        }

        private Label lblProperties(int x, int y, string lblName, string textVal)
        {
            Label lbl = new Label();

            lbl.AutoSize = true;
            lbl.Location = new System.Drawing.Point(x, y);
            lbl.Name = lblName;
            lbl.TabStop = false;
            lbl.Text = textVal;

            return lbl;
        }

        private LinkLabel llblProperties(int x, int y, string llblName, string textVal, string textTag)
        {
            LinkLabel llbl = new LinkLabel();

            llbl.AutoSize = true;
            llbl.Location = new System.Drawing.Point(x, y);
            llbl.Name = llblName;
            llbl.TextAlign = ContentAlignment.TopCenter;
            llbl.Size = new System.Drawing.Size(30, 20);
            llbl.Text = textVal;

            llbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            llbl.Tag = textTag;
            if (textTag.Length == 12)
            {
                llbl.Click += new EventHandler(llblName_Click);
            }
            else
            {
                llbl.Click += new EventHandler(llbl_Click);
            }
            return llbl;
        }

        private void llbl_Click(object sender, EventArgs e)
        {
            LinkLabel llbl = new LinkLabel();
            llbl = (LinkLabel)sender;

            FillFormClickLlbl(llbl.Text);
        }

        private void llblName_Click(object sender, EventArgs e)
        {
            LinkLabel llbl = new LinkLabel();
            llbl = (LinkLabel)sender;

            FillFormClickLlbl(llbl.Tag.ToString());
        }

        private void GenrateSerialNo()
        {
            for (int i = 1; i <= dgPatientReg.Rows.Count; i++)
            {
                dgPatientReg.Rows[i - 1].Cells["SerialNo"].Value = i.ToString();
            }
        }

        private void ClearField()
        {
            txtName.Text = "";
            txtRPRNo.Text = "";
            dtpDateBirth.Value = DateTime.Now.Date;
            rbMale.Checked = true;
            txtPhone.Text = "";
            txtCell.Text = "";
            txtAddress.Text = "";
            txtEmail.Text = "";
            dgPatientReg.ClearSelection();
            txtName.Focus();
            txtAge.Text = "0";
            cboPanel.Text = "Select";
            this.lblPatientID.Text = "";
            lblPRNO.Text = "";
            this.btnSave.Text = "&Save";
            txtRefNo.Text = "";
            rbCash.Checked = true;
            txtEmpNo.Text = "";
            rbSingle.Checked = true;
            lblOPDID.Text = "";
            _Path = "";
            _dgAppointRowIdx = -1;
            _dgRegPatientRowIdx = -1;
            picPatient.Image = null;

            strHistory = "";
            strPresentingComplaints = "";
            strSigns = "";
            strDiagnosis = "";
            strInvestigation = "";
            strPlans = "";

            dtpAppointDate.Value = DateTime.Now;

            //clsBLDBConnection objConnection = new clsBLDBConnection();
            //clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);

            //try
            //{
            //    objConnection.Connection_Open();
            //    if (objPatientReg.GetAll(10).Table.Rows.Count != 0)
            //    {
            //        cboCity.Text = objPatientReg.GetAll(10).Table.Rows[0]["Name"].ToString();
            //    }

            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show(exc.Message);
            //}
            //finally
            //{
            //    objConnection.Connection_Close();
            //    objConnection = null;
            //    objPatientReg = null;
            //}
        }

        private void ClearSearch()
        {
            txtSearchPRNO.Text = "";
            txtSearchName.Text = "";
            txtSearchPhone.Text = "";
        }

        private void FillForm(int rowIndex)
        {
            this.btnSave.Text = "&Update";

            lblPatientID.Text = dgPatientReg.Rows[rowIndex].Cells["PatientID"].Value.ToString();
           txtRPRNo.Mask = "";
            lblPRNO.Text = dgPatientReg.Rows[rowIndex].Cells["PRNO"].Value.ToString();
            txtRPRNo.Text = dgPatientReg.Rows[rowIndex].Cells["PRNO"].Value.ToString() ;
            txtName.Text = dgPatientReg.Rows[rowIndex].Cells["PatientName"].Value.ToString();
            if (dgPatientReg.Rows[rowIndex].Cells["Gender"].Value.ToString().Equals("Male"))
            {
                rbMale.Checked = true;
            }
            else
            {
                rbFemale.Checked = true;
            }

            if (dgPatientReg.Rows[rowIndex].Cells["MaritalStatus"].Value.ToString().Equals("S"))
            {
                rbSingle.Checked = true;
            }
            else
            {
                rbMarried.Checked = true;
            }
            dtpDateBirth.Value = Convert.ToDateTime(dgPatientReg.Rows[rowIndex].Cells["DOB"].Value.ToString());
            txtPhone.Text = dgPatientReg.Rows[rowIndex].Cells["PhoneNo"].Value.ToString();
            txtCell.Text = dgPatientReg.Rows[rowIndex].Cells["CellNo"].Value.ToString();
            txtAddress.Text = dgPatientReg.Rows[rowIndex].Cells["Address"].Value.ToString();
            cboCity.Text = dgPatientReg.Rows[rowIndex].Cells["City"].Value.ToString();
            //txtAge.Text = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dgPatientReg.Rows[rowIndex].Cells["DOB"].Value.ToString()).Year);
            txtAge.Text = dgPatientReg.Rows[rowIndex].Cells["Age"].Value.ToString().Split(' ')[0].ToString();
            label6.Text = dgPatientReg.Rows[rowIndex].Cells["Age"].Value.ToString().Split(' ')[1].ToString();
            txtEmail.Text = dgPatientReg.Rows[rowIndex].Cells["Email"].Value.ToString();
            if (dgPatientReg.Rows[rowIndex].Cells["Panel"].Value.ToString().Equals("0") || dgPatientReg.Rows[rowIndex].Cells["Panel"].Value.ToString().Equals(""))
            {
                cboPanel.Text = "Select";
            }
            else
            {
                cboPanel.Text = dgPatientReg.Rows[rowIndex].Cells["Panel"].Value.ToString();
            }
            txtEmpNo.Text = dgPatientReg.Rows[rowIndex].Cells["EmployNo"].Value.ToString();
            GetPatientPicture();
            GetCharges();
        }

        private void dgPatientReg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                pnlRegisterSearch.Size = new Size(943, 30);
                btnExpend.Visible = true;
                btnCol.Visible = false;

                FillForm(e.RowIndex);
            }
        }

        private void txtInItCap_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private bool InsertOPD(string status, string VDate)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            try
            {
                objConnection.Connection_Open();
                if (!rbCash.Checked)
                {
                    if (txtRefNo.Text.Trim().Equals(""))
                    {
                        MessageBox.Show("Reference Number is Not Empty In case of Debit Card and Crdit Card ");
                        txtRefNo.Focus();
                        return false;
                    }
                }
                objOPD.TempVisitDate =  mcAppointment.SelectedDates[0].Date.ToString("yyyy/MM/dd");

                objOPD.Status = status;
                objOPD.VisitDate = VDate;
                objOPD = SetBLValues(objOPD);
                objConnection.Transaction_Begin();
                if (objOPD.Insert())
                {
                    lblOPDID.Text = objOPD.GetAll(15).Table.Rows[0][0].ToString();
                    objConnection.Transaction_ComRoll();
                    GetAppointment(objConnection);
                    if (mcAppointment.SelectedDates.Count != 0)
                    {
                        FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
                    }
                    else
                    {
                        FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
                    }

                    return true;
                }
                else
                {
                    MessageBox.Show(objOPD.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                objConnection.Connection_Close();
                objOPD = null;
                objConnection = null;
            }
        }

        private clsBLOPD SetBLValues(clsBLOPD objOPD)
        {
            Validation objValid = new Validation();

            objOPD.PRNO = lblPRNO.Text;
            objOPD.PatientID = lblPatientID.Text;

            if (!txtPayment.Equals("") && objValid.IsInteger(txtPayment.Text))
            {
                objOPD.Payment = txtPayment.Text;
            }

            if (rbCash.Checked)
            {
                objOPD.PaymentMode = "C";
            }
            else if (rbCredit.Checked)
            {
                objOPD.PaymentMode = "R";
                objOPD.RefNo = txtRefNo.Text.Trim();
            }
            else if (rbDebit.Checked)
            {
                objOPD.PaymentMode = "D";
                objOPD.RefNo = txtRefNo.Text.Trim();
            }

            objOPD.Active = "1";
            objOPD.EnteredBy = cboConsultent.SelectedValue.ToString();
            objOPD.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");

            return objOPD;
        }

        private void dtpDateBirth_Leave(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpDateBirth.Value.Year;
            txtAge.Text = Age.ToString();
        }

        private void txtAge_Leave(object sender, EventArgs e)
        {
            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtAge.Text) && Convert.ToInt16(txtAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtAge.Text.Trim())) + "";
                dtpDateBirth.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtAge.Text = "";
            }
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnPatientSearch_Click(sender, EventArgs.Empty);
            }
        }

        private void btnPatientSearch_Click(object sender, EventArgs e)
        {
            SearchCriteria();
            //lblTotalPatient.Text = "Total Record Found : " + dgPatientReg.Rows.Count.ToString();
        }

        private void SearchCriteria()//Register patient 
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            objConnection.Connection_Open();
            var pr = txtSearchPRNO.Text.Trim();
            txtSearchPRNO.Mask = "";
            if (!txtSearchPRNO.Text.Trim().Equals("") && !txtSearchPRNO.Text.Trim().Equals(""))
            {
                objPatient.PRNO = pr;
                objPatient.MRNo = pr.Replace("-", string.Empty);
                txtSearchPRNO.Text = pr;
            }
            if (!txtSearchName.Text.Trim().Equals("")&& txtSearchPRNO.Text.Trim().Equals(""))
            {
                objPatient.Name = clsSharedVariables.InItCaps(txtSearchName.Text.Trim());
            }
            if (txtSearchPhone.Text.Trim().Length >= 7&& txtSearchPRNO.Text.Trim().Equals(""))
            {
                if (rbSPhone.Checked)
                {
                    objPatient.PhoneNo = txtSearchPhone.Text.Trim();
                }
                else
                {
                    objPatient.CellNo = txtSearchPhone.Text.Trim();
                }
            }
          
            
                objPatient.FromDate = dtpFrom.Value.ToString("dd/MM/yyyy");
                objPatient.ToDate = dtpTo.Value.ToString("dd/MM/yyyy");
           
   
            FillGridView(objConnection, objPatient, 11);
            if (dgPatientReg.Rows.Count != 0)
            {
                // FillPatientReg(0);
            }
            else
            {
                MessageBox.Show(" Patient Not Found ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            objConnection.Connection_Close();


            objConnection = null;
            objPatient = null;
        }

        private void dgPatientReg_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenrateSerialNo();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void miSave_Click(object sender, EventArgs e)
        {


        }

        private void miCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboPanel_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPanel objPnl = new clsBLPanel(objConnection);
            DataView dv = new DataView();

            try
            {
                if (cboPanel.Text.Equals("Select"))
                {
                    lblEmpNo.Enabled = false;
                    txtEmpNo.Enabled = false;
                }
                else
                {
                    lblEmpNo.Enabled = true;
                    txtEmpNo.Enabled = true;
                }
                objConnection.Connection_Open();
                objPnl.PanelID = cboPanel.SelectedValue.ToString();
                dv = objPnl.GetAll(1);
                if (dv.Table.Rows.Count != 0)
                {
                    txtPayment.Text = dv.Table.Rows[0]["ConsultationFee"].ToString();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPnl = null;
                dv.Dispose();
            }
        }

        private void mcAppointment_DateSelected(object sender, DateRangeEventArgs e)
        {
            PopulateAppointment();
        }

        private void btnAppointment_Click(object sender, EventArgs e)
         {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dv = new DataView();
            try
            {
                if (!lblPRNO.Text.Equals("") && !lblPRNO.Text.Equals("lblPRNO"))
                {
                    objConnection.Connection_Open();

                    objAppoint.PersonID = cboConsultent.SelectedValue.ToString();
                    //objAppoint.AppointmentDate = dtpAppointment.Value.ToString("yyyy-MM-dd");
                    //objAppoint.AppointmentDate = mcAppointment1.SelectionStart.ToString("yyyy-MM-dd");


                    DateItem[] d = new DateItem[2];
                    d.Initialize();
                    for (int i = 0; i < 2; i++)
                        d[i] = new DateItem();
                    d[0].Date = new System.DateTime(2010, 3, 5);
                    d[1].Date = new System.DateTime(2010, 3, 5);
                    mcAppointment.AddDateInfo(d);
                    //if (mcAppointment.SelectedDates.Count == 0)
                    //{
                    //   // dtpAppointDate.Value = DateTime.Now;
                    //    mcAppointment.SelectDate(DateTime.Now);
                    //    //mcAppointment.Dates.InnerList[0];
                    //}

                    //mcAppointment.SelectedDates.Add(new DateTime(2010, 3, 5));
                    //mcAppointment.SelectedDates.Add(new DateTime(2010, 3, 5));
                    //mcAppointment.SelectedDates.Add(new DateTime(2010, 3, 25));

                    //SelectedDatesCollection dates = new SelectedDatesCollection(new DateTime());
                    //dates.Add(new DateTime());
                    

                    //mcAppointment.SelectedDates.IndexOf = new DateTime[] {new System.DateTime(2002, 4, 20, 0, 0, 0, 0),
                    //                new System.DateTime(2002, 4, 28, 0, 0, 0, 0),
                    //                new System.DateTime(2002, 5, 5, 0, 0, 0, 0),
                    //                new System.DateTime(2002, 7, 4, 0, 0, 0, 0),
                    //                new System.DateTime(2002, 12, 15, 0, 0, 0, 0),
                    //                new System.DateTime(2002, 12, 18, 0, 0, 0, 0) };


                  //  mcAppointment.SelectedDates.Move(new DateTime(2020,06,08), 0);

                   

                    objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");



                    var coun =  mcAppointment.SelectedDates.Count.ToString();
                   
                   // mcAppointment.SelectedDates.Clear();
                    
                   // var data= mcAppointment.SelectedDates;

                  //  objAppoint.AppointmentDate = mcAppointment.SelectedDates[2].Date.ToString("yyyy-MM-dd");

                   // objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");




                    //string data = "";
                    //data = mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy");
                    //objAppoint.AppointmentDate =data ;


                    objAppoint.PRNO = lblPRNO.Text;
                    if (objAppoint.GetAll(5).Table.Rows.Count == 0)
                    {
                        //if (InsertOPD("W", mcAppointment1.SelectionStart.ToString("dd/MM/yyyy")))
                        if (InsertOPD("W", mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy")))
                        {
                            InsertAppointment(objConnection, objAppoint);

                            dv = GetEamilAddress(objConnection, cboConsultent.SelectedValue.ToString(), lblPatientID.Text);

                            if (dv.Table.Rows.Count != 0)
                            {
                                if (!dv.Table.Rows[0]["toMessage"].ToString().Equals("") && !dv.Table.Rows[0]["fromMessage"].ToString().Equals(""))
                                {
                                    string subject = "Appointment Confirmation ";
                                    string Body = "Appointment Confirmation " + clsSharedVariables.PRNOHeading + " With Doctor";
                                    StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, "");//@srm
                                }
                            }

                            GetAppointment(objConnection);
                            if (mcAppointment.SelectedDates.Count != 0)
                            {
                                FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.Dates[0].Date.Year);
                            }
                            else
                            {
                                FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
                            }
                            if (mcAppointment.SelectedDates[0].Date.Equals(DateTime.Now.Date))
                            //if (mcAppointment1.SelectionStart.Date.Equals(DateTime.Now.Date))
                            {
                                //PrintPaymentSlip();
                            }
                            FillTodaySummary();
                            pnlRegisterSearch.Size = new Size(943, 226);
                            btnExpend.Visible = false;
                            btnCol.Visible = true;
                        }
                    }
                    objConnection.Connection_Close();
                }
                else
                {
                    MessageBox.Show("Please Select Patient For Appointment", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ClearField();
                objConnection = null;
            }
        }

        private void InsertAppointment(clsBLDBConnection objConnection, clsBLAppointment objAppoint)
        {
            DataView dv;

            try
            {
                dv = objAppoint.GetAll(3);

                if (dv.Table.Rows.Count == 0)//dv.Table.Rows[0][0].ToString().Equals(""))
                {
                    objAppoint.TokkenNo = "1";
                }
                else
                {
                    objAppoint.TokkenNo = dv.Table.Rows[0]["Tokkenno"].ToString();
                }
                //objAppoint.AppointmentDate = mcAppointment1.SelectionStart.ToString("dd/MM/yyyy"); ;
                objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy"); ;
                objAppoint.PRNO = lblPRNO.Text;
                objAppoint.MaxSlot = cboConsultent.Tag.ToString();

                objConnection.Transaction_Begin();
                if (objAppoint.Insert())
                {
                    objConnection.Transaction_ComRoll();
                }
                else
                {
                    MessageBox.Show(objAppoint.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objAppoint = null;
            }
        }

        private void UpdateAppointment(clsBLDBConnection objConnection, clsBLAppointment objAppoint)
        {
            DataView dv;
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);
            try
            {
                objAppoint.PersonID = cboChangeConultant.SelectedValue.ToString();
                objAppoint.AppointmentDate = dtpAppointDate.Value.Date.ToString("yyyy-MM-dd");

                dv = objAppoint.GetAll(3);

                if (dv.Table.Rows.Count == 0)// dv.Table.Rows[0][0].ToString().Equals(""))
                {
                    objAppoint.TokkenNo = "1";
                }
                else
                {
                    objAppoint.TokkenNo = dv.Table.Rows[0]["Tokkenno"].ToString();
                }

                if (!cboConsultent.SelectedValue.ToString().Equals(cboChangeConultant.SelectedValue.ToString()))
                {
                    objAppoint.MaxSlot = objPerson.GetAll(7).Table.Rows.Count == 0 ? "20" : objPerson.GetAll(7).Table.Rows[0]["MaxSlot"].ToString();
                }
                else
                {
                    objAppoint.MaxSlot = cboConsultent.Tag.ToString();
                }
                objAppoint.AppointmentDate = dtpAppointDate.Value.Date.ToString("dd/MM/yyyy");
                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx].Cells["AppointmentID"].Value.ToString();
                objConnection.Transaction_Begin();
                if (objAppoint.Update())
                {
                    objConnection.Transaction_ComRoll();
                }
                else
                {
                    MessageBox.Show(objAppoint.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objAppoint = null;
            }
        }

        private void UpdateOPD(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {
            try
            {
                objOPD.OPDID = dgAppointment.Rows[_dgAppointRowIdx].Cells["OPDID"].Value.ToString();

                objOPD.VisitDate = dtpAppointDate.Value.ToString("dd/MM/yyyy");
                objOPD.EnteredBy = cboChangeConultant.SelectedValue.ToString();

                objConnection.Transaction_Begin();
                if (objOPD.Update())
                {
                    objConnection.Transaction_ComRoll();
                }
                else
                {
                    MessageBox.Show(objOPD.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objOPD = null;
            }
        }
        private void cboConsultent_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateAppointment();
            if (mcAppointment.SelectedDates.Count != 0)
            {
                FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
            }
            else
            {
                FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
            }
        }

        private void PopulateAppointment()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);

            try
            {
                if (mcAppointment.SelectedDates.Count != 0)
                {
                    lblSelectedDate.Text = mcAppointment.SelectedDates[0].ToString("MMM dd, yyyy");
                }
                objConnection.Connection_Open();
                objPerson.PersonID = cboConsultent.SelectedValue.ToString();
                cboConsultent.Tag = objPerson.GetAll(7).Table.Rows[0]["MaxSlot"].ToString();
                GetAppointment(objConnection);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPerson = null;
            }
        }

        private void FillFormClickLlbl(string PRNO)
        {
            clsBLDBConnection objconnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objconnection);
            DataView dvPatient;

            this.btnSave.Text = "&Update";

            try
            {
                objPatient.PRNO = PRNO;
                objconnection.Connection_Open();
                dvPatient = objPatient.GetAll(1);
                txtRPRNo.Text = PRNO;
                lblPatientID.Text = dvPatient.Table.Rows[0]["PatientID"].ToString();
                lblPRNO.Text = dvPatient.Table.Rows[0]["PRNO"].ToString();
                txtName.Text = dvPatient.Table.Rows[0]["Name"].ToString();
                if (dvPatient.Table.Rows[0]["Gender"].ToString().Equals("Male"))
                {
                    rbMale.Checked = true;
                }
                else
                {
                    rbFemale.Checked = true;
                }

                if (dvPatient.Table.Rows[0]["MaritalStatus"].ToString().Equals("S"))
                {
                    rbSingle.Checked = true;
                }
                else
                {
                    rbMarried.Checked = true;
                }

                dtpDateBirth.Value = Convert.ToDateTime(dvPatient.Table.Rows[0]["DOB"].ToString());
                txtPhone.Text = dvPatient.Table.Rows[0]["PhoneNo"].ToString(); ;
                txtCell.Text = dvPatient.Table.Rows[0]["CellNo"].ToString(); ;
                txtAddress.Text = dvPatient.Table.Rows[0]["Address"].ToString();
                cboCity.Text = dvPatient.Table.Rows[0]["CityName"].ToString(); ;
                txtAge.Text = dvPatient.Table.Rows[0]["Age"].ToString().Split(' ')[0].ToString();
                label6.Text = dvPatient.Table.Rows[0]["Age"].ToString().Split(' ')[1].ToString();
                txtEmail.Text = dvPatient.Table.Rows[0]["Email"].ToString(); ;
                if (dvPatient.Table.Rows[0]["PanelName"].ToString().Equals(""))
                {
                    cboPanel.Text = "Select";
                }
                else
                {
                    cboPanel.Text = dvPatient.Table.Rows[0]["PanelName"].ToString();
                }
                txtEmpNo.Text = dvPatient.Table.Rows[0]["EmployNo"].ToString();
                GetPatientPicture();
                GetCharges();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objconnection.Connection_Close();
                objPatient = null;
                objconnection = null;
                dvPatient = null;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.btnSave.Text.Equals("&Save"))
            {
                if (Insert())
                {
                    if (InsertOPD("W", DateTime.Now.Date.ToString("dd/MM/yyyy")))
                    {
                        InsertPicture();
                        MessageBox.Show("Patient Registration complete Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //PrintPaymentSlip();
                        FillTodaySummary();
                        ClearField();
                    }
                }
            }
            else
            {
                if (UpdateData())
                {
                    InsertPicture();
                    ClearField();
                    //InsertOPD("W", DateTime.Now.Date.ToString("dd/MM/yyyy"));
                }
            }
        }

        private void rbPaymentMode_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCash.Checked)
            {
                lblRefNo.Enabled = false;
                txtRefNo.Enabled = false;
            }
            else
            {
                lblRefNo.Enabled = true;
                txtRefNo.Enabled = true;
            }
        }

        private void FillTodaySummary()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();
            DataTable dt = new DataTable();
            DataRow dr;
            string tmpDoctorName = "";

            dt.Columns.Add("PRNo");
            dt.Columns.Add("PName");
            dt.Columns.Add("Age");
            dt.Columns.Add("Status");
            dt.Columns.Add("Video");
            dt.Columns.Add("OPID");
            //dt.Columns.Add("Download Visit");

            try
            {
                objConnection.Connection_Open();

                dv = objPatientReg.GetAll(15);

                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    dr = dt.NewRow();

                    if (!tmpDoctorName.Equals(dv.Table.Rows[i]["DocName"].ToString()))
                    {
                        dr["PRNo"] = "Consultant :";
                        dr["PName"] = dv.Table.Rows[i]["DocName"].ToString();
                        dr["Status"] = "0";//Doctor Name
                        tmpDoctorName = dv.Table.Rows[i]["DocName"].ToString();
                        i--;
                    }
                    else
                    {
                        dr["PRNo"] = dv.Table.Rows[i]["PRNo"].ToString();
                        dr["PName"] = dv.Table.Rows[i]["PName"].ToString();
                        dr["Age"] = dv.Table.Rows[i]["Age"].ToString();
                        dr["Status"] = dv.Table.Rows[i]["Status"].ToString();
                        var Video= dv.Table.Rows[i]["Video"].ToString();
                        if(Video=="0")
                        {
                            dr["Video"] = "No";
                        }
                        else
                        {
                            dr["Video"] = "Yes";
                        }
                        dr["OPID"] = dv.Table.Rows[i]["OPDID"].ToString();


                    }
                    dt.Rows.Add(dr);
                }

                dgSummary.DataSource = dt;
               
                for (int i = 0; i < dgSummary.Rows.Count; i++)
                {
                    dgSummary.Rows[i].DataGridView.Columns[6].Visible = false;


                    if (dgSummary.Rows[i].Cells["SStatus"].Value.ToString().Equals("0"))
                    {
                        dgSummary.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                        dgSummary.Rows[i].DefaultCellStyle.Font = new Font(" Microsoft Sans Serif", 9F, FontStyle.Bold);
                    }
                    else if (dgSummary.Rows[i].Cells["SStatus"].Value.ToString().Equals("W"))
                    {
                        dgSummary.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    else if (dgSummary.Rows[i].Cells["SStatus"].Value.ToString().Equals("C"))
                    {
                        dgSummary.Rows[i].DefaultCellStyle.BackColor = Color.SeaShell;
                    }
                    else if (dgSummary.Rows[i].Cells["SStatus"].Value.ToString().Equals("H"))
                    {
                        dgSummary.Rows[i].DefaultCellStyle.BackColor = Color.Azure;
                    }
                }

                dgSummary.ClearSelection();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Today Summary", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatientReg = null;
                dv.Dispose();
                dt.Dispose();
            }
        }

        private void PrintPaymentSlip()
        {
            frmRptBill objRptPaymentSlip = new frmRptBill();

            try
            {
                objRptPaymentSlip.OPDID = lblOPDID.Text;
                objRptPaymentSlip.ReportReference = "OPD_001_04";
                objRptPaymentSlip.ShowDialog();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Payment Slip", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objRptPaymentSlip.Dispose();
            }

        }

        private void FillAppointmenthCal(int Month, int Year)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dvAppointment;
            DataView dv = new DataView();
            DateTime dt;
            int Days = 0;
            //int Years = 0;
            //int Month = 0;
            string DayWeak = "";

           // mcAppointment.Dates.Clear();
            Pabo.Calendar.DateItem[] DI;

            try
            {
                objRef.ReportRefrence = "PersonID" + cboConsultent.SelectedValue.ToString();

                objConnection.Connection_Open();

                dv = objRef.GetAll(2);

                objAppoint.AppointmentDate = mcAppointment.ActiveMonth.Month.ToString("00") + "/" + mcAppointment.ActiveMonth.Year.ToString();
                objAppoint.PersonID = cboConsultent.SelectedValue.ToString();
                dvAppointment = objAppoint.GetAll(7);

                objConnection.Connection_Close();

                if (dv.Table.Rows.Count != 0)
                {
                    DayWeak += (dv.Table.Rows[0]["Description"].ToString().Equals("N") ? "Monday " : "");
                    DayWeak += dv.Table.Rows[0]["ReportTitle1"].ToString().Equals("N") ? "Tuesday " : "";
                    DayWeak += dv.Table.Rows[0]["ReportTitle2"].ToString().Equals("N") ? "Wednesday " : "";
                    DayWeak += dv.Table.Rows[0]["ReportTitle3"].ToString().Equals("N") ? "Thursday " : "";
                    DayWeak += dv.Table.Rows[0]["TreesFooter1"].ToString().Equals("N") ? "Friday " : "";
                    DayWeak += dv.Table.Rows[0]["TreesFooter2"].ToString().Equals("N") ? "Saturday " : "";
                    DayWeak += dv.Table.Rows[0]["ClientWebAddress"].ToString().Equals("N") ? "Sunday " : "";
                }
                else
                {
                    DayWeak = "Saturday Sunday ";
                }

                //if (mcAppointment.GetDateInfo().Length != 0)
                //{
                //  Month = mcAppointment.SelectedDates[0].Date.Month;
                //  Years = mcAppointment.SelectedDates[0].Date.Year;
                dt = Convert.ToDateTime(Month.ToString() + "/"+ DateTime.Now.Day.ToString("00") +"/" + Year.ToString());
                //}
                //else
                //{
                //  Month = DateTime.Now.Date.Month;
                //  Years = DateTime.Now.Date.Year;
                //  dt = Convert.ToDateTime(Month.ToString() + "/01/" + Years.ToString());
                //}

                Days = DateTime.DaysInMonth(Year, Month);
                mcAppointment.Dates.Clear();
                DI = new Pabo.Calendar.DateItem[Days];

                int i = DateTime.Now.Day, j = 0;
               
                while (i <= Days)
                {
                    //if (i - 1 < Days - 1)
                    //{
                        DI[i - 1] = new Pabo.Calendar.DateItem();
                   // }
                        if (DayWeak.Contains(dt.DayOfWeek.ToString()))
                    {
                        if (j < dvAppointment.Table.Rows.Count && dt.Date.Equals(Convert.ToDateTime(dvAppointment.Table.Rows[j]["AppointmentDate"]).Date))
                        {
                            j++;
                        }

                        DI[i - 1].Text = "Off";
                        DI[i - 1].Date = dt.Date;
                        DI[i - 1].BackColor1 = Color.Silver;
                        DI[i - 1].Enabled = false;
                        //DI[i - 1].GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
                    }
                    else
                    {
                        if (j < dvAppointment.Table.Rows.Count && dt.Date.Equals(Convert.ToDateTime(dvAppointment.Table.Rows[j]["AppointmentDate"]).Date))
                        {
                            DI[i - 1].Text = dvAppointment.Table.Rows[j]["Count"].ToString() + "/" + cboConsultent.Tag.ToString();
                            DI[i - 1].Date = Convert.ToDateTime(dvAppointment.Table.Rows[j]["AppointmentDate"]).Date;
                            j++;
                        }
                        else
                        {
                            //DI[i - 1].Text = "On";
                            DI[i - 1].Date = dt.Date;
                        }
                        DI[i - 1].BackColor1 = Color.Azure;
                        DI[i - 1].Weekend = false;
                    }

                    if (i - 1 == Days - 1)
                    {
                        dt.AddDays(-1);
                        break;
                    }

                    

                    dt = dt.AddDays(1);
                    i++;
                }
                mcAppointment.AddDateInfo(DI);
                mcAppointment.Refresh();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objRef = null;
                objAppoint = null;
                dvAppointment = null;
            }
        }

        private void mcAppointment_MonthChanged(object sender, Pabo.Calendar.MonthChangedEventArgs e)
        {
            FillAppointmenthCal(e.Month, e.Year);
        }

        private void mcAppointment_DaySelected(object sender, Pabo.Calendar.DaySelectedEventArgs e)
        {

            
            PopulateAppointment();
        }

        private void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                if (!lblPatientID.Text.Equals("") && !lblPatientID.Text.Equals("lblPatientID"))
                {
                    string VDate = "";

                    if (mcAppointment.SelectedDates.Count != 0)
                    {
                        VDate = mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy");

                    }
                    else
                    {
                        VDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    }
                    if (InsertOPD("W", VDate))
                    {
                       
                        //PrintPaymentSlip();
                        FillTodaySummary();
                        ClearField();
                        pnlRegisterSearch.Size = new Size(943, 226);
                        btnExpend.Visible = false;
                        btnCol.Visible = true;
                        ClearSearch();
                        ClearField();
                    }
                }
                else
                {
                    //MessageBox.Show("Please Select Patient", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (Insert())
                    {
                        if (InsertOPD("W", DateTime.Now.Date.ToString("dd/MM/yyyy")))
                        {
                            InsertPicture();
                            MessageBox.Show("Patient Registration complete Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //PrintPaymentSlip();
                            FillTodaySummary();
                            ClearField();
                            pnlRegisterSearch.Size = new Size(943, 226);
                            btnExpend.Visible = false;
                            btnCol.Visible = true;
                            ClearSearch();
                            ClearField();
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
  
        }

        private void btnDoctorsCashReport_Click(object sender, EventArgs e)
        {
            frmRptBill objDailyCashRpt = new frmRptBill();

            try
            {
                objDailyCashRpt.ReportReference = "OPD_001_06";
                objDailyCashRpt.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Daily Cash Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objDailyCashRpt.Dispose();
            }
        }

        private void btnExpend_Click(object sender, EventArgs e)
        {
            pnlRegisterSearch.Size = new Size(943, 226);
            btnExpend.Visible = false;
            btnCol.Visible = true;
            ClearSearch();
            ClearField();
        }

        private void btnCol_Click(object sender, EventArgs e)
        {
            pnlRegisterSearch.Size = new Size(943, 30);
            btnExpend.Visible = true;
            btnCol.Visible = false;

            ClearSearch();
            ClearField();
        }

        private void dgAppointment_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                pnlRegisterSearch.Size = new Size(943, 30);
                btnExpend.Visible = true;
                btnCol.Visible = false;

                ClearSearch();
                ClearField();

                FillFormClickLlbl(dgAppointment.Rows[e.RowIndex].Cells["APRNo"].Value.ToString());
            }
        }

        private void InsertPicture()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientPic = new clsBLPatientPicture(objConnection);

            try
            {
                if (!_Path.Equals(""))
                {

                    byte[] imagedata = ReadImage();
                    objPatientPic.SmallPicture = Convert.ToBase64String(imagedata);
                    objPatientPic.PatientID = lblPatientID.Text;
                    objPatientPic.SavedMethod= "PP";
                    //objPatientPic.Comment = "Pic :PatientPicture";

                    objConnection.Connection_Open();

                    if (objPatientPic.GetAll(5).Table.Rows.Count == 0)
                    {

                        objConnection.Transaction_Begin();
                        if (objPatientPic.Insert())
                        {
                            objConnection.Transaction_ComRoll();
                            objConnection.Connection_Close();
                        }
                        else
                        {
                            MessageBox.Show(objPatientPic.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        objConnection.Connection_Open();
                        objConnection.Transaction_Begin();
                        if (objPatientPic.UpdatePatientPuicture())
                        {
                            objConnection.Transaction_ComRoll();
                            objConnection.Connection_Close();
                        }
                        else
                        {
                            MessageBox.Show(objPatientPic.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPatientPic = null;
                objConnection = null;
            }
        }

        byte[] ReadImage()
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(_Path);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(_Path, FileMode.Open,
                                                    FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to 

            //supply number of bytes to read from file.
            //In this case we want to read entire file. 

            //So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);
            return data;
        }

        private void picPatient_Click(object sender, EventArgs e)
        {
            SelectPicture();
        }

        private void SelectPicture()
        {
            OpenFileDialog OFDPic = new OpenFileDialog();

            //OFDPic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG| All files (*.*)|*.*";
            OFDPic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";

            // Allow the user to select multiple images.
            OFDPic.Multiselect = false;
            OFDPic.Title = "My Image Browser";
            DialogResult dr = OFDPic.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                _Path = OFDPic.FileName;
                picPatient.ImageLocation = _Path;
                picPatient.Tag = _Path;
                picPatient.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else
            {
                _Path = "";
            }

            OFDPic.Dispose();
        }

        private void GetPatientPicture()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientPic = new clsBLPatientPicture(objConnection);
            byte[] buffer = null;
            DataView dvPic;

            try
            {
                objConnection.Connection_Open();
                objPatientPic.PatientID = lblPatientID.Text;
                objPatientPic.SavedMethod= "PP";//Pateint Picture

                dvPic = objPatientPic.GetAll(5);
                if (dvPic.Table.Rows.Count != 0)
                {
                    buffer = Convert.FromBase64String(dvPic.Table.Rows[0]["smallpicture"].ToString());
                    MemoryStream StrMem = new MemoryStream(buffer);
                    picPatient.Image = Image.FromStream(StrMem);
                }
                else
                {
                    picPatient.Image = null;
                }
            }
            finally
            {
                objConnection.Connection_Close();
                objPatientPic = null;
                dvPic = null;
            }
        }

        private void GetCharges()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
            DataView dv = new DataView();
            DataView dvLastVist = new DataView();

            try
            {
                objConnection.Connection_Open();
                objPersonal.PersonID = cboConsultent.SelectedValue.ToString();

                dv = objPersonal.GetAll(8);

                objPersonal.Type = lblPatientID.Text;

                dvLastVist = objPersonal.GetAll(9);

                if (dvLastVist.Table.Rows.Count != 0)
                {
                    if ((Convert.ToDouble(DateTime.Now.ToString("yyyyMMdd")) - Convert.ToDouble(dvLastVist.Table.Rows[0]["VisitDate"].ToString())) <= (dv.Table.Rows[0]["RevisitDays"].ToString().Equals("") ? 500 : Convert.ToDouble(dv.Table.Rows[0]["RevisitDays"].ToString())))
                    {
                        txtPayment.Text = dv.Table.Rows[0]["Revisitcharges"].ToString().Equals("") ? "20" : dv.Table.Rows[0]["Revisitcharges"].ToString();
                    }
                    else
                    {
                        txtPayment.Text = dv.Table.Rows[0]["charges"].ToString().Equals("") ? "20" : dv.Table.Rows[0]["charges"].ToString();
                    }
                }
                else
                {
                    txtPayment.Text = dv.Table.Rows[0]["charges"].ToString().Equals("") ? "20" : dv.Table.Rows[0]["charges"].ToString();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Get Charges", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objPersonal = null;
            }

        }

        private void miDown_Click(object sender, EventArgs e)
        {
            if (_dgAppointRowIdx != -1 && _dgAppointRowIdx < dgAppointment.Rows.Count - 1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLAppointment objAppoint = new clsBLAppointment(objConnection);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                objAppoint.MaxSlot = cboConsultent.Tag.ToString();

                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx].Cells["AppointmentID"].Value.ToString();
                objAppoint.TokkenNo = dgAppointment.Rows[_dgAppointRowIdx + 1].Cells["ATokenNo"].Value.ToString();
                objAppoint.Update();

                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx + 1].Cells["AppointmentID"].Value.ToString();
                objAppoint.TokkenNo = dgAppointment.Rows[_dgAppointRowIdx].Cells["ATokenNo"].Value.ToString();
                objAppoint.Update();

                objConnection.Transaction_ComRoll();

                GetAppointment(objConnection);

                objConnection.Connection_Close();
            }
        }

        private void miUp_Click(object sender, EventArgs e)
        {
            if (_dgAppointRowIdx != -1 && _dgAppointRowIdx >= 1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLAppointment objAppoint = new clsBLAppointment(objConnection);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                objAppoint.MaxSlot = cboConsultent.Tag.ToString();

                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx].Cells["AppointmentID"].Value.ToString();
                objAppoint.TokkenNo = dgAppointment.Rows[_dgAppointRowIdx - 1].Cells["ATokenNo"].Value.ToString();
                objAppoint.Update();

                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx - 1].Cells["AppointmentID"].Value.ToString();
                objAppoint.TokkenNo = dgAppointment.Rows[_dgAppointRowIdx].Cells["ATokenNo"].Value.ToString();
                objAppoint.Update();

                objConnection.Transaction_ComRoll();

                GetAppointment(objConnection);

                objConnection.Connection_Close();
            }
        }

        private void dgAppointment_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgAppointment.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                //dgAppointment.ContextMenuStrip = cmsAppointment;
                dgAppointment.ContextMenuStrip.Items[0].Enabled = true;
                dgAppointment.ContextMenuStrip.Items[1].Enabled = true;
                dgAppointment.ContextMenuStrip.Items[2].Enabled = true;
                dgAppointment.ContextMenuStrip.Items[3].Enabled = true;

                _dgAppointRowIdx = hi.RowIndex;
                dgAppointment.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dgAppointment.ContextMenuStrip.Items[0].Enabled = false;
                dgAppointment.ContextMenuStrip.Items[1].Enabled = false;
                dgAppointment.ContextMenuStrip.Items[2].Enabled = false;
                dgAppointment.ContextMenuStrip.Items[3].Enabled = false;

                _dgAppointRowIdx = -1;
                //dgAppointment.ContextMenuStrip = null;
            }

            hi = null;
        }

        private void dgPatientReg_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgPatientReg.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dgPatientReg.ContextMenuStrip.Items[0].Enabled = true;
                dgPatientReg.ContextMenuStrip.Items[1].Enabled = true;
                //dgPatientReg.ContextMenuStrip.Items[1].Enabled = true;
                _dgRegPatientRowIdx = hi.RowIndex;
                dgPatientReg.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dgPatientReg.ContextMenuStrip.Items[0].Enabled = false;
                dgPatientReg.ContextMenuStrip.Items[1].Enabled = false;

                //dgPatientReg.ContextMenuStrip.Items[1].Enabled = false;
                _dgRegPatientRowIdx = -1;
            }

            hi = null;
        }

        private void miAppointment_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                btnAppointment_Click(btnAppointment, e);
            }
        }

        private void btnChangeExit_Click(object sender, EventArgs e)
        {
            pnlChangeAppoint.Visible = false;
            dgAppointment.Enabled = true;
        }

        private void miChangeAppoint_Click(object sender, EventArgs e)
        {
            pnlChangeAppoint.Visible = true;
            dgAppointment.Enabled = false;

            if (mcAppointment.SelectedDates.Count != 0)
            {
                dtpAppointDate.Value = mcAppointment.SelectedDates[0].AddMinutes(1439);
            }
            else
            {
                dtpAppointDate.Value = DateTime.Now;
            }
            lblPatientID.Text = dgAppointment.Rows[_dgAppointRowIdx].Cells["ApatientID"].Value.ToString();
            cboChangeConultant.SelectedValue = cboConsultent.SelectedValue;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();
            try
            {
                objConnection.Connection_Open();
                if (!cboConsultent.SelectedValue.ToString().Equals(cboChangeConultant.SelectedValue.ToString()) || !mcAppointment.SelectedDates[0].Date.Equals(dtpAppointDate.Value.Date))
                {
                    UpdateAppointment(objConnection, objAppoint);
                    UpdateOPD(objConnection, objOPD);

                    dv = GetEamilAddress(objConnection, cboChangeConultant.SelectedValue.ToString(), lblPatientID.Text);

                    if (dv.Table.Rows.Count != 0)
                    {
                        if (!dv.Table.Rows[0]["toMessage"].ToString().Equals("") && !dv.Table.Rows[0]["fromMessage"].ToString().Equals(""))
                        {
                            string subject = "Change Appointment";
                            string Body = "Appointment shift from 25-02-2010 to 28-02-2010 With " + cboChangeConultant.Text;
                            StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, "");//@srm
                        }
                    }

                    ClearField();
                    GetAppointment(objConnection);


                    if (mcAppointment.SelectedDates.Count != 0)
                    {
                        FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
                    }
                    else
                    {
                        FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
                    }
                }
                pnlChangeAppoint.Visible = false;
                dgAppointment.Enabled = true;
                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            objConnection = null;
        }

        private void miCancelAppoint_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();

            try
            {
                objConnection.Connection_Open();
                DeleteAppointment(objConnection, objAppoint);
                //DeleteOPD(objConnection, objOPD);

                dv = GetEamilAddress(objConnection, cboConsultent.SelectedValue.ToString(), dgAppointment.Rows[_dgAppointRowIdx].Cells["APatientID"].Value.ToString());
                if (dv.Table.Rows.Count != 0)
                {
                    if (!dv.Table.Rows[0]["toMessage"].ToString().Equals("") && !dv.Table.Rows[0]["fromMessage"].ToString().Equals(""))
                    {
                        string subject = "Cancel Appointment";
                        string Body = "Cancel appointment in " + lblSelectedDate.Text + " With " + cboChangeConultant.Text;
                        StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, "");//@srm
                    }
                }

                ClearField();
                GetAppointment(objConnection);

                if (mcAppointment.SelectedDates.Count != 0)
                {
                    FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
                }
                else
                {
                    FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
                }

                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            objConnection = null;
        }

        private void DeleteAppointment(clsBLDBConnection objConnection, clsBLAppointment objAppoint)
        {
            try
            {
                objAppoint.AppointmentID = dgAppointment.Rows[_dgAppointRowIdx].Cells["AppointmentID"].Value.ToString();
                objAppoint.Delete();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objAppoint = null;
            }
        }

        private void DeleteOPD(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {
            try
            {
                objOPD.OPDID = dgAppointment.Rows[_dgAppointRowIdx].Cells["AOPDID"].Value.ToString();
                objOPD.Delete();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objOPD = null;
            }
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();
            DataView dvFromEmail = new DataView();
            try
            {
                objConnection.Connection_Open();

                objAppoint.PersonID = cboConsultent.SelectedValue.ToString();
                objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");

                objAppoint.DeleteAll();

                objOPD.EnteredBy = cboConsultent.SelectedValue.ToString();
                objOPD.VisitDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");


                dv = objOPD.GetAll(18);
                if (dv.Table.Rows.Count != 0)
                {
                    string ToEmail = "";
                    string FromEmail = "";

                    for (int i = 0; i < dv.Table.Rows.Count; i++)
                    {
                        ToEmail += dv.Table.Rows[i]["Email"].ToString();
                        ToEmail += "     ";
                    }

                    dvFromEmail = GetEamilAddress(objConnection, cboConsultent.SelectedValue.ToString(), dv.Table.Rows[0]["patientID"].ToString());
                    if (dv.Table.Rows.Count != 0)
                    {
                        FromEmail = dvFromEmail.Table.Rows[0]["fromMessage"].ToString();
                        if (!FromEmail.Equals(""))
                        {

                            if (!ToEmail.Trim().Equals("") && !FromEmail.Equals(""))
                            {
                                string subject = "Cancel all Appointment";
                                string Body = "Cancel all appointment in " + mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd") + " With " + cboConsultent.Text;
                                StartSendingMailThread(ToEmail, FromEmail, subject, Body, "");//@srm
                            }
                        }
                    }
                }

                objOPD.DeleteAll();

                ClearField();
                GetAppointment(objConnection);
                if (mcAppointment.SelectedDates.Count != 0)
                {
                    FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
                }
                else
                {
                    FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
                }

                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            objConnection = null;
        }

        private void SendMail(clsBLDBConnection objConnection, string EnteredBy, string TopN)
        {
            frmSendEmail objSendMail = new frmSendEmail();
            DataView dv = new DataView();

            try
            {
                //objSendMail.lblAttachment.Text = "Attachment : " + lblOPDID.Text + ".Doc";
                if (EnteredBy.Equals(""))
                {
                    if (TopN.Equals("1000"))
                    {
                        objSendMail.lblAttachment.Text = lblPRNO.Text + ".pdf";
                        objSendMail.txtSubject.Text = "All Visit Patient Perscription";
                    }
                    else if (TopN.Equals("10"))
                    {
                        objSendMail.lblAttachment.Text = lblPRNO.Text + ".10pdf";
                        objSendMail.txtSubject.Text = "Last 10 Visit Patient Perscription";
                    }
                    else if (TopN.Equals("5"))
                    {
                        objSendMail.lblAttachment.Text = lblPRNO.Text + ".5pdf";
                        objSendMail.txtSubject.Text = "Last 5 Visit Patient Perscription";
                    }
                }
                else
                {
                    objSendMail.lblAttachment.Text = lblOPDID.Text + ".pdf";
                    objSendMail.txtSubject.Text = "Patient Last visit Perscription";
                }
                //objSendMail.txtSubject.Text = "Patient \"" + lblPRNO.Text + "\" Perscription";
                objSendMail.txtFrom.Tag = lblPatientID.Text;
                //objSendMail.txtTo.Tag = cboConsultent.SelectedValue.ToString();

                objSendMail.ShowDialog();

                if (!objSendMail.txtTo.Tag.Equals("C"))
                {
                    PerscriptionPreview("Export", objConnection, EnteredBy, TopN);

                    dv = GetEamilAddress(objConnection, cboChangeConultant.SelectedValue.ToString(), lblPatientID.Text);
                    if (dv.Table.Rows.Count != 0)
                    {
                        if (!dv.Table.Rows[0]["toMessage"].ToString().Equals("") && !dv.Table.Rows[0]["fromMessage"].ToString().Equals(""))
                        {
                            if (EnteredBy.Equals(""))
                            {

                                string Body = "Patient visit perscription";

                                if (TopN.Equals("1000"))
                                {
                                    string subject = "All Visit Patient Perscription";
                                    StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, Application.StartupPath + "\\EmailRpt\\" + lblPRNO.Text + ".pdf");

                                }
                                else if (TopN.Equals("10"))
                                {
                                    string subject = "Last 10 Visit Patient Perscription";
                                    StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, Application.StartupPath + "\\EmailRpt\\" + lblPRNO.Text + "10.pdf");

                                }
                                else if (TopN.Equals("5"))
                                {
                                    string subject = "Last 5 Visit Patient Perscription";
                                    StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, Application.StartupPath + "\\EmailRpt\\" + lblPRNO.Text + "5.pdf");

                                }
                            }
                            else
                            {
                                string subject = "Patient Last visit Perscription";
                                string Body = "Patient Last visit Perscription";
                                StartSendingMailThread(dv.Table.Rows[0]["toMessage"].ToString(), dv.Table.Rows[0]["FromMessage"].ToString(), subject, Body, Application.StartupPath + "\\EmailRpt\\" + lblOPDID.Text + ".pdf");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SMTP server not found \n" + ex.Message, "Email Sending", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                dv = null;
                objSendMail.Dispose();
            }
        }

        private void PerscriptionPreview(string opt, clsBLDBConnection objConnection, string EnteredBy, string TopN)
        {
            frmReport objRep = new frmReport();
            clsBLType objType = new clsBLType(objConnection);
            DataView dv = new DataView();

            if (EnteredBy.Equals(""))
            {
                objType.PersonID = cboConsultent.SelectedValue.ToString();
            }
            else
            {
                objType.PersonID = EnteredBy;
            }
            dv = objType.GetAll(1);
            try
            {

                #region Get Parameter Heading

                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    switch (dv.Table.Rows[i]["Name"].ToString())
                    {
                        case "History":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strHistory = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strHistory = "History";
                                }
                                break;
                            }
                        case "Diagnosis":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strDiagnosis = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strDiagnosis = "Diagnosis";
                                }
                                break;
                            }
                        case "Signs":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strSigns = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strSigns = "Signs";
                                }
                                break;
                            }
                        case "Presenting Complaints":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strPresentingComplaints = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strPresentingComplaints = "Presenting Complaints";
                                }
                                break;
                            }
                        case "Plans":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strPlans = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strPlans = "Plans";
                                }
                                break;
                            }
                        case "Investigation":
                            {

                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    strInvestigation = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    strInvestigation = "Investigation";
                                }
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                #endregion

                if (EnteredBy.Equals(""))
                {
                    objRep.ReportReference = "OPD_001_07";
                    objRep.OPDID = "";
                    objRep.PRNo = lblPRNO.Text;
                    objRep.PatientID = lblPatientID.Text;
                }
                else
                {
                    objRep.ReportReference = "OPD_001_02";
                    objRep.OPDID = lblOPDID.Text;
                }

                objRep.History = clsSharedVariables.InItCaps(strHistory) + " : \n";
                objRep.Complaints = clsSharedVariables.InItCaps(strPresentingComplaints) + " : \n";
                objRep.Signs = clsSharedVariables.InItCaps(strSigns) + " : \n";
                objRep.Plans = clsSharedVariables.InItCaps(strPlans) + " : \n";
                objRep.Investigation = clsSharedVariables.InItCaps(strInvestigation) + " :\n";
                objRep.Diagnosis = clsSharedVariables.InItCaps(strDiagnosis) + " : \n";
                objRep.DisplayOpt = opt;
                objRep.TopN = TopN;

                if (opt.Equals("Display"))
                {
                    objRep.ShowDialog();
                }
                else
                {
                    objRep.LoadReport();
                }
            }
            catch (Exception Exc)
            {
                MessageBox.Show(Exc.Message + " Report cannot be display due to internal error.");
            }
            finally
            {
                objRep.Dispose();
                dv = null;
                objType = null;
            }
        }

        private void miEmailAllVisit_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);

                objConnection.Connection_Open();

                lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                SendMail(objConnection, "", "");

                objConnection.Connection_Close();
            }
        }

        //private void SendMail(string To,string From, string Subject,string Body)
        //{
        //  try
        //  {
        //    MailMessage mail = new MailMessage();
        //    mail.To.Add(To);
        //    mail.From = new MailAddress(From);
        //    mail.Subject = Subject;
        //    mail.Body = Body;

        //    mail.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);//587
        //    smtp.UseDefaultCredentials = false;
        //    smtp.Credentials = new System.Net.NetworkCredential("TreesSoft@gmail.com", "123");
        //    //Or your Smtp Email ID and Password
        //    smtp.Timeout = 0;
        //    smtp.EnableSsl = true;
        //    smtp.Send(mail);
        //    MessageBox.Show("Send");
        //  }
        //  catch (Exception exc)
        //  {
        //    MessageBox.Show(exc.Message);
        //  }
        //}

        private DataView GetEamilAddress(clsBLDBConnection objConnetion, string ConsultantID, string PatientID)
        {
            clsBLPersonal objPerson = new clsBLPersonal(objConnetion);
            DataView dv = new DataView();

            objPerson.name = ConsultantID;
            objPerson.Address = PatientID;

            dv = objPerson.GetAll(5);

            return dv;
        }

        private void StartSendingMailThread(string To, string From, string Subject, string Body, string AttachmentData)
        {
            Thread th = new Thread(new ParameterizedThreadStart(TimeThread));
            string msg = To + "~!@" + From + "~!@" + Subject + "~!@" + Body + "~!@" + AttachmentData;

            th.Start(msg);
        }

        private void TimeThread(object EmailInfo)
        {
            try
            {
                string[] DelimeterMain ={ "~!@" };
                string[] DelimeterMailTo ={ "    " };
                System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
                string[] info = EmailInfo.ToString().Split(DelimeterMain, StringSplitOptions.None);
                string[] ToMail = info[0].Trim().Split(DelimeterMailTo, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    //Thread.Sleep(1000);
                    MailMessage mail = new MailMessage();
                    Attachment data;

                    for (int i = 0; i < ToMail.Length; i++)
                    {
                        mail.To.Add(ToMail[i]);
                    }
                    mail.From = new MailAddress(info[1]);
                    mail.Subject = info[2];
                    mail.Body = info[3];
                    if (!info[4].Equals(""))
                    {
                        data = new Attachment(Application.StartupPath + info[4]);
                        mail.Attachments.Add(data);
                    }

                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient(clsSharedVariables.SMTP, Convert.ToInt16(clsSharedVariables.Port));//587
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(clsSharedVariables.SMTPEmail, clsSharedVariables.Password);
                    //Or your Smtp Email ID and Password
                    //smtp.Timeout = 0;
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                    MessageBox.Show("Email Sending Successfully");
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = true;
                //Thread.Sleep(1000);
            }
            catch (ThreadAbortException tae)
            {
                MessageBox.Show(tae.Message);
            }
        }

        private void miAllVisit_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);
                try
                {
                    objConnection.Connection_Open();

                    lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                    lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                    SendMail(objConnection, "", "1000");
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                finally
                {
                    objConnection.Connection_Close();
                    objConnection = null;
                    objOPD = null;
                }
            }
        }

        private void miLastTenVisit_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);

                try
                {
                    objConnection.Connection_Open();

                    lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                    lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                    SendMail(objConnection, "", "10");

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                finally
                {
                    objConnection.Connection_Close();
                    objConnection = null;
                    objOPD = null;
                }
            }
        }

        private void miLastFiveVisit_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);

                try
                {
                    objConnection.Connection_Open();

                    lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                    lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                    SendMail(objConnection, "", "5");
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                finally
                {
                    objConnection.Connection_Close();
                    objOPD = null;
                    objConnection = null;
                }
            }
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            frmAttendentSetting objAttSetting = new frmAttendentSetting();

            objAttSetting.PersonID = cboConsultent.SelectedValue.ToString();
            objAttSetting.PersonName = cboConsultent.Text;
            objAttSetting.ShowDialog();

            if (mcAppointment.SelectedDates.Count != 0)
            {
                FillAppointmenthCal(mcAppointment.SelectedDates[0].Date.Month, mcAppointment.SelectedDates[0].Date.Year);
            }
            else
            {
                FillAppointmenthCal(DateTime.Now.Month, DateTime.Now.Year);
            }

            objAttSetting.Dispose();
        }

        private void FormatCal()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            DataView dv = new DataView();
            DateTime dt;
            int Days = 0;
            int Years = 0;
            int Month = 0;
            string DayWeak = "";

            try
            {
                objRef.ReportRefrence = "PersonID" + cboConsultent.SelectedValue.ToString();

                objConnection.Connection_Open();
                dv = objRef.GetAll(2);
                objConnection.Connection_Close();

                if (dv.Table.Rows.Count != 0)
                {
                    DayWeak += (dv.Table.Rows[0]["Description"].ToString().Equals("N") ? "Monday " : "");
                    DayWeak += dv.Table.Rows[0]["ReportTitle1"].ToString().Equals("N") ? "Tuesday " : "";
                    DayWeak += dv.Table.Rows[0]["ReportTitle2"].ToString().Equals("N") ? "Wednesday " : "";
                    DayWeak += dv.Table.Rows[0]["ReportTitle3"].ToString().Equals("N") ? "Thursday " : "";
                    DayWeak += dv.Table.Rows[0]["TreesFooter1"].ToString().Equals("N") ? "Friday " : "";
                    DayWeak += dv.Table.Rows[0]["TreesFooter2"].ToString().Equals("N") ? "Saturday " : "";
                    DayWeak += dv.Table.Rows[0]["ClientWebAddress"].ToString().Equals("N") ? "Sunday " : "";
                }
                else
                {
                    DayWeak = "Saturday Sunday ";
                }
                if (mcAppointment.GetDateInfo().Length != 0)
                {
                    Month = mcAppointment.SelectedDates[0].Date.Month;
                    Years = mcAppointment.SelectedDates[0].Date.Year;
                    dt = Convert.ToDateTime(Month.ToString() + "/01/" + Years.ToString());
                }
                else
                {
                    Month = DateTime.Now.Date.Month;
                    Years = DateTime.Now.Date.Year;
                    dt = Convert.ToDateTime(Month.ToString() + "/01/" + Years.ToString());
                }

                Days = DateTime.DaysInMonth(Years, Month);
                mcAppointment.Dates.Clear();
                Pabo.Calendar.DateItem[] DI;
                DI = new Pabo.Calendar.DateItem[Days];

                int i = 1;
                while (i <= Days)
                {
                    DI[i - 1] = new Pabo.Calendar.DateItem();
                    if (DayWeak.Contains(dt.DayOfWeek.ToString()))
                    {
                        //DI[i - 1].Text = "Off";
                        DI[i - 1].Date = dt.Date;
                        DI[i - 1].BackColor1 = Color.Silver;
                        DI[i - 1].Enabled = false;
                    }
                    else
                    {
                        //DI[i - 1].Text = "On";
                        DI[i - 1].Date = dt.Date;
                        DI[i - 1].BackColor1 = Color.Azure;
                    }
                    dt = dt.AddDays(1);
                    i++;
                }
                mcAppointment.AddDateInfo(DI);
                mcAppointment.Refresh();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objRef = null;
            }
        }

        private void miLastVisit_Click(object sender, EventArgs e)
        {
            if (_dgRegPatientRowIdx != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);
                DataView dv = new DataView();

                objConnection.Connection_Open();
                objOPD.PRNO = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();

                dv = objOPD.GetAll(4);

                if (dv.Table.Rows.Count != 0)
                {
                    lblPRNO.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PRNO"].Value.ToString();
                    lblPatientID.Text = dgPatientReg.Rows[_dgRegPatientRowIdx].Cells["PatientID"].Value.ToString();
                    lblOPDID.Text = dv.Table.Rows[0]["OPDID"].ToString();
                    SendMail(objConnection, dv.Table.Rows[0]["Enteredby"].ToString(), "1000");
                }
                objConnection.Connection_Close();
            }
        }

        private void ReminderEmail(string AppointmentDate, string subject, string Body)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dv = new DataView();
            DataView dvFromEmail = new DataView();
            try
            {
                objConnection.Connection_Open();

                objAppoint.PersonID = cboConsultent.SelectedValue.ToString();
                objAppoint.AppointmentDate = Convert.ToDateTime(AppointmentDate).Date.ToString("yyyy-MM-dd");

                dv = objAppoint.GetAll(8);
                if (dv.Table.Rows.Count != 0)
                {
                    string ToEmail = "";
                    string FromEmail = "";

                    for (int i = 0; i < dv.Table.Rows.Count; i++)
                    {
                        ToEmail += dv.Table.Rows[i]["Email"].ToString();
                        ToEmail += "     ";
                    }

                    dvFromEmail = GetEamilAddress(objConnection, cboConsultent.SelectedValue.ToString(), dv.Table.Rows[0]["patientID"].ToString());
                    if (dv.Table.Rows.Count != 0)
                    {
                        FromEmail = dvFromEmail.Table.Rows[0]["fromMessage"].ToString();
                        if (!FromEmail.Equals(""))
                        {
                            if (!ToEmail.Trim().Equals("") && !FromEmail.Equals(""))
                            {
                                StartSendingMailThread(ToEmail, FromEmail, subject, Body, "");//@srm
                            }
                        }
                    }
                }
                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objAppoint = null;
                objConnection = null;
                dv = null;
                dvFromEmail = null;
            }
        }

        private void btnTomorrow_Click(object sender, EventArgs e)
        {
            string Subject = "Tommorow Appointment Reminder";
            string Body = "Tommorow Appointment Reminder";
            ReminderEmail(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), Subject, Body);
        }

        private void btnNextTomorrow_Click(object sender, EventArgs e)
        {
            string Subject = "Next to Tommorow Appointment Reminder";
            string Body = "Next to Tommorow Appointment Reminder";
            ReminderEmail(DateTime.Now.AddDays(2).ToString("yyyy-MM-dd"), Subject, Body);
        }

        private void txtAge_TextChanged(object sender, EventArgs e)
        {

            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtAge.Text) && Convert.ToInt16(txtAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtAge.Text.Trim())) + "";
                dtpDateBirth.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtAge.Text = "";
            }
        }

        private void dtpDateBirth_ValueChanged(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpDateBirth.Value.Year;
            txtAge.Text = Age.ToString();
        }

        private void dgAppointment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgAppointment.Columns[e.ColumnIndex].Name.Equals("VitalSign"))
            {
                ClearVitalSigns();
                pnlVitalSigns.Visible = true;
                txtVSPRNO.Text = dgAppointment.Rows[e.RowIndex].Cells["APRNo"].Value.ToString();
                txtVSPRNO.Tag = dgAppointment.Rows[e.RowIndex].Cells["OPDID"].Value.ToString();
                FillVitalSign(txtVSPRNO.Tag.ToString());
            }
        }

        private void btnVitalSignClose_Click(object sender, EventArgs e)
        {
            pnlVitalSigns.Visible = false;
            ClearVitalSigns();
        }

        private void ClearVitalSigns()
        {
            txtBP.Text = "";
            txtTemp.Text = "";
            txtPulse.Text = "";
            txtHeartRate.Text = "";
            txtRespiratoryRate.Text = "";
            txtBMI.Text = "";
            txtHeadCircum.Text = "";
            txtWeight.Text = "";
            txtHeight.Text = "";
            txtBSA.Text = "";
            txtVSPRNO.Text = "";
        }

        private void txtValidation_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = "\b0123456789.";
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void txtWeight_Leave(object sender, EventArgs e)
        {
            CalculateBMI();
            CalculateBSA();

            if (sender != null)
            {
                TextBox txt = (TextBox)sender;

                if (txt.Name.Equals("txtHeight"))
                {
                    string[] str;
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        str = txtHeight.Text.Split('.');
                        if (str.Length == 2)
                        {
                            str[0] = Convert.ToString(Convert.ToInt16(str[0]) + (Convert.ToInt16(str[1]) / 12));
                            str[1] = Convert.ToString(Convert.ToInt16(str[1]) % 12);

                            txtHeight.Text = str[0] + "." + str[1];
                        }
                    }
                }
            }
        }

        private void CalculateBMI()
        {
            Validation objValid = new Validation();
            Double Height = 0;
            Double Weight = 0;
            Double BMI = 0;

            if (!txtWeight.Text.Trim().Equals("") && !txtHeight.Text.Trim().Equals(""))
            {
                if (objValid.IsNumber(txtWeight.Text.Trim()) && objValid.IsNumber(txtHeight.Text.Trim()))
                {
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        Height = Convert.ToDouble(txtHeight.Text) * 12;//Convert in Inches
                    }
                    else
                    {
                        Height = Convert.ToDouble(txtHeight.Text);
                    }
                    if (!cboWeightUnit.Text.Equals("Pounds"))
                    {
                        Weight = Convert.ToDouble(txtWeight.Text) * 2.020462262;//convert in pound
                    }
                    else
                    {
                        Weight = Convert.ToDouble(txtWeight.Text);
                    }

                    BMI = (Weight * 703) / (Height * Height);
                    txtBMI.Text = BMI.ToString(".00");

                    if (BMI < 18.5)
                    {
                        txtBMIStatus.Text = "Underweight";
                    }
                    else if (BMI > 18.5 && BMI < 24.9)
                    {
                        txtBMIStatus.Text = "Normal";
                    }
                    else if (BMI > 24.9 && BMI < 29.9)
                    {
                        txtBMIStatus.Text = "Overweight";
                    }
                    else if (BMI > 29.9)
                    {
                        txtBMIStatus.Text = "Above Obese";
                    }
                }
            }
            else
            {
                txtBMI.Text = "";
                txtBSA.Text = "";
                txtBMIStatus.Text = "";
            }
        }

        private void CalculateBSA()
        {
            Validation objValid = new Validation();
            Double Height = 0;
            Double Weight = 0;
            Double BSA = 0;

            if (!txtWeight.Text.Trim().Equals("") && !txtHeight.Text.Trim().Equals(""))
            {
                if (objValid.IsNumber(txtWeight.Text.Trim()) && objValid.IsNumber(txtHeight.Text.Trim()))
                {
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        Height = Convert.ToDouble(txtHeight.Text) * 12;//Convert in Inches
                    }
                    else
                    {
                        Height = Convert.ToDouble(txtHeight.Text);
                    }
                    if (!cboWeightUnit.Text.Equals("Pounds"))
                    {
                        Weight = Convert.ToDouble(txtWeight.Text) * 2.020462262;//convert in pound
                    }
                    else
                    {
                        Weight = Convert.ToDouble(txtWeight.Text);
                    }

                    BSA = Math.Sqrt(((Weight * Height) / 3131));
                    txtBSA.Text = BSA.ToString(".00");
                }
                else
                {
                    txtBMI.Text = "";
                    txtBSA.Text = "";
                    txtBMIStatus.Text = "";
                }
            }
        }

        private bool UpdateOPD()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            try
            {
                objOPD.OPDID = txtVSPRNO.Tag.ToString();
                objOPD = SetBLValuesVS(objOPD);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objOPD.Update())
                {
                    objConnection.Transaction_ComRoll();
                    return true;
                    //MessageBox.Show("Patient Informatoin updated Successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objOPD.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objOPD = null;
                objConnection = null;
            }
            return false;
        }

        private clsBLOPD SetBLValuesVS(clsBLOPD objOPD)
        {
            if (txtBP.Text.Trim().Length != 1)
            {
                objOPD.BP = txtBP.Text.Trim() + " " + lblBPUnit.Text;
            }
            else { objOPD.BP = ""; }

            if (!txtPulse.Text.Equals(""))
            {
                objOPD.Pulse = txtPulse.Text.Trim() + " " + lblPulseUnit.Text;
            }
            else { objOPD.Pulse = ""; }

            if (!txtHeartRate.Text.Equals(""))
            {
                objOPD.HeartRate= txtHeartRate.Text.Trim() ;
            }
            else { objOPD.HeartRate= ""; }

            if (!txtRespiratoryRate.Text.Equals(""))
            {
                objOPD.RespiratoryRate = txtRespiratoryRate.Text.Trim() ;
            }
            else { objOPD.RespiratoryRate = ""; }

            if (!txtTemp.Text.Equals(""))
            {
                objOPD.Temprature = txtTemp.Text.Trim() + " " + cboTempUnit.Text;
            }
            else { objOPD.Temprature = ""; }

            if (!txtWeight.Text.Equals(""))
            {
                objOPD.Weight = txtWeight.Text.Trim() + " " + cboWeightUnit.Text;
            }
            else { objOPD.Weight = ""; }

            if (!txtHeight.Text.Equals(""))
            {
                objOPD.Height = txtHeight.Text.Trim() + " " + cboHeightUnit.Text;
            }
            else { objOPD.Height = ""; }

            if (!txtHeadCircum.Text.Equals(""))
            {
                objOPD.HeadCircumferences = txtHeadCircum.Text.Trim() + " " + lblHeadCircumUnit.Text;
            }
            objOPD.BMI = txtBMI.Text;
            objOPD.BSA = txtBSA.Text;

            return objOPD;
        }

        private void btnVSSave_Click(object sender, EventArgs e)
        {
            UpdateOPD();
            btnVitalSignClose_Click(btnVitalSignClose, e);
        }

        private void FillVitalSign(string OPDID)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();

            try
            {
                objConnection.Connection_Open();
                objOPD.OPDID = OPDID;

                dv = objOPD.GetAll(19);

                if (dv.Table.Rows.Count != 0)
                {
                    txtBP.Text = dv.Table.Rows[0]["BP"].ToString().Split(' ')[0].ToString().Trim();
                    if (!dv.Table.Rows[0]["Temprature"].ToString().Equals(""))
                    {
                        txtTemp.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[0].ToString().Trim();
                        cboTempUnit.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[1].ToString().Trim();
                    }

                    if (!dv.Table.Rows[0]["pulse"].ToString().Equals(""))
                    {
                        txtPulse.Text = dv.Table.Rows[0]["pulse"].ToString().Split(' ')[0].ToString().Trim();
                    }
                    if (!dv.Table.Rows[0]["HeadCircumferences"].ToString().Equals(""))
                    {
                        txtHeadCircum.Text = dv.Table.Rows[0]["HeadCircumferences"].ToString().Split(' ')[0].ToString().Trim();
                    }
                    if (!dv.Table.Rows[0]["Weight"].ToString().Equals(""))
                    {
                        txtWeight.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[0].ToString().Trim();
                        cboWeightUnit.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[1].ToString().Trim();
                    }
                    if (!dv.Table.Rows[0]["Height"].ToString().Equals(""))
                    {
                        txtHeight.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[0].ToString().Trim();
                        cboHeightUnit.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[1].ToString().Trim();
                    }
                    if (!dv.Table.Rows[0]["HeartRate"].ToString().Equals(""))
                    {
                        txtHeartRate.Text = dv.Table.Rows[0]["HeartRate"].ToString().Split(' ')[0].ToString().Trim();
                    }
                    if (!dv.Table.Rows[0]["RespiratoryRate"].ToString().Equals(""))
                    {
                        txtRespiratoryRate.Text = dv.Table.Rows[0]["RespiratoryRate"].ToString().Split(' ')[0].ToString().Trim();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Fill Vital Sign", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objOPD = null;
            }
        }

        private void txtSearchPRNO_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void dgPatientReg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            //Add City Button
            var n = new frmAddCity();
            n.Show();
        }

        private void dgAppointment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rbMale_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbFemale_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cboCity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCell_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtSearchPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSearchName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBSA_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBMI_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBP_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void cboHeightUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboWeightUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboTempUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtHeight_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtWeight_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPulse_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTemp_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHeadCircum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtVSPRNO_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void lblPatientID_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblPatientSearchHeading_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblTo_Click(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSearchName_Click(object sender, EventArgs e)
        {

        }

        private void cmsPatientReg_Opening(object sender, CancelEventArgs e)
        {

        }

        private void miEmailAllVisit_Click_1(object sender, EventArgs e)
        {

        }

        private void lblFrom_Click(object sender, EventArgs e)
        {

        }

        private void lblSearchPRNO_Click(object sender, EventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlPaymentMode_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtRefNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblRefNo_Click(object sender, EventArgs e)
        {

        }

        private void txtPayment_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtEmpNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void pnlMaritalStatus_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rbMarried_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbSingle_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblPatientInfo_Click(object sender, EventArgs e)
        {

        }

        private void lblPanel_Click(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void lblAge_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblAddress_Click(object sender, EventArgs e)
        {

        }

        private void lblGender_Click(object sender, EventArgs e)
        {

        }

        private void lblPhone_Click(object sender, EventArgs e)
        {

        }

        private void lblCell_Click(object sender, EventArgs e)
        {

        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void lblEmpNo_Click(object sender, EventArgs e)
        {

        }

        private void lblEmail_Click(object sender, EventArgs e)
        {

        }

        private void lblDOB_Click(object sender, EventArgs e)
        {

        }

        private void lblCity_Click(object sender, EventArgs e)
        {

        }

        private void lblMaritalStatus_Click(object sender, EventArgs e)
        {

        }

        private void lblPayment_Click(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlChangeAppoint_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dtpAppointDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cboChangeConultant_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pnlRegisterSearch_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmsAppointment_Opening(object sender, CancelEventArgs e)
        {

        }

        private void lblSelectedDate_Click(object sender, EventArgs e)
        {

        }

        private void lblAppointment_Click(object sender, EventArgs e)
        {

        }

        private void lblConsultent_Click(object sender, EventArgs e)
        {

        }

        private void lblPRNO_Click(object sender, EventArgs e)
        {

        }

        private void dgSummary_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pnlSummry_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblConsulted_Click(object sender, EventArgs e)
        {

        }

        private void lblHold_Click(object sender, EventArgs e)
        {

        }

        private void lblWait_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void lblOPDID_Click(object sender, EventArgs e)
        {

        }

        private void pnlVitalSigns_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtRespiratoryRate_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHeartRate_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblRespiratoryRate_Click(object sender, EventArgs e)
        {

        }

        private void lblHeartRate_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void lblBSAUnit_Click(object sender, EventArgs e)
        {

        }

        private void txtBMIStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblBMIUnit_Click(object sender, EventArgs e)
        {

        }

        private void lblVitalSigns_Click(object sender, EventArgs e)
        {

        }

        private void lblHeadCircumUnit_Click(object sender, EventArgs e)
        {

        }

        private void lblPulseUnit_Click(object sender, EventArgs e)
        {

        }
        private void lblBPUnit_Click(object sender, EventArgs e)
        {

        }
        private void lblHeight_Click(object sender, EventArgs e)
        {

        }
        private void lblPulse_Click(object sender, EventArgs e)
        {

        }
        private void lblBSA_Click(object sender, EventArgs e)
        {

        }
        private void lblBMI_Click(object sender, EventArgs e)
        {

        }

        private void lblBMIStatus_Click(object sender, EventArgs e)
        {

        }

        private void lblWeight_Click(object sender, EventArgs e)
        {

        }

        private void lblBP_Click(object sender, EventArgs e)
        {

        }

        private void lblHeadCircum_Click(object sender, EventArgs e)
        {

        }

        private void lblTemp_Click(object sender, EventArgs e)
        {

        }

        private void rbSCell_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbSPhone_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtRPRNo_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void RemoveMask_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.txtRPRNo.Mask = "";
            this.txtSearchPRNO.Mask ="";
            this.txtVSPRNO.Mask = "";
            this.RemoveMask.Visible = false;
            this.ShowMask.Visible = true;
        }

        private void ShowMask_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.RemoveMask.Visible = true;
            this.ShowMask.Visible = false;
            this.txtRPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtVSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.txtSearchPRNO.Mask = "";
            this.button9.Visible = false;
            this.button12.Visible = true;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.button9.Visible = true; ;
            this.button12.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.txtRPRNo.Mask = "";
            this.button3.Visible = false;
            this.button2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.txtRPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.button3.Visible = true;
            this.button2.Visible = false;
        }

        private void mcAppointment_DayClick(object sender, DayClickEventArgs e)
        {
           var selectedDate=Convert.ToDateTime( e.Date);
            mcAppointment.SelectDate(selectedDate);
            PopulateAppointment();

        }

        private void dgSummary_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex>0)
            {
                string OPDId = dgSummary.Rows[e.RowIndex].Cells[6].Value.ToString();
                ClearVitalSigns();
                pnlVitalSigns.Visible = true;
                txtVSPRNO.Text = dgSummary.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtVSPRNO.Tag = OPDId.ToString();
                FillVitalSign(OPDId.ToString());
            }
            else
            {
                ClearVitalSigns();
            }
           
        }
    }
}