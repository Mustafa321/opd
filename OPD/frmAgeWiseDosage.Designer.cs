namespace OPD
{
    partial class frmAgeWiseDosage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAgeWiseDosageHeading = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgAgewiseDosage = new System.Windows.Forms.DataGridView();
            this.MedicineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaxAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMedName = new System.Windows.Forms.Label();
            this.txtMedName = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAgewiseDosage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtMedName);
            this.panel2.Controls.Add(this.lblMedName);
            this.panel2.Controls.Add(this.dgAgewiseDosage);
            this.panel2.Controls.Add(this.lblAgeWiseDosageHeading);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(492, 273);
            this.panel2.TabIndex = 5;
            this.panel2.Tag = "med";
            // 
            // lblAgeWiseDosageHeading
            // 
            this.lblAgeWiseDosageHeading.AutoSize = true;
            this.lblAgeWiseDosageHeading.Location = new System.Drawing.Point(198, 5);
            this.lblAgeWiseDosageHeading.Name = "lblAgeWiseDosageHeading";
            this.lblAgeWiseDosageHeading.Size = new System.Drawing.Size(93, 13);
            this.lblAgeWiseDosageHeading.TabIndex = 0;
            this.lblAgeWiseDosageHeading.Tag = "transdisplay";
            this.lblAgeWiseDosageHeading.Text = "Age Wise Dosage";
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::OPD.Properties.Resources.Cancel_15;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.Location = new System.Drawing.Point(471, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(18, 18);
            this.btnClose.TabIndex = 4;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgAgewiseDosage
            // 
            this.dgAgewiseDosage.AllowUserToAddRows = false;
            this.dgAgewiseDosage.AllowUserToDeleteRows = false;
            this.dgAgewiseDosage.AllowUserToOrderColumns = true;
            this.dgAgewiseDosage.AllowUserToResizeColumns = false;
            this.dgAgewiseDosage.AllowUserToResizeRows = false;
            this.dgAgewiseDosage.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAgewiseDosage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgAgewiseDosage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAgewiseDosage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MedicineID,
            this.MinAge,
            this.MaxAge,
            this.Weight,
            this.ADUnit});
            this.dgAgewiseDosage.Location = new System.Drawing.Point(1, 43);
            this.dgAgewiseDosage.MultiSelect = false;
            this.dgAgewiseDosage.Name = "dgAgewiseDosage";
            this.dgAgewiseDosage.ReadOnly = true;
            this.dgAgewiseDosage.RowHeadersVisible = false;
            this.dgAgewiseDosage.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgAgewiseDosage.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgAgewiseDosage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAgewiseDosage.Size = new System.Drawing.Size(488, 227);
            this.dgAgewiseDosage.TabIndex = 96;
            // 
            // MedicineID
            // 
            this.MedicineID.DataPropertyName = "MedicineID";
            this.MedicineID.HeaderText = "ADMedicineID";
            this.MedicineID.Name = "MedicineID";
            this.MedicineID.ReadOnly = true;
            this.MedicineID.Visible = false;
            // 
            // MinAge
            // 
            this.MinAge.DataPropertyName = "MinAge";
            this.MinAge.FillWeight = 25F;
            this.MinAge.HeaderText = "Min Age";
            this.MinAge.Name = "MinAge";
            this.MinAge.ReadOnly = true;
            // 
            // MaxAge
            // 
            this.MaxAge.DataPropertyName = "MaxAge";
            this.MaxAge.FillWeight = 25F;
            this.MaxAge.HeaderText = "Max Age";
            this.MaxAge.Name = "MaxAge";
            this.MaxAge.ReadOnly = true;
            // 
            // Weight
            // 
            this.Weight.DataPropertyName = "Weight";
            this.Weight.FillWeight = 25F;
            this.Weight.HeaderText = "Weight";
            this.Weight.Name = "Weight";
            this.Weight.ReadOnly = true;
            // 
            // ADUnit
            // 
            this.ADUnit.DataPropertyName = "UnitName";
            this.ADUnit.FillWeight = 25F;
            this.ADUnit.HeaderText = "Unit";
            this.ADUnit.Name = "ADUnit";
            this.ADUnit.ReadOnly = true;
            // 
            // lblMedName
            // 
            this.lblMedName.AutoSize = true;
            this.lblMedName.Location = new System.Drawing.Point(4, 24);
            this.lblMedName.Name = "lblMedName";
            this.lblMedName.Size = new System.Drawing.Size(44, 13);
            this.lblMedName.TabIndex = 97;
            this.lblMedName.Text = "Name : ";
            // 
            // txtMedName
            // 
            this.txtMedName.Location = new System.Drawing.Point(54, 21);
            this.txtMedName.Name = "txtMedName";
            this.txtMedName.ReadOnly = true;
            this.txtMedName.Size = new System.Drawing.Size(435, 20);
            this.txtMedName.TabIndex = 98;
            // 
            // frmAgeWiseDosage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 273);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmAgeWiseDosage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Age Wise Dosage";
            this.Load += new System.EventHandler(this.frmAgeWiseDosage_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAgewiseDosage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAgeWiseDosageHeading;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgAgewiseDosage;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedicineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaxAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADUnit;
        private System.Windows.Forms.TextBox txtMedName;
        private System.Windows.Forms.Label lblMedName;
    }
}