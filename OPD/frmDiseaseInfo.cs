using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
  public partial class frmDiseaseInfo : Form
  {
    private frmOPD objOPD = null;
    private string _DiseaseRefID = "";

    public string DiseaseRefID
    {
      get { return _DiseaseRefID; }
      set { _DiseaseRefID = value; }
    }
    
    public frmDiseaseInfo()
    {
      InitializeComponent();
    }

    public frmDiseaseInfo(frmOPD objOPD)
    {
      InitializeComponent();
      this.objOPD = objOPD;
    }

    private void frmDiseaseInfo_Load(object sender, EventArgs e)
    {
      SComponents objComp = new SComponents();
      clsBLDBConnection objConnection = new clsBLDBConnection();
      objConnection.Connection_Open();
      
      objConnection.Connection_Close();

      objComp.ApplyStyleToControls(this);

      objComp = null;
      objConnection = null;
    }

    private void miExit_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}