using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net.Configuration;
using OPD_BL;
using System.Threading;
using System.Diagnostics;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.Devices;
using Microsoft.VisualBasic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;


namespace OPD
{
    public partial class frmOPD : Form
    {
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        private static readonly Random getrandom = new Random();
        private frmLogin Login_Form = null;
        private frmLoginLic LoginLic = null;
        private int stop = 0;
        private int Voicestop = 0;
        private string NewOPDID = null;

        public frmOPD()
        {
            InitializeComponent();
        }

        public frmOPD(frmLogin Frm)
        {
            InitializeComponent();
            Login_Form = Frm;
        }

        public frmOPD(frmLoginLic Frm)
        {
            InitializeComponent();
            LoginLic = Frm;
        }
        ~frmOPD()
        {
            //this.Previous_Form.Show();
        }

        private void frmOPD_Load(object sender, EventArgs e)
        {
            //search
            //this.txtSearchPRNO.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            //this.txtCSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            //this.txtHSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            //this.txtSearchByPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
           // Register
            this.txtRPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtCPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtHPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtCPRNo.ReadOnly = true;
            this.txtHPRNo.ReadOnly = true;
            SComponentsMain objComp = new SComponentsMain(738, 1024);
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            clsBLType objType = new clsBLType(objConnection);//Heading of itenmDatagrid
            clsBLPanel objPnl = new clsBLPanel(objConnection);
           
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);
            DataView dvCity, dv;

            //if (clsSharedVariables.LoginID.ToUpper().Equals("TREES"))
            //{
                miNewAccount.Enabled = true;
            //}
            this.Text += " ( " + clsSharedVariables.UserName + " )";

            cboHeightUnit.SelectedIndex = 0;
            cboTempUnit.SelectedIndex = 0;
            cboWeightUnit.SelectedIndex = 0;

            dtpPatientDOB.MaxDate = DateTime.Now;
            dtpFrom.MaxDate = DateTime.Now.Date;
            dtpTo.Value = DateTime.Now.Date;
            dtpHFrom.Value = DateTime.Now.Date.AddMonths(-1);
            dtpHTo.MaxDate = DateTime.Now.Date;
            dtpCTo.MaxDate = DateTime.Now.Date;
            dtpCFrom.Value = DateTime.Now.Date;
            dtpFollowUpDate.MinDate = DateTime.Now;
            GetNotificationCount();
            InitializeTimer();
            try
            {
                objConnection.Connection_Open();
                dvCity = objPatientReg.GetAll(3);
                objComp.FillComboBox(cboPatientCity, dvCity, "Name", "CityID", false);
                objComp.FillComboBox(cboHCity, dvCity, "Name", "CityID", false);
                objComp.FillComboBox(cboCCity, dvCity, "Name", "CityID", false);
                if (objPatientReg.GetAll(10).Table.Rows.Count != 0)
                {
                    lblCity.Text = objPatientReg.GetAll(10).Table.Rows[0]["Name"].ToString();
                }

                dvCity = objPnl.GetAll(1);
                //objComp.FillComboBox(cboRPanel, dvCity, "PanelName", "PanelID", true);
                objComp.FillComboBox(cboCPanel, dvCity, "PanelName", "PanelID", true);
                objComp.FillComboBox(cboHPanel, dvCity, "PanelName", "PanelID", true);

                cboPatientCity.Text = lblCity.Text;
                cboCCity.Text = lblCity.Text;
                cboHCity.Text = lblCity.Text;

                objType.PersonID = clsSharedVariables.UserID;
                dv = objType.GetAll(1);
                HeadingDG(dv);

                dv = objType.GetAll(2);
                AssignDGTypeID(dv);
                GetSummary(objConnection);
                btnNewPatient.Focus();

                btnAppointControl.Tag = "1";
                dgMedicineLang();

                objPerson.PersonID = clsSharedVariables.UserID;
                btnAppointment.Tag = objPerson.GetAll(7).Table.Rows[0]["MaxSlot"].ToString();
                mcAppointment.SelectDate(DateTime.Now.Date);
                mcAppointment.MinDate = DateTime.Now.AddDays(-1);
                GetAppointment(objConnection);

                miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
                miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";

                //label12.BackColor = Color.FromArgb(199, 235, 110);
                //label11.BackColor = Color.FromArgb(138, 216, 55);
                //label9.BackColor = Color.FromArgb(229, 229, 229);


                label12.BackColor = Color.Lavender;
                label11.BackColor = Color.Gainsboro;
                label9.BackColor = Color.White;

                LinkLabel.Link ll = new LinkLabel.Link();
                LinkLabelLinkClickedEventArgs ee = new LinkLabelLinkClickedEventArgs(ll);
                lklTdaySummary_LinkClicked(lklTdaySummary, ee);
                ll = null;
                ee = null;
                dgRegisterPatient.ClearSelection();
                StartThread();
                lblVersion.Text = clsSharedVariables.Version;

                pnlSerachByPRNo.Visible = true;
                pnlSerachByPRNo.BringToFront();
                txtSearchByPRNo.SelectionStart = 0;
                txtSearchByPRNo.Focus();
                txtRPRNo.ReadOnly = txtCPRNo.ReadOnly = txtRPRNo.ReadOnly = clsSharedVariables.AutoPRNO;

                this.Size = Screen.PrimaryScreen.WorkingArea.Size;
                this.Top = 0;
                this.Left = 0;

                objComp.ApplyStyleToControls(this);

                pnlAppointment.Size = panel3.Size;
                pnlAppointment.Location = new Point(pnlInfo.Location.X + 2, pnlInfo.Location.Y + 2);//816, 243

                miStatusFill(miHistoryStatus);
                miStatusFill(miCompStatus);
                miStatusFill(miSignStatus);
                miStatusFill(miPlanStatus);
                miStatusFill(miDiagnosisStatus);
                miStatusFill(miInvestStatus);
                //Hide show Panel
              
                pnlAppointment.Visible = clsSharedVariables.Appointment;
                panel3.Visible = clsSharedVariables.WaitingQueue;
                pnlWaitingList.Visible = clsSharedVariables.WaitingQueue;
                pnlNotes.Visible = clsSharedVariables.Notes;
                pnlAllerAR.Visible = clsSharedVariables.Allergies;
                panel16.Visible = clsSharedVariables.VitalSign;
                pnlMedHist.Visible = clsSharedVariables.Medication;
                pnlProcAdmission.Visible = clsSharedVariables.AdmissionAndProcedure;
                pnlSelPatientHead.Visible = clsSharedVariables.PatientDocuments;
                pnlSelPatientDetail.Visible = clsSharedVariables.PatientDocuments;
                pnlTotalVisit.Visible = clsSharedVariables.PatientVisits;
                pnlVisit.Visible = clsSharedVariables.PatientVisits;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objComp = null;
                objPatientReg = null;
                objPerson = null;
                objType = null;
                dv = null;
                dvCity = null;
                objPnl = null;
            }
        }

        public void dgMedicineLang()
        {
            if (clsSharedVariables.Language.Equals("Eng"))
            {
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgMedicine.Columns["MedDosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
        }
        public async void GetNotificationCount()
        {
            int res = 0;
            try
            {
              var DoctorId = System.Configuration.ConfigurationSettings.AppSettings["DoctorId"].ToString();


                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                   
                    // New code:
                    HttpResponseMessage response = await client.GetAsync("api/Chat/GetUnReadMessagesCount?DoctorId=" + DoctorId);
                    if (response.IsSuccessStatusCode)
                    {
                        res = await response.Content.ReadAsAsync<int>();
                       

                    }
                   
                }

            }
            catch (Exception ex)
            {
                res = 0;
            }
            this.Notifications.Text = "Notifications (" + res.ToString() + ")";
            
        }
     
        public  void InitializeTimer()
        {
            System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer
            {
                Interval = 300000
            };
            timer1.Enabled = true;
           
            timer1.Tick += new System.EventHandler(OnTimerEvent);
        }
        public  void OnTimerEvent(object source, EventArgs e)
        {
            GetNotificationCount();
        }


        #region Button Clicks

        //Registration Tab
        private void btnNewPatient_Click(object sender, EventArgs e)
        {
            NewPatient();
            pnlRPatient.Enabled = true;
            ClearField();
            ClearSearchField();
            miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
            miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";
            lblMode.Text = "Mode : New Patient ";
        }

        private void btnRegisterPatient_Click(object sender, EventArgs e)
        {
            UnSelectedPatient();
            miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
            miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";
            lblMode.Text = "Mode : Search ";
            pnlRegisterSearch.Enabled = true;
            lblRegCount.Text = "Patient Found : " + dgRegisterPatient.Rows.Count.ToString();
        }

        private void btnPatientSave_Click(object sender, EventArgs e)
        {
            if (btnPatientSave.Text.Equals("Save"))
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                if (InsertPatient())
                {
                    InsertOPD("W", "");
                    objConnection.Connection_Open();
                    InsertAppointment(objConnection);
                    SelectedPatient();
                    CreateLBWaiting();
                    GetAppointment(objConnection);

                    objConnection.Connection_Close();
                }
                miHoldSave.Enabled = true;
                miConsultedSave.Enabled = true;
                lblSelect.Tag = "0";
            }
            else if (btnPatientSave.Text.Equals("Update"))
            {
                UpdatePatient();
            }
        }

        private void btnPatientSearch_Click(object sender, EventArgs e)
        {
            ClearField();
            pnlRPatient.Enabled = false;
            PatientSearch();
            pnlRegister.Size = new Size(panel1.Size.Width, pnlMain.Bottom);
            //pnlLegend.Visible = false;
            lblSelect.Tag = "1";
            lblRegCount.Text = "Patient Found : " + dgRegisterPatient.Rows.Count.ToString();
        }

        //Menue Item Click

        #region Menue Item Rename Click

        private void miComplaintsRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgComplaints, "ComplaintsName");
        }

        private void miHistoryRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgHistory, "HistoryName");
        }

        private void miSignsRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgSigns, "SignsName");
        }

        private void miDiagnosisRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgDiagnosis, "DiagnosisName");
        }

        private void miPlansRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgPlans, "PlansName");
        }

        private void miInvestigationRename_Click(object sender, EventArgs e)
        {
            UpdatePreferenceDescription(dgInvestigation, "InvestigatioName");
        }

        private void miPicture_Click(object sender, EventArgs e)
        {
            frmPicture objPic = new frmPicture(lblPatientID.Text, this);
            if (!miRegistration.Enabled)
            {
                objPic.PatientName = txtPatientName.Text;
            }
            else if (!miHold.Enabled)
            {
                objPic.PatientName = txtHName.Text;
            }
            else if (!miConsulted.Enabled)
            {
                objPic.PatientName = txtCName.Text;
            }
            objPic.Show();
            this.Hide();
        }

        private void miInstructions_Click(object sender, EventArgs e)
        {
            frmType objType = new frmType(this);
            objType.Show();

            this.Hide();
            //dgMedicineLang();
        }

        private void miCancel_Click(object sender, EventArgs e)
        {
            //frmLogin objLogin = new frmLogin();
            //objLogin.Show();
            //objLogin.Activate();
            this.Close();
            this.Dispose();
        }

        private void miComplaintDelete_Click(object sender, EventArgs e)
        {
            if (miComplaintDelete.Tag != null && Convert.ToInt16(miComplaintDelete.Tag) != (dgComplaints.Rows.Count - 1))
            {
                //dgComplaints.Rows.RemoveAt(Convert.ToInt16(miComplaintDelete.Tag));
                dgComplaints.Rows[Convert.ToInt16(miComplaintDelete.Tag)].Visible = false;
                dgComplaints.Rows[Convert.ToInt16(miComplaintDelete.Tag)].Cells[0].Value = null;
                dgComplaints.Rows[Convert.ToInt16(miComplaintDelete.Tag)].Cells["DelComp"].Value = "Y";
            }
        }

        private void miHistoryDelete_Click(object sender, EventArgs e)
        {
            if (miHistoryDelete.Tag != null && Convert.ToInt16(miHistoryDelete.Tag) != (dgHistory.Rows.Count - 1))
            {
                //dgHistory.Rows.RemoveAt(Convert.ToInt16(miHistoryDelete.Tag));
                dgHistory.Rows[Convert.ToInt16(miHistoryDelete.Tag)].Visible = false;
                dgHistory.Rows[Convert.ToInt16(miHistoryDelete.Tag)].Cells[0].Value = null;
                dgHistory.Rows[Convert.ToInt16(miHistoryDelete.Tag)].Cells["DelHist"].Value = "Y";
            }
        }

        private void miDiagnosisDelete_Click(object sender, EventArgs e)
        {
            if (miDiagnosisDelete.Tag != null && Convert.ToInt16(miDiagnosisDelete.Tag) != (dgDiagnosis.Rows.Count - 1))
            {
                //dgDiagnosis.Rows.RemoveAt(Convert.ToInt16(miDiagnosisDelete.Tag));
                dgDiagnosis.Rows[Convert.ToInt16(miDiagnosisDelete.Tag)].Visible = false;
                dgDiagnosis.Rows[Convert.ToInt16(miDiagnosisDelete.Tag)].Cells[0].Value = null;
                dgDiagnosis.Rows[Convert.ToInt16(miDiagnosisDelete.Tag)].Cells["DelDiagnosis"].Value = "Y";
            }
        }

        private void miPlansDelete_Click(object sender, EventArgs e)
        {
            if (miPlansDelete.Tag != null && Convert.ToInt16(miPlansDelete.Tag) != (dgPlans.Rows.Count - 1))
            {
                //dgPlans.Rows.RemoveAt(Convert.ToInt16(miPlansDelete.Tag));
                dgPlans.Rows[Convert.ToInt16(miPlansDelete.Tag)].Visible = false;
                dgPlans.Rows[Convert.ToInt16(miPlansDelete.Tag)].Cells[0].Value = null;
                dgPlans.Rows[Convert.ToInt16(miPlansDelete.Tag)].Cells["DelPlan"].Value = "Y";
            }
        }

        private void miSignsDelete_Click(object sender, EventArgs e)
        {
            if (miSignsDelete.Tag != null && Convert.ToInt16(miSignsDelete.Tag) != (dgSigns.Rows.Count - 1))
            {
                //dgSigns.Rows.RemoveAt(Convert.ToInt16(miSignsDelete.Tag));
                dgSigns.Rows[Convert.ToInt16(miSignsDelete.Tag)].Visible = false;
                dgSigns.Rows[Convert.ToInt16(miSignsDelete.Tag)].Cells[0].Value = null;
                dgSigns.Rows[Convert.ToInt16(miSignsDelete.Tag)].Cells["DelSign"].Value = "Y";
            }
        }

        private void miInvestigationDelete_Click(object sender, EventArgs e)
        {
            if (miInvestigationDelete.Tag != null && Convert.ToInt16(miInvestigationDelete.Tag) != (dgInvestigation.Rows.Count - 1))
            {
                //dgInvestigation.Rows.RemoveAt(Convert.ToInt16(miInvestigationDelete.Tag));
                dgInvestigation.Rows[Convert.ToInt16(miInvestigationDelete.Tag)].Visible = false;
                dgInvestigation.Rows[Convert.ToInt16(miInvestigationDelete.Tag)].Cells[0].Value = null;
                dgInvestigation.Rows[Convert.ToInt16(miInvestigationDelete.Tag)].Cells["DelInvest"].Value = "Y";
            }
        }

        private void DeleteOPDIDDetailONPrefID(string tmpPrefID)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDDetail objOPDDetail = new clsBLOPDDetail(objConnection);
            try
            {
                objOPDDetail.OPDID = lblOPDID.Text;
                objOPDDetail.PrefID = tmpPrefID;

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                if (objOPDDetail.Delete())
                {
                    objConnection.Transaction_ComRoll();
                    objConnection.Connection_Close();
                }
                else
                {
                    MessageBox.Show(objOPDDetail.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objOPDDetail = null;
                objConnection = null;
            }
        }

        private void miChangePassword_Click(object sender, EventArgs e)
        {
            frmChangePassword objChangePassword = new frmChangePassword();

            objChangePassword.ShowDialog();

            objChangePassword.Dispose();
        }

        private void btnCUpdate_Click(object sender, EventArgs e)
        {
            UpdatePatientReg();
        }

        //private void miPrintPreview_Click(object sender, EventArgs e)
        //{
        //    PrintPerscription();
        //}

        #endregion

        private void btnListPnl_Click(object sender, EventArgs e)
        {
            pnlDGList.Visible = false;
        }

        private void btnMedicine_Click(object sender, EventArgs e)
        {
            frmMedicineSelection objMedSelect = new frmMedicineSelection(this);
            int AddRow = -1;

            //dgMedicine.Rows.Clear();
            for (int i = 0; i < dgMedicine.Rows.Count; i++)
            {
                AddRow = objMedSelect.dgSelectedList.Rows.Add();

                objMedSelect.dgSelectedList.Rows[AddRow].Cells["MedID"].Value = clsSharedVariables.InItCaps(dgMedicine.Rows[i].Cells["MedID"].Value.ToString());
                objMedSelect.dgSelectedList.Rows[AddRow].Cells["MedicineName"].Value = clsSharedVariables.InItCaps(dgMedicine.Rows[i].Cells["MedName"].Value.ToString());
                objMedSelect.dgSelectedList.Rows[AddRow].Cells["MedicineDosage"].Value = dgMedicine.Rows[i].Cells["MedDosage"].Value.ToString();
                objMedSelect.dgSelectedList.Rows[AddRow].Cells["Mode"].Value = "U";
            }
            this.Hide();
            objMedSelect.PatientID = lblPatientID.Text;
            objMedSelect.Show();
            //objMedSelect.Dispose();
        }

        public void AssignSelectedMed(DataGridView dgMedSel)
        {
            int AddRow = -1;
            //if (Convert.ToString(dvMedSel.dgSelectedList.Tag).Equals("1"))
            {
                while (dgMedicine.Rows.Count > 0)
                {
                    dgMedicine.Rows.RemoveAt(0);
                }

                for (int i = 0; i < dgMedSel.Rows.Count; i++)
                {
                    AddRow = SelectMedicine(dgMedSel.Rows[i].Cells["MedicineName"].Value.ToString());
                    if (AddRow == -1)
                    {
                        AddRow = dgMedicine.Rows.Add();
                    }
                    dgMedicine.Rows[AddRow].Cells["MedID"].Value = clsSharedVariables.InItCaps(dgMedSel.Rows[i].Cells["MedID"].Value.ToString());
                    dgMedicine.Rows[AddRow].Cells["Medname"].Value = clsSharedVariables.InItCaps(dgMedSel.Rows[i].Cells["MedicineName"].Value.ToString());
                    if (clsSharedVariables.Language.Equals("Eng"))
                    {
                        dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = clsSharedVariables.InItCaps(dgMedSel.Rows[i].Cells["MedicineDosage"].Value.ToString());
                    }
                    else if (clsSharedVariables.Language.Equals("Urdu"))
                    {
                        dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = dgMedSel.Rows[i].Cells["MedicineDosage"].Value.ToString();
                    }
                    else if (clsSharedVariables.Language.Equals("Farsi"))
                    {
                        dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = dgMedSel.Rows[i].Cells["MedicineDosage"].Value.ToString();
                    }

                    dgMedicine.Rows[AddRow].Cells["Mode"].Value = dgMedSel.Rows[i].Cells["Mode"].Value.
ToString();
                    dgMedicine.Rows[AddRow].DefaultCellStyle.BackColor = dgMedSel.Rows[i].DefaultCellStyle.BackColor;

                }
            }
        }

        #region Save button click(signs,History,Diagnosis,Complaints,Investigation and Plans)

        private void btnHistory_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgHistory, "PrefIDHistory", "HistoryName");
            FillListGridView(dgHistory.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgHistory.Columns["HistoryName"].HeaderText;
        }

        private void btnSigns_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgSigns, "PrefIDSigns", "SignsName");
            FillListGridView(dgSigns.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgSigns.Columns["SignsName"].HeaderText;
        }

        private void btnPresentComplaint_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgComplaints, "PrefIDComplaints", "ComplaintsName");
            FillListGridView(dgComplaints.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgComplaints.Columns["ComplaintsName"].HeaderText;
        }

        private void btnDiagnosis_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgDiagnosis, "PrefIDDiagnosis", "DiagnosisName");
            FillListGridView(dgDiagnosis.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgDiagnosis.Columns["DiagnosisName"].HeaderText;
        }

        private void btnPlans_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgPlans, "PrefIDPlans", "PlansName");
            FillListGridView(dgPlans.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgPlans.Columns["PlansName"].HeaderText;
        }

        private void btnInvestigation_Click(object sender, EventArgs e)
        {
            CheckForPrefrence(ref dgInvestigation, "PrefIDInvestigation", "InvestigatioName");
            FillListGridView(dgInvestigation.Tag.ToString());
            dgList.Columns["Pname"].HeaderText = dgInvestigation.Columns["InvestigatioName"].HeaderText;
        }

        #endregion

        private void btnHSearch_Click(object sender, EventArgs e)
        {
            btnHoldUpdate.Enabled = false;
            ResizeHold();
            ClearHoldPatient();
            ClearGridRows(dgHold);
            pnlHPatient.Enabled = false;
            HoldSearchCriteria('H', dgHold);
            btnHSearch.Focus();
            //miPrintPreview.Enabled = false;
            //dgHold.ClearSelection();
            lblHoldCount.Text = "Patient Found : " + dgHold.Rows.Count.ToString();
        }

        private void btnConsultedSearch_Click(object sender, EventArgs e)
        {
            ClearConsultedPatient();
            btnCUpdate.Enabled = false;
            pnlCPatient.Enabled = false;
            ResizeConsulted();
            ConsultedSearchCriteria('C', dgConsulted);
            btnConsultedSearch.Focus();
            //miPrintPreview.Enabled = false;
            //dgConsulted.ClearSelection();b
            lblConsultCount.Text = "Patient Found : " + dgConsulted.Rows.Count.ToString();


        }

        private void btnHoldUpdate_Click(object sender, EventArgs e)
        {
            UpdatePatientReg();
        }

        #endregion

        #region Leave Event

        private void dtpPatientDOB_Leave(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpPatientDOB.Value.Year;
            txtPatientAge.Text = Age.ToString();
        }

        private void txtPatientAge_Leave(object sender, EventArgs e)
        {
            TextBox txt = new TextBox();
            txt = (TextBox)sender;
            if (txt != null)
            {
                txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
                txt.SelectionStart = 0;
            }

            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtPatientAge.Text) && Convert.ToInt16(txtPatientAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtPatientAge.Text.Trim())) + "";
                dtpPatientDOB.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtPatientAge.Text = "";
            }
        }

        private void txtHAge_Leave(object sender, EventArgs e)
        {
            if (txtHAge.Text != null)
            {
                txtHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
                txtHAge.SelectionStart = 0;
            }

            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtHAge.Text) && Convert.ToInt16(txtHAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtHAge.Text.Trim())) + "";
                dtpHDOB.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtHAge.Text = "";
            }
        }

        private void dtpHDOB_Leave(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpHDOB.Value.Year;
            txtHAge.Text = Age.ToString();
        }

        private void txtWeight_Leave(object sender, EventArgs e)
        {
            CalculateBMI();
            CalculateBSA();
            txtVitalSign_Leave(sender, e);

            if (sender != null)
            {
                TextBox txt = (TextBox)sender;

                if (txt.Name.Equals("txtHeight"))
                {
                    string[] str;
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        str = txtHeight.Text.Split('.');
                        if (str.Length == 2)
                        {
                            str[0] = Convert.ToString(Convert.ToInt16(str[0]) + (Convert.ToInt16(str[1]) / 12));
                            str[1] = Convert.ToString(Convert.ToInt16(str[1]) % 12);

                            txtHeight.Text = str[0] + "." + str[1];
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert and Update Function

        private bool InsertPatient()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            try
            {
                objPatient = SetBLValues(objPatient);
                if (!objPatient.PRNO.Equals("Duplicate"))
                {
                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    if (objPatient.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                        lblPatientID.Text = objPatient.GetAll(6).Table.Rows[0][0].ToString();
                        objConnection.Connection_Close();
                    }
                    else
                    {
                        MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                objPatient = null;
                objConnection = null;
            }
            return true;
        }

        private void UpdatePatient()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                objPatient.PatientID = this.lblPatientID.Text;
                clsBLDBConnection objConnection1 = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection1);

                objOPD.PatientID = this.lblPatientID.Text;

                objPatient = SetBLValues(objPatient);

                if (!objPatient.PRNO.Equals("Duplicate"))
                {
                    //    MessageBox.Show("PRNo Aleady exist","Duplicate",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    //    return ;
                    //}
                    //else if (objPatient.PRNO.Equals("Duplicate"))
                    //{
                    //    MessageBox.Show("Please En","Duplicate",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    //    return ;
                    //}
                    //else
                    //{
                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    if (objPatient.Update())
                    {
                        objConnection1.Connection_Open();
                        objOPD.PRNO = txtRPRNo.Text;
                        objOPD.UpdateOPD();
                        objConnection1.Connection_Close();
                        objConnection.Transaction_ComRoll();
                        //PatientSearch();
                        objConnection.Connection_Close();
                        //ClearPatientRegField();
                        MessageBox.Show("Patient Registration Update successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPatient = null;
                objConnection = null;
            }
        }

        private void InsertPreference(clsBLDBConnection objConnection, clsBLPreference objPreference)
        {
            try
            {
                objPreference = SetBLValues(objPreference);
                objConnection.Transaction_Begin();
                if (objPreference.Insert())
                {
                    //objConnection.Transaction_ComRoll();
                    //MessageBox.Show("Preference type is added Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPreference.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Transaction_ComRoll();
            }
        }

        private void UpdatePreference(clsBLDBConnection objConnection, clsBLPreference objPreference)
        {
            try
            {
                objPreference = SetBLValues(objPreference);
                objConnection.Transaction_Begin();
                if (objPreference.Update())
                {
                    //MessageBox.Show("Preference type is Updated Successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPreference.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Transaction_ComRoll();
            }

        }

        private void UpdatePreferenceDescription(DataGridView dg, string colName)
        {

            string Heading = "";
            if (dg.Tag != null)
            {
                Heading = Microsoft.VisualBasic.Interaction.InputBox("Enter Heading", "Heading Text", dg.Columns[0].HeaderText, 400, 400);
                if (!Heading.Equals(""))
                {
                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    clsBLPreference objPreference = new clsBLPreference(objConnection);
                    try
                    {
                        objPreference.TypeID = dg.Tag.ToString();
                        objPreference.PersonID = clsSharedVariables.UserID;
                        objPreference.Description = clsSharedVariables.InItCaps(Heading);
                        objConnection.Connection_Open();
                        objConnection.Transaction_Begin();
                        if (objPreference.UpdateDescription())
                        {
                            objConnection.Transaction_ComRoll();
                            objConnection.Connection_Close();
                            dg.Columns[colName].HeaderText = clsSharedVariables.InItCaps(Heading);
                            dgList.Columns["PName"].HeaderText = clsSharedVariables.InItCaps(Heading);
                            //MessageBox.Show("Heading is Updated Successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(objPreference.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        objConnection = null;
                        objPreference = null;
                    }
                }
            }
        }

        private bool InsertOPD(string status, string VDate)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            try
            {
                objOPD.Status = status;
                if (VDate.Equals(""))
                {
                    objOPD.VisitDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    objOPD.TempVisitDate = DateTime.Now.ToString("yyyy/MM/dd");
                }
                else
                {
                    objOPD.VisitDate = VDate;
                    objOPD.TempVisitDate =Convert.ToDateTime(VDate).ToString("yyyy/MM/dd");
                }
                objOPD = SetBLValues(objOPD);
                if (status.Equals("W"))
                {
                    objOPD.Payment = clsSharedVariables.Payment;
                }
                objConnection.Connection_Open();

                if (objOPD.GetAll(11).Table.Rows.Count == 0)
                {
                    objConnection.Transaction_Begin();
                    if (objOPD.Insert())
                    {
                        objConnection.Transaction_ComRoll();

                        //MessageBox.Show("Patient Informatoin Added Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        lblOPDID.Text = objOPD.GetAll(2).Table.Rows[0][0].ToString();
                        //if (lblOPDID.Text.Equals("14200"))
                        //{
                        //  try
                        //  {
                        //    objConnection.Ora_Connection_Open();
                        //    if (objOPD.GetAllOra(1).Table.Rows.Count == 0)
                        //    {
                        //      objConnection.Ora_Transaction_Begin();

                        //      objOPD.InsertHospital();

                        //      objConnection.Ora_Transaction_ComRoll();
                        //    }
                        //  }
                        //  catch { }
                        //  finally { objConnection.Ora_Connection_Close(); }

                        //}

                        return true;
                    }
                    else
                    {
                        MessageBox.Show(objOPD.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objOPD = null;
                objConnection = null;
            }
            return false;
        }

        private bool InsertOPDDetail()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDDetail objOPDDetail = new clsBLOPDDetail(objConnection);

            try
            {
                objOPDDetail.OPDID = lblOPDID.Text;
                objConnection.Connection_Open();
                //objOPDDetail.Delete();

                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "1", dgHistory, "HistoryName", "DelHist");
                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "2", dgDiagnosis, "DiagnosisName", "DelDiagnosis");
                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "3", dgComplaints, "ComplaintsName", "DelComp");
                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "4", dgSigns, "SignsName", "DelSign");
                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "5", dgPlans, "PlansName", "DelPlan");
                InsertOPDDetailConcatinate(objConnection, objOPDDetail, "6", dgInvestigation, "InvestigatioName", "DelInvest");

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objOPDDetail = null;
                objConnection = null;
            }
            return false;
        }

        private void InsertOPDDetailConcatinate(clsBLDBConnection objConnection, clsBLOPDDetail objOPDDetail, string TypeID, DataGridView dg, string cellType, string DelColName)
        {
            clsBLPreference objPref = new clsBLPreference(objConnection);
            DataView dvPref = new DataView();

            objPref.TypeID = TypeID;


            DataView dv = objOPDDetail.GetAll(4);

            for (int i = 0; i < dg.Rows.Count - 1; i++)
            {
                if (dg.Rows[i].Cells[cellType].Value != null)
                {
                    objPref.Name = dg.Rows[i].Cells[cellType].Value.ToString();
                }
                dvPref = objPref.GetAll(5);

                if (dvPref.Table.Rows.Count != 0)
                {
                    if (dg.Rows[i].Cells[DelColName].Value == null)
                    {
                        if (dg.Rows[i].Cells[cellType].Value != null)
                        {
                            objOPDDetail.PrefID = dvPref.Table.Rows[0]["PrefId"].ToString();
                            objOPDDetail.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

                            objConnection.Transaction_Begin();
                            dv.RowFilter = "PRefID=" + objOPDDetail.PrefID;

                            if (dv.Count == 0)
                            {
                                objOPDDetail.Insert();
                                UpdatePrefNumTime(objConnection, objPref, objOPDDetail.PrefID);
                            }
                            else
                            {
                                objOPDDetail.UpdateStatus();
                            }

                            objConnection.Transaction_ComRoll();
                        }
                    }
                    else if (dg.Rows[i].Cells[DelColName].Value.ToString().Equals("Y"))
                    {
                        if (dg.Rows[i].Cells[1].Value != null)
                        {
                            objOPDDetail.PrefID = dg.Rows[i].Cells[1].Value.ToString();
                            objConnection.Transaction_Begin();
                            objOPDDetail.DeleteFromPrefID();
                            objConnection.Transaction_ComRoll();
                        }
                    }
                }
            }
        }

        private void InsertOPDMedicine(int RowIndex)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);

            try
            {
                objOPDMed.OPDID = lblOPDID.Text;
                objOPDMed = SetBLValues(objOPDMed, RowIndex);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objOPDMed.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    objConnection.Connection_Close();
                }
                else
                {
                    MessageBox.Show(objOPDMed.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objOPDMed = null;
                objConnection = null;
            }
        }

        private bool UpdateOPD(string status)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            try
            {
                objOPD.OPDID = lblOPDID.Text;
                objOPD.Status = status;
                objOPD = SetBLValues(objOPD);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objOPD.Update())
                {
                    objConnection.Transaction_ComRoll();
                    return true;
                    //MessageBox.Show("Patient Informatoin updated Successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objOPD.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objOPD = null;
                objConnection = null;
            }
            return false;
        }

        private void UpdateOPDMed()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);

            try
            {

                objOPDMed.OPDID = lblOPDID.Text;
                objConnection.Connection_Open();
                objOPDMed.Delete();
                for (int i = 0; i < dgMedicine.Rows.Count; i++)
                {
                    objOPDMed = SetBLValues(objOPDMed, i);
                    objConnection.Transaction_Begin();
                    if (objOPDMed.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                    }
                    else
                    {
                        MessageBox.Show(objOPDMed.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                objConnection.Connection_Close();
            }
            finally
            {
                objConnection = null;
                objOPDMed = null;
            }

        }

        private void UpdatePatientReg()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                objPatient.PatientID = this.lblPatientID.Text;
                objConnection.Connection_Open();


                if (lblSelect.Text.Equals("1"))
                {
                    objPatient = SetBLValuesUpdate(objPatient);
                }
                else if (lblSelect.Text.Equals("2"))
                {
                    objPatient = SetBLValuesCUpdate(objPatient);
                }
                if (!objPatient.PRNO.Equals("Duplicate"))
                {
                    objConnection.Transaction_Begin();
                    if (objPatient.Update())
                    {
                        objConnection.Transaction_ComRoll();
                        //SearchCriteria();
                        objConnection.Connection_Close();

                        //ClearPatientRegField();
                        MessageBox.Show("Patient Registration updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPatient = null;
                objConnection = null;
            }
        }

        private void UpdateMedicineNumTime(int i)//0 For Inrement 1 for Decrement
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMed = new clsBLMedicine(objConnection);

            try
            {
                objMed.MedicineID = dgMedicine.Rows[i].Cells["MedID"].Value.ToString();
                objMed.NumTime = "Numtime + 1";
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objMed.Update())
                { }
                else
                {
                    MessageBox.Show(objMed.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                objConnection.Transaction_ComRoll();
                objConnection.Connection_Close();
                objMed = null;
                objConnection = null;
            }
        }

        private void UpdatePrefNumTime(clsBLDBConnection objConnection, clsBLPreference objPref, string PrefID)
        {
            try
            {
                objPref.PrefID = PrefID;
                objPref.NumTime = "Numtime + 1";
                if (objPref.Update())
                { }
                else
                {
                    MessageBox.Show(objPref.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        #endregion

        #region Set BL Values Functions

        private clsBLPatientReg SetBLValues(clsBLPatientReg objPatient)
        {
            var UserName = txtPatientName.Text.Replace(" ","_") + GenerateRendomNumber(1, 1000);
            var Password = GenerateRendomNumber(1, 100000);
            objPatient.Name = clsSharedVariables.InItCaps(txtPatientName.Text.Trim());
            //if (btnPatientSave.Text.Equals("Save"))
            if (!clsSharedVariables.AutoPRNO)
            {
                int Flag = 0;
                //if (!txtRPRNo.Text.Trim().Equals("-  -"))
                //{
                Flag = DuplicatePRNO();
                if (Flag == 0)
                {
                    MessageBox.Show("" + clsSharedVariables.PRNOHeading + " already assign", "" + clsSharedVariables.PRNOHeading + " Duplication", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objPatient.PRNO = "Duplicate";
                    return objPatient;
                }
                else
                {
                    txtPRNO.Text = objPatient.PRNO = txtRPRNo.Text;
                }

                //}
                //else
                //{
                //    objPatient.PRNO = "Duplicate";
                //    return objPatient;
                //}
            }
            else
            {
                if (btnPatientSave.Text.Equals("Save"))
                {
                    int Flag = 0;
                    while (Flag == 0)
                    {
                        Flag = DuplicatePRNOAuto();
                    }
                    txtPRNO.Text = objPatient.PRNO = txtRPRNo.Text;
                }
            }

            if (rbPatientMale.Checked)
            {
                objPatient.Gender = "M";
            }
            else
            {
                objPatient.Gender = "F";
            }

            objPatient.MRNo = objPatient.PRNO.Replace("-", string.Empty);
            objPatient.DOB = dtpPatientDOB.Value.Date.ToString("dd/MM/yyyy");
            objPatient.TempDOB= dtpPatientDOB.Value.Date.ToShortDateString();
            objPatient.PhoneNo = txtPatientPhone.Text.Trim();
            objPatient.CellNo = txtPatientCell.Text.Trim();
            objPatient.Address = clsSharedVariables.InItCaps(txtPatientAddress.Text.Trim());
            objPatient.EnteredBy = clsSharedVariables.UserID;
            objPatient.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPatient.CityID = clsSharedVariables.InItCaps(cboPatientCity.SelectedValue.ToString());
            objPatient.Email = txtRegEmail.Text.Trim();
            objPatient.UserName = UserName;
            objPatient.Password = Password.ToString();
            objPatient.SynchedToServer = "0";
            return objPatient;
        }
        
       public static int GenerateRendomNumber(int min, int max)
        {
            lock (getrandom) // synchronize
            {
                return getrandom.Next(min, max);
            }
        }

        private clsBLPreference SetBLValues(clsBLPreference objPreference)
        {
            objPreference.Active = "1";
            objPreference.EnteredBy = clsSharedVariables.UserID;
            objPreference.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            objPreference.ClientID = clsSharedVariables.ClientID;
            objPreference.PersonID = clsSharedVariables.UserID;

            return objPreference;
        }

        private clsBLOPD SetBLValues(clsBLOPD objOPD)
        {
            objOPD.PRNO = txtPRNO.Text;
            objOPD.PatientID = lblPatientID.Text;
            objOPD.PresentingComplaints = concatinateType(dgComplaints, "ComplaintsName");
            objOPD.Diagnosis = concatinateType(dgDiagnosis, "DiagnosisName");
            objOPD.Signs = concatinateType(dgSigns, "SignsName");
            objOPD.Video = VideoOption.Checked ? "1" : "0";
            objOPD.MRNO = txtPRNO.Text.Replace("-", string.Empty);
            if (txtBP.Text.Trim().Length != 1)
            {
                objOPD.BP = txtBP.Text.Trim() + " " + lblBPUnit.Text;
            }
            else { objOPD.BP = ""; }

            if (!txtPulse.Text.Equals(""))
            {
                objOPD.Pulse = txtPulse.Text.Trim() + " " + lblPulseUnit.Text;
            }
            else { objOPD.Pulse = ""; }

            if (!txtHeartRate.Text.Equals(""))
            {
                objOPD.HeartRate = txtHeartRate.Text.Trim();
            }
            else { objOPD.HeartRate = ""; }
            if (!txtRespiratoryRate.Text.Equals(""))
            {
                objOPD.RespiratoryRate = txtRespiratoryRate.Text.Trim();
            }
            else { objOPD.RespiratoryRate = ""; }

            if (!txtTemp.Text.Equals(""))
            {
                objOPD.Temprature = txtTemp.Text.Trim() + " " + cboTempUnit.Text;
            }
            else { objOPD.Temprature = ""; }

            if (!txtWeight.Text.Equals(""))
            {
                objOPD.Weight = txtWeight.Text.Trim() + " " + cboWeightUnit.Text;
            }
            else { objOPD.Weight = ""; }

            if (!txtHeight.Text.Equals(""))
            {
                objOPD.Height = txtHeight.Text.Trim() + " " + cboHeightUnit.Text;
            }
            else { objOPD.Height = ""; }

            objOPD.Active = "1";
            //MessageBox.Show(dgPlans.Rows[1].Cells[0].Value.ToString());
            objOPD.DisposalPlan = concatinateType(dgPlans, "PlansName");
            //MessageBox.Show(dgPlans.Rows[1].Cells[0].Value.ToString());
            objOPD.Investigation = concatinateType(dgInvestigation, "InvestigatioName");
           
            
            //if (!txtHeadCircum.Text.Equals(""))
            //{
            //    objOPD.HeadCircumferences = txtHeadCircum.Text.Trim() + " " + lblHeadCircumUnit.Text;
            //}
            
            
            objOPD.Instruction = "";
            foreach (Control c1 in pnlInstructions.Controls)
            {
                CheckBox chb = (CheckBox)c1;
                if (chb.Checked)
                {
                    if (clsSharedVariables.Language.Equals("Eng"))
                    {
                        objOPD.Instruction += clsSharedVariables.InItCaps(chb.Text) + "    ";
                    }
                    else if (clsSharedVariables.Language.Equals("Urdu"))
                    {
                        objOPD.Instruction += chb.Text.Replace(@"\", "\\") + "    ";
                    }
                    else if (clsSharedVariables.Language.Equals("Farsi"))
                    {
                        objOPD.Instruction += chb.Text.Replace(@"\", "\\") + "    ";
                    }
                }
            }
            objOPD.Instruction = objOPD.Instruction.Trim();
            objOPD.History = concatinateType(dgHistory, "HistoryName");
            objOPD.EnteredBy = clsSharedVariables.UserID;
            objOPD.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");

            if (dtpFollowUpDate.Tag.ToString().Equals("1"))
            {
                objOPD.FollowUpDate = dtpFollowUpDate.Value.ToString("dd/MM/yyyy");
            }
            if (!txtPayment.Text.Trim().Equals(""))
            {
                objOPD.Payment = txtPayment.Text.Trim();
            }
            else
            {
                objOPD.Payment = "0";
            }
            objOPD.BMI = txtBMI.Text;
            objOPD.BSA = txtBSA.Text;
            objOPD.Notes = txtNotes.Text.Trim();

            return objOPD;
        }

        private clsBLOPDMedicine SetBLValues(clsBLOPDMedicine objOPDMed, int RowIndex)
        {
            if (!dgMedicine.Rows[RowIndex].Cells["MedID"].Value.ToString().Equals(""))
            {
                objOPDMed.MedID = dgMedicine.Rows[RowIndex].Cells["MedID"].Value.ToString();
            }
            objOPDMed.MedName = clsSharedVariables.InItCaps(dgMedicine.Rows[RowIndex].Cells["Medname"].Value.ToString());
            if (clsSharedVariables.Language.Equals("Eng"))
            {
                objOPDMed.MedDosage = clsSharedVariables.InItCaps(dgMedicine.Rows[RowIndex].Cells["MedDosage"].Value.ToString());
            }

            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objOPDMed.MedDosage = dgMedicine.Rows[RowIndex].Cells["MedDosage"].Value.ToString().Replace("\\", "\\\\");
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                objOPDMed.MedDosage = dgMedicine.Rows[RowIndex].Cells["MedDosage"].Value.ToString().Replace("\\", "\\\\");
            }
            return objOPDMed;
        }

        private clsBLPatientReg SetBLValuesUpdate(clsBLPatientReg objPatient)
        {
            objPatient.Name = clsSharedVariables.InItCaps(txtHName.Text.Trim());
            if (!clsSharedVariables.AutoPRNO)
            {
                DataView dv = new DataView();

                objPatient.PRNO = txtHPRNo.Text.Trim();
                dv = objPatient.GetAll(7);
                dv.RowFilter = "PatientID <>" + lblPatientID.Text;
                if (dv.Count != 0)
                {
                    MessageBox.Show("" + clsSharedVariables.PRNOHeading + " already assign ", "" + clsSharedVariables.PRNOHeading + " Duplication", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objPatient.PRNO = "Duplicate";
                    return objPatient;
                }
            }
            if (rbHMale.Checked)
            {
                objPatient.Gender = "M";
            }
            else
            {
                objPatient.Gender = "F";
            }
            objPatient.DOB = dtpHDOB.Value.Date.ToString("dd/MM/yyyy");
            objPatient.PhoneNo = txtHPhone.Text.Trim();
            objPatient.CellNo = txtHCell.Text.Trim();
            objPatient.Address = clsSharedVariables.InItCaps(txtHAddress.Text.Trim());
            objPatient.EnteredBy = clsSharedVariables.UserID;
            objPatient.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPatient.CityID = clsSharedVariables.InItCaps(cboHCity.SelectedValue.ToString());
            if (!cboHPanel.SelectedValue.ToString().Equals("-1"))
            {
                objPatient.PanelID = clsSharedVariables.InItCaps(cboHPanel.SelectedValue.ToString());
            }
            else
            {
                objPatient.PanelID = "0";
            }

            return objPatient;
        }

        private clsBLPatientReg SetBLValuesCUpdate(clsBLPatientReg objPatient)
        {
            objPatient.Name = clsSharedVariables.InItCaps(txtCName.Text.Trim());

            if (!clsSharedVariables.AutoPRNO)
            {
                DataView dv = new DataView();

                objPatient.PRNO = txtCPRNo.Text.Trim();
                dv = objPatient.GetAll(7);
                dv.RowFilter = "PatientID <>" + lblPatientID.Text;

                if (dv.Count != 0)
                {
                    MessageBox.Show("" + clsSharedVariables.PRNOHeading + " already assign ", "" + clsSharedVariables.PRNOHeading + " Duplication", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objPatient.PRNO = "Duplicate";
                    return objPatient;
                }
            }
            if (rbCMale.Checked)
            {
                objPatient.Gender = "M";
            }
            else
            {
                objPatient.Gender = "F";
            }
            objPatient.DOB = dtpCDOB.Value.Date.ToString("dd/MM/yyyy");
            objPatient.PhoneNo = txtCPhone.Text.Trim();
            objPatient.CellNo = txtCCell.Text.Trim();
            objPatient.Address = clsSharedVariables.InItCaps(txtCAddress.Text.Trim());
            objPatient.EnteredBy = clsSharedVariables.UserID;
            objPatient.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPatient.CityID = clsSharedVariables.InItCaps(cboCCity.SelectedValue.ToString());
            objPatient.Email = txtCEmail.Text.Trim();
            if (!cboCPanel.SelectedValue.ToString().Equals("-1"))
            {
                objPatient.PanelID = clsSharedVariables.InItCaps(cboCPanel.SelectedValue.ToString());
            }
            else
            {
                objPatient.PanelID = "0";
            }

            return objPatient;
        }

        #endregion

        #region Fill Method

        private void FillPatientRegDG(clsBLDBConnection objConnection, clsBLPatientReg objPatient)
        {
            DataView dvPatient = objPatient.GetAll(2);
            dvPatient.Sort = "PatientName";
            this.dgRegisterPatient.DataSource = dvPatient;
            
            GenSerNo(dgRegisterPatient, "RSNo");

            dvPatient = null;
        }

        private void FillPatientReg(int rowIndex)
        {
            lblPatientID.Text = dgRegisterPatient.Rows[rowIndex].Cells["PatientID"].Value.ToString();
            txtRPRNo.Mask = txtPRNO.Mask = "";
            txtRPRNo.Text = txtPRNO.Text = dgRegisterPatient.Rows[rowIndex].Cells["PRNO"].Value.ToString();
            //GetPatientLastVisitOPDID(1);
            SavePatient.Text = dgRegisterPatient.Rows[rowIndex].Cells["PRNO"].Value.ToString();
            txtPatientName.Text = clsSharedVariables.InItCaps(dgRegisterPatient.Rows[rowIndex].Cells["PatientName"].Value.ToString());
            if (dgRegisterPatient.Rows[rowIndex].Cells["Gender"].Value.ToString().Equals("Male"))
            {
                rbPatientMale.Checked = true;
            }
            else
            {
                rbPatientFemale.Checked = true;
            }
            dtpPatientDOB.Value = Convert.ToDateTime(dgRegisterPatient.Rows[rowIndex].Cells["DOB"].Value.ToString());
            txtPatientAge.Text = Convert.ToString(DateTime.Now.Year - dtpPatientDOB.Value.Year);
            txtPatientPhone.Text = dgRegisterPatient.Rows[rowIndex].Cells["PhoneNo"].Value.ToString();
            txtPatientCell.Text = dgRegisterPatient.Rows[rowIndex].Cells["CellNo"].Value.ToString();
            txtPatientAddress.Text = clsSharedVariables.InItCaps(dgRegisterPatient.Rows[rowIndex].Cells["Address"].Value.ToString());
            cboPatientCity.Text = dgRegisterPatient.Rows[rowIndex].Cells["cityname"].Value.ToString();

            if (lblSelect.Tag.Equals("1"))
            {
                CreateInstructionCHB();
            }
            txtRegEmail.Text = dgRegisterPatient.Rows[rowIndex].Cells["PEmail"].Value.ToString();

            miHoldSave.Enabled = miConsultedSave.Enabled = true;
            //cboRPanel.Text = dgRegisterPatient.Rows[rowIndex].Cells["RPanel"].Value.ToString();
        }

        private void FillListGridView(string TypeID)
        {
            if (!TypeID.Equals(""))
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLPreference objPreference = new clsBLPreference(objConnection);

                objPreference.TypeID = TypeID;
                objPreference.PersonID = clsSharedVariables.UserID;
                objConnection.Connection_Open();
                DataView dv = objPreference.GetAll(2);
                //dv.Sort = "Name";
                dgList.DataSource = dv;
                objConnection.Connection_Close();

                txtSearch.Text = "";

                objPreference = null;
                objConnection = null;
            }
        }

        //private void FillConsultedInstruction(string str)
        //{
        //    int x = 10, y = 5;
        //    int InstructionCount = 1;
        //    string[] stringSeparators = { "    " };

        //    string[] ConsultedInstruction;

        //    ConsultedInstruction =  str.Split(stringSeparators, System.StringSplitOptions.RemoveEmptyEntries);
        //    foreach (string InstructionStr in ConsultedInstruction)
        //    {
        //        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), InstructionStr, true, true));
        //        y = y + 25;
        //    }
        //}

        private void FillHoldInstruction(string str)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLType objType = new clsBLType(objConnection);
            int x = 1, y = 0;
            int InstructionCount = 1;

            objConnection.Connection_Open();

            if (clsSharedVariables.Language.Equals("Eng"))
            {
                objType.Name = "EngInstruction";

                this.pnlInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objType.Name = "UrduInstruction";
                this.pnlInstructions.Font = new System.Drawing.Font("AlKatib1", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;

            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                objType.Name = "FarsiInstruction";
                this.pnlInstructions.Font = new System.Drawing.Font("AlKatib1", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;

            }

            objType.PersonID = clsSharedVariables.UserID;
            DataView dvType = objType.GetAll(3);

            objConnection.Connection_Close();

            foreach (DataRow dr in dvType.Table.Rows)
            {
                if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    if (str.Replace("Ju", "\\Ju").Contains(dr["Description"].ToString().Trim()))
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), true, true));
                    }
                    else
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), false, true));
                    }
                }
                else if (clsSharedVariables.Language.Equals("Eng"))
                {
                    if (str.Contains(dr["Description"].ToString().Trim()))
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), true, true));
                    }
                    else
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), false, true));
                    }
                }
                if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    if (str.Replace("Ju", "\\Ju").Contains(dr["Description"].ToString().Trim()))
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), true, true));
                    }
                    else
                    {
                        this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), false, true));
                    }
                }
                y = y + 25;
            }
            objConnection = null;
            objType = null;
            dvType = null;

        }

        private void FillMedDosage(DataView dv)
        {
            int AddRow = 0;
            for (int i = 0; i < dv.Table.Rows.Count; i++)
            {
                AddRow = dgMedicine.Rows.Add();
                dgMedicine.Rows[AddRow].Cells["MedID"].Value = dv.Table.Rows[i]["MedicineID"].ToString();
                dgMedicine.Rows[AddRow].Cells["Medname"].Value = clsSharedVariables.InItCaps(dv.Table.Rows[i]["MedicineName"].ToString());
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = clsSharedVariables.InItCaps(dv.Table.Rows[i]["MedicineDosage"].ToString());
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = dv.Table.Rows[i]["MedicineDosage"].ToString();
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    dgMedicine.Rows[AddRow].Cells["MedDosage"].Value = dv.Table.Rows[i]["MedicineDosage"].ToString();
                }
            }
        }

        private void FillPatientInfo(DataView dv)
        {
            string str = "";

            str = dv.Table.Rows[0]["History"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgHistory, "HistoryName", "PrefIDHistory", "ImgHistAvail", "HistDiseaseIDRef");
            }

            str = dv.Table.Rows[0]["PresentingComplaints"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgComplaints, "ComplaintsName", "PrefIDComplaints", "CompImgDisease", "CompDiseaseIDRef");
            }

            str = dv.Table.Rows[0]["Diagnosis"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgDiagnosis, "DiagnosisName", "PrefIDDiagnosis", "DiagnoImgDisease", "DiagnosisDiseaseRefID");
            }

            str = dv.Table.Rows[0]["Signs"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgSigns, "SignsName", "PrefIDSigns", "SignImgDisease", "SignDiseaseIDRef");
            }

            str = dv.Table.Rows[0]["DisposalPlan"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgPlans, "PlansName", "PrefIDPlans", "", "");
            }

            str = dv.Table.Rows[0]["Investigation"].ToString();
            if (!str.Equals(""))
            {
                PreviousVisitDataGrid(str.Replace("'", "''"), dgInvestigation, "InvestigatioName", "PrefIDInvestigation", "", "");
            }

            txtBP.Text = dv.Table.Rows[0]["BP"].ToString().Split(' ')[0].ToString().Trim();

            if (!dv.Table.Rows[0]["Temprature"].ToString().Equals(""))
            {
                txtTemp.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[0].ToString().Trim();
                cboTempUnit.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[1].ToString().Trim();
            }

            if (!dv.Table.Rows[0]["pulse"].ToString().Equals(""))
            {
                txtPulse.Text = dv.Table.Rows[0]["pulse"].ToString().Split(' ')[0].ToString().Trim();
            }
            //if (!dv.Table.Rows[0]["HeadCircumferences"].ToString().Equals(""))
            //{
            //    txtHeadCircum.Text = dv.Table.Rows[0]["HeadCircumferences"].ToString().Split(' ')[0].ToString().Trim();
            //}
            if (!dv.Table.Rows[0]["Weight"].ToString().Equals(""))
            {
                txtWeight.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[0].ToString().Trim();
                cboWeightUnit.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[1].ToString().Trim();
            }
            if (!dv.Table.Rows[0]["Height"].ToString().Equals(""))
            {
                txtHeight.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[0].ToString().Trim();
                cboHeightUnit.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[1].ToString().Trim();
            }
            //if (lblSelect.Text.Equals("1") || lblSelect.Text.Equals("2") || !lblSelect.Tag.ToString().Equals("1"))
            {
                FillHoldInstruction(dv.Table.Rows[0]["Instruction"].ToString());
                //}
                //else if (lblSelect.Text.Equals("2"))
                //{
                //FillHoldInstruction(dv.Table.Rows[0]["Instruction"].ToString());
                //FillConsultedInstruction(dv.Table.Rows[0]["Instruction"].ToString());
            }

            //dtpFollowUpDate.Value = Convert.ToDateTime( dv.Table.Rows[0]["FollowUpDate"].ToString());
            if (!dv.Table.Rows[0]["Payment"].ToString().Equals(""))
            {
                txtPayment.Text = dv.Table.Rows[0]["Payment"].ToString();
            }
            else
            {
                txtPayment.Text = clsSharedVariables.Payment; ;
            }
            txtNotes.Text = dv.Table.Rows[0]["Notes"].ToString();
            txtHeartRate.Text = dv.Table.Rows[0]["HeartRate"].ToString();
            txtRespiratoryRate.Text = dv.Table.Rows[0]["RespiratoryRate"].ToString();
            txtWeight_Leave(null, null);
        }

        private void FillConsultedDatagrid(clsBLDBConnection objConnection, clsBLPatientReg objPatient, int flag, DataGridView dg)
        {
            DataView dvPatient = objPatient.GetAll(flag);
            dvPatient.Sort = "PatientName";

            dg.DataSource = dvPatient;
            //dg.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            if (dg.Name.Equals("dgHold"))
            {
                GenSerNo(dg, "HSNo");
            }
            else
            {
                GenSerNo(dg, "CSNo");
            }
            objPatient = null;
            dvPatient = null;
        }

        private void FillHoldPatient(int rowIndex)
        {
            lblPatientID.Text = dgHold.Rows[rowIndex].Cells["HPatientID"].Value.ToString();
            txtHPRNo.Mask= txtPRNO.Mask="";
            lblOPDID.Text = dgHold.Rows[rowIndex].Cells["OPDID"].Value.ToString();
            txtHPRNo.Text = txtPRNO.Text = "";
            txtPRNO.Text = dgHold.Rows[rowIndex].Cells["HPRNO"].Value.ToString();
            txtHPRNo.Text = dgHold.Rows[rowIndex].Cells["HPRNO"].Value.ToString();
            txtHName.Text = clsSharedVariables.InItCaps(dgHold.Rows[rowIndex].Cells["HPatientName"].Value.ToString());
            if (dgHold.Rows[rowIndex].Cells["HGender"].Value.ToString().Equals("Male"))
            {
                rbHMale.Checked = true;
            }
            else
            {
                rbHFemale.Checked = true;
            }
            dtpHDOB.Value = Convert.ToDateTime(dgHold.Rows[rowIndex].Cells["HDOB"].Value.ToString());
            txtHAge.Text = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dgHold.Rows[rowIndex].Cells["HDOB"].Value.ToString()).Year);
            txtHPhone.Text = dgHold.Rows[rowIndex].Cells["HPhone"].Value.ToString();
            txtHCell.Text = dgHold.Rows[rowIndex].Cells["HCell"].Value.ToString();
            txtHAddress.Text = clsSharedVariables.InItCaps(dgHold.Rows[rowIndex].Cells["HAddress"].Value.ToString());
            cboHCity.Text = dgHold.Rows[rowIndex].Cells["Hcityname"].Value.ToString();
            txtHEmail.Text = dgHold.Rows[rowIndex].Cells["HEmail"].Value.ToString();
            cboHPanel.Text = dgHold.Rows[rowIndex].Cells["HPanel"].Value.ToString();
            //CreateInstructionCHB();
        }

        private void FillConsultedPatient(int rowIndex)
        {
            lblCPatientID.Text = dgConsulted.Rows[rowIndex].Cells["CPatientID"].Value.ToString();
            lblPatientID.Text = dgConsulted.Rows[rowIndex].Cells["CPatientID"].Value.ToString();
            txtCPRNo.Mask = txtPRNO.Mask = "";
            lblOPDID.Text = dgConsulted.Rows[rowIndex].Cells["COPDID"].Value.ToString();
            txtCPRNo.Mask = "";
            txtCPRNo.Text = txtPRNO.Text = dgConsulted.Rows[rowIndex].Cells["CPRNO"].Value.ToString();
            txtCName.Text = clsSharedVariables.InItCaps(dgConsulted.Rows[rowIndex].Cells["CPatientName"].Value.ToString());
            if (dgConsulted.Rows[rowIndex].Cells["CGender"].Value.ToString().Equals("Male"))
            {
                rbCMale.Checked = true;
            }
            else
            {
                rbHFemale.Checked = true;
            }
            dtpCDOB.Value = Convert.ToDateTime(dgConsulted.Rows[rowIndex].Cells["CDOB"].Value.ToString());
            txtCAge.Text = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dgConsulted.Rows[rowIndex].Cells["CDOB"].Value.ToString()).Year);
            txtCPhone.Text = dgConsulted.Rows[rowIndex].Cells["CPhone"].Value.ToString();
            txtCCell.Text = dgConsulted.Rows[rowIndex].Cells["CCell"].Value.ToString();
            txtCAddress.Text = clsSharedVariables.InItCaps(dgConsulted.Rows[rowIndex].Cells["CAddress"].Value.ToString());
            cboCCity.Text = dgConsulted.Rows[rowIndex].Cells["Ccityname"].Value.ToString();
            txtCEmail.Text = dgConsulted.Rows[rowIndex].Cells["CEmail"].Value.ToString();
            cboCPanel.Text = dgConsulted.Rows[rowIndex].Cells["CPanel"].Value.ToString();
            //CreateInstructionCHB();
        }

        #endregion

        #region Datagridview Cell Double Click

        private void dgRegisterPatient_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NewOPDID = lblOPDID.Text;
            if (e.RowIndex != -1)
            {
                lblOPDID.Tag = "U";
                if (lblSelect.Tag.ToString().Equals("1"))
                {
                    FillPatientReg(e.RowIndex);
                    miEnable(true);
                    //          miEmail.Enabled = false;
                    pnlEnable(true);
                    pnlRPatient.Enabled = true;
                    btnPatientSave.Enabled = true;
                    dtpFollowUpDate.Enabled = true;
                    pnlTotalVisit.Visible = true;
                    pnlVisit.Visible = true;

                    pnlSelPatientDetail.Visible = true;
                    pnlSelPatientHead.Visible = true;

                    GetPatientDetail();
                    GetPatientAlerAdver();
                    GetPatientProcAdmssion();

                    ClearVitalSigns();

                    pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);
                    PrevoiusInfo();
                    CreateVisitLB("");
                }
                else
                {
                    FillPatientReg(e.RowIndex);

                    miEnable(true);
                    pnlEnable(true);
                    pnlRPatient.Enabled = true;
                    btnPatientSave.Enabled = true;
                    dtpFollowUpDate.Enabled = true;
                    pnlTotalVisit.Visible = true;
                    pnlVisit.Visible = true;
                    pnlSelPatientDetail.Visible = true;
                    pnlSelPatientHead.Visible = true;

                    GetPatientDetail();
                    GetPatientAlerAdver();
                    GetPatientProcAdmssion();
                    //if (dgRegisterPatient.Rows[e.RowIndex].Cells["Status"].Value.ToString().Equals("C"))
                    //{
                    //  miHoldSave.Enabled = false;
                    //}
                    //else if (dgRegisterPatient.Rows[e.RowIndex].Cells["Status"].Value.ToString().Equals("H"))
                    //{
                    //  miConsultedSave.Enabled = false;
                    //}
                    ClearVitalSigns();

                    pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);

                    lblOPDID.Text = dgRegisterPatient.Rows[e.RowIndex].Cells["ROPDID"].Value.ToString();
                  
                    CreateVisitLB("");
                    CreateLBWaiting();
                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    objConnection.Connection_Open();
                    GetAppointment(objConnection);
                    objConnection.Connection_Close();
                    if (dgRegisterPatient.Rows[e.RowIndex].Cells["Status"].Value.ToString().Equals("W"))
                    {
                        PrevoiusInfo();
                    }
                    else
                    {
                        PrevoiusInfo();
                    }
                }
                lblMode.Text = "Mode : Prescription";
                pnlRegisterSearch.Enabled = false;
            }
        }

        private void GetHistory()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();

            objConnection.Connection_Open();
            objOPD.PRNO = txtPRNO.Text;
            dv = objOPD.GetAll(5);
            CreateVisitLB("");
            CreateLBWaiting();
            GetAppointment(objConnection);
            objConnection.Connection_Close();
            dgHistory.Rows.Clear();
            for (int i = 0; i < dv.Table.Rows.Count; i++)
            {
                string str = "";
                str = dv.Table.Rows[i]["History"].ToString();
                if (!str.Equals(""))
                {
                    PreviousVisitDataGrid(str, dgHistory, "HistoryName", "PrefIDHistory", "ImgHistAvail", "HistDiseaseIDRef");
                }
                if (i == dv.Table.Rows.Count - 1)
                {
                    txtNotes.Text = dv.Table.Rows[i]["Notes"].ToString();
                }
            }

            objConnection = null;
            objOPD = null;
            dv = null;
        }

        private void pnlSize(int x, int y)
        {
            if (lblSelect.Text.Equals("0"))
            {
                pnlRegister.Size = new System.Drawing.Size(x, y);
                pnlRegister.Visible = true;
                pnlRegister.BringToFront();
                pnlHold.Visible = false;
                pnlConsult.Visible = false;
            }
            else if (lblSelect.Text.Equals("1"))
            {
                pnlHold.Size = new System.Drawing.Size(x, y);
                pnlRegister.Visible = false;
                pnlHold.Visible = true;
                pnlHold.BringToFront();
                pnlConsult.Visible = false;

            }
            else if (lblSelect.Text.Equals("2"))
            {
                pnlConsult.Size = new System.Drawing.Size(x, y);
                pnlRegister.Visible = false;
                pnlHold.Visible = false;
                pnlConsult.Visible = true;
                pnlConsult.BringToFront();
            }
        }

        private void dgList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!e.RowIndex.ToString().Equals("-1"))
            {
                if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgHistory.Columns["HistoryName"].HeaderText))
                {
                    DuplicationCheck(ref dgHistory, "PrefIDHistory", e.RowIndex, "HistoryName", "HistDiseaseIDRef", "ImgHistAvail");
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgDiagnosis.Columns["DiagnosisName"].HeaderText))
                {
                    DuplicationCheck(ref dgDiagnosis, "PrefIDDiagnosis", e.RowIndex, "DiagnosisName", "DiagnosisDiseaseRefID", "DiagnoImgDisease");
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgComplaints.Columns["ComplaintsName"].HeaderText))
                {
                    DuplicationCheck(ref dgComplaints, "PrefIDComplaints", e.RowIndex, "ComplaintsName", "CompDiseaseIDRef", "CompImgDisease");
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgSigns.Columns["SignsName"].HeaderText))
                {
                    DuplicationCheck(ref dgSigns, "PrefIDSigns", e.RowIndex, "SignsName", "SignDiseaseIDRef", "SignImgDisease");
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgPlans.Columns["PlansName"].HeaderText))
                {
                    DuplicationCheck(ref dgPlans, "PrefIDPlans", e.RowIndex, "PlansName", "~!@", "~!@");
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgInvestigation.Columns["InvestigatioName"].HeaderText))
                {
                    DuplicationCheck(ref dgInvestigation, "PrefIDInvestigation", e.RowIndex, "InvestigatioName", "~!@", "~!@");
                }

                txtSearch.Text = "";

                dgList.ClearSelection();
                dgHistory.ClearSelection();
                dgComplaints.ClearSelection();
                dgSigns.ClearSelection();
                dgDiagnosis.ClearSelection();
                dgPlans.ClearSelection();
                dgInvestigation.ClearSelection();
            }
        }

        private void dgHold_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                lblOPDID.Tag = "U";
                FillHoldPatient(e.RowIndex);
                btnHoldUpdate.Enabled = true;
                miEnable(true);
                pnlHPatient.Enabled = true;
                pnlVisit.Visible = true;
                pnlTotalVisit.Visible = true;
                pnlSelPatientDetail.Visible = true;
                pnlSelPatientHead.Visible = true;

                GetPatientDetail();
                GetPatientAlerAdver();
                GetPatientProcAdmssion();

                pnlEnable(true);
                //miPrintPreview.Enabled = true;
                pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);
                PrevoiusInfo();
                CreateVisitLB("H");
                CreateLBWaiting();
                lblMode.Text = "Mode : Prescription";
                pnlHoldSearch.Enabled = false;
                pnlHPatient.Enabled = true;
            }
        }

        private void dgConsulted_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {

                FillConsultedPatient(e.RowIndex);
                miEnable(true);
                //miHoldSave.Enabled = false;
                pnlEnable(true);
                pnlCPatient.Enabled = true;
                pnlVisit.Visible = true;
                pnlTotalVisit.Visible = true;
                pnlSelPatientDetail.Visible = true;
                pnlSelPatientHead.Visible = true;

                GetPatientDetail();
                GetPatientAlerAdver();
                GetPatientProcAdmssion();

                pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);
                //panel2.Size = new System.Drawing.Size(884, 93);
                btnCUpdate.Enabled = true;
                PrevoiusInfo();
                CreateVisitLB("C");
                CreateLBWaiting();
                lblMode.Text = "Mode : Prescription";
                pnlConsultedSearch.Enabled = false;
                pnlCPatient.Enabled = true;
            }
        }

        #endregion

        #region Text Changed

        private void cboHeightUnit_TextChanged(object sender, EventArgs e)
        {
            Double Height = 0;
            if (!txtHeight.Text.Equals(""))
            {
                if (cboHeightUnit.Text.Equals("Inches"))
                {
                    Height = Convert.ToDouble(txtHeight.Text) * 12;
                }
                else
                {
                    Height = Convert.ToDouble(txtHeight.Text) / 12;
                }
                txtHeight.Text = Height.ToString(".000");//Convert in Inches
            }
        }

        private void cboWeightUnit_TextChanged(object sender, EventArgs e)
        {
            Double Weight = 0;
            if (!txtWeight.Text.Equals(""))
            {
                if (cboWeightUnit.Text.Equals("Pounds"))
                {
                    Weight = Convert.ToDouble(txtWeight.Text) * 2.20462262;//convert in pound
                }
                else
                {
                    Weight = Convert.ToDouble(txtWeight.Text) * 0.45359237;
                }
                txtWeight.Text = Weight.ToString(".00");
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgList.Rows.Count; i++)
            {
                if (dgList.Rows[i].Cells["PName"].Value.ToString().ToUpper().StartsWith(txtSearch.Text.ToUpper()))
                {
                    dgList.Rows[i].Selected = true;
                    dgList.FirstDisplayedScrollingRowIndex = i;
                    break;
                }
            }
        }

        #endregion

        #region Clear Field

        private void ClearDG()
        {
            dgHistory.Rows.Clear();
            dgComplaints.Rows.Clear();
            dgDiagnosis.Rows.Clear();
            dgSigns.Rows.Clear();
            dgPlans.Rows.Clear();
            dgInvestigation.Rows.Clear();
            dgMedicine.Rows.Clear();
            pnlInstructions.Controls.Clear();
        }

        private void ClearField()
        {
            txtCPRNo.Text = "";
            txtRPRNo.Text = "";
            txtHPRNo.Text = "";
            txtPRNO.Text = "";
            txtPatientName.Text = "";
            txtRPRNo.Text = "";
            dtpPatientDOB.Value = dtpPatientDOB.MinDate;
            txtPatientAge.Text = "";
            rbPatientMale.Checked = true;
            txtPatientPhone.Text = "";
            txtRegEmail.Text = "";
            txtPatientCell.Text = "";
            txtPatientAddress.Text = "";
            cboPatientCity.Text = "Islamabad";
            //cboRPanel.Text = "Select";
            lblPatientID.Text = "";
            txtPayment.Text = clsSharedVariables.Payment;
            pnlSerachByPRNo.Visible = false;
        }

        private void ClearGridRows(DataGridView dg)
        {
            try
            {
                DataView dv = (DataView)dg.DataSource;

                if (dv != null)
                {
                    dv.Table.Rows.Clear();
                    dg.DataSource = dv;
                }
            }
            catch
            {
                DataTable dt = (DataTable)dg.DataSource;

                if (dt != null)
                {
                    dt.Rows.Clear();
                    dg.DataSource = dt;
                }
            }

            while (dg.Rows.Count > 0)
            {
                dg.Rows.RemoveAt(0);
            }
        }

        private void ClearHoldPatient()
        {
            txtHName.Text = "";
            txtHPRNo.Text = "";
            dtpHDOB.Value = DateTime.Now.Date;
            rbHMale.Checked = true;
            txtHPhone.Text = "";
            txtHEmail.Text = "";
            txtHCell.Text = "";
            txtHAddress.Text = "";
            txtHName.Focus();
            this.lblPatientID.Text = "";
            txtHAge.Text = "";
            cboHPanel.Text = "Select";
            txtPayment.Text = clsSharedVariables.Payment;
            pnlSerachByPRNo.Visible = false;
        }

        private void clearConsulted()
        {
            txtCName.Text = "";
            txtCPRNo.Text = "";
            dtpCDOB.Value = dtpCDOB.MinDate;
            txtCAge.Text = "";
            rbCMale.Checked = true;
            txtCPhone.Text = "";
            txtCEmail.Text = "";
            txtCCell.Text = "";
            txtCAddress.Text = "";
            btnCUpdate.Enabled = false;
            cboCCity.SelectedIndex = 1;
            txtPayment.Text = clsSharedVariables.Payment;
            pnlSerachByPRNo.Visible = false;
        }

        private void clearHold()
        {
            txtHName.Text = "";
            txtHPRNo.Text = "";
            txtHAge.Text = "";
            dtpHDOB.Value = dtpHDOB.MinDate;
            rbHMale.Checked = true;
            txtHPhone.Text = "";
            txtHEmail.Text = "";
            txtHCell.Text = "";
            txtHAddress.Text = "";
            cboHCity.SelectedIndex = 1;
            btnHoldUpdate.Enabled = false;
            txtPayment.Text = clsSharedVariables.Payment;
        }

        private void ClearConsultedPatient()
        {
            txtCName.Text = "";
            txtCPRNo.Text = "";
            dtpCDOB.Value = dtpCDOB.MinDate;
            rbCMale.Checked = true;
            txtCPhone.Text = "";
            txtCEmail.Text = "";
            txtCCell.Text = "";
            txtCAddress.Text = "";
            txtCSName.Focus();
            this.lblCPatientID.Text = "";
            txtCAge.Text = "";
            cboCPanel.Text = "Select";
            txtPayment.Text = clsSharedVariables.Payment;
        }

        private void ClearVitalSigns()
        {
            txtBP.Text = "";
            txtTemp.Text = "";
            txtPulse.Text = "";
            txtRespiratoryRate.Text = "";
            txtHeartRate.Text = "";
            txtBMI.Text = "";
           // txtHeadCircum.Text = "";
            txtWeight.Text = "";
            txtHeight.Text = "";
            txtBSA.Text = "";
            dtpFollowUpDate.Tag = "0";
            pnlMedHistGrid.Visible = false;
            ClearGridRows(dgMedHist);
            pnlVitalSignHist.Visible = false;
            txtNotes.Text = "";
        }

        private void ClearSearchField()
        {
            txtSearchPRNO.Text = "";
            txtSearchName.Text = "";
            txtSearchPhone.Text = "";

            txtHSPRNO.Text = "";
            txtHSName.Text = "";
            txtHSPhone.Text = "";

            txtCSPRNO.Text = "";
            txtCSName.Text = "";
            txtCSPhone.Text = "";
        }

        #endregion

        #region For Patient Search

        private void PatientSearch()
        {
            SearchCriteria();
            //for (int i = 0; i < dgRegisterPatient.Rows.Count; i++)
            //{
            //  if (dgRegisterPatient.Rows[i].Cells["EnteredBy"].Value.ToString().Equals(clsSharedVariables.UserID))
            //  {
            //    dgRegisterPatient.Rows[i].DefaultCellStyle.BackColor = Color.LightYellow;
            //  }
            //}
        }

        private void SearchCriteria()//Register patient 
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            ClearDG();
            objConnection.Connection_Open();
     

            if (!txtSearchPRNO.Text.Trim().Equals(""))
            {
               
                objPatient.PRNO = txtSearchPRNO.Text.Trim();
                //txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
                //txtSearchPRNO.Text = txtSearchPRNO.Text.Trim();
                objPatient.MRNo = txtSearchPRNO.Text.Replace("-", string.Empty);
            }
            if (!txtSearchName.Text.Trim().Equals(""))
            {
                objPatient.Name = clsSharedVariables.InItCaps(txtSearchName.Text.Trim());
            }
            if (txtSearchPhone.Text.Trim().Length >= 7)
            {
                //if (rbSPhone.Checked)
                //{
                //    objPatient.PhoneNo = txtSearchPhone.Text.Trim();
                //}
                //else
                //{
                //    objPatient.CellNo = txtSearchPhone.Text.Trim();
                //}

                objPatient.CellNo = txtSearchPhone.Text.Trim();

            }
            if (txtSearchPRNO.Text.Trim().Equals("") && txtSearchName.Text.Trim().Equals("") && txtSearchPhone.Text.Trim().Equals(""))
            {
                objPatient.FromDate = dtpFrom.Value.ToString("dd/MM/yyyy");
                objPatient.ToDate = dtpTo.Value.ToString("dd/MM/yyyy");
            }
           
            FillPatientRegDG(objConnection, objPatient);
            if (dgRegisterPatient.Rows.Count != 0)
            {
                // FillPatientReg(0);
            }
            else
            {
                MessageBox.Show(" Patient Not Found ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            objConnection.Connection_Close();


            objConnection = null;
            objPatient = null;
        }

        private void TodaySummarySearch()//Register patient 
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            ClearDG();

            DataView dvPatient = new DataView();
            DataView dv = new DataView();
            DataTable dt = new DataTable();

            objConnection.Connection_Open();

            dvPatient = objPatient.GetAll(9);
            dvPatient.Sort = "PatientName";
            this.dgRegisterPatient.DataSource = dvPatient;
            objConnection.Connection_Close();

            GenSerNo(dgRegisterPatient, "RSNo");
            lklTdaySummary.Text = "Waiting Queue ( " + dvPatient.Table.Rows.Count.ToString() + " )";

            dt = null;
            dvPatient = null;
            objConnection = null;
            objPatient = null;
        }

        private void ConsultedSearchCriteria(char Status, DataGridView dg)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            objConnection.Connection_Open();
            txtCSPRNO.Mask = "";
            if (!txtCSPRNO.Text.Trim().Equals(""))
            {
                objPatient.PRNO = txtCSPRNO.Text.Trim();
                txtCSPRNO.Text = txtCSPRNO.Text.Trim();
                objPatient.MRNo = txtCSPRNO.Text.Replace("-", string.Empty);
            }
            if (!txtCSName.Text.Trim().Equals(""))
            {
                objPatient.Name = clsSharedVariables.InItCaps(txtCSName.Text.Trim());
            }
            if (txtCSPhone.Text.Trim().Length >= 7)
            {
                if (rbCSPhone.Checked)
                {
                    objPatient.PhoneNo = txtCSPhone.Text.Trim();
                }
                else
                {
                    objPatient.CellNo = txtCSPhone.Text.Trim();
                }
            }

            if (txtCSPRNO.Text.Trim().Equals("") && txtCSName.Text.Trim().Equals("") && txtCSPhone.Text.Trim().Equals(""))
            {
                objPatient.FromDate = dtpCFrom.Value.ToString("dd/MM/yyyy");
                objPatient.ToDate = dtpCTo.Value.ToString("dd/MM/yyyy");
            }
            
            objPatient.Status = Status.ToString();
            FillConsultedDatagrid(objConnection, objPatient, 4, dg);
            if (dg.Rows.Count != 0)
            {
                //if (!txtCSPRNO.Text.Equals(""))
                //{
                //    DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(0, dg.Rows.Count - 1);
                //    dgConsulted_CellDoubleClick(null, e);
                //    e = null;
                //}
            }
            else
            {
                MessageBox.Show(" No Consulted Patient Found for Selected Critaria ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            objConnection.Connection_Close();
            objConnection = null;
            objPatient = null;
        }

        private void HoldSearchCriteria(char Status, DataGridView dg)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            txtHSPRNO.Mask = "";
            objConnection.Connection_Open();
            if (!txtHSPRNO.Text.Trim().Trim().Equals(""))
            {
                objPatient.PRNO = txtHSPRNO.Text.Trim();
                txtHSPRNO.Text = txtHSPRNO.Text.Trim();
                objPatient.MRNo = txtHSPRNO.Text.Replace("-", string.Empty);
            }
            if (!txtHSName.Text.Trim().Equals(""))
            {
                objPatient.Name = clsSharedVariables.InItCaps(txtHSName.Text.Trim());
            }
            if (txtHSPhone.Text.Trim().Length >= 7)
            {
                if (rbHSPhone.Checked)
                {
                    objPatient.PhoneNo = txtHSPhone.Text.Trim();
                }
                else
                {
                    objPatient.CellNo = txtHSPhone.Text.Trim();
                }
            }
          
        
            if (txtHSPRNO.Text.Trim().Equals("") && txtHSName.Text.Trim().Equals("") && txtHSPhone.Text.Trim().Equals(""))
            {
                objPatient.FromDate =Convert.ToDateTime(dtpHFrom.Value).ToString("dd/MM/yyyy");
                objPatient.ToDate = Convert.ToDateTime(dtpHTo.Value).ToString("dd/MM/yyyy");
            }
            
            objPatient.Status = Status.ToString();
            FillConsultedDatagrid(objConnection, objPatient, 4, dg);
            if (dg.Rows.Count != 0)
            {
                //if(!txtHSPRNO.Text.Equals(""))
                //{
                //    DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(0, dg.Rows.Count-1);
                //    dgHold_CellDoubleClick(null,e);
                //    e = null;
                //}
            }
            else
            {
                MessageBox.Show(" No Hold Patient Found for Selected Search Critaria", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            objConnection.Connection_Close();
            objConnection = null;
            objPatient = null;
        }

        #endregion

        #region KeyPress Event

        private void txtSearchPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnPatientSearch_Click(sender, EventArgs.Empty);
            }
        }

        private void txtHSPRNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnHSearch_Click(sender, EventArgs.Empty);
            }
        }

        private void txtCSPRNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnConsultedSearch_Click(sender, EventArgs.Empty);
            }
        }

        #endregion

        #region DataGridview Parameter events

        private void dgParameter_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            dg.Rows[e.RowIndex].Tag = "1";
        }

        private void dgParameter_Click(object sender, EventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            if (dg.Tag != null)
            {
                FillListGridView(dg.Tag.ToString());
                dgList.Columns["Pname"].HeaderText = clsSharedVariables.InItCaps(dg.Columns[0].HeaderText);

                ControlepnlList(dg);

                this.ttOPD.SetToolTip(txtSearch, "Type here to be search from " + dgList.Columns["Pname"].HeaderText +" List");
                GetPreParameter(dgList.Columns["Pname"].HeaderText + " :  ", dg.Name);
            }
            pnlMedHistGrid.Visible = false;
            ClearGridRows(dgMedHist);
            pnlDGList.BringToFront();
        }

        private void dgParameter_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;

            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dg.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dg.ContextMenuStrip.Items[0].Enabled = false;

                if (dg.Rows[hi.RowIndex].Cells[hi.ColumnIndex].Value == null)
                {
                    dg.ContextMenuStrip.Items[2].Enabled = false;
                    dg.ContextMenuStrip.Items[1].Enabled = false;
                }
                else
                {
                    dg.ContextMenuStrip.Items[2].Enabled = true;
                    dg.ContextMenuStrip.Items[1].Enabled = true;
                }
                dg.ContextMenuStrip.Items[1].Tag = hi.RowIndex;
                dg.ContextMenuStrip.Items[2].Tag = dg.Name;
                dg.Rows[hi.RowIndex].Selected = true;
                dg.ContextMenuStrip.Tag = dg.Name;
                //dg.ContextMenuStrip = this.cmsComplaints;
            }
            else
            {
                //Rename heading
               
                dg.ContextMenuStrip.Items[0].Enabled = clsSharedVariables.RenameHeading;
                dg.ContextMenuStrip.Items[1].Enabled = false;
                dg.ContextMenuStrip.Items[2].Enabled = false;
                dg.ContextMenuStrip.Items[1].Tag = null;
                dg.ContextMenuStrip.Items[2].Tag = null;
                //dg.ContextMenuStrip = null;
            }

            hi = null;

        }

        #endregion

        private void NewPatient()
        {
            pnlRegisterSearch.Enabled = false;
            dgRegisterPatient.Enabled = false;
            btnPatientSave.Text = "Save";
            btnPatientSave.Enabled = true;
            ClearDG();
            ClearVitalSigns();
            miEnable(false);
            pnlEnable(false);
            pnlVisit.Controls.Clear();
            pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);
        }

        private void pnlEnable(bool Flag)
        {
            pnlVitalSigns.Visible = Flag;
            pnlMain.Visible = Flag;
            pnlPayment.Visible = Flag;
            pnlInfo.Visible = Flag;
            pnlAppointment.Visible = Flag;
            pnlAppointment.BringToFront();
            pnlInfo.SendToBack();

        }

        private void UnSelectedPatient()
        {
            pnlRegisterSearch.Enabled = true;
            dgRegisterPatient.Enabled = true;
            pnlRPatient.Enabled = false;
            btnPatientSave.Text = "Update";
            btnPatientSave.Enabled = false;
            ClearDG();
            pnlSize(panel1.Size.Width, pnlMain.Bottom);
            dtpFollowUpDate.Enabled = false;
            miEnable(false);
            pnlEnable(false);
            ClearGridRows(dgRegisterPatient);
        }

        private void SelectedPatient()
        {
            pnlRegisterSearch.Enabled = true;
            dgRegisterPatient.Enabled = true;
            btnPatientSave.Text = "Update";
            btnPatientSave.Enabled = true;
            ClearDG();
            miEnable(true);
            miPrint.Enabled = true;
            miHoldSave.Enabled = miConsultedSave.Enabled = false;
            pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);
            pnlEnable(true);
            dtpFollowUpDate.Enabled = true;
            CreateInstructionCHB();
        }

        private int DuplicatePRNOAuto()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            int RndVal = 0;
            Random rnd = new Random();

            RndVal = rnd.Next(0, 999999);

            objPatient.PRNO = txtRPRNo.Text = DateTime.Now.ToString("yy") + "-" + DateTime.Now.ToString("MM") + "-" + RndVal.ToString("000000");
            objConnection.Connection_Open();

            if (objPatient.GetAll(7).Table.Rows.Count == 0)
            {
                objConnection.Connection_Close();
                return 1;
            }
            else
            {
                objConnection.Connection_Close();
                return 0;
            }
        }

        private int DuplicatePRNO()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();

            objPatient.PRNO = txtRPRNo.Text;
            objConnection.Connection_Open();
            dv = objPatient.GetAll(7);

            if (!btnPatientSave.Text.Equals("Save"))
            {
                dv.RowFilter = "PatientID <>" + lblPatientID.Text;
               
            }
            if (dv.Count == 0)
            {
                objConnection.Connection_Close();
                return 1;
            }
            else
            {
                objConnection.Connection_Close();
                return 0;
            }
           
        }

        private void GetPatientLastVisitOPDID(int flag)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            
            
          
            objConnection.Connection_Open();
            objOPD.PRNO = txtPRNO.Text;
            DataView dv = new DataView();
       
       
          
            dv = objOPD.GetAll(4);
            objConnection.Connection_Close();

            if (dv.Table.Rows.Count != 0)
            {
                lblOPDID.Text = dv.Table.Rows[0][0].ToString();
                if (flag == 0 && dv.Table.Rows.Count == 1 && !lblSelect.Text.Equals("0"))
                {
                    //MessageBox.Show("No Previous Visit Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
               
                if (flag == 0)
                {
                    //MessageBox.Show("New Register Patient", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            objConnection = null;
            objOPD = null;
            dv = null;
        }

        private void CreateInstructionCHB()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLType objType = new clsBLType(objConnection);
            int x = 1, y = 0;
            int InstructionCount = 1;

            objConnection.Connection_Open();

            if (clsSharedVariables.Language.Equals("Eng"))
            {
                objType.Name = "EngInstruction";

                this.pnlInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objType.Name = "UrduInstruction";
                this.pnlInstructions.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                objType.Name = "FarsiInstruction";
                this.pnlInstructions.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

            }
            objType.PersonID = clsSharedVariables.UserID;
            DataView dvType = objType.GetAll(3);

            objConnection.Connection_Close();

            foreach (DataRow dr in dvType.Table.Rows)
            {
                if (dr["DefaultSel"].ToString().Equals("Y"))
                {
                    this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), true, true));
                }
                else
                {
                    this.pnlInstructions.Controls.Add(CHBProperties(x, y, "instruction" + InstructionCount.ToString(), dr["Description"].ToString(), false, true));
                }
                y = y + 25;
            }
            objConnection = null;
            objType = null;
            dvType = null;
        }

        private CheckBox CHBProperties(int x, int y, string chbName, string textVal, bool chbChecked, bool chbEnable)
        {
            CheckBox chb = new CheckBox();

            chb.CheckAlign = ContentAlignment.MiddleRight;
            chb.Location = new System.Drawing.Point(x, y);
            chb.Text = textVal.ToString();
            chb.AutoSize = true;

            if (clsSharedVariables.Language.Equals("Urdu"))
            {
                chb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                chb.Tag = "urdu";
                chb.Size = new System.Drawing.Size(pnlInstructions.Width - 20, 33);
                chb.RightToLeft = RightToLeft.No;
                chb.AutoSize = false;
                chb.Name = chbName;
            }
            else if (clsSharedVariables.Language.Equals("Eng"))
            {
                chb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                chb.Size = new System.Drawing.Size(350, 25);
                chb.RightToLeft = RightToLeft.Yes;
                chb.Name = clsSharedVariables.InItCaps(chbName);
                chb.AutoSize = true;
                ttOPD.SetToolTip(chb, chb.Text);
            }
            if (clsSharedVariables.Language.Equals("Farsi"))
            {
                chb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                chb.Tag = "urdu";
                chb.Size = new System.Drawing.Size(350, 33);
                chb.RightToLeft = RightToLeft.No;
                chb.AutoSize = false;
                chb.Name = chbName;
            }

            chb.Checked = chbChecked;
            chb.Enabled = chbEnable;
            chb.UseVisualStyleBackColor = true;
            chb.TextAlign = ContentAlignment.TopRight;

            return chb;
        }

        private void ControlepnlList(DataGridView dg)
        {
            pnlDGList.Visible = true;
            if (dg.Name.Equals(dgComplaints.Name) || dg.Name.Equals(dgInvestigation.Name))
            {
                ManagePnlList(dgSigns.Location, dgSigns.Size.Width, dgSigns.Size.Height + dgPlans.Size.Height, 11F);
            }
            //else if (dg.Name.Equals(dgSigns.Name) || dg.Name.Equals(dgPlans.Name))
            //{
            //  ManagePnlList(270, 1, 266, 341, 1, 1, 239, 20, 240, 0, 1, 23, 264, 315, 11F); 
            //}
            else
            {
                ManagePnlList(dgComplaints.Location, dgComplaints.Size.Width, dgComplaints.Size.Height + dgInvestigation.Size.Height, 11F);
            }
        }

        private void ManagePnlList(Point pnt, int pnlSX, int pnlSY, float FS)
        {
            pnlDGList.Location = pnt;
            pnlDGList.Size = new System.Drawing.Size(pnlSX, pnlSY);
            pnlDGList.BringToFront();

            //pnlDGList.Location = new Point(pnlLX, pnlLY);
            //pnlDGList.Size = new System.Drawing.Size(pnlSX, pnlSY);

            //txtSearch.Location = new Point(txtLX, txtLY);
            //txtSearch.Size = new System.Drawing.Size(txtSX, txtSY);

            //btnListPnl.Location = new Point(btnLX, btnLY);

            //dgList.Location = new Point(dgLX, dgLY);
            //dgList.Size = new System.Drawing.Size(dgSX, dgSY);
            foreach (DataGridViewRow dr in dgList.Rows)
            {
                dr.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", FS);
            }
        }

        private void HeadingDG(DataView dv)
        {
            //DataView tempDV = new DataView();
            try
            {
                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    switch (dv.Table.Rows[i]["Name"].ToString())
                    {
                        case "History":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgHistory.Columns["HistoryName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgHistory.Columns["HistoryName"].HeaderText = "History";
                                }
                                break;
                            }
                        case "Diagnosis":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgDiagnosis.Columns["DiagnosisName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgDiagnosis.Columns["DiagnosisName"].HeaderText = "Diagnosis";
                                }
                                break;
                            }
                        case "Signs":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgSigns.Columns["SignsName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgSigns.Columns["SignsName"].HeaderText = "Signs";
                                }
                                break;
                            }
                        case "Presenting Complaints":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgComplaints.Columns["ComplaintsName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgComplaints.Columns["ComplaintsName"].HeaderText = "Presenting Complaints";
                                }
                                break;
                            }
                        case "Plans":
                            {
                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgPlans.Columns["PlansName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgPlans.Columns["PlansName"].HeaderText = "Plans";
                                }
                                break;
                            }
                        case "Investigation":
                            {

                                if (!dv.Table.Rows[i]["Description"].ToString().Equals(""))
                                {
                                    dgInvestigation.Columns["InvestigatioName"].HeaderText = clsSharedVariables.InItCaps(dv.Table.Rows[i]["Description"].ToString());
                                }
                                else
                                {
                                    dgInvestigation.Columns["InvestigatioName"].HeaderText = "Investigation";
                                }
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void AssignDGTypeID(DataView dv)
        {
            try
            {
                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    switch (dv.Table.Rows[i]["Name"].ToString())
                    {
                        case "History":
                            {
                                dgHistory.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        case "Diagnosis":
                            {
                                dgDiagnosis.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        case "Signs":
                            {
                                dgSigns.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        case "Presenting Complaints":
                            {
                                dgComplaints.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        case "Plans":
                            {
                                dgPlans.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        case "Investigation":
                            {
                                dgInvestigation.Tag = dv.Table.Rows[i]["TypeID"].ToString();
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        /// <summary>
        /// Function insert the valus in selected grid for "Types", if record insert already it leave this w/o any             action
        /// </summary>
        /// <param name="dg">DatagridView Name</param>
        /// <param name="PrefCol">column Name of PrefID  w.r.t datagrid pass</param>
        /// <param name="RowVal">row index of datagrid of type list</param>
        /// <param name="NameCol">column Name of Name w.r.t datagrid pass</param>
        private void DuplicationCheck(ref DataGridView dg, string PrefCol, int RowVal, string NameCol, string DisRefID, string ImgColName)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                if (dg.Rows[i].Cells[PrefCol].Value != null && dg.Rows[i].Cells[PrefCol].Value.ToString().Equals(dgList.Rows[RowVal].Cells["PrefID"].Value.ToString()))
                {
                    return;
                }
            }
            int AddRow = -1;
            AddRow = dg.Rows.Add();
            dg.Rows[AddRow].Cells[PrefCol].Value = dgList.Rows[RowVal].Cells["PrefID"].Value.ToString();
            dg.Rows[AddRow].Cells[NameCol].Value = clsSharedVariables.InItCaps(dgList.Rows[RowVal].Cells["Pname"].Value.ToString());
            if (!DisRefID.Equals("~!@"))
            {
                if (dgList.Rows[RowVal].Cells["ListDisRefID"].Value.ToString().Equals(""))
                {
                    dg.Rows[AddRow].Cells[DisRefID].Value = "";
                    dg.Rows[AddRow].Cells[ImgColName].Value = Bitmap.FromFile(Application.StartupPath + "\\icons\\NA1.jpg");
                }
                else
                {
                    dg.Rows[AddRow].Cells[DisRefID].Value = dgList.Rows[RowVal].Cells["ListDisRefID"].Value.ToString();
                    dg.Rows[AddRow].Cells[ImgColName].Value = Bitmap.FromFile(Application.StartupPath + "\\icons\\IA1.jpg");
                }
            }
        }

        private void CheckForPrefrence(ref DataGridView dg, string PrefCol, string NameCol)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPreference objPreference = new clsBLPreference(objConnection);
            try
            {
                objConnection.Connection_Open();

                for (int i = 0; i < dg.Rows.Count - 1; i++)
                {
                    if (dg.Rows[i].Cells[PrefCol].Value == null && dg.Rows[i].Cells[NameCol].Value != null)
                    {
                        if (dg.Rows[i].Cells[NameCol].Value != null)
                        {
                            objPreference.TypeID = dg.Tag.ToString();
                            objPreference.Name = clsSharedVariables.InItCaps(dg.Rows[i].Cells[NameCol].Value.ToString());
                            objPreference.Description = clsSharedVariables.InItCaps(dg.Columns[NameCol].HeaderText);
                            InsertPreference(objConnection, objPreference);
                            dg.Rows[i].Cells[PrefCol].Value = objPreference.GetAll(3).Table.Rows[0][0].ToString();
                            dg.Rows[i].Tag = "0";
                        }
                        else
                        {
                            dg.Rows[i].Tag = "D";
                        }
                    }
                    else if (dg.Rows[i].Tag != null && dg.Rows[i].Tag.Equals("1"))
                    {
                        if (dg.Rows[i].Cells[NameCol].Value != null)
                        {
                            objPreference.PrefID = dg.Rows[i].Cells[PrefCol].Value.ToString();
                            objPreference.TypeID = dg.Tag.ToString();
                            objPreference.Name = clsSharedVariables.InItCaps(dg.Rows[i].Cells[NameCol].Value.ToString().Replace("'", "''"));
                            objPreference.Description = clsSharedVariables.InItCaps(NameCol);
                            UpdatePreference(objConnection, objPreference);
                            dg.Rows[i].Tag = "0";
                        }
                        else
                        {
                            dg.Rows[i].Tag = "D";
                        }
                    }
                    else
                    {
                        dg.Rows[i].Tag = "0";
                    }
                }
                for (int j = 0; j < dg.Rows.Count - 1; j++)
                {
                    if (dg.Rows[j].Tag.ToString().Equals("D"))
                    {
                        dg.Rows.RemoveAt(j);
                    }

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objPreference = null;
                objConnection = null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="Flag"> Define the Type(History,Diagnosis,Signs and Complaints)  </param>

        private int SelectMedicine(string MedicineID)
        {
            for (int i = 0; i < dgMedicine.Rows.Count; i++)
            {
                if (dgMedicine.Rows[i].Cells["Medname"].Value.ToString().Equals(MedicineID))
                {
                    return i;
                }
            }
            return -1;
        }

        private string MultiDosage(int idx)
        {
            idx = idx - 1;
            if (dgMedicine.Rows[idx].Cells["Medname"].Value == null)
            {
                return MultiDosage(idx);
            }
            return clsSharedVariables.InItCaps(dgMedicine.Rows[idx].Cells["Medname"].Value.ToString());
        }

        private void PrevoiusInfo()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);
            DataView dv = new DataView();

            clsBLDBConnection objConnection1 = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection1);
            DataView dv2 = new DataView();
            objConnection1.Connection_Open();
            objPatient.PRNO = txtPRNO.Text;
            dv2 = objPatient.GetAll(1);
            objConnection1.Connection_Close();

            if (dv2.Table.Rows.Count != 0)
            {
                objOPD.PatientID = dv2.Table.Rows[0][0].ToString();

            }
            objConnection.Connection_Open();
            objOPD.PRNO = txtPRNO.Text;
           
            if (objOPD.PatientID != "")
            {
                objOPD.UpdateOPD();
            }
            GetPatientLastVisitOPDID(0);
            ClearDG();
            //CreateInstructionCHB();
            ClearVitalSigns();

            objConnection.Connection_Open();
            
            objOPDMed.OPDID = objOPD.OPDID = lblOPDID.Text;
            if(lblOPDID.Text!="")
            {
                
                dv = objOPD.GetAll(3);
                FillPatientInfo(dv);
             
                dv = objOPDMed.GetAll(2);
                FillMedDosage(dv);
            }
           

            DataView dv1 = new DataView();
            objOPD.PRNO = txtPRNO.Text;
            dv1 = objOPD.GetAll(24);
            if (dv1.Table.Rows.Count != 0)
            {
             
                lblOPDID.Text = dv1.Table.Rows[0][0].ToString();
                objOPD.OPDID = lblOPDID.Text;
                dv = objOPD.GetAll(3);
               
                if (dv.Table.Rows[0]["BP"].ToString()!=""|| !dv.Table.Rows[0]["pulse"].ToString().Equals("") || !dv.Table.Rows[0]["Weight"].ToString().Equals("")
                    || !dv.Table.Rows[0]["Height"].ToString().Equals("") || dv.Table.Rows[0]["HeartRate"].ToString()!=""|| dv.Table.Rows[0]["RespiratoryRate"].ToString()!="")
                {
                    FillNewVitalInfo(dv);
                }

                    
               
            }
            objConnection.Connection_Close();

            objConnection = null;
            objOPD = null;
            dv = null;
        }
        private void FillNewVitalInfo(DataView dv)
        {
          
            txtBP.Text = dv.Table.Rows[0]["BP"].ToString().Split(' ')[0].ToString().Trim();

            if (!dv.Table.Rows[0]["Temprature"].ToString().Equals(""))
            {
                txtTemp.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[0].ToString().Trim();
                cboTempUnit.Text = dv.Table.Rows[0]["Temprature"].ToString().Split(' ')[1].ToString().Trim();
            }

            if (!dv.Table.Rows[0]["pulse"].ToString().Equals(""))
            {
                txtPulse.Text = dv.Table.Rows[0]["pulse"].ToString().Split(' ')[0].ToString().Trim();
            }
            //if (!dv.Table.Rows[0]["HeadCircumferences"].ToString().Equals(""))
            //{
            //    txtHeadCircum.Text = dv.Table.Rows[0]["HeadCircumferences"].ToString().Split(' ')[0].ToString().Trim();
            //}
            if (!dv.Table.Rows[0]["Weight"].ToString().Equals(""))
            {
                txtWeight.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[0].ToString().Trim();
                cboWeightUnit.Text = dv.Table.Rows[0]["Weight"].ToString().Split(' ')[1].ToString().Trim();
            }
            if (!dv.Table.Rows[0]["Height"].ToString().Equals(""))
            {
                txtHeight.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[0].ToString().Trim();
                cboHeightUnit.Text = dv.Table.Rows[0]["Height"].ToString().Split(' ')[1].ToString().Trim();
            }
          
                if (!dv.Table.Rows[0]["Payment"].ToString().Equals(""))
            {
                txtPayment.Text = dv.Table.Rows[0]["Payment"].ToString();
            }
            else
            {
                txtPayment.Text = clsSharedVariables.Payment; ;
            }
            txtNotes.Text = dv.Table.Rows[0]["Notes"].ToString();
            txtHeartRate.Text = dv.Table.Rows[0]["HeartRate"].ToString();
            txtRespiratoryRate.Text = dv.Table.Rows[0]["RespiratoryRate"].ToString();
            txtWeight_Leave(null, null);
        }

        private void PreviousVisitDataGrid(string str, DataGridView dg, string TypeName, string PrefID, string ImgColName, string DisRefID)
        {
            string[] arr;
            arr = str.Split(',');

            for (int index = 0; index <= arr.GetUpperBound(0); index++)
            {
                int flag = 0;
                for (int i = 0; i < dg.Rows.Count; i++)
                {
                    if (dg.Rows[i].Cells[TypeName].Value != null)
                    {
                        if (arr[index].ToString().Trim().Equals("") || arr[index].ToString().Trim().Equals(dg.Rows[i].Cells[TypeName].Value))
                        {
                            flag = 1;
                            break;
                        }
                    }
                }
                if (flag == 0)
                {
                    int AddRow = -1;

                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    clsBLPreference objPref = new clsBLPreference(objConnection);
                    DataView dv;
                    try
                    {
                        objPref.PersonID = clsSharedVariables.UserID;
                        objPref.TypeID = dg.Tag.ToString();
                        objPref.Name = clsSharedVariables.InItCaps(arr[index].ToString().Trim());

                        objConnection.Connection_Open();
                        dv = objPref.GetAll(4);
                        objConnection.Connection_Close();


                        AddRow = dg.Rows.Add();
                        if (dv.Table.Rows.Count != 0)
                        {
                            dg.Rows[AddRow].Cells[PrefID].Value = dv.Table.Rows[0]["PrefID"].ToString();
                            if (!ImgColName.Equals(""))
                            {
                                dg.Rows[AddRow].Cells[DisRefID].Value = dv.Table.Rows[0]["DiseaseIDref"].ToString();
                                if (dv.Table.Rows[0]["DiseaseIDref"].ToString().Equals(""))
                                {
                                    dg.Rows[AddRow].Cells[ImgColName].Value = Bitmap.FromFile(Application.StartupPath + "\\Icons\\NA1.jpg");
                                }
                                else
                                {
                                    dg.Rows[AddRow].Cells[ImgColName].Value = Bitmap.FromFile(Application.StartupPath + "\\Icons\\IA1.jpg"); ;
                                }
                            }
                        }
                        else
                        {
                            dg.Rows[AddRow].Cells[PrefID].Value = null;
                            if (!ImgColName.Equals(""))
                            {
                                dg.Rows[AddRow].Cells[DisRefID].Value = "";
                                dg.Rows[AddRow].Cells[ImgColName].Value = Bitmap.FromFile(Application.StartupPath + "\\Icons\\NA1.jpg");
                            }
                        }
                        dg.Rows[AddRow].Cells[TypeName].Value = clsSharedVariables.InItCaps(arr[index].ToString().Trim());
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }
            }
        }

        private string concatinateType(DataGridView dg, string cellType)
        {
            string str = "";
            for (int i = 0; i < dg.Rows.Count - 1; i++)
            {
                if (dg.Rows[i].Cells[cellType].Value != null)
                {
                    str += clsSharedVariables.InItCaps(dg.Rows[i].Cells[cellType].Value.ToString());
                    //if (i != dg.Rows.Count - 2)
                    //{
                    str += " , ";
                    //}
                }
            }
            if (str.Length > 3)
            {
                str = str.Substring(0, str.Length - 3);
            }

            return str;
        }

        private void pbPatient_DoubleClick(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();

            try
            {
                OFD.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG| All files (*.*)|*.*";
                OFD.ShowDialog();
                if (OFD.FileName != "")
                {
                    pbPatient.ImageLocation = OFD.FileName;
                    pbHold.ImageLocation = OFD.FileName;
                    pbConsulted.ImageLocation = OFD.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Load : " + ex.Message);
            }
            finally
            {
                OFD.Dispose();
            }
        }

        private void ResizeHold()
        {
            ClearDG();
            miEnable(false);
            pnlEnable(false);
            pnlSize(panel1.Size.Width, pnlMain.Bottom);
            btnHoldUpdate.Enabled = false;
            ClearGridRows(dgHold);
        }

        private void miEnable(bool Flag)
        {
            miPicture.Enabled = Flag;
            miDoc.Enabled = Flag;
            ViewPatientDocument.Enabled = Flag;
            miVideo.Enabled = Flag;
            miHoldSave.Enabled = Flag;
            miConsultedSave.Enabled = Flag;
            miPrint.Enabled = Flag;
            miSMS.Enabled = Flag;
            miEmail.Enabled = Flag;
        }

        private void ResizeConsulted()
        {
            ClearDG();
            pnlSize(panel1.Size.Width, pnlMain.Bottom);
            miEnable(false);
            pnlEnable(false);
            btnCUpdate.Enabled = false;
            ClearGridRows(dgConsulted);
        }
        //private void PrintPerscription()
        //{
        //    frmReport objRep = new frmReport(this);
        //    try
        //    {
        //        objRep.ReportReference = "OPD_001_01";
        //        if (!lblOPDID.Text.Equals("") && !lblOPDID.Text.Equals("lblOPDID"))
        //        {
        //            objRep.OPDID = lblOPDID.Text;
        //        }
        //        else
        //        {
        //            objRep.PatientID = lblPatientID.Text;
        //        }
        //        if (dgHistory.Rows.Count != 1)
        //        {
        //            objRep.History = clsSharedVariables.InItCaps(dgHistory.Columns[0].HeaderText) + " : \n";
        //        }
        //        if (dgComplaints.Rows.Count != 1)
        //        {
        //            objRep.Complaints = clsSharedVariables.InItCaps(dgComplaints.Columns[0].HeaderText) + " : \n";
        //        }
        //        if (dgSigns.Rows.Count != 1 || !txtTemp.Text.Equals("") || !txtBP.Text.Trim().Equals(@"/") || !txtPulse.Text.Equals("") || /*!txtHeadCircum.Text.Equals("") ||*/ !txtWeight.Text.Equals("") || !txtHeight.Text.Equals("") || !txtHeartRate.Text.Equals("") || !txtRespiratoryRate.Text.Equals(""))
        //        {
        //            objRep.Signs = clsSharedVariables.InItCaps(dgSigns.Columns[0].HeaderText) + " : \n";
        //        }
        //        if (dgPlans.Rows.Count != 1)
        //        {
        //            objRep.Plans = clsSharedVariables.InItCaps(dgPlans.Columns[0].HeaderText) + " : \n";
        //        }
        //        if (dgInvestigation.Rows.Count != 1)
        //        {
        //            objRep.Investigation = clsSharedVariables.InItCaps(dgInvestigation.Columns[0].HeaderText) + " : \n";
        //        }
        //        if (dgDiagnosis.Rows.Count != 1)
        //        {
        //            objRep.Diagnosis = clsSharedVariables.InItCaps(dgDiagnosis.Columns[0].HeaderText) + " : \n";
        //        }
        //        objRep.DisplayOpt = "Print";

        //        objRep.ShowDialog();


        //    }
        //    catch (Exception Exc)
        //    {
        //        MessageBox.Show(Exc.Message + " Report cannot be display due to internal error.");
        //    }
        //    finally
        //    {
        //        objRep.Dispose();
        //    }
        //}

        private void PrintPerscription()
        {
            PrintValue Print = new PrintValue();

            Print.StartPosition = FormStartPosition.CenterScreen;
            Print.ShowDialog();
            var value = Print.GetPrintValue();
            frmReport objRep = new frmReport(this);
            objRep.PrintValue = value;
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            objPatient.PatientID = lblPatientID.Text;
            objConnection.Connection_Open();
            DataView dvPatient = objPatient.GetAll(18);
            DataView dvOpd = objPatient.GetAll(19);
            objConnection.Connection_Close();


            try
            {
                //objRep.ReportReference = "MedicinePerscription";  //For New Clinic Medicine Perscription
                objRep.ReportReference = "PatientPerscription";

                if (!lblOPDID.Text.Equals("") && !lblOPDID.Text.Equals("lblOPDID"))
                {
                    objRep.OPDID = lblOPDID.Text;
                }
                else
                {

                    objRep.PatientID = lblPatientID.Text;
                }
                if (dvPatient.Table.Rows.Count != 0)
                {
                    objRep.PRNo = dvPatient.Table.Rows[0]["PRNO"].ToString();
                    objRep.UserName = dvPatient.Table.Rows[0]["UserName"].ToString();
                    objRep.Password = dvPatient.Table.Rows[0]["Password"].ToString();
                    objRep.PatientName = clsSharedVariables.InItCaps(dvPatient.Table.Rows[0]["Name"].ToString());
                    objRep.Age = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dvPatient.Table.Rows[0]["DOB"].ToString()).Year).ToString() + " Years";
                    if (dvPatient.Table.Rows[0]["Gender"].ToString() == "F")
                    {
                        objRep.Gender = "Female";
                    }
                    else
                    {
                        objRep.Gender = "Male";
                    }
                    //objRep.VisitDate = Convert.ToDateTime(dvOpd.Table.Rows[0]["VisitDate"]).ToString("dd MMMM yyyy");
                    objRep.VisitDate = DateTime.Now.ToString("dd MMMM yyyy");
                }

                if (dgHistory.Rows.Count != 1)
                {
                    string s = "";
                    for (int i = 0; i < dgHistory.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgHistory.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgHistory.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    objRep.History = clsSharedVariables.InItCaps(s);

                    //objRep.History = clsSharedVariables.InItCaps(dgHistory.Columns[0].HeaderText) + " : \n";
                }
                objRep.Instruction = "";
                foreach (Control c1 in pnlInstructions.Controls)
                {
                    CheckBox chb = (CheckBox)c1;
                    if (chb.Checked)
                    {
                        if (clsSharedVariables.Language.Equals("Eng"))
                        {
                            objRep.Instruction += clsSharedVariables.InItCaps(chb.Text) + "    ";
                        }
                        else if (clsSharedVariables.Language.Equals("Urdu"))
                        {
                            objRep.Instruction += chb.Text.Replace(@"\", "\\") + "    ";
                        }
                        else if (clsSharedVariables.Language.Equals("Farsi"))
                        {
                            objRep.Instruction += chb.Text.Replace(@"\", "\\") + "    ";
                        }
                    }
                }
                if (dgComplaints.Rows.Count != 1)
                {
                    string s = "";
                    for (int i = 0; i < dgComplaints.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgComplaints.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgComplaints.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    objRep.Complaints = clsSharedVariables.InItCaps(s);
                    //objRep.Complaints = clsSharedVariables.InItCaps(dgComplaints.Columns[0].HeaderText) + " : \n";
                }
                var s2 = "";
                 if (txtBP.Text != ""){ s2 =s2+ "blood pressure "+txtBP.Text + " \n"; }
                if (txtTemp.Text != "") { s2 = s2 + "temperature "+ txtTemp.Text+" "+cboTempUnit.Text + "  \n"; }
                if (txtPulse.Text != "") { s2 = s2 + "Pulse "+ txtPulse.Text + "   \n"; }
                if (txtHeartRate.Text != "") { s2 = s2 + "Heart Rate "+ txtHeartRate.Text + "  \n"; }
                if (txtWeight.Text != "") { s2 = s2 +"Weight "+ txtWeight.Text + " " + cboWeightUnit.Text + "\n"; }
                if (txtHeight.Text != "") { s2 = s2 +"Height "+ txtHeight.Text + " " + cboHeightUnit.Text + "\n"; }
                //
                //if (txtRespiratoryRate.Text != "") { s2 = s2 + txtRespiratoryRate.Text + "\n"; }
                ////if (txtBP.Text != "") { s2 = s2 + txtBP.Text + "\n"; }
                //if (txtBP.Text != "") { s2 = s2 + txtBP.Text + "\n"; }
                //if (txtBP.Text != "") { s2 = s2 + txtBP.Text + "\n"; }
                //if (txtBP.Text != "") { s2 = s2 + txtBP.Text + "\n"; }
                //if (txtBP.Text != "") { s2 = s2 + txtBP.Text + "\n"; }


                if (dgSigns.Rows.Count != 1 || !txtTemp.Text.Equals("") || !txtBP.Text.Trim().Equals(@"/") || !txtPulse.Text.Equals("") || /*!txtHeadCircum.Text.Equals("") ||*/ !txtWeight.Text.Equals("") || !txtHeight.Text.Equals("") || !txtHeartRate.Text.Equals("") || !txtRespiratoryRate.Text.Equals(""))
                {
                    string s = s2;
                    for (int i = 0; i < dgSigns.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgSigns.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgSigns.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    objRep.Signs = clsSharedVariables.InItCaps(s);
                    //objRep.Signs = clsSharedVariables.InItCaps(dgSigns.Columns[0].HeaderText) + " : \n";
                }
                if (dgPlans.Rows.Count != 1)
                {
                    string s = "";
                    for (int i = 0; i < dgPlans.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgPlans.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgPlans.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    objRep.Plans = clsSharedVariables.InItCaps(s);
                    //objRep.Plans = clsSharedVariables.InItCaps(dgPlans.Columns[0].HeaderText) + " : \n";
                }
                if (dgInvestigation.Rows.Count  != 1)
                {
                    string s = "";
                    for (int i = 0; i < dgInvestigation.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgInvestigation.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgInvestigation.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    int count = 1;
                    int x = 1;
                    //for (int i = 0; i < dgInvestigation.Rows.Count; i++)
                    //{
                    //    try
                    //    {
                    //        if (x != 3)
                    //        {
                    //            if (dgInvestigation.Rows[i].Cells[0].Value.ToString() != "")
                    //            {
                    //                s = s + count + " : " + dgInvestigation.Rows[i].Cells[0].Value + "\t\t";

                    //                x++;
                    //                count++;

                    //            }

                    //        }
                    //        else
                    //        {
                    //            if (dgInvestigation.Rows[i].Cells[0].Value.ToString() != "")
                    //            {
                    //                s = s + " \n\n" + +count + " : " + dgInvestigation.Rows[i].Cells[0].Value + "\t\t";
                    //                x = 2;
                    //                count++;
                    //            }
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }
                    //}

                   
                    objRep.Investigation = clsSharedVariables.InItCaps(s);
                }
                if (dgDiagnosis.Rows.Count != 1)
                {
                    string s = "";
                    for (int i = 0; i < dgDiagnosis.Rows.Count; i++)
                    {
                        try
                        {
                            if (dgDiagnosis.Rows[i].Cells[0].Value.ToString() != "")
                            {
                                s = s + dgDiagnosis.Rows[i].Cells[0].Value + "\n";

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    objRep.Diagnosis = clsSharedVariables.InItCaps(s);
                }
                objRep.DisplayOpt = "Print";

                objRep.ShowDialog();


            }
            catch (Exception Exc)
            {
                MessageBox.Show(Exc.Message + " Report cannot be display due to internal error.");
            }
            finally
            {
                objRep.Dispose();
            }
        }

        private void PerscriptionPreview(string OPDID, string opt)
        {
            frmReport objRep = new frmReport(this);
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            objPatient.PatientID = lblPatientID.Text;
            objRep.OPDID = OPDID;
            objConnection.Connection_Open();
            DataView dvPatient = objPatient.GetAll(18);
           
            objConnection.Connection_Close();
            ////////////////////////////////////////////////////////////
            clsBLDBConnection objConnection1 = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection1);
          
           
            objConnection1.Connection_Open();
            objOPD.OPDID = OPDID;
            DataView dv = objOPD.GetAll(3);
        
            try
            {
                //objRep.ReportReference = "MedicinePerscription";  //For New Clinic Medicine Perscription
                objRep.ReportReference = "PatientPerscription";
                var s2 = "";
                var BloodPresure = dv.Table.Rows[0]["BP"].ToString();
                var Temprature = dv.Table.Rows[0]["Temprature"].ToString();
                var pulse = dv.Table.Rows[0]["Pulse"].ToString();
                var HeartRate = dv.Table.Rows[0]["HeartRate"].ToString();
                var Weight = dv.Table.Rows[0]["Weight"].ToString();
                var Height = dv.Table.Rows[0]["Height"].ToString();
                if (BloodPresure != "") { s2 = s2 + "blood pressure " + BloodPresure + " \n"; }
                if (Temprature != "") { s2 = s2 + "temperature " + Temprature + "  \n"; }
                if (pulse != "") { s2 = s2 + "Pulse " + pulse + "   \n"; }
                if (HeartRate != "") { s2 = s2 + "Heart Rate " + HeartRate + "  \n"; }
                if (Weight != "") { s2 = s2 + "Weight " + Weight + "\n"; }
                if (Height != "") { s2 = s2 + "Height " + Height + "\n"; }

                if (dvPatient.Table.Rows.Count != 0)
                {
                    objRep.PRNo = dvPatient.Table.Rows[0]["PRNO"].ToString();
                    objRep.UserName = dvPatient.Table.Rows[0]["UserName"].ToString();
                    objRep.Password = dvPatient.Table.Rows[0]["Password"].ToString();
                    objRep.PatientName = clsSharedVariables.InItCaps(dvPatient.Table.Rows[0]["Name"].ToString());
                    objRep.Age = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dvPatient.Table.Rows[0]["DOB"].ToString()).Year).ToString() + "Years";
                    if (dvPatient.Table.Rows[0]["Gender"].ToString() == "F")
                    {
                        objRep.Gender = "Female";
                    }
                    else
                    {
                        objRep.Gender = "Male";
                    }
                    objRep.VisitDate = Convert.ToDateTime(dv.Table.Rows[0]["VisitDate"]).ToString("dd MMMM yyyy");

                }

                if (dv.Table.Rows[0]["History"].ToString()!="")
                {
                    string s = dv.Table.Rows[0]["History"].ToString().Replace(',', '\n');
                   
                    objRep.History = clsSharedVariables.InItCaps(s);

                    //objRep.History = clsSharedVariables.InItCaps(dgHistory.Columns[0].HeaderText) + " : \n";
                }
                if (dv.Table.Rows[0]["Instruction"].ToString() != "")
                {
                    
                    string s = dv.Table.Rows[0]["Instruction"].ToString();

                     objRep.Instruction= s;

                    //objRep.History = clsSharedVariables.InItCaps(dgHistory.Columns[0].HeaderText) + " : \n";
                }
                if (dv.Table.Rows[0]["PresentingComplaints"].ToString()!="")
                {
                    string s = dv.Table.Rows[0]["PresentingComplaints"].ToString().Replace(',', '\n');
                
                    objRep.Complaints = clsSharedVariables.InItCaps(s);
                    //objRep.Complaints = clsSharedVariables.InItCaps(dgComplaints.Columns[0].HeaderText) + " : \n";
                }
                if (dv.Table.Rows[0]["Signs"].ToString() != "")
                {
                    string s = s2 + "\n" + dv.Table.Rows[0]["Signs"].ToString().Replace(',', '\n');
                    objRep.Signs = clsSharedVariables.InItCaps(s);
                    //objRep.Signs = clsSharedVariables.InItCaps(dgSigns.Columns[0].HeaderText) + " : \n";
                }
                else
                {
                    string s = s2;
                    objRep.Signs = clsSharedVariables.InItCaps(s);
                }
                if (dv.Table.Rows[0]["DisposalPlan"].ToString()!="")
                {
                    string s = dv.Table.Rows[0]["DisposalPlan"].ToString().Replace(',', '\n');
                   
                    objRep.Plans = clsSharedVariables.InItCaps(s);
                    //objRep.Plans = clsSharedVariables.InItCaps(dgPlans.Columns[0].HeaderText) + " : \n";
                }
                if (dv.Table.Rows[0]["Investigation"].ToString()!="")
                {
                    string s = dv.Table.Rows[0]["Investigation"].ToString().Replace(',','\n');
                    
                    objRep.Investigation = clsSharedVariables.InItCaps(s);
                }
                if (dv.Table.Rows[0]["Diagnosis"].ToString()!="")
                {
                    string s = dv.Table.Rows[0]["Diagnosis"].ToString().Replace(',', '\n');
                   
                    objRep.Diagnosis = clsSharedVariables.InItCaps(s);
                }
                objRep.DisplayOpt = "Print";

                objRep.ShowDialog();


            }
            catch (Exception Exc)
            {
                MessageBox.Show(Exc.Message + " Report cannot be display due to internal error.");
            }
            finally
            {
                objRep.Dispose();
            }
        }

        private void PaymentReport()
        {
            frmRptBill objRptBil = new frmRptBill(this);
            try
            {
                objRptBil.ReportReference = "OPD_001_03";
                objRptBil.ShowDialog();
            }
            catch (Exception Exc)
            {
                MessageBox.Show(Exc.Message + " Report cannot be display due to internal error.");
            }
            finally
            {
                objRptBil.Dispose();
            }
        }

        private void CalculateBMI()
        {
            Validation objValid = new Validation();
            Double Height = 0;
            Double Weight = 0;
            Double BMI = 0;

            if (!txtWeight.Text.Trim().Equals("") && !txtHeight.Text.Trim().Equals(""))
            {
                if (objValid.IsNumber(txtWeight.Text.Trim()) && objValid.IsNumber(txtHeight.Text.Trim()))
                {
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        Height = Convert.ToDouble(txtHeight.Text) * 12;//Convert in Inches
                    }
                    else
                    {
                        Height = Convert.ToDouble(txtHeight.Text);
                    }
                    if (!cboWeightUnit.Text.Equals("Pounds"))
                    {
                        Weight = Convert.ToDouble(txtWeight.Text) * 2.020462262;//convert in pound
                    }
                    else
                    {
                        Weight = Convert.ToDouble(txtWeight.Text);
                    }

                    BMI = (Weight * 703) / (Height * Height);
                    txtBMI.Text = BMI.ToString(".00");

                    if (BMI < 18.5)
                    {
                        txtBMIStatus.Text = "Underweight";
                    }
                    else if (BMI > 18.5 && BMI < 24.9)
                    {
                        txtBMIStatus.Text = "Normal";
                    }
                    else if (BMI > 24.9 && BMI < 29.9)
                    {
                        txtBMIStatus.Text = "Overweight";
                    }
                    else if (BMI > 29.9)
                    {
                        txtBMIStatus.Text = "Above Obese";
                    }
                }
            }
            else
            {
                txtBMI.Text = "";
                txtBSA.Text = "";
                txtBMIStatus.Text = "";
            }
        }

        private void CalculateBSA()
        {
            Validation objValid = new Validation();
            Double Height = 0;
            Double Weight = 0;
            Double BSA = 0;

            if (!txtWeight.Text.Trim().Equals("") && !txtHeight.Text.Trim().Equals(""))
            {
                if (objValid.IsNumber(txtWeight.Text.Trim()) && objValid.IsNumber(txtHeight.Text.Trim()))
                {
                    if (cboHeightUnit.Text.Equals("Feet"))
                    {
                        Height = Convert.ToDouble(txtHeight.Text) * 12;//Convert in Inches
                    }
                    else
                    {
                        Height = Convert.ToDouble(txtHeight.Text);
                    }
                    if (!cboWeightUnit.Text.Equals("Pounds"))
                    {
                        Weight = Convert.ToDouble(txtWeight.Text) * 2.020462262;//convert in pound
                    }
                    else
                    {
                        Weight = Convert.ToDouble(txtWeight.Text);
                    }

                    BSA = Math.Sqrt(((Weight * Height) / 3131));
                    txtBSA.Text = BSA.ToString(".00");
                }
                else
                {
                    txtBMI.Text = "";
                    txtBSA.Text = "";
                    txtBMIStatus.Text = "";
                }
            }
        }

        private void miVideo_Click(object sender, EventArgs e)
        {
            frmVideo objVdo = new frmVideo(lblPatientID.Text, this);
            if (!miRegistration.Enabled)
            {
                objVdo.PatientName = txtPatientName.Text;
            }
            else if (!miHold.Enabled)
            {
                objVdo.PatientName = txtHName.Text;
            }
            else if (!miConsulted.Enabled)
            {
                objVdo.PatientName = txtCName.Text;
            }
            objVdo.Show();
            this.Hide();
        }

        private void dgMedicine_DoubleClick(object sender, EventArgs e)
        {
            btnMedicine_Click(sender, e);
        }

        private void CreateVisitLB(string status)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            int x = 15, y = 5;
            int VisitCount = 1;
            pnlSerachByPRNo.Visible = false;
            pnlVisit.Controls.Clear();
            objConnection.Connection_Open();
            objOPD.PatientID = lblPatientID.Text;

            if (!lblSelect.Tag.ToString().Equals("1"))
            {
                objOPD.OPDID = lblOPDID.Text;
            }

            //objOPD.Status = status;
            DataView dvVisit = objOPD.GetAll(23);

            lblVisitCount.Text = " " + dvVisit.Table.Rows.Count.ToString() + " ";

            objConnection.Connection_Close();

            foreach (DataRow dr in dvVisit.Table.Rows)
            {
                //this.pnlVisit.Controls.Add(lblProperties(x, y, "lbl" + VisitCount.ToString(), VisitCount.ToString() + ".  " + dr["visitdate"].ToString()));
                if (VisitCount % 2 != 0)
                {
                    //this.pnlVisit.Controls.Add(btnProperties(2, y - 5, "btn" + VisitCount.ToString(), dr["visitdate"].ToString(), dr["opdid"].ToString(), dr["History"].ToString()));
                    this.pnlVisit.Controls.Add(btnProperties(3, y - 5, "lbtn" + VisitCount.ToString(), dr["visitdate"].ToString(), dr["opdid"].ToString(), dr["History"].ToString()));
                }
                else
                {
                    //this.pnlVisit.Controls.Add(btnProperties(87, y - 5, "btn" + VisitCount.ToString(), dr["visitdate"].ToString(), dr["opdid"].ToString(), dr["History"].ToString()));
                    this.pnlVisit.Controls.Add(btnProperties(88, y - 5, "lbtn" + VisitCount.ToString(), dr["visitdate"].ToString(), dr["opdid"].ToString(), dr["History"].ToString()));
                    y = y + 18;
                }
                VisitCount++;
            }
            objConnection = null;
            objOPD = null;
            dvVisit = null;
        }

        private Button btnProperties(int x, int y, string btnName, string textVal, string textTag, string textTooltip)
        {
            Button btn = new Button();

            btn.AutoSize = false;
            btn.Location = new System.Drawing.Point(x, y);
            btn.Name = btnName;
            btn.TextAlign = ContentAlignment.TopCenter;
            btn.Size = new System.Drawing.Size(85, 18);
            btn.TabStop = false;
            btn.TextAlign = ContentAlignment.TopCenter;
            if (!textVal.Equals(""))
            {
                btn.Text = textVal;
            }
            btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btn.Tag = textTag;
            btn.Click += new EventHandler(btnVisit_Click);

            this.ttOPD.SetToolTip(btn, "History" + " : " + textTooltip);

            return btn;
        }

        private Label lblProperties(int x, int y, string lblName, string textVal)
        {
            Label lbl = new Label();

            lbl.AutoSize = true;
            lbl.Location = new System.Drawing.Point(x, y);
            lbl.Name = lblName;
            lbl.TabStop = false;
            lbl.Text = textVal;
            lbl.BackColor = Color.Transparent;
            lbl.Font = new Font("Arial", lbl.Font.Size, FontStyle.Regular, GraphicsUnit.Point);//  float.Parse("9")

            return lbl;
        }

        private void btnVisit_Click(object sender, EventArgs e)
        {
            Button btn = new Button();
            btn = (Button)sender;

            //lblOPDID.Text = ll.Tag.ToString();
            PerscriptionPreview(btn.Tag.ToString(), "Display");
            //PrevoiusInfo();
        }

        private void miHoldSave_Click(object sender, EventArgs e)
        {
            if (lblSelect.Text.Equals("0"))
            {
                txtPRNO.Focus();
                if (lblSelect.Tag.Equals("1"))
                {
                    if (InsertOPD("H", ""))
                    {
                        for (int i = 0; i < dgMedicine.Rows.Count; i++)
                        {
                            InsertOPDMedicine(i);
                            if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                            {
                                UpdateMedicineNumTime(i);
                            }
                        }
                        InsertOPDDetail();
                        //MessageBox.Show("Patient Hold \" " + txtPatientName.Text + " \" Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (lblSelect.Tag.Equals("0"))
                {
                    if (UpdateOPD("H"))//Update 
                    {
                        UpdateOPDMed();//Update Medicine dosage
                        InsertOPDDetail();
                        for (int i = 0; i < dgMedicine.Rows.Count; i++)
                        {
                            if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                            {
                                UpdateMedicineNumTime(i);
                            }
                        }
                        //MessageBox.Show("Patient Hold \" " + txtHName.Text + " \" Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else if (lblSelect.Text.Equals("1") || lblSelect.Text.Equals("2"))
            {
                if (UpdateOPD("H"))//Update 
                {
                    UpdateOPDMed();//Update Medicine dosage
                    InsertOPDDetail();
                    for (int i = 0; i < dgMedicine.Rows.Count; i++)
                    {
                        if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                        {
                            UpdateMedicineNumTime(i);
                        }
                    }
                    //MessageBox.Show("Patient Hold \" " + txtHName.Text + " \" Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            //if (sender.ToString().Equals("Hold"))
            //else if (lblSelect.Text.Equals("2"))
            //{
            //    if (UpdateOPD("C"))//Update 
            //    {
            //        UpdateOPDMed();//Update Medicine dosage
            //        MessageBox.Show("Consulted Patient Save \" " + txtHName.Text + " \" Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}

            miRegistration_Click(miRegistration, e);
            LinkLabel.Link ll = new LinkLabel.Link();
            LinkLabelLinkClickedEventArgs ee = new LinkLabelLinkClickedEventArgs(ll);
            lklTdaySummary_LinkClicked(lklTdaySummary, ee);
            dgRegisterPatient.ClearSelection();

            pnlSerachByPRNo.Visible = true;
            pnlSerachByPRNo.BringToFront();
            txtSearchByPRNo.Focus();
            txtSearchByPRNo.SelectionStart = 0;

            ll = null;
            ee = null;
        }

        private void miConsultedSave_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();

            objConnection.Connection_Open();
            objConnection.Transaction_Begin();
            if (lblSelect.Text.Equals("0"))
            {
                txtPRNO.Focus();
                if (lblSelect.Tag.Equals("1"))
                {
                    if (InsertOPD("C", ""))
                    {
                        for (int i = 0; i < dgMedicine.Rows.Count; i++)
                        {
                            InsertOPDMedicine(i);
                            if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                            {
                                UpdateMedicineNumTime(i);
                            }
                        }
                        InsertOPDDetail();
                    }
                }
                else if (lblSelect.Tag.Equals("0"))
                {
                    if (UpdateOPD("C"))
                    {
                        UpdateOPDMed();//Update Medicine dosage
                    }
                    InsertOPDDetail();
                    for (int i = 0; i < dgMedicine.Rows.Count; i++)
                    {
                        if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                        {
                            UpdateMedicineNumTime(i);
                        }
                    }
                }

            }
            else if (lblSelect.Text.Equals("1") || lblSelect.Text.Equals("2"))
            {
                if (UpdateOPD("C"))
                {
                    UpdateOPDMed();//Update Medicine dosage
                    InsertOPDDetail();
                }
                for (int i = 0; i < dgMedicine.Rows.Count; i++)
                {
                    if (dgMedicine.Rows[i].Cells["Mode"].Value != null && dgMedicine.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                    {
                        UpdateMedicineNumTime(i);
                    }
                }
            }
            //if (lblSelect.Text.Equals("1"))
            //{
            //    PrintPerscription();
            //}
            if (sender.ToString().Equals("&Consulted"))
            {
                if (lblSelect.Text.Equals("0") || lblSelect.Text.Equals("2"))
                {
                    miRegistration_Click(miRegistration, e);
                    LinkLabel.Link ll = new LinkLabel.Link();
                    LinkLabelLinkClickedEventArgs ee = new LinkLabelLinkClickedEventArgs(ll);
                    lklTdaySummary_LinkClicked(lklTdaySummary, ee);
                    ll = null;
                    ee = null;
                    dgRegisterPatient.ClearSelection();
                }
                else if (lblSelect.Text.Equals("1"))
                {
                    miHold_Click(miHold, EventArgs.Empty);
                    dgHold.ClearSelection();
                }
            }
            //miRegistration_Click(miRegistration, e);

            pnlSerachByPRNo.Visible = true;
            pnlSerachByPRNo.BringToFront();
            txtSearchByPRNo.Focus();
            txtSearchByPRNo.SelectionStart = 0;
        }
        private void txtVitalSign_Enter(object sender, EventArgs e)
        {
            TextBox txt = new TextBox();
            txt = (TextBox)sender;

            txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
        }

        private void txtVitalSign_Leave(object sender, EventArgs e)
        {
            TextBox txt = new TextBox();
            txt = (TextBox)sender;
            if (txt != null)
            {
                txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
                txt.SelectionStart = 0;
            }
        }

        private void txtBP_Leave(object sender, EventArgs e)
        {
            txtBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
        }

        private void txtBP_Enter(object sender, EventArgs e)
        {
            txtBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            txtBP.SelectionStart = 0;
        }

        private void btnPreviousVisit_Click(object sender, EventArgs e)
        {
            
            lblOPDID.Text = "";
            lblOPDID.Tag = "LV";
            GetPatientLastVisitOPDID(0);
            //lblSelect.Tag = "0";
            if (!lblOPDID.Text.Equals(""))
            {
                PrevoiusInfo();
            }
            
        }

        private void miPrint_Click(object sender, EventArgs e)
        {
            txtPRNO.Focus();
            miConsultedSave_Click(sender, e);
            if (lblOPDID.Text == "")
            {
                PrintPerscription();
            }
            else
            {
                PerscriptionPreview(lblOPDID.Text, "");
            }
            miRegistration_Click(miRegistration, e);
            LinkLabel.Link ll = new LinkLabel.Link();
            LinkLabelLinkClickedEventArgs ee = new LinkLabelLinkClickedEventArgs(ll);
            lklTdaySummary_LinkClicked(lklTdaySummary, ee);
            ll = null;
            ee = null;
            dgRegisterPatient.ClearSelection();
        }

        private void GetSummary(clsBLDBConnection objConnection)
        {
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();
            try
            {
                dv = objOPD.GetAll(7);
                if (dv.Table.Rows.Count == 0)
                {
                    lblTotalPatient.Text = "0";
                    lblTotalAmount.Text = "0";
                }
                else
                {
                    lblTotalPatient.Text = dv.Table.Rows.Count.ToString();
                    int Tpay = 0;
                    for (int i = 0; i < dv.Table.Rows.Count; i++)
                    {
                        if (!dv.Table.Rows[i]["payment"].ToString().Equals(""))
                        {
                            Tpay = Tpay + Convert.ToInt16(dv.Table.Rows[i]["payment"].ToString());
                        }
                    }
                    lblTotalAmount.Text = Tpay.ToString();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                dv.Dispose();
                objOPD = null;
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                objConnection.Connection_Open();

                btnNewPatient_Click(btnNewPatient, EventArgs.Empty);
                //miPrintPreview.Enabled = false;
                pnlVisit.Visible = false;
                pnlTotalVisit.Visible = false;

                GetPatientDetail();
                GetPatientAlerAdver();
                GetPatientProcAdmssion();

                pnlSelPatientDetail.Visible = false;
                pnlSelPatientHead.Visible = false;

                GetSummary(objConnection);
                pnlVisit.Controls.Clear();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void btnConsulted_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                objConnection.Connection_Open();

                ResizeConsulted();
                clearConsulted();
                objPatient.Status = "C";
                //miPrintPreview.Enabled = false;
                FillConsultedDatagrid(objConnection, objPatient, 8, dgConsulted);

                GetSummary(objConnection);
                pnlVisit.Controls.Clear();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void btnHold_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                objConnection.Connection_Open();

                ResizeHold();
                clearHold();
                objPatient.Status = "H";
                //miPrintPreview.Enabled = false;
                FillConsultedDatagrid(objConnection, objPatient, 8, dgHold);

                GetSummary(objConnection);
                pnlVisit.Controls.Clear();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void dtpCDOB_Leave(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpCDOB.Value.Year;
            txtCAge.Text = Age.ToString();
        }

        private void txtCAge_Leave(object sender, EventArgs e)
        {
            if (txtCAge.Text != null)
            {
                txtCAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
                txtCAge.SelectionStart = 0;
            }

            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtCAge.Text) && Convert.ToInt16(txtCAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtCAge.Text.Trim())) + "";
                dtpCDOB.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtCAge.Text = "";
            }
        }

        private void miRegistration_Click(object sender, EventArgs e)
        {

            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                miConsulted.Enabled = true;
                miRegistration.Enabled = false;
                miHold.Enabled = true;
                //miEmail.Enabled = false;
                lblOPDID.Tag = "U";
                lblMode.Text = "Mode : Registration ";
                lklTdaySummary.Enabled = true;
                objConnection.Connection_Open();
                lblSelect.Text = "0";
                btnNewPatient_Click(btnNewPatient, EventArgs.Empty);
                //miPrintPreview.Enabled = false;

                GetSummary(objConnection);
                pnlVisit.Controls.Clear();
                cboPatientCity.Text = lblCity.Text;
                pnlRPatient.Enabled = true;
                pnlSerachByPRNo.Visible = false;
                miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
                miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";

                dtpFrom.MaxDate = DateTime.Now.Date;
                dtpTo.Value = DateTime.Now.Date;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void miHold_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                miConsulted.Enabled = true;
                miRegistration.Enabled = true;
                miHold.Enabled = false;
                //miEmail.Enabled = false;

                dtpHFrom.Value = DateTime.Now.Date.AddDays(-2);
                dtpHTo.MaxDate = DateTime.Now.Date;

                lblMode.Text = "Mode : Search Hold Patient";
                lblOPDID.Tag = "U";

                //        lklTdaySummary.Enabled = false;
                objConnection.Connection_Open();
                lblSelect.Text = "1";
                ResizeHold();
                clearHold();
                objPatient.Status = "H";
                cboHCity.Text = lblCity.Text;
                ClearSearchField();
                //miPrintPreview.Enabled = false;
                //FillConsultedDatagrid(objConnection, objPatient, 8, dgHold);
                btnHSearch_Click(btnHSearch, e);
                GetSummary(objConnection);
                pnlVisit.Controls.Clear();
                pnlSerachByPRNo.Visible = false;
                miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
                miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";
                lblConsultCount.Text = "Patient Found : " + dgConsulted.Rows.Count.ToString();
                pnlHoldSearch.Enabled = true;
                pnlHPatient.Enabled = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void miConsulted_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            try
            {
                miConsulted.Enabled = false;
                miRegistration.Enabled = true;
                miHold.Enabled = true;
                //miEmail.Enabled = false;
                lblMode.Text = "Mode : Search Consulted Patient";
                lblOPDID.Tag = "U";
                pnlSerachByPRNo.Visible = false;
                //        lklTdaySummary.Enabled = false;
                objConnection.Connection_Open();
                lblSelect.Text = "2";
                ResizeConsulted();
                clearConsulted();
                ClearSearchField();
                objPatient.Status = "C";
                cboCCity.Text = lblCity.Text;
                //miPrintPreview.Enabled = false;
                FillConsultedDatagrid(objConnection, objPatient, 8, dgConsulted);

                GetSummary(objConnection);
                pnlVisit.Controls.Clear();

                miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
                miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";
                lblHoldCount.Text = "Patient Found : " + dgHold.Rows.Count.ToString();

                dtpCTo.MaxDate = DateTime.Now.Date;
                dtpCFrom.Value = DateTime.Now.Date;

                pnlConsultedSearch.Enabled = true;
                pnlCPatient.Enabled = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPatient = null;
            }
        }

        private void miNewAccount_Click(object sender, EventArgs e)
        {
            frmPersonal objPer = new frmPersonal(this);
            objPer.Show();
            this.Hide();
        }

        private void dtpFollowUpDate_Leave(object sender, EventArgs e)
        {
            dtpFollowUpDate.Tag = "1";
        }

        private void lklTdaySummary_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            miRegistration_Click(miRegistration, e);

            lblSelect.Tag = "0";
            btnRegisterPatient_Click(btnPatientSearch, e);
            TodaySummarySearch();
            dgRegisterPatient.Columns["Status"].Visible = false;
            dgRegisterPatient.Columns["ROPDID"].Visible = false;
            //dgRegisterPatient.Columns["PanelName"].Visible = false;
            dgRegisterPatient.ClearSelection();
            //pnlLegend.Visible = true;
            lblRegCount.Text = "Patient Found : " + dgRegisterPatient.Rows.Count.ToString();

        }

        private void mi_MouseDown(object sender, MouseEventArgs e)
        {
            //ToolStripButton tsb = (ToolStripButton)sender;

            //if (tsb.Name.Equals("miRegistration") && !lblSelect.Text.Equals("0"))
            //{
            //  if (MessageBox.Show(this, "Do You want to Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //  {
            //    //Cursor.Position = new Point(e.X, e.Y);
            //    tsb.Tag = "1";
            //    miRegistration_Click(miRegistration, EventArgs.Empty);
            //  }
            //}
            //else if (tsb.Name.Equals("miHold") && !lblSelect.Text.Equals("1"))
            //{
            //  if (MessageBox.Show(this, "Do You want to Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //  {
            //    //Cursor.Position = new Point(e.X, e.Y);
            //    tsb.Tag = "1";
            //    miHold_Click(miHold, EventArgs.Empty);
            //  }
            //}
            //else if (tsb.Name.Equals("miConsulted") && !lblSelect.Text.Equals("2"))
            //{
            //  if (MessageBox.Show(this, "Do You want to Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //  {
            //    //Cursor.Position = new Point(e.X, e.Y);
            //    tsb.Tag = "1";
            //    miConsulted_Click(miConsulted, EventArgs.Empty);
            //  }
            //}
            //else
            //{
            //  //tsb.Enabled = false;
            //}
        }

        private void lkbPayRep_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PaymentReport();
        }

        private void txtInItCap_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void miEmail_Click(object sender, EventArgs e)
        {
            SendMail();
        }

        private void SendMail()
        {
            frmSendEmail objSendMail = new frmSendEmail();
            MailMessage mail = new MailMessage();
            Attachment data;
            MailAddress ma;
            System.Net.NetworkCredential nc;

            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            DataView dv = new DataView();

            try
            {

                //objSendMail.lblAttachment.Text = "Attachment : " + lblOPDID.Text + ".Doc";
                objSendMail.lblAttachment.Text = "Patient Perscription " + txtPRNO.Text + ".Doc";
                objSendMail.txtSubject.Text = "Patient Perscription\"" + txtPRNO.Text + "\"";
                objSendMail.txtFrom.Tag = lblPatientID.Text;

                miConsultedSave_Click(miConsultedSave, EventArgs.Empty);
                objSendMail.ShowDialog();

                if (!objSendMail.txtTo.Tag.Equals("C"))
                {
                    PerscriptionPreview(lblOPDID.Text, "Export");
                    mail.To.Add(objSendMail.txtTo.Text.Trim());
                    mail.Subject = objSendMail.txtSubject.Text.Trim();

                    data = new Attachment(Application.StartupPath + "\\EmailRpt\\" + lblOPDID.Text + ".Doc");
                    ma = new MailAddress(objSendMail.txtFrom.Text.Trim(), clsSharedVariables.UserName);
                    mail.From = ma;
                    mail.Body = objSendMail.txtDescription.Text.Trim(); ;

                    //string userName = System.Configuration.ConfigurationSettings.AppSettings["username"].ToString();
                    objConnection.Connection_Open();
                    objRef.ReportRefrence = "SetMail";
                    dv = objRef.GetAll(2);
                    objConnection.Connection_Close();

                    string userName = "";
                    string password = "";

                    if (dv.Table.Rows.Count != 0)
                    {
                        userName = dv.Table.Rows[0]["Description"].ToString();
                        password = dv.Table.Rows[0]["ReportTitle1"].ToString();

                        //string userName = "mohsin@treesvalley.com";
                        //string password = System.Configuration.ConfigurationSettings.AppSettings["password"].ToString();
                        //string password = "mp5bk103m3";

                        nc = new System.Net.NetworkCredential(userName, password);

                        SmtpClient st = new SmtpClient();

                        st.EnableSsl = true;
                        st.UseDefaultCredentials = false;
                        st.Credentials = nc;
                        //st.Port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"].ToString());
                        //st.Port = 25;
                        st.Port = Convert.ToInt16(dv.Table.Rows[0]["ReportTitle2"].ToString());
                        //st.Host = System.Configuration.ConfigurationSettings.AppSettings["host"].ToString();
                        //st.Host = "82.165.187.167";
                        st.Host = dv.Table.Rows[0]["ReportTitle3"].ToString();
                        mail.Attachments.Add(data);
                        st.Send(mail);

                        MessageBox.Show("Email Sent Successfully", "Finish", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SMTP server not found");
            }
            finally
            {

                objRef = null;
                objConnection = null;
                dv.Dispose();
                objSendMail.Dispose();
                mail.Dispose();
                data = null;
                ma = null;
                nc = null;
            }
        }

        private void miPanelInfo_Click(object sender, EventArgs e)
        {
            frmPanel objPanel = new frmPanel(this);
            objPanel.Show();
            this.Hide();
        }

        private void txtPatientAddress_MouseHover(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            this.ttOPD.SetToolTip(txt, txt.Text);
        }

        private void cboPanel_MouseHover(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            this.ttOPD.SetToolTip(cbo, cbo.Text);
        }
        //visit.jawad@gmail.com
        private void btnAppointControl_Click(object sender, EventArgs e)
        {
            if (btnAppointControl.Tag.Equals("1"))
            {
                //Bitmap bmp = new Bitmap("Expend.gif");
                //btnAppointControl.BackgroundImage = bmp;
                this.mcAppointment.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btnAppointControl.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\collapse2222.png");

                pnlAppointment.Size = new Size(pnlInfo.Width + 316, 459);//360, 238);581, 459//
                pnlAppointment.Location = new Point(pnlInfo.Location.X - 316, pnlAppointment.Location.Y);//652, 241);
                //btnAppointControl.Location = new Point(32, 0);
                //lblAppointment.Location = new Point(49, 8);
                btnAppointControl.Tag = "0";

            }
            else if (btnAppointControl.Tag.Equals("0"))
            {
                //Bitmap bmp = new Bitmap("RightArrow.gif");
                //btnAppointControl.Image = bmp ;
                btnAppointControl.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\Exp2222.png");
                pnlAppointment.Size = panel3.Size;//22
                pnlAppointment.Location = new Point(pnlInfo.Location.X + 2, pnlInfo.Location.Y + 2);//816, 243
                //btnAppointControl.Location = new Point(32, 0);
                //lblAppointment.Location = new Point(49, 8);

                btnAppointControl.Tag = "1";
            }
            btnAppointControl.BringToFront();
        }

        private void btnAppointment_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();

            try
            {
                objConnection.Connection_Open();

                if (InsertAppointment(objConnection))
                {
                    InsertOPD("W", mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy"));
                    GetAppointment(objConnection);
                    FillAppointmenthCal();
                }

                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            objConnection = null;
        }

        private bool InsertAppointment(clsBLDBConnection objConnection)
        {
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dv;

            try
            {
                if (!txtPRNO.Text.Trim().Equals("-  -") && !txtPRNO.Text.Equals("lblPRNO") && !txtPRNO.Text.Equals(""))
                {
                    objAppoint.PersonID = clsSharedVariables.UserID;
                    //objAppoint.AppointmentDate = dtpAppointment.Value.ToString("yyyy-MM-dd");
                    objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");
                    objAppoint.PRNO = txtPRNO.Text;
                    if (objAppoint.GetAll(5).Table.Rows.Count == 0)
                    {
                        dv = objAppoint.GetAll(3);

                        if (dv.Table.Rows.Count == 0)
                        {
                            objAppoint.TokkenNo = "1";
                        }
                        else
                        {
                            objAppoint.TokkenNo = dv.Table.Rows[0]["Tokkenno"].ToString();
                        }
                        objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("dd/MM/yyyy");
                        objAppoint.PRNO = txtPRNO.Text;
                        objAppoint.MaxSlot = btnAppointment.Tag.ToString();

                        objConnection.Transaction_Begin();
                        if (objAppoint.Insert())
                        {
                            objConnection.Transaction_ComRoll();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show(objAppoint.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Patient For Appointment", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                objAppoint = null;
            }
            return false;
        }

        private void mcAppointment_DateSelected(object sender, DateRangeEventArgs e)
        {
            PopulateAppointment();
        }

        private void GetAppointment(clsBLDBConnection objConnection)
        {
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dvAppointment;
            //int x = 3, y = 5;
            //int AppointCount = 0;
            try
            {
                if (mcAppointment.GetDateInfo().Length != 0)
                {
                    objAppoint.AppointmentDate = mcAppointment.SelectedDates[0].Date.ToString("yyyy-MM-dd");
                }
                else
                {
                    objAppoint.AppointmentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                }
                objAppoint.PersonID = clsSharedVariables.UserID;

                //pnlAppointList.Controls.Clear();

                ClearGridRows(dgAppoinment);
                //lblAppointment.Text = "Appointments " + objAppoint.GetAll(2).Table.Rows[0]["FillSlot"].ToString() + " / " + btnAppointment.Tag.ToString();


                dvAppointment = objAppoint.GetAll(4);
                dvAppointment.RowFilter = "prNo <> '" + txtPRNO.Text + "'";

                dgAppoinment.DataSource = dvAppointment;
                

                lblAppointment.Text = "Appointments " + dgAppoinment.Rows.Count.ToString() + " / " + btnAppointment.Tag.ToString();

                //foreach (DataRow dr in dvAppointment.Table.Rows)
                //{
                //    this.pnlAppointList.Controls.Add(llblProperties(x, y, "llbl" + AppointCount.ToString(), dr["PRNO"].ToString(), dr["AppointmentID"].ToString(), dr["Name"].ToString()));
                //    this.pnlAppointList.Controls.Add(lblProperties(x + 100, y, "lbl" + AppointCount.ToString(), dr["TokkenNo"].ToString()));
                //    y = y + 17;
                //}

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objAppoint = null;
            }
        }

        //private void mcAppointment_DateSelected(object sender, DateRangeEventArgs e)
        //{
        //    PopulateAppointment();
        //}

        private void PopulateAppointment()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);

            try
            {
                objConnection.Connection_Open();
                objPerson.PersonID = clsSharedVariables.UserID;
                btnAppointment.Tag = objPerson.GetAll(7).Table.Rows[0]["MaxSlot"].ToString();
                GetAppointment(objConnection);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objPerson = null;
            }
        }

        private LinkLabel llblProperties(int x, int y, string llblName, string textVal, string textTag, string PatientName)
        {
            LinkLabel llbl = new LinkLabel();

            llbl.AutoSize = true;
            llbl.Location = new System.Drawing.Point(x, y);
            llbl.Name = llblName;
            llbl.TextAlign = ContentAlignment.TopCenter;
            llbl.Size = new System.Drawing.Size(30, 20);
            llbl.Text = textVal;
            llbl.Font = new System.Drawing.Font("Microsoft Sans Serif", llbl.Font.Size, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));// 8.25F
            llbl.BackColor = Color.Transparent;
            llbl.Tag = textTag;
            llbl.ForeColor = Color.Maroon;
            if (!PatientName.Equals(""))
            {
                llbl.Click += new EventHandler(llbl_Click);
                ttOPD.SetToolTip(llbl, PatientName);
            }
            else
            {
                llbl.Click += new EventHandler(llblWait_Click);
            }


            return llbl;
        }

        private void llbl_Click(object sender, EventArgs e)
        {
            LinkLabel llbl = new LinkLabel();
            llbl = (LinkLabel)sender;

            //FillFormClickLlbl(llbl.Text);
        }

        private int CountConsultHold(string Status)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            int Count = 0;

            objOPD.Status = Status;
            objConnection.Connection_Open();
            Count = objOPD.GetAll(7).Table.Rows.Count;
            objConnection.Connection_Close();

            objConnection = null;
            objOPD = null;

            return Count;
        }

        private void dataAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDataGraph objDataGraph = new frmDataGraph(this);
            this.Hide();
            objDataGraph.Show();
        }

        private void CreateLBWaiting()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dvPatientWait;
            int x = 10, y = 5;
            int VisitCount = 1;
            try
            {
                objAppoint.AppointmentDate = DateTime.Now.ToString("yyyy-MM-dd");
                objAppoint.PersonID = clsSharedVariables.UserID;

                pnlWaitingList.Controls.Clear();

                objConnection.Connection_Open();
                dvPatientWait = objAppoint.GetAll(6);
                objConnection.Connection_Close();
                dvPatientWait.RowFilter = " prno = '" + txtPRNO.Text + "'";

                foreach (DataRow dr in dvPatientWait.Table.Rows)
                {
                    //if (VisitCount == 4)
                    //{
                    //  break;
                    //}
                    if (!txtPRNO.Text.Equals(dr["prno"].ToString()))
                    {
                        this.pnlWaitingList.Controls.Add(lblProperties(x, y, "lbl" + VisitCount.ToString(), VisitCount.ToString() + ".  "));
                        this.pnlWaitingList.Controls.Add(llblProperties(x + 35, y, "llbl" + VisitCount.ToString(), dr["name"].ToString(), dr["prno"].ToString(), ""));
                        //if (VisitCount % 6 == 0)
                        //{
                        //    y = y + 20;
                        //    x = 10;
                        //}
                        //else
                        //{
                        //    x = x + 30;
                        //}
                        y = y + 20;
                        VisitCount++;
                    }
                }
                //lblWaitingQueue.Text = "Waiting Queue : " + dvPatientWait.Table.Rows.Count.ToString();
                lblWaitingQueue.Text = "Waiting Queue : " + (pnlWaitingList.Controls.Count / 2);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objAppoint = null;
                dvPatientWait = null;
            }
        }

        private void llblWait_Click(object sender, EventArgs e)
        {
            LinkLabel llbl = new LinkLabel();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();

            llbl = (LinkLabel)sender;

            objOPD.PRNO = llbl.Tag.ToString();
            objOPD.VisitDate = DateTime.Now.ToString("yyyy-MM-dd");
            objOPD.EnteredOn = DateTime.Now.ToString("yyyy-MM-dd");
            objOPD.EnteredBy = clsSharedVariables.UserID;
            objConnection.Connection_Open();
            dv = objOPD.GetAll(12);
            objConnection.Connection_Close();

            FillWaitPatient(dv);

            ClearDG();
            ClearVitalSigns();

            PrevoiusInfo();
            CreateInstructionCHB();

            objOPD = null;
            objConnection = null;
            dv.Dispose();
        }

        private void FillWaitPatient(DataView dv)
        {
            lblHPatientID.Text = lblPatientID.Text = lblCPatientID.Text = dv.Table.Rows[0]["PatientID"].ToString();
            lblOPDID.Text = dv.Table.Rows[0]["OPDID"].ToString();
            txtHPRNo.Text = txtPRNO.Text = txtCPRNo.Text = txtPRNO.Text = dv.Table.Rows[0]["PRNO"].ToString();
            txtHName.Text = txtPatientName.Text = txtCName.Text = clsSharedVariables.InItCaps(dv.Table.Rows[0]["PatientName"].ToString());
            if (dv.Table.Rows[0]["Gender"].ToString().Equals("Male"))
            {
                rbPatientMale.Checked = rbHMale.Checked = rbCMale.Checked = true;
            }
            else
            {
                rbPatientFemale.Checked = rbHFemale.Checked = rbHFemale.Checked = true;
            }
            dtpPatientDOB.Value = dtpHDOB.Value = dtpCDOB.Value = Convert.ToDateTime(dv.Table.Rows[0]["DOB"].ToString());
            txtPatientAge.Text = txtHAge.Text = txtCAge.Text = Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dv.Table.Rows[0]["DOB"].ToString()).Year);
            txtPatientPhone.Text = txtHPhone.Text = txtCPhone.Text = dv.Table.Rows[0]["PhoneNo"].ToString();
            txtPatientCell.Text = txtHCell.Text = txtCCell.Text = dv.Table.Rows[0]["CellNo"].ToString();
            txtPatientAddress.Text = txtHAddress.Text = txtCAddress.Text = clsSharedVariables.InItCaps(dv.Table.Rows[0]["Address"].ToString());
            cboPatientCity.Text = cboHCity.Text = cboCCity.Text = dv.Table.Rows[0]["cityname"].ToString();
            txtRegEmail.Text = txtHEmail.Text = txtCEmail.Text = dv.Table.Rows[0]["Email"].ToString();
            //cboRPanel.Text = cboHPanel.Text = cboCPanel.Text = dv.Table.Rows[0]["PanelName"].ToString();
            txtPayment.Text = dv.Table.Rows[0]["Payment"].ToString();
            //CreateInstructionCHB();
        }

        private void miItemDelete_Click(object sender, EventArgs e)
        {
            int RowIdx = Convert.ToInt16(miItemDelete.Tag);

            if (miItemDelete.Tag != null && Convert.ToInt16(miItemDelete.Tag) != (dgList.Rows.Count - 1))
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLPreference objPref = new clsBLPreference(objConnection);

                try
                {
                    objPref.PrefID = dgList.Rows[RowIdx].Cells[0].Value.ToString();
                    objPref.Active = "0";

                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    objPref.Update();
                    objConnection.Transaction_ComRoll();
                    dgList.Rows.RemoveAt(RowIdx);
                    dgList.Refresh();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                finally
                {
                    objConnection.Connection_Close();
                }
            }
        }

        private void dgList_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgList.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dgList.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dgList.ClearSelection();
                dgList.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dgList.ContextMenuStrip.Items[0].Tag = null;
                //dg.ContextMenuStrip = null;
            }

            hi = null;
        }

        private string GetSelectedItemTypeID(int RowIdx)
        {
            if (!RowIdx.ToString().Equals("-1"))
            {
                if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgHistory.Columns["HistoryName"].HeaderText))
                {
                    return dgHistory.Tag.ToString();
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgDiagnosis.Columns["DiagnosisName"].HeaderText))
                {
                    return dgDiagnosis.Tag.ToString();
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgComplaints.Columns["ComplaintsName"].HeaderText))
                {
                    return dgComplaints.Tag.ToString();
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgSigns.Columns["SignsName"].HeaderText))
                {
                    return dgSigns.Tag.ToString();
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgPlans.Columns["PlansName"].HeaderText))
                {
                    return dgPlans.Tag.ToString();
                }
                else if (dgList.Columns["Pname"].HeaderText.ToString().Equals(dgInvestigation.Columns["InvestigatioName"].HeaderText))
                {
                    return dgInvestigation.Tag.ToString();
                }
                return "";
            }
            return "";
        }

        private void frmOPD_FormClosing(object sender, FormClosingEventArgs e)
        {

            try
            {
                //ReportDocument rpt = (ReportDocument)crvVitalSign.ReportSource;

                //if (rpt != null)
                //{
                //    rpt.Close();
                //    rpt.Dispose();
                //    rpt = null;
                //}



                
                GC.Collect();
                
                
                stop = 1;
                if (this.Login_Form != null)
                {
                    this.Login_Form.Show();
                }
                else if (this.LoginLic != null)
                {
                    this.LoginLic.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("now you are going to logout");
                
            }
            

           
            //this.Dispose();
        }

        private void dtpFollowUpDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lblFollowUpDate_Click(object sender, EventArgs e)
        {

        }

        private void StartThread()
        {
            Thread th = new Thread(TimeThread);
            th.Start();
        }

        private void TimeThread()
        {
            try
            {
                while (stop == 0)
                {
                    System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
                    lblTime.Text = DateTime.Now.ToString("ddd, MMM d, yyyy") + "  " + DateTime.Now.ToLongTimeString();
                    System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = true;
                    Thread.Sleep(1000);
                }
            }
            catch (ThreadAbortException tae)
            {
                MessageBox.Show(tae.Message);
            }
        }

        private void miDoc_Click(object sender, EventArgs e)
        {
            frmDocument objDoc = new frmDocument(lblPatientID.Text, this);
            if (!miRegistration.Enabled)
            {
                objDoc.PatientName = txtPatientName.Text;
            }
            else if (!miHold.Enabled)
            {
                objDoc.PatientName = txtHName.Text;
            }
            else if (!miConsulted.Enabled)
            {
                objDoc.PatientName = txtCName.Text;
            }
            this.Hide();
            objDoc.Show();
        }

        private void dtpPatientDOB_ValueChanged(object sender, EventArgs e)
        {
            var date = Convert.ToDateTime(this.dtpPatientDOB.Value);
            var age = DateTime.Now.Year - date.Year;
            this.txtPatientAge.Text = age.ToString();
        }

        private void btnHoldSerach_Click(object sender, EventArgs e)
        {
            pnlHoldSearch.Enabled = true;
            pnlHPatient.Enabled = false;
            btnHold_Click(sender, e);
            lblMode.Text = "Mode : Search Hold Patient";
            lblHoldCount.Text = "Patient Found : " + dgHold.Rows.Count.ToString();
        }

        private void btnConsultedSerach_Click(object sender, EventArgs e)
        {
            pnlConsultedSearch.Enabled = true;
            pnlCPatient.Enabled = false;
            btnConsulted_Click(sender, e);
            lblMode.Text = "Mode : Search Consulted Patient";
            lblConsultCount.Text = "Patient Found : " + dgConsulted.Rows.Count.ToString();
        }

        //private void dgList_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //  if (dgList.Columns["PName"].HeaderText.Equals(dgHistory.Columns["HistoryName"].HeaderText) || dgList.Columns["PName"].HeaderText.Equals(dgDiagnosis.Columns["DiagnosisName"].HeaderText))
        //  {
        //    dgList.Font = new System.Drawing.Font("Microsoft Sans Serif", 132F);
        //  }
        //  else
        //  {
        //    dgList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
        //  }
        //}

        private void GenSerNo(DataGridView dg, string colName)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                dg.Rows[i].Cells[colName].Value = (i + 1).ToString();
            }
        }

        private void dgRegisterPatient_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenSerNo(dgRegisterPatient, "RSNo");
        }

        private void dgHold_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenSerNo(dgHold, "HSNo");
        }

        private void dgConsulted_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenSerNo(dgConsulted, "CSNo");
        }

        private void txtSearchPRNO_Enter(object sender, EventArgs e)
        {
            MaskedTextBox mtxt = new MaskedTextBox();
            mtxt = (MaskedTextBox)sender;

            mtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
        }

        private void txtSearchPRNO_Leave(object sender, EventArgs e)
        {
            MaskedTextBox mtxt = new MaskedTextBox();
            mtxt = (MaskedTextBox)sender;
            if (mtxt != null)
            {
                mtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
                mtxt.SelectionStart = 0;
            }
        }

        private void dtpTo_Enter(object sender, EventArgs e)
        {
            DateTimePicker dtp = new DateTimePicker();
            dtp = (DateTimePicker)sender;

            dtp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
        }

        private void dtpTo_Leave(object sender, EventArgs e)
        {
            DateTimePicker dtp = new DateTimePicker();
            dtp = (DateTimePicker)sender;
            if (dtp != null)
            {
                dtp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            }
        }

        private void cboCPanel_Leave(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo != null)
            {
                cbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            }
        }

        private void cboCPanel_Enter(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
        }

        //private void dgHistory_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        //{
        //    DataGridView dg = (DataGridView)sender;
        //    if (dg.Rows.Count != 1)
        //    {
        //        dg.Columns[0].DefaultCellStyle.ForeColor = Color.Black;
        //        dg.Columns[0].DefaultCellStyle.NullValue = null;
        //    }
        //    else
        //    {
        //        dg.Columns[0].DefaultCellStyle.ForeColor = Color.Blue;
        //        dg.Columns[0].DefaultCellStyle.NullValue = "Click here to see preffered " + dg.Columns[0].HeaderText + " list";
        //    }
        //}

        //private void dgHistory_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        //{
        //    DataGridView dg = (DataGridView)sender;
        //    if (dg.Rows.Count != 1)
        //    {
        //        dg.Columns[0].DefaultCellStyle.ForeColor = Color.Black;
        //        dg.Columns[0].DefaultCellStyle.NullValue = null;
        //    }
        //    else
        //    {
        //        dg.Columns[0].DefaultCellStyle.ForeColor = Color.Blue;
        //        dg.Columns[0].DefaultCellStyle.NullValue = "Click here to see preffered " + dg.Columns[0].HeaderText + " list";
        //    }
        //}

        private void GetPreParameter(string Title, string Col)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dvVisit = new DataView();

            objConnection.Connection_Open();

            foreach (Control ctr in pnlVisit.Controls)
            {
                if (ctr.GetType() == typeof(System.Windows.Forms.Button))
                {
                    Button btn = (Button)ctr;
                    objOPD.OPDID = btn.Tag.ToString();
                    dvVisit = objOPD.GetAll(13);
                    ttOPD.SetToolTip(btn, Title + "\n" + dvVisit.Table.Rows[0][Col].ToString().Replace(",", "\n"));
                }
            }

            objConnection.Connection_Close();

            objConnection = null;
            objOPD = null;
            dvVisit = null;

        }

        private void tpUrdu_Draw(object sender, DrawToolTipEventArgs e)
        {
            DrawToolTipEventArgs newArgs = new DrawToolTipEventArgs(e.Graphics,
                             e.AssociatedWindow, e.AssociatedControl, e.Bounds, e.ToolTipText,
                             this.BackColor, this.ForeColor, new Font(e.Font, FontStyle.Bold));
            newArgs.DrawBackground();
            newArgs.DrawBorder();
            newArgs.DrawText(TextFormatFlags.TextBoxControl);
        }

        private void Numeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = ",";
            e.Handled = (str.Contains(e.KeyChar.ToString()));
        }

        private void dg_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(this.Numeric_KeyPress);
        }

        private void txtPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = "\b0123456789";
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void llblRefershPatient_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

            ClearDG();

            DataView dvPatient = new DataView();
            DataView dv = new DataView();
            DataTable dt = new DataTable();

            //objPatient.FromDate = clsSharedVariables.RefPersonID;
            try
            {
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                //objConnection.Ora_Connection_Open();

                //for (int i = 0; i < dgRegisterPatient.Rows.Count; i++)
                //{
                //    objPatient.PRNO = dgRegisterPatient.Rows[i].Cells["PRNO"].Value.ToString();
                //    dvPatient = objPatient.Ora_GetAll(3);

                //    if (dvPatient.Table.Rows.Count != 0)
                //    {
                //        objPatient.PatientID = dgRegisterPatient.Rows[i].Cells["PatientID"].Value.ToString();
                //        objPatient.Name = dvPatient.Table.Rows[0]["PatientName"].ToString().Trim();
                //        if (dvPatient.Table.Rows[0]["Gender"].ToString().Trim().Equals("Male"))
                //        {
                //            objPatient.Gender = "M";
                //        }
                //        else
                //        {
                //            objPatient.Gender = "F";
                //        }
                //        objPatient.DOB = Convert.ToDateTime(dvPatient.Table.Rows[0]["DOB"].ToString().Trim()).ToString("dd/MM/yyyy");
                //        objPatient.PhoneNo = dvPatient.Table.Rows[0]["Phoneno"].ToString().Trim();
                //        objPatient.CellNo = dvPatient.Table.Rows[0]["CellNo"].ToString().Trim();
                //        objPatient.Address = clsSharedVariables.InItCaps(dvPatient.Table.Rows[0]["Address"].ToString().Trim());
                //        cboPatientCity.SelectedIndex = cboPatientCity.FindString(dvPatient.Table.Rows[0]["cityname"].ToString().Trim());
                //        if (cboPatientCity.SelectedValue != null && !cboPatientCity.SelectedValue.ToString().Equals("-1"))
                //        {
                //            objPatient.CityID = cboPatientCity.SelectedValue.ToString();
                //        }
                //        else
                //        {
                //            cboPatientCity.SelectedIndex = 0;
                //            objPatient.CityID = cboPatientCity.SelectedValue.ToString();
                //        }

                //        objPatient.Email = dvPatient.Table.Rows[0]["Email"].ToString().Trim();
                //        objPatient.PanelID = "0";

                //        if (!objPatient.Update())
                //        {
                //            MessageBox.Show(objPatient.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        }
                //    }
                //}
                //objConnection.Ora_Connection_Close();

                dvPatient = objPatient.GetAll(9);
                this.dgRegisterPatient.DataSource = dvPatient;
                GenSerNo(dgRegisterPatient, "RSNo");
                lklTdaySummary.Text = "Waiting Queue ( " + dvPatient.Table.Rows.Count.ToString() + " )";
                dgRegisterPatient.ClearSelection();
                lblRegCount.Text = "Patient Found : " + dgRegisterPatient.Rows.Count.ToString();

            }
            catch
            {
                MessageBox.Show("Server not available", "Server Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Transaction_ComRoll();
                objConnection.Connection_Close();
            }

            dvPatient = null;
            objConnection = null;
            objPatient = null;
        }

        private void miSMS_Click(object sender, EventArgs e)
        {
            SendSMS("", "03013001190");
        }


        private void miWhatsApp_Click(object sender, EventArgs e)
        {
            //SendSMS("", "03013001190");
            WhatsAppForm wa = new WhatsAppForm();
            wa.Show();
        }

        private void miVideoCall_Click(object sender, EventArgs e)
        {
            //SendSMS("", "03013001190");
            VideoChat vc = new VideoChat();
            vc.Show();
        }


        private void SendSMS(string msg, string sendTo)
        {

        }

        private void pnlMedHist_MouseLeave(object sender, EventArgs e)
        {
            //pnlMedHistGrid.Visible = false;
            //ClearGridRows(dgMedHist); 
        }

        private void pnlMedHist_MouseEnter(object sender, EventArgs e)
        {
            pnlMedHistGrid.Visible = true;

            pnlMedHistGrid.BringToFront();
            pnlVitalSignHist.Visible = false;

            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);
            DataView dv = new DataView();
            DataTable dt = new DataTable();
            DataRow dr;
            string tmpVisitDate = "";

            dt.Columns.Add("visitdate");
            dt.Columns.Add("OPDID");
            dt.Columns.Add("MedicineID");
            dt.Columns.Add("MedicineName");
            dt.Columns.Add("MedicineDosage");

            try
            {
                objConnection.Connection_Open();
                objOPDMed.MedID = lblPatientID.Text;
                objOPDMed.OPDID = lblOPDID.Text;

                dv = objOPDMed.GetAll(3);

                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    dr = dt.NewRow();

                    if (!tmpVisitDate.Equals(dv.Table.Rows[i]["Visitdate"].ToString()))
                    {
                        dr["MedicineName"] = Convert.ToDateTime(dv.Table.Rows[i]["Visitdate"].ToString()).ToString("D");
                        dr["VisitDate"] = "1";
                        tmpVisitDate = dv.Table.Rows[i]["Visitdate"].ToString();
                        i--;
                    }
                    else
                    {
                        dr["VisitDate"] = "0";
                        dr["OPDID"] = dv.Table.Rows[i]["OPDID"].ToString();
                        dr["MedicineID"] = dv.Table.Rows[i]["MedicineID"].ToString();
                        dr["MedicineName"] = dv.Table.Rows[i]["MedicineName"].ToString();
                        dr["MedicineDosage"] = dv.Table.Rows[i]["MedicineDosage"].ToString();
                    }
                    dt.Rows.Add(dr);
                }


                dgMedHist.DataSource = dt;
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                for (int i = 0; i < dgMedHist.Rows.Count; i++)
                {
                    if (dgMedHist.Rows[i].Cells["MHVisitDate"].Value.ToString().Equals("1"))
                    {
                        dgMedHist.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                        dgMedHist.Rows[i].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    }
                    else
                    {
                        dgMedHist.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                }
                dgMedHist.ClearSelection();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                //dv.Dispose();
            }
        }

        private void btnMedHistClose_Click(object sender, EventArgs e)
        {
            pnlMedHistGrid.Visible = false;
            ClearGridRows(dgMedHist);
        }

        private void btnVitalSignHist_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();

            pnlVitalSignHist.Visible = true;
            pnlVitalSignHist.BringToFront();
            try
            {
                objConnection.Connection_Open();

                objOPD.PRNO = txtPRNO.Text;
                dv = objOPD.GetAll(14);
                DrawGraph(dv);

                objConnection.Connection_Close();
            }
            catch (Exception)
            {

            }
        }

        private void DrawGraph(DataView dv)
        {
            ////BP, Pulse, Temprature, Weight, Height, HeadCircumferences,

            //string SerX = "", SerYLBP = "", SerYHBP = "", SerYTemp = "", SerYPulse = "", SerYWeight = "", SerYHeight = "", SerYHeadCircum = "";

            //csVitalSign1.Clear();
            //csVitalSign1.Charts.Add(0);

            //csVitalSign2.Clear();
            //csVitalSign2.Charts.Add(0);

            //csVitalSign1.Charts[0].Type = Microsoft.Office.Interop.Owc11.ChartChartTypeEnum.chChartTypeLineMarkers;
            //csVitalSign2.Charts[0].Type = Microsoft.Office.Interop.Owc11.ChartChartTypeEnum.chChartTypeLineMarkers;


            //csVitalSign1.Charts[0].HasLegend = true;
            ////csVitalSign1.Charts[0].HasTitle = true;
            ////csVitalSign1.Charts[0].Title.Font.Bold = true;
            //csVitalSign1.Charts[0].Axes[0].HasTitle = true;
            ////csVitalSign1.Charts[0].Axes[0].Font.Bold = true;
            //csVitalSign1.Charts[0].Axes[0].Title.Font.Bold = true;
            //csVitalSign1.Charts[0].Axes[1].HasTitle = true;
            ////csVitalSign1.Charts[0].Axes[1].Font.Bold = true;
            //csVitalSign1.Charts[0].Axes[1].Title.Font.Bold = true;

            ////csVitalSign1.Charts[0].Title.Font.Size = 10;
            //csVitalSign1.Charts[0].Axes[0].Font.Size = 7;
            //csVitalSign1.Charts[0].Axes[0].Title.Font.Size = 8;
            //csVitalSign1.Charts[0].Axes[1].Font.Size = 7;
            //csVitalSign1.Charts[0].Axes[1].Title.Font.Size = 8;

            //csVitalSign2.Charts[0].HasLegend = true;
            ////csVitalSign2.Charts[0].HasTitle = true;
            ////csVitalSign2.Charts[0].Title.Font.Bold = true;
            //csVitalSign2.Charts[0].Axes[0].HasTitle = true;
            ////csVitalSign2.Charts[0].Axes[0].Font.Bold = true;
            //csVitalSign2.Charts[0].Axes[0].Title.Font.Bold = true;
            //csVitalSign2.Charts[0].Axes[1].HasTitle = true;
            ////csVitalSign2.Charts[0].Axes[1].Font.Bold = true;
            //csVitalSign2.Charts[0].Axes[1].Title.Font.Bold = true;

            ////csVitalSign2.Charts[0].Title.Font.Size = 10;
            //csVitalSign2.Charts[0].Axes[0].Font.Size = 7;
            //csVitalSign2.Charts[0].Axes[0].Title.Font.Size = 8;
            //csVitalSign2.Charts[0].Axes[1].Font.Size = 7;
            //csVitalSign2.Charts[0].Axes[1].Title.Font.Size = 8;

            ////csPatients.ChartSpaceTitle.Caption = "Data Analysis for Location";

            //Microsoft.Office.Interop.Owc11.ChSeries serLBP = csVitalSign1.Charts[0].SeriesCollection.Add(0);
            //Microsoft.Office.Interop.Owc11.ChSeries serHBP = csVitalSign1.Charts[0].SeriesCollection.Add(1);
            //Microsoft.Office.Interop.Owc11.ChSeries serTemp = csVitalSign1.Charts[0].SeriesCollection.Add(2);
            //Microsoft.Office.Interop.Owc11.ChSeries serPulse = csVitalSign1.Charts[0].SeriesCollection.Add(3);

            //Microsoft.Office.Interop.Owc11.ChSeries serWeight = csVitalSign2.Charts[0].SeriesCollection.Add(0);
            //Microsoft.Office.Interop.Owc11.ChSeries serHeight = csVitalSign2.Charts[0].SeriesCollection.Add(1);
            //Microsoft.Office.Interop.Owc11.ChSeries serHeadCircum = csVitalSign2.Charts[0].SeriesCollection.Add(2);

            //csVitalSign1.Charts[0].SeriesCollection[0].DataLabelsCollection.Add().HasValue = true;
            //csVitalSign1.Charts[0].SeriesCollection[1].DataLabelsCollection.Add().HasValue = true;
            //csVitalSign1.Charts[0].SeriesCollection[2].DataLabelsCollection.Add().HasValue = true;
            //csVitalSign1.Charts[0].SeriesCollection[3].DataLabelsCollection.Add().HasValue = true;

            //csVitalSign2.Charts[0].SeriesCollection[0].DataLabelsCollection.Add().HasValue = true;
            //csVitalSign2.Charts[0].SeriesCollection[1].DataLabelsCollection.Add().HasValue = true;
            //csVitalSign2.Charts[0].SeriesCollection[2].DataLabelsCollection.Add().HasValue = true;

            ////csVitalSign1.Charts[0].SeriesCollection[0].DataLabelsCollection.Add().Font.Size = 6;
            ////csVitalSign1.Charts[0].SeriesCollection[1].DataLabelsCollection.Add().Font.Size = 6;
            ////csVitalSign1.Charts[0].SeriesCollection[2].DataLabelsCollection.Add().Font.Size = 6;
            ////csVitalSign1.Charts[0].SeriesCollection[3].DataLabelsCollection.Add().Font.Size = 6;

            ////csVitalSign2.Charts[0].SeriesCollection[0].DataLabelsCollection.Add().Font.Size = 6;
            ////csVitalSign2.Charts[0].SeriesCollection[1].DataLabelsCollection.Add().Font.Size = 6;
            ////csVitalSign2.Charts[0].SeriesCollection[2].DataLabelsCollection.Add().Font.Size = 6;

            ////change the caption of legend

            //csVitalSign1.Charts[0].Legend.Font.Size = 7;
            //csVitalSign2.Charts[0].Legend.Font.Size = 7;

            //csVitalSign1.Charts[0].Legend.Position = Microsoft.Office.Interop.Owc11.ChartLegendPositionEnum.chLegendPositionTop;
            //csVitalSign2.Charts[0].Legend.Position = Microsoft.Office.Interop.Owc11.ChartLegendPositionEnum.chLegendPositionTop;

            ////csVitalSign1.Charts[0].Title.Caption = "Vital Sign History(BP, Pulse and Temprature)";
            //csVitalSign1.Charts[0].Axes[0].Title.Caption = "Visit Date";
            //csVitalSign1.Charts[0].Axes[1].Title.Caption = "Value";

            //csVitalSign1.Charts[0].SeriesCollection[0].Caption = "Lower BP (mm/Hg)";
            //csVitalSign1.Charts[0].SeriesCollection[1].Caption = "High BP (mm/Hg)";
            //csVitalSign1.Charts[0].SeriesCollection[2].Caption = "Temprature (F)";
            //csVitalSign1.Charts[0].SeriesCollection[3].Caption = "Pulse (Pulse/Min)";

            ////csVitalSign2.Charts[0].Title.Caption = "Vital Sign History(Height, Weight and Headcircumferences)";
            //csVitalSign2.Charts[0].Axes[0].Title.Caption = "Visit Date";
            //csVitalSign2.Charts[0].Axes[1].Title.Caption = "Value";
            //csVitalSign2.Charts[0].SeriesCollection[0].Caption = "Weight (Kg)";
            //csVitalSign2.Charts[0].SeriesCollection[1].Caption = "Height (Feet)";
            //csVitalSign2.Charts[0].SeriesCollection[2].Caption = "Head Circum (cm)";
            ////csVitalSin.Charts[0].SeriesCollection[0].get_Scalings(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories).HasAutoMaximum= true;
            ////CreateSeries(dvOPD, ref SerX, ref SerY , "CityName", "CityCount");

            //#region Create X-Axis
            //SerX = "";
            //if (dv.Table.Rows.Count >= 1)
            //{
            //    //csVitalSign.Charts[0].Axes[0].Scaling.Minimum = Convert.ToDouble(Convert.ToDateTime(dv.Table.Rows[0]["VisitDate"].ToString().Trim()).AddDays(-10).ToString("dd/MM/yyyy"));
            //    //csVitalSign.Charts[0].Axes[0].Scaling.Maximum =  Convert.ToDouble( Convert.ToDateTime(dv.Table.Rows[dv.Table.Rows.Count-1]["VisitDate"].ToString().Trim()).AddDays(10).ToString("dd/MM/yyyy"));
            //    csVitalSign1.Charts[0].Axes[0].Scaling.Minimum = 0;
            //    csVitalSign1.Charts[0].Axes[0].Scaling.Maximum = dv.Table.Rows.Count + 1;

            //    csVitalSign2.Charts[0].Axes[0].Scaling.Minimum = 0;
            //    csVitalSign2.Charts[0].Axes[0].Scaling.Maximum = dv.Table.Rows.Count + 1;
            //}

            //for (int i = 0; i < dv.Table.Rows.Count; i++)
            //{
            //    SerX += Convert.ToDateTime(dv.Table.Rows[i]["VisitDate"].ToString().Trim()).ToString("dd/MM/yyyy") + " ,";

            //    //For BP
            //    if (!dv.Table.Rows[i]["BP"].ToString().Trim().Equals(""))
            //    {
            //        SerYLBP += ((dv.Table.Rows[i]["BP"].ToString().Trim().Split('/'))[1].Trim().Split(' '))[0] + " ,";
            //        SerYHBP += (dv.Table.Rows[i]["BP"].ToString().Trim().Split('/'))[0].Trim() + " ,";
            //    }
            //    else
            //    {
            //        SerYLBP += null + "0,";
            //        SerYHBP += null + "0,";
            //    }

            //    //For Temprature
            //    if (!dv.Table.Rows[i]["Temprature"].ToString().Trim().Equals(""))
            //    {
            //        if ((dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[1].Trim().Equals("C"))
            //        {
            //            SerYTemp += Convert.ToString((1.8 * Convert.ToDouble((dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[0].Trim())) + 32) + " ,";
            //            ;
            //        }
            //        else
            //        {
            //            SerYTemp += (dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[0].Trim() + " ,";
            //        }
            //    }
            //    else
            //    {
            //        SerYTemp += null + "0,";
            //    }

            //    //For Pulse
            //    if (!dv.Table.Rows[i]["Pulse"].ToString().Trim().Equals(""))
            //    {
            //        SerYPulse += (dv.Table.Rows[i]["Pulse"].ToString().Trim().Split(' '))[0].Trim() + " ,";
            //    }
            //    else
            //    {
            //        SerYPulse += null + "0,";
            //    }

            //    //For Weight
            //    if (!dv.Table.Rows[i]["Weight"].ToString().Trim().Equals(""))
            //    {
            //        if (!(dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[1].Trim().Equals("Kg"))
            //        {
            //            SerYWeight += Convert.ToString(Math.Round(Convert.ToDouble((dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[0].Trim()) * 0.45359237, 2)) + " ,";
            //        }
            //        else
            //        {
            //            SerYWeight += (dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[0].Trim() + " ,";
            //        }
            //    }
            //    else
            //    {
            //        SerYWeight += null + "0,";
            //    }
            //    //For Height
            //    if (!dv.Table.Rows[i]["Height"].ToString().Trim().Equals(""))
            //    {
            //        if (!(dv.Table.Rows[i]["Height"].ToString().Trim().Split(' '))[1].Trim().Equals("Feet"))
            //        {
            //            SerYHeight += (Convert.ToDouble(dv.Table.Rows[i]["Height"].ToString().Trim().Split(' ')[0].Trim()) / 12).ToString() + " ,";
            //        }
            //        else
            //        {
            //            SerYHeight += (dv.Table.Rows[i]["Height"].ToString().Trim().Split(' '))[0].Trim() + " ,";
            //        }
            //    }
            //    else
            //    {
            //        SerYHeight += null + "0,";
            //    }
            //    //For HeadCircumferences
            //    if (!dv.Table.Rows[i]["HeadCircumferences"].ToString().Trim().Equals(""))
            //    {
            //        SerYHeadCircum += (dv.Table.Rows[i]["HeadCircumferences"].ToString().Trim().Split(' '))[0].Trim() + " ,";
            //    }
            //    else
            //    {
            //        SerYHeadCircum += null + "0,";
            //    }
            //}

            //if (SerX.Length > 0)
            //{
            //    SerX = SerX.Substring(0, SerX.Length - 1);
            //    SerYLBP = SerYLBP.Substring(0, SerYLBP.Length - 1);
            //    SerYHBP = SerYHBP.Substring(0, SerYHBP.Length - 1);
            //    SerYTemp = SerYTemp.Substring(0, SerYTemp.Length - 1);
            //    SerYPulse = SerYPulse.Substring(0, SerYPulse.Length - 1);
            //    SerYWeight = SerYWeight.Substring(0, SerYWeight.Length - 1);
            //    SerYHeight = SerYHeight.Substring(0, SerYHeight.Length - 1);
            //    SerYHeadCircum = SerYHeadCircum.Substring(0, SerYHeadCircum.Length - 1);
            //}
            //#endregion

            //serHBP.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //serLBP.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYLBP);
            //serHBP.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYHBP);
            //serTemp.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYTemp);
            //serPulse.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYPulse);

            //serWeight.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //serWeight.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYWeight);
            //serHeight.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYHeight);
            //serHeadCircum.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerYHeadCircum);
            //ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, "9,9,9,9,9,9");
            //ser1.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, "9,9,9,9,9,9");
        }

        private void btnVitalSignHistClose_Click(object sender, EventArgs e)
        {
            pnlVitalSignHist.Visible = false;
        }

        private void panel16_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv = new DataView();
            int colAdd = 0;

            ReportDocument report;
            if ((ReportDocument)crvVitalSign.ReportSource == null)
            {
                report = new ReportDocument();
            }
            else
            {
                report = (ReportDocument)crvVitalSign.ReportSource;
            }

            pnlVitalSignHist.Visible = true;

            pnlVitalSignHist.BringToFront();
            try
            {
                objConnection.Connection_Open();

                objOPD.PRNO = txtPRNO.Text;
                dv = objOPD.GetAll(14);
                report.Load(@"Report\VitalSignChart.rpt");

                string userName = clsSharedVariables.DBUserName;
                string pwd = clsSharedVariables.DBPassWord;
                string serverName = clsSharedVariables.DBServer;
                string sdb = clsSharedVariables.DBName;

                report.SetDatabaseLogon(userName, pwd, serverName, sdb);

                DataTable dt = new DataTable();

                dt.Columns.Add("lBP", typeof(Double));
                dt.Columns.Add("hBP", typeof(Double));
                dt.Columns.Add("Pulse", typeof(Double));
                dt.Columns.Add("Temprature", typeof(Double));
                dt.Columns.Add("Weight", typeof(Double));
                dt.Columns.Add("Height", typeof(Double));
                dt.Columns.Add("HeadCircumferences", typeof(Double));
                dt.Columns.Add("HeartRate", typeof(Double));
                dt.Columns.Add("RespiratoryRate", typeof(Double));
                dt.Columns.Add("VisitDate", typeof(DateTime));

                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    colAdd = 0;
                    DataRow dr = dt.NewRow();

                    //dr["VisitDate"]= Convert.ToDateTime(dv.Table.Rows[i]["VisitDate"].ToString().Trim()).ToString("dd/MM/yyyy") ;
                    dr["VisitDate"] = dv.Table.Rows[i]["VisitDate"].ToString().Trim();
                    //For BP
                    if (!dv.Table.Rows[i]["BP"].ToString().Trim().Equals(""))
                    {
                        dr["lbp"] = ((dv.Table.Rows[i]["BP"].ToString().Trim().Split('/'))[1].Trim().Split(' '))[0];
                        dr["hbp"] = (dv.Table.Rows[i]["BP"].ToString().Trim().Split('/'))[0].Trim();
                        colAdd++;
                    }
                    else
                    {
                        dr["lbp"] = "0";
                        dr["hbp"] = "0";
                    }

                    //For Temprature
                    if (!dv.Table.Rows[i]["Temprature"].ToString().Trim().Equals(""))
                    {
                        if ((dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[1].Trim().Equals("C"))
                        {
                            dr["Temprature"] = Convert.ToString((1.8 * Convert.ToDouble((dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[0].Trim())) + 32);
                        }
                        else
                        {
                            dr["Temprature"] = (dv.Table.Rows[i]["Temprature"].ToString().Trim().Split(' '))[0].Trim();
                        }
                        colAdd++;
                    }
                    else
                    {
                        dr["Temprature"] = "0";
                    }

                    //For Pulse
                    if (!dv.Table.Rows[i]["Pulse"].ToString().Trim().Equals(""))
                    {
                        dr["Pulse"] = (dv.Table.Rows[i]["Pulse"].ToString().Trim().Split(' '))[0].Trim();
                        colAdd++;
                    }
                    else
                    {
                        dr["Pulse"] = "0";
                    }

                    //For Weight
                    if (!dv.Table.Rows[i]["Weight"].ToString().Trim().Equals(""))
                    {
                        if (!(dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[1].Trim().Equals("Kg"))
                        {
                            dr["Weight"] = Convert.ToString(Math.Round(Convert.ToDouble((dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[0].Trim()) * 0.45359237, 2));
                        }
                        else
                        {
                            dr["weight"] = (dv.Table.Rows[i]["Weight"].ToString().Trim().Split(' '))[0].Trim();
                        }
                        colAdd++;
                    }
                    else
                    {
                        dr["Weight"] = "0";
                    }
                    //For Height
                    if (!dv.Table.Rows[i]["Height"].ToString().Trim().Equals(""))
                    {
                        if (!(dv.Table.Rows[i]["Height"].ToString().Trim().Split(' '))[1].Trim().Equals("Feet"))
                        {
                            dr["Height"] = (Convert.ToDouble(dv.Table.Rows[i]["Height"].ToString().Trim().Split(' ')[0].Trim()) / 12).ToString();
                        }
                        else
                        {
                            dr["Height"] = (dv.Table.Rows[i]["Height"].ToString().Trim().Split(' '))[0].Trim();
                        }
                        colAdd++;
                    }
                    else
                    {
                        dr["Height"] = "0";
                    }
                    //For HeadCircumferences
                    if (!dv.Table.Rows[i]["HeadCircumferences"].ToString().Trim().Equals(""))
                    {
                        dr["HeadCircumferences"] = (dv.Table.Rows[i]["HeadCircumferences"].ToString().Trim().Split(' '))[0].Trim();
                        colAdd++;
                    }
                    else
                    {
                        dr["HeadCircumferences"] = "0";
                    }
                    //Heart Rate
                    if (!dv.Table.Rows[i]["HeartRate"].ToString().Trim().Equals("") && dv.Table.Rows[i]["HeartRate"] != null)
                    {
                        dr["HeartRate"] = (dv.Table.Rows[i]["HeartRate"].ToString().Trim().Split(' '))[0].Trim();
                        colAdd++;
                    }
                    else
                    {
                        dr["HeartRate"] = "0";
                    }
                    //Respiratory Rate
                    if (!dv.Table.Rows[i]["RespiratoryRate"].ToString().Trim().Equals("") && dv.Table.Rows[i]["RespiratoryRate"] != null)
                    {
                        dr["RespiratoryRate"] = (dv.Table.Rows[i]["RespiratoryRate"].ToString().Trim().Split(' '))[0].Trim();
                        colAdd++;
                    }
                    else
                    {
                        dr["RespiratoryRate"] = "0";
                    }
                    if (colAdd != 0)
                    {
                        dt.Rows.Add(dr);
                    }
                }
                //int sum = 0;
                //for (int j = 0; j < dt.Columns.Count; j++)
                //{
                //    sum = 0;
                //    for (int k = 0; k < dt.Rows.Count; k++)
                //    {
                //        if (!dt.Rows[k][j].ToString().Equals("0"))
                //        {
                //            sum++;
                //        }
                //    }
                //    if (sum == 0)
                //    {
                //        dt.Columns.RemoveAt(j);
                //        j--;
                //    }
                //}
                report.SetDataSource(dt);

                crvVitalSign.ReportSource = report;

                //DrawGraph(dv);

                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);

            }
            finally
            {
                report = null;
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {
            pnlMedHistGrid.Visible = true;

            pnlMedHistGrid.BringToFront();
            pnlVitalSignHist.Visible = false;

            pnlMedHistGrid.Top = dgComplaints.Top;
            pnlMedHistGrid.Left = dgComplaints.Left;
            pnlMedHistGrid.Width = dgComplaints.Width + dgSigns.Width + 1;
            pnlMedHistGrid.Height = dgComplaints.Height + dgInvestigation.Height + 1;

            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);
            DataView dv = new DataView();
            DataTable dt = new DataTable();
            DataRow dr;
            string tmpVisitDate = "";

            dt.Columns.Add("visitdate");
            dt.Columns.Add("OPDID");
            dt.Columns.Add("MedicineID");
            dt.Columns.Add("MedicineName");
            dt.Columns.Add("MedicineDosage");

            try
            {
                objConnection.Connection_Open();
                objOPDMed.MedID = lblPatientID.Text;
                objOPDMed.OPDID = lblOPDID.Text;

                dv = objOPDMed.GetAll(3);

                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    dr = dt.NewRow();

                    if (!tmpVisitDate.Equals(dv.Table.Rows[i]["Visitdate"].ToString()))
                    {
                        dr["MedicineName"] = Convert.ToDateTime(dv.Table.Rows[i]["Visitdate"].ToString()).ToString("ddd, dd MMM yyyy");
                        dr["VisitDate"] = "1";
                        tmpVisitDate = dv.Table.Rows[i]["Visitdate"].ToString();
                        i--;
                    }
                    else
                    {
                        dr["VisitDate"] = "0";
                        dr["OPDID"] = dv.Table.Rows[i]["OPDID"].ToString();
                        dr["MedicineID"] = dv.Table.Rows[i]["MedicineID"].ToString();
                        dr["MedicineName"] = dv.Table.Rows[i]["MedicineName"].ToString();
                        dr["MedicineDosage"] = dv.Table.Rows[i]["MedicineDosage"].ToString();
                    }
                    dt.Rows.Add(dr);
                }


                dgMedHist.DataSource = dt;
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Font = new Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    dgMedHist.Columns["MHDosage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                for (int i = 0; i < dgMedHist.Rows.Count; i++)
                {
                    if (dgMedHist.Rows[i].Cells["MHVisitDate"].Value.ToString().Equals("1"))
                    {
                        dgMedHist.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                        dgMedHist.Rows[i].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    }
                    else
                    {
                        dgMedHist.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                }
                dgMedHist.ClearSelection();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                //dv.Dispose();
            }
        }

        private void rbVitalSignChart1_CheckedChanged(object sender, EventArgs e)
        {
            //csVitalSign1.Visible = true;
            //csVitalSign2.Visible = false;
        }

        private void rbVitalSignChart2_CheckedChanged(object sender, EventArgs e)
        {
            //csVitalSign2.Visible = true;
            //csVitalSign1.Visible = false;
        }

        private void miDosageInstruction_Click(object sender, EventArgs e)
        {
            frmDosageInstruction objDosIns = new frmDosageInstruction();
            objDosIns.ShowDialog();
            objDosIns.Dispose();
        }

        private void backupDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ///
            ///Export Database to Azeem user
            ///

            SaveFileDialog SaveDia = new SaveFileDialog();
            string tmestr = "";
            string value;
            StreamWriter sw;
            string path = "";
            FileInfo fi;

            tmestr = "Azeem" + DateTime.Now.ToString("yyyyMMddhhmmss");
            SaveDia.Filter = "*.sql | *.sql";
            SaveDia.FileName = tmestr;

            try
            {
                if (SaveDia.ShowDialog() == DialogResult.OK)
                {
                    fi = new FileInfo(SaveDia.FileName);

                    path = fi.DirectoryName;
                    sw = new StreamWriter(path + "\\Export.bat", false);

                    tmestr = "-uAzeem -ptrs_haisam_cm_1 Azeem>\"" + path + "\\Azeem" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".sql\"";

                    value = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\MySQL\MySQL Server 5.0\bin\mysqldump.exe"" " + tmestr;
                    sw.Write(value);
                    sw.Close();

                    ProcessStartInfo psi = new ProcessStartInfo();
                    psi.UseShellExecute = false;
                    psi.CreateNoWindow = false;
                    psi.FileName = path + "\\Export.bat";
                    psi.RedirectStandardError = true;
                    psi.RedirectStandardOutput = true;
                    psi.RedirectStandardInput = true;

                    Process process = Process.Start(psi);

                    process.BeginOutputReadLine();
                    process.StandardInput.WriteLine(value);
                    process.StandardInput.Close();
                    process.WaitForExit();
                    process.Close();
                }
                File.Delete(path + "\\Export.bat");
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error , unable to Create backup!");
            }

        }

        private void restoreDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ///
            ///Import Database to root user
            ///
            string value = "";
            OpenFileDialog OFD = new OpenFileDialog();
            string path = "";
            FileInfo fi;
            try
            {
                StreamWriter sw;
                OFD.Filter = "*.sql|*.sql";

                if (OFD.ShowDialog(this) == DialogResult.OK)
                {
                    fi = new FileInfo(OFD.FileName);

                    path = fi.DirectoryName;
                    sw = new StreamWriter(path + "\\Import.bat", false);

                    //value = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @" (x86)\MySQL\MySQL Server 5.0\bin\mysql.exe"" -uAzeem -ptrs_haisam_cm_1 Azeem<""" + OFD.FileName + "\"";
                    value = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\MySQL\MySQL Server 5.0\bin\mysql.exe"" -uAzeem -ptrs_haisam_cm_1 Azeem<""" + OFD.FileName + "\"";
                    sw.Write(value);
                    sw.Close();

                    ProcessStartInfo psi = new ProcessStartInfo();
                    psi.UseShellExecute = false;
                    psi.CreateNoWindow = false;
                    psi.FileName = path + "\\Import.bat";
                    psi.RedirectStandardError = true;
                    psi.RedirectStandardOutput = true;
                    psi.RedirectStandardInput = true;

                    Process process = Process.Start(psi);

                    process.BeginOutputReadLine();
                    process.StandardInput.WriteLine(value);
                    process.StandardInput.Close();
                    process.WaitForExit();
                    process.Close();

                    File.Delete(path + "\\Import.bat");
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error , unable to Restore!");
            }
        }

        private void llblSearchByPRNO_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            UnSelectedPatient();
            miHold.Text = "Hold Queue (" + CountConsultHold("H").ToString() + ")";
            miConsulted.Text = "Consulted (" + CountConsultHold("C").ToString() + ")";
            lblMode.Text = "Mode : Search ";
            pnlRegisterSearch.Enabled = true;
            lblRegCount.Text = "Patient Found : " + dgRegisterPatient.Rows.Count.ToString();

            pnlSerachByPRNo.Visible = true;
            pnlSerachByPRNo.BringToFront();
            txtSearchByPRNo.Focus();
            txtSearchByPRNo.SelectionStart = 0;
        }

        private void btnSearchPrnoClose_Click(object sender, EventArgs e)
        {
            pnlSerachByPRNo.Visible = false;
        }

        private void txtSerachPRNo_TextChanged(object sender, EventArgs e)
        {
            //if (txtSearchByPRNo.Text.Length == 12)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);

                objConnection.Connection_Open();
                txtSearchByPRNo.Mask = "" ;
                objPatient.PRNO = txtSearchByPRNo.Text.Trim();
                objPatient.MRNo = txtSearchByPRNo.Text.Replace("-",string.Empty);
                DataView dvPatient = objPatient.GetAll(2);

                if (dvPatient.Table.Rows.Count != 0)
                {
                    ClearField();
                    ClearVitalSigns();

                    miEnable(true);
                    pnlEnable(true);
                    pnlRPatient.Enabled = true;
                    btnPatientSave.Enabled = true;
                    dtpFollowUpDate.Enabled = true;
                    pnlTotalVisit.Visible = true;
                    pnlVisit.Visible = true;

                    pnlSelPatientDetail.Visible = true;
                    pnlSelPatientHead.Visible = true;

                    pnlSize(panel1.Size.Width, pnlVitalSigns.Top - pnlRegister.Top - 1);

                    lblSelect.Tag = "1";
                    lblOPDID.Tag = "U";
                    lblMode.Text = "Mode : Prescription";
                    pnlRegisterSearch.Enabled = false;
                   

                    lblPatientID.Text = dvPatient.Table.Rows[0]["PatientID"].ToString();
                    txtRPRNo.Mask = "";
                    txtPRNO.Mask = "";
                    txtRPRNo.Text = txtPRNO.Text = dvPatient.Table.Rows[0]["PRNO"].ToString();
                    txtRPRNo.ReadOnly = true;
                    txtPRNO.ReadOnly = true;
                    //GetPatientLastVisitOPDID(1);
                    PrevoiusInfo();
                    SavePatient.Text = dvPatient.Table.Rows[0]["PRNO"].ToString();
                    txtPatientName.Text = clsSharedVariables.InItCaps(dvPatient.Table.Rows[0]["PatientName"].ToString());
                    if (dvPatient.Table.Rows[0]["Gender"].ToString().Equals("Male"))
                    {
                        rbPatientMale.Checked = true;
                    }
                    else
                    {
                        rbPatientFemale.Checked = true;
                    }
                    dtpPatientDOB.Value = Convert.ToDateTime(dvPatient.Table.Rows[0]["DOB"].ToString());
                    txtPatientAge.Text = Convert.ToString(DateTime.Now.Year - dtpPatientDOB.Value.Year);
                    txtPatientPhone.Text = dvPatient.Table.Rows[0]["PhoneNo"].ToString();
                    txtPatientCell.Text = dvPatient.Table.Rows[0]["CellNo"].ToString();
                    txtPatientAddress.Text = clsSharedVariables.InItCaps(dvPatient.Table.Rows[0]["Address"].ToString());
                    cboPatientCity.Text = dvPatient.Table.Rows[0]["cityname"].ToString();

                    if (lblSelect.Tag.Equals("1"))
                    {
                        CreateInstructionCHB();
                    }
                    txtRegEmail.Text = dvPatient.Table.Rows[0]["Email"].ToString();

                    miHoldSave.Enabled = miConsultedSave.Enabled = true;
                    //cboRPanel.Text = dvPatient.Table.Rows[0]["panelName"].ToString();

                    txtSearchByPRNo.Text = "";
                    pnlSerachByPRNo.Visible = false;
                    CreateVisitLB("");
                }
                //else
                //{
                //    MessageBox.Show(" Patient Not Found ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}

                objConnection.Connection_Close();


                objConnection = null;
                objPatient = null;
                dvPatient = null;
            }
        }

        private void btnRegNewPateint_Click(object sender, EventArgs e)
        {
            btnNewPatient_Click(btnNewPatient, e);
            pnlSerachByPRNo.Visible = false;
        }

        private void miOption_Click(object sender, EventArgs e)
        {
            frmSetting objSetting = new frmSetting(this);
            objSetting.Show();

            this.Hide();
        }

        private void frmOPD_VisibleChanged(object sender, EventArgs e)
        {
            txtRPRNo.ReadOnly = txtCPRNo.ReadOnly = txtRPRNo.ReadOnly = clsSharedVariables.AutoPRNO;
        }

        private void btnStickyNotes_Click(object sender, EventArgs e)
        {
            if (btnStickyNotes.Tag.Equals("1"))
            {
                btnStickyNotes.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\collapse2222.png");
                btnStickyNotes.Tag = "0";
                pnlNotes.Height = pnlInfo.Height - (pnlAllerAR.Height + panel3.Height + pnlWaitingList.Height + 5);
            }
            else if (btnStickyNotes.Tag.Equals("0"))
            {
                btnStickyNotes.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\Exp2222.png");
                btnStickyNotes.Tag = "1";
                pnlNotes.Height = pnlAllerAR.Top - pnlWaitingList.Bottom - 2;
            }
            pnlNotes.BringToFront();
        }

        private void tsmiMonthlySummary_Click(object sender, EventArgs e)
        {
            frmMonthlySummary objMonthSumm = new frmMonthlySummary();
            objMonthSumm.ShowDialog(this);
            objMonthSumm.Dispose();
        }

        private void miAddDisease_Click(object sender, EventArgs e)
        {
            frmAddDiseaseFromList objAddDiseaseList = new frmAddDiseaseFromList(this);
            objAddDiseaseList.Show();
            this.Hide();
        }

        private void mcAppointment_DaySelected(object sender, Pabo.Calendar.DaySelectedEventArgs e)
        {
            PopulateAppointment();
        }

        private void mcAppointment_MonthChanged(object sender, Pabo.Calendar.MonthChangedEventArgs e)
        {
            FillAppointmenthCal();
        }

        private void FillAppointmenthCal()
        {
            #region Month Calender

            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAppointment objAppoint = new clsBLAppointment(objConnection);
            DataView dvAppointment;

            mcAppointment.Dates.Clear();

            Pabo.Calendar.DateItem[] DI;
            try
            {
                objAppoint.AppointmentDate = mcAppointment.ActiveMonth.Month.ToString("00") + "/" + mcAppointment.ActiveMonth.Year.ToString();

                objConnection.Connection_Open();
                dvAppointment = objAppoint.GetAll(7);

                DI = new Pabo.Calendar.DateItem[dvAppointment.Table.Rows.Count];

                for (int i = 0; i < dvAppointment.Table.Rows.Count; i++)
                {
                    DI[i] = new Pabo.Calendar.DateItem();
                    DI[i].Text = dvAppointment.Table.Rows[i]["Count"].ToString() + "/" + btnAppointment.Tag.ToString();
                    DI[i].Date = Convert.ToDateTime(dvAppointment.Table.Rows[i]["AppointmentDate"]).Date;
                    DI[i].BackColor1 = Color.LightBlue;
                }
                mcAppointment.AddDateInfo(DI);
                mcAppointment.Refresh();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objAppoint = null;
                objConnection = null;
                dvAppointment = null;
            }

            #endregion
        }

        private void llblDailyCashReport_Click(object sender, EventArgs e)
        {
            frmCashrptRange objCashRptRange = new frmCashrptRange();

            objCashRptRange.RptName = "OPD_001_05";
            objCashRptRange.ShowDialog();

            objCashRptRange.Dispose();
        }

        private void GetPatientDetail()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientDetail = new clsBLPatientPicture(objConnection);
            DataView dv;

            try
            {
                objPatientDetail.PatientID = lblPatientID.Text;
                objConnection.Connection_Open();

                dv = objPatientDetail.GetAll(6);
                //if (dv.Table.Rows[0]["Patientid"].ToString().Equals(""))
                if (dv.Table.Rows.Count == 0)
                {
                    lblPatientPicCount.Text = "(0)";
                    lblPatientVideoCount.Text = "(0)";
                    lblPatientDocCount.Text = "(0)";
                }
                else
                {
                    lblPatientPicCount.Text = "(" + dv.Table.Rows[0]["PicCount"].ToString() + ")";
                    lblPatientVideoCount.Text = "(" + dv.Table.Rows[0]["VideoCount"].ToString() + ")";
                    lblPatientDocCount.Text = "(" + dv.Table.Rows[0]["DocCount"].ToString() + ")";
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objPatientDetail = null;
                objConnection = null;
            }
        }

        private void GetPatientAlerAdver()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedAlerAdver objOPDMedAlerAdver = new clsBLOPDMedAlerAdver(objConnection);
            DataView dv;

            try
            {
                objOPDMedAlerAdver.PatientID = lblPatientID.Text;
                objConnection.Connection_Open();

                objOPDMedAlerAdver.Type = "A";
                dv = objOPDMedAlerAdver.GetAll(4);
                //if (dv.Table.Rows.Count == 0)
                {
                    //                    btnAllergies.Text = "Allergies(0)";
                    dgAllergies.DataSource = dv;
                }
                //else
                {
                    //btnAllergies.Text = "Allergies(" + dv.Table.Rows[0]["Calculate"].ToString() + ")";
                }
                objOPDMedAlerAdver.Type = "R";
                dv = objOPDMedAlerAdver.GetAll(4);
                dgAR.DataSource = dv;
                //if (dv.Table.Rows.Count == 0)
                //{
                //    //btnAdverseReaction.Text = "Adverse Reaction(0)";

                //}
                //else
                //{
                //    //btnAdverseReaction.Text = "Adverse Reaction(" + dv.Table.Rows[0]["Calculate"].ToString() + ")";
                //}

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objOPDMedAlerAdver = null;
                objConnection = null;
            }
        }

        private void GetPatientProcAdmssion()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAdmissionProc objAdmissionProc = new clsBLAdmissionProc(objConnection);
            DataView dv;

            try
            {
                objAdmissionProc.PatientID = lblPatientID.Text;
                objAdmissionProc.Type = "A";//Admission
                objConnection.Connection_Open();

                dv = objAdmissionProc.GetAll(2);

                dgAdmission.DataSource = dv;

                objAdmissionProc.Type = "P";//Procedure
                dv = objAdmissionProc.GetAll(2);
                dgProcedure.DataSource = dv;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection.Connection_Close();
                objAdmissionProc = null;
                objConnection = null;
            }
        }

        private void miStatusFill(ToolStripMenuItem mi)
        {
            DataView dv = GetStatus();
            Image img = null;

            mi.DropDownItems.Clear();

            if (dv != null && dv.Table.Rows.Count != 0)
            {
                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    mi.DropDownItems.Add(dv.Table.Rows[i]["Name"].ToString(), img, miStatusSub_Click);
                    mi.DropDownItems[i].Tag = dv.Table.Rows[i]["StatusID"].ToString();
                }
            }
        }

        private void miStatusSub_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            ContextMenuStrip cms = (ContextMenuStrip)tsmi.OwnerItem.Owner;
            DataGridView dg = new DataGridView();

            switch (cms.Items[2].Tag.ToString())
            {
                case "dgHistory":
                    UpdateStatus(cms.Items[1], dgHistory, tsmi);
                    break;
                case "dgComplaints":
                    UpdateStatus(cms.Items[1], dgComplaints, tsmi);
                    break;
                case "dgSigns":
                    UpdateStatus(cms.Items[1], dgSigns, tsmi);
                    break;
                case "dgDiagnosis":
                    UpdateStatus(cms.Items[1], dgDiagnosis, tsmi);
                    break;
                case "dgInvestigation":
                    UpdateStatus(cms.Items[1], dgInvestigation, tsmi);
                    break;
                case "dgPlans":
                    UpdateStatus(cms.Items[1], dgPlans, tsmi);
                    break;
            }
        }

        private DataView GetStatus()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLStatus objStatus = new clsBLStatus(objConnection);
            DataView dv;

            try
            {
                objConnection.Connection_Open();

                dv = objStatus.GetAll(2);
                return dv;
            }
            catch (Exception exc)
            {
                return null;
            }
            finally
            {
                objConnection.Connection_Close();
                objStatus = null;
                objConnection = null;
            }
        }

        private void UpdateStatus(ToolStripItem mi, DataGridView dg, ToolStripMenuItem tsmi)
        {
            if (mi.Tag != null && Convert.ToInt16(mi.Tag) != (dg.Rows.Count - 1))
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPDDetail objOPDDetail = new clsBLOPDDetail(objConnection);
                try
                {
                    if (dg.Rows[Convert.ToInt16(mi.Tag)].Cells[1].Value != null)
                    {
                        objOPDDetail.OPDID = lblOPDID.Text;
                        objOPDDetail.PrefID = dg.Rows[Convert.ToInt16(mi.Tag)].Cells[1].Value.ToString();
                        objOPDDetail.StatusID = tsmi.Tag.ToString();
                        objOPDDetail.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

                        objConnection.Connection_Open();
                        objConnection.Transaction_Begin();

                        if (objOPDDetail.GetAll(3).Table.Rows.Count == 0)
                        {
                            if (objOPDDetail.Insert())
                            {
                                objConnection.Transaction_ComRoll();
                                objConnection.Connection_Close();
                            }
                            else
                            {
                                MessageBox.Show(objOPDDetail.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            if (objOPDDetail.UpdateStatus())
                            {
                                objConnection.Transaction_ComRoll();
                                objConnection.Connection_Close();
                            }
                            else
                            {
                                MessageBox.Show(objOPDDetail.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    objOPDDetail = null;
                    objConnection = null;
                }
            }
        }

        private DataView GetStatus(int rowIdx, DataGridView dg)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDDetail objOPDDetail = new clsBLOPDDetail(objConnection);

            try
            {
                objConnection.Connection_Open();
                if (dg.Rows[rowIdx].Cells[1].Value != null)
                {
                    objOPDDetail.OPDID = lblOPDID.Text;
                    objOPDDetail.PrefID = dg.Rows[rowIdx].Cells[1].Value.ToString();

                    return objOPDDetail.GetAll(5);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                objConnection.Connection_Close();
                objOPDDetail = null;
                objConnection = null;
            }
        }

        private void dgPrameter_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;

            if (e.RowIndex != -1 && e.ColumnIndex == 0)
            {
                DataView dv = GetStatus(e.RowIndex, dg);

                if (dv != null && dv.Table.Rows.Count != 0)
                {
                    dg.Rows[e.RowIndex].Cells[0].ToolTipText = "Status : " + dv.Table.Rows[0]["Name"].ToString();
                    dg.Rows[e.RowIndex].Cells[0].ToolTipText += "\nDate   : " + dv.Table.Rows[0]["EnteredOn"].ToString();

                }
                else
                {
                    dg.Rows[e.RowIndex].Cells[0].ToolTipText = "No";
                }
            }
        }

        private void miICDMoreInfo_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            ContextMenuStrip cms = (ContextMenuStrip)tsmi.Owner;
            DataGridView dg = new DataGridView();

            switch (cms.Items[2].Tag.ToString())
            {
                case "dgHistory":
                    GetICDMoreInfo(cms.Items[1], dgHistory, tsmi, "HistDiseaseIDRef");
                    break;
                case "dgComplaints":
                    GetICDMoreInfo(cms.Items[1], dgComplaints, tsmi, "CompDiseaseIDRef");
                    break;
                case "dgSigns":
                    GetICDMoreInfo(cms.Items[1], dgSigns, tsmi, "SignDiseaseIDRef");
                    break;
                case "dgDiagnosis":
                    GetICDMoreInfo(cms.Items[1], dgDiagnosis, tsmi, "DiagnosisDiseaseRefID");
                    break;
                //case "dgInvestigation":
                //    GetICDMoreInfo(cms.Items[1], dgInvestigation, tsmi);
                //    break;
                //case "dgPlans":
                //    GetICDMoreInfo(cms.Items[1], dgPlans, tsmi);
                //    break;
            }
        }

        private void GetICDMoreInfo(ToolStripItem mi, DataGridView dg, ToolStripMenuItem tsmi, string DiseaseIDRefColName)
        {
            if (mi.Tag != null && Convert.ToInt16(mi.Tag) != (dg.Rows.Count - 1))
            {
                if (!dg.Rows[Convert.ToInt16(mi.Tag)].Cells[DiseaseIDRefColName].Value.ToString().Equals(""))
                {
                    //string msg = "";
                    //msg = GetICDDiseaseName(dg.Rows[Convert.ToInt16(mi.Tag)].Cells[DiseaseIDRefColName].Value.ToString());
                    //if (!msg.Equals(""))
                    //{
                    //    MessageBox.Show(msg, "ICD-10 Disease Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                    frmICDDiseaseMed objICDDisMed = new frmICDDiseaseMed();
                    objICDDisMed.DiseaseID = dg.Rows[Convert.ToInt16(mi.Tag)].Cells[DiseaseIDRefColName].Value.ToString();
                    objICDDisMed.ShowDialog();
                }
                else
                {
                    MessageBox.Show("ICD-10 Disease information is not Available", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private string GetICDDiseaseName(string DiseaseID)
        {
            clsBLDBConnection objConeection = new clsBLDBConnection();
            clsBLDisease objDisease = new clsBLDisease(objConeection);
            DataView dv = new DataView();

            try
            {
                objConeection.Connection_Open();
                objDisease.DiseaseID = DiseaseID;
                dv = objDisease.GetAll(5);
                if (dv.Table.Rows.Count != 0)
                {
                    return dv.Table.Rows[0]["DISEASENAME"].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Get ICD-10 Disease Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
            finally
            {
                objConeection.Connection_Close();
                objConeection = null;
                dv = null;
                objDisease = null;
            }

        }

        private void btnAdverseReaction_Click(object sender, EventArgs e)
        {
            frmPatientAlergAdverse objPatientAlergAdverse = new frmPatientAlergAdverse(this);

            objPatientAlergAdverse.PatientID = lblPatientID.Text;
            objPatientAlergAdverse.Type = "R";
            objPatientAlergAdverse.Show();
            this.Hide();
        }

        private void btnAllergies_Click(object sender, EventArgs e)
        {
            frmPatientAlergAdverse objPatientAlergAdverse = new frmPatientAlergAdverse(this);
            objPatientAlergAdverse.PatientID = lblPatientID.Text;
            objPatientAlergAdverse.Type = "A";
            objPatientAlergAdverse.Show();
            this.Hide();
        }

        private void btnAllergARExpend_Click(object sender, EventArgs e)
        {
            if (btnAllergARExpend.Tag.Equals("1"))
            {
                btnAllergARExpend.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\collapse2222.png");
                btnAllergARExpend.Tag = "0";
                pnlAllerAR.Height = pnlInfo.Height - (pnlNotes.Height + pnlAppointment.Height + panel3.Height + pnlWaitingList.Height + 6);
            }
            else if (btnAllergARExpend.Tag.Equals("0"))
            {
                btnAllergARExpend.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\Exp2222.png");
                btnAllergARExpend.Tag = "1";
                pnlAllerAR.Height = panel16.Top - pnlNotes.Bottom - 2;
            }
            pnlAllerAR.BringToFront();
        }

        private void miAllergies_Click(object sender, EventArgs e)
        {
            if (cmsMed.Items[0].Tag != null)
            {
                frmPatientAlergAdverse objPatientAlergAdverse = new frmPatientAlergAdverse(this);
                objPatientAlergAdverse.PatientID = lblPatientID.Text;
                objPatientAlergAdverse.gMedID = dgMedicine.Rows[Convert.ToInt16(cmsMed.Items[0].Tag)].Cells["MedID"].Value.ToString();
                objPatientAlergAdverse.gMedName = dgMedicine.Rows[Convert.ToInt16(cmsMed.Items[0].Tag)].Cells["MedName"].Value.ToString();

                objPatientAlergAdverse.Type = "A";

                objPatientAlergAdverse.Show();
                this.Hide();
            }
        }

        private void miAR_Click(object sender, EventArgs e)
        {
            if (cmsMed.Items[0].Tag != null)
            {
                frmPatientAlergAdverse objPatientAlergAdverse = new frmPatientAlergAdverse(this);
                objPatientAlergAdverse.PatientID = lblPatientID.Text;
                objPatientAlergAdverse.gMedID = dgMedicine.Rows[Convert.ToInt16(cmsMed.Items[0].Tag)].Cells["MedID"].Value.ToString();
                ;
                objPatientAlergAdverse.gMedName = dgMedicine.Rows[Convert.ToInt16(cmsMed.Items[0].Tag)].Cells["MedName"].Value.ToString();
                objPatientAlergAdverse.Type = "R";
                objPatientAlergAdverse.Show();
                this.Hide();
            }
        }

        private void dgMedicine_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;

            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dg.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dg.ContextMenuStrip.Items[0].Enabled = true;
                dg.ContextMenuStrip.Items[1].Enabled = true;
                dg.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dg.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dg.ContextMenuStrip.Items[0].Enabled = false;
                dg.ContextMenuStrip.Items[1].Enabled = false;
                dg.ContextMenuStrip.Items[0].Tag = null;
            }

            hi = null;
        }

        private void btnLabsTest_Click(object sender, EventArgs e)
        {
            LIMSReportStation objLimRpt = new LIMSReportStation(this);
            objLimRpt.PatientID = lblPatientID.Text;
            objLimRpt.Show();
            this.Hide();
        }

        private void lblPatientPicCount_Click(object sender, EventArgs e)
        {
            frmPicture objPic = new frmPicture(lblPatientID.Text, this);
            objPic.Show();
            this.Hide();
        }

        private void lblPatientDocCount_Click(object sender, EventArgs e)
        {
            frmDocument objDoc = new frmDocument(lblPatientID.Text, this);
            this.Hide();
            objDoc.Show();
        }

        private void lblPatientVideoCount_Click(object sender, EventArgs e)
        {
            frmVideo objVdo = new frmVideo(lblPatientID.Text, this);
            objVdo.Show();
            this.Hide();
        }

        private void btnProcAdmissionExp_Click(object sender, EventArgs e)
        {
            if (btnProcAdmissionExp.Tag.Equals("1"))
            {
                btnProcAdmissionExp.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\collapse2222.png");
                btnProcAdmissionExp.Tag = "0";
                pnlProcAdmission.Height = pnlInfo.Height - (pnlAllerAR.Height + panel3.Height + pnlWaitingList.Height + panel16.Height + pnlSelPatientDetail.Height + pnlSelPatientHead.Height + pnlMedHist.Height + pnlNotes.Height + pnlAppointment.Height + 9);
            }
            else if (btnProcAdmissionExp.Tag.Equals("0"))
            {
                btnProcAdmissionExp.BackgroundImage = Image.FromFile(Application.StartupPath + "\\Icons\\Exp2222.png");
                btnProcAdmissionExp.Tag = "1";
                pnlProcAdmission.Height = pnlTotalVisit.Top - pnlSelPatientDetail.Bottom - 2;
            }
            pnlProcAdmission.BringToFront();
        }

        private void btnAddAdmProc_Click(object sender, EventArgs e)
        {
            frmAdmissionProc objAdmissionProc = new frmAdmissionProc(this);
            objAdmissionProc.PatientID = lblPatientID.Text;
            objAdmissionProc.Show();
            this.Hide();
        }

        private void btnNotesDetail_Click(object sender, EventArgs e)
        {
            frmPatientNotes objPNotes = new frmPatientNotes();
            objPNotes.PatientID = lblPatientID.Text;
            objPNotes.ShowDialog();
        }

        private void miControlePnl_Click(object sender, EventArgs e)
        {

        }

        private void crvVitalSign_Load(object sender, EventArgs e)
        {

        }

        private void pnlAppointment_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlVitalSignHist_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblNotes_Click(object sender, EventArgs e)
        {

        }

        private void lblWaitingQueue_Click(object sender, EventArgs e)
        {

        }

        private void lblAppointment_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblPatientPhone_Click(object sender, EventArgs e)
        {

        }

        private void RemoveMask_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //search
            this.txtSearchPRNO.Mask = "";
            this.txtCSPRNO.Mask = "";
            this.txtHSPRNO.Mask = "";
            this.txtSearchByPRNo.Mask = "";
            //Register
            this.txtRPRNo.Mask = "";
            this.txtCPRNo.Mask = "";
            this.txtHPRNo.Mask = "";
            this.RemoveMask.Visible = false;
            this.ShowMask.Visible = true;
        }

        private void ShowMask_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.RemoveMask.Visible = true;
            this.ShowMask.Visible = false;
            //search
            this.txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtCSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtHSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtSearchByPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            //Register
            this.txtRPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtCPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.txtHPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();

        }

        private void RemoveMask_LinkClicked(object sender, EventArgs e)
        {

        }

        private void RemoveMask_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //search
            this.txtSearchPRNO.Mask = "";
            this.txtCSPRNO.Mask = "";
            this.txtHSPRNO.Mask = "";
            this.txtSearchByPRNo.Mask = "";
            //Register
            this.txtRPRNo.Mask = "";
            this.txtCPRNo.Mask = "";
            this.txtHPRNo.Mask = "";
            this.RemoveMask.Visible = false;
            this.ShowMask.Visible = true;
        }

        private void btnChangeExit_Click(object sender, EventArgs e)
        {
            this.txtHSPRNO.Mask = "";
            this.button6.Visible = false;
            this.button1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.txtHSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.button6.Visible = true;
            this.button1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.txtHPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            this.button3.Visible = true;
            this.button2.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.txtHPRNo.Mask = "";
            this.button3.Visible = false;
            this.button2.Visible = true;
        }

        private void txtCSPRNO_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.txtCSPRNO.Mask = "";
            this.button4.Visible = false;
            this.button7.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.button7.Visible = false;
            this.button4.Visible = true;
            this.txtCSPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.txtCPRNo.Mask = "";
            this.button8.Visible = false;
            this.button10.Visible = true; 
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.button10.Visible = false;
            this.button8.Visible = true;
            this.txtCPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.txtSearchPRNO.Mask = "";
            this.button11.Visible = false;
            this.button5.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.button5.Visible = false;
            this.button11.Visible = true;

            this.txtSearchPRNO.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.button12.Visible = false;
            this.button9.Visible = true; ;
            this.txtRPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.button9.Visible = false;
            this.button12.Visible = true;
            this.txtRPRNo.Mask = "";
        }

        private void VideoOption_CheckedChanged(object sender, EventArgs e)
        {

        }

        private async void SynchPatients_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to Save Patients On Server ??",
                                     "Confirm!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
               var CheckInternetConnection= await CheckInterNetConnection();
                if(CheckInternetConnection=="true")
                {
                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
                    DataView dvPatient;
                    List<PatientViewModel> list = new List<PatientViewModel>();
                    objConnection.Connection_Open();
                    dvPatient = objPatient.GetAll(17);

                    objConnection.Connection_Close();
                    for (int i = 0; i < dvPatient.Count - 1; i++)
                    {
                        var n = new PatientViewModel();
                        n.Id = 0;
                        n.MRNo = dvPatient.Table.Rows[i]["PRNO"].ToString();
                        n.PatientId = Convert.ToInt32(dvPatient.Table.Rows[i]["PatientID"]);
                        n.Name = dvPatient.Table.Rows[i]["Name"].ToString();
                        if (dvPatient.Table.Rows[i]["UserName"].ToString() == "")
                        {
                            n.UserName = n.MRNo;
                        }
                        else
                        {
                            n.UserName = dvPatient.Table.Rows[i]["UserName"].ToString();

                        }
                       
                        if (dvPatient.Table.Rows[i]["Password"].ToString() == "")
                        {
                            n.Password = GenerateRendomNumber(1, 100000).ToString();
                        }
                        else
                        {
                            n.Password = dvPatient.Table.Rows[i]["Password"].ToString();

                        }
                        if (dvPatient.Table.Rows[i]["UserName"].ToString() == "")
                        {
                            objConnection.Connection_Open();
                            var Response = objPatient.UpdateUserNameAndPassword(n.UserName,n.Password,n.PatientId);
                            objConnection.Connection_Close();
                        }
                      
                        n.Gender = dvPatient.Table.Rows[i]["Gender"].ToString();
                        n.DOB = dvPatient.Table.Rows[i]["DOB"].ToString();
                        n.WhatsappNumber = dvPatient.Table.Rows[i]["CellNo"].ToString();
                        n.Address = dvPatient.Table.Rows[i]["Address"].ToString();
                        n.Email = dvPatient.Table.Rows[i]["Email"].ToString();

                        n.MartialStatus = dvPatient.Table.Rows[i]["MaritalStatus"].ToString();

                        n.ClinicId = Convert.ToDecimal(System.Configuration.ConfigurationSettings.AppSettings["ClinicId"].ToString());
                        n.DoctorId = Convert.ToDecimal(clsSharedVariables.UserID);

                        list.Add(n);
                    }
                    string res = "";
                    res = await SavePatientAPI(list);
                    if (res == "True")
                    {
                        objConnection.Connection_Open();
                        var Response = objPatient.UpdatePatientsStatus();
                        objConnection.Connection_Close();
                    }
                   

                    if (res == "True")
                    {
                       
                       
                            MessageBox.Show("Patient Send To Server Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                        MessageBox.Show("Please Check your Internet Connection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Please Check your Internet Connection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
            else
            {
                // If 'No', do something here.
            }
        }
        public async Task<string> SavePatientAPI(List< PatientViewModel> list)
        {
            var res = "";
            try
            {
                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // New code:
                    HttpResponseMessage response = await client.PostAsJsonAsync("api/Patient/PatientRegister", list);
                    if (response.IsSuccessStatusCode)
                    {
                        res = await response.Content.ReadAsAsync<string>();
                    }
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public async Task<string> CheckInterNetConnection()
        {
            var res = "";
            try
            {

                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync("api/Patient/CheckInternetConnection");
                    if (response.IsSuccessStatusCode)
                    {
                        res = await response.Content.ReadAsAsync<string>();


                    }
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        private async void SendPatientDetailToServer_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to Save Patients Details On Server ??",
                                   "Confirm!!",
                                   MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                var checkinternet = await CheckInterNetConnection();
                if(checkinternet == "true")
                {
                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    clsBLOPD objOPD = new clsBLOPD(objConnection);
                    DataView dvOPD;
                    List<PatientOPDDetails> list = new List<PatientOPDDetails>();
                    objConnection.Connection_Open();
                    dvOPD = objOPD.GetAll(21);

                    objConnection.Connection_Close();

                    //Connection for Medicine
                    clsBLDBConnection objConnection1 = new clsBLDBConnection();

                    clsBLOPDMedicine objOPDMedicine = new clsBLOPDMedicine(objConnection1);
                    objConnection1.Connection_Open();
                    for (int i = 0; i < dvOPD.Count - 1; i++)
                    {
                        var n = new PatientOPDDetails();
                        DataView devMed;
                        objOPDMedicine.OPDID= dvOPD.Table.Rows[i]["opdid"].ToString();
                        devMed = objOPDMedicine.GetAll(2);
                        var MedicineList = new List<PatientMedicineMedicine>();
                        for (int x=0;x<devMed.Count-1;x++)
                        {
                            var a = new PatientMedicineMedicine();
                            a.MedicineName= devMed.Table.Rows[x]["MedicineName"].ToString();
                            a.Dosage = devMed.Table.Rows[x]["MedicineDosage"].ToString();
                            a.MedicineId = devMed.Table.Rows[x]["MedicineID"].ToString();
                            a.OpdId = devMed.Table.Rows[x]["OPDID"].ToString();
                            MedicineList.Add(a);
                        }
                        n.Medicinelist = MedicineList;
                        n.Id = 0;
                        n.UserName = dvOPD.Table.Rows[i]["UserName"].ToString();

                        n.Patientid = dvOPD.Table.Rows[i]["Patientid"].ToString();
                        n.UserName = dvOPD.Table.Rows[i]["UserName"].ToString();
                        n.Password = dvOPD.Table.Rows[i]["Password"].ToString();
                        n.opdid = dvOPD.Table.Rows[i]["opdid"].ToString();
                        n.PresentingComplaints = dvOPD.Table.Rows[i]["PresentingComplaints"].ToString();
                        n.Diagnosis = dvOPD.Table.Rows[i]["Diagnosis"].ToString();

                        n.Signs = dvOPD.Table.Rows[i]["Signs"].ToString();
                        n.BP = dvOPD.Table.Rows[i]["BP"].ToString();
                        n.Pulse = dvOPD.Table.Rows[i]["Pulse"].ToString();
                        n.Temprature = dvOPD.Table.Rows[i]["Temprature"].ToString();
                        n.Weight = dvOPD.Table.Rows[i]["Weight"].ToString();
                        n.Height = dvOPD.Table.Rows[i]["Height"].ToString();
                        n.Active = dvOPD.Table.Rows[i]["Active"].ToString();
                        n.DisposalPlan = dvOPD.Table.Rows[i]["DisposalPlan"].ToString();
                        n.Investigation = dvOPD.Table.Rows[i]["Investigation"].ToString();
                        n.HeadCircumferences = dvOPD.Table.Rows[i]["HeadCircumferences"].ToString();
                        n.Instruction = dvOPD.Table.Rows[i]["Instruction"].ToString();
                        n.History = dvOPD.Table.Rows[i]["History"].ToString();
                        n.EnteredOn = dvOPD.Table.Rows[i]["EnteredOn"].ToString();
                        n.Status = dvOPD.Table.Rows[i]["Status"].ToString();
                        n.BMI = dvOPD.Table.Rows[i]["BMI"].ToString();
                        n.BSA = dvOPD.Table.Rows[i]["BSA"].ToString();
                        n.FollowUpDate = dvOPD.Table.Rows[i]["FollowUpDate"].ToString();
                        n.VisitDate = dvOPD.Table.Rows[i]["VisitDate"].ToString();
                        n.Notes = dvOPD.Table.Rows[i]["Notes"].ToString();
                        n.HeartRate = dvOPD.Table.Rows[i]["HeartRate"].ToString();
                        n.RespiratoryRate = dvOPD.Table.Rows[i]["RespiratoryRate"].ToString();

                        n.DoctorId = Convert.ToDecimal(clsSharedVariables.UserID);

                        list.Add(n);
                    }
                    objConnection1.Connection_Close();
                    string res = "";
                    res = await SendOPDDetailToServerAPI(list);
                    if (res == "true")
                    {
                        objConnection.Connection_Open();
                        var Response = objOPD.UpdateSendStatus();
                        objConnection.Connection_Close();
                    }
                    //foreach (var item in list)
                    //{
                    //    res = await SendOPDDetailToServerAPI(item);
                    //    if(res=="True")
                    //    {
                    //        objConnection.Connection_Open();
                    //        var Response = objOPD.UpdateSendStatus(item.opdid);
                    //        objConnection.Connection_Close();
                    //    }

                    //}

                    if (res == "true")
                    {
                       

                            MessageBox.Show("Patient Detail To Server Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                        MessageBox.Show("Please Check your Internet Connection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Please Check your Internet Connection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                

            }
            else
            {
                // If 'No', do something here.
            }
        }
        public async Task<string> SendOPDDetailToServerAPI(List<PatientOPDDetails> list)
        {
    var res = "";
            try
            {

                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.PostAsJsonAsync("api/Patient/SavePatientDetailOPD", list);
                    if (response.IsSuccessStatusCode)
                    {
                        res = await response.Content.ReadAsAsync<string>();


                    }
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.txtSearchByPRNo.Mask = "";
            this.button14.Visible = true;
            this.button13.Visible = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            this.button14.Visible = false;
            this.button13.Visible = true;
            this.txtSearchByPRNo.Mask = System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
        }

        private void dtpCDOB_ValueChanged(object sender, EventArgs e)
        {
            var date = Convert.ToDateTime(this.dtpCDOB.Value);
            var age = DateTime.Now.Year - date.Year;
            this.txtCAge.Text = age.ToString() ;
        }

        private void ViewPatientDocument_Click(object sender, EventArgs e)
        {
            frmPatientDocuments objDoc = new frmPatientDocuments(lblPatientID.Text, this);
            if (!miRegistration.Enabled)
            {
                objDoc.PatientName = txtPatientName.Text;
            }
            else if (!miHold.Enabled)
            {
                objDoc.PatientName = txtHName.Text;
            }
            else if (!miConsulted.Enabled)
            {
                objDoc.PatientName = txtCName.Text;
            }
            this.Hide();
            objDoc.Show();
        }

        private void Notifications_Click(object sender, EventArgs e)
        {
            Notifications n = new Notifications(this);
            this.Hide();
            n.Show();
        }
    }
}