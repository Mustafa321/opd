using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
  public partial class frmPhrmacyMed : Form
  {
    public frmPhrmacyMed()
    {
      InitializeComponent();
    }

    private void frmPhrmacyMed_Load(object sender, EventArgs e)
    {
      SComponents objComp = new SComponents();
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPersonal objPerson = new clsBLPersonal(objConnection);

      objComp.ApplyStyleToControls(this);
      FillPatientDG();
      if (dgPatient.Rows.Count != 0)
      {
        FillMedDG(dgPatient.Rows[0].Cells["OPDID"].Value.ToString());
      }
      else
      {
        ClearGridRows(dgOPDMed);
        lblMed.Text = "Total Medicine = 0";
      }

      objConnection.Connection_Open();
      objComp.FillComboBox(cboConsultant, objPerson.GetAll(6), "Name", "Personid", true);
      objConnection.Connection_Close();

      objComp = null;
    }

    private void FillPatientDG()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);

      try
      {
        objConnection.Connection_Open();
        dgPatient.DataSource = objPatientReg.GetAll(14);
        lblTitalPatient.Text = "Total Patient = " + dgPatient.Rows.Count.ToString();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Fill data Grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objConnection.Connection_Close();
        objConnection = null;
        objPatientReg = null;
      }
    }

    private void FillMedDG(string OPDID)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);

      try
      {
        ClearGridRows(dgOPDMed);
        objConnection.Connection_Open();
        objOPDMed.OPDID = OPDID;
        dgOPDMed.DataSource = objOPDMed.GetAll(4);
        lblMed.Text = "Total Medicine = " + dgOPDMed.Rows.Count.ToString();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Fill data Grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objConnection.Connection_Close();
        objConnection = null;
        objOPDMed = null;
      }
    }

    private void dgPatient_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != -1)
      {
        if (dgPatient.Rows.Count != 0)
        {
          FillMedDG(dgPatient.Rows[e.RowIndex].Cells["OPDID"].Value.ToString());
          lbldgMedHeading.Text = "";
        }
        else
        {
          ClearGridRows(dgOPDMed);
          lblMed.Text = "Total Medicine = 0";
        }
      }
    }

    private void ClearGridRows(DataGridView dg)
    {
      try
      {
        DataView dv = (DataView)dg.DataSource;

        if (dv != null)
        {
          dv.Table.Rows.Clear();
          dg.DataSource = dv;
        }
      }
      catch
      {
        DataTable dt = (DataTable)dg.DataSource;

        if (dt != null)
        {
          dt.Rows.Clear();
          dg.DataSource = dt;
        }
      }
      //while (dg.Rows.Count > 0)
      //{
      //  dg.Rows.RemoveAt(0);
      //}
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);

      try
      {
          if (!txtPRNo.Text.Trim().Equals("-  -") && !txtPRNo.Text.Trim().Equals(""))
        {
          objPatientReg.PRNO = txtPRNo.Text.Trim();
        }

        if (!txtName.Text.Trim().Equals(""))
        {
          objPatientReg.Name = txtName.Text.Trim();
        }

        if (!txtCellNo.Text.Trim().Equals(""))
        {
          objPatientReg.CellNo = txtCellNo.Text.Trim();
        }

        if (!txtPhoneNo.Text.Trim().Equals(""))
        {
          objPatientReg.PhoneNo = txtPhoneNo.Text.Trim();
        }

        if (!cboConsultant.Text.Trim().Equals("Select"))
        {
          objPatientReg.EnteredBy = cboConsultant.SelectedValue.ToString();
        }

        objConnection.Connection_Open();
        dgPatient.DataSource = objPatientReg.GetAll(14);

        lblTitalPatient.Text = "Total Patient = " + dgPatient.Rows.Count.ToString();

        if (dgPatient.Rows.Count != 0)
        {
          FillMedDG(dgPatient.Rows[0].Cells["OPDID"].Value.ToString());
        }
        else
        {
          ClearGridRows(dgOPDMed);
          lblMed.Text = "Total Medicine = 0";
        }

      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Fill data Grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objConnection.Connection_Close();
        objConnection = null;
        objPatientReg = null;
      }
    }

    private void miRefresh_Click(object sender, EventArgs e)
    {
      ClearList();
    }

    private void ClearList()
    {
      txtPRNo.Text = "";
      txtCellNo.Text = "";
      txtName.Text = "";
      txtPhoneNo.Text = "";
      txtGrandTotal.Text = "0";
      txtGrandTotal.Tag = "0";
      nudDiscount.Value = 0;

      FillPatientDG();
      if (dgPatient.Rows.Count != 0)
      {
        FillMedDG(dgPatient.Rows[0].Cells["OPDID"].Value.ToString());
      }
      else
      {
        ClearGridRows(dgOPDMed);
        lblMed.Text = "Total Medicine = 0";
      }
    }

    private void miExit_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void miSave_Click(object sender, EventArgs e)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection);
      lblCellNo.Focus();
      
      try
      {
        objConnection.Connection_Open();
        objConnection.Transaction_Begin();

        for (int i = 0; i < dgOPDMed.Rows.Count; i++)
        {
          if (dgOPDMed.Rows[i].Cells["NotAvail"].Value != null && dgOPDMed.Rows[i].Cells["NotAvail"].Value.ToString().Equals("1"))
          {
            if (dgOPDMed.Rows[i].Cells["Deliver"].Value != null && dgOPDMed.Rows[i].Cells["Deliver"].Value.ToString().Equals("1"))
            {
              //objOPDMed.Status = "Y";
              //objOPDMed.IssuedBy = clsSharedVariables.UserID;
              //objOPDMed.IuusedOn = DateTime.Now.ToString("dd/MM/yyyy");

              if (dgOPDMed.Rows[i].Cells["Quantity"].Value != null && !dgOPDMed.Rows[i].Cells["Quantity"].Value.ToString().Trim().Equals(""))
              {
                //objOPDMed.Qty = dgOPDMed.Rows[i].Cells["Quantity"].Value.ToString().Trim();
              }

              if (dgOPDMed.Rows[i].Cells["UnitPrice"].Value != null && !dgOPDMed.Rows[i].Cells["UnitPrice"].Value.ToString().Trim().Equals(""))
              {
                //objOPDMed.Price = dgOPDMed.Rows[i].Cells["UnitPrice"].Value.ToString().Trim();
              }
              objOPDMed.OPDID = dgOPDMed.Rows[i].Cells["MOPDID"].Value.ToString();
              objOPDMed.MedID = dgOPDMed.Rows[i].Cells["MedID"].Value.ToString();

              //objOPDMed.UpdateStatus();
            }
          }
          else
          {
            //objOPDMed.Status = "N";
            //objOPDMed.IssuedBy = clsSharedVariables.UserID;
            //objOPDMed.IuusedOn = DateTime.Now.ToString("dd/MM/yyyy");

            objOPDMed.OPDID = dgOPDMed.Rows[i].Cells["MOPDID"].Value.ToString();
            objOPDMed.MedID = dgOPDMed.Rows[i].Cells["MedID"].Value.ToString();

            //objOPDMed.UpdateStatus();
          }
        }
        objConnection.Transaction_ComRoll();
        if (dgOPDMed.Rows.Count != 0)
        {
          PrintSlip(dgOPDMed.Rows[0].Cells["MOPDID"].Value.ToString().Trim());
        }
        ClearList();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Update Medicine Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      finally
      {
        objConnection.Connection_Close();
        objConnection = null;
        objOPDMed = null;
      }
    }

    private void dgOPDMed_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
    {
      e.Control.KeyPress += new KeyPressEventHandler(this.Numeric_KeyPress);
    }

    private void Numeric_KeyPress(object sender, KeyPressEventArgs e)
    {
      string str = "0123456789.\b";
      e.Handled = !(str.Contains(e.KeyChar.ToString()));
    }

    private void dgOPDMed_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != -1)
      {
        if (e.ColumnIndex == 1 || e.ColumnIndex == 0)
        {
          if (dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value == null)
          {
            dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = "0";
            dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = "0";
          }
          else
          {
            if (dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value != null && dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value != null && !dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value.ToString().Trim().Equals("") && !dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value.ToString().Trim().Equals(""))
            {
              dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = Convert.ToDouble(dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value) * Convert.ToDouble(dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value);
              txtGrandTotal.Text = CalcGrandTotal().ToString();
              txtGrandTotal.Tag = CalcGrandTotal().ToString();
              nudDiscount.Value = 0;
            }
          }
        }
        if (e.ColumnIndex == 2)
        {
          if (dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value == null)
          {
            dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = "0";
            dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = "0";
          }
          else
          {
            if (dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value != null && dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value != null && !dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value.ToString().Trim().Equals("") && !dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value.ToString().Trim().Equals(""))
            {
              if (!dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value.ToString().Trim().Equals("0"))
              {
                dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = Math.Round(Convert.ToDouble(dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value) / Convert.ToDouble(dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value), 2);
              }
              else
              {
                dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = "0";
                dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = "0";
              }
              txtGrandTotal.Text = CalcGrandTotal().ToString();
              txtGrandTotal.Tag = CalcGrandTotal().ToString();
              nudDiscount.Value = 0;
            }
          }
        }
      }
    }

    private Double CalcGrandTotal()
    {
      Double GT = 0;
      for (int i = 0; i < dgOPDMed.Rows.Count; i++)
      {
        if (dgOPDMed.Rows[i].Cells["Price"].Value != null && !dgOPDMed.Rows[i].Cells["Price"].Value.ToString().Trim().Equals(""))
        {
          GT = GT + Convert.ToDouble(dgOPDMed.Rows[i].Cells["Price"].Value.ToString().Trim());
        }
      }
      return GT;
    }

    private void dgOPDMed_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex == 7)
      {
        if (dgOPDMed.Rows[e.RowIndex].Cells["Deliver"].Value != null && dgOPDMed.Rows[e.RowIndex].Cells["Deliver"].Value.ToString().Equals("1"))
        {
          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value = "0";
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = "0";
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = "0";

          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].ReadOnly = true;
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].ReadOnly = true;
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].ReadOnly = true;
        }
        else
        {
          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].ReadOnly = false;
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].ReadOnly = false;
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].ReadOnly = false;
        }
      }
      else if (e.ColumnIndex == 8)
      {
        if (dgOPDMed.Rows[e.RowIndex].Cells["NotAvail"].Value != null && dgOPDMed.Rows[e.RowIndex].Cells["NotAvail"].Value.ToString().Equals("1"))
        {
          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].Value = "0";
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].Value = "0";
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].Value = "0";
          dgOPDMed.Rows[e.RowIndex].Cells["Deliver"].Value = "0";

          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].ReadOnly = true;
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].ReadOnly = true;
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].ReadOnly = true;
          dgOPDMed.Rows[e.RowIndex].Cells["Deliver"].ReadOnly = true;
        }
        else
        {
          dgOPDMed.Rows[e.RowIndex].Cells["Quantity"].ReadOnly = false;
          dgOPDMed.Rows[e.RowIndex].Cells["UnitPrice"].ReadOnly = false;
          dgOPDMed.Rows[e.RowIndex].Cells["Price"].ReadOnly = false;
          dgOPDMed.Rows[e.RowIndex].Cells["Deliver"].ReadOnly = false;
        }
      }
    }

    private void PrintSlip(string OPDID)
    {
      frmRptBill objMedSlip = new frmRptBill();

      try
      {
        //objMedSlip.OPDID = OPDID;
        objMedSlip.ReportReference = "OPD_001_08";
        objMedSlip.ShowDialog();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Pharmacy Medicine Slip", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        //objMedSlip.Dispose();
      }
    }

    private void btnMedDetailCost_Click(object sender, EventArgs e)
    {

      frmCashrptRange objCashRptRange = new frmCashrptRange();

      objCashRptRange.RptName = "OPD_001_09";
      objCashRptRange.ShowDialog();

      objCashRptRange.Dispose();
    }

    private void btnMedCost_Click(object sender, EventArgs e)
    {
      frmCashrptRange objCashRptRange = new frmCashrptRange();

      objCashRptRange.RptName = "OPD_001_10";
      objCashRptRange.ShowDialog();

      objCashRptRange.Dispose();
    }

    private void dgOPDMed_CellEnter(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex != 5 && e.ColumnIndex != 6)
      {
        dgOPDMed.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.BurlyWood;
      }
    }

    private void nudDiscount_ValueChanged(object sender, EventArgs e)
    {
      decimal Discount = 0;

      Discount = (nudDiscount.Value / 100) * Convert.ToDecimal(txtGrandTotal.Tag.ToString().Trim());
      txtGrandTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtGrandTotal.Tag.ToString().Trim()) - Discount, 2));
    }

    private void btnNotAvail_Click(object sender, EventArgs e)
    {
      frmCashrptRange objCashRptRange = new frmCashrptRange();

      objCashRptRange.RptName = "OPD_001_11";
      objCashRptRange.ShowDialog();

      objCashRptRange.Dispose();
    }
  }
}