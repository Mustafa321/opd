using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.Devices;
using Microsoft.VisualBasic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace OPD
{
    public partial class frmAbout : Form
    {
        //[DllImport("winmm.dll")]
        //extern static int mciSendString(string command, IntPtr responseBuffer, int bufferLength, int nothing);
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        private static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        public frmAbout()
        {
            InitializeComponent();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
             DateTime dt = DateTime.Now;

            //Under Record Button Click paste the below Code:

            // record from microphone
            mciSendString("open new Type waveaudio Alias recsound", "", 0, 0);
            mciSendString("record recsound", "", 0, 0);

            //while (!DateTime.Now.Equals(dt.AddSeconds(10)));
            //if (!System.IO.File.Exists("Voice\\record.wav"))
            //{
            //    mciSendString("save recsound  Voice\\record.wav", "", 0, 0);
            //    mciSendString("close recsound ", "", 0, 0);
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Under Save / Stop button Click, 
            // stop and save
            mciSendString("save recsound Voice\\record.wav", "", 0, 0);
            mciSendString("close recsound ", "", 0, 0);
            Computer c = new Computer();
            c.Audio.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Computer computer = new Computer();
            computer.Audio.Play("Voice\\record.wav", AudioPlayMode.Background);
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            // GetApi();
            //PostAPI();
        }
        public async Task GetApi()
        {
            try
            {
                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri("http://localhost:8296/");
                //    client.DefaultRequestHeaders.Accept.Clear();
                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //    // New code:
                //    HttpResponseMessage response = await client.GetAsync("api/values");
                //    if (response.IsSuccessStatusCode)
                //    {
                //        var product = await response.Content.ReadAsAsync<string[]>();

                //        MessageBox.Show(product.ToString());
                //    }
                //}
            }
            catch(Exception ex)
            {

            }
            
        }
        public async Task PostAPI()
        {
            //try
            //{
            //    var value = new Product() { Name = "Gizmo", Price = 100, Category = "Widget" };
            //    using (var client = new HttpClient())
            //    {
            //        client.BaseAddress = new Uri("http://localhost:8296/");
            //        client.DefaultRequestHeaders.Accept.Clear();
            //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //        // New code:
            //        HttpResponseMessage response = await client.PostAsJsonAsync("api/values",value);
            //        if (response.IsSuccessStatusCode)
            //        {
            //            var product = await response.Content.ReadAsAsync<string[]>();

            //            MessageBox.Show(product.ToString());
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{

            //}

        }
    }
    class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Category { get; set; }
    }
}