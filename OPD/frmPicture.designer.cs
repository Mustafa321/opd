namespace OPD
{
    partial class frmPicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPicture));
            this.pnlPicCollection = new System.Windows.Forms.Panel();
            this.pbSave = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlPicSingle = new System.Windows.Forms.Panel();
            this.pbLargeView = new System.Windows.Forms.PictureBox();
            this.lblPictureID = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.lblComment = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnAddNewType = new System.Windows.Forms.Button();
            this.cboDocType = new System.Windows.Forms.ComboBox();
            this.lblDocType = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbSave)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlPicSingle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLargeView)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPicCollection
            // 
            this.pnlPicCollection.AutoScroll = true;
            this.pnlPicCollection.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.pnlPicCollection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPicCollection.Location = new System.Drawing.Point(6, 555);
            this.pnlPicCollection.Name = "pnlPicCollection";
            this.pnlPicCollection.Size = new System.Drawing.Size(999, 133);
            this.pnlPicCollection.TabIndex = 0;
            this.toolTip1.SetToolTip(this.pnlPicCollection, "Collection of Images  w.r.t Patient");
            // 
            // pbSave
            // 
            this.pbSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSave.Location = new System.Drawing.Point(905, 428);
            this.pbSave.Name = "pbSave";
            this.pbSave.Size = new System.Drawing.Size(100, 100);
            this.pbSave.TabIndex = 11;
            this.pbSave.TabStop = false;
            this.toolTip1.SetToolTip(this.pbSave, "Preview of new Seleted image");
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Location = new System.Drawing.Point(905, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 423);
            this.panel1.TabIndex = 1;
            this.panel1.Tag = "top";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClear.Location = new System.Drawing.Point(19, 226);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(60, 60);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "&Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnClear, "Clear Selection");
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNew.Location = new System.Drawing.Point(19, 42);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(60, 60);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "&New";
            this.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnNew, "Select New Image for Save");
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.Enabled = false;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.Location = new System.Drawing.Point(19, 318);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 60);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnDelete, "Delete Selected Image");
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSave.Location = new System.Drawing.Point(19, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 60);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnSave, "Save New Image");
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlPicSingle
            // 
            this.pnlPicSingle.AutoScroll = true;
            this.pnlPicSingle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPicSingle.Controls.Add(this.pbLargeView);
            this.pnlPicSingle.Controls.Add(this.lblPictureID);
            this.pnlPicSingle.Location = new System.Drawing.Point(6, 4);
            this.pnlPicSingle.Name = "pnlPicSingle";
            this.pnlPicSingle.Size = new System.Drawing.Size(893, 524);
            this.pnlPicSingle.TabIndex = 0;
            // 
            // pbLargeView
            // 
            this.pbLargeView.Location = new System.Drawing.Point(3, 3);
            this.pbLargeView.Name = "pbLargeView";
            this.pbLargeView.Size = new System.Drawing.Size(885, 515);
            this.pbLargeView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbLargeView.TabIndex = 10;
            this.pbLargeView.TabStop = false;
            this.toolTip1.SetToolTip(this.pbLargeView, "Large Image view");
            this.pbLargeView.Click += new System.EventHandler(this.pbLargeView_Click);
            this.pbLargeView.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // lblPictureID
            // 
            this.lblPictureID.AutoSize = true;
            this.lblPictureID.Location = new System.Drawing.Point(261, 161);
            this.lblPictureID.Name = "lblPictureID";
            this.lblPictureID.Size = new System.Drawing.Size(61, 13);
            this.lblPictureID.TabIndex = 11;
            this.lblPictureID.Text = "lblPictureID";
            this.lblPictureID.Visible = false;
            // 
            // txtComment
            // 
            this.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComment.Location = new System.Drawing.Point(78, 532);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(610, 20);
            this.txtComment.TabIndex = 12;
            this.toolTip1.SetToolTip(this.txtComment, "Comment Related to Image");
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Location = new System.Drawing.Point(12, 535);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(60, 13);
            this.lblComment.TabIndex = 13;
            this.lblComment.Text = "Comment : ";
            // 
            // btnAddNewType
            // 
            this.btnAddNewType.Location = new System.Drawing.Point(959, 530);
            this.btnAddNewType.Name = "btnAddNewType";
            this.btnAddNewType.Size = new System.Drawing.Size(46, 23);
            this.btnAddNewType.TabIndex = 37;
            this.btnAddNewType.Text = "Add";
            this.btnAddNewType.UseVisualStyleBackColor = true;
            this.btnAddNewType.Click += new System.EventHandler(this.btnAddNewType_Click);
            // 
            // cboDocType
            // 
            this.cboDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDocType.FormattingEnabled = true;
            this.cboDocType.Location = new System.Drawing.Point(784, 531);
            this.cboDocType.Name = "cboDocType";
            this.cboDocType.Size = new System.Drawing.Size(176, 21);
            this.cboDocType.TabIndex = 36;
            this.cboDocType.SelectedIndexChanged += new System.EventHandler(this.cboDocType_SelectedIndexChanged);
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.Location = new System.Drawing.Point(694, 535);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(92, 13);
            this.lblDocType.TabIndex = 35;
            this.lblDocType.Text = "Document Type : ";
            // 
            // frmPicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 689);
            this.Controls.Add(this.btnAddNewType);
            this.Controls.Add(this.cboDocType);
            this.Controls.Add(this.lblDocType);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.pbSave);
            this.Controls.Add(this.pnlPicCollection);
            this.Controls.Add(this.pnlPicSingle);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmPicture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Picture";
            this.Load += new System.EventHandler(this.frmPicture_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPicture_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbSave)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnlPicSingle.ResumeLayout(false);
            this.pnlPicSingle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLargeView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlPicCollection;
        private System.Windows.Forms.PictureBox pbSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel pnlPicSingle;
        private System.Windows.Forms.PictureBox pbLargeView;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.Label lblPictureID;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnAddNewType;
        private System.Windows.Forms.ComboBox cboDocType;
        private System.Windows.Forms.Label lblDocType;
    }
}