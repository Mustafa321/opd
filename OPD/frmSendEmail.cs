using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmSendEmail : Form
    {
        public frmSendEmail()
        {
            InitializeComponent();
        }

        private void frmSendEmail_Load(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPerson = new clsBLPersonal(objConnection);
            clsBLReferences objRef = new clsBLReferences(objConnection);
            SComponents objComp = new SComponents();
            DataView dv=new DataView();

            objComp.ApplyStyleToControls(this);
            objConnection.Connection_Open();

            objRef.ReportRefrence="EmailInfo";

            dv=objRef.GetAll(2);
            if (dv.Table.Rows.Count != 0)
            {
                //txtTo.Text= dv.Table.Rows[0]["Description"].ToString();
                //txtFrom.Text = dv.Table.Rows[0]["ReportTitle1"].ToString();
                //txtSubject.Text = dv.Table.Rows[0]["ReportTitle2"].ToString();
                txtDescription.Text = dv.Table.Rows[0]["ReportTitle3"].ToString();
            }

            objPerson.name = clsSharedVariables.UserID;
            objPerson.Address = txtFrom.Tag.ToString();

            dv = objPerson.GetAll(5);
            if (dv.Table.Rows.Count != 0)
            {
                if (!dv.Table.Rows[0]["toMessage"].ToString().Equals(""))
                {
                    txtFrom.Text = dv.Table.Rows[0]["toMessage"].ToString();
                }
                if (!dv.Table.Rows[0]["fromMessage"].ToString().Equals(""))
                {
                    txtTo.Text = dv.Table.Rows[0]["fromMessage"].ToString();
                }
            }

            objConnection.Connection_Close();

            objPerson=null;
            objConnection = null;
            dv.Dispose();
            objRef = null;
            objComp = null;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Validation objValid = new Validation();

            if (!txtTo.Text.Equals(""))
            {
                if (!objValid.IsEmail(txtTo.Text.Trim()))
                {
                    MessageBox.Show("Invalid Send To Email Address ");
                    return;
                }
            }
            else
            {
                MessageBox.Show("To Email Address can not Empty");
                return;
            }
            if (!txtFrom.Text.Equals(""))
            {
                if (!objValid.IsEmail(txtFrom.Text.Trim()))
                {
                    MessageBox.Show("Invalid Send To Email Address ");
                    return ;
                }
            }
            else
            {
                MessageBox.Show("From Email Address can not Empty");
                return;
            }

            SaveEmailInfo();
            txtTo.Tag = "O";

            this.Close();
        }

        private void SaveEmailInfo()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            try
            {
                //objPersonal.PersonID = clsSharedVariables.UserID  ;
                objRef.ReportRefrence = "EmailInfo";

                objRef.Description = txtTo.Text.Trim();
                objRef.ReportTitle1 = txtFrom.Text.Trim();
                objRef.ReportTitle2 = txtSubject.Text.Trim();
                objRef.ReportTitle3  = txtDescription.Text.Trim();
                
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objRef = null;
                objConnection = null;
            }
        }

        private void frmSendEmail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(txtTo.Tag==null)
            {
                txtTo.Tag = "C";
            }
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }
    }
}