namespace OPD
{
  partial class frmAddDiseaseFromList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddDiseaseFromList));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        this.dgAllDisease = new System.Windows.Forms.DataGridView();
        this.AllDiseaseID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DiseaseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.AllDisaesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.AllHist = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.AllComplaints = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.AllSigns = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.AllDiagnosis = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.cmsSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
        this.miMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
        this.dgSelectDisease = new System.Windows.Forms.DataGridView();
        this.SelPrefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SelDiseaseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SelPrefName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SelType = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DiseaseRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SelMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.btnLink = new System.Windows.Forms.Button();
        this.txtSelectDiseaseSearch = new System.Windows.Forms.TextBox();
        this.panel2 = new System.Windows.Forms.Panel();
        this.lblMainHeading = new System.Windows.Forms.Label();
        this.tsMain = new System.Windows.Forms.ToolStrip();
        this.miSave = new System.Windows.Forms.ToolStripButton();
        this.miRefresh = new System.Windows.Forms.ToolStripButton();
        this.miExit = new System.Windows.Forms.ToolStripButton();
        this.txtAllDiseaseSerach = new System.Windows.Forms.TextBox();
        this.lblAllDisease = new System.Windows.Forms.Label();
        this.lblNewDisease = new System.Windows.Forms.Label();
        this.lblChangeDisease = new System.Windows.Forms.Label();
        this.lblSelectedDisease = new System.Windows.Forms.Label();
        this.btnSelection = new System.Windows.Forms.Button();
        this.panel1 = new System.Windows.Forms.Panel();
        this.dgLinkDisease = new System.Windows.Forms.DataGridView();
        this.LinkPrefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.LinkPrefName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.LinkType = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.LinkDiseaseRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.LinkMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.linkDiseaseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.lblLinkGridHeading = new System.Windows.Forms.Label();
        this.panel3 = new System.Windows.Forms.Panel();
        this.cboBlock = new System.Windows.Forms.ComboBox();
        this.cboChap = new System.Windows.Forms.ComboBox();
        this.lblBlock = new System.Windows.Forms.Label();
        this.lblChap = new System.Windows.Forms.Label();
        ((System.ComponentModel.ISupportInitialize)(this.dgAllDisease)).BeginInit();
        this.cmsSelection.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgSelectDisease)).BeginInit();
        this.panel2.SuspendLayout();
        this.tsMain.SuspendLayout();
        this.panel1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgLinkDisease)).BeginInit();
        this.panel3.SuspendLayout();
        this.SuspendLayout();
        // 
        // dgAllDisease
        // 
        this.dgAllDisease.AllowUserToAddRows = false;
        this.dgAllDisease.AllowUserToDeleteRows = false;
        this.dgAllDisease.AllowUserToOrderColumns = true;
        this.dgAllDisease.AllowUserToResizeColumns = false;
        this.dgAllDisease.AllowUserToResizeRows = false;
        this.dgAllDisease.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
        this.dgAllDisease.CausesValidation = false;
        this.dgAllDisease.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
        this.dgAllDisease.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgAllDisease.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        this.dgAllDisease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dgAllDisease.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AllDiseaseID,
            this.DiseaseCode,
            this.AllDisaesName,
            this.AllHist,
            this.AllComplaints,
            this.AllSigns,
            this.AllDiagnosis});
        this.dgAllDisease.ContextMenuStrip = this.cmsSelection;
        this.dgAllDisease.Location = new System.Drawing.Point(2, 166);
        this.dgAllDisease.MultiSelect = false;
        this.dgAllDisease.Name = "dgAllDisease";
        this.dgAllDisease.RowHeadersVisible = false;
        this.dgAllDisease.RowHeadersWidth = 25;
        this.dgAllDisease.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgAllDisease.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.dgAllDisease.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgAllDisease.ShowCellErrors = false;
        this.dgAllDisease.ShowCellToolTips = false;
        this.dgAllDisease.ShowEditingIcon = false;
        this.dgAllDisease.ShowRowErrors = false;
        this.dgAllDisease.Size = new System.Drawing.Size(480, 536);
        this.dgAllDisease.TabIndex = 64;
        this.dgAllDisease.Tag = "no";
        this.dgAllDisease.VirtualMode = true;
        this.dgAllDisease.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgAllDisease_MouseDown);
        this.dgAllDisease.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAllDisease_RowEnter);
        this.dgAllDisease.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAllDisease_CellDoubleClick);
        this.dgAllDisease.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAllDisease_CellClick);
        // 
        // AllDiseaseID
        // 
        this.AllDiseaseID.DataPropertyName = "DiseaseID";
        this.AllDiseaseID.HeaderText = "DiseaseID";
        this.AllDiseaseID.Name = "AllDiseaseID";
        this.AllDiseaseID.Visible = false;
        this.AllDiseaseID.Width = 79;
        // 
        // DiseaseCode
        // 
        this.DiseaseCode.DataPropertyName = "DISEASECODE";
        this.DiseaseCode.HeaderText = "Code";
        this.DiseaseCode.Name = "DiseaseCode";
        this.DiseaseCode.Width = 40;
        // 
        // AllDisaesName
        // 
        this.AllDisaesName.DataPropertyName = "DISEASENAME";
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.AllDisaesName.DefaultCellStyle = dataGridViewCellStyle2;
        this.AllDisaesName.HeaderText = "Name";
        this.AllDisaesName.Name = "AllDisaesName";
        this.AllDisaesName.ReadOnly = true;
        this.AllDisaesName.Width = 195;
        // 
        // AllHist
        // 
        this.AllHist.DataPropertyName = "History";
        this.AllHist.FalseValue = "0";
        this.AllHist.HeaderText = "History";
        this.AllHist.Name = "AllHist";
        this.AllHist.TrueValue = "1";
        this.AllHist.Width = 50;
        // 
        // AllComplaints
        // 
        this.AllComplaints.DataPropertyName = "Complaints";
        this.AllComplaints.FalseValue = "0";
        this.AllComplaints.HeaderText = "Complaints";
        this.AllComplaints.Name = "AllComplaints";
        this.AllComplaints.TrueValue = "1";
        this.AllComplaints.Width = 70;
        // 
        // AllSigns
        // 
        this.AllSigns.DataPropertyName = "Signs";
        this.AllSigns.FalseValue = "0";
        this.AllSigns.HeaderText = "Signs";
        this.AllSigns.Name = "AllSigns";
        this.AllSigns.TrueValue = "1";
        this.AllSigns.Width = 45;
        // 
        // AllDiagnosis
        // 
        this.AllDiagnosis.DataPropertyName = "Diagnosis";
        this.AllDiagnosis.FalseValue = "0";
        this.AllDiagnosis.HeaderText = "Diagnosis";
        this.AllDiagnosis.Name = "AllDiagnosis";
        this.AllDiagnosis.TrueValue = "1";
        this.AllDiagnosis.Width = 65;
        // 
        // cmsSelection
        // 
        this.cmsSelection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miMoreInfo});
        this.cmsSelection.Name = "cmsSelection";
        this.cmsSelection.Size = new System.Drawing.Size(169, 48);
        // 
        // miAdd
        // 
        this.miAdd.Name = "miAdd";
        this.miAdd.Size = new System.Drawing.Size(168, 22);
        this.miAdd.Text = "Add";
        this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
        // 
        // miMoreInfo
        // 
        this.miMoreInfo.Name = "miMoreInfo";
        this.miMoreInfo.Size = new System.Drawing.Size(168, 22);
        this.miMoreInfo.Text = "More Information";
        // 
        // dgSelectDisease
        // 
        this.dgSelectDisease.AllowDrop = true;
        this.dgSelectDisease.AllowUserToAddRows = false;
        this.dgSelectDisease.AllowUserToDeleteRows = false;
        this.dgSelectDisease.AllowUserToOrderColumns = true;
        this.dgSelectDisease.AllowUserToResizeColumns = false;
        this.dgSelectDisease.AllowUserToResizeRows = false;
        this.dgSelectDisease.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgSelectDisease.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
        this.dgSelectDisease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgSelectDisease.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelPrefID,
            this.SelDiseaseCode,
            this.SelPrefName,
            this.SelType,
            this.DiseaseRefID,
            this.SelMode});
        this.dgSelectDisease.Location = new System.Drawing.Point(533, 166);
        this.dgSelectDisease.MultiSelect = false;
        this.dgSelectDisease.Name = "dgSelectDisease";
        this.dgSelectDisease.ReadOnly = true;
        this.dgSelectDisease.RowHeadersVisible = false;
        this.dgSelectDisease.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgSelectDisease.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgSelectDisease.ShowEditingIcon = false;
        this.dgSelectDisease.ShowRowErrors = false;
        this.dgSelectDisease.Size = new System.Drawing.Size(480, 409);
        this.dgSelectDisease.TabIndex = 74;
        this.dgSelectDisease.VirtualMode = true;
        this.dgSelectDisease.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSelectDisease_CellClick);
        // 
        // SelPrefID
        // 
        this.SelPrefID.DataPropertyName = "PrefID";
        this.SelPrefID.HeaderText = "SelPrefID";
        this.SelPrefID.Name = "SelPrefID";
        this.SelPrefID.ReadOnly = true;
        this.SelPrefID.Visible = false;
        // 
        // SelDiseaseCode
        // 
        this.SelDiseaseCode.DataPropertyName = "diseasecode";
        this.SelDiseaseCode.HeaderText = "Code";
        this.SelDiseaseCode.Name = "SelDiseaseCode";
        this.SelDiseaseCode.ReadOnly = true;
        this.SelDiseaseCode.Width = 50;
        // 
        // SelPrefName
        // 
        this.SelPrefName.DataPropertyName = "PrefName";
        dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.SelPrefName.DefaultCellStyle = dataGridViewCellStyle4;
        this.SelPrefName.HeaderText = "Name";
        this.SelPrefName.Name = "SelPrefName";
        this.SelPrefName.ReadOnly = true;
        this.SelPrefName.Width = 270;
        // 
        // SelType
        // 
        this.SelType.DataPropertyName = "Typename";
        this.SelType.HeaderText = "Type";
        this.SelType.Name = "SelType";
        this.SelType.ReadOnly = true;
        this.SelType.Width = 140;
        // 
        // DiseaseRefID
        // 
        this.DiseaseRefID.DataPropertyName = "diseaseIDRef";
        this.DiseaseRefID.HeaderText = "DiseaseRefID";
        this.DiseaseRefID.Name = "DiseaseRefID";
        this.DiseaseRefID.ReadOnly = true;
        this.DiseaseRefID.Visible = false;
        // 
        // SelMode
        // 
        this.SelMode.DataPropertyName = "Mode";
        this.SelMode.HeaderText = "Mode";
        this.SelMode.Name = "SelMode";
        this.SelMode.ReadOnly = true;
        this.SelMode.Visible = false;
        // 
        // btnLink
        // 
        this.btnLink.Image = ((System.Drawing.Image)(resources.GetObject("btnLink.Image")));
        this.btnLink.Location = new System.Drawing.Point(486, 427);
        this.btnLink.Name = "btnLink";
        this.btnLink.Size = new System.Drawing.Size(45, 58);
        this.btnLink.TabIndex = 73;
        this.btnLink.UseVisualStyleBackColor = true;
        this.btnLink.Click += new System.EventHandler(this.btnLink_Click);
        // 
        // txtSelectDiseaseSearch
        // 
        this.txtSelectDiseaseSearch.Location = new System.Drawing.Point(533, 143);
        this.txtSelectDiseaseSearch.Name = "txtSelectDiseaseSearch";
        this.txtSelectDiseaseSearch.Size = new System.Drawing.Size(480, 20);
        this.txtSelectDiseaseSearch.TabIndex = 72;
        this.txtSelectDiseaseSearch.Text = "Click here to search Preferential Setting ......";
        this.txtSelectDiseaseSearch.TextChanged += new System.EventHandler(this.txtSelectDiseaseSearch_TextChanged);
        this.txtSelectDiseaseSearch.Click += new System.EventHandler(this.txt_Click);
        this.txtSelectDiseaseSearch.Leave += new System.EventHandler(this.txt_Leave);
        // 
        // panel2
        // 
        this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel2.Controls.Add(this.lblMainHeading);
        this.panel2.Controls.Add(this.tsMain);
        this.panel2.Location = new System.Drawing.Point(2, 2);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(1011, 53);
        this.panel2.TabIndex = 69;
        this.panel2.Tag = "top";
        // 
        // lblMainHeading
        // 
        this.lblMainHeading.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblMainHeading.Location = new System.Drawing.Point(48, -4);
        this.lblMainHeading.Name = "lblMainHeading";
        this.lblMainHeading.Size = new System.Drawing.Size(722, 58);
        this.lblMainHeading.TabIndex = 45;
        this.lblMainHeading.Tag = "no";
        this.lblMainHeading.Text = "Configuration with ICD - 10 ( International Classification of Diseases ) ";
        this.lblMainHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        // 
        // tsMain
        // 
        this.tsMain.AutoSize = false;
        this.tsMain.BackColor = System.Drawing.Color.Transparent;
        this.tsMain.CanOverflow = false;
        this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
        this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
        this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
        this.tsMain.Location = new System.Drawing.Point(813, -8);
        this.tsMain.Name = "tsMain";
        this.tsMain.Size = new System.Drawing.Size(200, 66);
        this.tsMain.TabIndex = 44;
        this.tsMain.Text = "toolStrip1";
        // 
        // miSave
        // 
        this.miSave.AutoSize = false;
        this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
        this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.miSave.ForeColor = System.Drawing.Color.Black;
        this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
        this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        this.miSave.Name = "miSave";
        this.miSave.Size = new System.Drawing.Size(65, 52);
        this.miSave.Tag = "Save";
        this.miSave.Text = "&Save";
        this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
        this.miSave.ToolTipText = "Save Instructions";
        this.miSave.Click += new System.EventHandler(this.miSave_Click);
        // 
        // miRefresh
        // 
        this.miRefresh.AutoSize = false;
        this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
        this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.miRefresh.ForeColor = System.Drawing.Color.Black;
        this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
        this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        this.miRefresh.Name = "miRefresh";
        this.miRefresh.Size = new System.Drawing.Size(65, 52);
        this.miRefresh.Text = "&Refresh";
        this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
        this.miRefresh.ToolTipText = "Refresh All Fields";
        this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
        // 
        // miExit
        // 
        this.miExit.AutoSize = false;
        this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
        this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.miExit.ForeColor = System.Drawing.Color.Black;
        this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
        this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        this.miExit.Name = "miExit";
        this.miExit.Size = new System.Drawing.Size(65, 52);
        this.miExit.Text = "&Exit";
        this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
        this.miExit.ToolTipText = "Exit ";
        this.miExit.Click += new System.EventHandler(this.miExit_Click);
        // 
        // txtAllDiseaseSerach
        // 
        this.txtAllDiseaseSerach.Location = new System.Drawing.Point(2, 143);
        this.txtAllDiseaseSerach.Name = "txtAllDiseaseSerach";
        this.txtAllDiseaseSerach.Size = new System.Drawing.Size(480, 20);
        this.txtAllDiseaseSerach.TabIndex = 68;
        this.txtAllDiseaseSerach.Text = "Click here to search ICD-10......";
        this.txtAllDiseaseSerach.TextChanged += new System.EventHandler(this.txtAllDiseaseSerach_TextChanged);
        this.txtAllDiseaseSerach.Click += new System.EventHandler(this.txt_Click);
        this.txtAllDiseaseSerach.Leave += new System.EventHandler(this.txt_Leave);
        // 
        // lblAllDisease
        // 
        this.lblAllDisease.AutoSize = true;
        this.lblAllDisease.Location = new System.Drawing.Point(1, 127);
        this.lblAllDisease.Name = "lblAllDisease";
        this.lblAllDisease.Size = new System.Drawing.Size(46, 13);
        this.lblAllDisease.TabIndex = 66;
        this.lblAllDisease.Tag = "display";
        this.lblAllDisease.Text = "ICD-10 :";
        // 
        // lblNewDisease
        // 
        this.lblNewDisease.BackColor = System.Drawing.Color.BurlyWood;
        this.lblNewDisease.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.lblNewDisease.Location = new System.Drawing.Point(823, 118);
        this.lblNewDisease.Name = "lblNewDisease";
        this.lblNewDisease.Size = new System.Drawing.Size(92, 23);
        this.lblNewDisease.TabIndex = 70;
        this.lblNewDisease.Tag = "no";
        this.lblNewDisease.Text = "New Disease";
        this.lblNewDisease.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        // 
        // lblChangeDisease
        // 
        this.lblChangeDisease.BackColor = System.Drawing.Color.Cornsilk;
        this.lblChangeDisease.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.lblChangeDisease.Location = new System.Drawing.Point(921, 118);
        this.lblChangeDisease.Name = "lblChangeDisease";
        this.lblChangeDisease.Size = new System.Drawing.Size(92, 23);
        this.lblChangeDisease.TabIndex = 71;
        this.lblChangeDisease.Tag = "no";
        this.lblChangeDisease.Text = "Link Disease";
        this.lblChangeDisease.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        // 
        // lblSelectedDisease
        // 
        this.lblSelectedDisease.AutoSize = true;
        this.lblSelectedDisease.Location = new System.Drawing.Point(530, 127);
        this.lblSelectedDisease.Name = "lblSelectedDisease";
        this.lblSelectedDisease.Size = new System.Drawing.Size(105, 13);
        this.lblSelectedDisease.TabIndex = 67;
        this.lblSelectedDisease.Tag = "display";
        this.lblSelectedDisease.Text = "Preferential Setting : ";
        // 
        // btnSelection
        // 
        this.btnSelection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSelection.BackgroundImage")));
        this.btnSelection.Location = new System.Drawing.Point(486, 342);
        this.btnSelection.Name = "btnSelection";
        this.btnSelection.Size = new System.Drawing.Size(45, 79);
        this.btnSelection.TabIndex = 65;
        this.btnSelection.UseVisualStyleBackColor = true;
        this.btnSelection.Click += new System.EventHandler(this.btnSelection_Click);
        // 
        // panel1
        // 
        this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel1.Controls.Add(this.dgLinkDisease);
        this.panel1.Location = new System.Drawing.Point(533, 596);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(480, 106);
        this.panel1.TabIndex = 75;
        // 
        // dgLinkDisease
        // 
        this.dgLinkDisease.AllowDrop = true;
        this.dgLinkDisease.AllowUserToAddRows = false;
        this.dgLinkDisease.AllowUserToDeleteRows = false;
        this.dgLinkDisease.AllowUserToOrderColumns = true;
        this.dgLinkDisease.AllowUserToResizeColumns = false;
        this.dgLinkDisease.AllowUserToResizeRows = false;
        this.dgLinkDisease.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgLinkDisease.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
        this.dgLinkDisease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgLinkDisease.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LinkPrefID,
            this.LinkPrefName,
            this.LinkType,
            this.LinkDiseaseRefID,
            this.LinkMode,
            this.linkDiseaseCode});
        this.dgLinkDisease.Location = new System.Drawing.Point(-1, 1);
        this.dgLinkDisease.MultiSelect = false;
        this.dgLinkDisease.Name = "dgLinkDisease";
        this.dgLinkDisease.ReadOnly = true;
        this.dgLinkDisease.RowHeadersVisible = false;
        this.dgLinkDisease.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgLinkDisease.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgLinkDisease.Size = new System.Drawing.Size(480, 102);
        this.dgLinkDisease.TabIndex = 75;
        // 
        // LinkPrefID
        // 
        this.LinkPrefID.DataPropertyName = "PrefID";
        this.LinkPrefID.HeaderText = "LinkPrefID";
        this.LinkPrefID.Name = "LinkPrefID";
        this.LinkPrefID.ReadOnly = true;
        this.LinkPrefID.Visible = false;
        // 
        // LinkPrefName
        // 
        this.LinkPrefName.DataPropertyName = "PrefName";
        dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.LinkPrefName.DefaultCellStyle = dataGridViewCellStyle6;
        this.LinkPrefName.HeaderText = "Name";
        this.LinkPrefName.Name = "LinkPrefName";
        this.LinkPrefName.ReadOnly = true;
        this.LinkPrefName.Width = 320;
        // 
        // LinkType
        // 
        this.LinkType.DataPropertyName = "Typename";
        this.LinkType.HeaderText = "Type";
        this.LinkType.Name = "LinkType";
        this.LinkType.ReadOnly = true;
        this.LinkType.Width = 140;
        // 
        // LinkDiseaseRefID
        // 
        this.LinkDiseaseRefID.DataPropertyName = "diseaseIDRef";
        this.LinkDiseaseRefID.HeaderText = "DiseaseRefID";
        this.LinkDiseaseRefID.Name = "LinkDiseaseRefID";
        this.LinkDiseaseRefID.ReadOnly = true;
        this.LinkDiseaseRefID.Visible = false;
        // 
        // LinkMode
        // 
        this.LinkMode.DataPropertyName = "Mode";
        this.LinkMode.HeaderText = "Mode";
        this.LinkMode.Name = "LinkMode";
        this.LinkMode.ReadOnly = true;
        this.LinkMode.Visible = false;
        // 
        // linkDiseaseCode
        // 
        this.linkDiseaseCode.DataPropertyName = "DiseaseCode";
        this.linkDiseaseCode.HeaderText = "Column1";
        this.linkDiseaseCode.Name = "linkDiseaseCode";
        this.linkDiseaseCode.ReadOnly = true;
        this.linkDiseaseCode.Visible = false;
        // 
        // lblLinkGridHeading
        // 
        this.lblLinkGridHeading.AutoSize = true;
        this.lblLinkGridHeading.Location = new System.Drawing.Point(530, 579);
        this.lblLinkGridHeading.Name = "lblLinkGridHeading";
        this.lblLinkGridHeading.Size = new System.Drawing.Size(149, 13);
        this.lblLinkGridHeading.TabIndex = 76;
        this.lblLinkGridHeading.Tag = "display";
        this.lblLinkGridHeading.Text = "Disease w.r.t Selected ICD-10";
        // 
        // panel3
        // 
        this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel3.Controls.Add(this.cboBlock);
        this.panel3.Controls.Add(this.cboChap);
        this.panel3.Controls.Add(this.lblBlock);
        this.panel3.Controls.Add(this.lblChap);
        this.panel3.Location = new System.Drawing.Point(2, 57);
        this.panel3.Name = "panel3";
        this.panel3.Size = new System.Drawing.Size(1011, 58);
        this.panel3.TabIndex = 77;
        // 
        // cboBlock
        // 
        this.cboBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
        this.cboBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
        this.cboBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.cboBlock.FormattingEnabled = true;
        this.cboBlock.Location = new System.Drawing.Point(164, 30);
        this.cboBlock.Name = "cboBlock";
        this.cboBlock.Size = new System.Drawing.Size(736, 21);
        this.cboBlock.TabIndex = 3;
        this.cboBlock.SelectedIndexChanged += new System.EventHandler(this.cboBlock_SelectedIndexChanged);
        // 
        // cboChap
        // 
        this.cboChap.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
        this.cboChap.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
        this.cboChap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.cboChap.FormattingEnabled = true;
        this.cboChap.Location = new System.Drawing.Point(164, 6);
        this.cboChap.Name = "cboChap";
        this.cboChap.Size = new System.Drawing.Size(736, 21);
        this.cboChap.TabIndex = 2;
        this.cboChap.SelectedIndexChanged += new System.EventHandler(this.cboChap_SelectedIndexChanged);
        // 
        // lblBlock
        // 
        this.lblBlock.AutoSize = true;
        this.lblBlock.Location = new System.Drawing.Point(108, 33);
        this.lblBlock.Name = "lblBlock";
        this.lblBlock.Size = new System.Drawing.Size(43, 13);
        this.lblBlock.TabIndex = 1;
        this.lblBlock.Text = "Block : ";
        // 
        // lblChap
        // 
        this.lblChap.AutoSize = true;
        this.lblChap.Location = new System.Drawing.Point(108, 9);
        this.lblChap.Name = "lblChap";
        this.lblChap.Size = new System.Drawing.Size(53, 13);
        this.lblChap.TabIndex = 0;
        this.lblChap.Text = "Chapter : ";
        // 
        // frmAddDiseaseFromList
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(1015, 704);
        this.Controls.Add(this.panel3);
        this.Controls.Add(this.lblLinkGridHeading);
        this.Controls.Add(this.panel1);
        this.Controls.Add(this.dgAllDisease);
        this.Controls.Add(this.btnLink);
        this.Controls.Add(this.txtSelectDiseaseSearch);
        this.Controls.Add(this.panel2);
        this.Controls.Add(this.txtAllDiseaseSerach);
        this.Controls.Add(this.lblAllDisease);
        this.Controls.Add(this.lblNewDisease);
        this.Controls.Add(this.lblChangeDisease);
        this.Controls.Add(this.lblSelectedDisease);
        this.Controls.Add(this.btnSelection);
        this.Controls.Add(this.dgSelectDisease);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        this.MaximizeBox = false;
        this.Name = "frmAddDiseaseFromList";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Configuration with ICD-10 ( International Classification of Diseases ) ";
        this.Load += new System.EventHandler(this.frmAddDiseaseFromList_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDiseaseInfo_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dgAllDisease)).EndInit();
        this.cmsSelection.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dgSelectDisease)).EndInit();
        this.panel2.ResumeLayout(false);
        this.tsMain.ResumeLayout(false);
        this.tsMain.PerformLayout();
        this.panel1.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dgLinkDisease)).EndInit();
        this.panel3.ResumeLayout(false);
        this.panel3.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgAllDisease;
    private System.Windows.Forms.ContextMenuStrip cmsSelection;
    private System.Windows.Forms.ToolStripMenuItem miAdd;
    private System.Windows.Forms.ToolStripMenuItem miMoreInfo;
    private System.Windows.Forms.DataGridView dgSelectDisease;
    private System.Windows.Forms.Button btnLink;
    private System.Windows.Forms.TextBox txtSelectDiseaseSearch;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ToolStrip tsMain;
    private System.Windows.Forms.ToolStripButton miSave;
    private System.Windows.Forms.ToolStripButton miRefresh;
    private System.Windows.Forms.ToolStripButton miExit;
    private System.Windows.Forms.TextBox txtAllDiseaseSerach;
    private System.Windows.Forms.Label lblAllDisease;
    private System.Windows.Forms.Label lblNewDisease;
    private System.Windows.Forms.Label lblChangeDisease;
    private System.Windows.Forms.Label lblSelectedDisease;
    private System.Windows.Forms.Button btnSelection;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.DataGridView dgLinkDisease;
    private System.Windows.Forms.Label lblLinkGridHeading;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Label lblBlock;
    private System.Windows.Forms.Label lblChap;
    private System.Windows.Forms.ComboBox cboBlock;
    private System.Windows.Forms.ComboBox cboChap;
    private System.Windows.Forms.Label lblMainHeading;
    private System.Windows.Forms.DataGridViewTextBoxColumn LinkPrefID;
    private System.Windows.Forms.DataGridViewTextBoxColumn LinkPrefName;
    private System.Windows.Forms.DataGridViewTextBoxColumn LinkType;
    private System.Windows.Forms.DataGridViewTextBoxColumn LinkDiseaseRefID;
    private System.Windows.Forms.DataGridViewTextBoxColumn LinkMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn linkDiseaseCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn AllDiseaseID;
    private System.Windows.Forms.DataGridViewTextBoxColumn DiseaseCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn AllDisaesName;
    private System.Windows.Forms.DataGridViewCheckBoxColumn AllHist;
    private System.Windows.Forms.DataGridViewCheckBoxColumn AllComplaints;
    private System.Windows.Forms.DataGridViewCheckBoxColumn AllSigns;
    private System.Windows.Forms.DataGridViewCheckBoxColumn AllDiagnosis;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelPrefID;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelDiseaseCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelPrefName;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelType;
    private System.Windows.Forms.DataGridViewTextBoxColumn DiseaseRefID;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelMode;
  }
}