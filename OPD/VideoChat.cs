﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OPD
{
    public partial class VideoChat : Form
    {
        int panelWidth;
        bool Hidden;
        public VideoChat()
        {
            InitializeComponent();
            panelWidth = PanelSlide.Width;
            Hidden = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Hidden)
            {

                PanelSlide.Width = PanelSlide.Width + 10;
                if (PanelSlide.Width >= panelWidth)
                {
                    timer1.Stop();
                    Hidden = false;
                    this.Refresh();

                }

            }
            else {

                PanelSlide.Width = PanelSlide.Width - 10;
                if (PanelSlide.Width <= 0)
                {
                    timer1.Stop();
                    Hidden = true;
                    this.Refresh();
                }
            
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uCcontacts1_Load(object sender, EventArgs e)
        {

        }

        private void uCcontacts1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
