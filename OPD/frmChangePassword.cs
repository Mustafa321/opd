using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmChangePassword : Form
    {
        public frmChangePassword()
        {
            InitializeComponent();
        }

        private void frmChangePassword_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);
            this.Text  +=  "( " + clsSharedVariables.UserName + ") ";
            txtLoginID.Text = clsSharedVariables.LoginID;

            objComp = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
            DataView dvPersonal;

            objPersonal.PersonID = clsSharedVariables.UserID;
            objPersonal.LoginID = txtLoginID.Text.Trim();

            objConnection.Connection_Open();
            dvPersonal = objPersonal.GetAll(3);

            if (txtOPassword.Text.ToString().Equals(dvPersonal.Table.Rows[0]["Pasword"].ToString()))
            {
                SaveNewPassword(objPersonal,objConnection);
                objConnection.Connection_Close();
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            objConnection = null;
            objPersonal = null;
            dvPersonal = null;
        }

        private void SaveNewPassword(clsBLPersonal objPersonal, clsBLDBConnection objConnection)
        {
            try
            {
                objPersonal.PersonID = clsSharedVariables.UserID;

                objPersonal.Pasword = txtNPassword.Text;
                objPersonal.RCPasword = txtCPassword.Text;
                objConnection.Transaction_Begin();
                if (objPersonal.Update())
                {
                    objConnection.Transaction_ComRoll();
                    MessageBox.Show("Successfully Password Change ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPersonal.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}