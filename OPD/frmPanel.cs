using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmPanel : Form
    {
        private frmOPD MainOPD = null;

        public frmPanel()
        {
            InitializeComponent();
        }

        public frmPanel(frmOPD Frm)
        {
            InitializeComponent();
            this.MainOPD = Frm;
        }

        private void frmPanel_Load(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);
            
            objConnection.Connection_Open();
            FillGridView(objConnection);
            objConnection.Connection_Close();

            dgPanel.ClearSelection();
            this.Text = this.Text + "(" + clsSharedVariables.InItCaps(clsSharedVariables.UserName) + ")";

            objComp = null;
            objConnection=null;
        }

        private clsBLPanel SetBLValues(clsBLPanel objPanel)
        {
            objPanel.PanelName =txtName.Text.Trim();
            objPanel.DefaultFee = txtFees.Text.Trim();
            if (chbActive.Checked)
            {
                objPanel.Active = "1";
            }
            else
            {
                objPanel.Active = "0";
            }
            objPanel.EnteredBy = clsSharedVariables.UserID; ;
            objPanel.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy hh:mm tt");
            objPanel.ClientID = clsSharedVariables.ClientID; ;

            return objPanel;
        }

        private void Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPanel objPanel = new clsBLPanel(objConnection);

            try
            {
                objPanel = SetBLValues(objPanel);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objPanel.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Panel Add Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPanel.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPanel = null;
                objConnection = null;
            }
        }

        private void UpdateData()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPanel objPanel = new clsBLPanel(objConnection);
            try
            {
                objPanel.PanelID = this.lblPanelID.Text;
                objPanel = SetBLValues(objPanel);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objPanel.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Panel updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPanel.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPanel = null;
                objConnection = null;
            }
        }

        private void FillGridView(clsBLDBConnection objConnection)
        {
            clsBLPanel objPanel = new clsBLPanel(objConnection);

            DataView dvPanel= objPanel.GetAll(1);
            //dvType.Sort = "Name";
            this.dgPanel.DataSource = dvPanel;
            lblTotalRec.Text = "Total Record : " + dgPanel.Rows.Count;

            GenrateSerialNo();

            objPanel = null;
            dvPanel = null;
        }

        private void ClearField()
        {
            txtName.Text = "";
            txtFees.Text = "";
            chbActive.Checked = true;
            dgPanel.ClearSelection();
            txtName.Focus();
            this.lblPanelID.Text = "";
            this.miSave.Tag = "Save";
            this.miSave.Text = "Save";
        }

        private void GenrateSerialNo()
        {
            for (int i = 1; i <= dgPanel.Rows.Count; i++)
            {
                dgPanel.Rows[i - 1].Cells["SerialNo"].Value = i.ToString();
            }
        }

        private void FillForm(int rowIndex)
        {
            this.miSave.Tag = "Update";
            this.miSave.Text = "Update";

            lblPanelID.Text = dgPanel.Rows[rowIndex].Cells["PanelID"].Value.ToString();
            txtName.Text = dgPanel.Rows[rowIndex].Cells["PanelName"].Value.ToString();
            txtFees.Text = dgPanel.Rows[rowIndex].Cells["Fees"].Value.ToString();
            if (dgPanel.Rows[rowIndex].Cells["Active"].Value.ToString().Equals("1"))
            {
                chbActive.Checked = true;
            }
            else
            {
                chbActive.Checked = false;
            }
        }

        private void dgPanel_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                FillForm(e.RowIndex);
            }
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if (this.miSave.Tag.Equals("Save"))
            {
                Insert();
            }
            else
            {
                UpdateData();
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            txtName .Text = clsSharedVariables.InItCaps(txtName.Text);
        }

        private void dgPanel_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenrateSerialNo();
        }

        private void frmPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }

        private void txtFees_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = "\b0123456789.";
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }
    }
}