namespace OPD
{
    partial class frmPatientAlergAdverse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPatientAlergAdverse));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtPName = new System.Windows.Forms.TextBox();
            this.txtPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.lblHName = new System.Windows.Forms.Label();
            this.lblHPRNo = new System.Windows.Forms.Label();
            this.lblHAge = new System.Windows.Forms.Label();
            this.lblHGender = new System.Windows.Forms.Label();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAllergARHeading = new System.Windows.Forms.Label();
            this.dgSelect = new System.Windows.Forms.DataGridView();
            this.PatientAllergAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlergAdversID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Medicineid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlergARName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsSelected = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSelectedDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.lblMedicineHeading = new System.Windows.Forms.Label();
            this.cmbMed = new System.Windows.Forms.ComboBox();
            this.cmbAllergAR = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSelect)).BeginInit();
            this.cmsSelected.SuspendLayout();
            this.SuspendLayout();
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtGender);
            this.panel3.Controls.Add(this.txtPName);
            this.panel3.Controls.Add(this.txtPRNo);
            this.panel3.Controls.Add(this.txtAge);
            this.panel3.Controls.Add(this.lblHName);
            this.panel3.Controls.Add(this.lblHPRNo);
            this.panel3.Controls.Add(this.lblHAge);
            this.panel3.Controls.Add(this.lblHGender);
            this.panel3.Location = new System.Drawing.Point(12, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(770, 47);
            this.panel3.TabIndex = 91;
            // 
            // txtGender
            // 
            this.txtGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGender.Location = new System.Drawing.Point(513, 12);
            this.txtGender.MaxLength = 3;
            this.txtGender.Name = "txtGender";
            this.txtGender.ReadOnly = true;
            this.txtGender.Size = new System.Drawing.Size(61, 20);
            this.txtGender.TabIndex = 190;
            // 
            // txtPName
            // 
            this.txtPName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPName.Location = new System.Drawing.Point(296, 12);
            this.txtPName.MaxLength = 50;
            this.txtPName.Name = "txtPName";
            this.txtPName.ReadOnly = true;
            this.txtPName.Size = new System.Drawing.Size(145, 20);
            this.txtPName.TabIndex = 179;
            // 
            // txtPRNo
            // 
            this.txtPRNo.BackColor = System.Drawing.Color.White;
            this.txtPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRNo.Location = new System.Drawing.Point(141, 12);
            this.txtPRNo.Name = "txtPRNo";
            this.txtPRNo.ReadOnly = true;
            this.txtPRNo.Size = new System.Drawing.Size(73, 20);
            this.txtPRNo.TabIndex = 178;
            this.txtPRNo.TabStop = false;
            // 
            // txtAge
            // 
            this.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAge.Location = new System.Drawing.Point(630, 12);
            this.txtAge.MaxLength = 3;
            this.txtAge.Name = "txtAge";
            this.txtAge.ReadOnly = true;
            this.txtAge.Size = new System.Drawing.Size(38, 20);
            this.txtAge.TabIndex = 182;
            // 
            // lblHName
            // 
            this.lblHName.AutoSize = true;
            this.lblHName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHName.Location = new System.Drawing.Point(249, 14);
            this.lblHName.Name = "lblHName";
            this.lblHName.Size = new System.Drawing.Size(41, 13);
            this.lblHName.TabIndex = 186;
            this.lblHName.Text = "Name :";
            // 
            // lblHPRNo
            // 
            this.lblHPRNo.AutoSize = true;
            this.lblHPRNo.Location = new System.Drawing.Point(62, 14);
            this.lblHPRNo.Name = "lblHPRNo";
            this.lblHPRNo.Size = new System.Drawing.Size(53, 13);
            this.lblHPRNo.TabIndex = 189;
            this.lblHPRNo.Tag = "";
            this.lblHPRNo.Text = "MR No. : ";
            // 
            // lblHAge
            // 
            this.lblHAge.AutoSize = true;
            this.lblHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAge.Location = new System.Drawing.Point(592, 14);
            this.lblHAge.Name = "lblHAge";
            this.lblHAge.Size = new System.Drawing.Size(32, 13);
            this.lblHAge.TabIndex = 184;
            this.lblHAge.Text = "Age :";
            // 
            // lblHGender
            // 
            this.lblHGender.AutoSize = true;
            this.lblHGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHGender.Location = new System.Drawing.Point(459, 14);
            this.lblHGender.Name = "lblHGender";
            this.lblHGender.Size = new System.Drawing.Size(48, 13);
            this.lblHGender.TabIndex = 183;
            this.lblHGender.Text = "Gender :";
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // tsMain
            // 
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.Transparent;
            this.tsMain.CanOverflow = false;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.tsMain.Location = new System.Drawing.Point(573, -8);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(200, 66);
            this.tsMain.TabIndex = 44;
            this.tsMain.Text = "toolStrip1";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tsMain);
            this.panel2.Location = new System.Drawing.Point(12, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(770, 53);
            this.panel2.TabIndex = 83;
            this.panel2.Tag = "top";
            // 
            // lblAllergARHeading
            // 
            this.lblAllergARHeading.AutoSize = true;
            this.lblAllergARHeading.Location = new System.Drawing.Point(170, 152);
            this.lblAllergARHeading.Name = "lblAllergARHeading";
            this.lblAllergARHeading.Size = new System.Drawing.Size(35, 13);
            this.lblAllergARHeading.TabIndex = 98;
            this.lblAllergARHeading.Tag = "display";
            this.lblAllergARHeading.Text = "New :";
            // 
            // dgSelect
            // 
            this.dgSelect.AllowDrop = true;
            this.dgSelect.AllowUserToAddRows = false;
            this.dgSelect.AllowUserToDeleteRows = false;
            this.dgSelect.AllowUserToOrderColumns = true;
            this.dgSelect.AllowUserToResizeColumns = false;
            this.dgSelect.AllowUserToResizeRows = false;
            this.dgSelect.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSelect.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSelect.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientAllergAR,
            this.AlergAdversID,
            this.Medicineid,
            this.EnteredOn,
            this.AlergARName,
            this.MedName,
            this.Mode});
            this.dgSelect.ContextMenuStrip = this.cmsSelected;
            this.dgSelect.Location = new System.Drawing.Point(12, 176);
            this.dgSelect.MultiSelect = false;
            this.dgSelect.Name = "dgSelect";
            this.dgSelect.ReadOnly = true;
            this.dgSelect.RowHeadersWidth = 40;
            this.dgSelect.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSelect.Size = new System.Drawing.Size(770, 380);
            this.dgSelect.TabIndex = 99;
            this.dgSelect.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgSelect_MouseDown);
            // 
            // PatientAllergAR
            // 
            this.PatientAllergAR.DataPropertyName = "ID";
            this.PatientAllergAR.HeaderText = "PatientAllergAR";
            this.PatientAllergAR.Name = "PatientAllergAR";
            this.PatientAllergAR.ReadOnly = true;
            this.PatientAllergAR.Visible = false;
            // 
            // AlergAdversID
            // 
            this.AlergAdversID.DataPropertyName = "AlergAdversID";
            this.AlergAdversID.HeaderText = "AlergAdversID";
            this.AlergAdversID.Name = "AlergAdversID";
            this.AlergAdversID.ReadOnly = true;
            this.AlergAdversID.Visible = false;
            // 
            // Medicineid
            // 
            this.Medicineid.DataPropertyName = "Medicineid";
            this.Medicineid.HeaderText = "MedID";
            this.Medicineid.Name = "Medicineid";
            this.Medicineid.ReadOnly = true;
            this.Medicineid.Visible = false;
            // 
            // EnteredOn
            // 
            this.EnteredOn.DataPropertyName = "EnteredOn";
            this.EnteredOn.FillWeight = 15F;
            this.EnteredOn.HeaderText = "Date";
            this.EnteredOn.Name = "EnteredOn";
            this.EnteredOn.ReadOnly = true;
            // 
            // AlergARName
            // 
            this.AlergARName.DataPropertyName = "AlergARName";
            this.AlergARName.FillWeight = 45F;
            this.AlergARName.HeaderText = "ALergARName";
            this.AlergARName.Name = "AlergARName";
            this.AlergARName.ReadOnly = true;
            // 
            // MedName
            // 
            this.MedName.DataPropertyName = "MedName";
            this.MedName.FillWeight = 40F;
            this.MedName.HeaderText = "Medicine Name";
            this.MedName.Name = "MedName";
            this.MedName.ReadOnly = true;
            // 
            // Mode
            // 
            this.Mode.DataPropertyName = "Mode";
            this.Mode.HeaderText = "Mode";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Visible = false;
            // 
            // cmsSelected
            // 
            this.cmsSelected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSelectedDelete});
            this.cmsSelected.Name = "cmsSelected";
            this.cmsSelected.Size = new System.Drawing.Size(108, 26);
            // 
            // miSelectedDelete
            // 
            this.miSelectedDelete.Name = "miSelectedDelete";
            this.miSelectedDelete.Size = new System.Drawing.Size(107, 22);
            this.miSelectedDelete.Text = "Delete";
            this.miSelectedDelete.Click += new System.EventHandler(this.miSelectedDelete_Click);
            // 
            // lblMedicineHeading
            // 
            this.lblMedicineHeading.AutoSize = true;
            this.lblMedicineHeading.Location = new System.Drawing.Point(170, 129);
            this.lblMedicineHeading.Name = "lblMedicineHeading";
            this.lblMedicineHeading.Size = new System.Drawing.Size(59, 13);
            this.lblMedicineHeading.TabIndex = 100;
            this.lblMedicineHeading.Text = "Medicine : ";
            // 
            // cmbMed
            // 
            this.cmbMed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbMed.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMed.FormattingEnabled = true;
            this.cmbMed.Location = new System.Drawing.Point(235, 126);
            this.cmbMed.Name = "cmbMed";
            this.cmbMed.Size = new System.Drawing.Size(389, 21);
            this.cmbMed.TabIndex = 103;
            // 
            // cmbAllergAR
            // 
            this.cmbAllergAR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAllergAR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAllergAR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAllergAR.FormattingEnabled = true;
            this.cmbAllergAR.Location = new System.Drawing.Point(235, 149);
            this.cmbAllergAR.Name = "cmbAllergAR";
            this.cmbAllergAR.Size = new System.Drawing.Size(389, 21);
            this.cmbAllergAR.TabIndex = 104;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(630, 126);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(44, 44);
            this.btnAdd.TabIndex = 105;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Red;
            this.panel4.Location = new System.Drawing.Point(620, 150);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(4, 19);
            this.panel4.TabIndex = 157;
            this.panel4.Tag = "Self";
            // 
            // frmPatientAlergAdverse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 568);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.cmbMed);
            this.Controls.Add(this.cmbAllergAR);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblAllergARHeading);
            this.Controls.Add(this.dgSelect);
            this.Controls.Add(this.lblMedicineHeading);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmPatientAlergAdverse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patient Alergies and Adverse Reaction";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPatientAlergAdverse_FormClosing);
            this.Load += new System.EventHandler(this.frmPatientAlergAdverse_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSelect)).EndInit();
            this.cmsSelected.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton miExit;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripButton miRefresh;
        private System.Windows.Forms.ToolStripButton miSave;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAllergARHeading;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtPName;
        private System.Windows.Forms.MaskedTextBox txtPRNo;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label lblHName;
        private System.Windows.Forms.Label lblHPRNo;
        private System.Windows.Forms.Label lblHAge;
        private System.Windows.Forms.Label lblHGender;
        private System.Windows.Forms.DataGridView dgSelect;
        private System.Windows.Forms.ContextMenuStrip cmsSelected;
        private System.Windows.Forms.ToolStripMenuItem miSelectedDelete;
        private System.Windows.Forms.Label lblMedicineHeading;
        private System.Windows.Forms.ComboBox cmbMed;
        private System.Windows.Forms.ComboBox cmbAllergAR;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientAllergAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlergAdversID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Medicineid;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlergARName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.Panel panel4;

    }
}