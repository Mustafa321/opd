using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using OPD_BL;

namespace OPD
{
    public partial class frmPicture : Form
    {
        # region "Class variables" and "Properties"

        private string _PatientID = "";
        private string _PatientName = "";
        private string _Path = "";
        private frmOPD MainOPD = null;

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string PatientName
        {
            set { _PatientName = value; }
        }

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }

        #endregion

        public frmPicture(string PatientID)
        {
            InitializeComponent();
            _PatientID = PatientID;
        }

        public frmPicture(string PatientID, frmOPD Frm)
        {
            InitializeComponent();
            _PatientID = PatientID;
            this.MainOPD = Frm;
        }

        private void frmPicture_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            this.Text += " ( " + _PatientName+ " )";
            objComp.ApplyStyleToControls(this);
            FillDocType();
            PopulatImages();

            if (pnlPicCollection.Controls.Count != 0)
            {
                pnlPicCollection.Controls[0].Focus();
            }

            objComp = null;
        }

        private void FillDocType()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLDocType objDocType = new clsBLDocType(objConnection);
            SComponents objComp = new SComponents();
            try
            {
                objDocType.Type = "P";//Picture
                objConnection.Connection_Open();
                objComp.FillComboBox(cboDocType, objDocType.GetAll(2), "Name", "DocTypeID", true);
                objConnection.Connection_Close();
            }
            catch
            { }
            finally
            {
                objDocType = null;
                objComp = null;
                objConnection = null;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            SelectPicture();
        }

        private void SelectPicture()
        {
            OpenFileDialog OFDPic = new OpenFileDialog();

            //OFDPic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG| All files (*.*)|*.*";
            OFDPic.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";

            // Allow the user to select multiple images.
            OFDPic.Multiselect = false;
            OFDPic.Title = "My Image Browser";
            DialogResult dr = OFDPic.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                _Path = OFDPic.FileName;
                pbSave.ImageLocation = _Path;
                pbSave.Tag = _Path;
                pbSave.SizeMode = PictureBoxSizeMode.StretchImage;
                btnSave.Enabled = true;
                pbLargeView.Image = null;
                txtComment.Text = "";
            }
            else
            {
                btnSave.Enabled = false;
            }

            OFDPic.Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Insert();
            PopulatImages();
            btnSave.Enabled = false;
            pbSave.Image = null;
            txtComment.Text = "";
            cboDocType.SelectedIndex = 0;
        }

        private void Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientPic = new clsBLPatientPicture(objConnection);

            try
            {
                if (!Path.Equals(""))
                {
                    byte[] imagedata = ReadImage();
                    objPatientPic.SmallPicture = Convert.ToBase64String(imagedata);
                    objPatientPic = SetBLValues(objPatientPic);

                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    if (objPatientPic.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                        objConnection.Connection_Close();

                        MessageBox.Show("Picture Added Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.
            Information);
                    }
                    else
                    {
                        MessageBox.Show(objPatientPic.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPatientPic = null;
                objConnection = null;
            }
        }

        private clsBLPatientPicture SetBLValues(clsBLPatientPicture objPatientPic)
        {
            objPatientPic.PatientID = PatientID;
            objPatientPic.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            objPatientPic.Comment = clsSharedVariables.InItCaps(txtComment.Text.Trim());
            if (cboDocType.SelectedIndex != 0)
            {
                objPatientPic.DocTypeID = cboDocType.SelectedValue.ToString();
            }
            objPatientPic.SavedMethod = "PS";//Picture Save Database

            return objPatientPic;
        }

        byte[] ReadImage()
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(_Path);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(_Path, FileMode.Open,
                                                    FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to 

            //supply number of bytes to read from file.
            //In this case we want to read entire file. 

            //So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);
            return data;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientPic = new clsBLPatientPicture(objConnection);

            try
            {
                objConnection.Connection_Open();
                objPatientPic.PictureID = lblPictureID.Text;
                objPatientPic.Delete();
                PopulatImages();
                pbLargeView.Image = null;
            }
            finally
            {
                objConnection.Connection_Close();
                btnDelete.Enabled = false;
                objPatientPic = null;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            pbSave.Image = null;
            pbLargeView.Image = null;
            txtComment.Text = "";
            btnDelete.Enabled = false;
            PopulatImages();
            cboDocType.SelectedIndex = 0;
        }

        private void PopulatImages()
        {
            pnlPicCollection.Controls.Clear();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objPatientPic = new clsBLPatientPicture(objConnection);
            byte[] buffer = null;
            DataView dvPic;
            int x = 1, y = 1;

            try
            {
                objConnection.Connection_Open();
                objPatientPic.PatientID = PatientID;
                if (cboDocType.SelectedIndex != 0)
                {
                    objPatientPic.DocTypeID= cboDocType.SelectedValue.ToString();
                }

                dvPic = objPatientPic.GetAll(1);
                for (int i = 0; i < dvPic.Table.Rows.Count; i++)
                {
                    buffer = Convert.FromBase64String(dvPic.Table.Rows[i]["smallpicture"].ToString());
                    MemoryStream StrMem = new MemoryStream(buffer);
                    pnlPicCollection.Controls.Add(CreatePB(x, y, "pbSmall" + i.ToString(), StrMem, dvPic.Table.Rows[i]["PictureID"].ToString() + "::c" + dvPic.Table.Rows[i]["Comment"].ToString() + "::d" + dvPic.Table.Rows[i]["DocTypeID"].ToString()));

                    pnlPicCollection.Controls.Add(llblProperties(x-2, y + 100, "llbl:" + i.ToString(), dvPic.Table.Rows[i]["Enteredon"].ToString(), dvPic.Table.Rows[i]["PictureID"].ToString()));
                    if (dvPic.Table.Rows[i]["DocName"] != null)
                    {
                        pnlPicCollection.Controls.Add(llblProperties(x - 2, y + 112, "llblType:" + i.ToString(), dvPic.Table.Rows[i]["DocName"].ToString(), dvPic.Table.Rows[i]["PictureID"].ToString()));
                    }

                    x = x + 104;
                }
            }
            finally
            {
                objConnection.Connection_Close();
                objPatientPic = null;
                dvPic = null;
            }
        }

        private LinkLabel llblProperties(int x, int y, string llblName, string textVal, string textTag)
        {
            LinkLabel llbl = new LinkLabel();

            llbl.AutoSize = true;
            llbl.Location = new System.Drawing.Point(x, y);
            llbl.Name = llblName;
            llbl.TextAlign = ContentAlignment.TopCenter;
            llbl.Size = new System.Drawing.Size(30, 20);
            llbl.Text = textVal;
            llbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));// 8.25F
            llbl.BackColor = Color.Transparent;
            llbl.Tag = textTag;
            llbl.ForeColor = Color.Maroon;
            llbl.Click += new EventHandler(llbl_Click);
            
            return llbl;
        }

        private void llbl_Click(object sender, EventArgs e)
        {
            string[] sep ={ "::" };
            LinkLabel llbl = new LinkLabel();
            llbl = (LinkLabel)sender;

            PictureBox pb = (PictureBox)pnlPicCollection.Controls.Find("pbSmall" + llbl.Name.Split(':')[1].ToString(), false)[0]; 

            lblPictureID.Text = llbl.Tag.ToString();
            
            txtComment.Text = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(1).ToString().Substring(1);
            if (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries)[2].Length == 1)
            {
                cboDocType.SelectedIndex = 0;
            }
            else
            {
                cboDocType.SelectedValue = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(2).ToString().Substring(1);
            }

            btnDelete.Enabled = true;
            pbLargeView.Image = pb.Image;

        }

        private PictureBox CreatePB(int x, int y, string picName, MemoryStream ms, string Comment)
        {
            PictureBox pb = new PictureBox();
            string[] sep ={ "::" };

            pb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pb.Location = new System.Drawing.Point(x, y);
            pb.Name = picName;
            pb.Tag = picName;
            pb.Size = new System.Drawing.Size(100, 100);
            pb.TabStop = false;
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
            if (Comment.Split(sep, StringSplitOptions.RemoveEmptyEntries).GetUpperBound(0) > 1 &&
                (Comment.Split(sep, StringSplitOptions.RemoveEmptyEntries)[1].StartsWith("c")))
            {
                this.toolTip1.SetToolTip(pb, Comment.Split(sep, StringSplitOptions.RemoveEmptyEntries)[1].Substring(1));
            }
            pb.Image = Image.FromStream(ms);
            pb.Tag = Comment;
            pb.Click += new System.EventHandler(pbSmall_Click);

            return pb;
        }

        private void pbSmall_Click(object sender, EventArgs e)
        {
            string[] sep ={ "::" };
            PictureBox pb = (PictureBox)sender;
            lblPictureID.Text = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(0).ToString();
            //if (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetUpperBound(0) > 1 &&
            //    (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries)[1].StartsWith("c")))
            //{
            txtComment.Text = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(1).ToString().Substring(1);
            //}
            //else if (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetUpperBound(0) > 1 &&
            //    (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries)[1].StartsWith("d")))
            //{
            //    cboDocType.SelectedValue = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(1).ToString();
            //}

            //if (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetUpperBound(0)==2)
            if (pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries)[2].Length == 1)
            {
                cboDocType.SelectedIndex = 0;
            }
            else
            {
                cboDocType.SelectedValue = pb.Tag.ToString().Split(sep, StringSplitOptions.RemoveEmptyEntries).GetValue(2).ToString().Substring(1);
            }

            btnDelete.Enabled = true;
            pbLargeView.Image = pb.Image;
        }

        private void ShowLargePic(PictureBox Pb)
        {
            pbLargeView.SizeMode = PictureBoxSizeMode.AutoSize;
            pbLargeView.Image = Pb.Image;
            pbLargeView.Width = pnlPicSingle.Width;
            pbLargeView.Height = pnlPicSingle.Height;
        }

        private void pbLargeView_Click(object sender, EventArgs e)
        {
            if (pbLargeView.SizeMode == PictureBoxSizeMode.AutoSize)
            {
                pbLargeView.Size = new System.Drawing.Size(590, 390);
                pbLargeView.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (pbLargeView.SizeMode == PictureBoxSizeMode.StretchImage)
            {
                pbLargeView.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void frmPicture_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }
     
        private void btnAddNewType_Click(object sender, EventArgs e)
        {
            frmDocType objDocType = new frmDocType(this);
            this.Hide();
            objDocType.Show();
        }

        private void cboDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatImages();
        }
    }
}