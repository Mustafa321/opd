namespace OPD
{
    partial class frmDataGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("January");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("February");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("March");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("April");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("May");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("June");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("July");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("August");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("September");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("October");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("November");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("December");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataGraph));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tvVisitMonth = new System.Windows.Forms.TreeView();
            this.tvVisitYear = new System.Windows.Forms.TreeView();
            this.tvCity = new System.Windows.Forms.TreeView();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.btnItemCAll = new System.Windows.Forms.Button();
            this.btnItemEAll = new System.Windows.Forms.Button();
            this.tvItems = new System.Windows.Forms.TreeView();
            this.lblItems = new System.Windows.Forms.Label();
            this.lblVisitDate = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboDrawCriteria = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbGraph = new System.Windows.Forms.RadioButton();
            this.rbData = new System.Windows.Forms.RadioButton();
            this.btnDraw = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lblPatientFound = new System.Windows.Forms.Label();
            this.lblItem = new System.Windows.Forms.Label();
            this.dgPatient = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PateintName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlGraph = new System.Windows.Forms.Panel();
            this.lblXHeading = new System.Windows.Forms.Label();
            this.lblSubHeading = new System.Windows.Forms.Label();
            this.lblMainHeading = new System.Windows.Forms.Label();
            this.crvGraph = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.pnlDetail = new System.Windows.Forms.Panel();
            this.lblGraphType = new System.Windows.Forms.Label();
            this.cboGraphType = new System.Windows.Forms.ComboBox();
            this.lblItemHeading = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatient)).BeginInit();
            this.panel4.SuspendLayout();
            this.pnlGraph.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tvVisitMonth);
            this.panel1.Controls.Add(this.tvVisitYear);
            this.panel1.Controls.Add(this.tvCity);
            this.panel1.Controls.Add(this.dtpFrom);
            this.panel1.Controls.Add(this.dtpTo);
            this.panel1.Controls.Add(this.btnItemCAll);
            this.panel1.Controls.Add(this.btnItemEAll);
            this.panel1.Controls.Add(this.tvItems);
            this.panel1.Controls.Add(this.lblItems);
            this.panel1.Controls.Add(this.lblVisitDate);
            this.panel1.Controls.Add(this.lblCity);
            this.panel1.Controls.Add(this.lblAge);
            this.panel1.Location = new System.Drawing.Point(1, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(212, 659);
            this.panel1.TabIndex = 1;
            // 
            // tvVisitMonth
            // 
            this.tvVisitMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvVisitMonth.CheckBoxes = true;
            this.tvVisitMonth.Indent = 15;
            this.tvVisitMonth.ItemHeight = 16;
            this.tvVisitMonth.Location = new System.Drawing.Point(78, 16);
            this.tvVisitMonth.Name = "tvVisitMonth";
            treeNode1.Name = "Jan";
            treeNode1.Text = "January";
            treeNode2.Name = "Feb";
            treeNode2.Text = "February";
            treeNode3.Name = "Mar";
            treeNode3.Text = "March";
            treeNode4.Name = "Apr";
            treeNode4.Text = "April";
            treeNode5.Name = "May";
            treeNode5.Text = "May";
            treeNode6.Name = "June";
            treeNode6.Text = "June";
            treeNode7.Name = "July";
            treeNode7.Text = "July";
            treeNode8.Name = "Aug";
            treeNode8.Text = "August";
            treeNode9.Name = "Sep";
            treeNode9.Text = "September";
            treeNode10.Name = "Oct";
            treeNode10.Text = "October";
            treeNode11.Name = "Nov";
            treeNode11.Text = "November";
            treeNode12.Name = "Dec";
            treeNode12.Text = "December";
            this.tvVisitMonth.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            this.tvVisitMonth.ShowLines = false;
            this.tvVisitMonth.ShowPlusMinus = false;
            this.tvVisitMonth.ShowRootLines = false;
            this.tvVisitMonth.Size = new System.Drawing.Size(131, 98);
            this.tvVisitMonth.TabIndex = 19;
            // 
            // tvVisitYear
            // 
            this.tvVisitYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvVisitYear.CheckBoxes = true;
            this.tvVisitYear.Indent = 15;
            this.tvVisitYear.ItemHeight = 16;
            this.tvVisitYear.Location = new System.Drawing.Point(1, 16);
            this.tvVisitYear.Name = "tvVisitYear";
            this.tvVisitYear.ShowLines = false;
            this.tvVisitYear.ShowPlusMinus = false;
            this.tvVisitYear.ShowRootLines = false;
            this.tvVisitYear.Size = new System.Drawing.Size(76, 98);
            this.tvVisitYear.TabIndex = 18;
            // 
            // tvCity
            // 
            this.tvCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvCity.CheckBoxes = true;
            this.tvCity.Indent = 15;
            this.tvCity.ItemHeight = 16;
            this.tvCity.Location = new System.Drawing.Point(1, 133);
            this.tvCity.Name = "tvCity";
            this.tvCity.ShowLines = false;
            this.tvCity.ShowPlusMinus = false;
            this.tvCity.ShowRootLines = false;
            this.tvCity.Size = new System.Drawing.Size(208, 211);
            this.tvCity.TabIndex = 17;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(126, 344);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(83, 20);
            this.dtpFrom.TabIndex = 14;
            this.dtpFrom.Leave += new System.EventHandler(this.dtpTo_Leave);
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(42, 344);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(85, 20);
            this.dtpTo.TabIndex = 13;
            this.dtpTo.Leave += new System.EventHandler(this.dtpTo_Leave);
            // 
            // btnItemCAll
            // 
            this.btnItemCAll.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnItemCAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemCAll.BackgroundImage")));
            this.btnItemCAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnItemCAll.Location = new System.Drawing.Point(183, 363);
            this.btnItemCAll.Name = "btnItemCAll";
            this.btnItemCAll.Size = new System.Drawing.Size(23, 23);
            this.btnItemCAll.TabIndex = 5;
            this.btnItemCAll.UseVisualStyleBackColor = false;
            this.btnItemCAll.Click += new System.EventHandler(this.btnItemCAll_Click);
            // 
            // btnItemEAll
            // 
            this.btnItemEAll.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnItemEAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemEAll.BackgroundImage")));
            this.btnItemEAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnItemEAll.Location = new System.Drawing.Point(160, 363);
            this.btnItemEAll.Name = "btnItemEAll";
            this.btnItemEAll.Size = new System.Drawing.Size(23, 23);
            this.btnItemEAll.TabIndex = 4;
            this.btnItemEAll.UseVisualStyleBackColor = false;
            this.btnItemEAll.Click += new System.EventHandler(this.btnItemEAll_Click);
            // 
            // tvItems
            // 
            this.tvItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvItems.Indent = 20;
            this.tvItems.Location = new System.Drawing.Point(1, 387);
            this.tvItems.Name = "tvItems";
            this.tvItems.Size = new System.Drawing.Size(208, 269);
            this.tvItems.TabIndex = 1;
            // 
            // lblItems
            // 
            this.lblItems.AutoSize = true;
            this.lblItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItems.Location = new System.Drawing.Point(2, 368);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(49, 13);
            this.lblItems.TabIndex = 3;
            this.lblItems.Tag = "display";
            this.lblItems.Text = "Items : ";
            // 
            // lblVisitDate
            // 
            this.lblVisitDate.AutoSize = true;
            this.lblVisitDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisitDate.Location = new System.Drawing.Point(2, 1);
            this.lblVisitDate.Name = "lblVisitDate";
            this.lblVisitDate.Size = new System.Drawing.Size(74, 13);
            this.lblVisitDate.TabIndex = 15;
            this.lblVisitDate.Text = "Visit Date : ";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCity.Location = new System.Drawing.Point(5, 117);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(40, 13);
            this.lblCity.TabIndex = 16;
            this.lblCity.Text = "City : ";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAge.Location = new System.Drawing.Point(2, 347);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(45, 13);
            this.lblAge.TabIndex = 10;
            this.lblAge.Text = "DOB : ";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cboDrawCriteria);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(1, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(212, 24);
            this.panel2.TabIndex = 2;
            // 
            // cboDrawCriteria
            // 
            this.cboDrawCriteria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDrawCriteria.FormattingEnabled = true;
            this.cboDrawCriteria.Items.AddRange(new object[] {
            "Location",
            "Age",
            "Visit Date"});
            this.cboDrawCriteria.Location = new System.Drawing.Point(113, 1);
            this.cboDrawCriteria.Name = "cboDrawCriteria";
            this.cboDrawCriteria.Size = new System.Drawing.Size(96, 21);
            this.cboDrawCriteria.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.rbGraph);
            this.panel3.Controls.Add(this.rbData);
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(111, 20);
            this.panel3.TabIndex = 4;
            // 
            // rbGraph
            // 
            this.rbGraph.AutoSize = true;
            this.rbGraph.Location = new System.Drawing.Point(56, 1);
            this.rbGraph.Name = "rbGraph";
            this.rbGraph.Size = new System.Drawing.Size(54, 17);
            this.rbGraph.TabIndex = 1;
            this.rbGraph.Text = "Graph";
            this.rbGraph.UseVisualStyleBackColor = true;
            this.rbGraph.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbData
            // 
            this.rbData.AutoSize = true;
            this.rbData.Checked = true;
            this.rbData.Location = new System.Drawing.Point(6, 1);
            this.rbData.Name = "rbData";
            this.rbData.Size = new System.Drawing.Size(48, 17);
            this.rbData.TabIndex = 0;
            this.rbData.TabStop = true;
            this.rbData.Text = "Data";
            this.rbData.UseVisualStyleBackColor = true;
            this.rbData.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // btnDraw
            // 
            this.btnDraw.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnDraw.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDraw.Location = new System.Drawing.Point(679, -1);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(60, 24);
            this.btnDraw.TabIndex = 10;
            this.btnDraw.Text = "Draw";
            this.btnDraw.UseVisualStyleBackColor = false;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Location = new System.Drawing.Point(739, -1);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(60, 24);
            this.btnSelect.TabIndex = 8;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lblPatientFound
            // 
            this.lblPatientFound.AutoSize = true;
            this.lblPatientFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientFound.Location = new System.Drawing.Point(555, 5);
            this.lblPatientFound.Name = "lblPatientFound";
            this.lblPatientFound.Size = new System.Drawing.Size(80, 13);
            this.lblPatientFound.TabIndex = 7;
            this.lblPatientFound.Text = "lblPatientFound";
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            this.lblItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItem.Location = new System.Drawing.Point(292, 5);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(37, 13);
            this.lblItem.TabIndex = 6;
            this.lblItem.Text = "lblItem";
            // 
            // dgPatient
            // 
            this.dgPatient.AllowUserToAddRows = false;
            this.dgPatient.AllowUserToResizeColumns = false;
            this.dgPatient.AllowUserToResizeRows = false;
            this.dgPatient.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatient.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPatient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPatient.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.cityid,
            this.PRNO,
            this.PateintName,
            this.VisitDate,
            this.Item,
            this.TypeID});
            this.dgPatient.Location = new System.Drawing.Point(214, 57);
            this.dgPatient.MultiSelect = false;
            this.dgPatient.Name = "dgPatient";
            this.dgPatient.ReadOnly = true;
            this.dgPatient.RowHeadersWidth = 25;
            this.dgPatient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPatient.Size = new System.Drawing.Size(802, 657);
            this.dgPatient.TabIndex = 3;
            this.dgPatient.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPatient_ColumnHeaderMouseClick);
            // 
            // SrNo
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SrNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.SrNo.FillWeight = 10F;
            this.SrNo.HeaderText = "Sr.";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            // 
            // cityid
            // 
            this.cityid.HeaderText = "cityid";
            this.cityid.Name = "cityid";
            this.cityid.ReadOnly = true;
            this.cityid.Visible = false;
            // 
            // PRNO
            // 
            this.PRNO.DataPropertyName = "PRNo";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PRNO.DefaultCellStyle = dataGridViewCellStyle3;
            this.PRNO.FillWeight = 25F;
            this.PRNO.HeaderText = "" + clsSharedVariables.PRNOHeading + "";
            this.PRNO.Name = "PRNO";
            this.PRNO.ReadOnly = true;
            // 
            // PateintName
            // 
            this.PateintName.DataPropertyName = "PatientName";
            this.PateintName.FillWeight = 40F;
            this.PateintName.HeaderText = "Patient Name";
            this.PateintName.Name = "PateintName";
            this.PateintName.ReadOnly = true;
            // 
            // VisitDate
            // 
            this.VisitDate.DataPropertyName = "Visitdate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd/MM/yyyy";
            this.VisitDate.DefaultCellStyle = dataGridViewCellStyle4;
            this.VisitDate.FillWeight = 25F;
            this.VisitDate.HeaderText = "Visit Date";
            this.VisitDate.Name = "VisitDate";
            this.VisitDate.ReadOnly = true;
            // 
            // Item
            // 
            this.Item.DataPropertyName = "ItemName";
            this.Item.HeaderText = "Patient Data";
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            this.Item.Visible = false;
            // 
            // TypeID
            // 
            this.TypeID.DataPropertyName = "itemtype";
            this.TypeID.HeaderText = "Typeid";
            this.TypeID.Name = "TypeID";
            this.TypeID.ReadOnly = true;
            this.TypeID.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(3, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1013, 28);
            this.panel4.TabIndex = 4;
            this.panel4.Tag = "med";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(446, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Analysis";
            // 
            // pnlGraph
            // 
            this.pnlGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGraph.Controls.Add(this.lblXHeading);
            this.pnlGraph.Controls.Add(this.lblSubHeading);
            this.pnlGraph.Controls.Add(this.lblMainHeading);
            this.pnlGraph.Controls.Add(this.crvGraph);
            this.pnlGraph.Location = new System.Drawing.Point(214, 55);
            this.pnlGraph.Name = "pnlGraph";
            this.pnlGraph.Size = new System.Drawing.Size(802, 659);
            this.pnlGraph.TabIndex = 5;
            // 
            // lblXHeading
            // 
            this.lblXHeading.AutoSize = true;
            this.lblXHeading.BackColor = System.Drawing.Color.White;
            this.lblXHeading.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXHeading.Location = new System.Drawing.Point(400, 605);
            this.lblXHeading.Name = "lblXHeading";
            this.lblXHeading.Size = new System.Drawing.Size(0, 16);
            this.lblXHeading.TabIndex = 2;
            this.lblXHeading.Tag = "SS";
            // 
            // lblSubHeading
            // 
            this.lblSubHeading.BackColor = System.Drawing.Color.White;
            this.lblSubHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubHeading.Location = new System.Drawing.Point(1, 46);
            this.lblSubHeading.Name = "lblSubHeading";
            this.lblSubHeading.Size = new System.Drawing.Size(800, 20);
            this.lblSubHeading.TabIndex = 3;
            this.lblSubHeading.Tag = "SS";
            this.lblSubHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMainHeading
            // 
            this.lblMainHeading.BackColor = System.Drawing.Color.White;
            this.lblMainHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainHeading.Location = new System.Drawing.Point(1, 25);
            this.lblMainHeading.Name = "lblMainHeading";
            this.lblMainHeading.Size = new System.Drawing.Size(800, 22);
            this.lblMainHeading.TabIndex = 1;
            this.lblMainHeading.Tag = "SS";
            this.lblMainHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // crvGraph
            // 
            this.crvGraph.ActiveViewIndex = -1;
            this.crvGraph.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.crvGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvGraph.DisplayBackgroundEdge = false;
            this.crvGraph.DisplayGroupTree = false;
            this.crvGraph.DisplayStatusBar = false;
            this.crvGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvGraph.EnableDrillDown = false;
            this.crvGraph.EnableToolTips = false;
            this.crvGraph.Location = new System.Drawing.Point(0, 0);
            this.crvGraph.Margin = new System.Windows.Forms.Padding(0);
            this.crvGraph.Name = "crvGraph";
            this.crvGraph.SelectionFormula = "";
            this.crvGraph.ShowCloseButton = false;
            this.crvGraph.ShowGotoPageButton = false;
            this.crvGraph.ShowGroupTreeButton = false;
            this.crvGraph.ShowPageNavigateButtons = false;
            this.crvGraph.ShowRefreshButton = false;
            this.crvGraph.ShowTextSearchButton = false;
            this.crvGraph.ShowZoomButton = false;
            this.crvGraph.Size = new System.Drawing.Size(800, 657);
            this.crvGraph.TabIndex = 0;
            this.crvGraph.ViewTimeSelectionFormula = "";
            this.crvGraph.Visible = false;
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.Controls.Add(this.lblGraphType);
            this.pnlDetail.Controls.Add(this.cboGraphType);
            this.pnlDetail.Controls.Add(this.lblItemHeading);
            this.pnlDetail.Controls.Add(this.btnDraw);
            this.pnlDetail.Controls.Add(this.lblPatientFound);
            this.pnlDetail.Controls.Add(this.lblItem);
            this.pnlDetail.Controls.Add(this.btnSelect);
            this.pnlDetail.Location = new System.Drawing.Point(214, 30);
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.Size = new System.Drawing.Size(802, 24);
            this.pnlDetail.TabIndex = 6;
            // 
            // lblGraphType
            // 
            this.lblGraphType.AutoSize = true;
            this.lblGraphType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGraphType.Location = new System.Drawing.Point(3, 5);
            this.lblGraphType.Name = "lblGraphType";
            this.lblGraphType.Size = new System.Drawing.Size(47, 13);
            this.lblGraphType.TabIndex = 14;
            this.lblGraphType.Text = "Type : ";
            this.lblGraphType.Visible = false;
            // 
            // cboGraphType
            // 
            this.cboGraphType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGraphType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGraphType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGraphType.Enabled = false;
            this.cboGraphType.FormattingEnabled = true;
            this.cboGraphType.Items.AddRange(new object[] {
            "Select",
            "Cluster Column",
            "Stack Column",
            "3D Column",
            "3D Column Cluster",
            "3D Stack Column",
            "Cluster Bar",
            "Stacked Bar",
            "3D Bar",
            "3D Bar Cluster",
            "3D Stacked Bar",
            "Line",
            "Line With Marker",
            "3D Line",
            "3D OverLaped Line",
            "Smooth Line",
            "Smooth Line With Marker",
            "High Low Close"});
            this.cboGraphType.Location = new System.Drawing.Point(51, 1);
            this.cboGraphType.Name = "cboGraphType";
            this.cboGraphType.Size = new System.Drawing.Size(176, 21);
            this.cboGraphType.TabIndex = 13;
            this.cboGraphType.Visible = false;
            this.cboGraphType.SelectedIndexChanged += new System.EventHandler(this.cboGraphType_SelectedIndexChanged);
            // 
            // lblItemHeading
            // 
            this.lblItemHeading.AutoSize = true;
            this.lblItemHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemHeading.Location = new System.Drawing.Point(245, 5);
            this.lblItemHeading.Name = "lblItemHeading";
            this.lblItemHeading.Size = new System.Drawing.Size(41, 13);
            this.lblItemHeading.TabIndex = 12;
            this.lblItemHeading.Text = "label3";
            // 
            // frmDataGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 714);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlGraph);
            this.Controls.Add(this.dgPatient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmDataGraph";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Analysis";
            this.Load += new System.EventHandler(this.frmDataGraph_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDataGraph_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatient)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnlGraph.ResumeLayout(false);
            this.pnlGraph.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        //private AxMicrosoft.Office.Interop.Owc11.AxChartSpace csPatients;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView tvItems;
        private System.Windows.Forms.Label lblItems;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbGraph;
        private System.Windows.Forms.RadioButton rbData;
        private System.Windows.Forms.Label lblItem;
        private System.Windows.Forms.DataGridView dgPatient;
        private System.Windows.Forms.Label lblPatientFound;
        private System.Windows.Forms.Button btnItemCAll;
        private System.Windows.Forms.Button btnItemEAll;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblVisitDate;
        private System.Windows.Forms.TreeView tvCity;
        private System.Windows.Forms.TreeView tvVisitYear;
        private System.Windows.Forms.TreeView tvVisitMonth;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.ComboBox cboDrawCriteria;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.Panel pnlGraph;
        private System.Windows.Forms.Panel pnlDetail;
        private System.Windows.Forms.Label lblItemHeading;
        private System.Windows.Forms.Label lblGraphType;
        private System.Windows.Forms.ComboBox cboGraphType;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityid;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PateintName;
        private System.Windows.Forms.DataGridViewTextBoxColumn VisitDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeID;
        private System.Windows.Forms.Label lblXHeading;
        private System.Windows.Forms.Label lblMainHeading;
        private System.Windows.Forms.Label lblSubHeading;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvGraph;
    }
}