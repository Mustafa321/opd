namespace OPD
{
  partial class frmDiseaseInfo
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDiseaseInfo));
      this.panel1 = new System.Windows.Forms.Panel();
      this.miExit = new System.Windows.Forms.ToolStripButton();
      this.panel2 = new System.Windows.Forms.Panel();
      this.lblDiseaseInfoHeading = new System.Windows.Forms.Label();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.panel2.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel1.Location = new System.Drawing.Point(1, 56);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1011, 645);
      this.panel1.TabIndex = 49;
      // 
      // miExit
      // 
      this.miExit.AutoSize = false;
      this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
      this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.miExit.ForeColor = System.Drawing.Color.Black;
      this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
      this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
      this.miExit.Name = "miExit";
      this.miExit.Size = new System.Drawing.Size(65, 52);
      this.miExit.Text = "&Exit";
      this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
      this.miExit.ToolTipText = "Exit ";
      this.miExit.Click += new System.EventHandler(this.miExit_Click);
      // 
      // panel2
      // 
      this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel2.Controls.Add(this.lblDiseaseInfoHeading);
      this.panel2.Controls.Add(this.toolStrip1);
      this.panel2.Location = new System.Drawing.Point(1, 1);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(1011, 53);
      this.panel2.TabIndex = 48;
      this.panel2.Tag = "top";
      // 
      // lblDiseaseInfoHeading
      // 
      this.lblDiseaseInfoHeading.AutoSize = true;
      this.lblDiseaseInfoHeading.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDiseaseInfoHeading.Location = new System.Drawing.Point(312, 6);
      this.lblDiseaseInfoHeading.Name = "lblDiseaseInfoHeading";
      this.lblDiseaseInfoHeading.Size = new System.Drawing.Size(385, 38);
      this.lblDiseaseInfoHeading.TabIndex = 45;
      this.lblDiseaseInfoHeading.Tag = "no";
      this.lblDiseaseInfoHeading.Text = "Disease Information";
      // 
      // toolStrip1
      // 
      this.toolStrip1.AutoSize = false;
      this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
      this.toolStrip1.CanOverflow = false;
      this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
      this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
      this.toolStrip1.Location = new System.Drawing.Point(942, -8);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(86, 66);
      this.toolStrip1.TabIndex = 44;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // frmDiseaseInfo
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1013, 702);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.panel2);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.MaximizeBox = false;
      this.Name = "frmDiseaseInfo";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Disease Information";
      this.Load += new System.EventHandler(this.frmDiseaseInfo_Load);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripButton miExit;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label lblDiseaseInfoHeading;
    private System.Windows.Forms.ToolStrip toolStrip1;



  }
}