﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OPD
{
    public partial class Notifications : Form
    {
        private frmOPD MainOPD = null;
        public Notifications()
        {
            InitializeComponent();
        }
        public Notifications(frmOPD Frm)
        {
            InitializeComponent();
          
            this.MainOPD = Frm;
        }
        private void Notifications_Load(object sender, EventArgs e)
        {
            GetNotificationDetail();
        }
        public void FillData(List<MessageDetail> list)
        {
            NotificationGridView.AutoGenerateColumns = false;
            NotificationGridView.AllowUserToAddRows = false;
            NotificationGridView.DataSource = list;
            //------------------------------------------------------
            DataGridViewTextBoxColumn column1 = new DataGridViewTextBoxColumn();
            column1.Name = "MRNo";
            column1.HeaderText = "MRN No.";
            column1.DataPropertyName = "MRNo";
            column1.Width = 200;
            NotificationGridView.Columns.Add(column1);
            //------------------------------------------------------
            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
            column2.Name = "Name";
            column2.HeaderText = "Patient Name";
            column2.DataPropertyName = "Name";
            column2.Width = 200;
            NotificationGridView.Columns.Add(column2);
            //------------------------------------------------------
            DataGridViewTextBoxColumn column3 = new DataGridViewTextBoxColumn();
            column3.Name = "Date";
            column3.HeaderText = "Date";
            column3.DataPropertyName = "Date";
            column3.Width = 200;
            NotificationGridView.Columns.Add(column3);
            //------------------------------------------------------
            DataGridViewTextBoxColumn column4 = new DataGridViewTextBoxColumn();
            column4.Name = "Time";
            column4.HeaderText = "Time";
            column4.DataPropertyName = "Time";
            column4.Width = 200;
            NotificationGridView.Columns.Add(column4);
            //------------------------------------------------------
            DataGridViewTextBoxColumn column5 = new DataGridViewTextBoxColumn();
            column5.Name = "Message";
            column5.HeaderText = "Message";
            column5.DataPropertyName = "Message";

            column5.Width = 200;
            NotificationGridView.Columns.Add(column5);

        }
        public async Task<string> GetNotificationDetail()
        {
            List<MessageDetail> list = new List<MessageDetail>();
            try
            {
                var DoctorId = System.Configuration.ConfigurationSettings.AppSettings["DoctorId"].ToString();


                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync("api/Chat/GetUnReadMessagesDetail?DoctorId=" + DoctorId);
                    if (response.IsSuccessStatusCode)
                    {
                        list = await response.Content.ReadAsAsync<List<MessageDetail>>();
                       

                    }

                    FillData(list); 
                }

            }
            catch (Exception ex)
            {
                List<MessageDetail> list2 = new List<MessageDetail>();
                FillData(list2); 
            }
            //this.Notifications.Text = "Notifications (" + res.ToString() + ")";
            return "true";
        }

        private void Notifications_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }
    }
}
public class MessageDetail
{
    public string MRNo { get; set; }
    public string Name { get; set; }

    public string Message { get; set; }

    public string Date { get; set; }

    public string Time { get; set; }

}