﻿using OPD_BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OPD
{
    public partial class frmAddCity : Form
    {
        public frmAddCity()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           var n= CityName.Text;
            if(n=="")
            {
                CityName.Focus();
                ErrorText.Text = "Please Enter City Name";
                ErrorText.Visible = true;
              

            }
            else
            {
               if(Insert())
                {
                    ErrorText.Text = "";
                    ErrorText.Visible = false;
                    MessageBox.Show("City Added  Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    var frm = new frmPatientReg();
                
                    frm.Show();
                //objComp.FillComboBox(cboCity, objPatientReg.GetAll(3), "Name", "CityID", false);
                //if (objPatientReg.GetAll(10).Table.Rows.Count != 0)
                //{
                //    cboCity.Text = objPatientReg.GetAll(10).Table.Rows[0]["Name"].ToString();
                //}
                  
                }
               else
                {
                    
                }
              
               
            }
        }
        private bool Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLCity objCity = new clsBLCity(objConnection);
            objConnection.Connection_Open();
            objCity = SetBLValues(objCity, objConnection);
            objConnection.Transaction_Begin();
            if(!objCity.CheckCityExist())
            {
           
                objConnection.Connection_Open();
              
                objConnection.Transaction_Begin();

                if (objCity.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    objCity.CityName = "";
                    objCity.CityId = "0";
                    //MessageBox.Show("Patient Registration complete Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
                else
                {
                    MessageBox.Show(objCity.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(objCity.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
           
           
           
        }
        private clsBLCity SetBLValues(clsBLCity objCity, clsBLDBConnection objConnection)
        {
            objCity.EnteredBy = clsSharedVariables.UserID; ;
            objCity.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
           
            objCity.CityName = CityName.Text;
            objCity.CityId = "0";
            
            return objCity;
        }
        private void frmAddCity_Load(object sender, EventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            var frm = new frmPatientReg();

            frm.Show();
        }
    }
}
