﻿using OPD_BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OPD
{
    public partial class frmPatientDocuments : Form
    {
        #region "Class variables" and "Properties"

        private string _PatientID = "";
        private string _UserName = "";
        private string _PatientName = "";
        

        private frmOPD MainOPD = null;

        //private string _FileName = "";
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string UserName
        {
            set { _UserName = value; }
            get { return _UserName; }
        }
        public string PatientName
        {
            set { _PatientName = value; }
        }


        #endregion
        public frmPatientDocuments()
        {
            InitializeComponent();
        }
        public frmPatientDocuments(string PatientID)
        {
            InitializeComponent();
            _PatientID = PatientID;
        }
        public frmPatientDocuments(string PatientID, frmOPD Frm)
        {
            InitializeComponent();
            _PatientID = PatientID;
            this.MainOPD = Frm;
        }

        private void frmPatientDocuments_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatient = new clsBLPatientReg(objConnection);
            objPatient.PatientID = _PatientID;
            DataView dvPatient;
            objConnection.Connection_Open();
            dvPatient = objPatient.GetAll(21);

            objConnection.Connection_Close();
            UserName = dvPatient.Table.Rows[0]["UserName"].ToString();
            objComp.ApplyStyleToControls(this);
            GetPatientDocuments();
        }

        private void frmPatientDocuments_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }
        public void FillData(List<DocumentForms> list)
        {
            DocumentGridView.AutoGenerateColumns = false;

         


            DocumentGridView.AllowUserToAddRows = false;
            DocumentGridView.DataSource = list;

            DataGridViewTextBoxColumn column1 = new DataGridViewTextBoxColumn();
            column1.Name = "Date";
            column1.HeaderText = "Date";
            column1.DataPropertyName = "Date";
            column1.Width = 200;
            DocumentGridView.Columns.Add(column1);

            DataGridViewLinkColumn column2 = new DataGridViewLinkColumn();
            column2.Name = "File";
            column2.HeaderText = "File";
            column2.DataPropertyName = "File";
            
            column2.Width = 500;
            DocumentGridView.Columns.Add(column2);

          
        }
        public async Task<string> GetPatientDocuments()
        {
            List<DocumentForms> list = new List<DocumentForms>();
            try
            {

                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync("api/Patient/Get-Document-By-UserName/?userName=" + _UserName);
                    if (response.IsSuccessStatusCode)
                    {
                        list = await response.Content.ReadAsAsync<List<DocumentForms>>();


                    }
                }
                FillData(list);
            }
            catch (Exception ex)
            {
                
            }
            return "true";
        }

        private void DocumentGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                string sUrl = DocumentGridView.Rows[e.RowIndex].Cells[1].Value.ToString();

                ProcessStartInfo sInfo = new ProcessStartInfo(sUrl);

                Process.Start(sInfo);
            }
        }
    }



    public class DocumentForms
    {
      
        public string File { get; set; }
        public string Date { get; set; }
    }
}
