namespace OPD
{
    partial class frmCashrptRange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCashrptRange));
          this.btnOk = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.dtpFrom = new System.Windows.Forms.DateTimePicker();
          this.dtpTo = new System.Windows.Forms.DateTimePicker();
          this.lblTo = new System.Windows.Forms.Label();
          this.lblFrom = new System.Windows.Forms.Label();
          this.lblPeriod = new System.Windows.Forms.Label();
          this.cboConsultant = new System.Windows.Forms.ComboBox();
          this.lblConsultant = new System.Windows.Forms.Label();
          this.SuspendLayout();
          // 
          // btnOk
          // 
          this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
          this.btnOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnOk.Location = new System.Drawing.Point(194, 85);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(60, 60);
          this.btnOk.TabIndex = 0;
          this.btnOk.Text = "OK";
          this.btnOk.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
          this.btnOk.UseVisualStyleBackColor = true;
          this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
          this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnCancel.Location = new System.Drawing.Point(264, 85);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(60, 60);
          this.btnCancel.TabIndex = 1;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
          this.btnCancel.UseVisualStyleBackColor = true;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // dtpFrom
          // 
          this.dtpFrom.CustomFormat = "dd/MM/yyyy";
          this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
          this.dtpFrom.Location = new System.Drawing.Point(107, 54);
          this.dtpFrom.Name = "dtpFrom";
          this.dtpFrom.Size = new System.Drawing.Size(127, 20);
          this.dtpFrom.TabIndex = 2;
          // 
          // dtpTo
          // 
          this.dtpTo.Cursor = System.Windows.Forms.Cursors.Default;
          this.dtpTo.CustomFormat = "dd/MM/yyyy";
          this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
          this.dtpTo.Location = new System.Drawing.Point(337, 54);
          this.dtpTo.Name = "dtpTo";
          this.dtpTo.Size = new System.Drawing.Size(130, 20);
          this.dtpTo.TabIndex = 3;
          // 
          // lblTo
          // 
          this.lblTo.AutoSize = true;
          this.lblTo.Location = new System.Drawing.Point(271, 58);
          this.lblTo.Name = "lblTo";
          this.lblTo.Size = new System.Drawing.Size(29, 13);
          this.lblTo.TabIndex = 4;
          this.lblTo.Text = "To : ";
          // 
          // lblFrom
          // 
          this.lblFrom.AutoSize = true;
          this.lblFrom.Location = new System.Drawing.Point(51, 58);
          this.lblFrom.Name = "lblFrom";
          this.lblFrom.Size = new System.Drawing.Size(39, 13);
          this.lblFrom.TabIndex = 5;
          this.lblFrom.Text = "From : ";
          // 
          // lblPeriod
          // 
          this.lblPeriod.AutoSize = true;
          this.lblPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.lblPeriod.Location = new System.Drawing.Point(39, 109);
          this.lblPeriod.Name = "lblPeriod";
          this.lblPeriod.Size = new System.Drawing.Size(51, 13);
          this.lblPeriod.TabIndex = 6;
          this.lblPeriod.Text = "Period :";
          this.lblPeriod.Visible = false;
          // 
          // cboConsultant
          // 
          this.cboConsultant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
          this.cboConsultant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
          this.cboConsultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cboConsultant.FormattingEnabled = true;
          this.cboConsultant.Location = new System.Drawing.Point(196, 22);
          this.cboConsultant.Name = "cboConsultant";
          this.cboConsultant.Size = new System.Drawing.Size(199, 21);
          this.cboConsultant.TabIndex = 7;
          // 
          // lblConsultant
          // 
          this.lblConsultant.AutoSize = true;
          this.lblConsultant.Location = new System.Drawing.Point(124, 25);
          this.lblConsultant.Name = "lblConsultant";
          this.lblConsultant.Size = new System.Drawing.Size(66, 13);
          this.lblConsultant.TabIndex = 8;
          this.lblConsultant.Text = "Consultant : ";
          // 
          // frmCashrptRange
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(519, 152);
          this.Controls.Add(this.lblConsultant);
          this.Controls.Add(this.cboConsultant);
          this.Controls.Add(this.lblPeriod);
          this.Controls.Add(this.lblFrom);
          this.Controls.Add(this.lblTo);
          this.Controls.Add(this.dtpTo);
          this.Controls.Add(this.dtpFrom);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOk);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.MaximizeBox = false;
          this.Name = "frmCashrptRange";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Date Range";
          this.Load += new System.EventHandler(this.frmCashrptRange_Load);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblPeriod;
      private System.Windows.Forms.ComboBox cboConsultant;
      private System.Windows.Forms.Label lblConsultant;
    }
}