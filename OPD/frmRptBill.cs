using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using OPD_BL;


namespace OPD
{
    public partial class frmRptBill : Form
    {
        #region "Class Variables"

        private string _Consultant = "";
        private string StrReportReference = "";
        private string _OPDID = "";
        private string _From = "";
        private string _To = "";

        #endregion

        #region "Properties"

        public string ReportReference
        {
            set { StrReportReference = value; }
        }

        public string OPDID
        {
            set { _OPDID = value; }
        }

        public string From
        {
            set { _From = value; }
        }
        public string To
        {
            set { _To = value; }
        }

        public string Consultant
        {
            set { _Consultant = value; }
        }
        #endregion

        public frmRptBill()
        {
            InitializeComponent();
        }
        public frmRptBill(frmOPD frm)
        {
            InitializeComponent();
            //ParentForm = frm;
        }

        private void frmRptBill_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            this.Text += " - (" + clsSharedVariables.UserName + ")";
            objComp.ApplyStyleToControls(this);
            LoadReport();

            objComp = null;
        }

        private void LoadReport()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            ReportDocument repdoc = new ReportDocument();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            
            try
            {
                string strPath = Application.StartupPath + "\\Report\\" + StrReportReference + ".rpt";
                repdoc.Load(strPath);

                if (StrReportReference.Equals("OPD_001_03"))
                {
                    repdoc.RecordSelectionFormula = "Date({cm_topd.VisitDate}) = date(currentdatetime())";
                    this.Text = "Today Payment Slip - (" + clsSharedVariables.UserName + ")";
                }
                if (StrReportReference.Equals("OPD_001_04"))
                ////Date({cm_topd.VisitDate}) =currentDate()
                {
                    repdoc.RecordSelectionFormula = "{cm_topd.OPDID}= " + _OPDID;
                    this.Text = "Payment Reciept";
                }
                else if (StrReportReference.Equals("OPD_001_05"))
                {
                    //repdoc.RecordSelectionFormula = "{cm_topd.VisitDate}=CurrentDate and {cm_topd.EnteredBy}=" + clsSharedVariables.UserID;
                    repdoc.RecordSelectionFormula = "{cm_topd.EnteredBy}=" + clsSharedVariables.UserID + " and date({cm_topd.VisitDate}) >= date('" + _From + "') and  date({cm_topd.VisitDate})  <= date('" + _To + "' )";
                    repdoc.Subreports[0].RecordSelectionFormula = "{cm_topd.EnteredBy}=" + clsSharedVariables.UserID + " and date({cm_topd.VisitDate}) >= date('" + _From + "') and  date({cm_topd.VisitDate})  <= date('" + _To + "' )";
                    this.Text = "Dialy Cash Report (" + clsSharedVariables.UserName + ")";
                }
                else if (StrReportReference.Equals("OPD_001_06"))
                {
                    repdoc.RecordSelectionFormula = "{cm_topd.VisitDate}=CurrentDate ";
                    this.Text = "Daily Cash Report";
                }
                else if (StrReportReference.Equals("OPD_001_08"))
                {
                    repdoc.RecordSelectionFormula = "{cm_topdmedicine.OPDID}= " + _OPDID;
                    this.Text = "Medicine Slip";
                }
                else if (StrReportReference.Equals("OPD_001_09"))
                {
                    repdoc.RecordSelectionFormula = "date({cm_topd.VisitDate}) >= date('" + _From + "') and  date({cm_topd.VisitDate})  <= date('" + _To + "' )";//" and 
                    if (!_Consultant.Equals(""))
                    {
                        repdoc.RecordSelectionFormula += " and {cm_topd.EnteredBy}= " + _Consultant;
                    }
                    this.Text = "Detail Pahrmacy Cash Reprot";
                }
                else if (StrReportReference.Equals("OPD_001_10"))
                {
                    repdoc.RecordSelectionFormula = "date({cm_topdmedicine.IssuedOn}) >= date('" + _From + "') and  date({cm_topdmedicine.IssuedOn})  <= date('" + _To + "' )";//" and 
                    if (!_Consultant.Equals(""))
                    {
                        repdoc.RecordSelectionFormula += " and {cm_topd.EnteredBy}= " + _Consultant;
                    }
                    this.Text = "Pahrmacy Cash Reprot";
                }
                else if (StrReportReference.Equals("OPD_001_11"))
                {
                    repdoc.RecordSelectionFormula = "{cm_topdmedicine.Status}='N' and date({cm_topdmedicine.IssuedOn}) >= date('" + _From + "') and  date({cm_topdmedicine.IssuedOn})  <= date('" + _To + "' )";
                    this.Text = "Not available Medicine";
                }

                int j = repdoc.Database.Tables.Count - 1;
              
                for (int i = 0; i <= j; i++)
                {
                    TableLogOnInfo logOnInfo = new TableLogOnInfo();
                    ConnectionInfo connectionInfo = new ConnectionInfo();

                    logOnInfo = repdoc.Database.Tables[i].LogOnInfo;

                    connectionInfo = logOnInfo.ConnectionInfo;
                    connectionInfo.ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();
                    connectionInfo.Password = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
                    connectionInfo.UserID = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
                    repdoc.Database.Tables[i].ApplyLogOnInfo(logOnInfo);
                }
                
                
                
                objRef.ReportRefrence = StrReportReference; 
                try
                {
                    repdoc.SetParameterValue("PersonalName", clsSharedVariables.UserName);
                }
                catch { }

                this.crystalReportViewer1.ReportSource = repdoc;

                //this.crystalReportViewer1.PrintReport();
                //this.Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if(objConnection._objOperation.GetConnection!=null)
                {
                    objConnection.Connection_Close();
                }
              
                repdoc = null;
                objRef = null;
               
                objConnection = null;
                
            }
        }

        private void crystalReportViewer1_ReportRefresh(object source, CrystalDecisions.Windows.Forms.ViewerEventArgs e)
        {
            LoadReport();
        }

        private void crystalReportViewer1_Load(object sender, System.EventArgs e)
        {
            Screen[] scr = Screen.AllScreens;
            Rectangle recScreen = scr[0].WorkingArea;
            Rectangle recForm = new Rectangle(0, 0, recScreen.Width, recScreen.Height);
            this.Bounds = recForm;

            this.crystalReportViewer1.Width = recScreen.Width - 8;
            this.crystalReportViewer1.Height = recScreen.Height - 32;
        }

        private void frmRptBill_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ParentForm != null)
            {
               
                ParentForm.Show();
            }
            this.Dispose();
        }
    }
}