namespace OPD
{
  partial class frmDrugInfo
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDrugInfo));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
        this.panel2 = new System.Windows.Forms.Panel();
        this.lblDrugInfoHeading = new System.Windows.Forms.Label();
        this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        this.miExit = new System.Windows.Forms.ToolStripButton();
        this.panel1 = new System.Windows.Forms.Panel();
        this.pnlExtra = new System.Windows.Forms.Panel();
        this.lblExtraHeading = new System.Windows.Forms.Label();
        this.btnCol = new System.Windows.Forms.Button();
        this.btnExpend = new System.Windows.Forms.Button();
        this.txtOverDosageTreat = new System.Windows.Forms.TextBox();
        this.txtPediatricSafety = new System.Windows.Forms.TextBox();
        this.txtIVAdministration = new System.Windows.Forms.TextBox();
        this.txtMedNotes = new System.Windows.Forms.TextBox();
        this.txtLactationStatus = new System.Windows.Forms.TextBox();
        this.txtDescription = new System.Windows.Forms.TextBox();
        this.txtPatientInfo = new System.Windows.Forms.TextBox();
        this.lblPatientInfo = new System.Windows.Forms.Label();
        this.lblDescription = new System.Windows.Forms.Label();
        this.lblMedNotes = new System.Windows.Forms.Label();
        this.lblLactationStatus = new System.Windows.Forms.Label();
        this.lblOverDosageTreat = new System.Windows.Forms.Label();
        this.lblPediatricSafety = new System.Windows.Forms.Label();
        this.lblIVAdministration = new System.Windows.Forms.Label();
        this.chbRenal = new System.Windows.Forms.CheckBox();
        this.dgManufacturer = new System.Windows.Forms.DataGridView();
        this.ManufacturerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.MSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ManufactName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.txtRoot = new System.Windows.Forms.TextBox();
        this.txtPacking = new System.Windows.Forms.TextBox();
        this.txtGroup = new System.Windows.Forms.TextBox();
        this.txtSystem = new System.Windows.Forms.TextBox();
        this.txtCategory = new System.Windows.Forms.TextBox();
        this.txtUnit = new System.Windows.Forms.TextBox();
        this.txtNature = new System.Windows.Forms.TextBox();
        this.txtQty = new System.Windows.Forms.TextBox();
        this.txtMedName = new System.Windows.Forms.TextBox();
        this.lblRoot = new System.Windows.Forms.Label();
        this.lblManufacturer = new System.Windows.Forms.Label();
        this.lblNature = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.lblGeneric = new System.Windows.Forms.Label();
        this.lblDrugReaction = new System.Windows.Forms.Label();
        this.dgDrugReaction = new System.Windows.Forms.DataGridView();
        this.DRGenericID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DRSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DRGCName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DRDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DRManagement = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dgGeneric = new System.Windows.Forms.DataGridView();
        this.GenericID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.GSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.GenName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.GenQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.GenUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.lblPrecaution = new System.Windows.Forms.Label();
        this.lblAdverseEffect = new System.Windows.Forms.Label();
        this.lblIngredient = new System.Windows.Forms.Label();
        this.txtContraIndication = new System.Windows.Forms.TextBox();
        this.txtPrecaution = new System.Windows.Forms.TextBox();
        this.txtAdverseEffect = new System.Windows.Forms.TextBox();
        this.txtIngredient = new System.Windows.Forms.TextBox();
        this.lblContraIndication = new System.Windows.Forms.Label();
        this.lblSystem = new System.Windows.Forms.Label();
        this.lblUnit = new System.Windows.Forms.Label();
        this.lblQty = new System.Windows.Forms.Label();
        this.lblPacking = new System.Windows.Forms.Label();
        this.lblGroup = new System.Windows.Forms.Label();
        this.lblCategory = new System.Windows.Forms.Label();
        this.lblMedName = new System.Windows.Forms.Label();
        this.panel3 = new System.Windows.Forms.Panel();
        this.label1 = new System.Windows.Forms.Label();
        this.dgAgewiseDosage = new System.Windows.Forms.DataGridView();
        this.MedicineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.MinAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.MaxAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ADUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.panel2.SuspendLayout();
        this.toolStrip1.SuspendLayout();
        this.panel1.SuspendLayout();
        this.pnlExtra.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgManufacturer)).BeginInit();
        this.groupBox1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgDrugReaction)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dgGeneric)).BeginInit();
        this.panel3.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgAgewiseDosage)).BeginInit();
        this.SuspendLayout();
        // 
        // panel2
        // 
        this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel2.Controls.Add(this.lblDrugInfoHeading);
        this.panel2.Controls.Add(this.toolStrip1);
        this.panel2.Location = new System.Drawing.Point(3, 3);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(1011, 53);
        this.panel2.TabIndex = 46;
        this.panel2.Tag = "top";
        // 
        // lblDrugInfoHeading
        // 
        this.lblDrugInfoHeading.AutoSize = true;
        this.lblDrugInfoHeading.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblDrugInfoHeading.Location = new System.Drawing.Point(337, 6);
        this.lblDrugInfoHeading.Name = "lblDrugInfoHeading";
        this.lblDrugInfoHeading.Size = new System.Drawing.Size(334, 38);
        this.lblDrugInfoHeading.TabIndex = 45;
        this.lblDrugInfoHeading.Tag = "no";
        this.lblDrugInfoHeading.Text = "Drug Information";
        // 
        // toolStrip1
        // 
        this.toolStrip1.AutoSize = false;
        this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
        this.toolStrip1.CanOverflow = false;
        this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
        this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
        this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
        this.toolStrip1.Location = new System.Drawing.Point(942, -8);
        this.toolStrip1.Name = "toolStrip1";
        this.toolStrip1.Size = new System.Drawing.Size(86, 66);
        this.toolStrip1.TabIndex = 44;
        this.toolStrip1.Text = "toolStrip1";
        // 
        // miExit
        // 
        this.miExit.AutoSize = false;
        this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
        this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.miExit.ForeColor = System.Drawing.Color.Black;
        this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
        this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        this.miExit.Name = "miExit";
        this.miExit.Size = new System.Drawing.Size(65, 52);
        this.miExit.Text = "&Exit";
        this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
        this.miExit.ToolTipText = "Exit ";
        this.miExit.Click += new System.EventHandler(this.miExit_Click);
        // 
        // panel1
        // 
        this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel1.Controls.Add(this.pnlExtra);
        this.panel1.Controls.Add(this.chbRenal);
        this.panel1.Controls.Add(this.dgManufacturer);
        this.panel1.Controls.Add(this.txtRoot);
        this.panel1.Controls.Add(this.txtPacking);
        this.panel1.Controls.Add(this.txtGroup);
        this.panel1.Controls.Add(this.txtSystem);
        this.panel1.Controls.Add(this.txtCategory);
        this.panel1.Controls.Add(this.txtUnit);
        this.panel1.Controls.Add(this.txtNature);
        this.panel1.Controls.Add(this.txtQty);
        this.panel1.Controls.Add(this.txtMedName);
        this.panel1.Controls.Add(this.lblRoot);
        this.panel1.Controls.Add(this.lblManufacturer);
        this.panel1.Controls.Add(this.lblNature);
        this.panel1.Controls.Add(this.groupBox1);
        this.panel1.Controls.Add(this.lblSystem);
        this.panel1.Controls.Add(this.lblUnit);
        this.panel1.Controls.Add(this.lblQty);
        this.panel1.Controls.Add(this.lblPacking);
        this.panel1.Controls.Add(this.lblGroup);
        this.panel1.Controls.Add(this.lblCategory);
        this.panel1.Controls.Add(this.lblMedName);
        this.panel1.Controls.Add(this.panel3);
        this.panel1.Location = new System.Drawing.Point(3, 58);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(1011, 645);
        this.panel1.TabIndex = 47;
        // 
        // pnlExtra
        // 
        this.pnlExtra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.pnlExtra.Controls.Add(this.lblExtraHeading);
        this.pnlExtra.Controls.Add(this.btnCol);
        this.pnlExtra.Controls.Add(this.btnExpend);
        this.pnlExtra.Controls.Add(this.txtOverDosageTreat);
        this.pnlExtra.Controls.Add(this.txtPediatricSafety);
        this.pnlExtra.Controls.Add(this.txtIVAdministration);
        this.pnlExtra.Controls.Add(this.txtMedNotes);
        this.pnlExtra.Controls.Add(this.txtLactationStatus);
        this.pnlExtra.Controls.Add(this.txtDescription);
        this.pnlExtra.Controls.Add(this.txtPatientInfo);
        this.pnlExtra.Controls.Add(this.lblPatientInfo);
        this.pnlExtra.Controls.Add(this.lblDescription);
        this.pnlExtra.Controls.Add(this.lblMedNotes);
        this.pnlExtra.Controls.Add(this.lblLactationStatus);
        this.pnlExtra.Controls.Add(this.lblOverDosageTreat);
        this.pnlExtra.Controls.Add(this.lblPediatricSafety);
        this.pnlExtra.Controls.Add(this.lblIVAdministration);
        this.pnlExtra.Location = new System.Drawing.Point(702, 181);
        this.pnlExtra.Name = "pnlExtra";
        this.pnlExtra.Size = new System.Drawing.Size(304, 33);
        this.pnlExtra.TabIndex = 94;
        // 
        // lblExtraHeading
        // 
        this.lblExtraHeading.AutoSize = true;
        this.lblExtraHeading.Location = new System.Drawing.Point(97, 9);
        this.lblExtraHeading.Name = "lblExtraHeading";
        this.lblExtraHeading.Size = new System.Drawing.Size(89, 13);
        this.lblExtraHeading.TabIndex = 96;
        this.lblExtraHeading.Tag = "display";
        this.lblExtraHeading.Text = "More Information ";
        // 
        // btnCol
        // 
        this.btnCol.Image = ((System.Drawing.Image)(resources.GetObject("btnCol.Image")));
        this.btnCol.Location = new System.Drawing.Point(269, 0);
        this.btnCol.Name = "btnCol";
        this.btnCol.Size = new System.Drawing.Size(31, 31);
        this.btnCol.TabIndex = 95;
        this.btnCol.UseVisualStyleBackColor = true;
        this.btnCol.Visible = false;
        this.btnCol.Click += new System.EventHandler(this.btnCol_Click);
        // 
        // btnExpend
        // 
        this.btnExpend.Image = ((System.Drawing.Image)(resources.GetObject("btnExpend.Image")));
        this.btnExpend.Location = new System.Drawing.Point(270, 0);
        this.btnExpend.Name = "btnExpend";
        this.btnExpend.Size = new System.Drawing.Size(31, 31);
        this.btnExpend.TabIndex = 94;
        this.btnExpend.Tag = "0";
        this.btnExpend.UseVisualStyleBackColor = true;
        this.btnExpend.Click += new System.EventHandler(this.btnExpend_Click);
        // 
        // txtOverDosageTreat
        // 
        this.txtOverDosageTreat.Location = new System.Drawing.Point(102, 164);
        this.txtOverDosageTreat.Multiline = true;
        this.txtOverDosageTreat.Name = "txtOverDosageTreat";
        this.txtOverDosageTreat.ReadOnly = true;
        this.txtOverDosageTreat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtOverDosageTreat.Size = new System.Drawing.Size(196, 50);
        this.txtOverDosageTreat.TabIndex = 87;
        // 
        // txtPediatricSafety
        // 
        this.txtPediatricSafety.Location = new System.Drawing.Point(102, 219);
        this.txtPediatricSafety.Multiline = true;
        this.txtPediatricSafety.Name = "txtPediatricSafety";
        this.txtPediatricSafety.ReadOnly = true;
        this.txtPediatricSafety.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtPediatricSafety.Size = new System.Drawing.Size(196, 50);
        this.txtPediatricSafety.TabIndex = 86;
        // 
        // txtIVAdministration
        // 
        this.txtIVAdministration.Location = new System.Drawing.Point(102, 109);
        this.txtIVAdministration.Multiline = true;
        this.txtIVAdministration.Name = "txtIVAdministration";
        this.txtIVAdministration.ReadOnly = true;
        this.txtIVAdministration.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtIVAdministration.Size = new System.Drawing.Size(196, 50);
        this.txtIVAdministration.TabIndex = 85;
        // 
        // txtMedNotes
        // 
        this.txtMedNotes.Location = new System.Drawing.Point(102, 382);
        this.txtMedNotes.Multiline = true;
        this.txtMedNotes.Name = "txtMedNotes";
        this.txtMedNotes.ReadOnly = true;
        this.txtMedNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtMedNotes.Size = new System.Drawing.Size(196, 50);
        this.txtMedNotes.TabIndex = 78;
        // 
        // txtLactationStatus
        // 
        this.txtLactationStatus.Location = new System.Drawing.Point(102, 54);
        this.txtLactationStatus.Multiline = true;
        this.txtLactationStatus.Name = "txtLactationStatus";
        this.txtLactationStatus.ReadOnly = true;
        this.txtLactationStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtLactationStatus.Size = new System.Drawing.Size(196, 50);
        this.txtLactationStatus.TabIndex = 84;
        // 
        // txtDescription
        // 
        this.txtDescription.Location = new System.Drawing.Point(102, 328);
        this.txtDescription.Multiline = true;
        this.txtDescription.Name = "txtDescription";
        this.txtDescription.ReadOnly = true;
        this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtDescription.Size = new System.Drawing.Size(196, 50);
        this.txtDescription.TabIndex = 79;
        // 
        // txtPatientInfo
        // 
        this.txtPatientInfo.Location = new System.Drawing.Point(102, 274);
        this.txtPatientInfo.Multiline = true;
        this.txtPatientInfo.Name = "txtPatientInfo";
        this.txtPatientInfo.ReadOnly = true;
        this.txtPatientInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtPatientInfo.Size = new System.Drawing.Size(196, 50);
        this.txtPatientInfo.TabIndex = 80;
        // 
        // lblPatientInfo
        // 
        this.lblPatientInfo.AutoSize = true;
        this.lblPatientInfo.Location = new System.Drawing.Point(2, 277);
        this.lblPatientInfo.Name = "lblPatientInfo";
        this.lblPatientInfo.Size = new System.Drawing.Size(70, 13);
        this.lblPatientInfo.TabIndex = 68;
        this.lblPatientInfo.Text = "Patient Info : ";
        // 
        // lblDescription
        // 
        this.lblDescription.AutoSize = true;
        this.lblDescription.Location = new System.Drawing.Point(2, 331);
        this.lblDescription.Name = "lblDescription";
        this.lblDescription.Size = new System.Drawing.Size(69, 13);
        this.lblDescription.TabIndex = 69;
        this.lblDescription.Text = "Description : ";
        // 
        // lblMedNotes
        // 
        this.lblMedNotes.AutoSize = true;
        this.lblMedNotes.Location = new System.Drawing.Point(2, 385);
        this.lblMedNotes.Name = "lblMedNotes";
        this.lblMedNotes.Size = new System.Drawing.Size(84, 13);
        this.lblMedNotes.TabIndex = 70;
        this.lblMedNotes.Text = "Medical Notes : ";
        // 
        // lblLactationStatus
        // 
        this.lblLactationStatus.AutoSize = true;
        this.lblLactationStatus.Location = new System.Drawing.Point(2, 57);
        this.lblLactationStatus.Name = "lblLactationStatus";
        this.lblLactationStatus.Size = new System.Drawing.Size(93, 13);
        this.lblLactationStatus.TabIndex = 89;
        this.lblLactationStatus.Text = "Lactation Status : ";
        // 
        // lblOverDosageTreat
        // 
        this.lblOverDosageTreat.AutoSize = true;
        this.lblOverDosageTreat.Location = new System.Drawing.Point(2, 222);
        this.lblOverDosageTreat.Name = "lblOverDosageTreat";
        this.lblOverDosageTreat.Size = new System.Drawing.Size(102, 13);
        this.lblOverDosageTreat.TabIndex = 93;
        this.lblOverDosageTreat.Text = "Overdosage Treat : ";
        // 
        // lblPediatricSafety
        // 
        this.lblPediatricSafety.AutoSize = true;
        this.lblPediatricSafety.Location = new System.Drawing.Point(2, 167);
        this.lblPediatricSafety.Name = "lblPediatricSafety";
        this.lblPediatricSafety.Size = new System.Drawing.Size(93, 13);
        this.lblPediatricSafety.TabIndex = 92;
        this.lblPediatricSafety.Text = "Pediatric Safety  : ";
        // 
        // lblIVAdministration
        // 
        this.lblIVAdministration.AutoSize = true;
        this.lblIVAdministration.Location = new System.Drawing.Point(2, 112);
        this.lblIVAdministration.Name = "lblIVAdministration";
        this.lblIVAdministration.Size = new System.Drawing.Size(94, 13);
        this.lblIVAdministration.TabIndex = 90;
        this.lblIVAdministration.Text = "IV Administration : ";
        // 
        // chbRenal
        // 
        this.chbRenal.AutoSize = true;
        this.chbRenal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.chbRenal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.chbRenal.Location = new System.Drawing.Point(528, 70);
        this.chbRenal.Name = "chbRenal";
        this.chbRenal.Size = new System.Drawing.Size(171, 19);
        this.chbRenal.TabIndex = 67;
        this.chbRenal.Text = "Renal Dosage Adjustment";
        this.chbRenal.UseVisualStyleBackColor = true;
        // 
        // dgManufacturer
        // 
        this.dgManufacturer.AllowUserToAddRows = false;
        this.dgManufacturer.AllowUserToDeleteRows = false;
        this.dgManufacturer.AllowUserToResizeColumns = false;
        this.dgManufacturer.AllowUserToResizeRows = false;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgManufacturer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        this.dgManufacturer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgManufacturer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ManufacturerID,
            this.MSNo,
            this.ManufactName});
        this.dgManufacturer.EnableHeadersVisualStyles = false;
        this.dgManufacturer.Location = new System.Drawing.Point(702, 21);
        this.dgManufacturer.MultiSelect = false;
        this.dgManufacturer.Name = "dgManufacturer";
        this.dgManufacturer.ReadOnly = true;
        this.dgManufacturer.RowHeadersVisible = false;
        this.dgManufacturer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgManufacturer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgManufacturer.Size = new System.Drawing.Size(304, 158);
        this.dgManufacturer.TabIndex = 82;
        // 
        // ManufacturerID
        // 
        this.ManufacturerID.DataPropertyName = "ManufacturerID";
        this.ManufacturerID.HeaderText = "ManufacturerID";
        this.ManufacturerID.Name = "ManufacturerID";
        this.ManufacturerID.ReadOnly = true;
        this.ManufacturerID.Visible = false;
        // 
        // MSNo
        // 
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.MSNo.DefaultCellStyle = dataGridViewCellStyle2;
        this.MSNo.HeaderText = "S#";
        this.MSNo.Name = "MSNo";
        this.MSNo.ReadOnly = true;
        this.MSNo.Width = 35;
        // 
        // ManufactName
        // 
        this.ManufactName.DataPropertyName = "MS_Name";
        this.ManufactName.HeaderText = "Manufacturer / Supplier";
        this.ManufactName.Name = "ManufactName";
        this.ManufactName.ReadOnly = true;
        this.ManufactName.Width = 260;
        // 
        // txtRoot
        // 
        this.txtRoot.Location = new System.Drawing.Point(644, 21);
        this.txtRoot.Name = "txtRoot";
        this.txtRoot.ReadOnly = true;
        this.txtRoot.Size = new System.Drawing.Size(55, 20);
        this.txtRoot.TabIndex = 58;
        // 
        // txtPacking
        // 
        this.txtPacking.Location = new System.Drawing.Point(430, 44);
        this.txtPacking.Name = "txtPacking";
        this.txtPacking.ReadOnly = true;
        this.txtPacking.Size = new System.Drawing.Size(46, 20);
        this.txtPacking.TabIndex = 54;
        // 
        // txtGroup
        // 
        this.txtGroup.Location = new System.Drawing.Point(79, 67);
        this.txtGroup.Name = "txtGroup";
        this.txtGroup.ReadOnly = true;
        this.txtGroup.Size = new System.Drawing.Size(397, 20);
        this.txtGroup.TabIndex = 53;
        // 
        // txtSystem
        // 
        this.txtSystem.Location = new System.Drawing.Point(538, 44);
        this.txtSystem.Name = "txtSystem";
        this.txtSystem.ReadOnly = true;
        this.txtSystem.Size = new System.Drawing.Size(161, 20);
        this.txtSystem.TabIndex = 52;
        // 
        // txtCategory
        // 
        this.txtCategory.Location = new System.Drawing.Point(79, 44);
        this.txtCategory.Name = "txtCategory";
        this.txtCategory.ReadOnly = true;
        this.txtCategory.Size = new System.Drawing.Size(262, 20);
        this.txtCategory.TabIndex = 51;
        // 
        // txtUnit
        // 
        this.txtUnit.Location = new System.Drawing.Point(538, 21);
        this.txtUnit.Name = "txtUnit";
        this.txtUnit.ReadOnly = true;
        this.txtUnit.Size = new System.Drawing.Size(58, 20);
        this.txtUnit.TabIndex = 50;
        // 
        // txtNature
        // 
        this.txtNature.Location = new System.Drawing.Point(251, 21);
        this.txtNature.Name = "txtNature";
        this.txtNature.ReadOnly = true;
        this.txtNature.Size = new System.Drawing.Size(90, 20);
        this.txtNature.TabIndex = 49;
        // 
        // txtQty
        // 
        this.txtQty.Location = new System.Drawing.Point(430, 21);
        this.txtQty.Name = "txtQty";
        this.txtQty.ReadOnly = true;
        this.txtQty.Size = new System.Drawing.Size(46, 20);
        this.txtQty.TabIndex = 48;
        // 
        // txtMedName
        // 
        this.txtMedName.Location = new System.Drawing.Point(79, 21);
        this.txtMedName.Name = "txtMedName";
        this.txtMedName.ReadOnly = true;
        this.txtMedName.Size = new System.Drawing.Size(116, 20);
        this.txtMedName.TabIndex = 47;
        // 
        // lblRoot
        // 
        this.lblRoot.AutoSize = true;
        this.lblRoot.Location = new System.Drawing.Point(602, 24);
        this.lblRoot.Name = "lblRoot";
        this.lblRoot.Size = new System.Drawing.Size(36, 13);
        this.lblRoot.TabIndex = 37;
        this.lblRoot.Text = "Root :";
        // 
        // lblManufacturer
        // 
        this.lblManufacturer.AutoSize = true;
        this.lblManufacturer.Location = new System.Drawing.Point(702, 5);
        this.lblManufacturer.Name = "lblManufacturer";
        this.lblManufacturer.Size = new System.Drawing.Size(128, 13);
        this.lblManufacturer.TabIndex = 36;
        this.lblManufacturer.Tag = "display";
        this.lblManufacturer.Text = "Manufacturer / Supplier : ";
        // 
        // lblNature
        // 
        this.lblNature.AutoSize = true;
        this.lblNature.Location = new System.Drawing.Point(201, 24);
        this.lblNature.Name = "lblNature";
        this.lblNature.Size = new System.Drawing.Size(48, 13);
        this.lblNature.TabIndex = 31;
        this.lblNature.Text = "Nature : ";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.lblGeneric);
        this.groupBox1.Controls.Add(this.lblDrugReaction);
        this.groupBox1.Controls.Add(this.dgDrugReaction);
        this.groupBox1.Controls.Add(this.dgGeneric);
        this.groupBox1.Controls.Add(this.lblPrecaution);
        this.groupBox1.Controls.Add(this.lblAdverseEffect);
        this.groupBox1.Controls.Add(this.lblIngredient);
        this.groupBox1.Controls.Add(this.txtContraIndication);
        this.groupBox1.Controls.Add(this.txtPrecaution);
        this.groupBox1.Controls.Add(this.txtAdverseEffect);
        this.groupBox1.Controls.Add(this.txtIngredient);
        this.groupBox1.Controls.Add(this.lblContraIndication);
        this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        this.groupBox1.Location = new System.Drawing.Point(3, 93);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(696, 547);
        this.groupBox1.TabIndex = 83;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "Generic Information";
        // 
        // lblGeneric
        // 
        this.lblGeneric.AutoSize = true;
        this.lblGeneric.Location = new System.Drawing.Point(11, 31);
        this.lblGeneric.Name = "lblGeneric";
        this.lblGeneric.Size = new System.Drawing.Size(74, 13);
        this.lblGeneric.TabIndex = 86;
        this.lblGeneric.Text = "Generic Info : ";
        // 
        // lblDrugReaction
        // 
        this.lblDrugReaction.AutoSize = true;
        this.lblDrugReaction.Location = new System.Drawing.Point(11, 315);
        this.lblDrugReaction.Name = "lblDrugReaction";
        this.lblDrugReaction.Size = new System.Drawing.Size(85, 13);
        this.lblDrugReaction.TabIndex = 85;
        this.lblDrugReaction.Tag = "trans";
        this.lblDrugReaction.Text = "Drug Reaction : ";
        // 
        // dgDrugReaction
        // 
        this.dgDrugReaction.AllowDrop = true;
        this.dgDrugReaction.AllowUserToAddRows = false;
        this.dgDrugReaction.AllowUserToDeleteRows = false;
        this.dgDrugReaction.AllowUserToOrderColumns = true;
        this.dgDrugReaction.AllowUserToResizeColumns = false;
        this.dgDrugReaction.AllowUserToResizeRows = false;
        this.dgDrugReaction.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgDrugReaction.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
        this.dgDrugReaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgDrugReaction.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DRGenericID,
            this.DRSNo,
            this.DRGCName,
            this.DRDescription,
            this.DRManagement});
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgDrugReaction.DefaultCellStyle = dataGridViewCellStyle9;
        this.dgDrugReaction.EnableHeadersVisualStyles = false;
        this.dgDrugReaction.Location = new System.Drawing.Point(106, 306);
        this.dgDrugReaction.MultiSelect = false;
        this.dgDrugReaction.Name = "dgDrugReaction";
        this.dgDrugReaction.ReadOnly = true;
        this.dgDrugReaction.RowHeadersVisible = false;
        this.dgDrugReaction.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
        this.dgDrugReaction.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.dgDrugReaction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgDrugReaction.Size = new System.Drawing.Size(584, 237);
        this.dgDrugReaction.TabIndex = 84;
        // 
        // DRGenericID
        // 
        this.DRGenericID.DataPropertyName = "genericID";
        dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.DRGenericID.DefaultCellStyle = dataGridViewCellStyle4;
        this.DRGenericID.HeaderText = "GenericID";
        this.DRGenericID.Name = "DRGenericID";
        this.DRGenericID.ReadOnly = true;
        this.DRGenericID.Visible = false;
        // 
        // DRSNo
        // 
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.DRSNo.DefaultCellStyle = dataGridViewCellStyle5;
        this.DRSNo.HeaderText = "S#";
        this.DRSNo.Name = "DRSNo";
        this.DRSNo.ReadOnly = true;
        this.DRSNo.Width = 35;
        // 
        // DRGCName
        // 
        this.DRGCName.DataPropertyName = "GCName";
        dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.DRGCName.DefaultCellStyle = dataGridViewCellStyle6;
        this.DRGCName.HeaderText = "Generic Name";
        this.DRGCName.Name = "DRGCName";
        this.DRGCName.ReadOnly = true;
        this.DRGCName.Width = 110;
        // 
        // DRDescription
        // 
        this.DRDescription.DataPropertyName = "Description";
        dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.DRDescription.DefaultCellStyle = dataGridViewCellStyle7;
        this.DRDescription.HeaderText = "Description";
        this.DRDescription.Name = "DRDescription";
        this.DRDescription.ReadOnly = true;
        this.DRDescription.Width = 245;
        // 
        // DRManagement
        // 
        this.DRManagement.DataPropertyName = "Management";
        dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.DRManagement.DefaultCellStyle = dataGridViewCellStyle8;
        this.DRManagement.HeaderText = "Management";
        this.DRManagement.Name = "DRManagement";
        this.DRManagement.ReadOnly = true;
        this.DRManagement.Width = 170;
        // 
        // dgGeneric
        // 
        this.dgGeneric.AllowUserToAddRows = false;
        this.dgGeneric.AllowUserToDeleteRows = false;
        this.dgGeneric.AllowUserToResizeColumns = false;
        this.dgGeneric.AllowUserToResizeRows = false;
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgGeneric.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
        this.dgGeneric.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgGeneric.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GenericID,
            this.GSNo,
            this.GenName,
            this.GenQty,
            this.GenUnit});
        this.dgGeneric.EnableHeadersVisualStyles = false;
        this.dgGeneric.Location = new System.Drawing.Point(106, 11);
        this.dgGeneric.MultiSelect = false;
        this.dgGeneric.Name = "dgGeneric";
        this.dgGeneric.ReadOnly = true;
        this.dgGeneric.RowHeadersVisible = false;
        this.dgGeneric.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgGeneric.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgGeneric.Size = new System.Drawing.Size(584, 77);
        this.dgGeneric.TabIndex = 57;
        this.dgGeneric.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgGeneric_CellClick);
        // 
        // GenericID
        // 
        this.GenericID.DataPropertyName = "GenericID";
        this.GenericID.HeaderText = "GenericID";
        this.GenericID.Name = "GenericID";
        this.GenericID.ReadOnly = true;
        this.GenericID.Visible = false;
        // 
        // GSNo
        // 
        dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.GSNo.DefaultCellStyle = dataGridViewCellStyle11;
        this.GSNo.HeaderText = "S #";
        this.GSNo.Name = "GSNo";
        this.GSNo.ReadOnly = true;
        this.GSNo.Width = 50;
        // 
        // GenName
        // 
        this.GenName.DataPropertyName = "GCName";
        this.GenName.HeaderText = "Generic Name";
        this.GenName.Name = "GenName";
        this.GenName.ReadOnly = true;
        this.GenName.Width = 287;
        // 
        // GenQty
        // 
        this.GenQty.DataPropertyName = "Generic_Qty";
        dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.GenQty.DefaultCellStyle = dataGridViewCellStyle12;
        this.GenQty.HeaderText = "Quantity";
        this.GenQty.Name = "GenQty";
        this.GenQty.ReadOnly = true;
        this.GenQty.Width = 120;
        // 
        // GenUnit
        // 
        this.GenUnit.DataPropertyName = "UnitName";
        dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.GenUnit.DefaultCellStyle = dataGridViewCellStyle13;
        this.GenUnit.HeaderText = "Unit";
        this.GenUnit.Name = "GenUnit";
        this.GenUnit.ReadOnly = true;
        this.GenUnit.Width = 120;
        // 
        // lblPrecaution
        // 
        this.lblPrecaution.AutoSize = true;
        this.lblPrecaution.Location = new System.Drawing.Point(11, 152);
        this.lblPrecaution.Name = "lblPrecaution";
        this.lblPrecaution.Size = new System.Drawing.Size(67, 13);
        this.lblPrecaution.TabIndex = 62;
        this.lblPrecaution.Tag = "trans";
        this.lblPrecaution.Text = "Precaution : ";
        // 
        // lblAdverseEffect
        // 
        this.lblAdverseEffect.AutoSize = true;
        this.lblAdverseEffect.Location = new System.Drawing.Point(11, 205);
        this.lblAdverseEffect.Name = "lblAdverseEffect";
        this.lblAdverseEffect.Size = new System.Drawing.Size(86, 13);
        this.lblAdverseEffect.TabIndex = 63;
        this.lblAdverseEffect.Tag = "trans";
        this.lblAdverseEffect.Text = "Adverse Effect : ";
        // 
        // lblIngredient
        // 
        this.lblIngredient.AutoSize = true;
        this.lblIngredient.Location = new System.Drawing.Point(11, 257);
        this.lblIngredient.Name = "lblIngredient";
        this.lblIngredient.Size = new System.Drawing.Size(68, 13);
        this.lblIngredient.TabIndex = 65;
        this.lblIngredient.Tag = "trans";
        this.lblIngredient.Text = "Ingredients : ";
        // 
        // txtContraIndication
        // 
        this.txtContraIndication.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.txtContraIndication.Location = new System.Drawing.Point(106, 92);
        this.txtContraIndication.Multiline = true;
        this.txtContraIndication.Name = "txtContraIndication";
        this.txtContraIndication.ReadOnly = true;
        this.txtContraIndication.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtContraIndication.Size = new System.Drawing.Size(584, 50);
        this.txtContraIndication.TabIndex = 60;
        this.txtContraIndication.Tag = "no";
        // 
        // txtPrecaution
        // 
        this.txtPrecaution.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.txtPrecaution.Location = new System.Drawing.Point(106, 145);
        this.txtPrecaution.Multiline = true;
        this.txtPrecaution.Name = "txtPrecaution";
        this.txtPrecaution.ReadOnly = true;
        this.txtPrecaution.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtPrecaution.Size = new System.Drawing.Size(584, 50);
        this.txtPrecaution.TabIndex = 61;
        this.txtPrecaution.Tag = "no";
        // 
        // txtAdverseEffect
        // 
        this.txtAdverseEffect.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.txtAdverseEffect.Location = new System.Drawing.Point(106, 198);
        this.txtAdverseEffect.Multiline = true;
        this.txtAdverseEffect.Name = "txtAdverseEffect";
        this.txtAdverseEffect.ReadOnly = true;
        this.txtAdverseEffect.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtAdverseEffect.Size = new System.Drawing.Size(584, 50);
        this.txtAdverseEffect.TabIndex = 64;
        this.txtAdverseEffect.Tag = "no";
        // 
        // txtIngredient
        // 
        this.txtIngredient.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.txtIngredient.Location = new System.Drawing.Point(106, 250);
        this.txtIngredient.Multiline = true;
        this.txtIngredient.Name = "txtIngredient";
        this.txtIngredient.ReadOnly = true;
        this.txtIngredient.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtIngredient.Size = new System.Drawing.Size(584, 50);
        this.txtIngredient.TabIndex = 66;
        this.txtIngredient.Tag = "no";
        // 
        // lblContraIndication
        // 
        this.lblContraIndication.AutoSize = true;
        this.lblContraIndication.Location = new System.Drawing.Point(11, 99);
        this.lblContraIndication.Name = "lblContraIndication";
        this.lblContraIndication.Size = new System.Drawing.Size(99, 13);
        this.lblContraIndication.TabIndex = 59;
        this.lblContraIndication.Tag = "trans";
        this.lblContraIndication.Text = "Contra Indication :  ";
        // 
        // lblSystem
        // 
        this.lblSystem.AutoSize = true;
        this.lblSystem.Location = new System.Drawing.Point(482, 47);
        this.lblSystem.Name = "lblSystem";
        this.lblSystem.Size = new System.Drawing.Size(53, 13);
        this.lblSystem.TabIndex = 33;
        this.lblSystem.Text = "System  : ";
        // 
        // lblUnit
        // 
        this.lblUnit.AutoSize = true;
        this.lblUnit.Location = new System.Drawing.Point(482, 24);
        this.lblUnit.Name = "lblUnit";
        this.lblUnit.Size = new System.Drawing.Size(35, 13);
        this.lblUnit.TabIndex = 32;
        this.lblUnit.Text = "Unit : ";
        // 
        // lblQty
        // 
        this.lblQty.AutoSize = true;
        this.lblQty.Location = new System.Drawing.Point(347, 24);
        this.lblQty.Name = "lblQty";
        this.lblQty.Size = new System.Drawing.Size(86, 13);
        this.lblQty.TabIndex = 30;
        this.lblQty.Text = "Brand Quantity : ";
        // 
        // lblPacking
        // 
        this.lblPacking.AutoSize = true;
        this.lblPacking.Location = new System.Drawing.Point(378, 47);
        this.lblPacking.Name = "lblPacking";
        this.lblPacking.Size = new System.Drawing.Size(55, 13);
        this.lblPacking.TabIndex = 35;
        this.lblPacking.Text = "Packing : ";
        // 
        // lblGroup
        // 
        this.lblGroup.AutoSize = true;
        this.lblGroup.Location = new System.Drawing.Point(11, 70);
        this.lblGroup.Name = "lblGroup";
        this.lblGroup.Size = new System.Drawing.Size(45, 13);
        this.lblGroup.TabIndex = 43;
        this.lblGroup.Text = "Group : ";
        // 
        // lblCategory
        // 
        this.lblCategory.AutoSize = true;
        this.lblCategory.Location = new System.Drawing.Point(11, 47);
        this.lblCategory.Name = "lblCategory";
        this.lblCategory.Size = new System.Drawing.Size(58, 13);
        this.lblCategory.TabIndex = 34;
        this.lblCategory.Text = "Category : ";
        // 
        // lblMedName
        // 
        this.lblMedName.AutoSize = true;
        this.lblMedName.Location = new System.Drawing.Point(11, 24);
        this.lblMedName.Name = "lblMedName";
        this.lblMedName.Size = new System.Drawing.Size(44, 13);
        this.lblMedName.TabIndex = 29;
        this.lblMedName.Text = "Name : ";
        // 
        // panel3
        // 
        this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel3.Controls.Add(this.label1);
        this.panel3.Controls.Add(this.dgAgewiseDosage);
        this.panel3.Location = new System.Drawing.Point(702, 215);
        this.panel3.Name = "panel3";
        this.panel3.Size = new System.Drawing.Size(304, 425);
        this.panel3.TabIndex = 96;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(97, 4);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(89, 13);
        this.label1.TabIndex = 96;
        this.label1.Tag = "display";
        this.label1.Text = "More Information ";
        // 
        // dgAgewiseDosage
        // 
        this.dgAgewiseDosage.AllowUserToAddRows = false;
        this.dgAgewiseDosage.AllowUserToDeleteRows = false;
        this.dgAgewiseDosage.AllowUserToOrderColumns = true;
        this.dgAgewiseDosage.AllowUserToResizeColumns = false;
        this.dgAgewiseDosage.AllowUserToResizeRows = false;
        this.dgAgewiseDosage.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgAgewiseDosage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
        this.dgAgewiseDosage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgAgewiseDosage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MedicineID,
            this.MinAge,
            this.MaxAge,
            this.Weight,
            this.ADUnit});
        this.dgAgewiseDosage.Location = new System.Drawing.Point(1, 25);
        this.dgAgewiseDosage.MultiSelect = false;
        this.dgAgewiseDosage.Name = "dgAgewiseDosage";
        this.dgAgewiseDosage.ReadOnly = true;
        this.dgAgewiseDosage.RowHeadersVisible = false;
        this.dgAgewiseDosage.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgAgewiseDosage.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.dgAgewiseDosage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgAgewiseDosage.Size = new System.Drawing.Size(300, 397);
        this.dgAgewiseDosage.TabIndex = 95;
        // 
        // MedicineID
        // 
        this.MedicineID.DataPropertyName = "MedicineID";
        this.MedicineID.HeaderText = "ADMedicineID";
        this.MedicineID.Name = "MedicineID";
        this.MedicineID.ReadOnly = true;
        this.MedicineID.Visible = false;
        // 
        // MinAge
        // 
        this.MinAge.DataPropertyName = "MinAge";
        this.MinAge.FillWeight = 25F;
        this.MinAge.HeaderText = "Min Age";
        this.MinAge.Name = "MinAge";
        this.MinAge.ReadOnly = true;
        // 
        // MaxAge
        // 
        this.MaxAge.DataPropertyName = "MaxAge";
        this.MaxAge.FillWeight = 25F;
        this.MaxAge.HeaderText = "Max Age";
        this.MaxAge.Name = "MaxAge";
        this.MaxAge.ReadOnly = true;
        // 
        // Weight
        // 
        this.Weight.DataPropertyName = "Weight";
        this.Weight.FillWeight = 25F;
        this.Weight.HeaderText = "Weight";
        this.Weight.Name = "Weight";
        this.Weight.ReadOnly = true;
        // 
        // ADUnit
        // 
        this.ADUnit.DataPropertyName = "UnitName";
        this.ADUnit.FillWeight = 25F;
        this.ADUnit.HeaderText = "Unit";
        this.ADUnit.Name = "ADUnit";
        this.ADUnit.ReadOnly = true;
        // 
        // frmDrugInfo
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(1017, 706);
        this.Controls.Add(this.panel1);
        this.Controls.Add(this.panel2);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        this.MaximizeBox = false;
        this.Name = "frmDrugInfo";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Drug Information";
        this.Load += new System.EventHandler(this.frmDrugInfo_Load);
        this.panel2.ResumeLayout(false);
        this.panel2.PerformLayout();
        this.toolStrip1.ResumeLayout(false);
        this.toolStrip1.PerformLayout();
        this.panel1.ResumeLayout(false);
        this.panel1.PerformLayout();
        this.pnlExtra.ResumeLayout(false);
        this.pnlExtra.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgManufacturer)).EndInit();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgDrugReaction)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dgGeneric)).EndInit();
        this.panel3.ResumeLayout(false);
        this.panel3.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dgAgewiseDosage)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton miExit;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.TextBox txtPacking;
    private System.Windows.Forms.TextBox txtGroup;
    private System.Windows.Forms.TextBox txtSystem;
    private System.Windows.Forms.TextBox txtCategory;
    private System.Windows.Forms.TextBox txtUnit;
    private System.Windows.Forms.TextBox txtNature;
    private System.Windows.Forms.TextBox txtQty;
    private System.Windows.Forms.TextBox txtMedName;
    private System.Windows.Forms.Label lblGroup;
    private System.Windows.Forms.Label lblRoot;
    private System.Windows.Forms.Label lblManufacturer;
    private System.Windows.Forms.Label lblPacking;
    private System.Windows.Forms.Label lblCategory;
    private System.Windows.Forms.Label lblSystem;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblNature;
    private System.Windows.Forms.Label lblQty;
    private System.Windows.Forms.Label lblMedName;
    private System.Windows.Forms.TextBox txtRoot;
    private System.Windows.Forms.CheckBox chbRenal;
    private System.Windows.Forms.TextBox txtPatientInfo;
    private System.Windows.Forms.TextBox txtDescription;
    private System.Windows.Forms.TextBox txtMedNotes;
    private System.Windows.Forms.Label lblMedNotes;
    private System.Windows.Forms.Label lblDescription;
    private System.Windows.Forms.Label lblPatientInfo;
    private System.Windows.Forms.DataGridView dgManufacturer;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.DataGridView dgGeneric;
    private System.Windows.Forms.Label lblPrecaution;
    private System.Windows.Forms.Label lblAdverseEffect;
    private System.Windows.Forms.Label lblIngredient;
    private System.Windows.Forms.TextBox txtContraIndication;
    private System.Windows.Forms.TextBox txtPrecaution;
    private System.Windows.Forms.TextBox txtAdverseEffect;
    private System.Windows.Forms.TextBox txtIngredient;
    private System.Windows.Forms.Label lblContraIndication;
    private System.Windows.Forms.DataGridView dgDrugReaction;
    private System.Windows.Forms.Label lblDrugReaction;
    private System.Windows.Forms.TextBox txtOverDosageTreat;
    private System.Windows.Forms.TextBox txtPediatricSafety;
    private System.Windows.Forms.TextBox txtIVAdministration;
    private System.Windows.Forms.TextBox txtLactationStatus;
    private System.Windows.Forms.Label lblOverDosageTreat;
    private System.Windows.Forms.Label lblPediatricSafety;
    private System.Windows.Forms.Label lblIVAdministration;
    private System.Windows.Forms.Label lblLactationStatus;
    private System.Windows.Forms.Panel pnlExtra;
    private System.Windows.Forms.DataGridViewTextBoxColumn ManufacturerID;
    private System.Windows.Forms.DataGridViewTextBoxColumn MSNo;
    private System.Windows.Forms.DataGridViewTextBoxColumn ManufactName;
    private System.Windows.Forms.Button btnExpend;
    private System.Windows.Forms.Button btnCol;
    private System.Windows.Forms.Label lblExtraHeading;
    private System.Windows.Forms.DataGridViewTextBoxColumn GenericID;
    private System.Windows.Forms.DataGridViewTextBoxColumn GSNo;
    private System.Windows.Forms.DataGridViewTextBoxColumn GenName;
    private System.Windows.Forms.DataGridViewTextBoxColumn GenQty;
    private System.Windows.Forms.DataGridViewTextBoxColumn GenUnit;
    private System.Windows.Forms.Label lblGeneric;
    private System.Windows.Forms.Label lblDrugInfoHeading;
    private System.Windows.Forms.DataGridViewTextBoxColumn DRGenericID;
    private System.Windows.Forms.DataGridViewTextBoxColumn DRSNo;
    private System.Windows.Forms.DataGridViewTextBoxColumn DRGCName;
    private System.Windows.Forms.DataGridViewTextBoxColumn DRDescription;
    private System.Windows.Forms.DataGridViewTextBoxColumn DRManagement;
      private System.Windows.Forms.DataGridView dgAgewiseDosage;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.DataGridViewTextBoxColumn MedicineID;
      private System.Windows.Forms.DataGridViewTextBoxColumn MinAge;
      private System.Windows.Forms.DataGridViewTextBoxColumn MaxAge;
      private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
      private System.Windows.Forms.DataGridViewTextBoxColumn ADUnit;
  }
}