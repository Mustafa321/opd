namespace OPD
{
    partial class frmInvestigation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInvestigation));
            this.dgLeftInvestigation = new System.Windows.Forms.DataGridView();
            this.dgRightInvestigation = new System.Windows.Forms.DataGridView();
            this.InvestigationNameS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDInvestigationS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.InvestigationNameP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDInvestigation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgLeftInvestigation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRightInvestigation)).BeginInit();
            this.SuspendLayout();
            // 
            // dgLeftInvestigation
            // 
            this.dgLeftInvestigation.AllowUserToResizeColumns = false;
            this.dgLeftInvestigation.AllowUserToResizeRows = false;
            this.dgLeftInvestigation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgLeftInvestigation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgLeftInvestigation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLeftInvestigation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvestigationNameP,
            this.PrefIDInvestigation});
            this.dgLeftInvestigation.Location = new System.Drawing.Point(7, 39);
            this.dgLeftInvestigation.Name = "dgLeftInvestigation";
            this.dgLeftInvestigation.RowHeadersVisible = false;
            this.dgLeftInvestigation.Size = new System.Drawing.Size(253, 430);
            this.dgLeftInvestigation.TabIndex = 0;
            // 
            // dgRightInvestigation
            // 
            this.dgRightInvestigation.AllowUserToOrderColumns = true;
            this.dgRightInvestigation.AllowUserToResizeColumns = false;
            this.dgRightInvestigation.AllowUserToResizeRows = false;
            this.dgRightInvestigation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRightInvestigation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgRightInvestigation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRightInvestigation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvestigationNameS,
            this.PrefIDInvestigationS});
            this.dgRightInvestigation.Location = new System.Drawing.Point(313, 39);
            this.dgRightInvestigation.Name = "dgRightInvestigation";
            this.dgRightInvestigation.RowHeadersVisible = false;
            this.dgRightInvestigation.Size = new System.Drawing.Size(253, 430);
            this.dgRightInvestigation.TabIndex = 1;
            // 
            // InvestigationNameS
            // 
            this.InvestigationNameS.HeaderText = "Selected List";
            this.InvestigationNameS.Name = "InvestigationNameS";
            // 
            // PrefIDInvestigationS
            // 
            this.PrefIDInvestigationS.HeaderText = "PrefIDInvestigation";
            this.PrefIDInvestigationS.Name = "PrefIDInvestigationS";
            this.PrefIDInvestigationS.ReadOnly = true;
            this.PrefIDInvestigationS.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Location = new System.Drawing.Point(536, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(30, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Location = new System.Drawing.Point(500, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(30, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnRight
            // 
            this.btnRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRight.BackgroundImage")));
            this.btnRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRight.Location = new System.Drawing.Point(265, 130);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(42, 46);
            this.btnRight.TabIndex = 4;
            this.btnRight.UseVisualStyleBackColor = true;
            // 
            // btnLeft
            // 
            this.btnLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLeft.BackgroundImage")));
            this.btnLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLeft.Location = new System.Drawing.Point(266, 262);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(42, 46);
            this.btnLeft.TabIndex = 5;
            this.btnLeft.UseVisualStyleBackColor = true;
            // 
            // InvestigationNameP
            // 
            this.InvestigationNameP.DataPropertyName = "Name";
            this.InvestigationNameP.HeaderText = "Preferred list";
            this.InvestigationNameP.Name = "InvestigationNameP";
            // 
            // PrefIDInvestigation
            // 
            this.PrefIDInvestigation.DataPropertyName = "PrefID";
            this.PrefIDInvestigation.HeaderText = "PrefID";
            this.PrefIDInvestigation.Name = "PrefIDInvestigation";
            this.PrefIDInvestigation.ReadOnly = true;
            this.PrefIDInvestigation.Visible = false;
            // 
            // frmInvestigation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 473);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.dgRightInvestigation);
            this.Controls.Add(this.dgLeftInvestigation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmInvestigation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Investigation";
            this.Load += new System.EventHandler(this.frmInvestigation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgLeftInvestigation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRightInvestigation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgLeftInvestigation;
        private System.Windows.Forms.DataGridView dgRightInvestigation;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvestigationNameS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDInvestigationS;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvestigationNameP;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDInvestigation;
    }
}