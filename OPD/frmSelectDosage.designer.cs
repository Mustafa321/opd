namespace OPD
{
    partial class frmSelectDosage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectDosage));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFinalDosage = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAddDosageInstruction = new System.Windows.Forms.Button();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.cboDayCount = new System.Windows.Forms.ComboBox();
            this.cboInstruction = new System.Windows.Forms.ComboBox();
            this.cboQunatity = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblSpeedSelection = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnSpeedSeletion8 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion7 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion6 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion5 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion4 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion3 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion2 = new System.Windows.Forms.Button();
            this.btnSpeedSeletion1 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblMedicineName = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chbEvening = new System.Windows.Forms.CheckBox();
            this.chbNoon = new System.Windows.Forms.CheckBox();
            this.chbNight = new System.Windows.Forms.CheckBox();
            this.chbMorning = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblFinalDosage);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Location = new System.Drawing.Point(2, 473);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(436, 121);
            this.panel1.TabIndex = 0;
            // 
            // lblFinalDosage
            // 
            this.lblFinalDosage.AutoSize = true;
            this.lblFinalDosage.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalDosage.Location = new System.Drawing.Point(11, 7);
            this.lblFinalDosage.Name = "lblFinalDosage";
            this.lblFinalDosage.Size = new System.Drawing.Size(0, 34);
            this.lblFinalDosage.TabIndex = 5;
            this.lblFinalDosage.Tag = "urdu";
            this.lblFinalDosage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.lblFinalDosage, "Selected Dosage");
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnOK.Location = new System.Drawing.Point(288, 56);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(60, 60);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "&OK";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnOK, "Ok for seleted Dosage");
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancel.Location = new System.Drawing.Point(354, 56);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(60, 60);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnCancel, "Reject Seleted Dosage");
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnAddDosageInstruction);
            this.panel2.Controls.Add(this.cboType);
            this.panel2.Controls.Add(this.cboDayCount);
            this.panel2.Controls.Add(this.cboInstruction);
            this.panel2.Controls.Add(this.cboQunatity);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(285, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(153, 401);
            this.panel2.TabIndex = 1;
            // 
            // btnAddDosageInstruction
            // 
            this.btnAddDosageInstruction.Location = new System.Drawing.Point(7, 367);
            this.btnAddDosageInstruction.Name = "btnAddDosageInstruction";
            this.btnAddDosageInstruction.Size = new System.Drawing.Size(137, 27);
            this.btnAddDosageInstruction.TabIndex = 8;
            this.btnAddDosageInstruction.Tag = "notset";
            this.btnAddDosageInstruction.Text = "Doasge Instruction";
            this.btnAddDosageInstruction.UseVisualStyleBackColor = true;
            this.btnAddDosageInstruction.Click += new System.EventHandler(this.btnAddDosageInstruction_Click);
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(7, 55);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(137, 48);
            this.cboType.TabIndex = 6;
            this.cboType.Tag = "urdu";
            this.toolTip1.SetToolTip(this.cboType, "Dosage Time");
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // cboDayCount
            // 
            this.cboDayCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDayCount.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDayCount.FormattingEnabled = true;
            this.cboDayCount.Location = new System.Drawing.Point(7, 264);
            this.cboDayCount.Name = "cboDayCount";
            this.cboDayCount.Size = new System.Drawing.Size(137, 48);
            this.cboDayCount.TabIndex = 1;
            this.cboDayCount.Tag = "urdu";
            this.toolTip1.SetToolTip(this.cboDayCount, "Number of Days for Medicine Dosage");
            this.cboDayCount.SelectedIndexChanged += new System.EventHandler(this.cboDayCount_SelectedIndexChanged);
            // 
            // cboInstruction
            // 
            this.cboInstruction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInstruction.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInstruction.FormattingEnabled = true;
            this.cboInstruction.Location = new System.Drawing.Point(7, 315);
            this.cboInstruction.Name = "cboInstruction";
            this.cboInstruction.Size = new System.Drawing.Size(137, 48);
            this.cboInstruction.TabIndex = 2;
            this.cboInstruction.Tag = "urdu";
            this.toolTip1.SetToolTip(this.cboInstruction, "Instruction Related to Medicine Dosage");
            this.cboInstruction.SelectedIndexChanged += new System.EventHandler(this.cboInstruction_SelectedIndexChanged);
            // 
            // cboQunatity
            // 
            this.cboQunatity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQunatity.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboQunatity.FormattingEnabled = true;
            this.cboQunatity.Location = new System.Drawing.Point(7, 4);
            this.cboQunatity.Name = "cboQunatity";
            this.cboQunatity.Size = new System.Drawing.Size(137, 48);
            this.cboQunatity.TabIndex = 0;
            this.cboQunatity.Tag = "urdu";
            this.toolTip1.SetToolTip(this.cboQunatity, "Medicine Quantity");
            this.cboQunatity.SelectedIndexChanged += new System.EventHandler(this.cboQunatity_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.lblSpeedSelection);
            this.panel4.Location = new System.Drawing.Point(2, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(436, 30);
            this.panel4.TabIndex = 3;
            this.panel4.Tag = "med";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(338, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 1;
            this.label2.Tag = "display";
            this.label2.Text = "Dosage";
            // 
            // lblSpeedSelection
            // 
            this.lblSpeedSelection.AutoSize = true;
            this.lblSpeedSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeedSelection.Location = new System.Drawing.Point(114, 4);
            this.lblSpeedSelection.Name = "lblSpeedSelection";
            this.lblSpeedSelection.Size = new System.Drawing.Size(141, 20);
            this.lblSpeedSelection.TabIndex = 0;
            this.lblSpeedSelection.Tag = "display";
            this.lblSpeedSelection.Text = "Speed Selection";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnSpeedSeletion8);
            this.panel5.Controls.Add(this.btnSpeedSeletion7);
            this.panel5.Controls.Add(this.btnSpeedSeletion6);
            this.panel5.Controls.Add(this.btnSpeedSeletion5);
            this.panel5.Controls.Add(this.btnSpeedSeletion4);
            this.panel5.Controls.Add(this.btnSpeedSeletion3);
            this.panel5.Controls.Add(this.btnSpeedSeletion2);
            this.panel5.Controls.Add(this.btnSpeedSeletion1);
            this.panel5.Location = new System.Drawing.Point(2, 71);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(279, 401);
            this.panel5.TabIndex = 4;
            // 
            // btnSpeedSeletion8
            // 
            this.btnSpeedSeletion8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion8.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion8.Location = new System.Drawing.Point(6, 348);
            this.btnSpeedSeletion8.Name = "btnSpeedSeletion8";
            this.btnSpeedSeletion8.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion8.TabIndex = 7;
            this.btnSpeedSeletion8.Tag = "urdu";
            this.btnSpeedSeletion8.Text = "�Bq j��e \\Ju ��n�η �e";
            this.btnSpeedSeletion8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion8, "Speed Dosage Selection");
            this.btnSpeedSeletion8.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion8.Click += new System.EventHandler(this.btnSpeedSeletion8_Click);
            // 
            // btnSpeedSeletion7
            // 
            this.btnSpeedSeletion7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion7.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion7.Location = new System.Drawing.Point(6, 299);
            this.btnSpeedSeletion7.Name = "btnSpeedSeletion7";
            this.btnSpeedSeletion7.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion7.TabIndex = 6;
            this.btnSpeedSeletion7.Tag = "urdu";
            this.btnSpeedSeletion7.Text = "�Bq j��e \\Ju ��n�η ��A";
            this.btnSpeedSeletion7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion7, "Speed Dosage Selection");
            this.btnSpeedSeletion7.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion7.Click += new System.EventHandler(this.btnSpeedSeletion7_Click);
            // 
            // btnSpeedSeletion6
            // 
            this.btnSpeedSeletion6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion6.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion6.Location = new System.Drawing.Point(6, 250);
            this.btnSpeedSeletion6.Name = "btnSpeedSeletion6";
            this.btnSpeedSeletion6.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion6.TabIndex = 5;
            this.btnSpeedSeletion6.Tag = "urdu";
            this.btnSpeedSeletion6.Text = "�Bq j��e \\Ju ��� �e";
            this.btnSpeedSeletion6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion6, "Speed Dosage Selection");
            this.btnSpeedSeletion6.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion6.Click += new System.EventHandler(this.btnSpeedSeletion6_Click);
            // 
            // btnSpeedSeletion5
            // 
            this.btnSpeedSeletion5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion5.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion5.Location = new System.Drawing.Point(6, 201);
            this.btnSpeedSeletion5.Name = "btnSpeedSeletion5";
            this.btnSpeedSeletion5.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion5.TabIndex = 4;
            this.btnSpeedSeletion5.Tag = "urdu";
            this.btnSpeedSeletion5.Text = "�Bq j��e \\Ju ��� ��A";
            this.btnSpeedSeletion5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion5, "Speed Dosage Selection");
            this.btnSpeedSeletion5.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion5.Click += new System.EventHandler(this.btnSpeedSeletion5_Click);
            // 
            // btnSpeedSeletion4
            // 
            this.btnSpeedSeletion4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion4.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion4.Location = new System.Drawing.Point(6, 152);
            this.btnSpeedSeletion4.Name = "btnSpeedSeletion4";
            this.btnSpeedSeletion4.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion4.TabIndex = 3;
            this.btnSpeedSeletion4.Tag = "urdu";
            this.btnSpeedSeletion4.Text = "�Bq j��e \\Ju ��� B�eE";
            this.btnSpeedSeletion4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion4, "Speed Dosage Selection");
            this.btnSpeedSeletion4.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion4.Click += new System.EventHandler(this.btnSpeedSeletion4_Click);
            // 
            // btnSpeedSeletion3
            // 
            this.btnSpeedSeletion3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion3.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion3.Location = new System.Drawing.Point(6, 103);
            this.btnSpeedSeletion3.Name = "btnSpeedSeletion3";
            this.btnSpeedSeletion3.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion3.TabIndex = 2;
            this.btnSpeedSeletion3.Tag = "urdu";
            this.btnSpeedSeletion3.Text = "�Bq j��e \\Ju ӻ̌ �e";
            this.btnSpeedSeletion3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion3, "Speed Dosage Selection");
            this.btnSpeedSeletion3.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion3.Click += new System.EventHandler(this.btnSpeedSeletion3_Click);
            // 
            // btnSpeedSeletion2
            // 
            this.btnSpeedSeletion2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion2.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion2.Location = new System.Drawing.Point(6, 54);
            this.btnSpeedSeletion2.Name = "btnSpeedSeletion2";
            this.btnSpeedSeletion2.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion2.TabIndex = 1;
            this.btnSpeedSeletion2.Tag = "urdu";
            this.btnSpeedSeletion2.Text = "�Bq j��e \\Ju ӻ̌ ��A";
            this.btnSpeedSeletion2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion2, "Speed Dosage Selection");
            this.btnSpeedSeletion2.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion2.Click += new System.EventHandler(this.btnSpeedSeletion2_Click);
            // 
            // btnSpeedSeletion1
            // 
            this.btnSpeedSeletion1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpeedSeletion1.Font = new System.Drawing.Font("AlKatib1", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpeedSeletion1.Location = new System.Drawing.Point(6, 5);
            this.btnSpeedSeletion1.Name = "btnSpeedSeletion1";
            this.btnSpeedSeletion1.Size = new System.Drawing.Size(265, 46);
            this.btnSpeedSeletion1.TabIndex = 0;
            this.btnSpeedSeletion1.Tag = "urdu";
            this.btnSpeedSeletion1.Text = "�Bq j��e \\Ju ӻ̌ ��eE";
            this.btnSpeedSeletion1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.btnSpeedSeletion1, "Speed Dosage Selection");
            this.btnSpeedSeletion1.UseVisualStyleBackColor = true;
            this.btnSpeedSeletion1.Click += new System.EventHandler(this.btnSpeedSeletion1_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblMedicineName);
            this.panel6.Location = new System.Drawing.Point(2, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(436, 36);
            this.panel6.TabIndex = 5;
            this.panel6.Tag = "top";
            // 
            // lblMedicineName
            // 
            this.lblMedicineName.AutoSize = true;
            this.lblMedicineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedicineName.Location = new System.Drawing.Point(139, 5);
            this.lblMedicineName.Name = "lblMedicineName";
            this.lblMedicineName.Size = new System.Drawing.Size(157, 24);
            this.lblMedicineName.TabIndex = 0;
            this.lblMedicineName.Text = "Medicine Name";
            // 
            // chbEvening
            // 
            this.chbEvening.AutoSize = true;
            this.chbEvening.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbEvening.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbEvening.Location = new System.Drawing.Point(57, 73);
            this.chbEvening.Name = "chbEvening";
            this.chbEvening.Size = new System.Drawing.Size(64, 38);
            this.chbEvening.TabIndex = 4;
            this.chbEvening.Tag = "urdu";
            this.chbEvening.Text = "�Bq ";
            this.toolTip1.SetToolTip(this.chbEvening, "Dosage Time");
            this.chbEvening.UseVisualStyleBackColor = true;
            this.chbEvening.CheckedChanged += new System.EventHandler(this.chbEvening_CheckedChanged);
            // 
            // chbNoon
            // 
            this.chbNoon.AutoSize = true;
            this.chbNoon.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbNoon.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNoon.Location = new System.Drawing.Point(49, 38);
            this.chbNoon.Name = "chbNoon";
            this.chbNoon.Size = new System.Drawing.Size(72, 38);
            this.chbNoon.TabIndex = 3;
            this.chbNoon.Tag = "urdu";
            this.chbNoon.Text = "j��e";
            this.toolTip1.SetToolTip(this.chbNoon, "Dosage Time");
            this.chbNoon.UseVisualStyleBackColor = true;
            this.chbNoon.CheckedChanged += new System.EventHandler(this.chbNoon_CheckedChanged);
            // 
            // chbNight
            // 
            this.chbNight.AutoSize = true;
            this.chbNight.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbNight.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNight.Location = new System.Drawing.Point(60, 106);
            this.chbNight.Name = "chbNight";
            this.chbNight.Size = new System.Drawing.Size(61, 38);
            this.chbNight.TabIndex = 7;
            this.chbNight.Tag = "urdu";
            this.chbNight.Text = "PAi";
            this.toolTip1.SetToolTip(this.chbNight, "Dosage Time for Night");
            this.chbNight.UseVisualStyleBackColor = true;
            this.chbNight.CheckedChanged += new System.EventHandler(this.chbNight_CheckedChanged);
            // 
            // chbMorning
            // 
            this.chbMorning.AutoSize = true;
            this.chbMorning.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbMorning.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbMorning.Location = new System.Drawing.Point(57, 9);
            this.chbMorning.Name = "chbMorning";
            this.chbMorning.Size = new System.Drawing.Size(64, 38);
            this.chbMorning.TabIndex = 5;
            this.chbMorning.Tag = "urdu";
            this.chbMorning.Text = "\\Ju";
            this.toolTip1.SetToolTip(this.chbMorning, "Dosage Time");
            this.chbMorning.UseVisualStyleBackColor = true;
            this.chbMorning.CheckedChanged += new System.EventHandler(this.chbMorning_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chbMorning);
            this.groupBox1.Controls.Add(this.chbNight);
            this.groupBox1.Controls.Add(this.chbNoon);
            this.groupBox1.Controls.Add(this.chbEvening);
            this.groupBox1.Location = new System.Drawing.Point(5, 114);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(137, 144);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // frmSelectDosage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 594);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmSelectDosage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSelectDosage";
            this.Load += new System.EventHandler(this.frmSelectDosage_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSelectDosage_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnSpeedSeletion8;
        private System.Windows.Forms.Button btnSpeedSeletion7;
        private System.Windows.Forms.Button btnSpeedSeletion6;
        private System.Windows.Forms.Button btnSpeedSeletion5;
        private System.Windows.Forms.Button btnSpeedSeletion4;
        private System.Windows.Forms.Button btnSpeedSeletion3;
        private System.Windows.Forms.Button btnSpeedSeletion2;
        private System.Windows.Forms.Button btnSpeedSeletion1;
        private System.Windows.Forms.Label lblMedicineName;
        private System.Windows.Forms.Label lblSpeedSelection;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboQunatity;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.ComboBox cboInstruction;
        private System.Windows.Forms.ComboBox cboDayCount;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lblFinalDosage;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnAddDosageInstruction;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chbMorning;
        private System.Windows.Forms.CheckBox chbNight;
        private System.Windows.Forms.CheckBox chbNoon;
        private System.Windows.Forms.CheckBox chbEvening;

    }
}