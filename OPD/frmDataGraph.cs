using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace OPD
{
    public partial class frmDataGraph : Form
    {
        private frmOPD MainOPD = null;

        public frmDataGraph()
        {
            InitializeComponent();
        }

        public frmDataGraph(frmOPD Frm)
        {
            InitializeComponent();
            this.MainOPD = Frm;
        }

        private void frmDataGraph_Load(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            SComponents objComp = new SComponents();
            DataView dv;

            try
            {
                objComp.ApplyStyleToControls(this);
                objConnection.Connection_Open();

                dv = objPatientReg.GetAll(12);
                PopulateVisitYear(objConnection);

                objConnection.Connection_Close();

                if (dv.Table.Rows.Count != 0)
                {
                    for (int i = 0; i < dv.Table.Rows.Count; i++)
                    {
                        tvCity.Nodes.Add(i.ToString(), dv.Table.Rows[i]["name"].ToString(), dv.Table.Rows[i]["Cityid"].ToString());
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            DrawItemTree();
            lblItem.Text = "";
            lblItemHeading.Text = "";
            lblPatientFound.Text = "";
            cboDrawCriteria.SelectedIndex = 0;
            cboGraphType.Text = "Select";

            //FillDatagrid();

            objConnection = null;
            objComp = null;
            objPatientReg = null;
        }

        #region Populate Trees

        private void PopulateVisitYear(clsBLDBConnection objConnection)
        {
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dv;

            dv = objOPD.GetAll(8);
            if (dv.Table.Rows.Count != 0)
            {
                for (int i = 0; i < dv.Table.Rows.Count; i++)
                {
                    tvVisitYear.Nodes.Add(dv.Table.Rows[i]["VisitYear"].ToString());
                }
            }

            objOPD = null;
            dv = null;
        }

        private void DrawItemTree()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPreference objPref = new clsBLPreference(objConnection);
            DataView dv;

            tvItems.Nodes.Clear();
            tvItems.Nodes.Add("-1", "Items", "-1");

            objConnection.Connection_Open();

            objPref.PersonID = clsSharedVariables.UserID;
            dv = objPref.GetAll(6);

            if (dv.Table.Rows.Count != 0)
            {
                populatItemTree(tvItems.Nodes["-1"], dv, 0, 0);
            }
            objConnection.Connection_Close();
            //tvItems.ExpandAll();

            objConnection = null;
            objPref = null;
            dv = null;

        }

        private void populatItemTree(TreeNode tr, DataView dv, int i, int Level)
        {
            try
            {
                if (i == dv.Table.Rows.Count)
                {
                    return;
                }
                else
                {
                    if (Level == 0)
                    {
                        tvItems.Nodes["-1"].Nodes.Add(dv.Table.Rows[i]["TypeID"].ToString(), dv.Table.Rows[i]["tName"].ToString(), dv.Table.Rows[i]["tName"].ToString());
                        tvItems.Nodes["-1"].Nodes[dv.Table.Rows[i]["TypeID"].ToString()].Nodes.Add(dv.Table.Rows[i]["PrefID"].ToString(), dv.Table.Rows[i]["Name"].ToString(), dv.Table.Rows[i]["Typeid"].ToString());
                        populatItemTree(tvItems.Nodes["-1"].Nodes[dv.Table.Rows[i]["TypeID"].ToString()], dv, ++i, 1);
                    }
                    else
                    {
                        if (dv.Table.Rows[i]["Typeid"].ToString().Equals(dv.Table.Rows[i - 1]["Typeid"].ToString()))
                        {
                            tvItems.Nodes["-1"].Nodes[dv.Table.Rows[i]["TypeID"].ToString()].Nodes.Add(dv.Table.Rows[i]["PrefID"].ToString(), dv.Table.Rows[i]["Name"].ToString(), dv.Table.Rows[i]["Typeid"].ToString());
                            populatItemTree(tvItems.Nodes["-1"].Nodes[dv.Table.Rows[i]["TypeID"].ToString()], dv, ++i, 1);
                        }
                        else
                        {
                            populatItemTree(tvItems.Nodes["-1"].Nodes[dv.Table.Rows[i]["TypeID"].ToString()], dv, i, 0);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            dv = null;
        }

        #endregion

        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            if (rbData.Checked)
            {
                dgPatient.Visible = true;
                lblPatientFound.Visible = true;
                crvGraph.Visible = false;
                //csPatients.Visible = false;
                pnlGraph.Visible = false;
            }
            else
            {
                dgPatient.Visible = false;
                lblPatientFound.Visible = false;
                crvGraph.Visible = true;
                //csPatients.Visible = true;
                pnlGraph.Visible = true;
                //btnDraw_Click(sender, e);
            }
        }

        private void FillDatagrid()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);
            DataView dvOPD = new DataView();

            DataItemSetting(objConnection, objOPD);

            objConnection.Connection_Open();
            dvOPD = objOPD.GetAll(9);
            objConnection.Connection_Close();

            dgPatient.DataSource = dvOPD;

            GenrateSerialNo();
            lblPatientFound.Text = "Patient Found : " + dgPatient.Rows.Count.ToString();
            dvOPD = null;
            objConnection = null;
            objOPD = null;
        }

        private void GenrateSerialNo()
        {
            for (int i = 1; i <= dgPatient.Rows.Count; i++)
            {
                dgPatient.Rows[i - 1].Cells["SrNo"].Value = i.ToString();
            }
        }

        private void btnItemCAll_Click(object sender, EventArgs e)
        {
            tvItems.CollapseAll();
        }

        private void btnItemEAll_Click(object sender, EventArgs e)
        {
            tvItems.ExpandAll();
        }

        private void dtpTo_Leave(object sender, EventArgs e)
        {
            if (dtpTo.Value.CompareTo(dtpFrom.Value) == -1)
            {
                //FillDatagrid();
            }
            else
            {
                MessageBox.Show("To Date is Always Less Then From Date");
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            FillDatagrid();
            //cboGraphType.Text = "Select";
            //cboGraphType.Enabled = false;
            rbData.Checked = true;
        }

        private void DrawGraph()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            //csPatients.Clear();
            //csPatients.Charts.Add(0);

            //csPatients.Charts[0].Type = Microsoft.Office.Interop.Owc11.ChartChartTypeEnum.chChartTypeColumnClustered;
            //csPatients.Charts[0].HasLegend = true;
            //csPatients.Charts[0].HasTitle = true;
            //csPatients.Charts[0].Title.Font.Bold = true;
            //csPatients.Charts[0].Axes[0].HasTitle = true;
            //csPatients.Charts[0].Axes[0].Font.Bold = true;
            //csPatients.Charts[0].Axes[0].Title.Font.Bold = true;
            //csPatients.Charts[0].Axes[1].HasTitle = true;
            //csPatients.Charts[0].Axes[1].Font.Bold = true;
            //csPatients.Charts[0].Axes[1].Title.Font.Bold = true;


            if (cboDrawCriteria.SelectedIndex == 0)
            {
                DrawLocation(objConnection, objOPD);
            }
            else if (cboDrawCriteria.SelectedIndex == 1)
            {

                if (DateTime.Compare(dtpTo.Value.Date, dtpFrom.Value.Date) < 0)
                {
                    DrawAge(objConnection, objOPD);
                }
            }
            else if (cboDrawCriteria.SelectedIndex == 2)
            {
                DrawVisit(objConnection, objOPD);
            }


            objConnection = null;
            objOPD = null;
        }

        private void CreateSeries(DataView dv, ref string XAxis, ref string YAxis, string XCol, string YCol)
        {
            for (int i = 0; i < dv.Table.Rows.Count; i++)
            {
                XAxis = XAxis + dv.Table.Rows[i][XCol].ToString();
                if (i != dv.Table.Rows.Count - 1)
                {
                    XAxis = XAxis + " , ";
                }
                YAxis = YAxis + dv.Table.Rows[i][YCol].ToString();
                if (i != dv.Table.Rows.Count - 1)
                {
                    YAxis = YAxis + " , ";
                }
            }
        }

        private void btnDraw_Click(object sender, EventArgs e)
        {
            DrawGraph();
            //cboGraphType.Enabled = true;
            //cboGraphType.Text = "Cluster Column";
            rbGraph.Checked = true;
            //rb_CheckedChanged(rbGraph, EventArgs.Empty);
        }

        private void DrawLocation(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {
            //string SerX = "", SerY = "";
            DataView dvOPD;

            objOPD.Signs = " ,cm_tcity ct ";
            objOPD.Pulse = "select com.CityName,count(com.cityID) as cityCount from (";
            objOPD.Investigation = ") as com  group by com.cityid order by cityname";
            objOPD.Payment = " ct.cityid=p.cityid ";
            objOPD.PresentingComplaints = " , ct.Name as cityName,ct.cityid ";
            DataItemSetting(objConnection, objOPD);
            objConnection.Connection_Open();

            dvOPD = objOPD.GetAll(9);
            objConnection.Connection_Close();

            lblMainHeading.Text = "Data Analysis for Location";

            ////csPatients.ChartSpaceTitle.Caption = "Data Analysis for Location";

            ////////Microsoft.Office.Interop.Owc11.ChSeries ser = csPatients.Charts[0].SeriesCollection.Add(0);
            //////////Microsoft.Office.Interop.Owc11.ChSeries ser1 = csPatients.Charts[0].SeriesCollection.Add(1);

            //////////change the caption of legend
            lblSubHeading.Text = "City vs Number of Visit";
            ////////csPatients.Charts[0].Title.Caption = "City vs Number of Visit";
            lblXHeading.Text = "City";
            GraphAssign(dvOPD, "CityName", "CityCount");

            ////////csPatients.Charts[0].Axes[0].Title.Caption = "City";
            ////////csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";
            //////csPatients.Charts[0].SeriesCollection[0].Caption = "City";
            ////csPatients.Charts[0].SeriesCollection[0].get_Scalings(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories).HasSplit = true;
            ////CreateSeries(dvOPD, ref SerX, ref SerY , "CityName", "CityCount");

            //#region Create X-Axis(City Name)
            //SerX = "";
            //for (int i = 0; i < tvCity.Nodes.Count; i++)
            //{
            //  if (tvCity.Nodes[i].Checked)
            //  {
            //    SerX += tvCity.Nodes[i].Text.Trim() + " ,";
            //  }
            //}

            //if (SerX.Length > 0)
            //{
            //  SerX = SerX.Substring(0, SerX.Length - 1);
            //}
            //#endregion

            //#region Create Y-Axis(City Count)
            //SerY = "";
            //for (int i = 0, j = 0; i < SerX.Split(',').Length; i++)
            //{
            //  if (j < dvOPD.Table.Rows.Count && dvOPD.Table.Rows[j]["CityName"].ToString().Equals(SerX.Split(',').GetValue(i).ToString().Trim()))
            //  {
            //    //SerY.Split(',').SetValue((dvOPD.Table.Rows[j]["CityCount"].ToString(), i);
            //    SerY += dvOPD.Table.Rows[j]["CityCount"].ToString() + ",";
            //    j++;
            //  }
            //  else
            //  {
            //    SerY += "0,";
            //  }
            //}
            //if (SerY.Length > 0)
            //{
            //  SerY = SerY.Substring(0, SerY.Length - 1);
            //}
            //#endregion
            ////ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, "1,2,3,4,5,6");
            ////////ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //////ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerY);
            ////ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, "9,9,9,9,9,9");
            ////ser1.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, "9,9,9,9,9,9");

            dvOPD = null;
        }

        private void DrawVisit(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {
            string tmp = "";

            #region Get Data From Database

            #region Create DateFromat At Runtime
            for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
            {
                if (tvVisitMonth.Nodes[i].Checked)
                {
                    tmp = "%M";
                    break;
                }
            }

            for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            {
                if (tvVisitYear.Nodes[i].Checked)
                {
                    if (tmp.Equals(""))
                    {
                        tmp = "%Y";
                        objOPD.Temprature = "select Date_format(com.visitdate,'%Y') as visitdate,count(com.Visitdate) as visitCount from ( ";
                        objOPD.Status = ") as com  group by  Date_format(com.visitdate,'" + tmp + "') order by Date_format(com.visitdate,'%Y')";

                    }
                    else
                    {
                        tmp += "/%Y";
                        objOPD.Temprature = "select Date_format(com.visitdate,'%M/%Y') as visitdate,count(com.Visitdate) as visitCount from ( ";
                        objOPD.Status = ") as com  group by  Date_format(com.visitdate,'" + tmp + "') order by Date_format(com.visitdate,'%Y/%m')";
                    }
                    break;
                }
            }
            if (tmp.Equals(""))
            {
                objOPD.Temprature = "select count(com.Visitdate) as visitCount from ( ";
                objOPD.Status = ") as com";
            }
            #endregion

            if (tmp.Equals("%M"))
            {
                objOPD.Temprature = "select Date_format(com.visitdate,'%M') as visitdate,count(com.Visitdate) as visitCount from ( ";
                objOPD.Status = ") as com  group by  Date_format(com.visitdate,'" + tmp + "') order by Date_format(com.visitdate,'%m')";
            }

            DataItemSetting(objConnection, objOPD);
            objConnection.Connection_Open();
            DataView dvOPD = objOPD.GetAll(9);
            objConnection.Connection_Close();

            #endregion

            GraphAssign(dvOPD, "VisitDate", "VisitCount");

            if (tmp.Equals("%M/%Y"))
            {
              #region Create series for Years

            //  SerZ = "";
            //  for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            //  {
            //    if (tvVisitYear.Nodes[i].Checked)
            //    {
            //      SerZ += tvVisitYear.Nodes[i].Text + " ,";
            //    }
            //  }

            //  if (SerZ.Length > 0)
            //  {
            //    SerZ = SerZ.Substring(0, SerZ.Length - 1);
            //  }
            //  else
            //  {
            //    for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            //    {
            //      SerZ += tvVisitYear.Nodes[i].Text + " ,";
            //    }
            //    if (SerZ.Length > 0)
            //    {
            //      SerZ = SerZ.Substring(0, SerZ.Length - 1);
            //    }
            //  }
              #endregion

            //  Microsoft.Office.Interop.Owc11.ChSeries[] Ser = new Microsoft.Office.Interop.Owc11.ChSeries[SerZ.Split(',').Length];
            //  SerY = new string[SerZ.Split(',').Length];

            //  for (int j = 0; j < SerZ.Split(',').Length; j++)
            //  {
            //    Ser[j] = csPatients.Charts[0].SeriesCollection.Add(j);
            //    csPatients.Charts[0].SeriesCollection[j].Caption = SerZ.Split(',').GetValue(j).ToString();
            //  }
            //  //Microsoft.Office.Interop.Owc11.ChSeries ser = csPatients.Charts[0].SeriesCollection.Add(0);
            //  //change the caption of legend
            //  //CreateSeries(dvOPD, ref SerX, ref SerY, "CityName", "CityCount");

              #region  Create series for X-Axis(Month)

            //  for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
            //  {
            //    if (tvVisitMonth.Nodes[i].Checked)
            //    {
            //      SerX += tvVisitMonth.Nodes[i].Text.Trim() + " ,";
            //      //for (int j = 0; j < SerY.Length ; j++)
            //      //{
            //      //    SerY[j]+="0,"; 
            //      //}
            //    }
            //  }
            //  if (SerX.Length > 0)
            //  {
            //    SerX = SerX.Substring(0, SerX.Length - 1);
            //    //for (int j = 0; j < SerY.Length ; j++)
            //    //    {
            //    //        SerY[j] = SerY[j].Substring(0, SerY[j].Length - 1);
            //    //    }
            //  }
            //  else
            //  {
            //    for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
            //    {
            //      SerX += tvVisitMonth.Nodes[i].Text.Trim() + " ,";
            //      //for (int j = 0; j < SerY.Length ; j++)
            //      //{
            //      //    SerY[j]+="0,"; 
            //      //}
            //    }
            //    SerX = SerX.Substring(0, SerX.Length - 1);
            //    //for (int j = 0; j < SerY.Length ; j++)
            //    //    {
            //    //        SerY[j] = SerY[j].Substring(0, SerY[j].Length - 1);
            //    //    }
              }

              #endregion

              #region  Create series for Y-Axis

            //  for (int k = 0, j = 0; k < SerZ.Split(',').Length; k++)
            //  {
            //    for (int i = 0; i < SerX.Split(',').Length; i++)
            //    {
            //      if (j < dvOPD.Table.Rows.Count && dvOPD.Table.Rows[j]["VisitDate"].ToString().Equals(SerX.Split(',').GetValue(i).ToString().Trim() + "/" + SerZ.Split(',').GetValue(k).ToString().Trim()))
            //      {
            //        //SerY.Split(',').SetValue((dvOPD.Table.Rows[j]["CityCount"].ToString(), i);
            //        SerY[k] += dvOPD.Table.Rows[j]["VisitCount"].ToString() + ",";
            //        j++;
            //      }
            //      else
            //      {
            //        SerY[k] += "0,";
            //      }
            //    }
            //    if (SerY[k].Length > 0)
            //    {
            //      SerY[k] = SerY[k].Substring(0, SerY[k].Length - 1);
            //    }
            //  }


              #endregion

            //  //CreateSeries(dvOPD, ref SerX, ref SerY, "visitdate", "visitcount");

            //  Ser[0].SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //  for (int j = 0; j < SerZ.Split(',').Length; j++)
            //  {
            //    Ser[j].SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerY[j]);
            //  }
            lblMainHeading.Text = "Data Analysis for Period";
            lblSubHeading.Text = "Visit Period vs Number of Visits";

            //  csPatients.Charts[0].Title.Caption = "Visit Period vs Number of Visits";
            lblXHeading.Text = "Visit Period (Month)";
            //  csPatients.Charts[0].Axes[0].Title.Caption = "Visit Period (Month)";
            //  csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";
            //}
            //else
            //{
            //  string SerX = "", SerY = "";
            //  Microsoft.Office.Interop.Owc11.ChSeries Ser = csPatients.Charts[0].SeriesCollection.Add(0);

             if (tmp.Equals(""))
              {
                  lblMainHeading.Text = "Data Analysis for Period";
                  lblSubHeading.Text = "Complete Period vs Number of Visits";
                  lblXHeading.Text = "Complete";
            //    csPatients.Charts[0].Title.Caption = "Complete Period vs Number of Visits";
            //    csPatients.Charts[0].Axes[0].Title.Caption = "Visit Period";
            //    csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";

            //    csPatients.Charts[0].SeriesCollection[0].Caption = "Complete";

            //    SerX = "Complete";
            //    SerY = dvOPD.Table.Rows[0]["VisitCount"].ToString();
              }
              else if (tmp.Equals("%Y"))
              {
            //    #region Create series for Years(X-Axis)

            //    SerX = "";
            //    for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            //    {
            //      if (tvVisitYear.Nodes[i].Checked)
            //      {
            //        SerX += tvVisitYear.Nodes[i].Text + " ,";
            //      }
            //    }

            //    if (SerX.Length > 0)
            //    {
            //      SerX = SerX.Substring(0, SerX.Length - 1);
            //    }
            //    else
            //    {
            //      for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            //      {
            //        SerX += tvVisitYear.Nodes[i].Text + " ,";
            //      }
            //      if (SerX.Length > 0)
            //      {
            //        SerX = SerX.Substring(0, SerX.Length - 1);
            //      }
            //   }
            //    #endregion

            //    csPatients.Charts[0].SeriesCollection[0].Caption = "Visit Year";

            //    #region  Create series for Y-Axis

            //    for (int k = 0, j = 0; k < SerX.Split(',').Length; k++)
            //    {
            //      if (j < dvOPD.Table.Rows.Count && dvOPD.Table.Rows[j]["VisitDate"].ToString().Equals(SerX.Split(',').GetValue(k).ToString().Trim()))
            //      {
            //        //SerY.Split(',').SetValue((dvOPD.Table.Rows[j]["CityCount"].ToString(), i);
            //        SerY += dvOPD.Table.Rows[j]["VisitCount"].ToString() + ",";
            //        j++;
            //      }
            //      else
            //      {
            //        SerY += "0,";
            //      }
            //    }
            //    if (SerY.Length > 0)
            //    {
            //      SerY = SerY.Substring(0, SerY.Length - 1);
            //    }
            //    #endregion

                  lblMainHeading.Text = "Data Analysis for Period";
                  lblSubHeading.Text = "Visit Period (Years) vs Number of Visits";
                  lblXHeading.Text = "Visit Period ( Years )";
            //    csPatients.Charts[0].Title.Caption = "Visit Period (Years) vs Number of Visits";
            //    csPatients.Charts[0].Axes[0].Title.Caption = "Visit Period ( Years )";
            //    csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";
              }
              else if (tmp.Equals("%M"))
              {
            //    csPatients.Charts[0].SeriesCollection[0].Caption = "Visit Month";

            //    #region  Create series for X-Axis(Month)

            //    for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
            //    {
            //      if (tvVisitMonth.Nodes[i].Checked)
            //      {
            //        SerX += tvVisitMonth.Nodes[i].Text.Trim() + " ,";
            //      }
            //    }
            //    if (SerX.Length > 0)
            //    {
            //      SerX = SerX.Substring(0, SerX.Length - 1);
            //    }
            //    else
            //    {
            //      for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
            //      {
            //        SerX += tvVisitMonth.Nodes[i].Text.Trim() + " ,";
            //      }
            //      SerX = SerX.Substring(0, SerX.Length - 1);
            //    }

            //    #endregion

            //    #region  Create series for Y-Axis


            //    for (int i = 0, j = 0; i < SerX.Split(',').Length; i++)
            //    {
            //      if (j < dvOPD.Table.Rows.Count && dvOPD.Table.Rows[j]["VisitDate"].ToString().Equals(SerX.Split(',').GetValue(i).ToString().Trim()))
            //      {
            //        //SerY.Split(',').SetValue((dvOPD.Table.Rows[j]["CityCount"].ToString(), i);
            //        SerY += dvOPD.Table.Rows[j]["VisitCount"].ToString() + ",";
            //        j++;
            //      }
            //      else
            //      {
            //        SerY += "0,";
            //      }
            //    }
            //    if (SerY.Length > 0)
            //    {
            //      SerY = SerY.Substring(0, SerY.Length - 1);
            //    }
            //    #endregion
                  lblMainHeading.Text = "Data Analysis for Period";
                  lblSubHeading.Text = "Visit Period (Month) vs Number of Visits";
                  lblXHeading.Text = "Visit Date ( Month )";
            //    csPatients.Charts[0].Title.Caption = "Visit Period (Month) vs Number of Visits";
            //    csPatients.Charts[0].Axes[0].Title.Caption = "Visit Date ( Month )";
            //    csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";
              }
            //  Ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //  Ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerY);
            //}
            dvOPD = null;
        }

        private void DrawAge(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {
            //string SerX = "", SerY = "";

            objOPD.Weight = "select (year(Now())-year(dob))as DOBYear,count(Dob) as DOBCount from (";
            objOPD.BP = " , p.DOB";
            objOPD.Height = ") as com  group by  year(com.dob) Desc";

            DataItemSetting(objConnection, objOPD);
            objConnection.Connection_Open();
            DataView dvOPD = objOPD.GetAll(9);
            objConnection.Connection_Close();

            //Microsoft.Office.Interop.Owc11.ChSeries ser = csPatients.Charts[0].SeriesCollection.Add(0);
            lblMainHeading.Text = "Data Analysis for Age";
            lblSubHeading.Text = "Age vs Number of Visit";
            lblXHeading.Text = "Age(in Years)";
            GraphAssign(dvOPD,"DOBYear","DOBCount");

            //csPatients.Charts[0].Title.Caption = "Age vs Number of Visit";
            //csPatients.Charts[0].Axes[0].Title.Caption = "Age(in Years)";
            //csPatients.Charts[0].Axes[1].Title.Caption = "Number of Visit";
            //csPatients.Charts[0].SeriesCollection[0].Caption = "Age(Years)";

            //CreateSeries(dvOPD, ref SerX, ref SerY, "DOBYear", "DOBCount");

            //ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimCategories, -1, SerX);
            //ser.SetData(Microsoft.Office.Interop.Owc11.ChartDimensionsEnum.chDimValues, -1, SerY);

            dvOPD = null;
        }

        private void DataItemSetting(clsBLDBConnection objConnection, clsBLOPD objOPD)
        {

            objOPD.VisitDate = "";

            #region Selecting Years and Months for Visit Date

            for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
            {
                for (int j = 0; j < tvVisitMonth.Nodes.Count; j++)
                {
                    if (!tvVisitYear.Nodes[i].Checked)
                    {
                        break;
                    }
                    else
                    {
                        if (tvVisitMonth.Nodes[j].Checked)
                        {
                            if (objOPD.VisitDate.Equals(""))
                            {
                                objOPD.VisitDate = " Date_format(o.visitdate,'%M/%Y') in ('" + tvVisitMonth.Nodes[j].Text + "/" + tvVisitYear.Nodes[i].Text + "'";//For seleted City
                            }
                            else
                            {
                                objOPD.VisitDate += " ,'" + tvVisitMonth.Nodes[j].Text + "/" + tvVisitYear.Nodes[i].Text + "'";//For seleted City
                            }
                        }
                    }
                }
            }
            if (!objOPD.VisitDate.Equals(""))
            {
                objOPD.VisitDate += " ) ";
            }
            else
            {
                for (int i = 0; i < tvVisitYear.Nodes.Count; i++)
                {
                    if (tvVisitYear.Nodes[i].Checked)
                    {
                        if (objOPD.VisitDate.Equals(""))
                        {
                            objOPD.VisitDate = " Date_format(o.visitdate,'%Y') in ( '" + tvVisitYear.Nodes[i].Text + "'";//For seleted City      
                        }
                        else
                        {
                            objOPD.VisitDate += " ,'" + tvVisitYear.Nodes[i].Text + "'";//For seleted City
                        }
                    }
                }
                if (!objOPD.VisitDate.Equals(""))
                {
                    objOPD.VisitDate += " ) ";
                }
                else
                {
                    for (int i = 0; i < tvVisitMonth.Nodes.Count; i++)
                    {
                        if (tvVisitMonth.Nodes[i].Checked)
                        {
                            if (objOPD.VisitDate.Equals(""))
                            {
                                objOPD.VisitDate = " Date_format(o.visitdate,'%M') in ( '" + tvVisitMonth.Nodes[i].Text + "'";//For seleted City
                            }
                            else
                            {
                                objOPD.VisitDate += " ,'" + tvVisitMonth.Nodes[i].Text + "'";//For seleted City
                            }
                        }
                    }
                    if (!objOPD.VisitDate.Equals(""))
                    {
                        objOPD.VisitDate += " ) ";
                    }
                }
            }

            #endregion

            #region Select item list

            if (tvItems.SelectedNode != null)
            {
                if (tvItems.SelectedNode.Level == 1)
                {
                    objOPD.History = tvItems.SelectedNode.ImageKey;//for Selected Type
                    lblItem.Text = tvItems.SelectedNode.ImageKey;
                    lblItemHeading.Text = "Type is : ";
                    dgPatient.Columns["Item"].Visible = true;
                    dgPatient.Columns["Item"].HeaderText = tvItems.SelectedNode.ImageKey;
                }
                else if (tvItems.SelectedNode.Level == 2)
                {
                    objOPD.History = tvItems.SelectedNode.Parent.Text;//for Selected SubType
                    objOPD.Instruction = tvItems.SelectedNode.Text.Replace("'", "''");
                    lblItem.Text = tvItems.SelectedNode.Text;
                    lblItemHeading.Text = "Item is : ";
                    dgPatient.Columns["Item"].Visible = true;
                    dgPatient.Columns["Item"].HeaderText = tvItems.SelectedNode.Parent.Text;
                }
                else
                {
                    dgPatient.Columns["Item"].Visible = false;
                    lblItem.Text = "";
                    lblItemHeading.Text = "For All Items";

                }
            }

            #endregion

            #region Selected City and DOB
            objOPD.BMI = "";
            for (int i = 0; i < tvCity.Nodes.Count; i++)
            {
                if (tvCity.Nodes[i].Checked)
                {
                    objOPD.BMI += tvCity.Nodes[i].ImageKey + " ,";//For seleted City
                }
            }
            if (!objOPD.BMI.Equals(""))
            {
                objOPD.BMI = objOPD.BMI.Substring(0, objOPD.BMI.Length - 1);
            }

            if (dtpTo.Value.CompareTo(dtpFrom.Value) == -1)
            {
                objOPD.DisposalPlan = dtpFrom.Value.AddYears(-1).ToString();//Range seleted fro DOB
                objOPD.Diagnosis = dtpTo.Value.AddYears(-1).ToString();
            }

            #endregion
        }

        private void dgPatient_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenrateSerialNo();
        }

        private void cboGraphType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboGraphType.Text.Equals("Cluster Column"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnClustered;
            //}
            //else if (cboGraphType.Text.Equals("Stack Column"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnStacked;
            //}
            //else if (cboGraphType.Text.Equals("100% Stack Column"))
            //{
            //    csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnStacked100;
            //}
            //else if (cboGraphType.Text.Equals("3D Column"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumn3D;
            //}
            //else if (cboGraphType.Text.Equals("3D Column Cluster"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnClustered3D;
            //}
            //else if (cboGraphType.Text.Equals("3D Stack Column"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnStacked3D;
            //}
            //else if (cboGraphType.Text.Equals("3D 100% Stack Column"))
            //{
            //    csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeColumnStacked1003D;
            //}
            //else if (cboGraphType.Text.Equals("Cluster Bar"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarClustered;
            //}
            //else if (cboGraphType.Text.Equals("Stacked Bar"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarStacked;
            //}
            //else if (cboGraphType.Text.Equals("100% Stacked Bar"))
            //{
            //    csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarStacked100;
            //}
            //else if (cboGraphType.Text.Equals("3D Bar"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarStacked3D;
            //}
            //else if (cboGraphType.Text.Equals("3D Bar Cluster"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarClustered3D;
            //}
            //else if (cboGraphType.Text.Equals("3D Stacked Bar"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarStacked3D;
            //}
            ////else if (cboGraphType.Text.Equals("3D 100% Stacked Bar"))
            ////{
            ////    csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeBarStacked1003D;
            ////}
            //else if (cboGraphType.Text.Equals("Line"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeLine;
            //}
            //else if (cboGraphType.Text.Equals("Line With Marker"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeLineMarkers;
            //}
            //else if (cboGraphType.Text.Equals("3D Line"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeLine3D;
            //}
            //else if (cboGraphType.Text.Equals("3D OverLaped Line"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeLineOverlapped3D;
            //}
            //else if (cboGraphType.Text.Equals("Smooth Line"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeSmoothLine;
            //}
            //else if (cboGraphType.Text.Equals("Smooth Line With Marker"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeSmoothLineMarkers;
            //}
            //else if (cboGraphType.Text.Equals("high Low Close"))
            //{
            //  csPatients.Charts[0].Type = ChartChartTypeEnum.chChartTypeStockHLC;
            //}

        }

        private void frmDataGraph_FormClosing(object sender, FormClosingEventArgs e)
        {
            ReportDocument rpt = (ReportDocument)crvGraph.ReportSource;

            if (rpt != null)
            {
                rpt.Close();
                rpt.Dispose();
                rpt = null;
            }
            GC.Collect();

            this.MainOPD.Show();
            this.Dispose();
        }

        private void GraphAssign(DataView dv,string Name,string Count)
        {
            ReportDocument report;
            if ((ReportDocument)crvGraph.ReportSource == null)
            {
                report = new ReportDocument();
            }
            else
            {
                report = (ReportDocument)crvGraph.ReportSource;
            }

            try
            {
                report.Load(@"Report\LocationChart.rpt");

                string userName = clsSharedVariables.DBUserName;
                string pwd = clsSharedVariables.DBPassWord;
                string serverName = clsSharedVariables.DBServer;
                string sdb = clsSharedVariables.DBName;

                dv.Table.Columns[Name].ColumnName = "Name";
                dv.Table.Columns[Count].ColumnName = "Count";
               
                report.SetDatabaseLogon(userName, pwd, serverName, sdb);
                report.SetDataSource(dv.Table);
                crvGraph.ReportSource = report;
                hideTheTabControl();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                report = null;
            }
        }

        public void hideTheTabControl()
        {
            //System.Diagnostics.Debug.Assert( crvGraph.ReportSource != null,"you have to set the ReportSource first");
            foreach (Control c1 in crvGraph.Controls)
            {
                if (c1 is CrystalDecisions.Windows.Forms.PageView)
                {
                    CrystalDecisions.Windows.Forms.PageView pv = (CrystalDecisions.Windows.Forms.PageView)c1;
                    foreach (Control c2 in pv.Controls)
                    {
                        if (c2 is TabControl)
                        {
                            TabControl tc = (TabControl)c2;
                            tc.ItemSize = new Size(0, 0);
                            tc.SizeMode = TabSizeMode.Fixed;
                            return;
                        }
                    }
                }
            }
        }
    }
}
