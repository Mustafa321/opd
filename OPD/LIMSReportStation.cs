using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;
using BusinessLayer;

namespace OPD
{
	public partial class LIMSReportStation : Form
	{
        private frmOPD objOPD = null;
        private string _PRNO="";
        private string _PatientID = "";
        
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string PRNO
        {
            get { return _PRNO; }
            set { _PRNO = value; }
        }

		public LIMSReportStation ( )
		{
			InitializeComponent();
		}

        public LIMSReportStation(frmOPD objOPD)
        {
            InitializeComponent();
            this.objOPD = objOPD;
        }
		private void LIMSReportStation_Load ( object sender , EventArgs e )
		{
			SComponents objComp = new SComponents();
            this.txtPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();

            this.Text = "Labs Reports( " + clsSharedVariables.UserName+ " ) ";

			objComp.ApplyStyleToControls(this);
            FillPatientInfo();
            if (!_PRNO.Equals(""))
            {
                FillGridView();
            }
			objComp = null;
		}

		private void FillGridView ( )
		{
			clsBLDispatch ds = new clsBLDispatch();
			DataView dv = new DataView();

			ds.PRNO = _PRNO;
            //ds.ProcessID = "6,7,8";
			
			dv = ds.GetAll(7);

			dgRptDelivery.DataSource = dv;

			for (int i = 0 ; i < dv.Table.Rows.Count ; i++)
			{
				dgRptDelivery.Rows[i].Cells["SNo"].Value = (i + 1);
                if (dv.Table.Rows[i]["ProcessID"].ToString().Equals("8") || dv.Table.Rows[i]["ProcessID"].ToString().Equals("7"))
				{
					dgRptDelivery.Rows[i].DefaultCellStyle.BackColor = Color.LightSeaGreen;
				}
				else //if (dv.Table.Rows[i]["ProcessID"].ToString().Equals("7"))
				{
					dgRptDelivery.Rows[i].DefaultCellStyle.BackColor = Color.CornflowerBlue;
				}
			}

			lblRecordCount.Text = "Test / Report ( " + dgRptDelivery.Rows.Count + " )";

			ds = null;
		}

		private void dgRptDelivery_CellDoubleClick ( object sender , DataGridViewCellEventArgs e )
		{
			ReportViewer rv = new ReportViewer();
			DataView dv = new DataView();
			clsBLDispatch objDis = new clsBLDispatch();

			objDis.PRID = dgRptDelivery.Rows[e.RowIndex].Cells["PRID"].Value.ToString();
			objDis.TestID = dgRptDelivery.Rows[e.RowIndex].Cells["testid"].Value.ToString();
			objDis.BookingID = dgRptDelivery.Rows[e.RowIndex].Cells["bookingID"].Value.ToString();
			dv = objDis.GetAll(8);


			rv.StrPRID = dgRptDelivery.Rows[e.RowIndex].Cells["PRID"].Value.ToString();
			rv.StrTestID = dgRptDelivery.Rows[e.RowIndex].Cells["testid"].Value.ToString();
			rv.StrBookingID = dgRptDelivery.Rows[e.RowIndex].Cells["bookingID"].Value.ToString();

			rv.dt = dv.Table;
			objDis = null;

			rv.ShowDialog();
		}

		private void btnClose_Click ( object sender , EventArgs e )
		{
			this.Close();
		}

		private void btnClear_Click ( object sender , EventArgs e )
		{
			FillGridView();
		}

		private void miPrint_Click ( object sender , EventArgs e )
		{
			string StrPRID = "";
			string StrBookingID = "";
			string StrTestID = "";

			if (cmsPrint.Tag != null)
            {
                int index = Convert.ToInt32(cmsPrint.Tag);
                {

                    DataView dv = new DataView();
                    DataView dvSub = new DataView();
                    clsBLDispatch objDis = new clsBLDispatch();

                    objDis.PRID = dgRptDelivery.Rows[index].Cells["PRID"].Value.ToString(); ;//prid
                    objDis.TestID = dgRptDelivery.Rows[index].Cells["TestID"].Value.ToString(); ; // testid
                    objDis.BookingID = dgRptDelivery.Rows[index].Cells["BookingID"].Value.ToString(); //bookingid

                    dv = objDis.GetAll(8);
                    dvSub = objDis.GetAll(9);

                    string sSelectionFormula = "";
                    sSelectionFormula = "{general_result_main.prid} in [" + objDis.PRID + "] and {general_result_main.testid} in [" + objDis.TestID + "] and {general_result_main.bookingid} in [" + objDis.BookingID + "]";
                    ReportViewer objRptViewer = new ReportViewer();

                    objRptViewer.StrSelectionFormula = sSelectionFormula;
                    objRptViewer.dt = dv.Table;
                    objRptViewer.dtSub = dvSub.Table;
                    objRptViewer.StrSubTableName = "one_column_1";

                    objRptViewer.ShowDialog();

                    FillGridView();
                }
            }
		}

		private void miPrintAll_Click ( object sender , EventArgs e )
		{
			string StrPRID_Prt = "";
			string StrBookingID_Prt = "";
			string StrTestID_Prt = "";

			string StrPRID = "";
			string StrBookingID = "";
			string StrTestID = "";

            if (cmsPrint.Tag != null)
            {
                int inde = Convert.ToInt32(cmsPrint.Tag);

                DataView dv = new DataView();
                DataView dvSub = new DataView();
                clsBLDispatch objDis = new clsBLDispatch();

                objDis.PRID = dgRptDelivery.Rows[inde].Cells["PRID"].Value.ToString(); //prid
                objDis.BookingID = dgRptDelivery.Rows[inde].Cells["BookingID"].Value.ToString(); //bookingid
                objDis.TestID = StrTestID_Prt;

                dv = objDis.GetAll(8);
                dvSub = objDis.GetAll(9);

                string sSelectionFormula = "";
                sSelectionFormula = "{general_result_main.prid} in [" + StrPRID_Prt + "] and {general_result_main.testid} in [" + StrTestID_Prt + "] and {general_result_main.bookingid} in [" + StrBookingID_Prt + "]";

                ReportViewer objRptViewer = new ReportViewer();

                objRptViewer.dt = dv.Table;
                objRptViewer.dtSub = dvSub.Table;
                objRptViewer.StrSubTableName = "one_column_1";

                objRptViewer.StrSelectionFormula = sSelectionFormula;

                objRptViewer.ShowDialog();
            }
        }

		private void dgRptDelivery_MouseDown ( object sender , MouseEventArgs e )
		{
			System.Windows.Forms.DataGridView.HitTestInfo hi;
			hi = dgRptDelivery.HitTest(e.X , e.Y);

			if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
			{
				dgRptDelivery.ContextMenuStrip.Items[0].Enabled = true;
				dgRptDelivery.ContextMenuStrip.Items[1].Enabled = true;
				cmsPrint.Tag = hi.RowIndex;
				dgRptDelivery.Rows[hi.RowIndex].Selected = true;
			}
			else
			{
				dgRptDelivery.ContextMenuStrip.Items[0].Enabled = false;
				dgRptDelivery.ContextMenuStrip.Items[1].Enabled = false;
				cmsPrint.Tag = null;
			}

			hi = null;
		}

		private void ClearGridRows ( DataGridView dg )
		{
			try
			{
				DataView dv = (DataView)dg.DataSource;

				if (dv != null)
				{
					dv.Table.Rows.Clear();
					dg.DataSource = dv;
				}
			}
			catch
			{
				DataTable dt = (DataTable)dg.DataSource;

				if (dt != null)
				{
					dt.Rows.Clear();
					dg.DataSource = dt;
				}
			}
		}

		private void dgRptDelivery_ColumnHeaderMouseClick ( object sender , DataGridViewCellMouseEventArgs e )
		{
			for (int i = 0 ; i < dgRptDelivery.Rows.Count ; i++)
			{
				dgRptDelivery.Rows[i].Cells["SNo"].Value = (i + 1);
				if (dgRptDelivery.Rows[i].Cells["ProcessID"].Value.ToString().Equals("8"))
				{
					dgRptDelivery.Rows[i].DefaultCellStyle.BackColor = Color.LightSeaGreen;
				}
				else if (dgRptDelivery.Rows[i].Cells["ProcessID"].Value.ToString().Equals("7"))
				{
					dgRptDelivery.Rows[i].DefaultCellStyle.BackColor = Color.CornflowerBlue;
				}
			}
		}

		private void btnReport_Click ( object sender , EventArgs e )
		{
            //int count = 0;
            //string StrPRID = "";
            //string StrBookingID = "";
            //string StrTestID = "";

            //for (int i = 0 ; i < dgRptDelivery.Rows.Count ; i++)
            //{
            //    if (dgRptDelivery.Rows[i].Cells["SelectAll"].Value.ToString().Equals("1"))
            //    {
            //        ////if (Convert.ToInt32(gvResult.Rows[i].Cells[5].Text.Trim().Replace("-" , "0")) > 0)
            //        ////{
            //        ////    ScriptManager.RegisterStartupScript(this , typeof(Page) , "warning" , "<script language='javascript'>alert('Please clear dues first');</script>" , false);
            //        ////    return;
            //        ////}
            //        //if (count == 0)
            //        //{
            //        //    StrPRID = dgRptDelivery.Rows[i].Cells["PRID"].Value.ToString();
            //        //    StrBookingID = dgRptDelivery.Rows[i].Cells["BookingID"].Value.ToString();
            //        //}
            //        //if (count > 0 && StrPRID != dgRptDelivery.Rows[i].Cells["PRID"].Value.ToString() && StrBookingID != dgRptDelivery.Rows[i].Cells["BookingID"].Value.ToString())
            //        //{
            //        //    MessageBox.Show("Please select same lab id." , "" , MessageBoxButtons.OK , MessageBoxIcon.Information);
            //        //    return;
            //        //}
            //        //else
            //        //{
            //        count++;
            //        //}
            //    }
            //}
            //if (count == 0)
            //{
            //    MessageBox.Show("Please select patient first" , "" , MessageBoxButtons.OK , MessageBoxIcon.Information);
            //}
            //else
            //{
            //    clsBLDispatch ds = new clsBLDispatch();

            //    ds.EnteredBy = clsSharedVariables.PersonID;
            //    ds.EnteredOn = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
            //    ds.ClientID = clsSharedVariables.OrgID;

            //    string[,] str = new string[count , 5];
            //    count = 0;
            //    for (int i = 0 ; i < dgRptDelivery.Rows.Count ; i++)
            //    {
            //        if (dgRptDelivery.Rows[i].Cells["SelectAll"].Value.ToString().Equals("1"))
            //        {
            //            str[count , 0] = dgRptDelivery.Rows[i].Cells["PRID"].Value.ToString();//prid
            //            str[count , 1] = dgRptDelivery.Rows[i].Cells["BookingID"].Value.ToString(); //bookingid
            //            str[count , 2] = dgRptDelivery.Rows[i].Cells["TestID"].Value.ToString(); // testid
            //            str[count , 3] = dgRptDelivery.Rows[i].Cells["LabID"].Value.ToString(); //labid
            //            str[count , 4] = dgRptDelivery.Rows[i].Cells["ProcessID"].Value.ToString(); //processid

            //            if (count == 0)
            //            {
            //                StrPRID = dgRptDelivery.Rows[i].Cells["PRID"].Value.ToString();//prid
            //                StrBookingID = dgRptDelivery.Rows[i].Cells["BookingID"].Value.ToString();//BookingID
            //                StrTestID = dgRptDelivery.Rows[i].Cells["TestID"].Value.ToString();//TestID
            //            }
            //            else
            //            {
            //                StrPRID = StrPRID + "," + dgRptDelivery.Rows[i].Cells["PRID"].Value.ToString();//prid
            //                StrBookingID = StrBookingID + "," + dgRptDelivery.Rows[i].Cells["BookingID"].Value.ToString();//BookingID
            //                StrTestID = StrTestID + "," + dgRptDelivery.Rows[i].Cells["TestID"].Value.ToString();//TestID
            //            }

            //            count++;
            //        }
            //    }

				
            //    if (ds.Dispatch(str))
            //    {
            //        DataView dv = new DataView();
            //        DataView dvSub = new DataView();
            //        clsBLDispatch objDis = new clsBLDispatch();

            //        objDis.PRID = StrPRID;//prid
            //        objDis.BookingID = StrBookingID; //bookingid

            //        dv = objDis.GetAll(8);
            //        dvSub = objDis.GetAll(9);

            //        string sSelectionFormula = "";
            //        sSelectionFormula = "{general_result_main.prid} in [" + StrPRID + "] and {general_result_main.testid} in [" + StrTestID + "] and {general_result_main.bookingid} in [" + StrBookingID + "]";
            //        ReportViewer objRptViewer = new ReportViewer();

            //        objRptViewer.dt = dv.Table;
            //        objRptViewer.dtSub = dvSub.Table;
            //        objRptViewer.StrSubTableName = "one_column_1";

            //        objRptViewer.StrSelectionFormula = sSelectionFormula;

            //        objRptViewer.ShowDialog();

            //        //uncheck all checked checkbox
            //        for (int i = 0 ; i < dgRptDelivery.Rows.Count ; i++)
            //        {
            //            dgRptDelivery.Rows[i].Cells["SelectAll"].Value = "0";
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show(ds.Error , "" , MessageBoxButtons.OK , MessageBoxIcon.Information);
            //    }
            //}
		}

        private void FillPatientInfo()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();

            objPatientReg.PatientID = _PatientID;
            objConnection.Connection_Open();
            dv = objPatientReg.GetAll(16);
            objConnection.Connection_Close();

            if (dv.Table.Rows.Count != 0)
            {
                txtPRNo.Text = dv.Table.Rows[0]["prno"].ToString();
                txtPName.Text = dv.Table.Rows[0]["PName"].ToString();
                txtGender.Text = dv.Table.Rows[0]["gender"].ToString().Equals("M") ? "Male" : "Female";
                txtAge.Text = dv.Table.Rows[0]["age"].ToString();
                _PRNO = dv.Table.Rows[0]["RefPrno"].ToString();
            }

            objPatientReg = null;
            objConnection = null;
        }

        private void LIMSReportStation_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objOPD != null)
            {
                objOPD.Show();
                this.Dispose();
            }
        }
	}
}