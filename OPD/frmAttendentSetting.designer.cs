namespace OPD
{
  partial class frmAttendentSetting
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblWorkingDays = new System.Windows.Forms.Label();
      this.pnlDays = new System.Windows.Forms.Panel();
      this.chbSaturday = new System.Windows.Forms.CheckBox();
      this.chbFriday = new System.Windows.Forms.CheckBox();
      this.chbThursday = new System.Windows.Forms.CheckBox();
      this.chbWednesday = new System.Windows.Forms.CheckBox();
      this.chbTuesday = new System.Windows.Forms.CheckBox();
      this.chbMonday = new System.Windows.Forms.CheckBox();
      this.chbSunday = new System.Windows.Forms.CheckBox();
      this.btnDaysSave = new System.Windows.Forms.Button();
      this.lblConsultant = new System.Windows.Forms.Label();
      this.pnlDays.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblWorkingDays
      // 
      this.lblWorkingDays.AutoSize = true;
      this.lblWorkingDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblWorkingDays.Location = new System.Drawing.Point(12, 35);
      this.lblWorkingDays.Name = "lblWorkingDays";
      this.lblWorkingDays.Size = new System.Drawing.Size(98, 13);
      this.lblWorkingDays.TabIndex = 5;
      this.lblWorkingDays.Text = "Working Days : ";
      // 
      // pnlDays
      // 
      this.pnlDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlDays.Controls.Add(this.chbSaturday);
      this.pnlDays.Controls.Add(this.chbFriday);
      this.pnlDays.Controls.Add(this.chbThursday);
      this.pnlDays.Controls.Add(this.chbWednesday);
      this.pnlDays.Controls.Add(this.chbTuesday);
      this.pnlDays.Controls.Add(this.chbMonday);
      this.pnlDays.Controls.Add(this.chbSunday);
      this.pnlDays.Location = new System.Drawing.Point(12, 51);
      this.pnlDays.Name = "pnlDays";
      this.pnlDays.Size = new System.Drawing.Size(553, 32);
      this.pnlDays.TabIndex = 4;
      // 
      // chbSaturday
      // 
      this.chbSaturday.AutoSize = true;
      this.chbSaturday.Location = new System.Drawing.Point(403, 7);
      this.chbSaturday.Name = "chbSaturday";
      this.chbSaturday.Size = new System.Drawing.Size(68, 17);
      this.chbSaturday.TabIndex = 1;
      this.chbSaturday.Text = "Saturday";
      this.chbSaturday.UseVisualStyleBackColor = true;
      // 
      // chbFriday
      // 
      this.chbFriday.AutoSize = true;
      this.chbFriday.Checked = true;
      this.chbFriday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbFriday.Location = new System.Drawing.Point(338, 7);
      this.chbFriday.Name = "chbFriday";
      this.chbFriday.Size = new System.Drawing.Size(54, 17);
      this.chbFriday.TabIndex = 5;
      this.chbFriday.Text = "Friday";
      this.chbFriday.UseVisualStyleBackColor = true;
      // 
      // chbThursday
      // 
      this.chbThursday.AutoSize = true;
      this.chbThursday.Checked = true;
      this.chbThursday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbThursday.Location = new System.Drawing.Point(257, 7);
      this.chbThursday.Name = "chbThursday";
      this.chbThursday.Size = new System.Drawing.Size(70, 17);
      this.chbThursday.TabIndex = 4;
      this.chbThursday.Text = "Thursday";
      this.chbThursday.UseVisualStyleBackColor = true;
      // 
      // chbWednesday
      // 
      this.chbWednesday.AutoSize = true;
      this.chbWednesday.Checked = true;
      this.chbWednesday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbWednesday.Location = new System.Drawing.Point(163, 7);
      this.chbWednesday.Name = "chbWednesday";
      this.chbWednesday.Size = new System.Drawing.Size(83, 17);
      this.chbWednesday.TabIndex = 3;
      this.chbWednesday.Text = "Wednesday";
      this.chbWednesday.UseVisualStyleBackColor = true;
      // 
      // chbTuesday
      // 
      this.chbTuesday.AutoSize = true;
      this.chbTuesday.Checked = true;
      this.chbTuesday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbTuesday.Location = new System.Drawing.Point(82, 7);
      this.chbTuesday.Name = "chbTuesday";
      this.chbTuesday.Size = new System.Drawing.Size(70, 17);
      this.chbTuesday.TabIndex = 2;
      this.chbTuesday.Text = "Tuesday ";
      this.chbTuesday.UseVisualStyleBackColor = true;
      // 
      // chbMonday
      // 
      this.chbMonday.AutoSize = true;
      this.chbMonday.Checked = true;
      this.chbMonday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbMonday.Location = new System.Drawing.Point(7, 7);
      this.chbMonday.Name = "chbMonday";
      this.chbMonday.Size = new System.Drawing.Size(64, 17);
      this.chbMonday.TabIndex = 1;
      this.chbMonday.Text = "Monday";
      this.chbMonday.UseVisualStyleBackColor = true;
      // 
      // chbSunday
      // 
      this.chbSunday.AutoSize = true;
      this.chbSunday.Location = new System.Drawing.Point(482, 7);
      this.chbSunday.Name = "chbSunday";
      this.chbSunday.Size = new System.Drawing.Size(62, 17);
      this.chbSunday.TabIndex = 0;
      this.chbSunday.Text = "Sunday";
      this.chbSunday.UseVisualStyleBackColor = true;
      // 
      // btnDaysSave
      // 
      this.btnDaysSave.Location = new System.Drawing.Point(540, 23);
      this.btnDaysSave.Name = "btnDaysSave";
      this.btnDaysSave.Size = new System.Drawing.Size(25, 25);
      this.btnDaysSave.TabIndex = 6;
      this.btnDaysSave.UseVisualStyleBackColor = true;
      this.btnDaysSave.Click += new System.EventHandler(this.btnDaysSave_Click);
      // 
      // lblConsultant
      // 
      this.lblConsultant.AutoSize = true;
      this.lblConsultant.Location = new System.Drawing.Point(125, 13);
      this.lblConsultant.Name = "lblConsultant";
      this.lblConsultant.Size = new System.Drawing.Size(35, 13);
      this.lblConsultant.TabIndex = 7;
      this.lblConsultant.Tag = "display";
      this.lblConsultant.Text = "label1";
      // 
      // frmAttendentSetting
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(580, 346);
      this.Controls.Add(this.lblConsultant);
      this.Controls.Add(this.btnDaysSave);
      this.Controls.Add(this.lblWorkingDays);
      this.Controls.Add(this.pnlDays);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "frmAttendentSetting";
      this.Text = "Attendent Setting";
      this.Load += new System.EventHandler(this.frmAttendentSetting_Load);
      this.pnlDays.ResumeLayout(false);
      this.pnlDays.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblWorkingDays;
    private System.Windows.Forms.Panel pnlDays;
    private System.Windows.Forms.CheckBox chbSaturday;
    private System.Windows.Forms.CheckBox chbFriday;
    private System.Windows.Forms.CheckBox chbThursday;
    private System.Windows.Forms.CheckBox chbWednesday;
    private System.Windows.Forms.CheckBox chbTuesday;
    private System.Windows.Forms.CheckBox chbMonday;
    private System.Windows.Forms.CheckBox chbSunday;
    private System.Windows.Forms.Button btnDaysSave;
    private System.Windows.Forms.Label lblConsultant;
  }
}