using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmDrugInfo : Form
    {
        public frmDrugInfo()
        {
            InitializeComponent();
        }

        private string _MedID = "1355";//"1418";

        public string MedID
        {
            get { return _MedID; }
            set { _MedID = value; }
        }

        private void frmDrugInfo_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();

            objComp.ApplyStyleToControls(this);

            objConnection.Connection_Open();
            FillData(objConnection);
            objConnection.Connection_Close();

            objComp = null;
            objConnection = null;
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FillData(clsBLDBConnection objConnection)
        {
            DataView dv = new DataView();
            clsBLMedicine objMed = new clsBLMedicine(objConnection);

            objMed.MedicineID = _MedID;
            dv = objMed.GetAll(9);

            if (dv.Table.Rows.Count != 0)
            {
                txtMedName.Text = dv.Table.Rows[0]["Med_Name"].ToString();
                txtNature.Text = dv.Table.Rows[0]["NatureName"].ToString();
                txtQty.Text = dv.Table.Rows[0]["Brand_Qty"].ToString();
                txtUnit.Text = dv.Table.Rows[0]["UnitName"].ToString();
                txtCategory.Text = dv.Table.Rows[0]["DrugCatName"].ToString();
                txtSystem.Text = dv.Table.Rows[0]["SystemName"].ToString();
                txtGroup.Text = dv.Table.Rows[0]["GroupName"].ToString();
                txtPacking.Text = dv.Table.Rows[0]["PackName"].ToString();
                txtRoot.Text = dv.Table.Rows[0]["RootName"].ToString();

                chbRenal.Checked = dv.Table.Rows[0]["RENALDOSAGE_ADJUST"].ToString().Equals("Y") ? true : false;
                txtPatientInfo.Text = dv.Table.Rows[0]["PatientInfo"].ToString();
                txtDescription.Text = dv.Table.Rows[0]["DESCRIPTION"].ToString();
                txtMedNotes.Text = dv.Table.Rows[0]["MEDICAL_NOTES"].ToString();

                txtLactationStatus.Text = dv.Table.Rows[0]["LACTATION_STATUS"].ToString();
                txtPediatricSafety.Text = dv.Table.Rows[0]["PEDIATRIC_SAFETY"].ToString();
                txtIVAdministration.Text = dv.Table.Rows[0]["IV_ADMINISTRATION"].ToString();
                txtOverDosageTreat.Text = dv.Table.Rows[0]["OVERDOSAGE_TREAT"].ToString();

                GetManufacturerInfo(objConnection, dv.Table.Rows[0]["ItemNo"].ToString());
            }

            dv = objMed.GetAll(10);
            dgGeneric.DataSource = dv;

            if (dv.Table.Rows.Count != 0)
            {
                FillGenericInfo(objConnection, dv.Table.Rows[0]["GenericID"].ToString());
                GetDrugReaction(objConnection, dv.Table.Rows[0]["GenericID"].ToString());
            }

            GetAgeWiseDosage(objConnection);
            objMed = null;
        }

        private void FillGenericInfo(clsBLDBConnection objConnection, string GenerciID)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            DataView dv = new DataView();

            objMed.MedIDRef = GenerciID;
            dv = objMed.GetAll(11);
            GenSerNo(dgGeneric, "GSNo");

            if (dv.Table.Rows.Count != 0)
            {
                txtContraIndication.Text = dv.Table.Rows[0]["GC_ContraIndication"].ToString();
                txtPrecaution.Text = dv.Table.Rows[0]["GC_PreCaution"].ToString();
                txtAdverseEffect.Text = dv.Table.Rows[0]["GC_ADVERSEEFFECT"].ToString();
                txtIngredient.Text = dv.Table.Rows[0]["GC_INGREDIENTS"].ToString();
                GetDrugReaction(objConnection, GenerciID);
            }
        }

        private void GetManufacturerInfo(clsBLDBConnection objConnection, string ItemNo)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            DataView dv = new DataView();

            objMed.MedIDRef = ItemNo;
            dv = objMed.GetAll(12);
            dgManufacturer.DataSource = dv;

            GenSerNo(dgManufacturer, "MSNo");

            objMed = null;
        }

        private void dgGeneric_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();

                objConnection.Connection_Open();
                FillGenericInfo(objConnection, dgGeneric.Rows[e.RowIndex].Cells["GenericID"].Value.ToString());
                objConnection.Connection_Close();
            }
        }

        private void GetDrugReaction(clsBLDBConnection objConnection, string GenericID)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            DataView dv = new DataView();

            objMed.MedIDRef = GenericID;
            dv = objMed.GetAll(13);
            dgDrugReaction.DataSource = dv;

            GenSerNo(dgDrugReaction, "DRSNo");
            objMed = null;
        }

        private void GenSerNo(DataGridView dg, string colName)
        {
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                dg.Rows[i].Cells[colName].Value = (i + 1).ToString();
            }
        }

        private void btnExpend_Click(object sender, EventArgs e)
        {
            btnExpend.Tag = "1";
            pnlExtra.Size = new Size(304, 459);
            btnExpend.Visible = false;
            btnCol.Visible = true;
        }

        private void btnCol_Click(object sender, EventArgs e)
        {
            btnExpend.Tag = "0";

            pnlExtra.Size = new Size(304, 33);
            btnExpend.Visible = true;
            btnCol.Visible = false;
        }

        private void GetAgeWiseDosage(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            DataView dv = new DataView();

            objMed.MedicineID = _MedID;
            dv = objMed.GetAll(14);
            dgAgewiseDosage.DataSource = dv;

            //GenSerNo(dgAgewiseDosage, "ADSNo");
            objMed = null;
        }

    }
}