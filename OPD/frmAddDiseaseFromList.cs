using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;
using System.Reflection;
using Microsoft.Win32.SafeHandles;

namespace OPD
{
  public partial class frmAddDiseaseFromList : Form
  {
    private frmOPD objOPD = null;

    private string _History = "";
    private string _Complaints = "";
    private string _Signs = "";
    private string _Diagnosis = "";
    private bool _Flag = false;

    public frmAddDiseaseFromList()
    {
      InitializeComponent();
    }

    public frmAddDiseaseFromList(frmOPD objOPD)
    {
      InitializeComponent();
      this.objOPD = objOPD;
    }

    private void frmAddDiseaseFromList_Load(object sender, EventArgs e)
    {
      SComponents objComp = new SComponents();
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLDisease objDisease = new clsBLDisease(objConnection);
      DataView dv = new DataView();

      objComp.ApplyStyleToControls(this);

      objConnection.Connection_Open();

      this.Cursor = Cursors.WaitCursor;
      FillAllDisease(objConnection,"");
      FillSelectDisease(objConnection,"");

      objDisease.EnteredBy = "C";
      dv = objDisease.GetAll(4);

      objComp.FillComboBox(cboChap, dv, "CHAPBLOCKTITLE", "CHAPBLOCKID", true);

      objDisease.EnteredBy = "B";
      dv = objDisease.GetAll(4);
      objComp.FillComboBox(cboBlock, dv, "CHAPBLOCKTITLE", "CHAPBLOCKID", true);

      SetTypeName(objConnection);
      this.Cursor = Cursors.Default;

      _Flag = true;

      objConnection.Connection_Close();

      objComp = null;
      objConnection = null;
    }

    private void miExit_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void FillAllDisease(clsBLDBConnection objConnection,string DiseaseName)
    {
      clsBLDisease objDisease = new clsBLDisease(objConnection);

      dgAllDisease.SuspendLayout();

      if (!cboBlock.Text.Equals("Select") && !cboBlock.Text.Equals(""))
      {
        objDisease.EnteredBy = cboBlock.SelectedValue.ToString();
      }
      if (!cboChap.Text.Equals("Select") && !cboChap.Text.Equals(""))
      {
        objDisease.EnteredOn = cboChap.SelectedValue.ToString();
      }
      objDisease.DiseaseName= DiseaseName;

      dgAllDisease.DataSource = objDisease.GetAll(2).Table;

      dgAllDisease.ResumeLayout();

      objDisease = null;
    }

    private void FillSelectDisease(clsBLDBConnection objConnection,string DiseaseID)
    {
      clsBLDisease objDisease = new clsBLDisease(objConnection);

      dgSelectDisease.SuspendLayout();
      //dgSelectedDisease.DataSource = objDisease.GetAll(3).Table; ;
      objDisease.DiseaseName = DiseaseID;
      dgSelectDisease.DataSource = objDisease.GetAll(3).Table; ;

      dgSelectDisease.ResumeLayout();
      objDisease = null;
    }

    private void txtAllDiseaseSerach_TextChanged(object sender, EventArgs e)
    {
      if (_Flag)
      {
        clsBLDBConnection objConnection=new clsBLDBConnection();
        
        objConnection.Connection_Open();
        if (txtAllDiseaseSerach.Text.Trim().Equals("Click here to search ICD-10......"))
        {
          FillAllDisease(objConnection, "");
        }
        else
        {
          FillAllDisease(objConnection, txtAllDiseaseSerach.Text.Trim());
        }
        objConnection.Connection_Close();
        //for (int i = 0; i < dgAllDisease.Rows.Count; i++)
        //{
        //  if (dgAllDisease.Rows[i].Cells["AllDisaesName"].Value.ToString().ToUpper().Contains(txtAllDiseaseSerach.Text.ToUpper()))
        //  {
        //    dgAllDisease.Rows[i].Selected = true;
        //    dgAllDisease.FirstDisplayedScrollingRowIndex = i;
        //    break;
        //  }
        //}
      }
    }

    private void txtSelectDiseaseSearch_TextChanged(object sender, EventArgs e)
    {
      if (_Flag)
      {
        clsBLDBConnection objConnection = new clsBLDBConnection();

        objConnection.Connection_Open();

        if (txtSelectDiseaseSearch.Text.Equals("Click here to search Preferential Setting ......"))
        {
          FillSelectDisease(objConnection, "");
        }
        else
        {
          FillSelectDisease(objConnection, txtSelectDiseaseSearch.Text.Trim());
        }
        objConnection.Connection_Close();

        //for (int i = 0; i < dgSelectDisease.Rows.Count; i++)
        //{
        //  if (dgSelectDisease.Rows[i].Cells["SelPrefName"].Value.ToString().ToUpper().StartsWith(txtSelectDiseaseSearch.Text.ToUpper()))
        //  {
        //    //dgSelectDisease.Rows[i].Selected = true;
        //    //dgSelectDisease.FirstDisplayedScrollingRowIndex = i;
        //    //break;
        //  }
        //}
      }
    }

    private void btnSelection_Click(object sender, EventArgs e)
    {
      if (dgAllDisease.SelectedRows.Count != 0)
      {
        SelectDisease(dgAllDisease.SelectedRows[0].Index);
      }
    }

    private void SelectDiseaseOld(int RowIdx)
    {
      int Flag = 0;
      DataTable dt = (DataTable)dgSelectDisease.DataSource;

      for (int i = 0; i < dgSelectDisease.Rows.Count; i++)
      {
        if (dgAllDisease.Rows[RowIdx].Cells["AllDiseaseID"].Value.ToString().Equals(dgSelectDisease.Rows[i].Cells["DiseaseIDRef"].Value.ToString()))
        {
          Flag = 1;
          //MessageBox.Show("Disease Already Added ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          break;
        }
      }

      if (Flag == 0)
      {
        DataRow dr = dt.NewRow();

        dr[1] = dgAllDisease.Rows[RowIdx].Cells["AllDisaesName"].Value;
        dr[2] = dgAllDisease.Rows[RowIdx].Cells["AllHist"].Value;
        dr[3] = dgAllDisease.Rows[RowIdx].Cells["AllComplaints"].Value;
        dr[4] = dgAllDisease.Rows[RowIdx].Cells["AllSigns"].Value;
        dr[5] = dgAllDisease.Rows[RowIdx].Cells["AllDiagnosis"].Value;
        dr[6] = dgAllDisease.Rows[RowIdx].Cells["AllDiseaseID"].Value;
        dr[7] = "I";
        dt.Rows.Add(dr);

        dt.DefaultView.Sort = "PrefName";
        dgSelectDisease.DataSource = dt;
        formatGrid();
      }
    }

    private void SelectDisease(int RowIdx)
    {
      dgAllDisease.Rows[RowIdx].Cells["AllDisaesName"].Selected = true;
      DataTable dt = (DataTable)dgSelectDisease.DataSource;

      //for (int j = 0; j <= 3; j++)
      {
        if (dgAllDisease.Rows[RowIdx].Cells["AllHist"].Value.Equals("1"))
        {
          dt = AddDiseaseToGrid(RowIdx, _History, dt);
        }
        if (dgAllDisease.Rows[RowIdx].Cells["AllComplaints"].Value.Equals("1"))
        {
          dt = AddDiseaseToGrid(RowIdx, _Complaints, dt);
        }
        if (dgAllDisease.Rows[RowIdx].Cells["AllSigns"].Value.Equals("1"))
        {
          dt = AddDiseaseToGrid(RowIdx, _Signs, dt);
        }
        if (dgAllDisease.Rows[RowIdx].Cells["AllDiagnosis"].Value.Equals("1"))
        {
          dt = AddDiseaseToGrid(RowIdx, _Diagnosis, dt);
        }
      }

      dt.DefaultView.Sort = "PrefName";
      dgSelectDisease.DataSource = dt;
      formatGrid();
    }

    private DataTable AddDiseaseToGrid(int rowIdx, string TypeID, DataTable dt)
    {
      int Flag = 0;

      for (int i = 0; i < dgSelectDisease.Rows.Count; i++)
      {
        if (dgAllDisease.Rows[rowIdx].Cells["AllDiseaseID"].Value.ToString().Equals(dgSelectDisease.Rows[i].Cells["DiseaseRefID"].Value.ToString()) && (dgSelectDisease.Rows[i].Cells["SelType"].Value.ToString().Equals(TypeID)))
        {
          Flag = 1;
          //MessageBox.Show("Disease Already Added ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          break;
        }
      }

      if (Flag == 0)
      {
        DataRow dr = dt.NewRow();

        dr[0] = dgAllDisease.Rows[rowIdx].Cells["DiseaseCode"].Value;
        dr[2] = dgAllDisease.Rows[rowIdx].Cells["AllDisaesName"].Value;
        dr[3] = TypeID;
        dr[4] = dgAllDisease.Rows[rowIdx].Cells["AllDiseaseID"].Value;
        dr[5] = "I";
        dt.Rows.Add(dr);
      }
      return dt;
    }

    private void btnLink_Click(object sender, EventArgs e)
    {
      if (dgAllDisease.SelectedRows.Count != 0 && dgSelectDisease.SelectedRows.Count != 0)
      {
        dgSelectDisease.SelectedRows[0].Cells["DiseaseRefID"].Value = dgAllDisease.SelectedRows[0].Cells["AllDiseaseID"].Value;
        dgSelectDisease.SelectedRows[0].Cells["SelMode"].Value = "U";
        formatGrid();
      }
    }

    public void formatGrid()
    {
      for (int i = 0; i < dgSelectDisease.Rows.Count; i++)
      {
        dgSelectDisease.Rows[0].Selected = true;

        if (dgSelectDisease.Rows[i].Cells["SelMode"].Value.ToString().Equals("I"))
        {
          dgSelectDisease.Rows[i].DefaultCellStyle.BackColor = Color.BurlyWood;
        }
        if (dgSelectDisease.Rows[i].Cells["SelMode"].Value.ToString().Equals("U"))
        {
          dgSelectDisease.Rows[i].DefaultCellStyle.BackColor = Color.Cornsilk;
        }
      }
    }

    private void miRefresh_Click(object sender, EventArgs e)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();

      objConnection.Connection_Open();

      FillAllDisease(objConnection,"");
      FillSelectDisease(objConnection,"");
      ClearGridRows(dgLinkDisease);

      objConnection.Connection_Close();
    }

    private void dgAllDisease_MouseDown(object sender, MouseEventArgs e)
    {
      System.Windows.Forms.DataGridView.HitTestInfo hi;
      hi = dgAllDisease.HitTest(e.X, e.Y);

      if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
      {

        dgAllDisease.ContextMenuStrip.Items[0].Enabled = true;
        dgAllDisease.ContextMenuStrip.Items[1].Enabled = true;
        dgAllDisease.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
        dgAllDisease.Rows[hi.RowIndex].Selected = true;
      }
      else
      {
        dgAllDisease.ContextMenuStrip.Items[0].Enabled = false;
        dgAllDisease.ContextMenuStrip.Items[1].Enabled = false;
        dgAllDisease.ContextMenuStrip.Items[0].Tag = null;

        //dgSelectedList.ContextMenuStrip = null;
      }

      hi = null;
    }

    private void miAdd_Click(object sender, EventArgs e)
    {
      if (dgAllDisease.ContextMenuStrip.Items[0].Tag != null)
      {
        SelectDisease((int)dgAllDisease.ContextMenuStrip.Items[0].Tag);
      }
    }

    private void dgAllDisease_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != -1)
      {
        SelectDisease(e.RowIndex);
      }
    }

    private void SetTypeName(clsBLDBConnection objConnection)
    {
      clsBLPreference objPref = new clsBLPreference(objConnection);
      DataView dv = new DataView();

      objPref.TypeID = "1";
      dv = objPref.GetAll(7);

      if (dv.Table.Rows.Count == 0)
      {
        _History = "History";
      }
      else
      {
        _History = dv.Table.Rows[0]["Description"].ToString();
      }

      objPref.TypeID = "3";
      dv = objPref.GetAll(7);

      if (dv.Table.Rows.Count == 0)
      {
        _Complaints = "Presenting Complaints";
      }
      else
      {
        _Complaints = dv.Table.Rows[0]["Description"].ToString();
      }

      objPref.TypeID = "2";
      dv = objPref.GetAll(7);

      if (dv.Table.Rows.Count == 0)
      {
        _Diagnosis = "Diagnosis";
      }
      else
      {
        _Diagnosis = dv.Table.Rows[0]["Description"].ToString();
      }

      objPref.TypeID = "4";
      dv = objPref.GetAll(7);

      if (dv.Table.Rows.Count == 0)
      {
        _Signs = "Signs";
      }
      else
      {
        _Signs = dv.Table.Rows[0]["Description"].ToString();
      }
    }

    private void miSave_Click(object sender, EventArgs e)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPreference objPref = new clsBLPreference(objConnection);

      try
      {
        objConnection.Connection_Open();


        for (int i = 0; i < dgSelectDisease.Rows.Count; i++)
        {
          if (dgSelectDisease.Rows[i].Cells["SelMode"].Value.ToString().Equals("I"))
          {
            objConnection.Transaction_Begin();
            Insert(objPref, i);
            objConnection.Transaction_ComRoll();
          }
          else if (dgSelectDisease.Rows[i].Cells["SelMode"].Value.ToString().Equals("U"))
          {
            objConnection.Transaction_Begin();
            Update(objPref, i);
            objConnection.Transaction_ComRoll();
          }
        }
        FillSelectDisease(objConnection,"");
        FillAllDisease(objConnection,"");
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Disease Insertion Successfully", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objConnection.Connection_Close();

        objConnection = null;
        objPref = null;
      }
    }

    private void Insert(clsBLPreference objPref, int RowIdx)
    {
      objPref.Name = dgSelectDisease.Rows[RowIdx].Cells["SelPrefName"].Value.ToString();

      if (dgSelectDisease.Rows[RowIdx].Cells["SelType"].Value.ToString().Equals(_History))
      {
        objPref.TypeID = "1";
      }
      else if (dgSelectDisease.Rows[RowIdx].Cells["SelType"].Value.ToString().Equals(_Diagnosis))
      {
        objPref.TypeID = "2";
      }
      else if (dgSelectDisease.Rows[RowIdx].Cells["SelType"].Value.ToString().Equals(_Complaints))
      {
        objPref.TypeID = "3";
      }
      else if (dgSelectDisease.Rows[RowIdx].Cells["SelType"].Value.ToString().Equals(_Signs))
      {
        objPref.TypeID = "4";
      }

      objPref.Description = dgSelectDisease.Rows[RowIdx].Cells["SelType"].Value.ToString();
      objPref.Active = "1";
      objPref.EnteredBy = clsSharedVariables.UserID;
      objPref.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
      objPref.PersonID = clsSharedVariables.UserID;
      objPref.ClientID = clsSharedVariables.ClientID;
      objPref.DiseaseIDRef = dgSelectDisease.Rows[RowIdx].Cells["DiseaseRefID"].Value.ToString();
      objPref.NumTime = "0";

      objPref.Insert();
    }

    private void Update(clsBLPreference objPref, int RowIdx)
    {
      objPref.PrefID = dgSelectDisease.Rows[RowIdx].Cells["SelPrefID"].Value.ToString();

      objPref.EnteredBy = clsSharedVariables.UserID;
      objPref.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
      objPref.PersonID = clsSharedVariables.UserID;
      objPref.DiseaseIDRef = dgSelectDisease.Rows[RowIdx].Cells["DiseaseRefID"].Value.ToString();

      objPref.Update();
    }

    private void frmDiseaseInfo_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (objOPD != null)
      {
        objOPD.Show();
        this.Dispose();
      }
    }

    private void dgSelectDisease_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != -1)
      {
        if (!dgSelectDisease.Rows[e.RowIndex].Cells["DiseaseRefID"].Value.ToString().Equals(""))
        {
          for (int i = 0; i < dgAllDisease.Rows.Count; i++)
          {
            if (dgAllDisease.Rows[i].Cells["AllDiseaseID"].Value.ToString().Equals(dgSelectDisease.Rows[e.RowIndex].Cells["DiseaseRefID"].Value.ToString()))
            {
              dgAllDisease.Rows[i].Selected = true;
              dgAllDisease.FirstDisplayedScrollingRowIndex = i;
              dgAllDisease.RowsDefaultCellStyle.SelectionBackColor = Color.Coral;
              return;
            }
          }
        }
        //if (dgAllDisease.Rows.Count > 0)
        //{
        //  dgAllDisease.FirstDisplayedScrollingRowIndex = 0;
        //}
        dgAllDisease.ClearSelection();
      }
    }

    private void dgAllDisease_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != -1)
      {
        clsBLDBConnection objConnection = new clsBLDBConnection();
        clsBLDisease objDisease = new clsBLDisease(objConnection);

        try
        {
          objConnection.Connection_Open();
          objDisease.DiseaseID = dgAllDisease.Rows[e.RowIndex].Cells["AllDiseaseID"].Value.ToString();
          dgLinkDisease.DataSource = objDisease.GetAll(3).Table;
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
        }
        finally
        {
          objConnection.Connection_Close();
          objDisease = null;
          objConnection = null;
        }
      }
    }

    private void ClearGridRows(DataGridView dg)
    {
      try
      {
        DataView dv = (DataView)dg.DataSource;

        if (dv != null)
        {
          dv.Table.Rows.Clear();
          dg.DataSource = dv;
        }
      }
      catch
      {
        DataTable dt = (DataTable)dg.DataSource;

        if (dt != null)
        {
          dt.Rows.Clear();
          dg.DataSource = dt;
        }
      }
      //while (dg.Rows.Count > 0)
      //{
      //  dg.Rows.RemoveAt(0);
      //}
    }

    private void dgAllDisease_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      if (_Flag)
      {
        if (e.RowIndex != -1)
        {
          clsBLDBConnection objConnection = new clsBLDBConnection();
          clsBLDisease objDisease = new clsBLDisease(objConnection);

          try
          {
            objConnection.Connection_Open();
            objDisease.DiseaseID = dgAllDisease.Rows[e.RowIndex].Cells["AllDiseaseID"].Value.ToString();
            dgLinkDisease.DataSource = objDisease.GetAll(3).Table;
          }
          catch (Exception exc)
          {
            MessageBox.Show(exc.Message);
          }
          finally
          {
            objConnection.Connection_Close();
            objDisease = null;
            objConnection = null;
          }
        }
      }
    }

    private void cboBlock_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (_Flag)
      {
        clsBLDBConnection objConnection = new clsBLDBConnection();
        objConnection.Connection_Open();
        FillAllDisease(objConnection,"");
        objConnection.Connection_Close();
      }
    }

    private void cboChap_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (_Flag)
      {
        clsBLDBConnection objConnection = new clsBLDBConnection();
        clsBLDisease objDisease = new clsBLDisease(objConnection);
        DataView dv = new DataView();
        SComponents objComp = new SComponents();

        objConnection.Connection_Open();

        objDisease.EnteredBy = "B";
        if (!cboChap.Text.Equals("Select") && !cboChap.Text.Equals(""))
        {
          objDisease.OPDID = cboChap.SelectedValue.ToString();
        }
        dv = objDisease.GetAll(4);
        objComp.FillComboBox(cboBlock, dv, "CHAPBLOCKTITLE", "CHAPBLOCKID", true);

        dv = null;
        objDisease = null;
        objComp = null;

        FillAllDisease(objConnection,"");

        objConnection.Connection_Close();
      }
    }

    private void txt_Click(object sender, EventArgs e)
    {
      TextBox txt = (TextBox)sender;
      if (txt.Text.Trim().Equals("Click here to search ICD-10......") || txt.Text.Trim().Equals("Click here to search Preferential Setting ......"))
      {
        txt.Text = "";
      }
    }

    private void txt_Leave(object sender, EventArgs e)
    {
      TextBox txt = (TextBox)sender;
      if (txt.Text.Trim().Equals(""))
      {
        if (txt.Name.Equals("txtAllDiseaseSerach"))
        {
          txt.Text = "Click here to search ICD-10......";
        }
        else if (txt.Name.Equals("txtSelectDiseaseSearch"))
        {
          txt.Text = "Click here to search Preferential Setting ......";
        }
      }
    }
  }
}