namespace OPD
{
    partial class frmLoginLic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLoginLic));
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtloginID = new System.Windows.Forms.TextBox();
            this.txtPasword = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbtnClinicNews = new System.Windows.Forms.LinkLabel();
            this.lbtnAbout = new System.Windows.Forms.LinkLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.chbRemember = new System.Windows.Forms.CheckBox();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.AutoSize = true;
            this.btnLogin.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogin.BackgroundImage")));
            this.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLogin.Location = new System.Drawing.Point(655, 243);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(53, 18);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtloginID
            // 
            this.txtloginID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtloginID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtloginID.Location = new System.Drawing.Point(626, 191);
            this.txtloginID.Name = "txtloginID";
            this.txtloginID.Size = new System.Drawing.Size(85, 20);
            this.txtloginID.TabIndex = 0;
            this.txtloginID.Tag = "notset";
            this.txtloginID.Text = "Sarim";
            this.txtloginID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPasword_KeyPress);
            this.txtloginID.Validating += new System.ComponentModel.CancelEventHandler(this.txtloginID_Validating);
            // 
            // txtPasword
            // 
            this.txtPasword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPasword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasword.Location = new System.Drawing.Point(626, 217);
            this.txtPasword.Name = "txtPasword";
            this.txtPasword.PasswordChar = '*';
            this.txtPasword.Size = new System.Drawing.Size(85, 20);
            this.txtPasword.TabIndex = 1;
            this.txtPasword.Tag = "Notset";
            this.txtPasword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPasword_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(707, 191);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(4, 20);
            this.panel1.TabIndex = 147;
            this.panel1.Tag = "no";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(707, 217);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 20);
            this.panel2.TabIndex = 148;
            this.panel2.Tag = "no";
            // 
            // lbtnClinicNews
            // 
            this.lbtnClinicNews.ActiveLinkColor = System.Drawing.Color.Black;
            this.lbtnClinicNews.AutoSize = true;
            this.lbtnClinicNews.BackColor = System.Drawing.Color.White;
            this.lbtnClinicNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnClinicNews.ForeColor = System.Drawing.Color.Black;
            this.lbtnClinicNews.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lbtnClinicNews.LinkColor = System.Drawing.Color.LimeGreen;
            this.lbtnClinicNews.Location = new System.Drawing.Point(652, 316);
            this.lbtnClinicNews.Name = "lbtnClinicNews";
            this.lbtnClinicNews.Size = new System.Drawing.Size(71, 15);
            this.lbtnClinicNews.TabIndex = 7;
            this.lbtnClinicNews.TabStop = true;
            this.lbtnClinicNews.Tag = "notset";
            this.lbtnClinicNews.Text = "ClinicNews ";
            this.lbtnClinicNews.VisitedLinkColor = System.Drawing.Color.Green;
            this.lbtnClinicNews.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbtnClinicNews_LinkClicked);
            this.lbtnClinicNews.MouseLeave += new System.EventHandler(this.lbtnGreen_MouseLeave);
            this.lbtnClinicNews.MouseHover += new System.EventHandler(this.lbtnBlack_MouseHover);
            // 
            // lbtnAbout
            // 
            this.lbtnAbout.ActiveLinkColor = System.Drawing.Color.Black;
            this.lbtnAbout.AutoSize = true;
            this.lbtnAbout.BackColor = System.Drawing.Color.White;
            this.lbtnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnAbout.ForeColor = System.Drawing.Color.Black;
            this.lbtnAbout.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lbtnAbout.LinkColor = System.Drawing.Color.LimeGreen;
            this.lbtnAbout.Location = new System.Drawing.Point(591, 316);
            this.lbtnAbout.Name = "lbtnAbout";
            this.lbtnAbout.Size = new System.Drawing.Size(38, 15);
            this.lbtnAbout.TabIndex = 6;
            this.lbtnAbout.TabStop = true;
            this.lbtnAbout.Tag = "notset";
            this.lbtnAbout.Text = "About";
            this.lbtnAbout.VisitedLinkColor = System.Drawing.Color.Green;
            this.lbtnAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbtnAbout_LinkClicked);
            this.lbtnAbout.MouseLeave += new System.EventHandler(this.lbtnGreen_MouseLeave);
            this.lbtnAbout.MouseHover += new System.EventHandler(this.lbtnBlack_MouseHover);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.lblVersion);
            this.panel3.Controls.Add(this.chbRemember);
            this.panel3.Controls.Add(this.txtloginID);
            this.panel3.Controls.Add(this.btnLogin);
            this.panel3.Controls.Add(this.txtPasword);
            this.panel3.Controls.Add(this.lbtnAbout);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.lbtnClinicNews);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 411);
            this.panel3.TabIndex = 153;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.White;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblVersion.Location = new System.Drawing.Point(49, 316);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(205, 13);
            this.lblVersion.TabIndex = 149;
            this.lblVersion.Tag = "no";
            this.lblVersion.Text = "Version # H-CM-1.0.2  Date : 10/10/2020";
            // 
            // chbRemember
            // 
            this.chbRemember.BackColor = System.Drawing.Color.Transparent;
            this.chbRemember.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbRemember.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbRemember.ForeColor = System.Drawing.Color.Indigo;
            this.chbRemember.Location = new System.Drawing.Point(562, 236);
            this.chbRemember.Name = "chbRemember";
            this.chbRemember.Size = new System.Drawing.Size(89, 32);
            this.chbRemember.TabIndex = 2;
            this.chbRemember.Tag = "trans";
            this.chbRemember.Text = "Remember Pwd";
            this.chbRemember.UseVisualStyleBackColor = false;
            // 
            // frmLoginLic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(794, 411);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "frmLoginLic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.frmLoginLic_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtloginID;
        private System.Windows.Forms.TextBox txtPasword;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel lbtnClinicNews;
        private System.Windows.Forms.LinkLabel lbtnAbout;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chbRemember;
        private System.Windows.Forms.Label lblVersion;
    }
}