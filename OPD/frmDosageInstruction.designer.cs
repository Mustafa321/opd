namespace OPD
{
	partial class frmDosageInstruction
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDosageInstruction));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDosageInstruction = new System.Windows.Forms.Label();
            this.tsDosageInstruction = new System.Windows.Forms.ToolStrip();
            this.tsbSave = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbExit = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dgDosageInstruction = new System.Windows.Forms.DataGridView();
            this.DosageInstructionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PesonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn2020 = new System.Windows.Forms.Button();
            this.btn2021 = new System.Windows.Forms.Button();
            this.btn2022 = new System.Windows.Forms.Button();
            this.btn2026 = new System.Windows.Forms.Button();
            this.btn2219 = new System.Windows.Forms.Button();
            this.btn2030 = new System.Windows.Forms.Button();
            this.btn2122 = new System.Windows.Forms.Button();
            this.btn2039 = new System.Windows.Forms.Button();
            this.btn203A = new System.Windows.Forms.Button();
            this.btn201E = new System.Windows.Forms.Button();
            this.btn201D = new System.Windows.Forms.Button();
            this.btn201C = new System.Windows.Forms.Button();
            this.btn201A = new System.Windows.Forms.Button();
            this.btn2019 = new System.Windows.Forms.Button();
            this.btn2018 = new System.Windows.Forms.Button();
            this.btn2DC = new System.Windows.Forms.Button();
            this.btn2C6 = new System.Windows.Forms.Button();
            this.btnED = new System.Windows.Forms.Button();
            this.btnEE = new System.Windows.Forms.Button();
            this.btnEF = new System.Windows.Forms.Button();
            this.btn192 = new System.Windows.Forms.Button();
            this.btnF0 = new System.Windows.Forms.Button();
            this.btn161 = new System.Windows.Forms.Button();
            this.btnF1 = new System.Windows.Forms.Button();
            this.btn160 = new System.Windows.Forms.Button();
            this.btn152 = new System.Windows.Forms.Button();
            this.btn153 = new System.Windows.Forms.Button();
            this.btnD3 = new System.Windows.Forms.Button();
            this.btnD4 = new System.Windows.Forms.Button();
            this.btnEC = new System.Windows.Forms.Button();
            this.btnD5 = new System.Windows.Forms.Button();
            this.btnEB = new System.Windows.Forms.Button();
            this.btnD6 = new System.Windows.Forms.Button();
            this.btnEA = new System.Windows.Forms.Button();
            this.btnD7 = new System.Windows.Forms.Button();
            this.btnE9 = new System.Windows.Forms.Button();
            this.btnD8 = new System.Windows.Forms.Button();
            this.btnE8 = new System.Windows.Forms.Button();
            this.btnE0 = new System.Windows.Forms.Button();
            this.btnE7 = new System.Windows.Forms.Button();
            this.btnD9 = new System.Windows.Forms.Button();
            this.btnE6 = new System.Windows.Forms.Button();
            this.btnDA = new System.Windows.Forms.Button();
            this.btnDB = new System.Windows.Forms.Button();
            this.btnE5 = new System.Windows.Forms.Button();
            this.btnDC = new System.Windows.Forms.Button();
            this.btnE4 = new System.Windows.Forms.Button();
            this.btnDD = new System.Windows.Forms.Button();
            this.btnE3 = new System.Windows.Forms.Button();
            this.btnDE = new System.Windows.Forms.Button();
            this.btnE2 = new System.Windows.Forms.Button();
            this.btnDF = new System.Windows.Forms.Button();
            this.btnE1 = new System.Windows.Forms.Button();
            this.btnC9 = new System.Windows.Forms.Button();
            this.btnCA = new System.Windows.Forms.Button();
            this.btnCB = new System.Windows.Forms.Button();
            this.btnD2 = new System.Windows.Forms.Button();
            this.btnCC = new System.Windows.Forms.Button();
            this.btnD1 = new System.Windows.Forms.Button();
            this.btnCD = new System.Windows.Forms.Button();
            this.btnD0 = new System.Windows.Forms.Button();
            this.btnCE = new System.Windows.Forms.Button();
            this.btnCF = new System.Windows.Forms.Button();
            this.btnAF = new System.Windows.Forms.Button();
            this.btnB0 = new System.Windows.Forms.Button();
            this.btnC8 = new System.Windows.Forms.Button();
            this.btnB1 = new System.Windows.Forms.Button();
            this.btnC7 = new System.Windows.Forms.Button();
            this.btnB2 = new System.Windows.Forms.Button();
            this.btnC6 = new System.Windows.Forms.Button();
            this.btnB3 = new System.Windows.Forms.Button();
            this.btnC5 = new System.Windows.Forms.Button();
            this.btnB4 = new System.Windows.Forms.Button();
            this.btnC4 = new System.Windows.Forms.Button();
            this.btnB5 = new System.Windows.Forms.Button();
            this.btnC3 = new System.Windows.Forms.Button();
            this.btnB6 = new System.Windows.Forms.Button();
            this.btnC2 = new System.Windows.Forms.Button();
            this.btnB7 = new System.Windows.Forms.Button();
            this.btnB8 = new System.Windows.Forms.Button();
            this.btnC1 = new System.Windows.Forms.Button();
            this.btnB9 = new System.Windows.Forms.Button();
            this.btnC0 = new System.Windows.Forms.Button();
            this.btnBA = new System.Windows.Forms.Button();
            this.btnBF = new System.Windows.Forms.Button();
            this.btnBB = new System.Windows.Forms.Button();
            this.btnBE = new System.Windows.Forms.Button();
            this.btnBC = new System.Windows.Forms.Button();
            this.btnBD = new System.Windows.Forms.Button();
            this.btnSpace = new System.Windows.Forms.Button();
            this.btnBS = new System.Windows.Forms.Button();
            this.btnA5 = new System.Windows.Forms.Button();
            this.btnA6 = new System.Windows.Forms.Button();
            this.btnA7 = new System.Windows.Forms.Button();
            this.btnAE = new System.Windows.Forms.Button();
            this.btnA8 = new System.Windows.Forms.Button();
            this.btnAD = new System.Windows.Forms.Button();
            this.btnA9 = new System.Windows.Forms.Button();
            this.btnAC = new System.Windows.Forms.Button();
            this.btnAA = new System.Windows.Forms.Button();
            this.btnAB = new System.Windows.Forms.Button();
            this.btn68 = new System.Windows.Forms.Button();
            this.btn69 = new System.Windows.Forms.Button();
            this.btnA4 = new System.Windows.Forms.Button();
            this.btn6A = new System.Windows.Forms.Button();
            this.btnA3 = new System.Windows.Forms.Button();
            this.btn6B = new System.Windows.Forms.Button();
            this.btnA2 = new System.Windows.Forms.Button();
            this.btn6C = new System.Windows.Forms.Button();
            this.btnA1 = new System.Windows.Forms.Button();
            this.btn6D = new System.Windows.Forms.Button();
            this.btn7E = new System.Windows.Forms.Button();
            this.btn6E = new System.Windows.Forms.Button();
            this.btn7C = new System.Windows.Forms.Button();
            this.btn6F = new System.Windows.Forms.Button();
            this.btn7B = new System.Windows.Forms.Button();
            this.btn70 = new System.Windows.Forms.Button();
            this.btn71 = new System.Windows.Forms.Button();
            this.btn7A = new System.Windows.Forms.Button();
            this.btn72 = new System.Windows.Forms.Button();
            this.btn79 = new System.Windows.Forms.Button();
            this.btn73 = new System.Windows.Forms.Button();
            this.btn78 = new System.Windows.Forms.Button();
            this.btn74 = new System.Windows.Forms.Button();
            this.btn77 = new System.Windows.Forms.Button();
            this.btn75 = new System.Windows.Forms.Button();
            this.btn76 = new System.Windows.Forms.Button();
            this.btn5C = new System.Windows.Forms.Button();
            this.btn5F = new System.Windows.Forms.Button();
            this.btn60 = new System.Windows.Forms.Button();
            this.btn67 = new System.Windows.Forms.Button();
            this.btn61 = new System.Windows.Forms.Button();
            this.btn66 = new System.Windows.Forms.Button();
            this.btn62 = new System.Windows.Forms.Button();
            this.btn65 = new System.Windows.Forms.Button();
            this.btn63 = new System.Windows.Forms.Button();
            this.btn64 = new System.Windows.Forms.Button();
            this.btn41 = new System.Windows.Forms.Button();
            this.btn42 = new System.Windows.Forms.Button();
            this.btn5A = new System.Windows.Forms.Button();
            this.btn43 = new System.Windows.Forms.Button();
            this.btn59 = new System.Windows.Forms.Button();
            this.btn44 = new System.Windows.Forms.Button();
            this.btn58 = new System.Windows.Forms.Button();
            this.btn45 = new System.Windows.Forms.Button();
            this.btn57 = new System.Windows.Forms.Button();
            this.btn46 = new System.Windows.Forms.Button();
            this.btn56 = new System.Windows.Forms.Button();
            this.btn47 = new System.Windows.Forms.Button();
            this.btn55 = new System.Windows.Forms.Button();
            this.btn48 = new System.Windows.Forms.Button();
            this.btn54 = new System.Windows.Forms.Button();
            this.btn49 = new System.Windows.Forms.Button();
            this.btn4A = new System.Windows.Forms.Button();
            this.btn53 = new System.Windows.Forms.Button();
            this.btn4B = new System.Windows.Forms.Button();
            this.btn52 = new System.Windows.Forms.Button();
            this.btn4C = new System.Windows.Forms.Button();
            this.btn51 = new System.Windows.Forms.Button();
            this.btn4D = new System.Windows.Forms.Button();
            this.btn50 = new System.Windows.Forms.Button();
            this.btn4E = new System.Windows.Forms.Button();
            this.btn4F = new System.Windows.Forms.Button();
            this.chbActive = new System.Windows.Forms.CheckBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tsDosageInstruction.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDosageInstruction)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblDosageInstruction);
            this.panel1.Controls.Add(this.tsDosageInstruction);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(859, 61);
            this.panel1.TabIndex = 0;
            this.panel1.Tag = "top";
            // 
            // lblDosageInstruction
            // 
            this.lblDosageInstruction.AutoSize = true;
            this.lblDosageInstruction.Location = new System.Drawing.Point(341, 30);
            this.lblDosageInstruction.Name = "lblDosageInstruction";
            this.lblDosageInstruction.Size = new System.Drawing.Size(0, 13);
            this.lblDosageInstruction.TabIndex = 1;
            this.lblDosageInstruction.Visible = false;
            // 
            // tsDosageInstruction
            // 
            this.tsDosageInstruction.AutoSize = false;
            this.tsDosageInstruction.BackColor = System.Drawing.Color.Transparent;
            this.tsDosageInstruction.CanOverflow = false;
            this.tsDosageInstruction.Dock = System.Windows.Forms.DockStyle.None;
            this.tsDosageInstruction.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsDosageInstruction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSave,
            this.tsbRefresh,
            this.tsbExit});
            this.tsDosageInstruction.Location = new System.Drawing.Point(670, -1);
            this.tsDosageInstruction.Name = "tsDosageInstruction";
            this.tsDosageInstruction.Size = new System.Drawing.Size(198, 60);
            this.tsDosageInstruction.TabIndex = 0;
            this.tsDosageInstruction.Text = "toolStrip1";
            // 
            // tsbSave
            // 
            this.tsbSave.AutoSize = false;
            this.tsbSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.tsbSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
            this.tsbSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tsbSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSave.Name = "tsbSave";
            this.tsbSave.Size = new System.Drawing.Size(62, 56);
            this.tsbSave.Tag = "Save";
            this.tsbSave.Text = "&Save";
            this.tsbSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.AutoSize = false;
            this.tsbRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.tsbRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tsbRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(62, 56);
            this.tsbRefresh.Text = "&Refresh";
            this.tsbRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // tsbExit
            // 
            this.tsbExit.AutoSize = false;
            this.tsbExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.tsbExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tsbExit.Image = ((System.Drawing.Image)(resources.GetObject("tsbExit.Image")));
            this.tsbExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.tsbExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExit.Name = "tsbExit";
            this.tsbExit.Size = new System.Drawing.Size(62, 56);
            this.tsbExit.Text = "&Exit";
            this.tsbExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tsbExit.Click += new System.EventHandler(this.tsbExit_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dgDosageInstruction);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.chbActive);
            this.panel2.Controls.Add(this.txtDescription);
            this.panel2.Controls.Add(this.lblDescription);
            this.panel2.Location = new System.Drawing.Point(1, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(859, 607);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(329, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 25);
            this.label1.TabIndex = 156;
            this.label1.Tag = "No";
            this.label1.Text = "Dosage Instruction";
            // 
            // dgDosageInstruction
            // 
            this.dgDosageInstruction.AllowUserToAddRows = false;
            this.dgDosageInstruction.AllowUserToDeleteRows = false;
            this.dgDosageInstruction.AllowUserToOrderColumns = true;
            this.dgDosageInstruction.AllowUserToResizeColumns = false;
            this.dgDosageInstruction.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDosageInstruction.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDosageInstruction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDosageInstruction.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DosageInstructionID,
            this.Active,
            this.Description,
            this.PesonID,
            this.EnteredBy,
            this.EnteredOn,
            this.ClientID});
            this.dgDosageInstruction.Location = new System.Drawing.Point(1, 340);
            this.dgDosageInstruction.MultiSelect = false;
            this.dgDosageInstruction.Name = "dgDosageInstruction";
            this.dgDosageInstruction.ReadOnly = true;
            this.dgDosageInstruction.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgDosageInstruction.RowTemplate.Height = 35;
            this.dgDosageInstruction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDosageInstruction.Size = new System.Drawing.Size(855, 264);
            this.dgDosageInstruction.TabIndex = 155;
            this.dgDosageInstruction.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDosageInstruction_CellDoubleClick);
            // 
            // DosageInstructionID
            // 
            this.DosageInstructionID.DataPropertyName = "DosageInstructionID";
            this.DosageInstructionID.HeaderText = "DosageIstructionID";
            this.DosageInstructionID.Name = "DosageInstructionID";
            this.DosageInstructionID.ReadOnly = true;
            this.DosageInstructionID.Visible = false;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "N";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "Y";
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.Description.DefaultCellStyle = dataGridViewCellStyle2;
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 680;
            // 
            // PesonID
            // 
            this.PesonID.DataPropertyName = "PersonID";
            this.PesonID.HeaderText = "PesonID";
            this.PesonID.Name = "PesonID";
            this.PesonID.ReadOnly = true;
            this.PesonID.Visible = false;
            // 
            // EnteredBy
            // 
            this.EnteredBy.DataPropertyName = "EnteredBy";
            this.EnteredBy.HeaderText = "EnteredBy";
            this.EnteredBy.Name = "EnteredBy";
            this.EnteredBy.ReadOnly = true;
            this.EnteredBy.Visible = false;
            // 
            // EnteredOn
            // 
            this.EnteredOn.DataPropertyName = "EnteredOn";
            this.EnteredOn.HeaderText = "EnteredOn";
            this.EnteredOn.Name = "EnteredOn";
            this.EnteredOn.ReadOnly = true;
            this.EnteredOn.Visible = false;
            // 
            // ClientID
            // 
            this.ClientID.DataPropertyName = "ClientID";
            this.ClientID.HeaderText = "ClientID";
            this.ClientID.Name = "ClientID";
            this.ClientID.ReadOnly = true;
            this.ClientID.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn2020);
            this.panel6.Controls.Add(this.btn2021);
            this.panel6.Controls.Add(this.btn2022);
            this.panel6.Controls.Add(this.btn2026);
            this.panel6.Controls.Add(this.btn2219);
            this.panel6.Controls.Add(this.btn2030);
            this.panel6.Controls.Add(this.btn2122);
            this.panel6.Controls.Add(this.btn2039);
            this.panel6.Controls.Add(this.btn203A);
            this.panel6.Controls.Add(this.btn201E);
            this.panel6.Controls.Add(this.btn201D);
            this.panel6.Controls.Add(this.btn201C);
            this.panel6.Controls.Add(this.btn201A);
            this.panel6.Controls.Add(this.btn2019);
            this.panel6.Controls.Add(this.btn2018);
            this.panel6.Controls.Add(this.btn2DC);
            this.panel6.Controls.Add(this.btn2C6);
            this.panel6.Controls.Add(this.btnED);
            this.panel6.Controls.Add(this.btnEE);
            this.panel6.Controls.Add(this.btnEF);
            this.panel6.Controls.Add(this.btn192);
            this.panel6.Controls.Add(this.btnF0);
            this.panel6.Controls.Add(this.btn161);
            this.panel6.Controls.Add(this.btnF1);
            this.panel6.Controls.Add(this.btn160);
            this.panel6.Controls.Add(this.btn152);
            this.panel6.Controls.Add(this.btn153);
            this.panel6.Controls.Add(this.btnD3);
            this.panel6.Controls.Add(this.btnD4);
            this.panel6.Controls.Add(this.btnEC);
            this.panel6.Controls.Add(this.btnD5);
            this.panel6.Controls.Add(this.btnEB);
            this.panel6.Controls.Add(this.btnD6);
            this.panel6.Controls.Add(this.btnEA);
            this.panel6.Controls.Add(this.btnD7);
            this.panel6.Controls.Add(this.btnE9);
            this.panel6.Controls.Add(this.btnD8);
            this.panel6.Controls.Add(this.btnE8);
            this.panel6.Controls.Add(this.btnE0);
            this.panel6.Controls.Add(this.btnE7);
            this.panel6.Controls.Add(this.btnD9);
            this.panel6.Controls.Add(this.btnE6);
            this.panel6.Controls.Add(this.btnDA);
            this.panel6.Controls.Add(this.btnDB);
            this.panel6.Controls.Add(this.btnE5);
            this.panel6.Controls.Add(this.btnDC);
            this.panel6.Controls.Add(this.btnE4);
            this.panel6.Controls.Add(this.btnDD);
            this.panel6.Controls.Add(this.btnE3);
            this.panel6.Controls.Add(this.btnDE);
            this.panel6.Controls.Add(this.btnE2);
            this.panel6.Controls.Add(this.btnDF);
            this.panel6.Controls.Add(this.btnE1);
            this.panel6.Controls.Add(this.btnC9);
            this.panel6.Controls.Add(this.btnCA);
            this.panel6.Controls.Add(this.btnCB);
            this.panel6.Controls.Add(this.btnD2);
            this.panel6.Controls.Add(this.btnCC);
            this.panel6.Controls.Add(this.btnD1);
            this.panel6.Controls.Add(this.btnCD);
            this.panel6.Controls.Add(this.btnD0);
            this.panel6.Controls.Add(this.btnCE);
            this.panel6.Controls.Add(this.btnCF);
            this.panel6.Controls.Add(this.btnAF);
            this.panel6.Controls.Add(this.btnB0);
            this.panel6.Controls.Add(this.btnC8);
            this.panel6.Controls.Add(this.btnB1);
            this.panel6.Controls.Add(this.btnC7);
            this.panel6.Controls.Add(this.btnB2);
            this.panel6.Controls.Add(this.btnC6);
            this.panel6.Controls.Add(this.btnB3);
            this.panel6.Controls.Add(this.btnC5);
            this.panel6.Controls.Add(this.btnB4);
            this.panel6.Controls.Add(this.btnC4);
            this.panel6.Controls.Add(this.btnB5);
            this.panel6.Controls.Add(this.btnC3);
            this.panel6.Controls.Add(this.btnB6);
            this.panel6.Controls.Add(this.btnC2);
            this.panel6.Controls.Add(this.btnB7);
            this.panel6.Controls.Add(this.btnB8);
            this.panel6.Controls.Add(this.btnC1);
            this.panel6.Controls.Add(this.btnB9);
            this.panel6.Controls.Add(this.btnC0);
            this.panel6.Controls.Add(this.btnBA);
            this.panel6.Controls.Add(this.btnBF);
            this.panel6.Controls.Add(this.btnBB);
            this.panel6.Controls.Add(this.btnBE);
            this.panel6.Controls.Add(this.btnBC);
            this.panel6.Controls.Add(this.btnBD);
            this.panel6.Controls.Add(this.btnSpace);
            this.panel6.Controls.Add(this.btnBS);
            this.panel6.Controls.Add(this.btnA5);
            this.panel6.Controls.Add(this.btnA6);
            this.panel6.Controls.Add(this.btnA7);
            this.panel6.Controls.Add(this.btnAE);
            this.panel6.Controls.Add(this.btnA8);
            this.panel6.Controls.Add(this.btnAD);
            this.panel6.Controls.Add(this.btnA9);
            this.panel6.Controls.Add(this.btnAC);
            this.panel6.Controls.Add(this.btnAA);
            this.panel6.Controls.Add(this.btnAB);
            this.panel6.Controls.Add(this.btn68);
            this.panel6.Controls.Add(this.btn69);
            this.panel6.Controls.Add(this.btnA4);
            this.panel6.Controls.Add(this.btn6A);
            this.panel6.Controls.Add(this.btnA3);
            this.panel6.Controls.Add(this.btn6B);
            this.panel6.Controls.Add(this.btnA2);
            this.panel6.Controls.Add(this.btn6C);
            this.panel6.Controls.Add(this.btnA1);
            this.panel6.Controls.Add(this.btn6D);
            this.panel6.Controls.Add(this.btn7E);
            this.panel6.Controls.Add(this.btn6E);
            this.panel6.Controls.Add(this.btn7C);
            this.panel6.Controls.Add(this.btn6F);
            this.panel6.Controls.Add(this.btn7B);
            this.panel6.Controls.Add(this.btn70);
            this.panel6.Controls.Add(this.btn71);
            this.panel6.Controls.Add(this.btn7A);
            this.panel6.Controls.Add(this.btn72);
            this.panel6.Controls.Add(this.btn79);
            this.panel6.Controls.Add(this.btn73);
            this.panel6.Controls.Add(this.btn78);
            this.panel6.Controls.Add(this.btn74);
            this.panel6.Controls.Add(this.btn77);
            this.panel6.Controls.Add(this.btn75);
            this.panel6.Controls.Add(this.btn76);
            this.panel6.Controls.Add(this.btn5C);
            this.panel6.Controls.Add(this.btn5F);
            this.panel6.Controls.Add(this.btn60);
            this.panel6.Controls.Add(this.btn67);
            this.panel6.Controls.Add(this.btn61);
            this.panel6.Controls.Add(this.btn66);
            this.panel6.Controls.Add(this.btn62);
            this.panel6.Controls.Add(this.btn65);
            this.panel6.Controls.Add(this.btn63);
            this.panel6.Controls.Add(this.btn64);
            this.panel6.Controls.Add(this.btn41);
            this.panel6.Controls.Add(this.btn42);
            this.panel6.Controls.Add(this.btn5A);
            this.panel6.Controls.Add(this.btn43);
            this.panel6.Controls.Add(this.btn59);
            this.panel6.Controls.Add(this.btn44);
            this.panel6.Controls.Add(this.btn58);
            this.panel6.Controls.Add(this.btn45);
            this.panel6.Controls.Add(this.btn57);
            this.panel6.Controls.Add(this.btn46);
            this.panel6.Controls.Add(this.btn56);
            this.panel6.Controls.Add(this.btn47);
            this.panel6.Controls.Add(this.btn55);
            this.panel6.Controls.Add(this.btn48);
            this.panel6.Controls.Add(this.btn54);
            this.panel6.Controls.Add(this.btn49);
            this.panel6.Controls.Add(this.btn4A);
            this.panel6.Controls.Add(this.btn53);
            this.panel6.Controls.Add(this.btn4B);
            this.panel6.Controls.Add(this.btn52);
            this.panel6.Controls.Add(this.btn4C);
            this.panel6.Controls.Add(this.btn51);
            this.panel6.Controls.Add(this.btn4D);
            this.panel6.Controls.Add(this.btn50);
            this.panel6.Controls.Add(this.btn4E);
            this.panel6.Controls.Add(this.btn4F);
            this.panel6.Location = new System.Drawing.Point(2, 98);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(854, 241);
            this.panel6.TabIndex = 154;
            // 
            // btn2020
            // 
            this.btn2020.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2020.Location = new System.Drawing.Point(69, 204);
            this.btn2020.Name = "btn2020";
            this.btn2020.Size = new System.Drawing.Size(35, 35);
            this.btn2020.TabIndex = 159;
            this.btn2020.Tag = "urdu1";
            this.btn2020.Text = "O";
            this.btn2020.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2020.UseVisualStyleBackColor = true;
            this.btn2020.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2021
            // 
            this.btn2021.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2021.Location = new System.Drawing.Point(103, 204);
            this.btn2021.Name = "btn2021";
            this.btn2021.Size = new System.Drawing.Size(35, 35);
            this.btn2021.TabIndex = 160;
            this.btn2021.Tag = "urdu1";
            this.btn2021.Text = "P";
            this.btn2021.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2021.UseVisualStyleBackColor = true;
            this.btn2021.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2022
            // 
            this.btn2022.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2022.Location = new System.Drawing.Point(612, 204);
            this.btn2022.Name = "btn2022";
            this.btn2022.Size = new System.Drawing.Size(35, 35);
            this.btn2022.TabIndex = 161;
            this.btn2022.Tag = "urdu1";
            this.btn2022.Text = "A";
            this.btn2022.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2022.UseVisualStyleBackColor = true;
            this.btn2022.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2026
            // 
            this.btn2026.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2026.Location = new System.Drawing.Point(646, 204);
            this.btn2026.Name = "btn2026";
            this.btn2026.Size = new System.Drawing.Size(35, 35);
            this.btn2026.TabIndex = 162;
            this.btn2026.Tag = "urdu1";
            this.btn2026.Text = "S";
            this.btn2026.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2026.UseVisualStyleBackColor = true;
            this.btn2026.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2219
            // 
            this.btn2219.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2219.Location = new System.Drawing.Point(816, 204);
            this.btn2219.Name = "btn2219";
            this.btn2219.Size = new System.Drawing.Size(35, 35);
            this.btn2219.TabIndex = 167;
            this.btn2219.Tag = "urdu1";
            this.btn2219.Text = "J";
            this.btn2219.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2219.UseVisualStyleBackColor = true;
            this.btn2219.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2030
            // 
            this.btn2030.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2030.Location = new System.Drawing.Point(680, 204);
            this.btn2030.Name = "btn2030";
            this.btn2030.Size = new System.Drawing.Size(35, 35);
            this.btn2030.TabIndex = 163;
            this.btn2030.Tag = "urdu1";
            this.btn2030.Text = "D";
            this.btn2030.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2030.UseVisualStyleBackColor = true;
            this.btn2030.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2122
            // 
            this.btn2122.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2122.Location = new System.Drawing.Point(782, 204);
            this.btn2122.Name = "btn2122";
            this.btn2122.Size = new System.Drawing.Size(35, 35);
            this.btn2122.TabIndex = 166;
            this.btn2122.Tag = "urdu1";
            this.btn2122.Text = "H";
            this.btn2122.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2122.UseVisualStyleBackColor = true;
            this.btn2122.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2039
            // 
            this.btn2039.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2039.Location = new System.Drawing.Point(714, 204);
            this.btn2039.Name = "btn2039";
            this.btn2039.Size = new System.Drawing.Size(35, 35);
            this.btn2039.TabIndex = 164;
            this.btn2039.Tag = "urdu1";
            this.btn2039.Text = "F";
            this.btn2039.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2039.UseVisualStyleBackColor = true;
            this.btn2039.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn203A
            // 
            this.btn203A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn203A.Location = new System.Drawing.Point(748, 204);
            this.btn203A.Name = "btn203A";
            this.btn203A.Size = new System.Drawing.Size(35, 35);
            this.btn203A.TabIndex = 165;
            this.btn203A.Tag = "urdu1";
            this.btn203A.Text = "G";
            this.btn203A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn203A.UseVisualStyleBackColor = true;
            this.btn203A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn201E
            // 
            this.btn201E.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn201E.Location = new System.Drawing.Point(35, 204);
            this.btn201E.Name = "btn201E";
            this.btn201E.Size = new System.Drawing.Size(35, 35);
            this.btn201E.TabIndex = 158;
            this.btn201E.Tag = "urdu1";
            this.btn201E.Text = "M";
            this.btn201E.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn201E.UseVisualStyleBackColor = true;
            this.btn201E.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn201D
            // 
            this.btn201D.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn201D.Location = new System.Drawing.Point(0, 204);
            this.btn201D.Name = "btn201D";
            this.btn201D.Size = new System.Drawing.Size(35, 35);
            this.btn201D.TabIndex = 157;
            this.btn201D.Tag = "urdu1";
            this.btn201D.Text = "N";
            this.btn201D.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn201D.UseVisualStyleBackColor = true;
            this.btn201D.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn201C
            // 
            this.btn201C.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn201C.Location = new System.Drawing.Point(816, 170);
            this.btn201C.Name = "btn201C";
            this.btn201C.Size = new System.Drawing.Size(35, 35);
            this.btn201C.TabIndex = 156;
            this.btn201C.Tag = "urdu1";
            this.btn201C.Text = "B";
            this.btn201C.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn201C.UseVisualStyleBackColor = true;
            this.btn201C.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn201A
            // 
            this.btn201A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn201A.Location = new System.Drawing.Point(782, 170);
            this.btn201A.Name = "btn201A";
            this.btn201A.Size = new System.Drawing.Size(35, 35);
            this.btn201A.TabIndex = 155;
            this.btn201A.Tag = "urdu1";
            this.btn201A.Text = "V";
            this.btn201A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn201A.UseVisualStyleBackColor = true;
            this.btn201A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2019
            // 
            this.btn2019.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2019.Location = new System.Drawing.Point(748, 170);
            this.btn2019.Name = "btn2019";
            this.btn2019.Size = new System.Drawing.Size(35, 35);
            this.btn2019.TabIndex = 154;
            this.btn2019.Tag = "urdu1";
            this.btn2019.Text = "C";
            this.btn2019.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2019.UseVisualStyleBackColor = true;
            this.btn2019.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2018
            // 
            this.btn2018.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2018.Location = new System.Drawing.Point(714, 170);
            this.btn2018.Name = "btn2018";
            this.btn2018.Size = new System.Drawing.Size(35, 35);
            this.btn2018.TabIndex = 153;
            this.btn2018.Tag = "urdu1";
            this.btn2018.Text = "X";
            this.btn2018.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2018.UseVisualStyleBackColor = true;
            this.btn2018.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2DC
            // 
            this.btn2DC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2DC.Location = new System.Drawing.Point(680, 170);
            this.btn2DC.Name = "btn2DC";
            this.btn2DC.Size = new System.Drawing.Size(35, 35);
            this.btn2DC.TabIndex = 152;
            this.btn2DC.Tag = "urdu1";
            this.btn2DC.Text = "Z";
            this.btn2DC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2DC.UseVisualStyleBackColor = true;
            this.btn2DC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2C6
            // 
            this.btn2C6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn2C6.Location = new System.Drawing.Point(646, 170);
            this.btn2C6.Name = "btn2C6";
            this.btn2C6.Size = new System.Drawing.Size(35, 35);
            this.btn2C6.TabIndex = 151;
            this.btn2C6.Tag = "urdu1";
            this.btn2C6.Text = "L";
            this.btn2C6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn2C6.UseVisualStyleBackColor = true;
            this.btn2C6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnED
            // 
            this.btnED.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnED.Location = new System.Drawing.Point(272, 170);
            this.btnED.Name = "btnED";
            this.btnED.Size = new System.Drawing.Size(35, 35);
            this.btnED.TabIndex = 141;
            this.btnED.Tag = "urdu1";
            this.btnED.Text = "O";
            this.btnED.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnED.UseVisualStyleBackColor = true;
            this.btnED.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnEE
            // 
            this.btnEE.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnEE.Location = new System.Drawing.Point(306, 170);
            this.btnEE.Name = "btnEE";
            this.btnEE.Size = new System.Drawing.Size(35, 35);
            this.btnEE.TabIndex = 142;
            this.btnEE.Tag = "urdu1";
            this.btnEE.Text = "P";
            this.btnEE.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEE.UseVisualStyleBackColor = true;
            this.btnEE.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnEF
            // 
            this.btnEF.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnEF.Location = new System.Drawing.Point(340, 170);
            this.btnEF.Name = "btnEF";
            this.btnEF.Size = new System.Drawing.Size(35, 35);
            this.btnEF.TabIndex = 143;
            this.btnEF.Tag = "urdu1";
            this.btnEF.Text = "A";
            this.btnEF.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEF.UseVisualStyleBackColor = true;
            this.btnEF.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn192
            // 
            this.btn192.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn192.Location = new System.Drawing.Point(578, 170);
            this.btn192.Name = "btn192";
            this.btn192.Size = new System.Drawing.Size(35, 35);
            this.btn192.TabIndex = 150;
            this.btn192.Tag = "urdu1";
            this.btn192.Text = "K";
            this.btn192.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn192.UseVisualStyleBackColor = true;
            this.btn192.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnF0
            // 
            this.btnF0.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnF0.Location = new System.Drawing.Point(374, 170);
            this.btnF0.Name = "btnF0";
            this.btnF0.Size = new System.Drawing.Size(35, 35);
            this.btnF0.TabIndex = 144;
            this.btnF0.Tag = "urdu1";
            this.btnF0.Text = "S";
            this.btnF0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnF0.UseVisualStyleBackColor = true;
            this.btnF0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn161
            // 
            this.btn161.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn161.Location = new System.Drawing.Point(544, 170);
            this.btn161.Name = "btn161";
            this.btn161.Size = new System.Drawing.Size(35, 35);
            this.btn161.TabIndex = 149;
            this.btn161.Tag = "urdu1";
            this.btn161.Text = "J";
            this.btn161.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn161.UseVisualStyleBackColor = true;
            this.btn161.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnF1
            // 
            this.btnF1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnF1.Location = new System.Drawing.Point(408, 170);
            this.btnF1.Name = "btnF1";
            this.btnF1.Size = new System.Drawing.Size(35, 35);
            this.btnF1.TabIndex = 145;
            this.btnF1.Tag = "urdu1";
            this.btnF1.Text = "D";
            this.btnF1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnF1.UseVisualStyleBackColor = true;
            this.btnF1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn160
            // 
            this.btn160.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn160.Location = new System.Drawing.Point(510, 170);
            this.btn160.Name = "btn160";
            this.btn160.Size = new System.Drawing.Size(35, 35);
            this.btn160.TabIndex = 148;
            this.btn160.Tag = "urdu1";
            this.btn160.Text = "H";
            this.btn160.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn160.UseVisualStyleBackColor = true;
            this.btn160.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn152
            // 
            this.btn152.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn152.Location = new System.Drawing.Point(442, 170);
            this.btn152.Name = "btn152";
            this.btn152.Size = new System.Drawing.Size(35, 35);
            this.btn152.TabIndex = 146;
            this.btn152.Tag = "urdu1";
            this.btn152.Text = "F";
            this.btn152.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn152.UseVisualStyleBackColor = true;
            this.btn152.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn153
            // 
            this.btn153.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn153.Location = new System.Drawing.Point(476, 170);
            this.btn153.Name = "btn153";
            this.btn153.Size = new System.Drawing.Size(35, 35);
            this.btn153.TabIndex = 147;
            this.btn153.Tag = "urdu1";
            this.btn153.Text = "G";
            this.btn153.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn153.UseVisualStyleBackColor = true;
            this.btn153.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD3
            // 
            this.btnD3.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD3.Location = new System.Drawing.Point(272, 136);
            this.btnD3.Name = "btnD3";
            this.btnD3.Size = new System.Drawing.Size(35, 35);
            this.btnD3.TabIndex = 115;
            this.btnD3.Tag = "urdu1";
            this.btnD3.Text = "Q";
            this.btnD3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD3.UseVisualStyleBackColor = true;
            this.btnD3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD4
            // 
            this.btnD4.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD4.Location = new System.Drawing.Point(306, 136);
            this.btnD4.Name = "btnD4";
            this.btnD4.Size = new System.Drawing.Size(35, 35);
            this.btnD4.TabIndex = 116;
            this.btnD4.Tag = "urdu1";
            this.btnD4.Text = "W";
            this.btnD4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD4.UseVisualStyleBackColor = true;
            this.btnD4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnEC
            // 
            this.btnEC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnEC.Location = new System.Drawing.Point(238, 170);
            this.btnEC.Name = "btnEC";
            this.btnEC.Size = new System.Drawing.Size(35, 35);
            this.btnEC.TabIndex = 140;
            this.btnEC.Tag = "urdu1";
            this.btnEC.Text = "M";
            this.btnEC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEC.UseVisualStyleBackColor = true;
            this.btnEC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD5
            // 
            this.btnD5.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD5.Location = new System.Drawing.Point(340, 136);
            this.btnD5.Name = "btnD5";
            this.btnD5.Size = new System.Drawing.Size(35, 35);
            this.btnD5.TabIndex = 117;
            this.btnD5.Tag = "urdu1";
            this.btnD5.Text = "E";
            this.btnD5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD5.UseVisualStyleBackColor = true;
            this.btnD5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnEB
            // 
            this.btnEB.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnEB.Location = new System.Drawing.Point(204, 170);
            this.btnEB.Name = "btnEB";
            this.btnEB.Size = new System.Drawing.Size(35, 35);
            this.btnEB.TabIndex = 139;
            this.btnEB.Tag = "urdu1";
            this.btnEB.Text = "N";
            this.btnEB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEB.UseVisualStyleBackColor = true;
            this.btnEB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD6
            // 
            this.btnD6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD6.Location = new System.Drawing.Point(374, 136);
            this.btnD6.Name = "btnD6";
            this.btnD6.Size = new System.Drawing.Size(35, 35);
            this.btnD6.TabIndex = 118;
            this.btnD6.Tag = "urdu1";
            this.btnD6.Text = "R";
            this.btnD6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD6.UseVisualStyleBackColor = true;
            this.btnD6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnEA
            // 
            this.btnEA.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnEA.Location = new System.Drawing.Point(170, 170);
            this.btnEA.Name = "btnEA";
            this.btnEA.Size = new System.Drawing.Size(35, 35);
            this.btnEA.TabIndex = 138;
            this.btnEA.Tag = "urdu1";
            this.btnEA.Text = "B";
            this.btnEA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEA.UseVisualStyleBackColor = true;
            this.btnEA.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD7
            // 
            this.btnD7.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD7.Location = new System.Drawing.Point(408, 136);
            this.btnD7.Name = "btnD7";
            this.btnD7.Size = new System.Drawing.Size(35, 35);
            this.btnD7.TabIndex = 119;
            this.btnD7.Tag = "urdu1";
            this.btnD7.Text = "T";
            this.btnD7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD7.UseVisualStyleBackColor = true;
            this.btnD7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE9
            // 
            this.btnE9.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE9.Location = new System.Drawing.Point(136, 170);
            this.btnE9.Name = "btnE9";
            this.btnE9.Size = new System.Drawing.Size(35, 35);
            this.btnE9.TabIndex = 137;
            this.btnE9.Tag = "urdu1";
            this.btnE9.Text = "V";
            this.btnE9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE9.UseVisualStyleBackColor = true;
            this.btnE9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD8
            // 
            this.btnD8.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD8.Location = new System.Drawing.Point(442, 136);
            this.btnD8.Name = "btnD8";
            this.btnD8.Size = new System.Drawing.Size(35, 35);
            this.btnD8.TabIndex = 120;
            this.btnD8.Tag = "urdu1";
            this.btnD8.Text = "Y";
            this.btnD8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD8.UseVisualStyleBackColor = true;
            this.btnD8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE8
            // 
            this.btnE8.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE8.Location = new System.Drawing.Point(102, 170);
            this.btnE8.Name = "btnE8";
            this.btnE8.Size = new System.Drawing.Size(35, 35);
            this.btnE8.TabIndex = 136;
            this.btnE8.Tag = "urdu1";
            this.btnE8.Text = "C";
            this.btnE8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE8.UseVisualStyleBackColor = true;
            this.btnE8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE0
            // 
            this.btnE0.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE0.Location = new System.Drawing.Point(714, 136);
            this.btnE0.Name = "btnE0";
            this.btnE0.Size = new System.Drawing.Size(35, 35);
            this.btnE0.TabIndex = 121;
            this.btnE0.Tag = "urdu1";
            this.btnE0.Text = "U";
            this.btnE0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE0.UseVisualStyleBackColor = true;
            this.btnE0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE7
            // 
            this.btnE7.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE7.Location = new System.Drawing.Point(68, 170);
            this.btnE7.Name = "btnE7";
            this.btnE7.Size = new System.Drawing.Size(35, 35);
            this.btnE7.TabIndex = 135;
            this.btnE7.Tag = "urdu1";
            this.btnE7.Text = "X";
            this.btnE7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE7.UseVisualStyleBackColor = true;
            this.btnE7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD9
            // 
            this.btnD9.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD9.Location = new System.Drawing.Point(476, 136);
            this.btnD9.Name = "btnD9";
            this.btnD9.Size = new System.Drawing.Size(35, 35);
            this.btnD9.TabIndex = 122;
            this.btnD9.Tag = "urdu1";
            this.btnD9.Text = "I";
            this.btnD9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD9.UseVisualStyleBackColor = true;
            this.btnD9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE6
            // 
            this.btnE6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE6.Location = new System.Drawing.Point(34, 170);
            this.btnE6.Name = "btnE6";
            this.btnE6.Size = new System.Drawing.Size(35, 35);
            this.btnE6.TabIndex = 134;
            this.btnE6.Tag = "urdu1";
            this.btnE6.Text = "Z";
            this.btnE6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE6.UseVisualStyleBackColor = true;
            this.btnE6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDA
            // 
            this.btnDA.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDA.Location = new System.Drawing.Point(510, 136);
            this.btnDA.Name = "btnDA";
            this.btnDA.Size = new System.Drawing.Size(35, 35);
            this.btnDA.TabIndex = 123;
            this.btnDA.Tag = "urdu1";
            this.btnDA.Text = "O";
            this.btnDA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDA.UseVisualStyleBackColor = true;
            this.btnDA.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDB
            // 
            this.btnDB.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDB.Location = new System.Drawing.Point(544, 136);
            this.btnDB.Name = "btnDB";
            this.btnDB.Size = new System.Drawing.Size(35, 35);
            this.btnDB.TabIndex = 124;
            this.btnDB.Tag = "urdu1";
            this.btnDB.Text = "P";
            this.btnDB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDB.UseVisualStyleBackColor = true;
            this.btnDB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE5
            // 
            this.btnE5.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE5.Location = new System.Drawing.Point(0, 170);
            this.btnE5.Name = "btnE5";
            this.btnE5.Size = new System.Drawing.Size(35, 35);
            this.btnE5.TabIndex = 133;
            this.btnE5.Tag = "urdu1";
            this.btnE5.Text = "L";
            this.btnE5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE5.UseVisualStyleBackColor = true;
            this.btnE5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDC
            // 
            this.btnDC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDC.Location = new System.Drawing.Point(578, 136);
            this.btnDC.Name = "btnDC";
            this.btnDC.Size = new System.Drawing.Size(35, 35);
            this.btnDC.TabIndex = 125;
            this.btnDC.Tag = "urdu1";
            this.btnDC.Text = "A";
            this.btnDC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDC.UseVisualStyleBackColor = true;
            this.btnDC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE4
            // 
            this.btnE4.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE4.Location = new System.Drawing.Point(612, 170);
            this.btnE4.Name = "btnE4";
            this.btnE4.Size = new System.Drawing.Size(35, 35);
            this.btnE4.TabIndex = 132;
            this.btnE4.Tag = "urdu1";
            this.btnE4.Text = "K";
            this.btnE4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE4.UseVisualStyleBackColor = true;
            this.btnE4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDD
            // 
            this.btnDD.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDD.Location = new System.Drawing.Point(612, 136);
            this.btnDD.Name = "btnDD";
            this.btnDD.Size = new System.Drawing.Size(35, 35);
            this.btnDD.TabIndex = 126;
            this.btnDD.Tag = "urdu1";
            this.btnDD.Text = "S";
            this.btnDD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDD.UseVisualStyleBackColor = true;
            this.btnDD.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE3
            // 
            this.btnE3.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE3.Location = new System.Drawing.Point(816, 136);
            this.btnE3.Name = "btnE3";
            this.btnE3.Size = new System.Drawing.Size(35, 35);
            this.btnE3.TabIndex = 131;
            this.btnE3.Tag = "urdu1";
            this.btnE3.Text = "J";
            this.btnE3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE3.UseVisualStyleBackColor = true;
            this.btnE3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDE
            // 
            this.btnDE.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDE.Location = new System.Drawing.Point(646, 136);
            this.btnDE.Name = "btnDE";
            this.btnDE.Size = new System.Drawing.Size(35, 35);
            this.btnDE.TabIndex = 127;
            this.btnDE.Tag = "urdu1";
            this.btnDE.Text = "D";
            this.btnDE.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDE.UseVisualStyleBackColor = true;
            this.btnDE.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE2
            // 
            this.btnE2.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE2.Location = new System.Drawing.Point(782, 136);
            this.btnE2.Name = "btnE2";
            this.btnE2.Size = new System.Drawing.Size(35, 35);
            this.btnE2.TabIndex = 130;
            this.btnE2.Tag = "urdu1";
            this.btnE2.Text = "H";
            this.btnE2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE2.UseVisualStyleBackColor = true;
            this.btnE2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnDF
            // 
            this.btnDF.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnDF.Location = new System.Drawing.Point(680, 136);
            this.btnDF.Name = "btnDF";
            this.btnDF.Size = new System.Drawing.Size(35, 35);
            this.btnDF.TabIndex = 128;
            this.btnDF.Tag = "urdu1";
            this.btnDF.Text = "F";
            this.btnDF.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDF.UseVisualStyleBackColor = true;
            this.btnDF.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnE1
            // 
            this.btnE1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnE1.Location = new System.Drawing.Point(748, 136);
            this.btnE1.Name = "btnE1";
            this.btnE1.Size = new System.Drawing.Size(35, 35);
            this.btnE1.TabIndex = 129;
            this.btnE1.Tag = "urdu1";
            this.btnE1.Text = "G";
            this.btnE1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnE1.UseVisualStyleBackColor = true;
            this.btnE1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC9
            // 
            this.btnC9.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC9.Location = new System.Drawing.Point(782, 102);
            this.btnC9.Name = "btnC9";
            this.btnC9.Size = new System.Drawing.Size(35, 35);
            this.btnC9.TabIndex = 105;
            this.btnC9.Tag = "urdu1";
            this.btnC9.Text = "O";
            this.btnC9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC9.UseVisualStyleBackColor = true;
            this.btnC9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCA
            // 
            this.btnCA.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCA.Location = new System.Drawing.Point(816, 102);
            this.btnCA.Name = "btnCA";
            this.btnCA.Size = new System.Drawing.Size(35, 35);
            this.btnCA.TabIndex = 106;
            this.btnCA.Tag = "urdu1";
            this.btnCA.Text = "P";
            this.btnCA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCA.UseVisualStyleBackColor = true;
            this.btnCA.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCB
            // 
            this.btnCB.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCB.Location = new System.Drawing.Point(0, 136);
            this.btnCB.Name = "btnCB";
            this.btnCB.Size = new System.Drawing.Size(35, 35);
            this.btnCB.TabIndex = 107;
            this.btnCB.Tag = "urdu1";
            this.btnCB.Text = "A";
            this.btnCB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCB.UseVisualStyleBackColor = true;
            this.btnCB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD2
            // 
            this.btnD2.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD2.Location = new System.Drawing.Point(238, 136);
            this.btnD2.Name = "btnD2";
            this.btnD2.Size = new System.Drawing.Size(35, 35);
            this.btnD2.TabIndex = 114;
            this.btnD2.Tag = "urdu1";
            this.btnD2.Text = "K";
            this.btnD2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD2.UseVisualStyleBackColor = true;
            this.btnD2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCC
            // 
            this.btnCC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCC.Location = new System.Drawing.Point(34, 136);
            this.btnCC.Name = "btnCC";
            this.btnCC.Size = new System.Drawing.Size(35, 35);
            this.btnCC.TabIndex = 108;
            this.btnCC.Tag = "urdu1";
            this.btnCC.Text = "S";
            this.btnCC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCC.UseVisualStyleBackColor = true;
            this.btnCC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD1
            // 
            this.btnD1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD1.Location = new System.Drawing.Point(204, 136);
            this.btnD1.Name = "btnD1";
            this.btnD1.Size = new System.Drawing.Size(35, 35);
            this.btnD1.TabIndex = 113;
            this.btnD1.Tag = "urdu1";
            this.btnD1.Text = "J";
            this.btnD1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD1.UseVisualStyleBackColor = true;
            this.btnD1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCD
            // 
            this.btnCD.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCD.Location = new System.Drawing.Point(68, 136);
            this.btnCD.Name = "btnCD";
            this.btnCD.Size = new System.Drawing.Size(35, 35);
            this.btnCD.TabIndex = 109;
            this.btnCD.Tag = "urdu1";
            this.btnCD.Text = "D";
            this.btnCD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCD.UseVisualStyleBackColor = true;
            this.btnCD.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnD0
            // 
            this.btnD0.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnD0.Location = new System.Drawing.Point(170, 136);
            this.btnD0.Name = "btnD0";
            this.btnD0.Size = new System.Drawing.Size(35, 35);
            this.btnD0.TabIndex = 112;
            this.btnD0.Tag = "urdu1";
            this.btnD0.Text = "H";
            this.btnD0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnD0.UseVisualStyleBackColor = true;
            this.btnD0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCE
            // 
            this.btnCE.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCE.Location = new System.Drawing.Point(102, 136);
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(35, 35);
            this.btnCE.TabIndex = 110;
            this.btnCE.Tag = "urdu1";
            this.btnCE.Text = "F";
            this.btnCE.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCE.UseVisualStyleBackColor = true;
            this.btnCE.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCF
            // 
            this.btnCF.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnCF.Location = new System.Drawing.Point(136, 136);
            this.btnCF.Name = "btnCF";
            this.btnCF.Size = new System.Drawing.Size(35, 35);
            this.btnCF.TabIndex = 111;
            this.btnCF.Tag = "urdu1";
            this.btnCF.Text = "G";
            this.btnCF.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCF.UseVisualStyleBackColor = true;
            this.btnCF.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAF
            // 
            this.btnAF.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAF.Location = new System.Drawing.Point(748, 68);
            this.btnAF.Name = "btnAF";
            this.btnAF.Size = new System.Drawing.Size(35, 35);
            this.btnAF.TabIndex = 79;
            this.btnAF.Tag = "urdu1";
            this.btnAF.Text = "Q";
            this.btnAF.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAF.UseVisualStyleBackColor = true;
            this.btnAF.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB0
            // 
            this.btnB0.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB0.Location = new System.Drawing.Point(782, 68);
            this.btnB0.Name = "btnB0";
            this.btnB0.Size = new System.Drawing.Size(35, 35);
            this.btnB0.TabIndex = 80;
            this.btnB0.Tag = "urdu1";
            this.btnB0.Text = "W";
            this.btnB0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB0.UseVisualStyleBackColor = true;
            this.btnB0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC8
            // 
            this.btnC8.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC8.Location = new System.Drawing.Point(748, 102);
            this.btnC8.Name = "btnC8";
            this.btnC8.Size = new System.Drawing.Size(35, 35);
            this.btnC8.TabIndex = 104;
            this.btnC8.Tag = "urdu1";
            this.btnC8.Text = "M";
            this.btnC8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC8.UseVisualStyleBackColor = true;
            this.btnC8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB1
            // 
            this.btnB1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB1.Location = new System.Drawing.Point(816, 68);
            this.btnB1.Name = "btnB1";
            this.btnB1.Size = new System.Drawing.Size(35, 35);
            this.btnB1.TabIndex = 81;
            this.btnB1.Tag = "urdu1";
            this.btnB1.Text = "E";
            this.btnB1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB1.UseVisualStyleBackColor = true;
            this.btnB1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC7
            // 
            this.btnC7.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC7.Location = new System.Drawing.Point(714, 102);
            this.btnC7.Name = "btnC7";
            this.btnC7.Size = new System.Drawing.Size(35, 35);
            this.btnC7.TabIndex = 103;
            this.btnC7.Tag = "urdu1";
            this.btnC7.Text = "N";
            this.btnC7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC7.UseVisualStyleBackColor = true;
            this.btnC7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB2
            // 
            this.btnB2.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB2.Location = new System.Drawing.Point(0, 102);
            this.btnB2.Name = "btnB2";
            this.btnB2.Size = new System.Drawing.Size(35, 35);
            this.btnB2.TabIndex = 82;
            this.btnB2.Tag = "urdu1";
            this.btnB2.Text = "R";
            this.btnB2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB2.UseVisualStyleBackColor = true;
            this.btnB2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC6
            // 
            this.btnC6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC6.Location = new System.Drawing.Point(680, 102);
            this.btnC6.Name = "btnC6";
            this.btnC6.Size = new System.Drawing.Size(35, 35);
            this.btnC6.TabIndex = 102;
            this.btnC6.Tag = "urdu1";
            this.btnC6.Text = "B";
            this.btnC6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC6.UseVisualStyleBackColor = true;
            this.btnC6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB3
            // 
            this.btnB3.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB3.Location = new System.Drawing.Point(34, 102);
            this.btnB3.Name = "btnB3";
            this.btnB3.Size = new System.Drawing.Size(35, 35);
            this.btnB3.TabIndex = 83;
            this.btnB3.Tag = "urdu1";
            this.btnB3.Text = "T";
            this.btnB3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB3.UseVisualStyleBackColor = true;
            this.btnB3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC5
            // 
            this.btnC5.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC5.Location = new System.Drawing.Point(646, 102);
            this.btnC5.Name = "btnC5";
            this.btnC5.Size = new System.Drawing.Size(35, 35);
            this.btnC5.TabIndex = 101;
            this.btnC5.Tag = "urdu1";
            this.btnC5.Text = "V";
            this.btnC5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC5.UseVisualStyleBackColor = true;
            this.btnC5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB4
            // 
            this.btnB4.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB4.Location = new System.Drawing.Point(68, 102);
            this.btnB4.Name = "btnB4";
            this.btnB4.Size = new System.Drawing.Size(35, 35);
            this.btnB4.TabIndex = 84;
            this.btnB4.Tag = "urdu1";
            this.btnB4.Text = "Y";
            this.btnB4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB4.UseVisualStyleBackColor = true;
            this.btnB4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC4
            // 
            this.btnC4.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC4.Location = new System.Drawing.Point(612, 102);
            this.btnC4.Name = "btnC4";
            this.btnC4.Size = new System.Drawing.Size(35, 35);
            this.btnC4.TabIndex = 100;
            this.btnC4.Tag = "urdu1";
            this.btnC4.Text = "C";
            this.btnC4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC4.UseVisualStyleBackColor = true;
            this.btnC4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB5
            // 
            this.btnB5.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB5.Location = new System.Drawing.Point(102, 102);
            this.btnB5.Name = "btnB5";
            this.btnB5.Size = new System.Drawing.Size(35, 35);
            this.btnB5.TabIndex = 85;
            this.btnB5.Tag = "urdu1";
            this.btnB5.Text = "U";
            this.btnB5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB5.UseVisualStyleBackColor = true;
            this.btnB5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC3
            // 
            this.btnC3.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC3.Location = new System.Drawing.Point(578, 102);
            this.btnC3.Name = "btnC3";
            this.btnC3.Size = new System.Drawing.Size(35, 35);
            this.btnC3.TabIndex = 99;
            this.btnC3.Tag = "urdu1";
            this.btnC3.Text = "X";
            this.btnC3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC3.UseVisualStyleBackColor = true;
            this.btnC3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB6
            // 
            this.btnB6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB6.Location = new System.Drawing.Point(136, 102);
            this.btnB6.Name = "btnB6";
            this.btnB6.Size = new System.Drawing.Size(35, 35);
            this.btnB6.TabIndex = 86;
            this.btnB6.Tag = "urdu1";
            this.btnB6.Text = "I";
            this.btnB6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB6.UseVisualStyleBackColor = true;
            this.btnB6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC2
            // 
            this.btnC2.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC2.Location = new System.Drawing.Point(544, 102);
            this.btnC2.Name = "btnC2";
            this.btnC2.Size = new System.Drawing.Size(35, 35);
            this.btnC2.TabIndex = 98;
            this.btnC2.Tag = "urdu1";
            this.btnC2.Text = "Z";
            this.btnC2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC2.UseVisualStyleBackColor = true;
            this.btnC2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB7
            // 
            this.btnB7.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB7.Location = new System.Drawing.Point(170, 102);
            this.btnB7.Name = "btnB7";
            this.btnB7.Size = new System.Drawing.Size(35, 35);
            this.btnB7.TabIndex = 87;
            this.btnB7.Tag = "urdu1";
            this.btnB7.Text = "O";
            this.btnB7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB7.UseVisualStyleBackColor = true;
            this.btnB7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB8
            // 
            this.btnB8.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB8.Location = new System.Drawing.Point(204, 102);
            this.btnB8.Name = "btnB8";
            this.btnB8.Size = new System.Drawing.Size(35, 35);
            this.btnB8.TabIndex = 88;
            this.btnB8.Tag = "urdu1";
            this.btnB8.Text = "P";
            this.btnB8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB8.UseVisualStyleBackColor = true;
            this.btnB8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC1
            // 
            this.btnC1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC1.Location = new System.Drawing.Point(510, 102);
            this.btnC1.Name = "btnC1";
            this.btnC1.Size = new System.Drawing.Size(35, 35);
            this.btnC1.TabIndex = 97;
            this.btnC1.Tag = "urdu1";
            this.btnC1.Text = "L";
            this.btnC1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC1.UseVisualStyleBackColor = true;
            this.btnC1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnB9
            // 
            this.btnB9.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnB9.Location = new System.Drawing.Point(238, 102);
            this.btnB9.Name = "btnB9";
            this.btnB9.Size = new System.Drawing.Size(35, 35);
            this.btnB9.TabIndex = 89;
            this.btnB9.Tag = "urdu1";
            this.btnB9.Text = "A";
            this.btnB9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnB9.UseVisualStyleBackColor = true;
            this.btnB9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnC0
            // 
            this.btnC0.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnC0.Location = new System.Drawing.Point(476, 102);
            this.btnC0.Name = "btnC0";
            this.btnC0.Size = new System.Drawing.Size(35, 35);
            this.btnC0.TabIndex = 96;
            this.btnC0.Tag = "urdu1";
            this.btnC0.Text = "K";
            this.btnC0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnC0.UseVisualStyleBackColor = true;
            this.btnC0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBA
            // 
            this.btnBA.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBA.Location = new System.Drawing.Point(272, 102);
            this.btnBA.Name = "btnBA";
            this.btnBA.Size = new System.Drawing.Size(35, 35);
            this.btnBA.TabIndex = 90;
            this.btnBA.Tag = "urdu1";
            this.btnBA.Text = "S";
            this.btnBA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBA.UseVisualStyleBackColor = true;
            this.btnBA.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBF
            // 
            this.btnBF.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBF.Location = new System.Drawing.Point(442, 102);
            this.btnBF.Name = "btnBF";
            this.btnBF.Size = new System.Drawing.Size(35, 35);
            this.btnBF.TabIndex = 95;
            this.btnBF.Tag = "urdu1";
            this.btnBF.Text = "J";
            this.btnBF.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBF.UseVisualStyleBackColor = true;
            this.btnBF.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBB
            // 
            this.btnBB.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBB.Location = new System.Drawing.Point(306, 102);
            this.btnBB.Name = "btnBB";
            this.btnBB.Size = new System.Drawing.Size(35, 35);
            this.btnBB.TabIndex = 91;
            this.btnBB.Tag = "urdu1";
            this.btnBB.Text = "D";
            this.btnBB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBB.UseVisualStyleBackColor = true;
            this.btnBB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBE
            // 
            this.btnBE.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBE.Location = new System.Drawing.Point(408, 102);
            this.btnBE.Name = "btnBE";
            this.btnBE.Size = new System.Drawing.Size(35, 35);
            this.btnBE.TabIndex = 94;
            this.btnBE.Tag = "urdu1";
            this.btnBE.Text = "H";
            this.btnBE.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBE.UseVisualStyleBackColor = true;
            this.btnBE.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBC
            // 
            this.btnBC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBC.Location = new System.Drawing.Point(340, 102);
            this.btnBC.Name = "btnBC";
            this.btnBC.Size = new System.Drawing.Size(35, 35);
            this.btnBC.TabIndex = 92;
            this.btnBC.Tag = "urdu1";
            this.btnBC.Text = "F";
            this.btnBC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBC.UseVisualStyleBackColor = true;
            this.btnBC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnBD
            // 
            this.btnBD.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnBD.Location = new System.Drawing.Point(374, 102);
            this.btnBD.Name = "btnBD";
            this.btnBD.Size = new System.Drawing.Size(35, 35);
            this.btnBD.TabIndex = 93;
            this.btnBD.Tag = "urdu1";
            this.btnBD.Text = "G";
            this.btnBD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBD.UseVisualStyleBackColor = true;
            this.btnBD.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnSpace
            // 
            this.btnSpace.Font = new System.Drawing.Font("AlKatib1", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSpace.Location = new System.Drawing.Point(137, 204);
            this.btnSpace.Name = "btnSpace";
            this.btnSpace.Size = new System.Drawing.Size(374, 35);
            this.btnSpace.TabIndex = 78;
            this.btnSpace.Tag = "urdu";
            this.btnSpace.UseVisualStyleBackColor = true;
            this.btnSpace.Click += new System.EventHandler(this.btnSpace_Click);
            // 
            // btnBS
            // 
            this.btnBS.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBS.Location = new System.Drawing.Point(510, 204);
            this.btnBS.Name = "btnBS";
            this.btnBS.Size = new System.Drawing.Size(103, 35);
            this.btnBS.TabIndex = 77;
            this.btnBS.Tag = "";
            this.btnBS.Text = "Back Space";
            this.btnBS.UseVisualStyleBackColor = true;
            this.btnBS.Click += new System.EventHandler(this.btnBS_Click);
            // 
            // btnA5
            // 
            this.btnA5.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA5.Location = new System.Drawing.Point(408, 68);
            this.btnA5.Name = "btnA5";
            this.btnA5.Size = new System.Drawing.Size(35, 35);
            this.btnA5.TabIndex = 67;
            this.btnA5.Tag = "urdu1";
            this.btnA5.Text = "O";
            this.btnA5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA5.UseVisualStyleBackColor = true;
            this.btnA5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA6
            // 
            this.btnA6.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA6.Location = new System.Drawing.Point(442, 68);
            this.btnA6.Name = "btnA6";
            this.btnA6.Size = new System.Drawing.Size(35, 35);
            this.btnA6.TabIndex = 68;
            this.btnA6.Tag = "urdu1";
            this.btnA6.Text = "P";
            this.btnA6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA6.UseVisualStyleBackColor = true;
            this.btnA6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA7
            // 
            this.btnA7.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA7.Location = new System.Drawing.Point(476, 68);
            this.btnA7.Name = "btnA7";
            this.btnA7.Size = new System.Drawing.Size(35, 35);
            this.btnA7.TabIndex = 69;
            this.btnA7.Tag = "urdu1";
            this.btnA7.Text = "A";
            this.btnA7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA7.UseVisualStyleBackColor = true;
            this.btnA7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAE
            // 
            this.btnAE.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAE.Location = new System.Drawing.Point(714, 68);
            this.btnAE.Name = "btnAE";
            this.btnAE.Size = new System.Drawing.Size(35, 35);
            this.btnAE.TabIndex = 76;
            this.btnAE.Tag = "urdu1";
            this.btnAE.Text = "K";
            this.btnAE.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAE.UseVisualStyleBackColor = true;
            this.btnAE.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA8
            // 
            this.btnA8.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA8.Location = new System.Drawing.Point(510, 68);
            this.btnA8.Name = "btnA8";
            this.btnA8.Size = new System.Drawing.Size(35, 35);
            this.btnA8.TabIndex = 70;
            this.btnA8.Tag = "urdu1";
            this.btnA8.Text = "S";
            this.btnA8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA8.UseVisualStyleBackColor = true;
            this.btnA8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAD
            // 
            this.btnAD.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAD.Location = new System.Drawing.Point(680, 68);
            this.btnAD.Name = "btnAD";
            this.btnAD.Size = new System.Drawing.Size(35, 35);
            this.btnAD.TabIndex = 75;
            this.btnAD.Tag = "urdu1";
            this.btnAD.Text = "J";
            this.btnAD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAD.UseVisualStyleBackColor = true;
            this.btnAD.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA9
            // 
            this.btnA9.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA9.Location = new System.Drawing.Point(544, 68);
            this.btnA9.Name = "btnA9";
            this.btnA9.Size = new System.Drawing.Size(35, 35);
            this.btnA9.TabIndex = 71;
            this.btnA9.Tag = "urdu1";
            this.btnA9.Text = "D";
            this.btnA9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA9.UseVisualStyleBackColor = true;
            this.btnA9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAC
            // 
            this.btnAC.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAC.Location = new System.Drawing.Point(646, 68);
            this.btnAC.Name = "btnAC";
            this.btnAC.Size = new System.Drawing.Size(35, 35);
            this.btnAC.TabIndex = 74;
            this.btnAC.Tag = "urdu1";
            this.btnAC.Text = "H";
            this.btnAC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAC.UseVisualStyleBackColor = true;
            this.btnAC.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAA
            // 
            this.btnAA.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAA.Location = new System.Drawing.Point(578, 68);
            this.btnAA.Name = "btnAA";
            this.btnAA.Size = new System.Drawing.Size(35, 35);
            this.btnAA.TabIndex = 72;
            this.btnAA.Tag = "urdu1";
            this.btnAA.Text = "F";
            this.btnAA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAA.UseVisualStyleBackColor = true;
            this.btnAA.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnAB
            // 
            this.btnAB.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnAB.Location = new System.Drawing.Point(612, 68);
            this.btnAB.Name = "btnAB";
            this.btnAB.Size = new System.Drawing.Size(35, 35);
            this.btnAB.TabIndex = 73;
            this.btnAB.Tag = "urdu1";
            this.btnAB.Text = "G";
            this.btnAB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAB.UseVisualStyleBackColor = true;
            this.btnAB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn68
            // 
            this.btn68.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn68.Location = new System.Drawing.Point(374, 34);
            this.btn68.Name = "btn68";
            this.btn68.Size = new System.Drawing.Size(35, 35);
            this.btn68.TabIndex = 41;
            this.btn68.Tag = "urdu1";
            this.btn68.Text = "Q";
            this.btn68.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn68.UseVisualStyleBackColor = true;
            this.btn68.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn69
            // 
            this.btn69.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn69.Location = new System.Drawing.Point(408, 34);
            this.btn69.Name = "btn69";
            this.btn69.Size = new System.Drawing.Size(35, 35);
            this.btn69.TabIndex = 42;
            this.btn69.Tag = "urdu1";
            this.btn69.Text = "W";
            this.btn69.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn69.UseVisualStyleBackColor = true;
            this.btn69.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA4
            // 
            this.btnA4.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA4.Location = new System.Drawing.Point(374, 68);
            this.btnA4.Name = "btnA4";
            this.btnA4.Size = new System.Drawing.Size(35, 35);
            this.btnA4.TabIndex = 66;
            this.btnA4.Tag = "urdu1";
            this.btnA4.Text = "M";
            this.btnA4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA4.UseVisualStyleBackColor = true;
            this.btnA4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6A
            // 
            this.btn6A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6A.Location = new System.Drawing.Point(442, 34);
            this.btn6A.Name = "btn6A";
            this.btn6A.Size = new System.Drawing.Size(35, 35);
            this.btn6A.TabIndex = 43;
            this.btn6A.Tag = "urdu1";
            this.btn6A.Text = "E";
            this.btn6A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6A.UseVisualStyleBackColor = true;
            this.btn6A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA3
            // 
            this.btnA3.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA3.Location = new System.Drawing.Point(340, 68);
            this.btnA3.Name = "btnA3";
            this.btnA3.Size = new System.Drawing.Size(35, 35);
            this.btnA3.TabIndex = 65;
            this.btnA3.Tag = "urdu1";
            this.btnA3.Text = "N";
            this.btnA3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA3.UseVisualStyleBackColor = true;
            this.btnA3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6B
            // 
            this.btn6B.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6B.Location = new System.Drawing.Point(476, 34);
            this.btn6B.Name = "btn6B";
            this.btn6B.Size = new System.Drawing.Size(35, 35);
            this.btn6B.TabIndex = 44;
            this.btn6B.Tag = "urdu1";
            this.btn6B.Text = "R";
            this.btn6B.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6B.UseVisualStyleBackColor = true;
            this.btn6B.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA2
            // 
            this.btnA2.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA2.Location = new System.Drawing.Point(306, 68);
            this.btnA2.Name = "btnA2";
            this.btnA2.Size = new System.Drawing.Size(35, 35);
            this.btnA2.TabIndex = 64;
            this.btnA2.Tag = "urdu1";
            this.btnA2.Text = "B";
            this.btnA2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA2.UseVisualStyleBackColor = true;
            this.btnA2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6C
            // 
            this.btn6C.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6C.Location = new System.Drawing.Point(510, 34);
            this.btn6C.Name = "btn6C";
            this.btn6C.Size = new System.Drawing.Size(35, 35);
            this.btn6C.TabIndex = 45;
            this.btn6C.Tag = "urdu1";
            this.btn6C.Text = "T";
            this.btn6C.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6C.UseVisualStyleBackColor = true;
            this.btn6C.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnA1
            // 
            this.btnA1.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btnA1.Location = new System.Drawing.Point(272, 68);
            this.btnA1.Name = "btnA1";
            this.btnA1.Size = new System.Drawing.Size(35, 35);
            this.btnA1.TabIndex = 63;
            this.btnA1.Tag = "urdu1";
            this.btnA1.Text = "V";
            this.btnA1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnA1.UseVisualStyleBackColor = true;
            this.btnA1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6D
            // 
            this.btn6D.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6D.Location = new System.Drawing.Point(544, 34);
            this.btn6D.Name = "btn6D";
            this.btn6D.Size = new System.Drawing.Size(35, 35);
            this.btn6D.TabIndex = 46;
            this.btn6D.Tag = "urdu1";
            this.btn6D.Text = "Y";
            this.btn6D.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6D.UseVisualStyleBackColor = true;
            this.btn6D.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn7E
            // 
            this.btn7E.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn7E.Location = new System.Drawing.Point(238, 68);
            this.btn7E.Name = "btn7E";
            this.btn7E.Size = new System.Drawing.Size(35, 35);
            this.btn7E.TabIndex = 62;
            this.btn7E.Tag = "urdu1";
            this.btn7E.Text = "C";
            this.btn7E.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn7E.UseVisualStyleBackColor = true;
            this.btn7E.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6E
            // 
            this.btn6E.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6E.Location = new System.Drawing.Point(578, 34);
            this.btn6E.Name = "btn6E";
            this.btn6E.Size = new System.Drawing.Size(35, 35);
            this.btn6E.TabIndex = 47;
            this.btn6E.Tag = "urdu1";
            this.btn6E.Text = "U";
            this.btn6E.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6E.UseVisualStyleBackColor = true;
            this.btn6E.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn7C
            // 
            this.btn7C.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn7C.Location = new System.Drawing.Point(204, 68);
            this.btn7C.Name = "btn7C";
            this.btn7C.Size = new System.Drawing.Size(35, 35);
            this.btn7C.TabIndex = 61;
            this.btn7C.Tag = "urdu1";
            this.btn7C.Text = "X";
            this.btn7C.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn7C.UseVisualStyleBackColor = true;
            this.btn7C.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6F
            // 
            this.btn6F.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn6F.Location = new System.Drawing.Point(612, 34);
            this.btn6F.Name = "btn6F";
            this.btn6F.Size = new System.Drawing.Size(35, 35);
            this.btn6F.TabIndex = 48;
            this.btn6F.Tag = "urdu1";
            this.btn6F.Text = "I";
            this.btn6F.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn6F.UseVisualStyleBackColor = true;
            this.btn6F.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn7B
            // 
            this.btn7B.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn7B.Location = new System.Drawing.Point(170, 68);
            this.btn7B.Name = "btn7B";
            this.btn7B.Size = new System.Drawing.Size(35, 35);
            this.btn7B.TabIndex = 60;
            this.btn7B.Tag = "urdu1";
            this.btn7B.Text = "Z";
            this.btn7B.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn7B.UseVisualStyleBackColor = true;
            this.btn7B.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn70
            // 
            this.btn70.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn70.Location = new System.Drawing.Point(646, 34);
            this.btn70.Name = "btn70";
            this.btn70.Size = new System.Drawing.Size(35, 35);
            this.btn70.TabIndex = 49;
            this.btn70.Tag = "urdu1";
            this.btn70.Text = "O";
            this.btn70.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn70.UseVisualStyleBackColor = true;
            this.btn70.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn71
            // 
            this.btn71.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn71.Location = new System.Drawing.Point(680, 34);
            this.btn71.Name = "btn71";
            this.btn71.Size = new System.Drawing.Size(35, 35);
            this.btn71.TabIndex = 50;
            this.btn71.Tag = "urdu1";
            this.btn71.Text = "P";
            this.btn71.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn71.UseVisualStyleBackColor = true;
            this.btn71.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn7A
            // 
            this.btn7A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn7A.Location = new System.Drawing.Point(136, 68);
            this.btn7A.Name = "btn7A";
            this.btn7A.Size = new System.Drawing.Size(35, 35);
            this.btn7A.TabIndex = 59;
            this.btn7A.Tag = "urdu1";
            this.btn7A.Text = "L";
            this.btn7A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn7A.UseVisualStyleBackColor = true;
            this.btn7A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn72
            // 
            this.btn72.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn72.Location = new System.Drawing.Point(714, 34);
            this.btn72.Name = "btn72";
            this.btn72.Size = new System.Drawing.Size(35, 35);
            this.btn72.TabIndex = 51;
            this.btn72.Tag = "urdu1";
            this.btn72.Text = "A";
            this.btn72.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn72.UseVisualStyleBackColor = true;
            this.btn72.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn79
            // 
            this.btn79.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn79.Location = new System.Drawing.Point(102, 68);
            this.btn79.Name = "btn79";
            this.btn79.Size = new System.Drawing.Size(35, 35);
            this.btn79.TabIndex = 58;
            this.btn79.Tag = "urdu1";
            this.btn79.Text = "K";
            this.btn79.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn79.UseVisualStyleBackColor = true;
            this.btn79.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn73
            // 
            this.btn73.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn73.Location = new System.Drawing.Point(748, 34);
            this.btn73.Name = "btn73";
            this.btn73.Size = new System.Drawing.Size(35, 35);
            this.btn73.TabIndex = 52;
            this.btn73.Tag = "urdu1";
            this.btn73.Text = "S";
            this.btn73.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn73.UseVisualStyleBackColor = true;
            this.btn73.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn78
            // 
            this.btn78.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn78.Location = new System.Drawing.Point(68, 68);
            this.btn78.Name = "btn78";
            this.btn78.Size = new System.Drawing.Size(35, 35);
            this.btn78.TabIndex = 57;
            this.btn78.Tag = "urdu1";
            this.btn78.Text = "J";
            this.btn78.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn78.UseVisualStyleBackColor = true;
            this.btn78.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn74
            // 
            this.btn74.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn74.Location = new System.Drawing.Point(782, 34);
            this.btn74.Name = "btn74";
            this.btn74.Size = new System.Drawing.Size(35, 35);
            this.btn74.TabIndex = 53;
            this.btn74.Tag = "urdu1";
            this.btn74.Text = "D";
            this.btn74.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn74.UseVisualStyleBackColor = true;
            this.btn74.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn77
            // 
            this.btn77.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn77.Location = new System.Drawing.Point(34, 68);
            this.btn77.Name = "btn77";
            this.btn77.Size = new System.Drawing.Size(35, 35);
            this.btn77.TabIndex = 56;
            this.btn77.Tag = "urdu1";
            this.btn77.Text = "H";
            this.btn77.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn77.UseVisualStyleBackColor = true;
            this.btn77.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn75
            // 
            this.btn75.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn75.Location = new System.Drawing.Point(816, 34);
            this.btn75.Name = "btn75";
            this.btn75.Size = new System.Drawing.Size(35, 35);
            this.btn75.TabIndex = 54;
            this.btn75.Tag = "urdu1";
            this.btn75.Text = "F";
            this.btn75.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn75.UseVisualStyleBackColor = true;
            this.btn75.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn76
            // 
            this.btn76.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn76.Location = new System.Drawing.Point(0, 68);
            this.btn76.Name = "btn76";
            this.btn76.Size = new System.Drawing.Size(35, 35);
            this.btn76.TabIndex = 55;
            this.btn76.Tag = "urdu1";
            this.btn76.Text = "G";
            this.btn76.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn76.UseVisualStyleBackColor = true;
            this.btn76.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5C
            // 
            this.btn5C.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn5C.Location = new System.Drawing.Point(34, 34);
            this.btn5C.Name = "btn5C";
            this.btn5C.Size = new System.Drawing.Size(35, 35);
            this.btn5C.TabIndex = 31;
            this.btn5C.Tag = "urdu1";
            this.btn5C.Text = "O";
            this.btn5C.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn5C.UseVisualStyleBackColor = true;
            this.btn5C.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5F
            // 
            this.btn5F.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn5F.Location = new System.Drawing.Point(68, 34);
            this.btn5F.Name = "btn5F";
            this.btn5F.Size = new System.Drawing.Size(35, 35);
            this.btn5F.TabIndex = 32;
            this.btn5F.Tag = "urdu1";
            this.btn5F.Text = "P";
            this.btn5F.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn5F.UseVisualStyleBackColor = true;
            this.btn5F.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn60
            // 
            this.btn60.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn60.Location = new System.Drawing.Point(102, 34);
            this.btn60.Name = "btn60";
            this.btn60.Size = new System.Drawing.Size(35, 35);
            this.btn60.TabIndex = 33;
            this.btn60.Tag = "urdu1";
            this.btn60.Text = "A";
            this.btn60.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn60.UseVisualStyleBackColor = true;
            this.btn60.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn67
            // 
            this.btn67.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn67.Location = new System.Drawing.Point(340, 34);
            this.btn67.Name = "btn67";
            this.btn67.Size = new System.Drawing.Size(35, 35);
            this.btn67.TabIndex = 40;
            this.btn67.Tag = "urdu1";
            this.btn67.Text = "K";
            this.btn67.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn67.UseVisualStyleBackColor = true;
            this.btn67.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn61
            // 
            this.btn61.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn61.Location = new System.Drawing.Point(136, 34);
            this.btn61.Name = "btn61";
            this.btn61.Size = new System.Drawing.Size(35, 35);
            this.btn61.TabIndex = 34;
            this.btn61.Tag = "urdu1";
            this.btn61.Text = "S";
            this.btn61.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn61.UseVisualStyleBackColor = true;
            this.btn61.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn66
            // 
            this.btn66.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn66.Location = new System.Drawing.Point(306, 34);
            this.btn66.Name = "btn66";
            this.btn66.Size = new System.Drawing.Size(35, 35);
            this.btn66.TabIndex = 39;
            this.btn66.Tag = "urdu1";
            this.btn66.Text = "J";
            this.btn66.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn66.UseVisualStyleBackColor = true;
            this.btn66.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn62
            // 
            this.btn62.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn62.Location = new System.Drawing.Point(170, 34);
            this.btn62.Name = "btn62";
            this.btn62.Size = new System.Drawing.Size(35, 35);
            this.btn62.TabIndex = 35;
            this.btn62.Tag = "urdu1";
            this.btn62.Text = "D";
            this.btn62.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn62.UseVisualStyleBackColor = true;
            this.btn62.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn65
            // 
            this.btn65.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn65.Location = new System.Drawing.Point(272, 34);
            this.btn65.Name = "btn65";
            this.btn65.Size = new System.Drawing.Size(35, 35);
            this.btn65.TabIndex = 38;
            this.btn65.Tag = "urdu1";
            this.btn65.Text = "H";
            this.btn65.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn65.UseVisualStyleBackColor = true;
            this.btn65.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn63
            // 
            this.btn63.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn63.Location = new System.Drawing.Point(204, 34);
            this.btn63.Name = "btn63";
            this.btn63.Size = new System.Drawing.Size(35, 35);
            this.btn63.TabIndex = 36;
            this.btn63.Tag = "urdu1";
            this.btn63.Text = "F";
            this.btn63.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn63.UseVisualStyleBackColor = true;
            this.btn63.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn64
            // 
            this.btn64.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn64.Location = new System.Drawing.Point(238, 34);
            this.btn64.Name = "btn64";
            this.btn64.Size = new System.Drawing.Size(35, 35);
            this.btn64.TabIndex = 37;
            this.btn64.Tag = "urdu1";
            this.btn64.Text = "G";
            this.btn64.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn64.UseVisualStyleBackColor = true;
            this.btn64.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn41
            // 
            this.btn41.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn41.Location = new System.Drawing.Point(0, 0);
            this.btn41.Name = "btn41";
            this.btn41.Size = new System.Drawing.Size(35, 35);
            this.btn41.TabIndex = 4;
            this.btn41.Tag = "urdu1";
            this.btn41.Text = "Q";
            this.btn41.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn41.UseVisualStyleBackColor = true;
            this.btn41.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn42
            // 
            this.btn42.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn42.Location = new System.Drawing.Point(34, 0);
            this.btn42.Name = "btn42";
            this.btn42.Size = new System.Drawing.Size(35, 35);
            this.btn42.TabIndex = 5;
            this.btn42.Tag = "urdu1";
            this.btn42.Text = "W";
            this.btn42.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn42.UseVisualStyleBackColor = true;
            this.btn42.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5A
            // 
            this.btn5A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn5A.Location = new System.Drawing.Point(0, 34);
            this.btn5A.Name = "btn5A";
            this.btn5A.Size = new System.Drawing.Size(35, 35);
            this.btn5A.TabIndex = 30;
            this.btn5A.Tag = "urdu1";
            this.btn5A.Text = "M";
            this.btn5A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn5A.UseVisualStyleBackColor = true;
            this.btn5A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn43
            // 
            this.btn43.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn43.Location = new System.Drawing.Point(68, 0);
            this.btn43.Name = "btn43";
            this.btn43.Size = new System.Drawing.Size(35, 35);
            this.btn43.TabIndex = 6;
            this.btn43.Tag = "urdu1";
            this.btn43.Text = "E";
            this.btn43.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn43.UseVisualStyleBackColor = true;
            this.btn43.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn59
            // 
            this.btn59.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn59.Location = new System.Drawing.Point(816, 0);
            this.btn59.Name = "btn59";
            this.btn59.Size = new System.Drawing.Size(35, 35);
            this.btn59.TabIndex = 29;
            this.btn59.Tag = "urdu1";
            this.btn59.Text = "N";
            this.btn59.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn59.UseVisualStyleBackColor = true;
            this.btn59.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn44
            // 
            this.btn44.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn44.Location = new System.Drawing.Point(102, 0);
            this.btn44.Name = "btn44";
            this.btn44.Size = new System.Drawing.Size(35, 35);
            this.btn44.TabIndex = 7;
            this.btn44.Tag = "urdu1";
            this.btn44.Text = "R";
            this.btn44.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn44.UseVisualStyleBackColor = true;
            this.btn44.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn58
            // 
            this.btn58.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn58.Location = new System.Drawing.Point(782, 0);
            this.btn58.Name = "btn58";
            this.btn58.Size = new System.Drawing.Size(35, 35);
            this.btn58.TabIndex = 28;
            this.btn58.Tag = "urdu1";
            this.btn58.Text = "B";
            this.btn58.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn58.UseVisualStyleBackColor = true;
            this.btn58.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn45
            // 
            this.btn45.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn45.Location = new System.Drawing.Point(136, 0);
            this.btn45.Name = "btn45";
            this.btn45.Size = new System.Drawing.Size(35, 35);
            this.btn45.TabIndex = 8;
            this.btn45.Tag = "urdu1";
            this.btn45.Text = "T";
            this.btn45.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn45.UseVisualStyleBackColor = true;
            this.btn45.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn57
            // 
            this.btn57.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn57.Location = new System.Drawing.Point(748, 0);
            this.btn57.Name = "btn57";
            this.btn57.Size = new System.Drawing.Size(35, 35);
            this.btn57.TabIndex = 27;
            this.btn57.Tag = "urdu1";
            this.btn57.Text = "V";
            this.btn57.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn57.UseVisualStyleBackColor = true;
            this.btn57.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn46
            // 
            this.btn46.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn46.Location = new System.Drawing.Point(170, 0);
            this.btn46.Name = "btn46";
            this.btn46.Size = new System.Drawing.Size(35, 35);
            this.btn46.TabIndex = 9;
            this.btn46.Tag = "urdu1";
            this.btn46.Text = "Y";
            this.btn46.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn46.UseVisualStyleBackColor = true;
            this.btn46.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn56
            // 
            this.btn56.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn56.Location = new System.Drawing.Point(714, 0);
            this.btn56.Name = "btn56";
            this.btn56.Size = new System.Drawing.Size(35, 35);
            this.btn56.TabIndex = 26;
            this.btn56.Tag = "urdu1";
            this.btn56.Text = "C";
            this.btn56.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn56.UseVisualStyleBackColor = true;
            this.btn56.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn47
            // 
            this.btn47.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn47.Location = new System.Drawing.Point(204, 0);
            this.btn47.Name = "btn47";
            this.btn47.Size = new System.Drawing.Size(35, 35);
            this.btn47.TabIndex = 10;
            this.btn47.Tag = "urdu1";
            this.btn47.Text = "U";
            this.btn47.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn47.UseVisualStyleBackColor = true;
            this.btn47.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn55
            // 
            this.btn55.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn55.Location = new System.Drawing.Point(680, 0);
            this.btn55.Name = "btn55";
            this.btn55.Size = new System.Drawing.Size(35, 35);
            this.btn55.TabIndex = 25;
            this.btn55.Tag = "urdu1";
            this.btn55.Text = "X";
            this.btn55.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn55.UseVisualStyleBackColor = true;
            this.btn55.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn48
            // 
            this.btn48.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn48.Location = new System.Drawing.Point(238, 0);
            this.btn48.Name = "btn48";
            this.btn48.Size = new System.Drawing.Size(35, 35);
            this.btn48.TabIndex = 11;
            this.btn48.Tag = "urdu1";
            this.btn48.Text = "I";
            this.btn48.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn48.UseVisualStyleBackColor = true;
            this.btn48.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn54
            // 
            this.btn54.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn54.Location = new System.Drawing.Point(646, 0);
            this.btn54.Name = "btn54";
            this.btn54.Size = new System.Drawing.Size(35, 35);
            this.btn54.TabIndex = 24;
            this.btn54.Tag = "urdu1";
            this.btn54.Text = "Z";
            this.btn54.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn54.UseVisualStyleBackColor = true;
            this.btn54.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn49
            // 
            this.btn49.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn49.Location = new System.Drawing.Point(272, 0);
            this.btn49.Name = "btn49";
            this.btn49.Size = new System.Drawing.Size(35, 35);
            this.btn49.TabIndex = 12;
            this.btn49.Tag = "urdu1";
            this.btn49.Text = "O";
            this.btn49.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn49.UseVisualStyleBackColor = true;
            this.btn49.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4A
            // 
            this.btn4A.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4A.Location = new System.Drawing.Point(306, 0);
            this.btn4A.Name = "btn4A";
            this.btn4A.Size = new System.Drawing.Size(35, 35);
            this.btn4A.TabIndex = 13;
            this.btn4A.Tag = "urdu1";
            this.btn4A.Text = "P";
            this.btn4A.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4A.UseVisualStyleBackColor = true;
            this.btn4A.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn53
            // 
            this.btn53.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn53.Location = new System.Drawing.Point(612, 0);
            this.btn53.Name = "btn53";
            this.btn53.Size = new System.Drawing.Size(35, 35);
            this.btn53.TabIndex = 22;
            this.btn53.Tag = "urdu1";
            this.btn53.Text = "L";
            this.btn53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn53.UseVisualStyleBackColor = true;
            this.btn53.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4B
            // 
            this.btn4B.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4B.Location = new System.Drawing.Point(340, 0);
            this.btn4B.Name = "btn4B";
            this.btn4B.Size = new System.Drawing.Size(35, 35);
            this.btn4B.TabIndex = 14;
            this.btn4B.Tag = "urdu1";
            this.btn4B.Text = "A";
            this.btn4B.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4B.UseVisualStyleBackColor = true;
            this.btn4B.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn52
            // 
            this.btn52.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn52.Location = new System.Drawing.Point(578, 0);
            this.btn52.Name = "btn52";
            this.btn52.Size = new System.Drawing.Size(35, 35);
            this.btn52.TabIndex = 21;
            this.btn52.Tag = "urdu1";
            this.btn52.Text = "K";
            this.btn52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn52.UseVisualStyleBackColor = true;
            this.btn52.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4C
            // 
            this.btn4C.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4C.Location = new System.Drawing.Point(374, 0);
            this.btn4C.Name = "btn4C";
            this.btn4C.Size = new System.Drawing.Size(35, 35);
            this.btn4C.TabIndex = 15;
            this.btn4C.Tag = "urdu1";
            this.btn4C.Text = "S";
            this.btn4C.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4C.UseVisualStyleBackColor = true;
            this.btn4C.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn51
            // 
            this.btn51.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn51.Location = new System.Drawing.Point(544, 0);
            this.btn51.Name = "btn51";
            this.btn51.Size = new System.Drawing.Size(35, 35);
            this.btn51.TabIndex = 20;
            this.btn51.Tag = "urdu1";
            this.btn51.Text = "J";
            this.btn51.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn51.UseVisualStyleBackColor = true;
            this.btn51.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4D
            // 
            this.btn4D.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4D.Location = new System.Drawing.Point(408, 0);
            this.btn4D.Name = "btn4D";
            this.btn4D.Size = new System.Drawing.Size(35, 35);
            this.btn4D.TabIndex = 16;
            this.btn4D.Tag = "urdu1";
            this.btn4D.Text = "D";
            this.btn4D.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4D.UseVisualStyleBackColor = true;
            this.btn4D.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn50
            // 
            this.btn50.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn50.Location = new System.Drawing.Point(510, 0);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(35, 35);
            this.btn50.TabIndex = 19;
            this.btn50.Tag = "urdu1";
            this.btn50.Text = "H";
            this.btn50.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4E
            // 
            this.btn4E.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4E.Location = new System.Drawing.Point(442, 0);
            this.btn4E.Name = "btn4E";
            this.btn4E.Size = new System.Drawing.Size(35, 35);
            this.btn4E.TabIndex = 17;
            this.btn4E.Tag = "urdu1";
            this.btn4E.Text = "F";
            this.btn4E.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4E.UseVisualStyleBackColor = true;
            this.btn4E.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4F
            // 
            this.btn4F.Font = new System.Drawing.Font("AlKatib1", 12F);
            this.btn4F.Location = new System.Drawing.Point(476, 0);
            this.btn4F.Name = "btn4F";
            this.btn4F.Size = new System.Drawing.Size(35, 35);
            this.btn4F.TabIndex = 18;
            this.btn4F.Tag = "urdu1";
            this.btn4F.Text = "G";
            this.btn4F.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn4F.UseVisualStyleBackColor = true;
            this.btn4F.Click += new System.EventHandler(this.btn_Click);
            // 
            // chbActive
            // 
            this.chbActive.AutoSize = true;
            this.chbActive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbActive.Checked = true;
            this.chbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbActive.Location = new System.Drawing.Point(81, 35);
            this.chbActive.Name = "chbActive";
            this.chbActive.Size = new System.Drawing.Size(65, 17);
            this.chbActive.TabIndex = 4;
            this.chbActive.Text = "Active : ";
            this.chbActive.UseVisualStyleBackColor = true;
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(152, 35);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDescription.Size = new System.Drawing.Size(579, 62);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.Tag = "urdu";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("AlKatib1", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(737, 34);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(86, 40);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = ": PB�Af�";
            // 
            // frmDosageInstruction
            // 
            this.ClientSize = new System.Drawing.Size(860, 670);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MinimizeBox = false;
            this.Name = "frmDosageInstruction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dosage Instruction";
            this.Load += new System.EventHandler(this.frmDosageInstruction_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tsDosageInstruction.ResumeLayout(false);
            this.tsDosageInstruction.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDosageInstruction)).EndInit();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ToolStrip tsDosageInstruction;
		private System.Windows.Forms.ToolStripButton tsbSave;
		private System.Windows.Forms.ToolStripButton tsbRefresh;
		private System.Windows.Forms.ToolStripButton tsbExit;
		private System.Windows.Forms.CheckBox chbActive;
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Button btn2020;
		private System.Windows.Forms.Button btn2021;
		private System.Windows.Forms.Button btn2022;
		private System.Windows.Forms.Button btn2026;
		private System.Windows.Forms.Button btn2219;
		private System.Windows.Forms.Button btn2030;
		private System.Windows.Forms.Button btn2122;
		private System.Windows.Forms.Button btn2039;
		private System.Windows.Forms.Button btn203A;
		private System.Windows.Forms.Button btn201E;
		private System.Windows.Forms.Button btn201D;
		private System.Windows.Forms.Button btn201C;
		private System.Windows.Forms.Button btn201A;
		private System.Windows.Forms.Button btn2019;
		private System.Windows.Forms.Button btn2018;
		private System.Windows.Forms.Button btn2DC;
		private System.Windows.Forms.Button btn2C6;
		private System.Windows.Forms.Button btnED;
		private System.Windows.Forms.Button btnEE;
		private System.Windows.Forms.Button btnEF;
		private System.Windows.Forms.Button btn192;
		private System.Windows.Forms.Button btnF0;
		private System.Windows.Forms.Button btn161;
		private System.Windows.Forms.Button btnF1;
		private System.Windows.Forms.Button btn160;
		private System.Windows.Forms.Button btn152;
		private System.Windows.Forms.Button btn153;
		private System.Windows.Forms.Button btnD3;
		private System.Windows.Forms.Button btnD4;
		private System.Windows.Forms.Button btnEC;
		private System.Windows.Forms.Button btnD5;
		private System.Windows.Forms.Button btnEB;
		private System.Windows.Forms.Button btnD6;
		private System.Windows.Forms.Button btnEA;
		private System.Windows.Forms.Button btnD7;
		private System.Windows.Forms.Button btnE9;
		private System.Windows.Forms.Button btnD8;
		private System.Windows.Forms.Button btnE8;
		private System.Windows.Forms.Button btnE0;
		private System.Windows.Forms.Button btnE7;
		private System.Windows.Forms.Button btnD9;
		private System.Windows.Forms.Button btnE6;
		private System.Windows.Forms.Button btnDA;
		private System.Windows.Forms.Button btnDB;
		private System.Windows.Forms.Button btnE5;
		private System.Windows.Forms.Button btnDC;
		private System.Windows.Forms.Button btnE4;
		private System.Windows.Forms.Button btnDD;
		private System.Windows.Forms.Button btnE3;
		private System.Windows.Forms.Button btnDE;
		private System.Windows.Forms.Button btnE2;
		private System.Windows.Forms.Button btnDF;
		private System.Windows.Forms.Button btnE1;
		private System.Windows.Forms.Button btnC9;
		private System.Windows.Forms.Button btnCA;
		private System.Windows.Forms.Button btnCB;
		private System.Windows.Forms.Button btnD2;
		private System.Windows.Forms.Button btnCC;
		private System.Windows.Forms.Button btnD1;
		private System.Windows.Forms.Button btnCD;
		private System.Windows.Forms.Button btnD0;
		private System.Windows.Forms.Button btnCE;
		private System.Windows.Forms.Button btnCF;
		private System.Windows.Forms.Button btnAF;
		private System.Windows.Forms.Button btnB0;
		private System.Windows.Forms.Button btnC8;
		private System.Windows.Forms.Button btnB1;
		private System.Windows.Forms.Button btnC7;
		private System.Windows.Forms.Button btnB2;
		private System.Windows.Forms.Button btnC6;
		private System.Windows.Forms.Button btnB3;
		private System.Windows.Forms.Button btnC5;
		private System.Windows.Forms.Button btnB4;
		private System.Windows.Forms.Button btnC4;
		private System.Windows.Forms.Button btnB5;
		private System.Windows.Forms.Button btnC3;
		private System.Windows.Forms.Button btnB6;
		private System.Windows.Forms.Button btnC2;
		private System.Windows.Forms.Button btnB7;
		private System.Windows.Forms.Button btnB8;
		private System.Windows.Forms.Button btnC1;
		private System.Windows.Forms.Button btnB9;
		private System.Windows.Forms.Button btnC0;
		private System.Windows.Forms.Button btnBA;
		private System.Windows.Forms.Button btnBF;
		private System.Windows.Forms.Button btnBB;
		private System.Windows.Forms.Button btnBE;
		private System.Windows.Forms.Button btnBC;
		private System.Windows.Forms.Button btnBD;
		private System.Windows.Forms.Button btnSpace;
		private System.Windows.Forms.Button btnBS;
		private System.Windows.Forms.Button btnA5;
		private System.Windows.Forms.Button btnA6;
		private System.Windows.Forms.Button btnA7;
		private System.Windows.Forms.Button btnAE;
		private System.Windows.Forms.Button btnA8;
		private System.Windows.Forms.Button btnAD;
		private System.Windows.Forms.Button btnA9;
		private System.Windows.Forms.Button btnAC;
		private System.Windows.Forms.Button btnAA;
		private System.Windows.Forms.Button btnAB;
		private System.Windows.Forms.Button btn68;
		private System.Windows.Forms.Button btn69;
		private System.Windows.Forms.Button btnA4;
		private System.Windows.Forms.Button btn6A;
		private System.Windows.Forms.Button btnA3;
		private System.Windows.Forms.Button btn6B;
		private System.Windows.Forms.Button btnA2;
		private System.Windows.Forms.Button btn6C;
		private System.Windows.Forms.Button btnA1;
		private System.Windows.Forms.Button btn6D;
		private System.Windows.Forms.Button btn7E;
		private System.Windows.Forms.Button btn6E;
		private System.Windows.Forms.Button btn7C;
		private System.Windows.Forms.Button btn6F;
		private System.Windows.Forms.Button btn7B;
		private System.Windows.Forms.Button btn70;
		private System.Windows.Forms.Button btn71;
		private System.Windows.Forms.Button btn7A;
		private System.Windows.Forms.Button btn72;
		private System.Windows.Forms.Button btn79;
		private System.Windows.Forms.Button btn73;
		private System.Windows.Forms.Button btn78;
		private System.Windows.Forms.Button btn74;
		private System.Windows.Forms.Button btn77;
		private System.Windows.Forms.Button btn75;
		private System.Windows.Forms.Button btn76;
		private System.Windows.Forms.Button btn5C;
		private System.Windows.Forms.Button btn5F;
		private System.Windows.Forms.Button btn60;
		private System.Windows.Forms.Button btn67;
		private System.Windows.Forms.Button btn61;
		private System.Windows.Forms.Button btn66;
		private System.Windows.Forms.Button btn62;
		private System.Windows.Forms.Button btn65;
		private System.Windows.Forms.Button btn63;
		private System.Windows.Forms.Button btn64;
		private System.Windows.Forms.Button btn41;
		private System.Windows.Forms.Button btn42;
		private System.Windows.Forms.Button btn5A;
		private System.Windows.Forms.Button btn43;
		private System.Windows.Forms.Button btn59;
		private System.Windows.Forms.Button btn44;
		private System.Windows.Forms.Button btn58;
		private System.Windows.Forms.Button btn45;
		private System.Windows.Forms.Button btn57;
		private System.Windows.Forms.Button btn46;
		private System.Windows.Forms.Button btn56;
		private System.Windows.Forms.Button btn47;
		private System.Windows.Forms.Button btn55;
		private System.Windows.Forms.Button btn48;
		private System.Windows.Forms.Button btn54;
		private System.Windows.Forms.Button btn49;
		private System.Windows.Forms.Button btn4A;
		private System.Windows.Forms.Button btn53;
		private System.Windows.Forms.Button btn4B;
		private System.Windows.Forms.Button btn52;
		private System.Windows.Forms.Button btn4C;
		private System.Windows.Forms.Button btn51;
		private System.Windows.Forms.Button btn4D;
		private System.Windows.Forms.Button btn50;
		private System.Windows.Forms.Button btn4E;
		private System.Windows.Forms.Button btn4F;
		private System.Windows.Forms.DataGridView dgDosageInstruction;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblDosageInstruction;
		private System.Windows.Forms.DataGridViewTextBoxColumn DosageInstructionID;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
		private System.Windows.Forms.DataGridViewTextBoxColumn Description;
		private System.Windows.Forms.DataGridViewTextBoxColumn PesonID;
		private System.Windows.Forms.DataGridViewTextBoxColumn EnteredBy;
		private System.Windows.Forms.DataGridViewTextBoxColumn EnteredOn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClientID;
	}
}