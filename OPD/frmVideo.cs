using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmVideo : Form
    {
        # region "Class variables" and "Properties"

        private string _PatientID = "";
        private string _PatientName = "";
        private string _Path = "";
        private frmOPD MainOPD = null;

        //private string _FileName = "";
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string PatientName
        {
            set { _PatientName = value; }
        }

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }

        #endregion

        public frmVideo(string PatientID)
        {
            InitializeComponent();
            _PatientID = PatientID;
        }

        public frmVideo(string PatientID, frmOPD Frm)
        {
            InitializeComponent();
            _PatientID = PatientID;
            this.MainOPD = Frm;
        }

        private void frmVideo_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);

            this.Text += " ( " + clsSharedVariables.InItCaps(_PatientName) + " )";
            objComp.ApplyStyleToControls(this);
            try
            {
                objConnection.Connection_Open();
                FillDocType(objConnection);
                FillDatagrid(objConnection, objVdo);
                objConnection.Connection_Close();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objVdo = null;
                objComp = null;
            }
        }

        private void FillDocType(clsBLDBConnection objConnection)
        {
            clsBLDocType objDocType = new clsBLDocType(objConnection);
            SComponents objComp = new SComponents();

            objDocType.Type = "V";
            objComp.FillComboBox(cboDocType, objDocType.GetAll(2), "Name", "DocTypeID", true);

            objDocType = null;
            objComp = null;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            btnClear_Click(btnClear, e);
            SelectVideo();
        }

        private void SelectVideo()
        {
            OFDVdo.Filter = "Video (*.Avi;*.MPEG;*.MPG)|*.AVi;*.MPAG;*.MPG| All files (*.*)|*.*";

            // Allow the user to select multiple images.
            OFDVdo.Multiselect = false;
            OFDVdo.Title = "My Video Browser";
            DialogResult dr = OFDVdo.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                Player.FileName = OFDVdo.FileName;
                _Path = OFDVdo.FileName;
                btnSave.Enabled = true;
                btnCopy.Enabled = true;
                txtComment.Text = "";
            }
            else
            {
                btnSave.Enabled = false;
                btnCopy.Enabled = false;
            }

            OFDVdo.Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text.Equals("Save"))
            {
                Insert('P');
            }
            else
            {
                UpdateData('P');
                btnSave.Text = "Save";
            }

            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            txtComment.Text = "";
        }

        private void Insert(char Mode)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);
            string msg = "";

            try
            {
                if (!Path.Equals(""))
                {
                    if (Mode == 'S')
                    {
                        objVdo.SmallPicture = Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path);
                        msg = "Copy File Successfully";
                    }
                    else if (Mode == 'P')
                    {
                        objVdo.SmallPicture = _Path.Replace(":", ":\\");
                        objVdo.SmallPicture = _Path.Replace("\\", "\\\\");
                        msg = "Save Path Successfully";
                    }
                    objVdo = SetBLValues(objVdo, Mode);

                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    if (objVdo.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                        FillDatagrid(objConnection, objVdo);


                        MessageBox.Show(msg, "Save", MessageBoxButtons.OK, MessageBoxIcon.
            Information);
                    }
                    else
                    {
                        MessageBox.Show(objVdo.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objVdo = null;
                objConnection = null;
                msg = null;
            }
        }

        private void UpdateData(char Mode)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);
            string msg = "";

            try
            {
                objVdo.PictureID = this.lblPictureID.Text;
                if (Mode == 'S')
                {
                    objVdo.SmallPicture = Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path);
                    msg = "Update Copying File Successfully";
                }
                else if (Mode == 'P')
                {
                    objVdo.SmallPicture = _Path.Replace(":", ":\\");
                    objVdo.SmallPicture = _Path.Replace("\\", "\\\\");

                    msg = "Update path Successfully";
                }
                objVdo = SetBLValues(objVdo, Mode);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objVdo.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillDatagrid(objConnection, objVdo);
                    objConnection.Connection_Close();

                    MessageBox.Show(msg, "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objVdo.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objVdo = null;
                objConnection = null;
            }
        }

        private clsBLPatientPicture SetBLValues(clsBLPatientPicture objVdo, int Mode)
        {
            objVdo.PatientID = PatientID;
            objVdo.Comment = clsSharedVariables.InItCaps(txtComment.Text.Trim());
            if (Mode == 'S')
            {
                objVdo.SavedMethod = "VS";
                
            }
            else if (Mode == 'P')
            {
                objVdo.SavedMethod = "VP";
            }
            if (cboDocType.SelectedIndex != 0)
            {
                objVdo.DocTypeID = cboDocType.SelectedValue.ToString();
            }
            objVdo.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            return objVdo;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);

            try
            {
                Player.FileName = null;
                if (lblMode.Text.Equals("C"))
                {
                    if (!System.IO.File.Exists(_Path.Replace("\\", "")))
                    {
                        System.IO.File.Delete(_Path);
                        //Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(_Path,Microsoft.VisualBasic.FileIO.UIOption
                    }
                }
                objConnection.Connection_Open();
                objVdo.PictureID = lblPictureID.Text;
                objVdo.Delete();
                FillDatagrid(objConnection, objVdo);
                btnDelete.Enabled = false;
                btnSave.Enabled = false;
                btnCopy.Enabled = false;
            }
            finally
            {
                objConnection.Connection_Close();
                btnDelete.Enabled = false;
                objVdo = null;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            Player.FileName = null;
            txtComment.Text = "";
            btnDelete.Enabled = false;
            dgVdo.ClearSelection();
            btnSave.Text = "Save";
            cboDocType.SelectedIndex = 0;
        }

        private void FillDatagrid(clsBLDBConnection objConnection, clsBLPatientPicture objVdo)
        {
            DataView dvVdo;
            try
            {
                objVdo.PatientID = PatientID;
                if (cboDocType.SelectedIndex != 0)
                {
                    objVdo.DocTypeID = cboDocType.SelectedValue.ToString();
                }

                dvVdo = objVdo.GetAll(2);
                dgVdo.DataSource = dvVdo;

                for (int i = 0; i < dgVdo.Rows.Count; i++)
                {
                    if (dgVdo.Rows[i].Cells["SavedMethod"].Value.ToString().Contains("VS"))
                    {
                        dgVdo.Rows[i].Cells["comments"].Value = dgVdo.Rows[i].Cells["comments"].Value.ToString();
                        dgVdo.Rows[i].Cells["Mode"].Value = "S";
                    }
                    else if (dgVdo.Rows[i].Cells["SavedMethod"].Value.ToString().Contains("VP"))
                    {
                        dgVdo.Rows[i].Cells["comments"].Value = dgVdo.Rows[i].Cells["comments"].Value.ToString();
                        dgVdo.Rows[i].Cells["Mode"].Value = "P";
                    }
                }
            }
            finally
            {
                dvVdo = null;
            }
        }

        private void dgVdo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                btnDelete.Enabled = true;
                FillHoldPatient(e.RowIndex);
            }
        }

        private void FillHoldPatient(int RowIndex)
        {
            btnSave.Text = "Update";
            btnSave.Enabled = true;
            btnCopy.Enabled = true;

            txtComment.Text = clsSharedVariables.InItCaps(dgVdo.Rows[RowIndex].Cells["Comments"].Value.ToString());
            if (dgVdo.Rows[RowIndex].Cells["Mode"].Value.Equals("S"))
            {
                Player.FileName = Application.StartupPath + "\\Videos\\" + dgVdo.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
                _Path = Application.StartupPath + "\\Videos\\" + dgVdo.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
                lblMode.Text = "C";
            }
            else if (dgVdo.Rows[RowIndex].Cells["Mode"].Value.Equals("P"))
            {
                lblMode.Text = "P";
                Player.FileName = dgVdo.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
                _Path = dgVdo.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
            }

            lblPatientID.Text = dgVdo.Rows[RowIndex].Cells["PatientID1"].Value.ToString();
            lblPictureID.Text = dgVdo.Rows[RowIndex].Cells["PictureID"].Value.ToString();
        }

        private void txtInItCap_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (btnSave.Text.Equals("Save"))
            {
                Insert('S');
            }
            else
            {
                UpdateData('S');
                btnSave.Text = "Save";
            }
            if (!Microsoft.VisualBasic.FileIO.FileSystem.FileExists(Application.StartupPath + "\\Videos\\" + Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path)))
            {
                Microsoft.VisualBasic.FileIO.FileSystem.CopyFile(_Path, Application.StartupPath + "\\Videos\\" + Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path));
            }
            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            txtComment.Text = "";
        }

        private void frmVideo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }

        private void btnAddNewType_Click(object sender, EventArgs e)
        {
            frmDocType objDocType = new frmDocType(this);
            this.Hide();
            objDocType.Show();
        }

        private void cboDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);
            try
            {
                objConnection.Connection_Open();
                FillDatagrid(objConnection, objVdo);
                objConnection.Connection_Close();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objVdo = null;
            }
        }
    }
}