namespace OPD
{
    partial class frmPatientNotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlPatientNotes = new System.Windows.Forms.Panel();
            this.dgTextNotes = new System.Windows.Forms.DataGridView();
            this.TextDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TextNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbVoice = new System.Windows.Forms.RadioButton();
            this.rbText = new System.Windows.Forms.RadioButton();
            this.pnlVoiceNotes = new System.Windows.Forms.Panel();
            this.dgVoiceNotes = new System.Windows.Forms.DataGridView();
            this.VoiceNotesID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblNotesHeading = new System.Windows.Forms.Label();
            this.pnlPatientNotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTextNotes)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlVoiceNotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVoiceNotes)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPatientNotes
            // 
            this.pnlPatientNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPatientNotes.Controls.Add(this.dgTextNotes);
            this.pnlPatientNotes.Location = new System.Drawing.Point(0, 25);
            this.pnlPatientNotes.Name = "pnlPatientNotes";
            this.pnlPatientNotes.Size = new System.Drawing.Size(600, 375);
            this.pnlPatientNotes.TabIndex = 1;
            // 
            // dgTextNotes
            // 
            this.dgTextNotes.AllowDrop = true;
            this.dgTextNotes.AllowUserToAddRows = false;
            this.dgTextNotes.AllowUserToDeleteRows = false;
            this.dgTextNotes.AllowUserToOrderColumns = true;
            this.dgTextNotes.AllowUserToResizeColumns = false;
            this.dgTextNotes.AllowUserToResizeRows = false;
            this.dgTextNotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTextNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTextNotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TextDate,
            this.TextNotes});
            this.dgTextNotes.Location = new System.Drawing.Point(-1, -1);
            this.dgTextNotes.MultiSelect = false;
            this.dgTextNotes.Name = "dgTextNotes";
            this.dgTextNotes.ReadOnly = true;
            this.dgTextNotes.RowHeadersWidth = 20;
            this.dgTextNotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTextNotes.Size = new System.Drawing.Size(600, 375);
            this.dgTextNotes.TabIndex = 0;
            // 
            // TextDate
            // 
            this.TextDate.DataPropertyName = "VisitDate";
            this.TextDate.FillWeight = 15F;
            this.TextDate.HeaderText = "Date";
            this.TextDate.Name = "TextDate";
            this.TextDate.ReadOnly = true;
            // 
            // TextNotes
            // 
            this.TextNotes.DataPropertyName = "Notes";
            this.TextNotes.FillWeight = 85F;
            this.TextNotes.HeaderText = "Notes";
            this.TextNotes.Name = "TextNotes";
            this.TextNotes.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbVoice);
            this.panel1.Controls.Add(this.rbText);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(142, 24);
            this.panel1.TabIndex = 2;
            this.panel1.Tag = "med";
            this.panel1.Visible = false;
            // 
            // rbVoice
            // 
            this.rbVoice.AutoSize = true;
            this.rbVoice.Location = new System.Drawing.Point(70, 2);
            this.rbVoice.Name = "rbVoice";
            this.rbVoice.Size = new System.Drawing.Size(52, 17);
            this.rbVoice.TabIndex = 1;
            this.rbVoice.Text = "Voice";
            this.rbVoice.UseVisualStyleBackColor = true;
            this.rbVoice.CheckedChanged += new System.EventHandler(this.rbText_CheckedChanged);
            // 
            // rbText
            // 
            this.rbText.AutoSize = true;
            this.rbText.Checked = true;
            this.rbText.Location = new System.Drawing.Point(18, 2);
            this.rbText.Name = "rbText";
            this.rbText.Size = new System.Drawing.Size(46, 17);
            this.rbText.TabIndex = 0;
            this.rbText.TabStop = true;
            this.rbText.Text = "Text";
            this.rbText.UseVisualStyleBackColor = true;
            this.rbText.CheckedChanged += new System.EventHandler(this.rbText_CheckedChanged);
            // 
            // pnlVoiceNotes
            // 
            this.pnlVoiceNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVoiceNotes.Controls.Add(this.dgVoiceNotes);
            this.pnlVoiceNotes.Location = new System.Drawing.Point(0, 25);
            this.pnlVoiceNotes.Name = "pnlVoiceNotes";
            this.pnlVoiceNotes.Size = new System.Drawing.Size(600, 375);
            this.pnlVoiceNotes.TabIndex = 3;
            // 
            // dgVoiceNotes
            // 
            this.dgVoiceNotes.AllowUserToAddRows = false;
            this.dgVoiceNotes.AllowUserToDeleteRows = false;
            this.dgVoiceNotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVoiceNotes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgVoiceNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVoiceNotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoiceNotesID,
            this.EnteredDate,
            this.Comments,
            this.Path});
            this.dgVoiceNotes.EnableHeadersVisualStyles = false;
            this.dgVoiceNotes.Location = new System.Drawing.Point(-1, -1);
            this.dgVoiceNotes.MultiSelect = false;
            this.dgVoiceNotes.Name = "dgVoiceNotes";
            this.dgVoiceNotes.ReadOnly = true;
            this.dgVoiceNotes.RowHeadersWidth = 20;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVoiceNotes.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgVoiceNotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVoiceNotes.Size = new System.Drawing.Size(600, 375);
            this.dgVoiceNotes.TabIndex = 22;
            this.dgVoiceNotes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVoiceNotes_CellDoubleClick);
            // 
            // VoiceNotesID
            // 
            this.VoiceNotesID.DataPropertyName = "VoiceNotesID";
            this.VoiceNotesID.HeaderText = "VoiceNotesID";
            this.VoiceNotesID.Name = "VoiceNotesID";
            this.VoiceNotesID.ReadOnly = true;
            this.VoiceNotesID.Visible = false;
            // 
            // EnteredDate
            // 
            this.EnteredDate.DataPropertyName = "EnteredOn";
            this.EnteredDate.FillWeight = 15F;
            this.EnteredDate.HeaderText = "Date";
            this.EnteredDate.Name = "EnteredDate";
            this.EnteredDate.ReadOnly = true;
            // 
            // Comments
            // 
            this.Comments.DataPropertyName = "Description";
            this.Comments.FillWeight = 85F;
            this.Comments.HeaderText = "Comments";
            this.Comments.Name = "Comments";
            this.Comments.ReadOnly = true;
            // 
            // Path
            // 
            this.Path.DataPropertyName = "Path";
            this.Path.FillWeight = 20F;
            this.Path.HeaderText = "Path";
            this.Path.Name = "Path";
            this.Path.ReadOnly = true;
            this.Path.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::OPD.Properties.Resources.Cancel_15;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.Location = new System.Drawing.Point(576, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(25, 25);
            this.btnClose.TabIndex = 0;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblNotesHeading);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(573, 24);
            this.panel2.TabIndex = 3;
            this.panel2.Tag = "med";
            // 
            // lblNotesHeading
            // 
            this.lblNotesHeading.AutoSize = true;
            this.lblNotesHeading.Location = new System.Drawing.Point(268, 5);
            this.lblNotesHeading.Name = "lblNotesHeading";
            this.lblNotesHeading.Size = new System.Drawing.Size(35, 13);
            this.lblNotesHeading.TabIndex = 0;
            this.lblNotesHeading.Tag = "transdisplay";
            this.lblNotesHeading.Text = "Notes";
            // 
            // frmPatientNotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.pnlPatientNotes);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pnlVoiceNotes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmPatientNotes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patient Notes";
            this.Load += new System.EventHandler(this.frmPatientNotes_Load);
            this.pnlPatientNotes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTextNotes)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlVoiceNotes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVoiceNotes)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlPatientNotes;
        private System.Windows.Forms.DataGridView dgTextNotes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbVoice;
        private System.Windows.Forms.RadioButton rbText;
        private System.Windows.Forms.DataGridViewTextBoxColumn TextDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TextNotes;
        private System.Windows.Forms.Panel pnlVoiceNotes;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblNotesHeading;
        private System.Windows.Forms.DataGridView dgVoiceNotes;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoiceNotesID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path;
    }
}