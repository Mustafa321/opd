﻿namespace OPD
{
    partial class Notifications
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NotificationGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.NotificationGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // NotificationGridView
            // 
            this.NotificationGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NotificationGridView.Location = new System.Drawing.Point(-3, 2);
            this.NotificationGridView.Name = "NotificationGridView";
            this.NotificationGridView.Size = new System.Drawing.Size(689, 276);
            this.NotificationGridView.TabIndex = 0;
            // 
            // Notifications
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 280);
            this.Controls.Add(this.NotificationGridView);
            this.Name = "Notifications";
            this.Text = "Notifications";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Notifications_FormClosed);
            this.Load += new System.EventHandler(this.Notifications_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NotificationGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView NotificationGridView;
    }
}