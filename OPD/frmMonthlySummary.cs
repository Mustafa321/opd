using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace OPD
{
    public partial class frmMonthlySummary : Form
    {
        public frmMonthlySummary()
        {
            InitializeComponent();
        }

        private void frmMonthlySummary_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD = new clsBLOPD(objConnection);

            DataView dv = new DataView();
            int MinYear = 0;

            objComp.ApplyStyleToControls(this);

            try
            {
                objConnection.Connection_Open();

                dv = objOPD.GetAll(16);
                if (dv.Table.Rows.Count != 0)
                {
                    MinYear = Convert.ToInt16(dv.Table.Rows[0]["MinYear"].ToString());

                    for (int i = Convert.ToInt16(DateTime.Now.ToString("yyyy")); i >= MinYear; i--)
                    {
                        cboYear.Items.Add(i);
                    }

                    objConnection.Connection_Close();

                    cboMonth.Text = DateTime.Now.ToString("MM");
                    if (cboYear.Items.Count != 0)
                    {
                      cboYear.Text = cboYear.Items[0].ToString();
                    }
                    DrawGraph();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            objComp = null;
        }

        private void DrawGraph()
        {
            if (!cboMonth.Text.Equals("") && !cboYear.Text.Equals(""))
            {
                clsBLDBConnection objConnection = new clsBLDBConnection();
                clsBLOPD objOPD = new clsBLOPD(objConnection);
                DataView dv = new DataView();


                ReportDocument report;
                if ((ReportDocument)crvGraph.ReportSource == null)
                {
                    report = new ReportDocument();
                }
                else
                {
                    report = (ReportDocument)crvGraph.ReportSource;
                }

                try
                {
                    objConnection.Connection_Open();

                    objOPD.VisitDate = cboMonth.Text + "/" + cboYear.Text;
                    objOPD.EnteredBy = clsSharedVariables.UserID;
                    dv = objOPD.GetAll(17);

                    report.Load(@"Report\MonthlySummaryChart.rpt");

                    string userName = clsSharedVariables.DBUserName;
                    string pwd = clsSharedVariables.DBPassWord;
                    string serverName = clsSharedVariables.DBServer;
                    string sdb = clsSharedVariables.DBName;

                    report.SetDatabaseLogon(userName, pwd, serverName, sdb);

                    report.SetDataSource(dv.Table);

                    crvGraph.ReportSource = report;

                    //DrawGraph(dv);


                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);

                }
                finally
                {
                    objConnection.Connection_Close();
                    report = null;
                }
            }
        }

        private void cboMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawGraph();
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawGraph();
        }

        private void frmMonthlySummary_FormClosing(object sender, FormClosingEventArgs e)
        {
            ReportDocument rpt = (ReportDocument)crvGraph.ReportSource;

            if (rpt != null)
            {
                rpt.Close();
                rpt.Dispose();
                rpt = null;
            }
            GC.Collect();
        }
    }
}