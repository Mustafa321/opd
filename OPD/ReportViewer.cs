using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using CrystalDecisions.Shared;

namespace OPD
{
	public partial class ReportViewer : Form
	{
		public string StrPRID = "";
		public string StrBookingID= "";
		public string StrTestID= "";

		public string StrSubTableName = "";

		public string StrSelectionFormula= ""; 
		public DataTable dt = null;
		public DataTable dtSub = null;
        private ReportDocument repdoc = new ReportDocument();

		public ReportViewer ( )
		{
			InitializeComponent();
		}

		private void ReportViewer_Load ( object sender , EventArgs e )
		{
			LoadReport();
		}

		public void LoadReport ( )
		{
			DataView dvPrefTabl;
			try
			{
				string strPath = Application.StartupPath + "\\Report\\Result.rpt";
				repdoc.Load(strPath);

				repdoc.RecordSelectionFormula = StrSelectionFormula;
				repdoc.SetDataSource(dt);
				repdoc.Subreports[StrSubTableName].SetDataSource(dtSub);

				int j = repdoc.Database.Tables.Count - 1;

				for (int i = 0 ; i <= j ; i++)
				{
					TableLogOnInfo logOnInfo = new TableLogOnInfo();
					ConnectionInfo connectionInfo = new ConnectionInfo();

					logOnInfo = repdoc.Database.Tables[i].LogOnInfo;

					connectionInfo = logOnInfo.ConnectionInfo;
                    connectionInfo.ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();
                    connectionInfo.Password = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
                    connectionInfo.UserID = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
                    repdoc.Database.Tables[i].ApplyLogOnInfo(logOnInfo);
				}

                //CrystalDecisions.Shared.DiskFileDestinationOptions dfdoCustomers = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                //string szFileName = "Report/Result.pdf";
                //dfdoCustomers.DiskFileName = szFileName;
                //repdoc.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                //repdoc.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                //repdoc.ExportOptions.DestinationOptions = dfdoCustomers;
                //repdoc.Export();

				this.crystalReportViewer1.ReportSource = repdoc;

			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message , "Error" , MessageBoxButtons.OK , MessageBoxIcon.Error);
			}
			finally
			{
				dvPrefTabl = null;
			}
		}

        private void ReportViewer_FormClosed(object sender, FormClosedEventArgs e)
        {
            repdoc.Close();
            repdoc.Dispose();
            repdoc = null;
            GC.Collect();
        }
	}
}