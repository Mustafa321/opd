namespace OPD
{
    partial class frmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangePassword));
          this.btnCancel = new System.Windows.Forms.Button();
          this.lblLoginID = new System.Windows.Forms.Label();
          this.lblOPassword = new System.Windows.Forms.Label();
          this.lblCPassword = new System.Windows.Forms.Label();
          this.lblNewPassword = new System.Windows.Forms.Label();
          this.txtLoginID = new System.Windows.Forms.TextBox();
          this.txtOPassword = new System.Windows.Forms.TextBox();
          this.txtNPassword = new System.Windows.Forms.TextBox();
          this.txtCPassword = new System.Windows.Forms.TextBox();
          this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
          this.btnOK = new System.Windows.Forms.Button();
          this.panel2 = new System.Windows.Forms.Panel();
          this.panel1 = new System.Windows.Forms.Panel();
          this.panel3 = new System.Windows.Forms.Panel();
          this.SuspendLayout();
          // 
          // btnCancel
          // 
          this.btnCancel.BackColor = System.Drawing.Color.LightSlateGray;
          resources.ApplyResources(this.btnCancel, "btnCancel");
          this.btnCancel.Name = "btnCancel";
          this.toolTip1.SetToolTip(this.btnCancel, resources.GetString("btnCancel.ToolTip"));
          this.btnCancel.UseVisualStyleBackColor = false;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // lblLoginID
          // 
          resources.ApplyResources(this.lblLoginID, "lblLoginID");
          this.lblLoginID.Name = "lblLoginID";
          // 
          // lblOPassword
          // 
          resources.ApplyResources(this.lblOPassword, "lblOPassword");
          this.lblOPassword.Name = "lblOPassword";
          // 
          // lblCPassword
          // 
          resources.ApplyResources(this.lblCPassword, "lblCPassword");
          this.lblCPassword.Name = "lblCPassword";
          // 
          // lblNewPassword
          // 
          resources.ApplyResources(this.lblNewPassword, "lblNewPassword");
          this.lblNewPassword.Name = "lblNewPassword";
          // 
          // txtLoginID
          // 
          this.txtLoginID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          resources.ApplyResources(this.txtLoginID, "txtLoginID");
          this.txtLoginID.Name = "txtLoginID";
          this.txtLoginID.ReadOnly = true;
          this.toolTip1.SetToolTip(this.txtLoginID, resources.GetString("txtLoginID.ToolTip"));
          // 
          // txtOPassword
          // 
          this.txtOPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          resources.ApplyResources(this.txtOPassword, "txtOPassword");
          this.txtOPassword.Name = "txtOPassword";
          this.toolTip1.SetToolTip(this.txtOPassword, resources.GetString("txtOPassword.ToolTip"));
          // 
          // txtNPassword
          // 
          this.txtNPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          resources.ApplyResources(this.txtNPassword, "txtNPassword");
          this.txtNPassword.Name = "txtNPassword";
          this.toolTip1.SetToolTip(this.txtNPassword, resources.GetString("txtNPassword.ToolTip"));
          // 
          // txtCPassword
          // 
          this.txtCPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          resources.ApplyResources(this.txtCPassword, "txtCPassword");
          this.txtCPassword.Name = "txtCPassword";
          this.toolTip1.SetToolTip(this.txtCPassword, resources.GetString("txtCPassword.ToolTip"));
          // 
          // btnOK
          // 
          this.btnOK.BackColor = System.Drawing.Color.LightSlateGray;
          resources.ApplyResources(this.btnOK, "btnOK");
          this.btnOK.Name = "btnOK";
          this.toolTip1.SetToolTip(this.btnOK, resources.GetString("btnOK.ToolTip"));
          this.btnOK.UseVisualStyleBackColor = false;
          this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
          // 
          // panel2
          // 
          this.panel2.BackColor = System.Drawing.Color.Red;
          resources.ApplyResources(this.panel2, "panel2");
          this.panel2.Name = "panel2";
          this.panel2.Tag = "no";
          // 
          // panel1
          // 
          this.panel1.BackColor = System.Drawing.Color.Red;
          resources.ApplyResources(this.panel1, "panel1");
          this.panel1.Name = "panel1";
          this.panel1.Tag = "no";
          // 
          // panel3
          // 
          this.panel3.BackColor = System.Drawing.Color.Red;
          resources.ApplyResources(this.panel3, "panel3");
          this.panel3.Name = "panel3";
          this.panel3.Tag = "no";
          // 
          // frmChangePassword
          // 
          resources.ApplyResources(this, "$this");
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.Controls.Add(this.panel3);
          this.Controls.Add(this.panel1);
          this.Controls.Add(this.panel2);
          this.Controls.Add(this.txtCPassword);
          this.Controls.Add(this.txtNPassword);
          this.Controls.Add(this.txtOPassword);
          this.Controls.Add(this.txtLoginID);
          this.Controls.Add(this.lblNewPassword);
          this.Controls.Add(this.lblCPassword);
          this.Controls.Add(this.lblOPassword);
          this.Controls.Add(this.lblLoginID);
          this.Controls.Add(this.btnOK);
          this.Controls.Add(this.btnCancel);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
          this.MaximizeBox = false;
          this.Name = "frmChangePassword";
          this.Tag = "no";
          this.Load += new System.EventHandler(this.frmChangePassword_Load);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblLoginID;
        private System.Windows.Forms.Label lblOPassword;
        private System.Windows.Forms.Label lblCPassword;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.TextBox txtLoginID;
        private System.Windows.Forms.TextBox txtOPassword;
        private System.Windows.Forms.TextBox txtNPassword;
        private System.Windows.Forms.TextBox txtCPassword;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
    }
}