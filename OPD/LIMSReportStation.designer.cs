namespace OPD
{
    partial class LIMSReportStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LIMSReportStation));
            this.dgRptDelivery = new System.Windows.Forms.DataGridView();
            this.SelectAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LabID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Indoor_out = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TestID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TestName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookingID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispatchOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProvisionRpt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReferenceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.miPrintAll = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbPatientInfo = new System.Windows.Forms.GroupBox();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtPName = new System.Windows.Forms.TextBox();
            this.txtPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.lblHName = new System.Windows.Forms.Label();
            this.lblHPRNo = new System.Windows.Forms.Label();
            this.lblHAge = new System.Windows.Forms.Label();
            this.lblHGender = new System.Windows.Forms.Label();
            this.btnReport = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblRecordCount = new System.Windows.Forms.Label();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.pnlDescription = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgRptDelivery)).BeginInit();
            this.cmsPrint.SuspendLayout();
            this.gbPatientInfo.SuspendLayout();
            this.pnlDescription.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgRptDelivery
            // 
            this.dgRptDelivery.AllowUserToAddRows = false;
            this.dgRptDelivery.AllowUserToDeleteRows = false;
            this.dgRptDelivery.AllowUserToOrderColumns = true;
            this.dgRptDelivery.AllowUserToResizeColumns = false;
            this.dgRptDelivery.AllowUserToResizeRows = false;
            this.dgRptDelivery.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRptDelivery.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgRptDelivery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgRptDelivery.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelectAll,
            this.SNo,
            this.PRNo,
            this.LabID,
            this.PatientName,
            this.Indoor_out,
            this.TestID,
            this.ProcessID,
            this.TestName,
            this.BookingID,
            this.PRID,
            this.BookedOn,
            this.DeliveryOn,
            this.DispatchOn,
            this.ProvisionRpt,
            this.ReferenceNo,
            this.Balance});
            this.dgRptDelivery.ContextMenuStrip = this.cmsPrint;
            this.dgRptDelivery.Location = new System.Drawing.Point(12, 133);
            this.dgRptDelivery.MultiSelect = false;
            this.dgRptDelivery.Name = "dgRptDelivery";
            this.dgRptDelivery.RowHeadersWidth = 20;
            this.dgRptDelivery.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgRptDelivery.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRptDelivery.Size = new System.Drawing.Size(990, 540);
            this.dgRptDelivery.TabIndex = 4;
            this.dgRptDelivery.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgRptDelivery_ColumnHeaderMouseClick);
            this.dgRptDelivery.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgRptDelivery_MouseDown);
            // 
            // SelectAll
            // 
            this.SelectAll.DataPropertyName = "sel";
            this.SelectAll.FalseValue = "0";
            this.SelectAll.FillWeight = 5F;
            this.SelectAll.HeaderText = "Print";
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.TrueValue = "1";
            // 
            // SNo
            // 
            this.SNo.FillWeight = 5F;
            this.SNo.HeaderText = "S#";
            this.SNo.Name = "SNo";
            this.SNo.ReadOnly = true;
            // 
            // PRNo
            // 
            this.PRNo.DataPropertyName = "PRNo";
            this.PRNo.HeaderText = "PR No. : ";
            this.PRNo.Name = "PRNo";
            this.PRNo.ReadOnly = true;
            this.PRNo.Visible = false;
            // 
            // LabID
            // 
            this.LabID.DataPropertyName = "Labid";
            this.LabID.FillWeight = 15F;
            this.LabID.HeaderText = "Lab ID";
            this.LabID.Name = "LabID";
            this.LabID.ReadOnly = true;
            // 
            // PatientName
            // 
            this.PatientName.DataPropertyName = "patientname";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PatientName.DefaultCellStyle = dataGridViewCellStyle2;
            this.PatientName.HeaderText = "Patient Name";
            this.PatientName.Name = "PatientName";
            this.PatientName.ReadOnly = true;
            this.PatientName.Visible = false;
            // 
            // Indoor_out
            // 
            this.Indoor_out.DataPropertyName = "Ind_Out";
            this.Indoor_out.HeaderText = "Type";
            this.Indoor_out.Name = "Indoor_out";
            this.Indoor_out.ReadOnly = true;
            this.Indoor_out.Visible = false;
            // 
            // TestID
            // 
            this.TestID.DataPropertyName = "TestID";
            this.TestID.HeaderText = "TestID";
            this.TestID.Name = "TestID";
            this.TestID.ReadOnly = true;
            this.TestID.Visible = false;
            // 
            // ProcessID
            // 
            this.ProcessID.DataPropertyName = "ProcessID";
            this.ProcessID.HeaderText = "ProcessId";
            this.ProcessID.Name = "ProcessID";
            this.ProcessID.ReadOnly = true;
            this.ProcessID.Visible = false;
            // 
            // TestName
            // 
            this.TestName.DataPropertyName = "test_name";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TestName.DefaultCellStyle = dataGridViewCellStyle3;
            this.TestName.FillWeight = 40F;
            this.TestName.HeaderText = "Investigation Name";
            this.TestName.Name = "TestName";
            this.TestName.ReadOnly = true;
            // 
            // BookingID
            // 
            this.BookingID.DataPropertyName = "BookingID";
            this.BookingID.HeaderText = "BookingID";
            this.BookingID.Name = "BookingID";
            this.BookingID.ReadOnly = true;
            this.BookingID.Visible = false;
            // 
            // PRID
            // 
            this.PRID.DataPropertyName = "PRid";
            this.PRID.HeaderText = "PRID";
            this.PRID.Name = "PRID";
            this.PRID.ReadOnly = true;
            this.PRID.Visible = false;
            // 
            // BookedOn
            // 
            this.BookedOn.DataPropertyName = "bookedon";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BookedOn.DefaultCellStyle = dataGridViewCellStyle4;
            this.BookedOn.FillWeight = 15F;
            this.BookedOn.HeaderText = "Booking Time";
            this.BookedOn.Name = "BookedOn";
            this.BookedOn.ReadOnly = true;
            // 
            // DeliveryOn
            // 
            this.DeliveryOn.DataPropertyName = "deliveryon";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryOn.DefaultCellStyle = dataGridViewCellStyle5;
            this.DeliveryOn.FillWeight = 15F;
            this.DeliveryOn.HeaderText = "Reporting Time";
            this.DeliveryOn.Name = "DeliveryOn";
            this.DeliveryOn.ReadOnly = true;
            // 
            // DispatchOn
            // 
            this.DispatchOn.DataPropertyName = "dispatchon";
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DispatchOn.DefaultCellStyle = dataGridViewCellStyle6;
            this.DispatchOn.FillWeight = 15F;
            this.DispatchOn.HeaderText = "Dispatch Time";
            this.DispatchOn.Name = "DispatchOn";
            this.DispatchOn.ReadOnly = true;
            // 
            // ProvisionRpt
            // 
            this.ProvisionRpt.DataPropertyName = "ProvisionRpt";
            this.ProvisionRpt.HeaderText = "ProvisionRpt";
            this.ProvisionRpt.Name = "ProvisionRpt";
            this.ProvisionRpt.ReadOnly = true;
            this.ProvisionRpt.Visible = false;
            // 
            // ReferenceNo
            // 
            this.ReferenceNo.DataPropertyName = "ReferenceNo";
            this.ReferenceNo.HeaderText = "ReferenceNo";
            this.ReferenceNo.Name = "ReferenceNo";
            this.ReferenceNo.ReadOnly = true;
            this.ReferenceNo.Visible = false;
            // 
            // Balance
            // 
            this.Balance.DataPropertyName = "balance";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Balance.DefaultCellStyle = dataGridViewCellStyle7;
            this.Balance.HeaderText = "Balance";
            this.Balance.Name = "Balance";
            this.Balance.ReadOnly = true;
            this.Balance.Visible = false;
            // 
            // cmsPrint
            // 
            this.cmsPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPrint,
            this.miPrintAll});
            this.cmsPrint.Name = "cmsPrint";
            this.cmsPrint.Size = new System.Drawing.Size(117, 48);
            // 
            // miPrint
            // 
            this.miPrint.Name = "miPrint";
            this.miPrint.Size = new System.Drawing.Size(116, 22);
            this.miPrint.Text = "Print";
            this.miPrint.Click += new System.EventHandler(this.miPrint_Click);
            // 
            // miPrintAll
            // 
            this.miPrintAll.Name = "miPrintAll";
            this.miPrintAll.Size = new System.Drawing.Size(116, 22);
            this.miPrintAll.Text = "Print All";
            this.miPrintAll.Click += new System.EventHandler(this.miPrintAll_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(914, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 39);
            this.btnClose.TabIndex = 2;
            this.btnClose.Tag = "no";
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbPatientInfo
            // 
            this.gbPatientInfo.Controls.Add(this.txtGender);
            this.gbPatientInfo.Controls.Add(this.txtPName);
            this.gbPatientInfo.Controls.Add(this.txtPRNo);
            this.gbPatientInfo.Controls.Add(this.txtAge);
            this.gbPatientInfo.Controls.Add(this.lblHName);
            this.gbPatientInfo.Controls.Add(this.lblHPRNo);
            this.gbPatientInfo.Controls.Add(this.lblHAge);
            this.gbPatientInfo.Controls.Add(this.lblHGender);
            this.gbPatientInfo.Location = new System.Drawing.Point(12, 46);
            this.gbPatientInfo.Name = "gbPatientInfo";
            this.gbPatientInfo.Size = new System.Drawing.Size(990, 51);
            this.gbPatientInfo.TabIndex = 12;
            this.gbPatientInfo.TabStop = false;
            this.gbPatientInfo.Text = "Patient Info";
            // 
            // txtGender
            // 
            this.txtGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGender.Location = new System.Drawing.Point(601, 19);
            this.txtGender.MaxLength = 3;
            this.txtGender.Name = "txtGender";
            this.txtGender.ReadOnly = true;
            this.txtGender.Size = new System.Drawing.Size(61, 20);
            this.txtGender.TabIndex = 198;
            // 
            // txtPName
            // 
            this.txtPName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPName.Location = new System.Drawing.Point(384, 19);
            this.txtPName.MaxLength = 50;
            this.txtPName.Name = "txtPName";
            this.txtPName.ReadOnly = true;
            this.txtPName.Size = new System.Drawing.Size(145, 20);
            this.txtPName.TabIndex = 192;
            // 
            // txtPRNo
            // 
            this.txtPRNo.BackColor = System.Drawing.Color.White;
            this.txtPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRNo.Location = new System.Drawing.Point(229, 19);
            this.txtPRNo.Name = "txtPRNo";
            this.txtPRNo.ReadOnly = true;
            this.txtPRNo.Size = new System.Drawing.Size(73, 20);
            this.txtPRNo.TabIndex = 191;
            this.txtPRNo.TabStop = false;
            // 
            // txtAge
            // 
            this.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAge.Location = new System.Drawing.Point(718, 19);
            this.txtAge.MaxLength = 3;
            this.txtAge.Name = "txtAge";
            this.txtAge.ReadOnly = true;
            this.txtAge.Size = new System.Drawing.Size(38, 20);
            this.txtAge.TabIndex = 193;
            // 
            // lblHName
            // 
            this.lblHName.AutoSize = true;
            this.lblHName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHName.Location = new System.Drawing.Point(337, 21);
            this.lblHName.Name = "lblHName";
            this.lblHName.Size = new System.Drawing.Size(41, 13);
            this.lblHName.TabIndex = 196;
            this.lblHName.Text = "Name :";
            // 
            // lblHPRNo
            // 
            this.lblHPRNo.AutoSize = true;
            this.lblHPRNo.Location = new System.Drawing.Point(150, 21);
            this.lblHPRNo.Name = "lblHPRNo";
            this.lblHPRNo.Size = new System.Drawing.Size(53, 13);
            this.lblHPRNo.TabIndex = 197;
            this.lblHPRNo.Tag = "";
            this.lblHPRNo.Text = "MR No. : ";
            // 
            // lblHAge
            // 
            this.lblHAge.AutoSize = true;
            this.lblHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAge.Location = new System.Drawing.Point(680, 21);
            this.lblHAge.Name = "lblHAge";
            this.lblHAge.Size = new System.Drawing.Size(32, 13);
            this.lblHAge.TabIndex = 195;
            this.lblHAge.Text = "Age :";
            // 
            // lblHGender
            // 
            this.lblHGender.AutoSize = true;
            this.lblHGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHGender.Location = new System.Drawing.Point(547, 21);
            this.lblHGender.Name = "lblHGender";
            this.lblHGender.Size = new System.Drawing.Size(48, 13);
            this.lblHGender.TabIndex = 194;
            this.lblHGender.Text = "Gender :";
            // 
            // btnReport
            // 
            this.btnReport.Image = ((System.Drawing.Image)(resources.GetObject("btnReport.Image")));
            this.btnReport.Location = new System.Drawing.Point(824, 3);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(84, 39);
            this.btnReport.TabIndex = 22;
            this.btnReport.Tag = "no";
            this.btnReport.Text = "View";
            this.btnReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 180000;
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.AutoSize = true;
            this.lblRecordCount.Location = new System.Drawing.Point(107, 4);
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(119, 13);
            this.lblRecordCount.TabIndex = 19;
            this.lblRecordCount.Tag = "display";
            this.lblRecordCount.Text = "Test / Report :  (00000)";
            this.lblRecordCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(18, 2);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(71, 17);
            this.chkAll.TabIndex = 20;
            this.chkAll.Text = "Check All";
            this.chkAll.UseVisualStyleBackColor = true;
            // 
            // pnlDescription
            // 
            this.pnlDescription.Controls.Add(this.label1);
            this.pnlDescription.Controls.Add(this.label2);
            this.pnlDescription.Controls.Add(this.lblRecordCount);
            this.pnlDescription.Controls.Add(this.chkAll);
            this.pnlDescription.Location = new System.Drawing.Point(12, 103);
            this.pnlDescription.Name = "pnlDescription";
            this.pnlDescription.Size = new System.Drawing.Size(990, 24);
            this.pnlDescription.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(855, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 21;
            this.label1.Tag = "no";
            this.label1.Text = "In Process";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightSeaGreen;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(935, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 22;
            this.label2.Tag = "no";
            this.label2.Text = "Ready";
            // 
            // LIMSReportStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 685);
            this.Controls.Add(this.dgRptDelivery);
            this.Controls.Add(this.pnlDescription);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.gbPatientInfo);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LIMSReportStation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Labs Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LIMSReportStation_FormClosing);
            this.Load += new System.EventHandler(this.LIMSReportStation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRptDelivery)).EndInit();
            this.cmsPrint.ResumeLayout(false);
            this.gbPatientInfo.ResumeLayout(false);
            this.gbPatientInfo.PerformLayout();
            this.pnlDescription.ResumeLayout(false);
            this.pnlDescription.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgRptDelivery;
		private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox gbPatientInfo;
		private System.Windows.Forms.ContextMenuStrip cmsPrint;
		private System.Windows.Forms.ToolStripMenuItem miPrint;
        private System.Windows.Forms.ToolStripMenuItem miPrintAll;
        private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label lblRecordCount;
		private System.Windows.Forms.CheckBox chkAll;
		private System.Windows.Forms.Panel pnlDescription;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtPName;
        private System.Windows.Forms.MaskedTextBox txtPRNo;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label lblHName;
        private System.Windows.Forms.Label lblHPRNo;
        private System.Windows.Forms.Label lblHAge;
        private System.Windows.Forms.Label lblHGender;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn LabID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Indoor_out;
        private System.Windows.Forms.DataGridViewTextBoxColumn TestID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TestName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookingID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispatchOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProvisionRpt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReferenceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Balance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;


	}
}