using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmPatientNotes : Form
    {
        private string _PatientID = "";
     
        public string PatientID
        {
            set { _PatientID = value; }
        }
        public frmPatientNotes()
        {
            InitializeComponent();
        }

        private void frmPatientNotes_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);
            pnlPatientNotes.Visible = true;
            pnlVoiceNotes.Visible = false;
            rbText.Checked = true;
            FillTextNotes();
            FillVoiceNotes();
            this.Top = 275;
            this.Left = 150;

            objComp = null;
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FillTextNotes()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPD objOPD= new clsBLOPD(objConnection);

            try
            {
                objOPD.PatientID= _PatientID;
                objConnection.Connection_Open();
                DataView dv = objOPD.GetAll(20);
                dgTextNotes.DataSource = dv;
                objConnection.Connection_Close();
            }
            catch { }
            finally
            {
                objOPD= null;
                objConnection = null;
            }
        }

        private void FillVoiceNotes()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLVoiceNotes objVoiceNotes = new clsBLVoiceNotes(objConnection);

            try
            {
                objVoiceNotes.PatientID = _PatientID;
                objConnection.Connection_Open();
                DataView dv = objVoiceNotes.GetAll(1);
                dgVoiceNotes.DataSource = dv;
                objConnection.Connection_Close();
            }
            catch { }
            finally
            {
                objVoiceNotes= null;
                objConnection = null;
            }
        }

        private void rbText_CheckedChanged(object sender, EventArgs e)
        {
            if (rbText.Checked)
            {
                FillTextNotes();
                pnlPatientNotes.Visible = true;
                pnlVoiceNotes.Visible = false;
            }
            else if (rbVoice.Checked)
            {
                pnlPatientNotes.Visible = false;
                pnlVoiceNotes.Visible = true ;
            }

        }

        private void dgVoiceNotes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string Path = Application.StartupPath + "\\" + dgVoiceNotes.Rows[e.RowIndex].Cells["Path"].Value.ToString();
                if (System.IO.File.Exists(Path))
                {
                    System.Diagnostics.Process objPro = new System.Diagnostics.Process();
                    objPro.StartInfo.FileName = Path;
                    objPro.Start();
                    //objPro.WaitForExit();
                }
                else
                {
                    MessageBox.Show("File Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}