using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmAddFromList : Form
    {
        frmMedicineSelection objMedSelect = null;

        public frmAddFromList()
        {
            InitializeComponent();
        }

        public frmAddFromList(frmMedicineSelection frm)
        {
            InitializeComponent();
            objMedSelect = frm;
        }

        private void frmAddFromList_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();

            objConnection.Connection_Open();
            FillAllMed(objConnection);
            FillSelectMed(objConnection);


            if (clsSharedVariables.Language.Equals("Eng"))
            {
                dgSelectedMed.Columns["SelectEngDosage"].Visible = true;
                dgSelectedMed.Columns["SelectedMedDosage"].Visible = false;
                FillCboEngDosage(objConnection);
                button1.Visible = false;
                pnlEngDosage.Visible = true;
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                dgSelectedMed.Columns["SelectEngDosage"].Visible = false;
                dgSelectedMed.Columns["SelectedMedDosage"].Visible = true;
                button1.Visible = true;
                pnlEngDosage.Visible = false;
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                dgSelectedMed.Columns["SelectEngDosage"].Visible = false;
                dgSelectedMed.Columns["SelectedMedDosage"].Visible = true;
                button1.Visible = true;
                pnlEngDosage.Visible = false;
            }

            objConnection.Connection_Close();
            objComp.ApplyStyleToControls(this);

            objComp = null;
            objConnection = null;
        }

        private void FillCboEngDosage(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            SComponents objComp = new SComponents();
            DataView dv = new DataView();
            try
            {
                dv = objMed.GetAll(6);
                objComp.FillComboBox(cboDosage, dv, "Acronym", "Dosageid", true);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            //dv.Dispose();
            objMed = null;
            objComp = null;
        }


        private void FillSelectMed(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
            dgSelectedMed.DataSource = objMedicine.GetAll(15).Table; ;
            objMedicine = null;
        }

        private void FillAllMed(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
            dgAllMed.DataSource = objMedicine.GetAll(8);
            objMedicine = null;
        }

        private void txtAllMedSerach_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgAllMed.Rows.Count; i++)
            {
                if (dgAllMed.Rows[i].Cells["AllMedName"].Value.ToString().ToUpper().StartsWith(txtAllMedSerach.Text.ToUpper()))
                {
                    dgAllMed.Rows[i].Selected = true;
                    dgAllMed.FirstDisplayedScrollingRowIndex = i;
                    break;
                }
            }
        }

        private void btnSelection_Click(object sender, EventArgs e)
        {
            if (dgAllMed.SelectedRows.Count != 0)
            {
                SelectMedicine(dgAllMed.SelectedRows[0].Index);
            }
        }

        private void SelectMedicine(int RowIdx)
        {
            int Flag = 0;
            DataTable dt = (DataTable)dgSelectedMed.DataSource;

            for (int i = 0; i < dgSelectedMed.Rows.Count; i++)
            {
                if (dgAllMed.Rows[RowIdx].Cells["AllMedID"].Value.ToString().Equals(dgSelectedMed.Rows[i].Cells["RefMedID"].Value.ToString()))
                {
                    Flag = 1;
                    MessageBox.Show("Medicine Already Added ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                }
            }

            if (Flag == 0)
            {
                DataRow dr = dt.NewRow();

                //dgSelectedMed.Rows[RowNumber].Cells["SelectMedID"].Value = dgAllMed.Rows[RowIdx].Cells["AllMedID"].Value.ToString();
                //dgSelectedMed.Rows[RowNumber].Cells["SelectMedName"].Value = dgAllMed.Rows[RowIdx].Cells["AllMedName"].Value.ToString();
                //dgSelectedMed.Rows[RowNumber].Cells["RefMedID"].Value = dgAllMed.Rows[RowIdx].Cells["AllMedName"].Value.ToString();
                dr[0] = dgAllMed.Rows[RowIdx].Cells["AllMedID"].Value;
                dr[1] = dgAllMed.Rows[RowIdx].Cells["AllMedName"].Value;
                dr[2] = dgAllMed.Rows[RowIdx].Cells["AllMedID"].Value;
                dr[3] = "I";
                dt.Rows.Add(dr);

                dt.DefaultView.Sort = "Name";
                dgSelectedMed.DataSource = dt;
                formatGrid();
            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);

            try
            {
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                for (int i = 0; i < dgSelectedMed.Rows.Count; i++)
                {
                    if (dgSelectedMed.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                    {
                        Insert(objMedicine, i);
                    }
                    else if (dgSelectedMed.Rows[i].Cells["Mode"].Value.ToString().Equals("U"))
                    {
                        Update(objMedicine, i);
                    }
                }
                objConnection.Transaction_ComRoll();

                FillSelectMed(objConnection);
                FillAllMed(objConnection);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Medicine Insertion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objMedicine = null;
            }
        }

        private void Insert(clsBLMedicine objMedicine, int RowIdx)
        {
            objMedicine.Name = dgSelectedMed.Rows[RowIdx].Cells["SelectMedName"].Value.ToString();
            objMedicine.MedIDRef = dgSelectedMed.Rows[RowIdx].Cells["RefMedID"].Value.ToString();
            if (clsSharedVariables.Language.Equals("Eng"))
            {
                objMedicine.EngDosage= dgSelectedMed.Rows[RowIdx].Cells["SelectEngDosage"].Value.ToString();
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objMedicine.UrduDosage = dgSelectedMed.Rows[RowIdx].Cells["SelectedMedDosage"].Value.ToString();
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                objMedicine.UrduDosage = dgSelectedMed.Rows[RowIdx].Cells["SelectedMedDosage"].Value.ToString();
            }
            objMedicine.PatientInfo = "";
            objMedicine.Active = "1";
            objMedicine.PersonID = clsSharedVariables.UserID;
            objMedicine.EnteredBy = clsSharedVariables.UserID;
            objMedicine.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

            objMedicine.Insert();
        }

        private void Update(clsBLMedicine objMedicine, int RowIdx)
        {
            objMedicine.MedicineID = dgSelectedMed.Rows[RowIdx].Cells["SelectMedID"].Value.ToString();
            if (clsSharedVariables.Language.Equals("Urdu") || clsSharedVariables.Language.Equals("Farsi"))
            {
                objMedicine.UrduDosage = dgSelectedMed.Rows[RowIdx].Cells["SelectedMedDosage"].Value.ToString();
            }
            else if (clsSharedVariables.Language.Equals("Eng"))
            {
                objMedicine.EngDosage = dgSelectedMed.Rows[RowIdx].Cells["SelectEngDosage"].Value.ToString();
            }

            objMedicine.PersonID = clsSharedVariables.UserID;
            objMedicine.EnteredBy = clsSharedVariables.UserID;
            objMedicine.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

            objMedicine.Update();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();

            objConnection.Connection_Open();

            FillAllMed(objConnection);
            FillSelectMed(objConnection);

            objConnection.Connection_Close();
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            if (dgAllMed.ContextMenuStrip.Items[0].Tag != null)
            {
                SelectMedicine((int)dgAllMed.ContextMenuStrip.Items[0].Tag);
            }
        }

        private void dgAllMed_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgAllMed.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dgAllMed.ContextMenuStrip.Items[0].Enabled = true;
                dgAllMed.ContextMenuStrip.Items[1].Enabled = true;
                dgAllMed.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dgAllMed.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dgAllMed.ContextMenuStrip.Items[0].Enabled = false;
                dgAllMed.ContextMenuStrip.Items[1].Enabled = false;
                dgAllMed.ContextMenuStrip.Items[0].Tag = null;

                //dgSelectedList.ContextMenuStrip = null;
            }

            hi = null;
        }

        private void dgAllMed_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                SelectMedicine(e.RowIndex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgSelectedMed.CurrentRow.Index != -1)
            {
                frmSelectDosage objSelectDosage = new frmSelectDosage(this);
                objSelectDosage.Show();
                this.Hide();
            }
        }

        public void SetDosage(string Dosage)
        {
            if (!Dosage.Equals(""))
            {
                if (dgSelectedMed.CurrentRow != null)
                {
                    dgSelectedMed.CurrentRow.Cells["SelectedMedDosage"].Value = Dosage;
                    if (dgSelectedMed.CurrentRow.Cells["Mode"].Value.ToString().Equals("0"))
                    {
                        dgSelectedMed.CurrentRow.Cells["Mode"].Value = "U";
                    }
                    formatGrid();
                }
            }
        }

        public void formatGrid()
        {
            for (int i = 0; i < dgSelectedMed.Rows.Count; i++)
            {
                if (dgSelectedMed.Rows[i].Cells["Mode"].Value.ToString().Equals("I"))
                {
                    dgSelectedMed.Rows[i].DefaultCellStyle.BackColor = Color.BurlyWood;
                }
                if (dgSelectedMed.Rows[i].Cells["Mode"].Value.ToString().Equals("U"))
                {
                    dgSelectedMed.Rows[i].DefaultCellStyle.BackColor = Color.Cornsilk;
                }
            }
        }

        private void frmAddFromList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objMedSelect != null)
            {
                objMedSelect.Show();
                this.Dispose();
            }
        }

        private void dgSelectedMed_Sorted(object sender, EventArgs e)
        {
            formatGrid();
        }

        private void miMoreInfo_Click(object sender, EventArgs e)
        {
            if (dgAllMed.ContextMenuStrip.Items[0].Tag != null)
            {
                if (!dgAllMed.Rows[Convert.ToInt16(dgAllMed.ContextMenuStrip.Items[0].Tag)].Cells["AllMedID"].Value.ToString().Equals(""))
                {
                    frmDrugInfo objDruginfo = new frmDrugInfo();
                    objDruginfo.MedID = dgAllMed.Rows[Convert.ToInt16(dgAllMed.ContextMenuStrip.Items[0].Tag)].Cells["AllMedID"].Value.ToString();
                    objDruginfo.ShowDialog();
                    objDruginfo.Dispose();
                }
                else
                {
                    MessageBox.Show("More Information for Selected Medicine is not Available", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnEngDosageAdd_Click(object sender, EventArgs e)
        {
            if (dgSelectedMed.CurrentRow.Index != -1)
            {
                if (cboDosage.SelectedIndex!=0)
                {
                    if (dgSelectedMed.CurrentRow != null)
                    {
                        dgSelectedMed.CurrentRow.Cells["SelectEngDosage"].Value = cboDosage.Text ;
                        if (cboEngPeriod.SelectedIndex != 0)
                        {
                            dgSelectedMed.CurrentRow.Cells["SelectEngDosage"].Value += " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                        }
                        if (dgSelectedMed.CurrentRow.Cells["Mode"].Value.ToString().Equals("0"))
                        {
                            dgSelectedMed.CurrentRow.Cells["Mode"].Value = "U";
                        }
                        formatGrid();
                    }
                }
            }


            
        }
    }
}
