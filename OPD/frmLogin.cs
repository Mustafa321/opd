using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using OPD_BL;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.IO;

namespace OPD
{
	public partial class frmLogin : Form
	{
		private string KeyVal = "";
		private string LoginIDS = "";
		public frmLogin ( )
		{
			InitializeComponent();
		}
		private void frmLogin_Load ( object sender , EventArgs e )
		{
            var datenow = DateTime.Now.AddYears(5);
          var s=  Encrypt(datenow.ToString());
            SComponents objComp = new SComponents();
			clsBLDBConnection objConnection = new clsBLDBConnection();
			try
			{
              

                objComp.ApplyStyleToControls(this);
                lblVersion.Text = clsSharedVariables.Version;
                {
                       // GetLanguage();
                        GetLoginID();
                        GetLoginIDAutoPRno();
                   
                        objComp = null;
                }
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}

			finally
			{
				objComp = null;
			}
		}
        private void GetallRefernce()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);

            objRef.ReportRefrence = "InstLang";
            objConnection.Connection_Open();

            var n = objRef.GetAll(3);
            for (var i = 0; i < n.Count; i++)
            {
                var Refernce = n.Table.Rows[i]["ReportRefrence"].ToString();
                var Description = n.Table.Rows[i]["Description"].ToString();
                if (Refernce == "InstLang")
                {
                    clsSharedVariables.Language = Description;
                }
                if (Refernce == "AutoPRNO")
                {
                    clsSharedVariables.Language = Description;
                }
                else if (Refernce == "RenameHead")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.RenameHeading = true;
                    }
                    else
                    {
                        clsSharedVariables.RenameHeading = false;
                    }
                }
                else if (Refernce == "Appointment")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.Appointment = true;
                    }
                    else
                    {
                        clsSharedVariables.Appointment = false;
                    }
                }
                else if (Refernce == "WaitingQueue")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.WaitingQueue = true;
                    }
                    else
                    {
                        clsSharedVariables.WaitingQueue = false;
                    }
                }
                else if (Refernce == "Notes")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.Notes = true;
                    }
                    else
                    {
                        clsSharedVariables.Notes = false;
                    }
                }
                else if (Refernce == "Allergies")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.Allergies = true;
                    }
                    else
                    {
                        clsSharedVariables.Allergies = false;
                    }
                }
                else if (Refernce == "VitalSign")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.VitalSign = true;
                    }
                    else
                    {
                        clsSharedVariables.VitalSign = false;
                    }
                }
                else if (Refernce == "Medication")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.Medication = true;
                    }
                    else
                    {
                        clsSharedVariables.Medication = false;
                    }
                }
                else if (Refernce == "PatientDocuments")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.PatientDocuments = true;
                    }
                    else
                    {
                        clsSharedVariables.PatientDocuments = false;
                    }
                }

                else if (Refernce == "AdmissionAndProcedure")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.AdmissionAndProcedure = true;
                    }
                    else
                    {
                        clsSharedVariables.AdmissionAndProcedure = false;
                    }
                }
                else if (Refernce == "PatientVisits")
                {
                    if (Description == "Y")
                    {
                        clsSharedVariables.PatientVisits = true;
                    }
                    else
                    {
                        clsSharedVariables.PatientVisits = false;
                    }
                }
            }
            objConnection.Connection_Close();

            objConnection = null;
            objRef = null;
        }
        private string EncKey(string str)
        {
            string str1 = "";

            for (int i = str.Length - 1; i >= 0; i--)
            {
                str1 = str1 + (char)(str[i] - 10);
            }

            return str1;
        }
        private string DecryptKey(string Info)
        {
            string tmpInfo = "";
            for (int i = Info.Length - 1; i >= 0; i--)
            {
                tmpInfo = tmpInfo + (char)(Info[i] + 10);
            }

            return tmpInfo;
        }
        private int DecryptCode()
        {
            string Info = "";
            string[] InfoArr;

            if (System.IO.File.Exists("License.lic"))
            {
                try
                {
                    KeyVal = GetKey();
                    if (KeyVal.Trim().Equals("") || clsSharedVariables.UserName.Equals("") || clsSharedVariables.LoginID.Equals(""))
                    {
                        frmReg objReg = new frmReg();

                        objReg.ShowDialog();
                        if (objReg.Validation.Equals(""))
                        {
                            objReg.Dispose();
                            this.Close();
                        }
                        else
                        {
                            SaveLoginName(EncKey("Key"), EncKey(objReg.Validation), EncKey(clsSharedVariables.UserName), EncKey(clsSharedVariables.LoginID));
                            KeyVal = objReg.Validation;
                        }
                        objReg.Dispose();
                    }

                    Info = System.IO.File.ReadAllText("License.lic");

                    InfoArr = DecryptKey(Info).Split('\n');
                    if (InfoArr.Length == 5)
                    {
                        if (!InfoArr[2].Contains(GetKey()))
                        {
                            return 1;//change License File
                        }
                        else if (!InfoArr[0].ToUpper().Contains(clsSharedVariables.UserName.ToUpper()))
                        {
                            return 4;//change User Name 
                        }
                        else if (!InfoArr[1].ToUpper().Contains(clsSharedVariables.LoginID.ToUpper()))
                        {
                            return 5;//change Login ID
                        }
                        else if (DateTime.Compare(DateTime.ParseExact(InfoArr[4].Substring(9, 10),"dd/MM/yyyy",null), DateTime.Now) == -1 )
                        {
                            return 2;//License is Expired
                        }
                        else
                        {
                            LoginIDS = InfoArr[1];
                            return 3;//Validate Every Thing
                        }
                    }
                    else
                    {
                        return 1;//change License File
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                    return 1;
                }
            }
            else
            {
                return 0;//License file deleted
            }
        }
        private string GetKey()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            string KeyVal = "";
            try
            {
                objRef.ReportRefrence = EncKey("Key");

                objConnection.Connection_Open();
                DataView dv = objRef.GetAll(2);
                KeyVal = dv.Table.Rows[0]["Description"].ToString().Trim();
                clsSharedVariables.UserName = DecryptKey(dv.Table.Rows[0]["ReportTitle1"].ToString().Trim());
                clsSharedVariables.LoginID = DecryptKey(dv.Table.Rows[0]["ReportTitle2"].ToString().Trim());
                objConnection.Connection_Close();

                objConnection = null;
                objRef = null;
                return DecryptKey(KeyVal);
            }
            catch (Exception exc)
            {
                //MessageBox.Show(exc.Message);
                return "";
            }
        }
        
        private void GetLanguage()
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLReferences objRef = new clsBLReferences(objConnection);

			objRef.ReportRefrence = "InstLang";


			objConnection.Connection_Open();

			clsSharedVariables.Language = objRef.GetAll(2).Table.Rows[0]["Description"].ToString();
			objConnection.Connection_Close();

			objConnection = null;
			objRef = null;
		}

		private string GetUpdatePK( )
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLReferences objRef = new clsBLReferences(objConnection);

			objRef.ReportRefrence = "SetUpdVal";


			objConnection.Connection_Open();
			DataView  dv=objRef.GetAll(1);
			objConnection.Connection_Close();
			
			objConnection = null;
			objRef = null;

			if (dv.Table.Rows.Count != 0)
			{
				return 	dv.Table.Rows[0]["Description"].ToString();
			}
			return "-1";
		}

		private void llblForget_LinkClicked ( object sender , LinkLabelLinkClickedEventArgs e )
		{
			//lblHint.Visible = true;
			//txtHint.Visible = true;
			GetHintValue();
		}

		private void btnCancel_Click ( object sender , EventArgs e )
		{
			this.Close();
		}

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            var IsFirstLogin = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["IsFirstLogin"].ToString());
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
            DataView dvPersonal;
            if (!txtloginID.Text.Equals(""))
            {
                objPersonal.LoginID = txtloginID.Text.Trim();
                objConnection.Connection_Open();
                dvPersonal = objPersonal.GetAll(3);
                objConnection.Connection_Close();

                if (dvPersonal.Count == 0)
                {
                    MessageBox.Show("User not Exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    if (dvPersonal.Table.Rows[0]["Active"].ToString().Equals("1"))
                    {
                        if (txtPasword.Text.ToString().Equals(dvPersonal.Table.Rows[0]["Pasword"].ToString()))
                        {
                            if (chbRemember.Checked)
                            {
                                SaveLoginName("LoginID", txtloginID.Text.Trim(), txtPasword.Text, "");
                            }
                            else
                            {
                                SaveLoginName("LoginID", txtloginID.Text.Trim(), "", "");
                            }
                            if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("O"))
                            {

                                clsSharedVariables.ExpireDate = dvPersonal.Table.Rows[0]["Expire"].ToString();
                                clsSharedVariables.UserName = dvPersonal.Table.Rows[0]["Title"].ToString() + " " + dvPersonal.Table.Rows[0]["Name"].ToString();
                                clsSharedVariables.UserID = dvPersonal.Table.Rows[0]["PersonID"].ToString();
                                clsSharedVariables.RefPersonID = dvPersonal.Table.Rows[0]["RefPersonID"].ToString();
                                clsSharedVariables.LoginID = txtloginID.Text.Trim();
                                clsSharedVariables.Payment = System.Configuration.ConfigurationSettings.AppSettings["Fees"].ToString();
                                GetSMTPSetting();
                                GetallRefernce();
                                if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("C"))
                                {
                                    frmOPD objOPD = new frmOPD(this);
                                    if (!chbRemember.Checked)
                                    {
                                        txtPasword.Text = "";
                                    }
                                    //objOPD.MdiParent = this;
                                    this.Hide();
                                    objOPD.Show();
                                }
                                else if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("O"))
                                {
                                    frmPatientReg objPatientReg = new frmPatientReg();
                                    txtPasword.Text = "";
                                    objPatientReg.Show(this);
                                }


                            }
                            else
                            {
                                var ExpireDate = dvPersonal.Table.Rows[0]["Expire"].ToString();
                                var getDecriptedDate = Decrypt(ExpireDate);
                                var exDate = Convert.ToDateTime(getDecriptedDate);
                                if (DateTime.Now.Date != exDate.Date)
                                {
                                    clsSharedVariables.ExpireDate = dvPersonal.Table.Rows[0]["Expire"].ToString();
                                    clsSharedVariables.UserName = dvPersonal.Table.Rows[0]["Title"].ToString() + " " + dvPersonal.Table.Rows[0]["Name"].ToString();
                                    clsSharedVariables.UserID = dvPersonal.Table.Rows[0]["PersonID"].ToString();
                                    clsSharedVariables.RefPersonID = dvPersonal.Table.Rows[0]["RefPersonID"].ToString();
                                    clsSharedVariables.LoginID = txtloginID.Text.Trim();
                                    clsSharedVariables.Payment = System.Configuration.ConfigurationSettings.AppSettings["Fees"].ToString();
                                    GetSMTPSetting();
                                    GetallRefernce();
                                    if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("C"))
                                    {
                                        frmOPD objOPD = new frmOPD(this);
                                        if (!chbRemember.Checked)
                                        {
                                            txtPasword.Text = "";
                                        }
                                        //objOPD.MdiParent = this;
                                        this.Hide();
                                        objOPD.Show();
                                    }
                                    else if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("O"))
                                    {
                                        frmPatientReg objPatientReg = new frmPatientReg();
                                        txtPasword.Text = "";
                                        objPatientReg.Show(this);
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("your Licence is Expired Please Contact To the Administrator", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                            }

                            //this.Close();

                        }
                        else
                        {
                            MessageBox.Show("Invalid Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Inactive User, Please contact your Admin", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }
            else
            {
                MessageBox.Show("Please Enter LoginID", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            objConnection = null;
            objPersonal = null;
            dvPersonal = null;
        }
        private clsBLPersonal SetBLValues(clsBLPersonal objPersonal,Doctor dValues)
        {
            objPersonal.PersonID = dValues.Id.ToString();
            objPersonal.Title = dValues.Title;
            objPersonal.name = clsSharedVariables.InItCaps(dValues.DoctorName);

            objPersonal.Gender = dValues.Gender;

            objPersonal.MaritalStatus = dValues.MartialStatus;

            objPersonal.DOB = dValues.DOB.Date.ToString("dd/MM/yyyy");
            objPersonal.PhoneNo = dValues.WhatsappNumber;
            objPersonal.CellNo = dValues.WhatsappNumber;

            objPersonal.NIC = dValues.CNIC;

            objPersonal.Address = clsSharedVariables.InItCaps(dValues.Address);

            objPersonal.BloodGroup = dValues.BloodGroup;

            objPersonal.Designation = clsSharedVariables.InItCaps(dValues.Designation);

            objPersonal.LoginID = dValues.UserName;
            objPersonal.Pasword = dValues.Password;
            objPersonal.RCPasword = dValues.Password;
            objPersonal.Hint = "";
            if(dValues.Active==true)
            {
                objPersonal.Active = "1";
            }
            else
            {
                objPersonal.Active = "0";
            }
           

            objPersonal.RegistrationCode = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPersonal.EnteredBy = clsSharedVariables.UserID;
            objPersonal.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPersonal.ClientID = clsSharedVariables.ClientID;
            objPersonal.Type = "C";
            objPersonal.Email = dValues.Email;
            objPersonal.maxSlot = "500";
            objPersonal.Charges = dValues.Charges.ToString();
            objPersonal.RevisitCharges = dValues.RevisitCharges.ToString();
            objPersonal.RevisitDays = dValues.RevisitDays.ToString();
            objPersonal.Expire = Encrypt(dValues.LicenceExpireDate.ToString());
            return objPersonal;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "abc123";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "abc123";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        //public async Task<Doctor> GetDoctorLogin(string username,string password)
        //{
        //    var n = new Doctor();
        //    try
        //    {

        //       var BaseURL= System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();
        //        var value = new Doctor() { UserName = username, Password = password };
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseURL);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            // New code:
        //            HttpResponseMessage response = await client.PostAsJsonAsync("api/Doctor/DoctorLogin", value);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                n = await response.Content.ReadAsAsync<Doctor>();

                       
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        n.Id = 0;
        //    }
        //    return n;
        //}
        private void GetHintValue ( )
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
			DataView dvPersonal;

			if (!txtloginID.Text.Equals(""))
			{
				objPersonal.LoginID = txtloginID.Text.Trim();
				objConnection.Connection_Open();
				dvPersonal = objPersonal.GetAll(3);
				objConnection.Connection_Close();

				if (dvPersonal.Count == 0)
				{
					MessageBox.Show("User not Exist" , "Information" , MessageBoxButtons.OK , MessageBoxIcon.Information);

					return;
				}
				else
				{
					//if (txtHint.Text.ToString().ToUpper().Equals(dvPersonal.Table.Rows[0]["Hint"].ToString().ToUpper()))
					{
						MessageBox.Show(dvPersonal.Table.Rows[0]["Hint"].ToString());
					}
					//    else
					//    {
					//        MessageBox.Show("Invalid Hint", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					//    }
				}
			}
			else
			{
				MessageBox.Show("Please Enter LoginID " , "Information" , MessageBoxButtons.OK , MessageBoxIcon.Information);
			}
			objConnection = null;
			objPersonal = null;
			dvPersonal = null;
		}

		private void GetLoginIDAutoPRno ( )
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLReferences objRef = new clsBLReferences(objConnection);
			DataView dvPersonal;

			//objRef.Description= clsSharedVariables.UserID;
            try
            {
                objConnection.Connection_Open();

                objRef.ReportRefrence = "LoginID";
                dvPersonal = objRef.GetAll(2);

                if (dvPersonal.Count != 0)
                {
                    txtloginID.Text = dvPersonal.Table.Rows[0]["Description"].ToString();
                    if (!dvPersonal.Table.Rows[0]["ReportTitle1"].ToString().Equals(""))
                    {
                        txtPasword.Text = dvPersonal.Table.Rows[0]["ReportTitle1"].ToString();
                        chbRemember.Checked = true;
                    }
                }

                objRef.ReportRefrence = "AutoPRNO";
                dvPersonal = objRef.GetAll(2);

                if (dvPersonal.Count != 0)
                {
                    clsSharedVariables.AutoPRNO = dvPersonal.Table.Rows[0]["Description"].ToString().Equals("Y");
                }

            }
            catch { }
            finally
            {
                objConnection.Connection_Close();
                objConnection = null;
                objRef = null;
            }
			dvPersonal = null;
		}
    

        private void SaveLoginName ( string RptRef , string Description , string ReportTitle1 , string ReportTitle2 )
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLReferences objRef = new clsBLReferences(objConnection);
			try
			{
				//objPersonal.PersonID = clsSharedVariables.UserID  ;
				objRef.ReportRefrence = RptRef;
				objRef.Description = Description;
				objRef.ReportTitle1 = ReportTitle1;
				objRef.ReportTitle2 = ReportTitle2;

				objConnection.Connection_Open();
				objConnection.Transaction_Begin();
				if (objRef.Update())
				{
					objConnection.Transaction_ComRoll();
					objConnection.Connection_Close();
				}
				else
				{
					MessageBox.Show(objRef.ErrorMessage , "Error" , MessageBoxButtons.OK , MessageBoxIcon.Error);
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message , "Error" , MessageBoxButtons.OK , MessageBoxIcon.Error);
			}
			finally
			{
				objRef = null;
				objConnection = null;
			}
		}

		private void txtPasword_KeyPress ( object sender , KeyPressEventArgs e )
		{
			if (e.KeyChar == '\r')
			{
				EventArgs ea = new EventArgs();
				btnLogin_Click(null , ea);

				ea = null;
			}
		}

		private void txtloginID_Validating ( object sender , CancelEventArgs e )
		{
			clsSharedVariables.InItCaps(txtloginID.Text);
		}

		private void lbtnGreen_MouseHover ( object sender , EventArgs e )
		{
			LinkLabel lbtn = (LinkLabel)sender;

			lbtn.LinkColor = Color.LimeGreen;
		}

		private void lbtnBlack_MouseHover ( object sender , EventArgs e )
		{
			LinkLabel lbtn = (LinkLabel)sender;
			lbtn.LinkColor = Color.Black;
		}

		private void lbtnGreen_MouseLeave ( object sender , EventArgs e )
		{
			LinkLabel lbtn = (LinkLabel)sender;
			lbtn.LinkColor = Color.LimeGreen;
		}

		private void lbtnGray_MouseLeave ( object sender , EventArgs e )
		{
			LinkLabel lbtn = (LinkLabel)sender;
			lbtn.LinkColor = Color.Gray;
		}

		private void lbtnAbout_LinkClicked ( object sender , LinkLabelLinkClickedEventArgs e )
		{
			frmAbout objAbout = new frmAbout();
			objAbout.ShowDialog();
			objAbout.Dispose();
		}

		private void lbtnClinicNews_LinkClicked ( object sender , LinkLabelLinkClickedEventArgs e )
		{
			frmNews objNews = new frmNews();
			objNews.ShowDialog();
			objNews.Dispose();
		}

		private void lbtntreesvalley_LinkClicked ( object sender , LinkLabelLinkClickedEventArgs e )
		{
			WebBrowser objWebBrowser = new WebBrowser();
			objWebBrowser.Navigate("http://www.treesvalley.com");
			objWebBrowser.Dispose();
		}

		private void lbtnhaisam_LinkClicked ( object sender , LinkLabelLinkClickedEventArgs e )
		{
			WebBrowser objWebBrowser = new WebBrowser();
			objWebBrowser.Navigate("http://www.haisam.org" , true);
			objWebBrowser.Dispose();
		}

		private void UpdatPKWhims ( )
		{
            //clsBLDBConnection objConnection = new clsBLDBConnection();
            //clsBLOPD objOPD = new clsBLOPD(objConnection);
            //try
            //{
            //    objConnection.Ora_Connection_Open("0");

            //    objConnection.Ora_Transaction_Begin();

            //    objOPD.UpdatePK("ALTER TABLE AC_TCASHRECEIVED drop CONSTRAINT AC_CASHRECEIVED_PK_PAIDNO");
            //    objOPD.UpdatePK("ALTER TABLE AC_CASHRECHISTORY drop CONSTRAINT AC_CASHRECHISTORY");
            //    objOPD.UpdatePK("ALTER TABLE GL_TTRANSACTION drop CONSTRAINT TRANSONO_GL");
            //    objOPD.UpdatePK("ALTER TABLE Pr_Tpatientreg drop CONSTRAINT PRNO");
            //    objOPD.UpdatePK("ALTER TABLE PR_TPATIENTVISITD drop CONSTRAINT PR_TPATIENTVISITD_UNIQUEKEYS");
            //    objOPD.UpdatePK("ALTER TABLE PR_TPATIENTVISITM drop CONSTRAINT PR_TPATIENTVISITM_VISITNO_PK");
            //    objOPD.UpdatePK("ALTER TABLE BL_TBILLMASTER drop CONSTRAINT BILLID");
            //    objOPD.UpdatePK("ALTER TABLE WD_TTRANSFER drop CONSTRAINT TRANSFERID");

				
            //    objConnection.Ora_Transaction_ComRoll();
            //    objConnection.Ora_Connection_Close();

            //    objConnection.Ora_Connection_Open("1");

            //    objConnection.Ora_Transaction_Begin();

            //    objOPD.UpdatePK("ALTER TABLE BB_DONATION drop CONSTRAINT PK_DONATION");
            //    objOPD.UpdatePK("ALTER TABLE LS_TDTRANSACTION drop CONSTRAINT LS_TDTRANSACT_PK_DSERIALNO");
            //    objOPD.UpdatePK("ALTER TABLE LS_TMTRANSACTION drop CONSTRAINT LS_TMTRANSACT_PK11129006952500");
            //    objOPD.UpdatePK("ALTER TABLE LS_TTESTRESULTM drop CONSTRAINT LS_TTESTRESUL_PKRSERIALNO");

            //    objConnection.Ora_Transaction_ComRoll();
            //    objConnection.Ora_Connection_Close();


            //    clsBLReferences objRef = new clsBLReferences(objConnection);

            //    objRef.ReportRefrence = "SetUpdVal";
            //    objRef.Description = "1";	
				
            //    objConnection.Connection_Open();
            //    objConnection.Transaction_Begin();

            //    objRef.Update();

            //    objConnection.Transaction_ComRoll();
            //    objConnection.Connection_Close();

            //    SendMail();

            //    objConnection = null;
            //    objRef = null;
            //}
            //catch { }
		}

		private void SendMail ( )
		{
			frmSendEmail objSendMail = new frmSendEmail();
			MailMessage mail = new MailMessage();
			
			try
			{
				mail.To.Add("zahid@treesvalley.com");
				
				mail.From = new MailAddress("Sarimhaleem@gmail.com");
				mail.Subject = "Update PK in Whims and whims2";
				mail.Body = "Primary keys updated succsessfully";
				
				mail.IsBodyHtml = true;
				SmtpClient smtp = new SmtpClient("smtp.gmail.com" , 587);//587,465
				smtp.UseDefaultCredentials = false;
				smtp.Credentials = new System.Net.NetworkCredential("Sarimhaleem@gmail.com" ,"1234");
				//Or your Smtp Email ID and Password
				//smtp.Timeout = 0;
				smtp.EnableSsl = true;
				smtp.Send(mail);
			}
			catch (Exception ex)
			{
				
			}
			finally
			{
				objSendMail.Dispose();
				mail.Dispose();
			}
		}

        private void GetSMTPSetting()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            DataView dv = new DataView();

            try
            {
                objRef.ReportRefrence = "SetMail";
                objConnection.Connection_Open();
                dv = objRef.GetAll(2);

                if (dv.Table.Rows.Count != 0)
                {
                    clsSharedVariables.SMTPEmail = dv.Table.Rows[0]["Description"].ToString();
                    clsSharedVariables.Password = dv.Table.Rows[0]["ReportTitle1"].ToString();
                    clsSharedVariables.Port = dv.Table.Rows[0]["ReportTitle2"].ToString();
                    clsSharedVariables.SMTP = dv.Table.Rows[0]["ReportTitle3"].ToString();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objRef = null;
                dv.Dispose();
            }
        }

        private void GetLoginID()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            DataView dvPersonal;

            //objRef.Description= clsSharedVariables.UserID;
            objRef.ReportRefrence = "LoginID";

            objConnection.Connection_Open();
            dvPersonal = objRef.GetAll(2);
            

            if (dvPersonal.Count != 0)
            {
                txtloginID.Text = dvPersonal.Table.Rows[0]["Description"].ToString();
                if (!dvPersonal.Table.Rows[0]["ReportTitle1"].ToString().Equals(""))
                {
                    txtPasword.Text = dvPersonal.Table.Rows[0]["ReportTitle1"].ToString();
                    chbRemember.Checked = true;
                }
            }
            objRef.ReportRefrence = "MaxRecTime";

            dvPersonal = objRef.GetAll(2);
            

            if (dvPersonal.Count != 0)
            {
                clsSharedVariables.MaxRecTime= Convert.ToInt16(dvPersonal.Table.Rows[0]["Description"].ToString());
            }
            objConnection.Connection_Close();
            objConnection = null;
            objRef = null;
            dvPersonal = null;
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblVersion_Click(object sender, EventArgs e)
        {

        }
	}
}