using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmICDDiseaseMed : Form
    {
        private string _DiseaseID = "";

        public string DiseaseID
        {
            set { _DiseaseID = value; }
        }

        public frmICDDiseaseMed()
        {
            InitializeComponent();
        }

        private void frmICDDiseaseMed_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            objComp.ApplyStyleToControls(this);
            fillDiseaseMed();

            objComp = null;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fillDiseaseMed()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLDisease objDisease= new clsBLDisease(objConnection);

            try
            {
                objDisease.DiseaseID= _DiseaseID;
                objConnection.Connection_Open();
                DataView dv = objDisease.GetAll(6);
                dgDiseaseMed.DataSource = dv;
                if(dv.Table.Rows.Count>0)
                {
                    txtDiseaseCode.Text = dv.Table.Rows[0]["DiseaseCode"].ToString();
                    txtDiseaseName.Text = dv.Table.Rows[0]["DiseaseName"].ToString();
                }
                SetSNO();
                objConnection.Connection_Close();
            }
            catch(Exception exc) 
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objDisease = null;
                objConnection = null;
            }
        }

        private void SetSNO()
        {
            for (int i = 0; i < dgDiseaseMed.Rows.Count; i++)
            {
                dgDiseaseMed.Rows[i].Cells["SNO"].Value = Convert.ToString(i+1);
            }
        }
    }
}