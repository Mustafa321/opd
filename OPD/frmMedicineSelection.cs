using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmMedicineSelection : Form
    {
        private frmOPD MainOPD = null;
        private string _PatientID = "";
        private string _Morning = "";
        private string _AfterNoon = "";
        private string _Evening = "";
        private string _Night = "";
        private string _DayCount = "";
        private string _Instruction = "";
        private string _Quantity = "";
        public string PatientID
        {
            set { _PatientID = value; }
        }
        public string Morning
        {
            get { return _Morning; }
            set { _Morning = value; }
        }
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public string Afternoon
        {
            get { return _AfterNoon; }
            set { _AfterNoon = value; }
        }
        public string Evening
        {
            get { return _Evening; }
            set { _Evening = value; }
        }
        public string Night
        {
            get { return _Night; }
            set { _Night = value; }
        }
        public string Instruction
        {
            get { return _Instruction; }
            set { _Instruction = value; }
        }
        public string DaysCount
        {
            get { return _DayCount; }
            set { _DayCount = value; }
        }

        public frmMedicineSelection()
        {
            InitializeComponent();
        }

        public frmMedicineSelection(frmOPD Frm)
        {
            InitializeComponent();
            this.MainOPD = Frm;
        }

        private void frmMedicineSelection_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();

            objComp.ApplyStyleToControls(this);

            if (clsSharedVariables.Language.Equals("Eng"))
            {
                cboDosage.Visible = true;
                pnlEngDosage.Visible = true;
                txtDosage.Visible = false;
                btnSelectDosage.Enabled = false;

                dgList.Columns["Dosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
                dgList.Columns["Dosage"].DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                cboDosage.Text = "Select";
                nudEngPeriodCount.Value = 1;
                cboEngPeriod.Text = "Days";
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                cboDosage.Visible = false;
                pnlEngDosage.Visible = false;
                txtDosage.Visible = true;
                btnSelectDosage.Enabled = true;

                dgList.Columns["Dosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgList.Columns["Dosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                cboDosage.Visible = false;
                pnlEngDosage.Visible = false;
                txtDosage.Visible = true;
                btnSelectDosage.Enabled = true;

                dgList.Columns["Dosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgList.Columns["Dosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                dgSelectedList.Columns["MedicineDosage"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
            objConnection.Connection_Open();
            FillGridView(objConnection);
            FillCboEngDosage(objConnection);
            objConnection.Connection_Close();

            dgList.ClearSelection();

            txtSearchMedicine.Tag = "S";
            this.txtName.Font = new Font("Verdana", float.Parse("12"), GraphicsUnit.Point);
            cboDosage.Font = new Font("Verdana", float.Parse("12"), GraphicsUnit.Point);
            nudEngPeriodCount.Font = new Font("Verdana", float.Parse("12"), GraphicsUnit.Point);
            cboEngPeriod.Font = new Font("Verdana", float.Parse("12"), GraphicsUnit.Point);

            objComp = null;
            //rmilab@hotmail.com
            objConnection = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            dgSelectedList.Tag = "";
            this.Close();
        }

        private void FillGridView(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
            DataView dv = new DataView();

            objMedicine.PersonID = clsSharedVariables.UserID;
            if (clsSharedVariables.Language.Equals("Eng"))
            {
                dv = objMedicine.GetAll(4);
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                dv = objMedicine.GetAll(2);
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                dv = objMedicine.GetAll(7);
            }
            dgList.DataSource = dv;
            AddInfoImageInDG();

            objMedicine = null;
        }

        private void AddInfoImageInDG()
        {
            for (int i = 0; i < dgList.Rows.Count; i++)
            {
                if (dgList.Rows[i].Cells["MedRefID"].Value.ToString().Equals("") || dgList.Rows[i].Cells["MedRefID"].Value.ToString().Equals("0") || dgList.Rows[i].Cells["MedRefID"].Value == null)
                {
                    //Bitmap bmp = Bitmap.FromFile("c:\\NA1.jpeg");
                    dgList.Rows[i].Cells["InfoImage"].Value = Bitmap.FromFile(Application.StartupPath + "\\Icons\\NA1.jpg");
                }
                else
                {
                    //Bitmap bmp = Bitmap.FromFile("c:\\IA1.jpeg");
                    dgList.Rows[i].Cells["InfoImage"].Value = Bitmap.FromFile(Application.StartupPath + "\\Icons\\IA1.jpg");
                }
            }
        }

        private void btnSelectDosage_Click(object sender, EventArgs e)
        {
            frmSelectDosage objSelectDosage = new frmSelectDosage(this);
            objSelectDosage.Show();
            this.Hide();

            //objSelectDosage.Dispose();
        }

        public void GetDosage(string Dosage,string chbMorning,string chbAfterNoon,string chbEvening, string chbNight,string chbDay,string chbInstruction,string cboQunatity)
        {
            if (!Dosage.Equals(""))
            {
                txtDosage.Text = Dosage;
                this.Morning = chbMorning;
                this.Afternoon = chbAfterNoon;
                this.Evening = chbEvening;
                this.Night = chbNight;
                this.DaysCount = chbDay;
                this.Instruction = chbInstruction;
                this.Quantity = cboQunatity;
                txtDosage.Tag = "1";
            }
            else
            {
                txtDosage.Tag = "0";
                this.Morning = "";
                this.Afternoon = "";
                this.Evening = "";
                this.Night = "";
                this.DaysCount = "";
                this.Instruction = "";
                this.Quantity = "0";

            }
        }

        private clsBLMedicine SetBLValues(clsBLMedicine objMedicine)
        {
            objMedicine.Name = txtName.Text.Trim();
            if (clsSharedVariables.Language.Equals("Eng"))
            {
                if (!cboDosage.Text.Equals("Select"))
                {
                    objMedicine.EngDosage = cboDosage.Text.Trim();
                    if (cboEngPeriod.SelectedIndex != 0)
                    {
                        objMedicine.EngDosage += " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                    }
                }
            }
            else if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objMedicine.UrduDosage = txtDosage.Text.Trim();//.Replace("\\", "\\\\");
            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                objMedicine.FarsiDosage = txtDosage.Text.Trim();//.Replace("\\", "\\\\");
            }

            objMedicine.PatientInfo = "";
            objMedicine.Active = "1";
            objMedicine.Morning = this.Morning;
            objMedicine.Evening = this.Evening;
            objMedicine.Afternoon = this.Afternoon;
            objMedicine.Night = this.Night;
            objMedicine.DaysCount = this.DaysCount;
            objMedicine.Instruction = this.Instruction;
            objMedicine.Quantity = this.Quantity;
            objMedicine.PersonID = clsSharedVariables.UserID;
            objMedicine.EnteredBy = clsSharedVariables.UserID;
            objMedicine.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

            return objMedicine;
        }

        private void Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);

            try
            {
                objMedicine = SetBLValues(objMedicine);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objMedicine.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    MessageBox.Show("\"" + txtName.Text + "\" Inserted Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearField();
                }
                else
                {
                    MessageBox.Show(objMedicine.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objMedicine = null;
                objConnection = null;
            }
        }

        private void UpdateData()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
            try
            {
                objMedicine.MedicineID = txtName.Tag.ToString();
                objMedicine = SetBLValues(objMedicine);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objMedicine.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Medicine updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objMedicine.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objMedicine = null;
                objConnection = null;
            }
        }

        private void ClearField()
        {
            txtDosage.Text = "";
            txtName.Text = "";
            Morning = "";
            Afternoon = "";
            Evening = "";
            Night = "";
            DaysCount = "";
            Instruction = "";
            btnSave.Tag = "&Save";
            btnSave.Text = "&Save";
            dgList.ClearSelection();
            cboDosage.Text = "Select";
            nudEngPeriodCount.Value = 1;
            cboEngPeriod.SelectedIndex = 0;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            //if (dgList.CurrentRow != null)
            //{
            //    SelectMedicine(dgList.CurrentRow.Index );
            //}
            if (dgList.SelectedRows.Count != 0)
            {
                SelectMedicine(dgList.SelectedRows[0].Index);
            }
        }

        private void dgList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                SelectMedicine(e.RowIndex);
                pnlAlternateMed.Visible = false;
            }
        }

        private void SelectMedicine(int RowIdx)
        {
            int RowNumber = -1, Flag = 0;

            for (int i = 0; i < dgSelectedList.Rows.Count; i++)
            {
                if (dgList.Rows[RowIdx].Cells["MedName"].Value.ToString().Equals(dgSelectedList.Rows[i].Cells["MedicineName"].Value.ToString()))
                {
                    Flag = 1;
                    MessageBox.Show("Medicine Already Added ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                }
            }

            txtName.Text = dgList.Rows[RowIdx].Cells["MedName"].Value.ToString();
            txtName.Tag = dgList.Rows[RowIdx].Cells["MedicineId"].Value.ToString();
            //txtDosage.Text = dgList.Rows[RowIdx].Cells["Dosage"].Value.ToString();
            if (Flag == 0)
            {
                RowNumber = dgSelectedList.Rows.Add();
                dgSelectedList.Rows[RowNumber].Cells["MedID"].Value = dgList.Rows[RowIdx].Cells["MedicineId"].Value.ToString();
                dgSelectedList.Rows[RowNumber].Cells["MedicineName"].Value = dgList.Rows[RowIdx].Cells["MedName"].Value.ToString();
                //dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value = dgList.Rows[RowIdx].Cells["Dosage"].Value.ToString();
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    if (!cboDosage.Text.Equals("Select"))
                    {
                        dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value = clsSharedVariables.InItCaps(cboDosage.Text);
                        if (cboEngPeriod.SelectedIndex != 0)
                        {
                            dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value += " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                        }
                    }
                    else
                    {
                        dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value = "";
                    }
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value = txtDosage.Text;
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    dgSelectedList.Rows[RowNumber].Cells["MedicineDosage"].Value = txtDosage.Text;
                }

                dgSelectedList.Rows[RowNumber].Cells["Mode"].Value = "I";
                if (getAdversAllerg(dgList.Rows[RowIdx].Cells["MedicineId"].Value.ToString()))
                {
                    dgSelectedList.Rows[RowNumber].DefaultCellStyle.BackColor = Color.LightCoral;
                }
            }

            txtSearchMedicine.Text = "";
            dgList.ClearSelection();
            btnSave.Tag = "&Update";
            btnSave.Text = "&Update";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            dgSelectedList.Tag = "1";
            this.Close();
        }

        private void txtSearchMedicine_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgList.Rows.Count; i++)
            {
                if (dgList.Rows[i].Cells["MedName"].Value.ToString().ToUpper().StartsWith(txtSearchMedicine.Text.ToUpper()))
                {
                    dgList.Rows[i].Selected = true;
                    dgList.FirstDisplayedScrollingRowIndex = i;
                    txtName.Tag = dgList.Rows[i].Cells["MedicineId"].Value.ToString();
                    txtName.Text = dgList.Rows[i].Cells["MedName"].Value.ToString();
                    if (clsSharedVariables.Language.Equals("Eng"))
                    {
                        cboDosage.Text = dgList.Rows[i].Cells["Dosage"].Value.ToString().Split(' ')[0];
                        if (dgList.Rows[i].Cells["Dosage"].Value.ToString().Split(' ').Length == 3)
                        {
                            nudEngPeriodCount.Value = Convert.ToDecimal(dgList.Rows[i].Cells["Dosage"].Value.ToString().Split(' ')[1]);
                            cboEngPeriod.Text = dgList.Rows[i].Cells["Dosage"].Value.ToString().Split(' ')[2];
                        }
                    }
                    else if (clsSharedVariables.Language.Equals("Urdu"))
                    {
                        txtDosage.Text = dgList.Rows[i].Cells["Dosage"].Value.ToString();
                    }
                    else if (clsSharedVariables.Language.Equals("Farsi"))
                    {
                        txtDosage.Text = dgList.Rows[i].Cells["Dosage"].Value.ToString();
                    }
                    break;
                }
            }
        }

        private void btnChangeDosage_Click(object sender, EventArgs e)
        {
            int Flag = 0, i = 0;

            for (i = 0; i < dgSelectedList.Rows.Count; i++)
            {
                if (txtName.Tag.Equals(dgSelectedList.Rows[i].Cells["MedID"].Value.ToString()))
                {

                    if (clsSharedVariables.Language.Equals("Eng"))
                    {
                        string tmp = "";
                        if (cboEngPeriod.SelectedIndex != 0)
                        {
                            tmp = cboDosage.Text + " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                        }
                        else
                        {
                            tmp = cboDosage.Text;
                        }

                        if (dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString().Contains(tmp))
                        {
                            Flag = 1;
                            break;
                        }
                    }
                    else if (clsSharedVariables.Language.Equals("Urdu"))
                    {
                        if (dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString().Contains(txtDosage.Text))
                        {
                            Flag = 1;
                            break;
                        }
                    }
                    else if (clsSharedVariables.Language.Equals("Farsi"))
                    {
                        if (dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString().Contains(txtDosage.Text))
                        {
                            Flag = 1;
                            break;
                        }
                    }
                    break;
                }
            }
            if (Flag == 0)
            {
                if (dgSelectedList.Rows.Count != i)
                {
                    if (clsSharedVariables.Language.Equals("Eng"))
                    {
                        string tmp = "";
                        if (cboEngPeriod.SelectedIndex != 0)
                        {
                            tmp = cboDosage.Text + " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                        }
                        else
                        {
                            tmp = cboDosage.Text;
                        }

                        if (!tmp.Equals(dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString()) && !cboDosage.Text.Equals("Select"))
                        {
                            dgSelectedList.Rows[i].Cells["MedicineDosage"].Value = dgSelectedList.Rows[i].Cells["MedicineDosage"].Value + " , " + tmp;
                        }
                    }
                    else if (clsSharedVariables.Language.Equals("Urdu"))
                    {
                        if (!txtName.Text.Equals(dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString()) && !txtDosage.Text.Equals(""))
                        {
                            dgSelectedList.Rows[i].Cells["MedicineDosage"].Value = dgSelectedList.Rows[i].Cells["MedicineDosage"].Value + " , " + txtDosage.Text.Trim();
                        }
                    }
                    else if (clsSharedVariables.Language.Equals("Farsi"))
                    {
                        if (!txtName.Text.Equals(dgSelectedList.Rows[i].Cells["MedicineDosage"].Value.ToString()) && !txtDosage.Text.Equals(""))
                        {
                            dgSelectedList.Rows[i].Cells["MedicineDosage"].Value = dgSelectedList.Rows[i].Cells["MedicineDosage"].Value + " , " + txtDosage.Text.Trim();
                        }
                    }
                }
            }
        }

        private void dgList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                txtSearchMedicine.Tag = "S";
                txtDosage.Text = "";
                txtName.Text = "";
                btnSave.Tag = "&Save";
                btnSave.Text = "&Save";
                cboDosage.Text = "Select";
                nudEngPeriodCount.Value = 1;
                cboEngPeriod.SelectedIndex = 0;

                txtName.Tag = dgList.Rows[e.RowIndex].Cells["MedicineId"].Value.ToString();
                txtName.Text = dgList.Rows[e.RowIndex].Cells["MedName"].Value.ToString();
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    cboDosage.Text = dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Split(' ')[0].Trim();
                    if (dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Split(' ').Length == 3)
                    {
                        nudEngPeriodCount.Value = Convert.ToDecimal(dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Split(' ')[1].Trim());
                        cboEngPeriod.Text = dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Split(' ')[2].Trim();
                    }
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    txtDosage.Text = dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Replace("Ju", @"\Ju");
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    txtDosage.Text = dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString().Replace("Ju", @"\Ju");
                }
                btnSave.Tag = "&Update";
                btnSave.Text = "&Update";
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            if (miDelete.Tag != null)
            {
                dgSelectedList.Rows.RemoveAt(Convert.ToInt16(miDelete.Tag));
            }
        }

        private void dgSelectedList_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgSelectedList.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right))
            {
                dgSelectedList.ContextMenuStrip.Items[0].Enabled = true;
                dgSelectedList.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dgSelectedList.Rows[hi.RowIndex].Selected = true;
            }
            else
            {
                dgSelectedList.ContextMenuStrip.Items[0].Enabled = false;
                dgSelectedList.ContextMenuStrip.Items[0].Tag = null;

                //dgSelectedList.ContextMenuStrip = null;
            }

            hi = null;
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void cboDosage_Validating(object sender, CancelEventArgs e)
        {
            clsSharedVariables.InItCaps(cboDosage.Text);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtSearchMedicine.Tag.Equals("S"))
            {
                if (btnSave.Tag.Equals("&Save"))
                {
                    Insert();
                }
                else if (btnSave.Tag.Equals("&Update"))
                {
                    UpdateData();
                }
            }
            else
            {
                dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedId"].Value = txtName.Tag;
                dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedicineName"].Value = txtName.Text.Trim();
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedicineDosage"].Value = cboDosage.Text.Trim();
                    if (cboEngPeriod.SelectedIndex != 0)
                    {
                        dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedicineDosage"].Value +=
                            " " + nudEngPeriodCount.Value.ToString() + " " + cboEngPeriod.Text;
                    }
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedicineDosage"].Value = txtDosage.Text.Trim();
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    dgSelectedList.Rows[Convert.ToInt16(txtSearchMedicine.Tag)].Cells["MedicineDosage"].Value = txtDosage.Text.Trim();
                }
                txtSearchMedicine.Tag = "S";
                txtDosage.Text = "";
                txtName.Text = "";
                cboDosage.Text = "Select";
                nudEngPeriodCount.Value = 1;
                cboEngPeriod.SelectedIndex = 0;
            }
        }

        private void dgSelectedList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                txtSearchMedicine.Tag = e.RowIndex.ToString();

                txtDosage.Text = "";
                txtName.Text = "";
                btnSave.Tag = "&Save";
                btnSave.Text = "&Save";
                cboDosage.Text = "Select";
                nudEngPeriodCount.Value = 1;
                cboEngPeriod.SelectedIndex = 0;

                txtName.Tag = dgSelectedList.Rows[e.RowIndex].Cells["MedId"].Value.ToString();
                txtName.Text = dgSelectedList.Rows[e.RowIndex].Cells["MedicineName"].Value.ToString();
                if (clsSharedVariables.Language.Equals("Eng"))
                {
                    cboDosage.Text = dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString().Split(' ')[0];

                    if (dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString().Split(' ').Length == 3)
                    {
                        nudEngPeriodCount.Value = Convert.ToDecimal(dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString().Split(' ')[1]);
                        cboEngPeriod.Text = dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString().Split(' ')[2];
                    }
                }
                else if (clsSharedVariables.Language.Equals("Urdu"))
                {
                    txtDosage.Text = dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString();
                }
                else if (clsSharedVariables.Language.Equals("Farsi"))
                {
                    txtDosage.Text = dgSelectedList.Rows[e.RowIndex].Cells["MedicineDosage"].Value.ToString();
                }
                btnSave.Tag = "&Update";
                btnSave.Text = "&Update";
            }
        }

        private void txtDosage_Click(object sender, EventArgs e)
        {
            if (!txtName.Text.Equals(""))
            {
                btnSelectDosage_Click(btnSelectDosage, e);
            }
        }

        private void frmMedicineSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            if (dgSelectedList.Tag.Equals("1"))
            {
                DataGridView dg = new DataGridView();
                dg = dgSelectedList;

                MainOPD.AssignSelectedMed(dg);
            }
            this.Dispose();
        }

        private void FillCboEngDosage(clsBLDBConnection objConnection)
        {
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            SComponents objComp = new SComponents();
            DataView dv = new DataView();
            try
            {
                dv = objMed.GetAll(6);
                objComp.FillComboBox(cboDosage, dv, "Acronym", "Dosageid", true);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            //dv.Dispose();
            objMed = null;
            objComp = null;
        }

        private void btnAddFromList_Click(object sender, EventArgs e)
        {
            frmAddFromList objAddFromList = new frmAddFromList(this);
            objAddFromList.Show();
            this.Hide();
        }

        private void miMoreInfo_Click(object sender, EventArgs e)
        {
            if (miMoreInfo.Tag != null)
            {
                if (!dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.ToString().Equals(""))
                {
                    frmDrugInfo objDruginfo = new frmDrugInfo();
                    objDruginfo.MedID = dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.ToString();
                    objDruginfo.ShowDialog();
                    objDruginfo.Dispose();
                }
                else
                {
                    MessageBox.Show("More Information for Selected Medicine is not Available", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void dgList_MouseDown(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.DataGridView.HitTestInfo hi;
            hi = dgList.HitTest(e.X, e.Y);

            if (hi.RowIndex != -1 && e.Button.Equals(MouseButtons.Right) && !dgList.Rows[hi.RowIndex].Cells["MedRefID"].Value.ToString().Equals("0"))
            {
                dgList.ContextMenuStrip.Items[0].Enabled = true;
                dgList.ContextMenuStrip.Items[3].Enabled = true;

                dgList.ContextMenuStrip.Items[0].Tag = hi.RowIndex;
                dgList.Rows[hi.RowIndex].Selected = true;
                dgList.ContextMenuStrip.Items[3].Tag = hi.RowIndex;
            }
            else
            {
                dgList.ContextMenuStrip.Items[0].Enabled = false;
                dgList.ContextMenuStrip.Items[0].Tag = null;

                //dgSelectedList.ContextMenuStrip = null;
            }

            hi = null;
        }

        private void alternateMedicineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (miMoreInfo.Tag != null)
            {
                if (!dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.ToString().Equals(""))
                {
                    clsBLDBConnection objConnection = new clsBLDBConnection();
                    clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
                    DataView dv = new DataView();

                    objMedicine.MedicineID = dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.ToString(); ;
                    objConnection.Connection_Open();
                    dv = objMedicine.GetAll(17);
                    objConnection.Connection_Close();
                    dgAlterMed.DataSource = dv;
                    objMedicine = null;
                    pnlAlternateMed.Visible = true;
                }
                else
                {
                    MessageBox.Show("Alternate Medicine is not Available", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnAlterMedClose_Click(object sender, EventArgs e)
        {
            pnlAlternateMed.Visible = false;
        }

        private void dgList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.dgList.Columns["MedName"].Index)
            {
                DataGridViewCell cell = this.dgList.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = this.dgList.Rows[e.RowIndex].Cells["Dosage"].Value.ToString();
            }
        }

        private bool getAdversAllerg(string MedID)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLOPDMedAlerAdver objOPDMedAlergAdver = new clsBLOPDMedAlerAdver(objConnection);
            DataView dv = new DataView();
            try
            {
                objOPDMedAlergAdver.PatientID = _PatientID;
                objOPDMedAlergAdver.MedID = MedID;
                objConnection.Connection_Open();
                dv = objOPDMedAlergAdver.GetAll(5);

                if (dv.Table.Rows.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                return false;
            }
            finally
            {
                objConnection.Connection_Close();
                dv = null;
                objOPDMedAlergAdver = null;
                objConnection = null;
            }
        }

        private void miAgeWiseDosage_Click(object sender, EventArgs e)
        {
            if (miMoreInfo.Tag != null)
            {
                frmAgeWiseDosage objAgeWiseDos = new frmAgeWiseDosage();
                if (!dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.ToString().Equals(""))
                {
                    objAgeWiseDos.MedID = dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedRefID"].Value.
ToString();
                    objAgeWiseDos.MedName = dgList.Rows[Convert.ToInt16(miMoreInfo.Tag)].Cells["MedName"].Value.
ToString();
                    objAgeWiseDos.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Age Wise Dosage Information is not available", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                objAgeWiseDos = null;
            }
        }

        private void cmsDelete_Opening(object sender, CancelEventArgs e)
        {

        }

        private void DeleteMedicine_Click(object sender, EventArgs e)
        {
            var flag = 0;
            if (DeleteMedicine.Tag != null)
            {
                var id =Convert.ToString( this.dgList.Rows[Convert.ToInt16(DeleteMedicine.Tag)].Cells["MedicineID"].Value);
                if (!dgList.Rows[Convert.ToInt16(DeleteMedicine.Tag)].Cells["MedicineID"].Value.ToString().Equals(""))
                {
                    var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        
                        for (int i = 0; i < dgSelectedList.Rows.Count; i++)
                        {
                            if (dgList.Rows[Convert.ToInt16(DeleteMedicine.Tag)].Cells["MedName"].Value.ToString().Equals(dgSelectedList.Rows[i].Cells["MedicineName"].Value.ToString()))
                            {
                                
                                MessageBox.Show("Medicine Already Added in Patient Panal", "Cannot Delete Medicine", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                flag = 1;
                                break;
                             
                            }
                        }
                        if(flag==0)
                        {
                            clsBLDBConnection objConnection = new clsBLDBConnection();
                            clsBLMedicine objMedicine = new clsBLMedicine(objConnection);
                            try
                            {
                                objMedicine.MedicineID = dgList.Rows[Convert.ToInt16(DeleteMedicine.Tag)].Cells["MedicineID"].Value.ToString(); ;
                                objConnection.Connection_Open();
                                objConnection.Transaction_Begin();
                                if (objMedicine.Delete())
                                {
                                    objConnection.Transaction_ComRoll();
                                    FillGridView(objConnection);
                                    objConnection.Connection_Close();
                                    MessageBox.Show("Medicine Deleted Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ClearField();
                                }

                                else
                                {
                                    MessageBox.Show(objMedicine.ErrorMessage, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            finally
                            {
                                objMedicine = null;
                                objConnection = null;
                            }
                        }
                      
                    }
                    else
                    {
                        // If 'No', do something here.
                    }
                    

                    
                }
                else
                {
                    MessageBox.Show("Medicine Id Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void dgSelectedList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
