using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmDocType : Form
    {
       private Form frmParent= null;

        public frmDocType()
        {
            InitializeComponent();
        }

        public frmDocType(Form Frm)
        {
            InitializeComponent();
            this.frmParent = Frm;
        }

        private void frmDocType_Load(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);
            
            objConnection.Connection_Open();
            FillGridView(objConnection);
            objConnection.Connection_Close();

            dgDocType.ClearSelection();
            this.Text = this.Text + "(" + clsSharedVariables.InItCaps(clsSharedVariables.UserName) + ")";

            objComp = null;
            objConnection=null;
        }

        private clsBLDocType SetBLValues(clsBLDocType objDocType)
        {
            objDocType.Name= txtName.Text.Trim();
            objDocType.Description= txtDescription.Text.Trim();
            if (chbActive.Checked)
            {
                objDocType.Active = "1";
            }
            else
            {
                objDocType.Active = "0";
            }
            objDocType.EnteredBy = clsSharedVariables.UserID; ;
            objDocType.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objDocType.Type = rbPic.Checked ? "P" : rbDoc.Checked ? "D" : "V";

            return objDocType;
        }

        private void Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLDocType objDocType = new clsBLDocType(objConnection);

            try
            {
                objDocType = SetBLValues(objDocType);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objDocType.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Document Type Add Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objDocType.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objDocType = null;
                objConnection = null;
            }
        }

        private void UpdateData()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLDocType objDocType = new clsBLDocType(objConnection);
            try
            {
                objDocType.DocTypeID= this.lblDocTypeID.Text;
                objDocType = SetBLValues(objDocType);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objDocType.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Document Type updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objDocType.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objDocType = null;
                objConnection = null;
            }
        }

        private void FillGridView(clsBLDBConnection objConnection)
        {
            clsBLDocType objDocType = new clsBLDocType(objConnection);

            DataView dvDocType = objDocType.GetAll(1);
            //dvType.Sort = "Name";
            this.dgDocType.DataSource = dvDocType;
            lblTotalRec.Text = "Total Record : " + dgDocType.Rows.Count;

            GenrateSerialNo();

            objDocType= null;
            dvDocType = null;
        }

        private void ClearField()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            chbActive.Checked = true;
            dgDocType.ClearSelection();
            rbPic.Checked = true;
            txtName.Focus();
            this.lblDocTypeID.Text = "";
            this.miSave.Tag = "Save";
            this.miSave.Text = "Save";
        }

        private void GenrateSerialNo()
        {
            for (int i = 1; i <= dgDocType.Rows.Count; i++)
            {
                dgDocType.Rows[i - 1].Cells["SerialNo"].Value = i.ToString();
            }
        }

        private void FillForm(int rowIndex)
        {
            this.miSave.Tag = "Update";
            this.miSave.Text = "Update";

            lblDocTypeID.Text = dgDocType.Rows[rowIndex].Cells["DocTypeID"].Value.ToString();
            txtName.Text = dgDocType.Rows[rowIndex].Cells["DocTypeName"].Value.ToString();
            txtDescription.Text = dgDocType.Rows[rowIndex].Cells["Description"].Value.ToString();
            if (dgDocType.Rows[rowIndex].Cells["Active"].Value.ToString().Equals("1"))
            {
                chbActive.Checked = true;
            }
            else
            {
                chbActive.Checked = false;
            }
            if(dgDocType.Rows[rowIndex].Cells["Type"].Value.ToString().Equals("Picture"))
            {
                rbPic.Checked = true;
            }
            else if(dgDocType.Rows[rowIndex].Cells["Type"].Value.ToString().Equals("Document"))
            {
                rbDoc.Checked = true;
            }
            else if (dgDocType.Rows[rowIndex].Cells["Type"].Value.ToString().Equals("Video"))
            {
                rbVideo.Checked = true;
            }
        }

        private void dgDocType_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                FillForm(e.RowIndex);
            }
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if (this.miSave.Tag.Equals("Save"))
            {
                Insert();
            }
            else
            {
                UpdateData();
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            txtName .Text = clsSharedVariables.InItCaps(txtName.Text);
        }

        private void dgDocType_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GenrateSerialNo();
        }

        private void frmDocType_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.frmParent.Show();
            this.Dispose();
        }
    }
}