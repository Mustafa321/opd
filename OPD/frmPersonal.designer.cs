namespace OPD
{
    partial class frmPersonal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPersonal));
            this.lblNIC = new System.Windows.Forms.Label();
            this.lblCell = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblMaritalStatus = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cboTitle = new System.Windows.Forms.ComboBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.dtpDateBirth = new System.Windows.Forms.DateTimePicker();
            this.txtDesignation = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtCell = new System.Windows.Forms.TextBox();
            this.cboBloodGroup = new System.Windows.Forms.ComboBox();
            this.lblRank = new System.Windows.Forms.Label();
            this.lblDesignation = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblBloodGroup = new System.Windows.Forms.Label();
            this.txtRank = new System.Windows.Forms.TextBox();
            this.lblEducation = new System.Windows.Forms.Label();
            this.txtEducation = new System.Windows.Forms.TextBox();
            this.dgPersonal = new System.Windows.Forms.DataGridView();
            this.PersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PersonName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoginID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaritalStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Specialty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NIC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BloodGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Education = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pasword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EmailText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaxSlot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Charges = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevisitCharges = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevisitDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.nudRevisitDays = new System.Windows.Forms.NumericUpDown();
            this.nudCharges = new System.Windows.Forms.NumericUpDown();
            this.nudRevisitCharges = new System.Windows.Forms.NumericUpDown();
            this.nudmaxSlot = new System.Windows.Forms.NumericUpDown();
            this.txtEmailText = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblReVisitCharges = new System.Windows.Forms.Label();
            this.lblEmailText = new System.Windows.Forms.Label();
            this.lblRevisitDay = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbSingle = new System.Windows.Forms.RadioButton();
            this.rbMarried = new System.Windows.Forms.RadioButton();
            this.lblCharges = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rbOperator = new System.Windows.Forms.RadioButton();
            this.rbConsultant = new System.Windows.Forms.RadioButton();
            this.lblMaxSlot = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cboSpecialty = new System.Windows.Forms.ComboBox();
            this.lblSpecialty = new System.Windows.Forms.Label();
            this.txtCPassword = new System.Windows.Forms.TextBox();
            this.chbActive = new System.Windows.Forms.CheckBox();
            this.txtLoginID = new System.Windows.Forms.TextBox();
            this.lblLoginID = new System.Windows.Forms.Label();
            this.txtHint = new System.Windows.Forms.TextBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.txtPasword = new System.Windows.Forms.TextBox();
            this.txtNIC = new System.Windows.Forms.MaskedTextBox();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblPasword = new System.Windows.Forms.Label();
            this.lblCPassword = new System.Windows.Forms.Label();
            this.lblPersonID = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.miClear = new System.Windows.Forms.ToolStripButton();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miCancel = new System.Windows.Forms.ToolStripButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonal)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRevisitDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCharges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRevisitCharges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudmaxSlot)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNIC
            // 
            this.lblNIC.AutoSize = true;
            this.lblNIC.Location = new System.Drawing.Point(429, 111);
            this.lblNIC.Name = "lblNIC";
            this.lblNIC.Size = new System.Drawing.Size(31, 13);
            this.lblNIC.TabIndex = 0;
            this.lblNIC.Text = "NIC :";
            // 
            // lblCell
            // 
            this.lblCell.AutoSize = true;
            this.lblCell.Location = new System.Drawing.Point(409, 88);
            this.lblCell.Name = "lblCell";
            this.lblCell.Size = new System.Drawing.Size(50, 13);
            this.lblCell.TabIndex = 1;
            this.lblCell.Text = "Cell No. :";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(395, 63);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(64, 13);
            this.lblPhone.TabIndex = 2;
            this.lblPhone.Text = "Phone No. :";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(383, 14);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(42, 13);
            this.lblDOB.TabIndex = 3;
            this.lblDOB.Text = "D.O.B :";
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = true;
            this.lblMaritalStatus.Location = new System.Drawing.Point(70, 63);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(77, 13);
            this.lblMaritalStatus.TabIndex = 4;
            this.lblMaritalStatus.Text = "Marital Status :";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(99, 39);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(48, 13);
            this.lblGender.TabIndex = 5;
            this.lblGender.Text = "Gender :";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(106, 14);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name :";
            // 
            // cboTitle
            // 
            this.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTitle.FormattingEnabled = true;
            this.cboTitle.Items.AddRange(new object[] {
            "Dr.",
            "Prof.",
            "Mr.",
            "Miss",
            "Mrs."});
            this.cboTitle.Location = new System.Drawing.Point(150, 11);
            this.cboTitle.Name = "cboTitle";
            this.cboTitle.Size = new System.Drawing.Size(49, 21);
            this.cboTitle.TabIndex = 0;
            this.cboTitle.SelectedIndexChanged += new System.EventHandler(this.cboTitle_SelectedIndexChanged);
            // 
            // txtPhone
            // 
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Location = new System.Drawing.Point(461, 60);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(120, 20);
            this.txtPhone.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(200, 11);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(177, 20);
            this.txtName.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtName, "First, Middle and Last Name");
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbMale);
            this.panel1.Controls.Add(this.rbFemale);
            this.panel1.Location = new System.Drawing.Point(150, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 23);
            this.panel1.TabIndex = 3;
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Checked = true;
            this.rbMale.Location = new System.Drawing.Point(34, 2);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(48, 17);
            this.rbMale.TabIndex = 1;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "Male";
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(124, 2);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(59, 17);
            this.rbFemale.TabIndex = 0;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // dtpDateBirth
            // 
            this.dtpDateBirth.CustomFormat = "dd-MM-yyyy";
            this.dtpDateBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateBirth.Location = new System.Drawing.Point(425, 10);
            this.dtpDateBirth.Name = "dtpDateBirth";
            this.dtpDateBirth.Size = new System.Drawing.Size(78, 20);
            this.dtpDateBirth.TabIndex = 1;
            this.toolTip1.SetToolTip(this.dtpDateBirth, "Date Of Birth");
            this.dtpDateBirth.Leave += new System.EventHandler(this.dtpDateBirth_Leave);
            // 
            // txtDesignation
            // 
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.Location = new System.Drawing.Point(150, 152);
            this.txtDesignation.MaxLength = 50;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(227, 20);
            this.txtDesignation.TabIndex = 14;
            this.toolTip1.SetToolTip(this.txtDesignation, "Designation");
            this.txtDesignation.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // txtAddress
            // 
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Location = new System.Drawing.Point(150, 220);
            this.txtAddress.MaxLength = 255;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(432, 21);
            this.txtAddress.TabIndex = 20;
            this.toolTip1.SetToolTip(this.txtAddress, "Home address");
            this.txtAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // txtCell
            // 
            this.txtCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCell.Location = new System.Drawing.Point(461, 85);
            this.txtCell.MaxLength = 15;
            this.txtCell.Name = "txtCell";
            this.txtCell.Size = new System.Drawing.Size(120, 20);
            this.txtCell.TabIndex = 9;
            // 
            // cboBloodGroup
            // 
            this.cboBloodGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBloodGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBloodGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBloodGroup.FormattingEnabled = true;
            this.cboBloodGroup.Location = new System.Drawing.Point(461, 36);
            this.cboBloodGroup.Name = "cboBloodGroup";
            this.cboBloodGroup.Size = new System.Drawing.Size(58, 21);
            this.cboBloodGroup.TabIndex = 4;
            this.toolTip1.SetToolTip(this.cboBloodGroup, "Blood Group");
            // 
            // lblRank
            // 
            this.lblRank.AutoSize = true;
            this.lblRank.Location = new System.Drawing.Point(420, 155);
            this.lblRank.Name = "lblRank";
            this.lblRank.Size = new System.Drawing.Size(39, 13);
            this.lblRank.TabIndex = 20;
            this.lblRank.Text = "Rank :";
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Location = new System.Drawing.Point(78, 155);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(69, 13);
            this.lblDesignation.TabIndex = 21;
            this.lblDesignation.Text = "Designation :";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(96, 223);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 22;
            this.lblAddress.Text = "Address :";
            // 
            // lblBloodGroup
            // 
            this.lblBloodGroup.AutoSize = true;
            this.lblBloodGroup.Location = new System.Drawing.Point(387, 39);
            this.lblBloodGroup.Name = "lblBloodGroup";
            this.lblBloodGroup.Size = new System.Drawing.Size(72, 13);
            this.lblBloodGroup.TabIndex = 23;
            this.lblBloodGroup.Text = "Blood Group :";
            // 
            // txtRank
            // 
            this.txtRank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRank.Location = new System.Drawing.Point(461, 152);
            this.txtRank.MaxLength = 50;
            this.txtRank.Name = "txtRank";
            this.txtRank.Size = new System.Drawing.Size(121, 20);
            this.txtRank.TabIndex = 15;
            this.toolTip1.SetToolTip(this.txtRank, "Scale of ");
            this.txtRank.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Location = new System.Drawing.Point(86, 133);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(61, 13);
            this.lblEducation.TabIndex = 25;
            this.lblEducation.Text = "Education :";
            // 
            // txtEducation
            // 
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.Location = new System.Drawing.Point(150, 130);
            this.txtEducation.MaxLength = 255;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.Size = new System.Drawing.Size(227, 20);
            this.txtEducation.TabIndex = 12;
            this.toolTip1.SetToolTip(this.txtEducation, "Education");
            this.txtEducation.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // dgPersonal
            // 
            this.dgPersonal.AllowUserToAddRows = false;
            this.dgPersonal.AllowUserToDeleteRows = false;
            this.dgPersonal.AllowUserToResizeRows = false;
            this.dgPersonal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgPersonal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPersonal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgPersonal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPersonal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PersonID,
            this.Type,
            this.Email,
            this.Title,
            this.PersonName,
            this.LoginID,
            this.Gender,
            this.MaritalStatus,
            this.DOB,
            this.Specialty,
            this.PhoneNo,
            this.CellNo,
            this.NIC,
            this.Address,
            this.BloodGroup,
            this.Education,
            this.Designation,
            this.Rank,
            this.Pasword,
            this.Hint,
            this.Active,
            this.EmailText,
            this.MaxSlot,
            this.Charges,
            this.RevisitCharges,
            this.RevisitDays});
            this.dgPersonal.EnableHeadersVisualStyles = false;
            this.dgPersonal.Location = new System.Drawing.Point(3, 361);
            this.dgPersonal.MultiSelect = false;
            this.dgPersonal.Name = "dgPersonal";
            this.dgPersonal.ReadOnly = true;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPersonal.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgPersonal.RowHeadersWidth = 20;
            this.dgPersonal.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPersonal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPersonal.Size = new System.Drawing.Size(685, 209);
            this.dgPersonal.TabIndex = 27;
            this.dgPersonal.TabStop = false;
            this.dgPersonal.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPersonal_CellDoubleClick);
            // 
            // PersonID
            // 
            this.PersonID.DataPropertyName = "PersonID";
            this.PersonID.HeaderText = "Person ID";
            this.PersonID.Name = "PersonID";
            this.PersonID.ReadOnly = true;
            this.PersonID.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Visible = false;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Visible = false;
            // 
            // Title
            // 
            this.Title.DataPropertyName = "Title";
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Visible = false;
            // 
            // PersonName
            // 
            this.PersonName.DataPropertyName = "Name";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PersonName.DefaultCellStyle = dataGridViewCellStyle26;
            this.PersonName.HeaderText = "Name";
            this.PersonName.Name = "PersonName";
            this.PersonName.ReadOnly = true;
            this.PersonName.Width = 150;
            // 
            // LoginID
            // 
            this.LoginID.DataPropertyName = "LoginID";
            this.LoginID.HeaderText = "LoginID";
            this.LoginID.Name = "LoginID";
            this.LoginID.ReadOnly = true;
            this.LoginID.Visible = false;
            this.LoginID.Width = 110;
            // 
            // Gender
            // 
            this.Gender.DataPropertyName = "Gender";
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            this.Gender.Visible = false;
            // 
            // MaritalStatus
            // 
            this.MaritalStatus.DataPropertyName = "MaritalStatus";
            this.MaritalStatus.HeaderText = "MaritalStatus";
            this.MaritalStatus.Name = "MaritalStatus";
            this.MaritalStatus.ReadOnly = true;
            this.MaritalStatus.Visible = false;
            // 
            // DOB
            // 
            this.DOB.DataPropertyName = "DOB";
            this.DOB.HeaderText = "DOB";
            this.DOB.Name = "DOB";
            this.DOB.ReadOnly = true;
            this.DOB.Visible = false;
            // 
            // Specialty
            // 
            this.Specialty.DataPropertyName = "Specialty";
            this.Specialty.HeaderText = "Specialty";
            this.Specialty.Name = "Specialty";
            this.Specialty.ReadOnly = true;
            // 
            // PhoneNo
            // 
            this.PhoneNo.DataPropertyName = "PhoneNo";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PhoneNo.DefaultCellStyle = dataGridViewCellStyle27;
            this.PhoneNo.HeaderText = "Phone No.";
            this.PhoneNo.Name = "PhoneNo";
            this.PhoneNo.ReadOnly = true;
            this.PhoneNo.Width = 85;
            // 
            // CellNo
            // 
            this.CellNo.DataPropertyName = "CellNo";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CellNo.DefaultCellStyle = dataGridViewCellStyle28;
            this.CellNo.HeaderText = "Cell No.";
            this.CellNo.Name = "CellNo";
            this.CellNo.ReadOnly = true;
            this.CellNo.Width = 90;
            // 
            // NIC
            // 
            this.NIC.DataPropertyName = "NIC";
            this.NIC.HeaderText = "NIC";
            this.NIC.Name = "NIC";
            this.NIC.ReadOnly = true;
            this.NIC.Visible = false;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Address.DefaultCellStyle = dataGridViewCellStyle29;
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            this.Address.Width = 180;
            // 
            // BloodGroup
            // 
            this.BloodGroup.DataPropertyName = "BloodGroup";
            this.BloodGroup.HeaderText = "BloodGroup";
            this.BloodGroup.Name = "BloodGroup";
            this.BloodGroup.ReadOnly = true;
            this.BloodGroup.Visible = false;
            // 
            // Education
            // 
            this.Education.DataPropertyName = "Education";
            this.Education.HeaderText = "Education";
            this.Education.Name = "Education";
            this.Education.ReadOnly = true;
            this.Education.Visible = false;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "Designation";
            this.Designation.HeaderText = "Designation";
            this.Designation.Name = "Designation";
            this.Designation.ReadOnly = true;
            this.Designation.Visible = false;
            // 
            // Rank
            // 
            this.Rank.DataPropertyName = "Rank";
            this.Rank.HeaderText = "Rank";
            this.Rank.Name = "Rank";
            this.Rank.ReadOnly = true;
            this.Rank.Visible = false;
            // 
            // Pasword
            // 
            this.Pasword.DataPropertyName = "Pasword";
            this.Pasword.HeaderText = "Pasword";
            this.Pasword.Name = "Pasword";
            this.Pasword.ReadOnly = true;
            this.Pasword.Visible = false;
            // 
            // Hint
            // 
            this.Hint.DataPropertyName = "Hint";
            this.Hint.HeaderText = "Hint";
            this.Hint.Name = "Hint";
            this.Hint.ReadOnly = true;
            this.Hint.Visible = false;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "0";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.TrueValue = "1";
            this.Active.Width = 50;
            // 
            // EmailText
            // 
            this.EmailText.DataPropertyName = "EmailText";
            this.EmailText.HeaderText = "EmailText";
            this.EmailText.Name = "EmailText";
            this.EmailText.ReadOnly = true;
            this.EmailText.Visible = false;
            // 
            // MaxSlot
            // 
            this.MaxSlot.DataPropertyName = "MaxSlot";
            this.MaxSlot.HeaderText = "MaxSlot";
            this.MaxSlot.Name = "MaxSlot";
            this.MaxSlot.ReadOnly = true;
            this.MaxSlot.Visible = false;
            // 
            // Charges
            // 
            this.Charges.DataPropertyName = "Charges";
            this.Charges.HeaderText = "Charges";
            this.Charges.Name = "Charges";
            this.Charges.ReadOnly = true;
            this.Charges.Visible = false;
            // 
            // RevisitCharges
            // 
            this.RevisitCharges.DataPropertyName = "RevisitCharges";
            this.RevisitCharges.HeaderText = "RevisitCharges";
            this.RevisitCharges.Name = "RevisitCharges";
            this.RevisitCharges.ReadOnly = true;
            this.RevisitCharges.Visible = false;
            // 
            // RevisitDays
            // 
            this.RevisitDays.DataPropertyName = "RevisitDays";
            this.RevisitDays.HeaderText = "RevisitDays";
            this.RevisitDays.Name = "RevisitDays";
            this.RevisitDays.ReadOnly = true;
            this.RevisitDays.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.nudRevisitDays);
            this.panel3.Controls.Add(this.nudCharges);
            this.panel3.Controls.Add(this.nudRevisitCharges);
            this.panel3.Controls.Add(this.nudmaxSlot);
            this.panel3.Controls.Add(this.txtEmailText);
            this.panel3.Controls.Add(this.txtEmail);
            this.panel3.Controls.Add(this.lblEmail);
            this.panel3.Controls.Add(this.txtAge);
            this.panel3.Controls.Add(this.lblType);
            this.panel3.Controls.Add(this.lblReVisitCharges);
            this.panel3.Controls.Add(this.lblEmailText);
            this.panel3.Controls.Add(this.lblRevisitDay);
            this.panel3.Controls.Add(this.lblMaritalStatus);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.lblCharges);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.lblMaxSlot);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.cboSpecialty);
            this.panel3.Controls.Add(this.lblSpecialty);
            this.panel3.Controls.Add(this.txtCPassword);
            this.panel3.Controls.Add(this.chbActive);
            this.panel3.Controls.Add(this.txtLoginID);
            this.panel3.Controls.Add(this.lblLoginID);
            this.panel3.Controls.Add(this.txtHint);
            this.panel3.Controls.Add(this.lblHint);
            this.panel3.Controls.Add(this.txtPasword);
            this.panel3.Controls.Add(this.lblGender);
            this.panel3.Controls.Add(this.txtNIC);
            this.panel3.Controls.Add(this.lblDOB);
            this.panel3.Controls.Add(this.lblNIC);
            this.panel3.Controls.Add(this.lblName);
            this.panel3.Controls.Add(this.txtEducation);
            this.panel3.Controls.Add(this.lblEducation);
            this.panel3.Controls.Add(this.txtRank);
            this.panel3.Controls.Add(this.lblBloodGroup);
            this.panel3.Controls.Add(this.lblAddress);
            this.panel3.Controls.Add(this.cboTitle);
            this.panel3.Controls.Add(this.lblRank);
            this.panel3.Controls.Add(this.txtPhone);
            this.panel3.Controls.Add(this.txtName);
            this.panel3.Controls.Add(this.txtCell);
            this.panel3.Controls.Add(this.txtAddress);
            this.panel3.Controls.Add(this.dtpDateBirth);
            this.panel3.Controls.Add(this.txtDesignation);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.lblPhone);
            this.panel3.Controls.Add(this.lblAge);
            this.panel3.Controls.Add(this.lblCell);
            this.panel3.Controls.Add(this.lblPasword);
            this.panel3.Controls.Add(this.lblCPassword);
            this.panel3.Controls.Add(this.lblDesignation);
            this.panel3.Controls.Add(this.cboBloodGroup);
            this.panel3.Location = new System.Drawing.Point(3, 57);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(685, 302);
            this.panel3.TabIndex = 0;
            // 
            // nudRevisitDays
            // 
            this.nudRevisitDays.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudRevisitDays.Location = new System.Drawing.Point(533, 267);
            this.nudRevisitDays.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudRevisitDays.Name = "nudRevisitDays";
            this.nudRevisitDays.Size = new System.Drawing.Size(49, 20);
            this.nudRevisitDays.TabIndex = 25;
            this.nudRevisitDays.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudCharges
            // 
            this.nudCharges.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudCharges.Location = new System.Drawing.Point(260, 267);
            this.nudCharges.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudCharges.Name = "nudCharges";
            this.nudCharges.Size = new System.Drawing.Size(49, 20);
            this.nudCharges.TabIndex = 23;
            this.nudCharges.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // nudRevisitCharges
            // 
            this.nudRevisitCharges.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudRevisitCharges.Location = new System.Drawing.Point(409, 267);
            this.nudRevisitCharges.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudRevisitCharges.Name = "nudRevisitCharges";
            this.nudRevisitCharges.Size = new System.Drawing.Size(49, 20);
            this.nudRevisitCharges.TabIndex = 24;
            this.nudRevisitCharges.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // nudmaxSlot
            // 
            this.nudmaxSlot.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudmaxSlot.Location = new System.Drawing.Point(150, 267);
            this.nudmaxSlot.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudmaxSlot.Name = "nudmaxSlot";
            this.nudmaxSlot.Size = new System.Drawing.Size(49, 20);
            this.nudmaxSlot.TabIndex = 22;
            this.nudmaxSlot.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // txtEmailText
            // 
            this.txtEmailText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailText.Location = new System.Drawing.Point(150, 244);
            this.txtEmailText.Name = "txtEmailText";
            this.txtEmailText.Size = new System.Drawing.Size(432, 20);
            this.txtEmailText.TabIndex = 21;
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Location = new System.Drawing.Point(150, 108);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(227, 20);
            this.txtEmail.TabIndex = 10;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(109, 111);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 151;
            this.lblEmail.Text = "Email :";
            // 
            // txtAge
            // 
            this.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAge.Location = new System.Drawing.Point(550, 11);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(32, 20);
            this.txtAge.TabIndex = 2;
            this.txtAge.Leave += new System.EventHandler(this.txtAge_Leave);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(109, 88);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(37, 13);
            this.lblType.TabIndex = 149;
            this.lblType.Text = "Type :";
            this.lblType.Visible = false;
            // 
            // lblReVisitCharges
            // 
            this.lblReVisitCharges.AutoSize = true;
            this.lblReVisitCharges.Location = new System.Drawing.Point(317, 269);
            this.lblReVisitCharges.Name = "lblReVisitCharges";
            this.lblReVisitCharges.Size = new System.Drawing.Size(87, 13);
            this.lblReVisitCharges.TabIndex = 154;
            this.lblReVisitCharges.Text = "Revisit Charges :";
            // 
            // lblEmailText
            // 
            this.lblEmailText.AutoSize = true;
            this.lblEmailText.Location = new System.Drawing.Point(79, 244);
            this.lblEmailText.Name = "lblEmailText";
            this.lblEmailText.Size = new System.Drawing.Size(65, 13);
            this.lblEmailText.TabIndex = 157;
            this.lblEmailText.Text = "Email Text  :";
            // 
            // lblRevisitDay
            // 
            this.lblRevisitDay.AutoSize = true;
            this.lblRevisitDay.Location = new System.Drawing.Point(458, 270);
            this.lblRevisitDay.Name = "lblRevisitDay";
            this.lblRevisitDay.Size = new System.Drawing.Size(72, 13);
            this.lblRevisitDay.TabIndex = 158;
            this.lblRevisitDay.Text = "Revisit Days :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rbSingle);
            this.panel2.Controls.Add(this.rbMarried);
            this.panel2.Location = new System.Drawing.Point(150, 58);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 23);
            this.panel2.TabIndex = 6;
            // 
            // rbSingle
            // 
            this.rbSingle.AutoSize = true;
            this.rbSingle.Checked = true;
            this.rbSingle.Location = new System.Drawing.Point(34, 2);
            this.rbSingle.Name = "rbSingle";
            this.rbSingle.Size = new System.Drawing.Size(54, 17);
            this.rbSingle.TabIndex = 13;
            this.rbSingle.TabStop = true;
            this.rbSingle.Text = "Single";
            this.rbSingle.UseVisualStyleBackColor = true;
            // 
            // rbMarried
            // 
            this.rbMarried.AutoSize = true;
            this.rbMarried.Location = new System.Drawing.Point(124, 2);
            this.rbMarried.Name = "rbMarried";
            this.rbMarried.Size = new System.Drawing.Size(60, 17);
            this.rbMarried.TabIndex = 12;
            this.rbMarried.Text = "Married";
            this.rbMarried.UseVisualStyleBackColor = true;
            // 
            // lblCharges
            // 
            this.lblCharges.AutoSize = true;
            this.lblCharges.Location = new System.Drawing.Point(204, 269);
            this.lblCharges.Name = "lblCharges";
            this.lblCharges.Size = new System.Drawing.Size(52, 13);
            this.lblCharges.TabIndex = 156;
            this.lblCharges.Text = "Charges :";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.rbOperator);
            this.panel7.Controls.Add(this.rbConsultant);
            this.panel7.Location = new System.Drawing.Point(150, 83);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(227, 23);
            this.panel7.TabIndex = 8;
            this.panel7.Visible = false;
            // 
            // rbOperator
            // 
            this.rbOperator.AutoSize = true;
            this.rbOperator.Location = new System.Drawing.Point(124, 2);
            this.rbOperator.Name = "rbOperator";
            this.rbOperator.Size = new System.Drawing.Size(66, 17);
            this.rbOperator.TabIndex = 1;
            this.rbOperator.Text = "Operator";
            this.rbOperator.UseVisualStyleBackColor = true;
            // 
            // rbConsultant
            // 
            this.rbConsultant.AutoCheck = false;
            this.rbConsultant.AutoSize = true;
            this.rbConsultant.Checked = true;
            this.rbConsultant.Location = new System.Drawing.Point(34, 2);
            this.rbConsultant.Name = "rbConsultant";
            this.rbConsultant.Size = new System.Drawing.Size(75, 17);
            this.rbConsultant.TabIndex = 0;
            this.rbConsultant.TabStop = true;
            this.rbConsultant.Text = "Consultant";
            this.rbConsultant.UseVisualStyleBackColor = true;
            // 
            // lblMaxSlot
            // 
            this.lblMaxSlot.AutoSize = true;
            this.lblMaxSlot.Location = new System.Drawing.Point(70, 269);
            this.lblMaxSlot.Name = "lblMaxSlot";
            this.lblMaxSlot.Size = new System.Drawing.Size(78, 13);
            this.lblMaxSlot.TabIndex = 155;
            this.lblMaxSlot.Text = "Maximum Slot :";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Red;
            this.panel6.Location = new System.Drawing.Point(373, 11);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(4, 20);
            this.panel6.TabIndex = 147;
            this.panel6.Tag = "no";
            // 
            // cboSpecialty
            // 
            this.cboSpecialty.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSpecialty.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSpecialty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSpecialty.FormattingEnabled = true;
            this.cboSpecialty.Items.AddRange(new object[] {
            "Select",
            "Orthopedic",
            "Cardiologist   ",
            "General Surgeon"});
            this.cboSpecialty.Location = new System.Drawing.Point(461, 130);
            this.cboSpecialty.Name = "cboSpecialty";
            this.cboSpecialty.Size = new System.Drawing.Size(121, 21);
            this.cboSpecialty.TabIndex = 13;
            // 
            // lblSpecialty
            // 
            this.lblSpecialty.AutoSize = true;
            this.lblSpecialty.Location = new System.Drawing.Point(403, 133);
            this.lblSpecialty.Name = "lblSpecialty";
            this.lblSpecialty.Size = new System.Drawing.Size(56, 13);
            this.lblSpecialty.TabIndex = 39;
            this.lblSpecialty.Text = "Specialty :";
            // 
            // txtCPassword
            // 
            this.txtCPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCPassword.Location = new System.Drawing.Point(150, 197);
            this.txtCPassword.MaxLength = 15;
            this.txtCPassword.Name = "txtCPassword";
            this.txtCPassword.PasswordChar = '*';
            this.txtCPassword.Size = new System.Drawing.Size(167, 20);
            this.txtCPassword.TabIndex = 18;
            // 
            // chbActive
            // 
            this.chbActive.AutoSize = true;
            this.chbActive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbActive.Checked = true;
            this.chbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbActive.Location = new System.Drawing.Point(520, 38);
            this.chbActive.Name = "chbActive";
            this.chbActive.Size = new System.Drawing.Size(62, 17);
            this.chbActive.TabIndex = 5;
            this.chbActive.Text = "Active :";
            this.chbActive.UseVisualStyleBackColor = true;
            // 
            // txtLoginID
            // 
            this.txtLoginID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoginID.Location = new System.Drawing.Point(150, 174);
            this.txtLoginID.MaxLength = 15;
            this.txtLoginID.Name = "txtLoginID";
            this.txtLoginID.Size = new System.Drawing.Size(167, 20);
            this.txtLoginID.TabIndex = 16;
            this.toolTip1.SetToolTip(this.txtLoginID, "Education");
            this.txtLoginID.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // lblLoginID
            // 
            this.lblLoginID.AutoSize = true;
            this.lblLoginID.Location = new System.Drawing.Point(94, 177);
            this.lblLoginID.Name = "lblLoginID";
            this.lblLoginID.Size = new System.Drawing.Size(53, 13);
            this.lblLoginID.TabIndex = 37;
            this.lblLoginID.Text = "Login ID :";
            // 
            // txtHint
            // 
            this.txtHint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHint.Location = new System.Drawing.Point(404, 197);
            this.txtHint.MaxLength = 15;
            this.txtHint.Name = "txtHint";
            this.txtHint.Size = new System.Drawing.Size(177, 20);
            this.txtHint.TabIndex = 19;
            // 
            // lblHint
            // 
            this.lblHint.AutoSize = true;
            this.lblHint.Location = new System.Drawing.Point(367, 200);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(32, 13);
            this.lblHint.TabIndex = 34;
            this.lblHint.Text = "Hint :";
            // 
            // txtPasword
            // 
            this.txtPasword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPasword.Location = new System.Drawing.Point(405, 174);
            this.txtPasword.MaxLength = 15;
            this.txtPasword.Name = "txtPasword";
            this.txtPasword.PasswordChar = '*';
            this.txtPasword.Size = new System.Drawing.Size(177, 20);
            this.txtPasword.TabIndex = 17;
            this.toolTip1.SetToolTip(this.txtPasword, "Designation");
            // 
            // txtNIC
            // 
            this.txtNIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNIC.HidePromptOnLeave = true;
            this.txtNIC.HideSelection = false;
            this.txtNIC.Location = new System.Drawing.Point(462, 108);
            this.txtNIC.Mask = "00000-0000000-0";
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.ResetOnSpace = false;
            this.txtNIC.Size = new System.Drawing.Size(120, 20);
            this.txtNIC.TabIndex = 11;
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(512, 14);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(35, 13);
            this.lblAge.TabIndex = 150;
            this.lblAge.Text = "Age : ";
            // 
            // lblPasword
            // 
            this.lblPasword.AutoSize = true;
            this.lblPasword.Location = new System.Drawing.Point(340, 177);
            this.lblPasword.Name = "lblPasword";
            this.lblPasword.Size = new System.Drawing.Size(59, 13);
            this.lblPasword.TabIndex = 35;
            this.lblPasword.Text = "Password :";
            // 
            // lblCPassword
            // 
            this.lblCPassword.AutoSize = true;
            this.lblCPassword.Location = new System.Drawing.Point(50, 200);
            this.lblCPassword.Name = "lblCPassword";
            this.lblCPassword.Size = new System.Drawing.Size(97, 13);
            this.lblCPassword.TabIndex = 38;
            this.lblCPassword.Text = "Confirm Password :";
            // 
            // lblPersonID
            // 
            this.lblPersonID.AutoSize = true;
            this.lblPersonID.Location = new System.Drawing.Point(114, 340);
            this.lblPersonID.Name = "lblPersonID";
            this.lblPersonID.Size = new System.Drawing.Size(61, 13);
            this.lblPersonID.TabIndex = 31;
            this.lblPersonID.Text = "lblPersonID";
            this.lblPersonID.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Azure;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miClear,
            this.miSave,
            this.miCancel});
            this.toolStrip1.Location = new System.Drawing.Point(501, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(181, 55);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // miClear
            // 
            this.miClear.AutoSize = false;
            this.miClear.BackColor = System.Drawing.Color.LightSlateGray;
            this.miClear.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miClear.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(60, 50);
            this.miClear.Text = "&Clear";
            this.miClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miClear.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miClear.Click += new System.EventHandler(this.miClear_Click);
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(60, 50);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miCancel
            // 
            this.miCancel.AutoSize = false;
            this.miCancel.BackColor = System.Drawing.Color.LightSlateGray;
            this.miCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miCancel.Image = ((System.Drawing.Image)(resources.GetObject("miCancel.Image")));
            this.miCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miCancel.Name = "miCancel";
            this.miCancel.Size = new System.Drawing.Size(60, 50);
            this.miCancel.Text = "&Exit";
            this.miCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miCancel.Click += new System.EventHandler(this.miCancel_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.toolStrip1);
            this.panel4.Location = new System.Drawing.Point(3, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(685, 55);
            this.panel4.TabIndex = 1;
            this.panel4.Tag = "top";
            // 
            // frmPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 571);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.dgPersonal);
            this.Controls.Add(this.lblPersonID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmPersonal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Personal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPersonal_FormClosing);
            this.Load += new System.EventHandler(this.frmPersonal_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonal)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRevisitDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCharges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRevisitCharges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudmaxSlot)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNIC;
        private System.Windows.Forms.Label lblCell;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblMaritalStatus;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cboTitle;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.DateTimePicker dtpDateBirth;
        private System.Windows.Forms.TextBox txtDesignation;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtCell;
        private System.Windows.Forms.ComboBox cboBloodGroup;
        private System.Windows.Forms.Label lblRank;
        private System.Windows.Forms.Label lblDesignation;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblBloodGroup;
        private System.Windows.Forms.TextBox txtRank;
        private System.Windows.Forms.Label lblEducation;
        private System.Windows.Forms.TextBox txtEducation;
        private System.Windows.Forms.DataGridView dgPersonal;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.MaskedTextBox txtNIC;
        private System.Windows.Forms.Label lblPersonID;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chbActive;
        private System.Windows.Forms.TextBox txtLoginID;
        private System.Windows.Forms.Label lblLoginID;
        private System.Windows.Forms.TextBox txtHint;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.TextBox txtCPassword;
        private System.Windows.Forms.Label lblCPassword;
        private System.Windows.Forms.Label lblPasword;
        private System.Windows.Forms.TextBox txtPasword;
        private System.Windows.Forms.ComboBox cboSpecialty;
        private System.Windows.Forms.Label lblSpecialty;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton rbOperator;
        private System.Windows.Forms.RadioButton rbConsultant;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbSingle;
        private System.Windows.Forms.RadioButton rbMarried;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton miClear;
        private System.Windows.Forms.ToolStripButton miSave;
        private System.Windows.Forms.ToolStripButton miCancel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtEmailText;
        private System.Windows.Forms.Label lblReVisitCharges;
        private System.Windows.Forms.Label lblEmailText;
        private System.Windows.Forms.Label lblRevisitDay;
        private System.Windows.Forms.Label lblCharges;
        private System.Windows.Forms.Label lblMaxSlot;
        private System.Windows.Forms.NumericUpDown nudRevisitDays;
        private System.Windows.Forms.NumericUpDown nudCharges;
        private System.Windows.Forms.NumericUpDown nudRevisitCharges;
        private System.Windows.Forms.NumericUpDown nudmaxSlot;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoginID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaritalStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn DOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn Specialty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn BloodGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Education;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pasword;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hint;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmailText;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaxSlot;
        private System.Windows.Forms.DataGridViewTextBoxColumn Charges;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevisitCharges;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevisitDays;
    }
}