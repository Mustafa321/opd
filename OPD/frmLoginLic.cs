using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using OPD_BL;

namespace OPD
{
  public partial class frmLoginLic : Form
  {
    private string KeyVal = "";
    private string LoginIDS = "";

      public frmLoginLic()
    {
      InitializeComponent();
    }

    private void frmLoginLic_Load(object sender, EventArgs e)
    {
      SComponents objComp = new SComponents();
      clsBLDBConnection objConnection = new clsBLDBConnection();

      objComp.ApplyStyleToControls(this);
      lblVersion.Text = clsSharedVariables.Version;
      {
        int opt = DecryptCode();
        if (opt == 0)
        {
          MessageBox.Show("License File is Deleted ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          SaveLoginName(EncKey("Key"), " ", "", "");
          this.Close();
        }
        else if (opt == 1)
        {
          MessageBox.Show("Corrupt License File ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          SaveLoginName(EncKey("Key"), "", "", "");
          this.Close();
        }
        else if (opt == 2)
        {
          MessageBox.Show("License is Expired", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          SaveLoginName(EncKey("Key"), "", "", "");

          this.Close();
        }
        else if (opt == 3)
        {
          GetLanguage();
          GetLoginID();
          objComp = null;
        }
        else if (opt == 4)
        {
          MessageBox.Show("Change User Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          SaveLoginName(EncKey("Key"), "", "", "");
          this.Close();
        }
        else if (opt == 5)
        {
          MessageBox.Show("Change Login ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          SaveLoginName(EncKey("Key"), "", "", "");
          this.Close();
        }
      }
    }

    private string EncKey(string str)
    {
      string str1 = "";

      for (int i = str.Length - 1; i >= 0; i--)
      {
        str1 = str1 + (char)(str[i] - 10);
      }

      return str1;
    }

    private string DecryptKey(string Info)
    {
      string tmpInfo = "";
      for (int i = Info.Length - 1; i >= 0; i--)
      {
        tmpInfo = tmpInfo + (char)(Info[i] + 10);
      }

      return tmpInfo;
    }

    private int DecryptCode()
    {
      string Info = "";
      string[] InfoArr;

      if (System.IO.File.Exists("License.lic"))
      {
        try
        {
          KeyVal = GetKey();
          if (KeyVal.Trim().Equals("") || clsSharedVariables.UserName.Equals("") || clsSharedVariables.LoginID.Equals(""))
          {
            frmReg objReg = new frmReg();

            objReg.ShowDialog();
            if (objReg.Validation.Equals(""))
            {
              objReg.Dispose();
              this.Close();
            }
            else
            {
              SaveLoginName(EncKey("Key"), EncKey(objReg.Validation), EncKey(clsSharedVariables.UserName), EncKey(clsSharedVariables.LoginID));
              KeyVal = objReg.Validation;
            }
            objReg.Dispose();
          }

          Info = System.IO.File.ReadAllText("License.lic");

          InfoArr = DecryptKey(Info).Split('\n');
          if (InfoArr.Length == 5)
          {
            if (!InfoArr[2].Contains(GetKey()))
            {
              return 1;//change License File
            }
            else if (!InfoArr[0].ToUpper().Contains(clsSharedVariables.UserName.ToUpper()))
            {
              return 4;//change User Name 
            }
            else if (!InfoArr[1].ToUpper().Contains(clsSharedVariables.LoginID.ToUpper()))
            {
              return 5;//change Login ID
            }
            //else if (DateTime.Compare(Convert.ToDateTime(InfoArr[4].Substring(9, 10)), DateTime.Now) == -1)
            //{
            //  return 2;//License is Expired
            //}
            else
            {
              LoginIDS = InfoArr[1];
              return 3;//Validate Every Thing
            }
          }
          else
          {
            return 1;//change License File
          }
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
          return 1;
        }
      }
      else
      {
        return 0;//License file deleted
      }
    }

    private void GetLanguage()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);

      objRef.ReportRefrence = "InstLang";

      objConnection.Connection_Open();
      clsSharedVariables.Language = objRef.GetAll(2).Table.Rows[0]["Description"].ToString();
      objConnection.Connection_Close();

      objConnection = null;
      objRef = null;
    }

    private string GetKey()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      string KeyVal = "";
      try
      {
        objRef.ReportRefrence = EncKey("Key");

        objConnection.Connection_Open();
        DataView dv = objRef.GetAll(2);
        KeyVal = dv.Table.Rows[0]["Description"].ToString().Trim();
        clsSharedVariables.UserName = DecryptKey(dv.Table.Rows[0]["ReportTitle1"].ToString().Trim());
        clsSharedVariables.LoginID = DecryptKey(dv.Table.Rows[0]["ReportTitle2"].ToString().Trim());
        objConnection.Connection_Close();

        objConnection = null;
        objRef = null;
        return DecryptKey(KeyVal);
      }
      catch (Exception exc)
      {
        //MessageBox.Show(exc.Message);
        return "";
      }
    }

    private void llblForget_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      //lblHint.Visible = true;
      //txtHint.Visible = true;
      GetHintValue();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void btnLogin_Click(object sender, EventArgs e)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
      DataView dvPersonal;

      if (!txtloginID.Text.Equals(""))
      {
        if (!txtloginID.Text.ToUpper().Equals("TREES"))
        {

          if (!LoginIDS.ToUpper().Contains(txtloginID.Text.Trim().ToUpper()))
          {
            MessageBox.Show("User ID is not Register", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
          }
        }
        objPersonal.LoginID = txtloginID.Text.Trim();
        objConnection.Connection_Open();
        dvPersonal = objPersonal.GetAll(3);
        objConnection.Connection_Close();

        if (dvPersonal.Count == 0)
        {
          MessageBox.Show("User not Exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          return;
        }
        else
        {
          if (dvPersonal.Table.Rows[0]["Active"].ToString().Equals("1"))
          {
            if (txtPasword.Text.ToString().Equals(dvPersonal.Table.Rows[0]["Pasword"].ToString()))
            {
              if (chbRemember.Checked)
              {
                SaveLoginName("LoginID", txtloginID.Text.Trim(), txtPasword.Text, "");
              }
              else
              {
                SaveLoginName("LoginID", txtloginID.Text.Trim(), "", "");
              }
              clsSharedVariables.UserName = dvPersonal.Table.Rows[0]["Title"].ToString() + " " + dvPersonal.Table.Rows[0]["Name"].ToString();
              clsSharedVariables.UserID = dvPersonal.Table.Rows[0]["PersonID"].ToString();
              clsSharedVariables.LoginID = txtloginID.Text.Trim();

              GetSMTPSetting();

              if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("C"))
              {
                frmOPD objOPD = new frmOPD(this);
                if (!chbRemember.Checked)
                {
                  txtPasword.Text = "";
                }
                //objOPD.MdiParent = this;
                this.Hide();
                objOPD.Show();
              }
              else if (dvPersonal.Table.Rows[0]["Type"].ToString().Equals("O"))
              {
                frmPatientReg objPatientReg = new frmPatientReg();
                txtPasword.Text = "";
                objPatientReg.Show(this);
              }

              //this.Close();

            }
            else
            {
              MessageBox.Show("Invalid Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          }
          else
          {
            MessageBox.Show("Inactive User, Please contact your Admin", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          }

        }
      }
      else
      {
        MessageBox.Show("Please Enter LoginID ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      objConnection = null;
      objPersonal = null;
      dvPersonal = null;

    }

    private void GetHintValue()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
      DataView dvPersonal;

      if (!txtloginID.Text.Equals(""))
      {
        objPersonal.LoginID = txtloginID.Text.Trim();
        objConnection.Connection_Open();
        dvPersonal = objPersonal.GetAll(3);
        objConnection.Connection_Close();

        if (dvPersonal.Count == 0)
        {
          MessageBox.Show("User not Exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

          return;
        }
        else
        {
          //if (txtHint.Text.ToString().ToUpper().Equals(dvPersonal.Table.Rows[0]["Hint"].ToString().ToUpper()))
          {
            MessageBox.Show(dvPersonal.Table.Rows[0]["Hint"].ToString());
          }
          //    else
          //    {
          //        MessageBox.Show("Invalid Hint", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          //    }
        }
      }
      else
      {
        MessageBox.Show("Please Enter LoginID ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      objConnection = null;
      objPersonal = null;
      dvPersonal = null;
    }

    private void GetLoginID()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      DataView dvPersonal;

      //objRef.Description= clsSharedVariables.UserID;
      objRef.ReportRefrence = "LoginID";

      objConnection.Connection_Open();
      dvPersonal = objRef.GetAll(2);
      objConnection.Connection_Close();

      if (dvPersonal.Count != 0)
      {
        txtloginID.Text = dvPersonal.Table.Rows[0]["Description"].ToString();
        if (!dvPersonal.Table.Rows[0]["ReportTitle1"].ToString().Equals(""))
        {
          txtPasword.Text = dvPersonal.Table.Rows[0]["ReportTitle1"].ToString();
          chbRemember.Checked = true;
        }
      }
      objConnection = null;
      objRef = null;
      dvPersonal = null;
    }

    private void SaveLoginName(string RptRef, string Description, string ReportTitle1, string ReportTitle2)
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      try
      {
        //objPersonal.PersonID = clsSharedVariables.UserID  ;
        objRef.ReportRefrence = RptRef;
        objRef.Description = Description;
        objRef.ReportTitle1 = ReportTitle1;
        objRef.ReportTitle2 = ReportTitle2;

        objConnection.Connection_Open();
        objConnection.Transaction_Begin();
        if (objRef.Update())
        {
          objConnection.Transaction_ComRoll();
          objConnection.Connection_Close();
        }
        else
        {
          MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objRef = null;
        objConnection = null;
      }
    }

    private void txtPasword_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == '\r')
      {
        EventArgs ea = new EventArgs();
        btnLogin_Click(null, ea);

        ea = null;
      }
    }

    private void txtloginID_Validating(object sender, CancelEventArgs e)
    {
      clsSharedVariables.InItCaps(txtloginID.Text);
    }

    private void lbtnGreen_MouseHover(object sender, EventArgs e)
    {
      LinkLabel lbtn = (LinkLabel)sender;

      lbtn.LinkColor = Color.LimeGreen;
    }

    private void lbtnBlack_MouseHover(object sender, EventArgs e)
    {
      LinkLabel lbtn = (LinkLabel)sender;
      lbtn.LinkColor = Color.Black;
    }

    private void lbtnGreen_MouseLeave(object sender, EventArgs e)
    {
      LinkLabel lbtn = (LinkLabel)sender;
      lbtn.LinkColor = Color.LimeGreen;
    }

    private void lbtnGray_MouseLeave(object sender, EventArgs e)
    {
      LinkLabel lbtn = (LinkLabel)sender;
      lbtn.LinkColor = Color.Gray;
    }

    private void lbtnAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      frmAbout objAbout = new frmAbout();
      objAbout.ShowDialog();
      objAbout.Dispose();
    }

    private void lbtnClinicNews_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      frmNews objNews = new frmNews();
      objNews.ShowDialog();
      objNews.Dispose();
    }

    private void lbtntreesvalley_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      WebBrowser objWebBrowser = new WebBrowser();
      objWebBrowser.Navigate("http://www.treesvalley.com");
      objWebBrowser.Dispose();
    }

    private void lbtnhaisam_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      WebBrowser objWebBrowser = new WebBrowser();
      objWebBrowser.Navigate("http://www.haisam.org", true);
      objWebBrowser.Dispose();
    }

    private void GetSMTPSetting()
    {
      clsBLDBConnection objConnection=new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      DataView dv = new DataView();

      try
      {
          objRef.ReportRefrence = "SetMail";
          objConnection.Connection_Open();
          dv = objRef.GetAll(2);

          if (dv.Table.Rows.Count != 0)
          {
            clsSharedVariables.SMTPEmail = dv.Table.Rows[0]["Description"].ToString();
            clsSharedVariables.Password = dv.Table.Rows[0]["ReportTitle1"].ToString();
            clsSharedVariables.Port = dv.Table.Rows[0]["ReportTitle2"].ToString();
            clsSharedVariables.SMTP = dv.Table.Rows[0]["ReportTitle3"].ToString();
          }
      }
      catch (Exception ex)
      {
      }
      finally
      {
        objConnection.Connection_Close();

        objConnection = null;
        objRef = null;
        dv.Dispose();
      }
    }

      private void panel3_Paint(object sender, PaintEventArgs e)
      {

      }
  }
}