using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
  public partial class frmAttendentSetting : Form
  {
    private string _PersonID = "";
    private string _PersonName = "";
    
    public string PersonID
    {
      get { return _PersonID; }
      set { _PersonID = value; }
    }

    public string PersonName
    {
      get { return _PersonName; }
      set { _PersonName = value; }
    }

    public frmAttendentSetting()
    {
      InitializeComponent();
    }

    private void frmAttendentSetting_Load(object sender, EventArgs e)
    {
      SComponents objComp = new SComponents();

      objComp.ApplyStyleToControls(this);
      lblConsultant.Text = "Consultant Name : Dr. " +  _PersonName;
      GetDaysRecord();


      objComp = null;
    }

    private void GetDaysRecord()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      DataView dv = new DataView();

      objRef.ReportRefrence = "PersonID" + _PersonID;

      objConnection.Connection_Open();
      dv = objRef.GetAll(2);
      objConnection.Connection_Close();

      if (dv.Table.Rows.Count != 0)
      {
        chbMonday.Checked = dv.Table.Rows[0]["Description"].ToString().Equals("Y") ? true : false;
        chbTuesday.Checked = dv.Table.Rows[0]["ReportTitle1"].ToString().Equals("Y") ? true : false;
        chbWednesday.Checked = dv.Table.Rows[0]["ReportTitle2"].ToString().Equals("Y") ? true : false;
        chbThursday.Checked = dv.Table.Rows[0]["ReportTitle3"].ToString().Equals("Y") ? true : false;
        chbFriday.Checked = dv.Table.Rows[0]["TreesFooter1"].ToString().Equals("Y") ? true : false;
        chbSaturday.Checked = dv.Table.Rows[0]["TreesFooter2"].ToString().Equals("Y") ? true : false;
        chbSunday.Checked = dv.Table.Rows[0]["ClientWebAddress"].ToString().Equals("Y") ? true : false;
      }
      else
      {
        chbMonday.Checked = true;
        chbTuesday.Checked = true; 
        chbWednesday.Checked = true; 
        chbThursday.Checked = true; 
        chbFriday.Checked = true; 
        chbSaturday.Checked = false ;
        chbSunday.Checked = false ;
      }
      objConnection = null;
      objRef = null;
    }

    private void btnDaysSave_Click(object sender, EventArgs e)
    {
      InsertDays();
    }

    private void InsertDays()
    {
      clsBLDBConnection objConnection = new clsBLDBConnection();
      clsBLReferences objRef = new clsBLReferences(objConnection);
      DataView dv = new DataView();

      objRef.ReportRefrence = "PersonID" + _PersonID;

      try
      {
        objConnection.Connection_Open();
        dv = objRef.GetAll(2);
        objRef= SetBL(objRef);

        objConnection.Transaction_Begin();

        if (dv.Table.Rows.Count != 0)
        {
          if (!objRef.Update())
          {
            MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          else
          {
            MessageBox.Show("Update Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
        else
        {
          if (!objRef.Insert())
          {
            MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          else
          {
            MessageBox.Show("Update Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        objConnection.Transaction_ComRoll();
        objConnection.Connection_Close();

        objConnection = null;
        objRef = null;
      }
    }

    private clsBLReferences SetBL(clsBLReferences objRef)
    {
      objRef.Description = chbMonday.Checked ? "Y" : "N";
      objRef.ReportTitle1= chbTuesday.Checked ? "Y" : "N";
      objRef.ReportTitle2 = chbWednesday.Checked ? "Y" : "N";
      objRef.ReportTitle3 = chbThursday.Checked ? "Y" : "N";
      objRef.TreesFooter1 = chbFriday.Checked ? "Y" : "N";
      objRef.TreesFooter2 = chbSaturday.Checked ? "Y" : "N";
      objRef.ClientWebAddress = chbSunday.Checked ? "Y" : "N";


      return objRef;
    }
  }
}