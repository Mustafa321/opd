namespace OPD
{
    partial class frmStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStatus));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTotalRec = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.EnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Fees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.dgPanel = new System.Windows.Forms.DataGridView();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblPanelID = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblTotalRec);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.lblName);
            this.panel3.Controls.Add(this.txtName);
            this.panel3.Controls.Add(this.txtDescription);
            this.panel3.Controls.Add(this.lblDescription);
            this.panel3.Location = new System.Drawing.Point(1, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(586, 63);
            this.panel3.TabIndex = 50;
            this.panel3.Tag = "";
            // 
            // lblTotalRec
            // 
            this.lblTotalRec.AutoSize = true;
            this.lblTotalRec.Location = new System.Drawing.Point(37, 47);
            this.lblTotalRec.Name = "lblTotalRec";
            this.lblTotalRec.Size = new System.Drawing.Size(78, 13);
            this.lblTotalRec.TabIndex = 156;
            this.lblTotalRec.Tag = "display";
            this.lblTotalRec.Text = "Total Record : ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(308, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(5, 20);
            this.panel1.TabIndex = 147;
            this.panel1.Tag = "no";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(50, 11);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(44, 13);
            this.lblName.TabIndex = 154;
            this.lblName.Text = "Name : ";
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(97, 8);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(216, 20);
            this.txtName.TabIndex = 0;
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.Location = new System.Drawing.Point(383, 8);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(63, 20);
            this.txtDescription.TabIndex = 1;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(322, 11);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(69, 13);
            this.lblDescription.TabIndex = 151;
            this.lblDescription.Text = "Description : ";
            // 
            // EnteredBy
            // 
            this.EnteredBy.DataPropertyName = "EnteredBy";
            this.EnteredBy.HeaderText = "EnteredBy";
            this.EnteredBy.Name = "EnteredBy";
            this.EnteredBy.ReadOnly = true;
            this.EnteredBy.Visible = false;
            // 
            // ClientID
            // 
            this.ClientID.DataPropertyName = "ClientId";
            this.ClientID.HeaderText = "ClientID";
            this.ClientID.Name = "ClientID";
            this.ClientID.ReadOnly = true;
            this.ClientID.Visible = false;
            // 
            // EnteredOn
            // 
            this.EnteredOn.DataPropertyName = "EnteredOn";
            this.EnteredOn.HeaderText = "EnteredOn";
            this.EnteredOn.Name = "EnteredOn";
            this.EnteredOn.ReadOnly = true;
            this.EnteredOn.Visible = false;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "0";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "1";
            // 
            // Fees
            // 
            this.Fees.DataPropertyName = "DefaultFee";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Fees.DefaultCellStyle = dataGridViewCellStyle1;
            this.Fees.HeaderText = "Fees";
            this.Fees.Name = "Fees";
            this.Fees.ReadOnly = true;
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(1, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(586, 55);
            this.panel2.TabIndex = 53;
            this.panel2.Tag = "top";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.toolStrip1.Location = new System.Drawing.Point(387, -1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(197, 55);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            // 
            // dgPanel
            // 
            this.dgPanel.AllowUserToAddRows = false;
            this.dgPanel.AllowUserToDeleteRows = false;
            this.dgPanel.AllowUserToResizeRows = false;
            this.dgPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgPanel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPanel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgPanel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPanel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SerialNo,
            this.PanelID,
            this.PanelName,
            this.Fees,
            this.Active,
            this.EnteredBy,
            this.EnteredOn,
            this.ClientID});
            this.dgPanel.EnableHeadersVisualStyles = false;
            this.dgPanel.Location = new System.Drawing.Point(1, 122);
            this.dgPanel.MultiSelect = false;
            this.dgPanel.Name = "dgPanel";
            this.dgPanel.ReadOnly = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPanel.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgPanel.RowHeadersWidth = 25;
            this.dgPanel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPanel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPanel.Size = new System.Drawing.Size(586, 251);
            this.dgPanel.TabIndex = 51;
            this.dgPanel.TabStop = false;
            this.toolTip1.SetToolTip(this.dgPanel, "All add instructions");
            // 
            // SerialNo
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SerialNo.DefaultCellStyle = dataGridViewCellStyle3;
            this.SerialNo.HeaderText = "S#";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            this.SerialNo.Width = 50;
            // 
            // PanelID
            // 
            this.PanelID.DataPropertyName = "PanelID";
            this.PanelID.HeaderText = "PanelID";
            this.PanelID.Name = "PanelID";
            this.PanelID.ReadOnly = true;
            this.PanelID.Visible = false;
            // 
            // PanelName
            // 
            this.PanelName.DataPropertyName = "PanelName";
            this.PanelName.HeaderText = "Name";
            this.PanelName.Name = "PanelName";
            this.PanelName.ReadOnly = true;
            this.PanelName.Width = 305;
            // 
            // lblPanelID
            // 
            this.lblPanelID.AutoSize = true;
            this.lblPanelID.Location = new System.Drawing.Point(371, 278);
            this.lblPanelID.Name = "lblPanelID";
            this.lblPanelID.Size = new System.Drawing.Size(45, 13);
            this.lblPanelID.TabIndex = 52;
            this.lblPanelID.Text = "PanelID";
            this.lblPanelID.Visible = false;
            // 
            // frmStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 391);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgPanel);
            this.Controls.Add(this.lblPanelID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Status";
            this.Load += new System.EventHandler(this.frmStatus_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPanel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTotalRec;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredOn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fees;
        private System.Windows.Forms.ToolStripButton miRefresh;
        private System.Windows.Forms.ToolStripButton miSave;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton miExit;
        private System.Windows.Forms.DataGridView dgPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PanelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PanelName;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblPanelID;
    }
}