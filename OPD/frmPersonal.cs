using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmPersonal : Form
    {
        private frmOPD MainOPD = null;

        public frmPersonal()
        {
            InitializeComponent();
        }

        public frmPersonal(frmOPD Frm)
        {
            InitializeComponent();
            this.MainOPD = Frm;
        }

        private void frmPersonal_Load(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            SComponents objComp = new SComponents();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);

            objComp.ApplyStyleToControls(this);
            objConnection.Connection_Open();
            objComp.FillComboBox(cboBloodGroup, objPersonal.GetAll(2), "BloodGroup", true);
            FillGridView(objConnection);
            objConnection.Connection_Close();

            cboTitle.SelectedIndex = 0;
            cboSpecialty.SelectedIndex = 0;
            dgPersonal.ClearSelection();

            this.Text = this.Text + "(" + clsSharedVariables.UserName + ")";

            objConnection = null;
            objComp = null;
            objPersonal = null;
        }

        private clsBLPersonal SetBLValues(clsBLPersonal objPersonal)
        {
            objPersonal.Title = cboTitle.SelectedItem.ToString();
            objPersonal.name = clsSharedVariables.InItCaps(txtName.Text.Trim());
            if (rbMale.Checked)
            {
                objPersonal.Gender = "M";
            }
            else
            {
                objPersonal.Gender = "F";
            }

            if (rbSingle.Checked)
            {
                objPersonal.MaritalStatus = "S";
            }
            else
            {
                objPersonal.MaritalStatus = "M";
            }
            objPersonal.DOB = dtpDateBirth.Value.Date.ToString("dd/MM/yyyy");
            objPersonal.PhoneNo = txtPhone.Text.Trim();
            objPersonal.CellNo = txtCell.Text.Trim();
            if (txtNIC.Text.Length != 14)
            {
                objPersonal.NIC = txtNIC.Text.Trim();
            }
            objPersonal.Address = clsSharedVariables.InItCaps(txtAddress.Text.Trim());
            if (!cboBloodGroup.Text.Equals("Select"))
            {
                objPersonal.BloodGroup = cboBloodGroup.Text;
            }
            objPersonal.Designation = clsSharedVariables.InItCaps(txtDesignation.Text.Trim());
            objPersonal.Rank = clsSharedVariables.InItCaps(txtRank.Text.Trim());
            objPersonal.Education = clsSharedVariables.InItCaps(txtEducation.Text.Trim());
            objPersonal.LoginID = txtLoginID.Text.Trim();
            objPersonal.Pasword = txtPasword.Text.Trim();
            objPersonal.RCPasword = txtCPassword.Text.Trim();
            objPersonal.Hint = txtHint.Text.Trim();
            if (chbActive.Checked)
            {
                objPersonal.Active = "1";
            }
            else
            {
                objPersonal.Active = "0";
            }
            objPersonal.RegistrationCode = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPersonal.EnteredBy = clsSharedVariables.UserID;
            objPersonal.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
            objPersonal.ClientID = clsSharedVariables.ClientID;
            if (cboSpecialty.SelectedIndex != 0)
            {
                objPersonal.Specialty = cboSpecialty.Text;
            }
            //if (rbConsultant.Checked)
            //{
            //    objPersonal.Type = "C";
            //}
            //else
            //{
                objPersonal.Type = "O";
            //}
            if (!txtEmail.Text.Equals(""))
            {
                objPersonal.Email = txtEmail.Text;
            }
            if (!txtEmailText.Text.Equals(""))
            {
                objPersonal.EmailText = txtEmailText.Text.Trim();
            }
            objPersonal.maxSlot = nudmaxSlot.Value.ToString();
            objPersonal.Charges = nudCharges.Value.ToString();
            objPersonal.RevisitCharges = nudRevisitCharges.Value.ToString();
            objPersonal.RevisitDays = nudRevisitDays.Value.ToString();
            objPersonal.Expire = clsSharedVariables.ExpireDate;
            return objPersonal;
        }

        private void Insert()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
            try
            {
                objPersonal = SetBLValues(objPersonal);
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objPersonal.Insert())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Personal Information Registered Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPersonal.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPersonal = null;
                objConnection = null;
            }
        }

        private void UpdateData()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);
            try
            {
                objPersonal.PersonID = this.lblPersonID.Text;
                objPersonal = SetBLValues(objPersonal);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objPersonal.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillGridView(objConnection);
                    objConnection.Connection_Close();
                    ClearField();
                    MessageBox.Show("Pesonal information updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objPersonal.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objPersonal = null;
                objConnection = null;
            }
        }

        private void FillGridView(clsBLDBConnection objConnection)
        {
            clsBLPersonal objPersonal = new clsBLPersonal(objConnection);

            DataView dvPersonal = objPersonal.GetAll(1);
            dvPersonal.Sort = "Name";
            this.dgPersonal.DataSource = dvPersonal;

            objPersonal = null;
            dvPersonal = null;
        }

        private void ClearField()
        {
            cboTitle.SelectedIndex = 0;
            txtName.Text = "";
            dtpDateBirth.Value = DateTime.Now.Date;
            rbMale.Checked = true;
            rbSingle.Checked = true;
            cboBloodGroup.SelectedIndex = 0;
            txtPhone.Text = "";
            txtCell.Text = "";
            txtNIC.Text = "";
            txtEducation.Text = "";
            txtDesignation.Text = "";
            txtRank.Text = "";
            txtAddress.Text = "";
            txtLoginID.Text = "";
            txtPasword.Text = "";
            txtCPassword.Text = "";
            txtHint.Text = "";
            txtEmail.Text = "";
            chbActive.Checked = true;
            panel1.Enabled = true;
            panel2.Enabled = true;
            dgPersonal.ClearSelection();
            cboTitle.Focus();
            cboSpecialty.SelectedIndex = 0;
            rbConsultant.Checked = true;
            this.lblPersonID.Text = "";
            this.miSave.Tag = "Save";
            this.miSave.Text = "Save";
            txtEmailText.Text = "";
            nudmaxSlot.Value = 20;
            nudCharges.Value = 500;
            nudRevisitCharges.Value = 500;
            nudRevisitDays.Value = 10;
        }

        private void FillForm(int rowIndex)
        {
            this.miSave.Tag = "Update";

            lblPersonID.Text = dgPersonal.Rows[rowIndex].Cells["PersonID"].Value.ToString();
            cboTitle.Text = dgPersonal.Rows[rowIndex].Cells["Title"].Value.ToString();
            txtName.Text = clsSharedVariables.InItCaps(dgPersonal.Rows[rowIndex].Cells["PersonName"].Value.ToString());
            if (dgPersonal.Rows[rowIndex].Cells["Gender"].Value.ToString().Equals("M"))
            {
                rbMale.Checked = true;
            }
            else
            {
                rbFemale.Checked = true;
            }

            if (dgPersonal.Rows[rowIndex].Cells["MaritalStatus"].Value.ToString().Equals("S"))
            {
                rbSingle.Checked = true;
            }
            else
            {
                rbMarried.Checked = true;
            }
            cboBloodGroup.Text = dgPersonal.Rows[rowIndex].Cells["BloodGroup"].Value.ToString();
            dtpDateBirth.Value = Convert.ToDateTime(dgPersonal.Rows[rowIndex].Cells["DOB"].Value.ToString());
            txtPhone.Text = dgPersonal.Rows[rowIndex].Cells["PhoneNo"].Value.ToString();
            txtCell.Text = dgPersonal.Rows[rowIndex].Cells["CellNo"].Value.ToString();
            txtNIC.Text = dgPersonal.Rows[rowIndex].Cells["NIC"].Value.ToString();
            txtAddress.Text = clsSharedVariables.InItCaps(dgPersonal.Rows[rowIndex].Cells["Address"].Value.ToString());
            txtDesignation.Text = clsSharedVariables.InItCaps(dgPersonal.Rows[rowIndex].Cells["Designation"].Value.ToString());
            txtRank.Text = clsSharedVariables.InItCaps(dgPersonal.Rows[rowIndex].Cells["Rank"].Value.ToString());
            txtEducation.Text = clsSharedVariables.InItCaps(dgPersonal.Rows[rowIndex].Cells["Education"].Value.ToString());
            txtLoginID.Text = dgPersonal.Rows[rowIndex].Cells["LoginID"].Value.ToString();
            txtPasword.Text = dgPersonal.Rows[rowIndex].Cells["Pasword"].Value.ToString();
            txtCPassword.Text = dgPersonal.Rows[rowIndex].Cells["Pasword"].Value.ToString();
            txtHint.Text = dgPersonal.Rows[rowIndex].Cells["Hint"].Value.ToString();
            if (dgPersonal.Rows[rowIndex].Cells["Active"].Value.ToString().Equals("0"))
            {
                chbActive.Checked = false;
            }
            else
            {
                chbActive.Checked = true;
            }
            cboSpecialty.Text = dgPersonal.Rows[rowIndex].Cells["Specialty"].Value.ToString();
            if (dgPersonal.Rows[rowIndex].Cells["Type"].Value.ToString().Equals("C"))
            {
                rbConsultant.Checked = true;
            }
            else
            {
                rbOperator.Checked = true;
            }
            txtEmail.Text = dgPersonal.Rows[rowIndex].Cells["Email"].Value.ToString();

            txtEmailText.Text = dgPersonal.Rows[rowIndex].Cells["EmailText"].Value.ToString();
            nudmaxSlot.Value= dgPersonal.Rows[rowIndex].Cells["MaxSlot"].Value.ToString().Equals("")? 20: Convert.ToDecimal(dgPersonal.Rows[rowIndex].Cells["MaxSlot"].Value.ToString());
            nudCharges.Value = dgPersonal.Rows[rowIndex].Cells["Charges"].Value.ToString().Equals("") ? 500 : Convert.ToDecimal(dgPersonal.Rows[rowIndex].Cells["Charges"].Value.ToString());
            nudRevisitCharges.Value = dgPersonal.Rows[rowIndex].Cells["RevisitCharges"].Value.ToString().Equals("") ? 400 : Convert.ToDecimal(dgPersonal.Rows[rowIndex].Cells["RevisitCharges"].Value.ToString());
            nudRevisitDays.Value = dgPersonal.Rows[rowIndex].Cells["RevisitDays"].Value.ToString().Equals("") ? 20 : Convert.ToDecimal(dgPersonal.Rows[rowIndex].Cells["RevisitDays"].Value.ToString());
        }

        private void dgPersonal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                FillForm(e.RowIndex);
            }
        }

        private void cboTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboTitle.Text.Equals("Miss"))
            {
                rbFemale.Checked = true;
                rbSingle.Checked = true;
                panel1.Enabled = false;
                panel2.Enabled = false;
            }
            else if (cboTitle.Text.Equals("Mrs."))
            {
                rbFemale.Checked = true;
                rbMarried.Checked = true;
                panel1.Enabled = false;
                panel2.Enabled = false;
            }
            else if (cboTitle.Text.Equals("Mr."))
            {
                panel1.Enabled = true;
                panel2.Enabled = true;
                rbMale.Checked = true;
                rbSingle.Checked = true;
            }
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void dtpDateBirth_Leave(object sender, EventArgs e)
        {
            int Age = 0;
            Age = DateTime.Now.Year - dtpDateBirth.Value.Year;
            txtAge.Text = Age.ToString();
        }

        private void txtAge_Leave(object sender, EventArgs e)
        {
            Validation objValid = new Validation();

            string str = "";
            if (objValid.IsInteger(txtAge.Text) && Convert.ToInt16(txtAge.Text) < 250)
            {
                str = DateTime.Now.Date.ToString().Split('/')[0].ToString() + "/" + DateTime.Now.Date.ToString().Split('/')[1].ToString() + "/" + (Convert.ToInt16(DateTime.Now.Date.ToString().Split('/')[2].ToString().Split(' ')[0].ToString()) - Convert.ToInt16(txtAge.Text.Trim())) + "";
                dtpDateBirth.Value = Convert.ToDateTime(str);
            }
            else
            {
                txtAge.Text = "";
            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if (this.miSave.Tag.Equals("Save"))
            {
                Insert();
            }
            else
            {
                UpdateData();
            }
        }

        private void miCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPersonal_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            ClearField();
        }
    }
}