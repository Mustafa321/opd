namespace OPD
{
    partial class frmMedicineSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMedicineSelection));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgSelectedList = new System.Windows.Forms.DataGridView();
            this.MedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedicineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedicineDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsDelete = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSelectDosage = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtDosage = new System.Windows.Forms.TextBox();
            this.txtSearchMedicine = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnAddFromList = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnChangeDosage = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgList = new System.Windows.Forms.DataGridView();
            this.MedicineId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InfoImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmsMedInfo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.alternateMedicineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAgeWiseDosage = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteMedicine = new System.Windows.Forms.ToolStripMenuItem();
            this.lblMedicine = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlEngDosage = new System.Windows.Forms.Panel();
            this.cboDosage = new System.Windows.Forms.ComboBox();
            this.cboEngPeriod = new System.Windows.Forms.ComboBox();
            this.nudEngPeriodCount = new System.Windows.Forms.NumericUpDown();
            this.lblNewMed = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlAlternateMed = new System.Windows.Forms.Panel();
            this.dgAlterMed = new System.Windows.Forms.DataGridView();
            this.AlterMedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlterGenericID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlterMedicineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlterNatureID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlterMedGeneric = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlterMedNature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAlterMedHeading = new System.Windows.Forms.Label();
            this.btnAlterMedClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgSelectedList)).BeginInit();
            this.cmsDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.cmsMedInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlEngDosage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEngPeriodCount)).BeginInit();
            this.pnlAlternateMed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAlterMed)).BeginInit();
            this.SuspendLayout();
            // 
            // dgSelectedList
            // 
            this.dgSelectedList.AllowUserToAddRows = false;
            this.dgSelectedList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.dgSelectedList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgSelectedList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSelectedList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgSelectedList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSelectedList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MedID,
            this.Mode,
            this.MedicineName,
            this.MedicineDosage});
            this.dgSelectedList.ContextMenuStrip = this.cmsDelete;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSelectedList.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgSelectedList.EnableHeadersVisualStyles = false;
            this.dgSelectedList.Location = new System.Drawing.Point(369, 96);
            this.dgSelectedList.MultiSelect = false;
            this.dgSelectedList.Name = "dgSelectedList";
            this.dgSelectedList.ReadOnly = true;
            this.dgSelectedList.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro;
            this.dgSelectedList.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgSelectedList.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dgSelectedList.RowTemplate.Height = 35;
            this.dgSelectedList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSelectedList.Size = new System.Drawing.Size(644, 608);
            this.dgSelectedList.TabIndex = 1;
            this.dgSelectedList.TabStop = false;
            this.dgSelectedList.Tag = "self";
            this.toolTip1.SetToolTip(this.dgSelectedList, "Selected Medicine and its Dosage");
            this.dgSelectedList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSelectedList_CellClick);
            this.dgSelectedList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSelectedList_CellContentClick);
            this.dgSelectedList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgSelectedList_MouseDown);
            // 
            // MedID
            // 
            this.MedID.HeaderText = "MedicineID";
            this.MedID.Name = "MedID";
            this.MedID.ReadOnly = true;
            this.MedID.Visible = false;
            // 
            // Mode
            // 
            this.Mode.HeaderText = "Mode";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Visible = false;
            // 
            // MedicineName
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedicineName.DefaultCellStyle = dataGridViewCellStyle3;
            this.MedicineName.HeaderText = "Medicine Name";
            this.MedicineName.Name = "MedicineName";
            this.MedicineName.ReadOnly = true;
            // 
            // MedicineDosage
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedicineDosage.DefaultCellStyle = dataGridViewCellStyle4;
            this.MedicineDosage.HeaderText = "Dosage";
            this.MedicineDosage.Name = "MedicineDosage";
            this.MedicineDosage.ReadOnly = true;
            // 
            // cmsDelete
            // 
            this.cmsDelete.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDelete});
            this.cmsDelete.Name = "cmsDelete";
            this.cmsDelete.Size = new System.Drawing.Size(160, 26);
            this.cmsDelete.Opening += new System.ComponentModel.CancelEventHandler(this.cmsDelete_Opening);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(159, 22);
            this.miDelete.Text = "Delete Medicine";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // btnSelectDosage
            // 
            this.btnSelectDosage.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSelectDosage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelectDosage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectDosage.Location = new System.Drawing.Point(694, 6);
            this.btnSelectDosage.Name = "btnSelectDosage";
            this.btnSelectDosage.Size = new System.Drawing.Size(60, 55);
            this.btnSelectDosage.TabIndex = 10;
            this.btnSelectDosage.Tag = "dosage";
            this.btnSelectDosage.Text = "&Dosage";
            this.btnSelectDosage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnSelectDosage, "Dosgae Selection(Alt+D)");
            this.btnSelectDosage.UseVisualStyleBackColor = false;
            this.btnSelectDosage.Click += new System.EventHandler(this.btnSelectDosage_Click);
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(62, 23);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(192, 26);
            this.txtName.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtName, "Medicine Name for Insertion, Updation and Selection");
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
            // 
            // txtDosage
            // 
            this.txtDosage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDosage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDosage.Location = new System.Drawing.Point(322, 16);
            this.txtDosage.Name = "txtDosage";
            this.txtDosage.ReadOnly = true;
            this.txtDosage.Size = new System.Drawing.Size(301, 22);
            this.txtDosage.TabIndex = 1;
            this.txtDosage.Tag = "urdu";
            this.txtDosage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.txtDosage, "Dosage for Insertion, Updation and Selection");
            this.txtDosage.Click += new System.EventHandler(this.txtDosage_Click);
            // 
            // txtSearchMedicine
            // 
            this.txtSearchMedicine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchMedicine.Location = new System.Drawing.Point(5, 96);
            this.txtSearchMedicine.Name = "txtSearchMedicine";
            this.txtSearchMedicine.Size = new System.Drawing.Size(285, 20);
            this.txtSearchMedicine.TabIndex = 2;
            this.toolTip1.SetToolTip(this.txtSearchMedicine, "Search Medicine");
            this.txtSearchMedicine.TextChanged += new System.EventHandler(this.txtSearchMedicine_TextChanged);
            // 
            // btnAddFromList
            // 
            this.btnAddFromList.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnAddFromList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddFromList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFromList.Location = new System.Drawing.Point(627, 6);
            this.btnAddFromList.Name = "btnAddFromList";
            this.btnAddFromList.Size = new System.Drawing.Size(66, 55);
            this.btnAddFromList.TabIndex = 151;
            this.btnAddFromList.Tag = "dosage";
            this.btnAddFromList.Text = "Master Medicine";
            this.btnAddFromList.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnAddFromList, "Dosgae Selection(Alt+D)");
            this.btnAddFromList.UseVisualStyleBackColor = false;
            this.btnAddFromList.Click += new System.EventHandler(this.btnAddFromList_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClear.Location = new System.Drawing.Point(816, 6);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(60, 55);
            this.btnClear.TabIndex = 2;
            this.btnClear.Tag = "";
            this.btnClear.Text = "&Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnClear, "Clear Seleted Medicine and Dosage(Alt+C)");
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSave.Location = new System.Drawing.Point(755, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 55);
            this.btnSave.TabIndex = 3;
            this.btnSave.Tag = "&Save";
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnSave, "Save and Update New Medicine(Alt+S)");
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnChangeDosage
            // 
            this.btnChangeDosage.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnChangeDosage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnChangeDosage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeDosage.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeDosage.Image")));
            this.btnChangeDosage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnChangeDosage.Location = new System.Drawing.Point(291, 331);
            this.btnChangeDosage.Name = "btnChangeDosage";
            this.btnChangeDosage.Size = new System.Drawing.Size(77, 102);
            this.btnChangeDosage.TabIndex = 15;
            this.btnChangeDosage.Text = "Add Double Dosage For Medicine";
            this.btnChangeDosage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnChangeDosage, "For multiple Dosage Selection");
            this.btnChangeDosage.UseVisualStyleBackColor = false;
            this.btnChangeDosage.Click += new System.EventHandler(this.btnChangeDosage_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancel.Location = new System.Drawing.Point(953, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(60, 55);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Tag = "notset";
            this.btnCancel.Text = "&Exit";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnCancel, "Cance Selection(Alt+E)");
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSelect.Location = new System.Drawing.Point(291, 166);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(77, 102);
            this.btnSelect.TabIndex = 8;
            this.btnSelect.Text = "Add Medicine For Patient";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnSelect, "For Single Dosage Selection");
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnOK.Location = new System.Drawing.Point(892, 10);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(60, 55);
            this.btnOK.TabIndex = 0;
            this.btnOK.Tag = "notset";
            this.btnOK.Text = "&OK";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnOK, "Medicine and Dosage selction for patient(Alt+O)");
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dgList
            // 
            this.dgList.AllowUserToAddRows = false;
            this.dgList.AllowUserToDeleteRows = false;
            this.dgList.AllowUserToResizeColumns = false;
            this.dgList.AllowUserToResizeRows = false;
            this.dgList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MedicineId,
            this.MedName,
            this.Dosage,
            this.MedRefID,
            this.InfoImage});
            this.dgList.ContextMenuStrip = this.cmsMedInfo;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgList.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgList.EnableHeadersVisualStyles = false;
            this.dgList.Location = new System.Drawing.Point(5, 119);
            this.dgList.MultiSelect = false;
            this.dgList.Name = "dgList";
            this.dgList.ReadOnly = true;
            this.dgList.RowHeadersVisible = false;
            this.dgList.RowTemplate.Height = 35;
            this.dgList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgList.Size = new System.Drawing.Size(285, 584);
            this.dgList.TabIndex = 152;
            this.dgList.TabStop = false;
            this.dgList.Tag = "self";
            this.dgList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgList_CellClick);
            this.dgList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgList_CellDoubleClick);
            this.dgList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgList_CellFormatting);
            this.dgList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgList_MouseDown);
            // 
            // MedicineId
            // 
            this.MedicineId.DataPropertyName = "MedicineID";
            this.MedicineId.HeaderText = "MedicineID";
            this.MedicineId.Name = "MedicineId";
            this.MedicineId.ReadOnly = true;
            this.MedicineId.Visible = false;
            // 
            // MedName
            // 
            this.MedName.DataPropertyName = "Name";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedName.DefaultCellStyle = dataGridViewCellStyle8;
            this.MedName.FillWeight = 179.6954F;
            this.MedName.HeaderText = "Medicine Name";
            this.MedName.Name = "MedName";
            this.MedName.ReadOnly = true;
            // 
            // Dosage
            // 
            this.Dosage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Dosage.DataPropertyName = "Dosage";
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dosage.DefaultCellStyle = dataGridViewCellStyle9;
            this.Dosage.HeaderText = "Dosage";
            this.Dosage.Name = "Dosage";
            this.Dosage.ReadOnly = true;
            this.Dosage.Visible = false;
            // 
            // MedRefID
            // 
            this.MedRefID.DataPropertyName = "MedIDRef";
            this.MedRefID.HeaderText = "MedRefID";
            this.MedRefID.Name = "MedRefID";
            this.MedRefID.ReadOnly = true;
            this.MedRefID.Visible = false;
            // 
            // InfoImage
            // 
            this.InfoImage.FillWeight = 20.30457F;
            this.InfoImage.HeaderText = "";
            this.InfoImage.Name = "InfoImage";
            this.InfoImage.ReadOnly = true;
            // 
            // cmsMedInfo
            // 
            this.cmsMedInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMoreInfo,
            this.alternateMedicineToolStripMenuItem,
            this.miAgeWiseDosage,
            this.DeleteMedicine});
            this.cmsMedInfo.Name = "cmsMedInfo";
            this.cmsMedInfo.Size = new System.Drawing.Size(175, 92);
            // 
            // miMoreInfo
            // 
            this.miMoreInfo.Name = "miMoreInfo";
            this.miMoreInfo.Size = new System.Drawing.Size(174, 22);
            this.miMoreInfo.Text = "More Information";
            this.miMoreInfo.Click += new System.EventHandler(this.miMoreInfo_Click);
            // 
            // alternateMedicineToolStripMenuItem
            // 
            this.alternateMedicineToolStripMenuItem.Name = "alternateMedicineToolStripMenuItem";
            this.alternateMedicineToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.alternateMedicineToolStripMenuItem.Text = "Alternate Medicine";
            this.alternateMedicineToolStripMenuItem.Click += new System.EventHandler(this.alternateMedicineToolStripMenuItem_Click);
            // 
            // miAgeWiseDosage
            // 
            this.miAgeWiseDosage.Name = "miAgeWiseDosage";
            this.miAgeWiseDosage.Size = new System.Drawing.Size(174, 22);
            this.miAgeWiseDosage.Text = "Age Wise Dosage";
            this.miAgeWiseDosage.Click += new System.EventHandler(this.miAgeWiseDosage_Click);
            // 
            // DeleteMedicine
            // 
            this.DeleteMedicine.Name = "DeleteMedicine";
            this.DeleteMedicine.Size = new System.Drawing.Size(174, 22);
            this.DeleteMedicine.Text = "Delete";
            this.DeleteMedicine.Click += new System.EventHandler(this.DeleteMedicine_Click);
            // 
            // lblMedicine
            // 
            this.lblMedicine.AutoSize = true;
            this.lblMedicine.Location = new System.Drawing.Point(3, 27);
            this.lblMedicine.Name = "lblMedicine";
            this.lblMedicine.Size = new System.Drawing.Size(56, 13);
            this.lblMedicine.TabIndex = 17;
            this.lblMedicine.Tag = "self";
            this.lblMedicine.Text = "Medicine :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pnlEngDosage);
            this.panel1.Controls.Add(this.btnAddFromList);
            this.panel1.Controls.Add(this.lblNewMed);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.btnSelectDosage);
            this.panel1.Controls.Add(this.lblMedicine);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.txtDosage);
            this.panel1.Location = new System.Drawing.Point(5, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(882, 69);
            this.panel1.TabIndex = 19;
            this.panel1.Tag = "self";
            // 
            // pnlEngDosage
            // 
            this.pnlEngDosage.BackColor = System.Drawing.Color.Transparent;
            this.pnlEngDosage.Controls.Add(this.cboDosage);
            this.pnlEngDosage.Controls.Add(this.cboEngPeriod);
            this.pnlEngDosage.Controls.Add(this.nudEngPeriodCount);
            this.pnlEngDosage.Location = new System.Drawing.Point(322, 17);
            this.pnlEngDosage.Name = "pnlEngDosage";
            this.pnlEngDosage.Size = new System.Drawing.Size(301, 29);
            this.pnlEngDosage.TabIndex = 154;
            // 
            // cboDosage
            // 
            this.cboDosage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDosage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDosage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDosage.FormattingEnabled = true;
            this.cboDosage.Location = new System.Drawing.Point(2, 4);
            this.cboDosage.Name = "cboDosage";
            this.cboDosage.Size = new System.Drawing.Size(125, 21);
            this.cboDosage.TabIndex = 150;
            this.cboDosage.Validating += new System.ComponentModel.CancelEventHandler(this.cboDosage_Validating);
            // 
            // cboEngPeriod
            // 
            this.cboEngPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEngPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEngPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEngPeriod.FormattingEnabled = true;
            this.cboEngPeriod.Items.AddRange(new object[] {
            "Select",
            "Days",
            "Weeks",
            "Months"});
            this.cboEngPeriod.Location = new System.Drawing.Point(176, 4);
            this.cboEngPeriod.Name = "cboEngPeriod";
            this.cboEngPeriod.Size = new System.Drawing.Size(122, 21);
            this.cboEngPeriod.TabIndex = 153;
            // 
            // nudEngPeriodCount
            // 
            this.nudEngPeriodCount.Location = new System.Drawing.Point(133, 5);
            this.nudEngPeriodCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEngPeriodCount.Name = "nudEngPeriodCount";
            this.nudEngPeriodCount.Size = new System.Drawing.Size(37, 20);
            this.nudEngPeriodCount.TabIndex = 152;
            this.nudEngPeriodCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNewMed
            // 
            this.lblNewMed.AutoSize = true;
            this.lblNewMed.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblNewMed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNewMed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewMed.Location = new System.Drawing.Point(-1, -1);
            this.lblNewMed.Name = "lblNewMed";
            this.lblNewMed.Size = new System.Drawing.Size(128, 17);
            this.lblNewMed.TabIndex = 149;
            this.lblNewMed.Tag = "self";
            this.lblNewMed.Text = "Add New Medicine";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 148;
            this.label1.Tag = "self";
            this.label1.Text = "Dosage  :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(253, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 26);
            this.panel2.TabIndex = 147;
            this.panel2.Tag = "no";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 22);
            this.label2.TabIndex = 150;
            this.label2.Tag = "self";
            this.label2.Text = "Preferred Medicine List ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(369, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(247, 22);
            this.label3.TabIndex = 151;
            this.label3.Tag = "self";
            this.label3.Text = "Medicine Selected for Patient";
            // 
            // pnlAlternateMed
            // 
            this.pnlAlternateMed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAlternateMed.Controls.Add(this.dgAlterMed);
            this.pnlAlternateMed.Controls.Add(this.lblAlterMedHeading);
            this.pnlAlternateMed.Controls.Add(this.btnAlterMedClose);
            this.pnlAlternateMed.Location = new System.Drawing.Point(374, 133);
            this.pnlAlternateMed.Name = "pnlAlternateMed";
            this.pnlAlternateMed.Size = new System.Drawing.Size(578, 231);
            this.pnlAlternateMed.TabIndex = 153;
            this.pnlAlternateMed.Visible = false;
            // 
            // dgAlterMed
            // 
            this.dgAlterMed.AllowDrop = true;
            this.dgAlterMed.AllowUserToAddRows = false;
            this.dgAlterMed.AllowUserToDeleteRows = false;
            this.dgAlterMed.AllowUserToOrderColumns = true;
            this.dgAlterMed.AllowUserToResizeColumns = false;
            this.dgAlterMed.AllowUserToResizeRows = false;
            this.dgAlterMed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAlterMed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAlterMed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlterMedName,
            this.AlterGenericID,
            this.AlterMedicineID,
            this.AlterNatureID,
            this.AlterMedGeneric,
            this.AlterMedNature});
            this.dgAlterMed.Location = new System.Drawing.Point(1, 23);
            this.dgAlterMed.Name = "dgAlterMed";
            this.dgAlterMed.Size = new System.Drawing.Size(574, 205);
            this.dgAlterMed.TabIndex = 151;
            // 
            // AlterMedName
            // 
            this.AlterMedName.DataPropertyName = "med_name";
            this.AlterMedName.HeaderText = "Name";
            this.AlterMedName.Name = "AlterMedName";
            // 
            // AlterGenericID
            // 
            this.AlterGenericID.DataPropertyName = "GenericID";
            this.AlterGenericID.HeaderText = "GenericID";
            this.AlterGenericID.Name = "AlterGenericID";
            this.AlterGenericID.Visible = false;
            // 
            // AlterMedicineID
            // 
            this.AlterMedicineID.DataPropertyName = "medicineid";
            this.AlterMedicineID.HeaderText = "medicineid";
            this.AlterMedicineID.Name = "AlterMedicineID";
            this.AlterMedicineID.Visible = false;
            // 
            // AlterNatureID
            // 
            this.AlterNatureID.DataPropertyName = "natureid";
            this.AlterNatureID.HeaderText = "natureid";
            this.AlterNatureID.Name = "AlterNatureID";
            this.AlterNatureID.Visible = false;
            // 
            // AlterMedGeneric
            // 
            this.AlterMedGeneric.DataPropertyName = "GCName";
            this.AlterMedGeneric.HeaderText = "Generic";
            this.AlterMedGeneric.Name = "AlterMedGeneric";
            // 
            // AlterMedNature
            // 
            this.AlterMedNature.DataPropertyName = "NatureName";
            this.AlterMedNature.HeaderText = "Nature";
            this.AlterMedNature.Name = "AlterMedNature";
            // 
            // lblAlterMedHeading
            // 
            this.lblAlterMedHeading.AutoSize = true;
            this.lblAlterMedHeading.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblAlterMedHeading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAlterMedHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlterMedHeading.Location = new System.Drawing.Point(224, 3);
            this.lblAlterMedHeading.Name = "lblAlterMedHeading";
            this.lblAlterMedHeading.Size = new System.Drawing.Size(129, 17);
            this.lblAlterMedHeading.TabIndex = 150;
            this.lblAlterMedHeading.Tag = "self";
            this.lblAlterMedHeading.Text = "Alternate Medicine";
            // 
            // btnAlterMedClose
            // 
            this.btnAlterMedClose.BackgroundImage = global::OPD.Properties.Resources.Cancel_15;
            this.btnAlterMedClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAlterMedClose.Location = new System.Drawing.Point(0, 0);
            this.btnAlterMedClose.Name = "btnAlterMedClose";
            this.btnAlterMedClose.Size = new System.Drawing.Size(23, 23);
            this.btnAlterMedClose.TabIndex = 1;
            this.btnAlterMedClose.UseVisualStyleBackColor = true;
            this.btnAlterMedClose.Click += new System.EventHandler(this.btnAlterMedClose_Click);
            // 
            // frmMedicineSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(1015, 704);
            this.Controls.Add(this.dgList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlAlternateMed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtSearchMedicine);
            this.Controls.Add(this.dgSelectedList);
            this.Controls.Add(this.btnChangeDosage);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmMedicineSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Medicine Selection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMedicineSelection_FormClosing);
            this.Load += new System.EventHandler(this.frmMedicineSelection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgSelectedList)).EndInit();
            this.cmsDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.cmsMedInfo.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlEngDosage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudEngPeriodCount)).EndInit();
            this.pnlAlternateMed.ResumeLayout(false);
            this.pnlAlternateMed.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAlterMed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSelectDosage;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtDosage;
        public System.Windows.Forms.DataGridView dgSelectedList;
        private System.Windows.Forms.TextBox txtSearchMedicine;
        private System.Windows.Forms.Button btnChangeDosage;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblMedicine;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip cmsDelete;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
      private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNewMed;
      private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboDosage;
        private System.Windows.Forms.Button btnAddFromList;
        private System.Windows.Forms.DataGridView dgList;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedicineId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dosage;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedRefID;
        private System.Windows.Forms.DataGridViewImageColumn InfoImage;
        private System.Windows.Forms.ContextMenuStrip cmsMedInfo;
        private System.Windows.Forms.ToolStripMenuItem miMoreInfo;
        private System.Windows.Forms.ToolStripMenuItem alternateMedicineToolStripMenuItem;
        private System.Windows.Forms.Panel pnlAlternateMed;
        private System.Windows.Forms.Button btnAlterMedClose;
        private System.Windows.Forms.DataGridView dgAlterMed;
        private System.Windows.Forms.Label lblAlterMedHeading;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterMedName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterGenericID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterMedicineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterNatureID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterMedGeneric;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlterMedNature;
        private System.Windows.Forms.Panel pnlEngDosage;
        private System.Windows.Forms.ComboBox cboEngPeriod;
        private System.Windows.Forms.NumericUpDown nudEngPeriodCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedicineName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedicineDosage;
        private System.Windows.Forms.ToolStripMenuItem miAgeWiseDosage;
        private System.Windows.Forms.ToolStripMenuItem DeleteMedicine;
    }
}