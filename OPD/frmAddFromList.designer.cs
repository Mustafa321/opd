namespace OPD
{
  partial class frmAddFromList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddFromList));
            this.dgSelectedMed = new System.Windows.Forms.DataGridView();
            this.SelectMedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectMedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefMedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectedMedDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectEngDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgAllMed = new System.Windows.Forms.DataGridView();
            this.AllMedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllMedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSelection = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAllMedSerach = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlEngDosage = new System.Windows.Forms.Panel();
            this.btnEngDosageAdd = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDosage = new System.Windows.Forms.ComboBox();
            this.cboEngPeriod = new System.Windows.Forms.ComboBox();
            this.nudEngPeriodCount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgSelectedMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllMed)).BeginInit();
            this.cmsSelection.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.pnlEngDosage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEngPeriodCount)).BeginInit();
            this.SuspendLayout();
            // 
            // dgSelectedMed
            // 
            this.dgSelectedMed.AllowUserToAddRows = false;
            this.dgSelectedMed.AllowUserToDeleteRows = false;
            this.dgSelectedMed.AllowUserToOrderColumns = true;
            this.dgSelectedMed.AllowUserToResizeColumns = false;
            this.dgSelectedMed.AllowUserToResizeRows = false;
            this.dgSelectedMed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSelectedMed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgSelectedMed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgSelectedMed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelectMedID,
            this.SelectMedName,
            this.RefMedID,
            this.Mode,
            this.SelectedMedDosage,
            this.SelectEngDosage});
            this.dgSelectedMed.Location = new System.Drawing.Point(375, 112);
            this.dgSelectedMed.MultiSelect = false;
            this.dgSelectedMed.Name = "dgSelectedMed";
            this.dgSelectedMed.ReadOnly = true;
            this.dgSelectedMed.RowHeadersWidth = 35;
            this.dgSelectedMed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSelectedMed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSelectedMed.Size = new System.Drawing.Size(547, 571);
            this.dgSelectedMed.TabIndex = 0;
            this.dgSelectedMed.Sorted += new System.EventHandler(this.dgSelectedMed_Sorted);
            // 
            // SelectMedID
            // 
            this.SelectMedID.DataPropertyName = "MedicineID";
            this.SelectMedID.HeaderText = "MedID";
            this.SelectMedID.Name = "SelectMedID";
            this.SelectMedID.ReadOnly = true;
            this.SelectMedID.Visible = false;
            // 
            // SelectMedName
            // 
            this.SelectMedName.DataPropertyName = "Name";
            this.SelectMedName.FillWeight = 50F;
            this.SelectMedName.HeaderText = "Name";
            this.SelectMedName.Name = "SelectMedName";
            this.SelectMedName.ReadOnly = true;
            // 
            // RefMedID
            // 
            this.RefMedID.DataPropertyName = "RefMedID";
            this.RefMedID.HeaderText = "RefMedID";
            this.RefMedID.Name = "RefMedID";
            this.RefMedID.ReadOnly = true;
            this.RefMedID.Visible = false;
            // 
            // Mode
            // 
            this.Mode.DataPropertyName = "Mode";
            this.Mode.HeaderText = "mode";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Visible = false;
            // 
            // SelectedMedDosage
            // 
            this.SelectedMedDosage.DataPropertyName = "UrduDosage";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectedMedDosage.DefaultCellStyle = dataGridViewCellStyle2;
            this.SelectedMedDosage.FillWeight = 50F;
            this.SelectedMedDosage.HeaderText = "Dosage";
            this.SelectedMedDosage.Name = "SelectedMedDosage";
            this.SelectedMedDosage.ReadOnly = true;
            // 
            // SelectEngDosage
            // 
            this.SelectEngDosage.DataPropertyName = "EngDosage";
            this.SelectEngDosage.HeaderText = "Dosage";
            this.SelectEngDosage.Name = "SelectEngDosage";
            this.SelectEngDosage.ReadOnly = true;
            this.SelectEngDosage.Visible = false;
            // 
            // dgAllMed
            // 
            this.dgAllMed.AllowUserToAddRows = false;
            this.dgAllMed.AllowUserToDeleteRows = false;
            this.dgAllMed.AllowUserToOrderColumns = true;
            this.dgAllMed.AllowUserToResizeColumns = false;
            this.dgAllMed.AllowUserToResizeRows = false;
            this.dgAllMed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAllMed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgAllMed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgAllMed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AllMedID,
            this.AllMedName});
            this.dgAllMed.ContextMenuStrip = this.cmsSelection;
            this.dgAllMed.Location = new System.Drawing.Point(12, 112);
            this.dgAllMed.MultiSelect = false;
            this.dgAllMed.Name = "dgAllMed";
            this.dgAllMed.ReadOnly = true;
            this.dgAllMed.RowHeadersWidth = 35;
            this.dgAllMed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgAllMed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAllMed.Size = new System.Drawing.Size(306, 568);
            this.dgAllMed.TabIndex = 1;
            this.dgAllMed.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAllMed_CellDoubleClick);
            this.dgAllMed.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgAllMed_MouseDown);
            // 
            // AllMedID
            // 
            this.AllMedID.DataPropertyName = "MedicineID";
            this.AllMedID.HeaderText = "MedID";
            this.AllMedID.Name = "AllMedID";
            this.AllMedID.ReadOnly = true;
            this.AllMedID.Visible = false;
            // 
            // AllMedName
            // 
            this.AllMedName.DataPropertyName = "Med_Name";
            this.AllMedName.HeaderText = "Name";
            this.AllMedName.Name = "AllMedName";
            this.AllMedName.ReadOnly = true;
            // 
            // cmsSelection
            // 
            this.cmsSelection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miMoreInfo});
            this.cmsSelection.Name = "cmsSelection";
            this.cmsSelection.Size = new System.Drawing.Size(169, 48);
            // 
            // miAdd
            // 
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(168, 22);
            this.miAdd.Text = "Add";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miMoreInfo
            // 
            this.miMoreInfo.Name = "miMoreInfo";
            this.miMoreInfo.Size = new System.Drawing.Size(168, 22);
            this.miMoreInfo.Text = "More Information";
            this.miMoreInfo.Click += new System.EventHandler(this.miMoreInfo_Click);
            // 
            // btnSelection
            // 
            this.btnSelection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSelection.BackgroundImage")));
            this.btnSelection.Location = new System.Drawing.Point(324, 355);
            this.btnSelection.Name = "btnSelection";
            this.btnSelection.Size = new System.Drawing.Size(45, 79);
            this.btnSelection.TabIndex = 2;
            this.btnSelection.UseVisualStyleBackColor = true;
            this.btnSelection.Click += new System.EventHandler(this.btnSelection_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "All Medicine : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(372, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Selected  Medicine : ";
            // 
            // txtAllMedSerach
            // 
            this.txtAllMedSerach.Location = new System.Drawing.Point(12, 91);
            this.txtAllMedSerach.Name = "txtAllMedSerach";
            this.txtAllMedSerach.Size = new System.Drawing.Size(306, 20);
            this.txtAllMedSerach.TabIndex = 5;
            this.txtAllMedSerach.TextChanged += new System.EventHandler(this.txtAllMedSerach_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(12, 11);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(910, 53);
            this.panel2.TabIndex = 46;
            this.panel2.Tag = "top";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.toolStrip1.Location = new System.Drawing.Point(712, -8);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(200, 66);
            this.toolStrip1.TabIndex = 44;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(762, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 33);
            this.button1.TabIndex = 47;
            this.button1.Text = "Select Dosage";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.BurlyWood;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(508, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 23);
            this.label3.TabIndex = 48;
            this.label3.Tag = "no";
            this.label3.Text = "New Medicine";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Cornsilk;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(606, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 23);
            this.label4.TabIndex = 49;
            this.label4.Tag = "no";
            this.label4.Text = "Change Dosage";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlEngDosage
            // 
            this.pnlEngDosage.BackColor = System.Drawing.Color.Transparent;
            this.pnlEngDosage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEngDosage.Controls.Add(this.btnEngDosageAdd);
            this.pnlEngDosage.Controls.Add(this.label5);
            this.pnlEngDosage.Controls.Add(this.cboDosage);
            this.pnlEngDosage.Controls.Add(this.cboEngPeriod);
            this.pnlEngDosage.Controls.Add(this.nudEngPeriodCount);
            this.pnlEngDosage.Location = new System.Drawing.Point(704, 65);
            this.pnlEngDosage.Name = "pnlEngDosage";
            this.pnlEngDosage.Size = new System.Drawing.Size(218, 46);
            this.pnlEngDosage.TabIndex = 155;
            // 
            // btnEngDosageAdd
            // 
            this.btnEngDosageAdd.Location = new System.Drawing.Point(11, 21);
            this.btnEngDosageAdd.Name = "btnEngDosageAdd";
            this.btnEngDosageAdd.Size = new System.Drawing.Size(59, 23);
            this.btnEngDosageAdd.TabIndex = 155;
            this.btnEngDosageAdd.Text = "Add";
            this.btnEngDosageAdd.UseVisualStyleBackColor = true;
            this.btnEngDosageAdd.Click += new System.EventHandler(this.btnEngDosageAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 154;
            this.label5.Text = "Dosage : ";
            // 
            // cboDosage
            // 
            this.cboDosage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDosage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDosage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDosage.FormattingEnabled = true;
            this.cboDosage.Location = new System.Drawing.Point(72, 0);
            this.cboDosage.Name = "cboDosage";
            this.cboDosage.Size = new System.Drawing.Size(135, 21);
            this.cboDosage.TabIndex = 150;
            // 
            // cboEngPeriod
            // 
            this.cboEngPeriod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEngPeriod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEngPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEngPeriod.FormattingEnabled = true;
            this.cboEngPeriod.Items.AddRange(new object[] {
            "Select",
            "Days",
            "Weeks",
            "Months"});
            this.cboEngPeriod.Location = new System.Drawing.Point(116, 22);
            this.cboEngPeriod.Name = "cboEngPeriod";
            this.cboEngPeriod.Size = new System.Drawing.Size(91, 21);
            this.cboEngPeriod.TabIndex = 153;
            // 
            // nudEngPeriodCount
            // 
            this.nudEngPeriodCount.Location = new System.Drawing.Point(73, 22);
            this.nudEngPeriodCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEngPeriodCount.Name = "nudEngPeriodCount";
            this.nudEngPeriodCount.Size = new System.Drawing.Size(37, 20);
            this.nudEngPeriodCount.TabIndex = 152;
            this.nudEngPeriodCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // frmAddFromList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(934, 692);
            this.Controls.Add(this.pnlEngDosage);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txtAllMedSerach);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgAllMed);
            this.Controls.Add(this.dgSelectedMed);
            this.Controls.Add(this.btnSelection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAddFromList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Medicine From List";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAddFromList_FormClosing);
            this.Load += new System.EventHandler(this.frmAddFromList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgSelectedMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllMed)).EndInit();
            this.cmsSelection.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pnlEngDosage.ResumeLayout(false);
            this.pnlEngDosage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEngPeriodCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgSelectedMed;
    private System.Windows.Forms.DataGridView dgAllMed;
    private System.Windows.Forms.Button btnSelection;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txtAllMedSerach;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton miSave;
    private System.Windows.Forms.ToolStripButton miRefresh;
    private System.Windows.Forms.ToolStripButton miExit;
    private System.Windows.Forms.ContextMenuStrip cmsSelection;
    private System.Windows.Forms.ToolStripMenuItem miAdd;
      private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ToolStripMenuItem miMoreInfo;
    private System.Windows.Forms.DataGridViewTextBoxColumn AllMedID;
      private System.Windows.Forms.DataGridViewTextBoxColumn AllMedName;
      private System.Windows.Forms.DataGridViewTextBoxColumn SelectMedID;
      private System.Windows.Forms.DataGridViewTextBoxColumn SelectMedName;
      private System.Windows.Forms.DataGridViewTextBoxColumn RefMedID;
      private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
      private System.Windows.Forms.DataGridViewTextBoxColumn SelectedMedDosage;
      private System.Windows.Forms.DataGridViewTextBoxColumn SelectEngDosage;
      private System.Windows.Forms.Panel pnlEngDosage;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.ComboBox cboDosage;
      private System.Windows.Forms.ComboBox cboEngPeriod;
      private System.Windows.Forms.NumericUpDown nudEngPeriodCount;
      private System.Windows.Forms.Button btnEngDosageAdd;
  }
}