namespace OPD
{
    partial class frmPatientReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPatientReg));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.dtpDateBirth = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.cboCity = new System.Windows.Forms.ComboBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtCell = new System.Windows.Forms.TextBox();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.txtSearchPRNO = new System.Windows.Forms.MaskedTextBox();
            this.txtSearchPhone = new System.Windows.Forms.TextBox();
            this.txtSearchName = new System.Windows.Forms.TextBox();
            this.btnPatientSearch = new System.Windows.Forms.Button();
            this.btnDoctorsCashReport = new System.Windows.Forms.Button();
            this.picPatient = new System.Windows.Forms.PictureBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.txtBSA = new System.Windows.Forms.TextBox();
            this.txtBMI = new System.Windows.Forms.TextBox();
            this.txtBP = new System.Windows.Forms.MaskedTextBox();
            this.cboHeightUnit = new System.Windows.Forms.ComboBox();
            this.cboWeightUnit = new System.Windows.Forms.ComboBox();
            this.cboTempUnit = new System.Windows.Forms.ComboBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.txtPulse = new System.Windows.Forms.TextBox();
            this.txtTemp = new System.Windows.Forms.TextBox();
            this.txtHeadCircum = new System.Windows.Forms.TextBox();
            this.txtVSPRNO = new System.Windows.Forms.MaskedTextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lblPatientID = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlRegisterSearch = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.ShowMask = new System.Windows.Forms.LinkLabel();
            this.RemoveMask = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPatientSearchHeading = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTo = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.rbSPhone = new System.Windows.Forms.RadioButton();
            this.rbSCell = new System.Windows.Forms.RadioButton();
            this.dgPatientReg = new System.Windows.Forms.DataGridView();
            this.PatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaritalStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPatientReg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAppointment = new System.Windows.Forms.ToolStripMenuItem();
            this.miEmailAllVisit = new System.Windows.Forms.ToolStripMenuItem();
            this.miLastFiveVisit = new System.Windows.Forms.ToolStripMenuItem();
            this.miLastTenVisit = new System.Windows.Forms.ToolStripMenuItem();
            this.miAllVisit = new System.Windows.Forms.ToolStripMenuItem();
            this.miLastVisit = new System.Windows.Forms.ToolStripMenuItem();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblSearchPRNO = new System.Windows.Forms.Label();
            this.btnCol = new System.Windows.Forms.Button();
            this.btnExpend = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtRPRNo = new System.Windows.Forms.MaskedTextBox();
            this.pnlPaymentMode = new System.Windows.Forms.Panel();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblRefNo = new System.Windows.Forms.Label();
            this.rbCredit = new System.Windows.Forms.RadioButton();
            this.rbDebit = new System.Windows.Forms.RadioButton();
            this.rbCash = new System.Windows.Forms.RadioButton();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrintReceipt = new System.Windows.Forms.Button();
            this.txtEmpNo = new System.Windows.Forms.TextBox();
            this.pnlMaritalStatus = new System.Windows.Forms.Panel();
            this.rbMarried = new System.Windows.Forms.RadioButton();
            this.rbSingle = new System.Windows.Forms.RadioButton();
            this.lblPatientInfo = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblPanel = new System.Windows.Forms.Label();
            this.cboPanel = new System.Windows.Forms.ComboBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.lblAge = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblCell = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEmpNo = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblMaritalStatus = new System.Windows.Forms.Label();
            this.lblPayment = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlChangeAppoint = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpAppointDate = new System.Windows.Forms.DateTimePicker();
            this.cboChangeConultant = new System.Windows.Forms.ComboBox();
            this.btnChangeExit = new System.Windows.Forms.Button();
            this.dgAppointment = new System.Windows.Forms.DataGridView();
            this.APatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ATokenNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VitalSign = new System.Windows.Forms.DataGridViewLinkColumn();
            this.opdid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsAppointment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miDown = new System.Windows.Forms.ToolStripMenuItem();
            this.miUp = new System.Windows.Forms.ToolStripMenuItem();
            this.miChangeAppoint = new System.Windows.Forms.ToolStripMenuItem();
            this.miCancelAppoint = new System.Windows.Forms.ToolStripMenuItem();
            this.cboConsultent = new System.Windows.Forms.ComboBox();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnAppointment = new System.Windows.Forms.Button();
            this.lblSelectedDate = new System.Windows.Forms.Label();
            this.lblAppointment = new System.Windows.Forms.Label();
            this.mcAppointment = new Pabo.Calendar.MonthCalendar();
            this.dateItem1 = new Pabo.Calendar.DateItem();
            this.dateItem2 = new Pabo.Calendar.DateItem();
            this.dateItem3 = new Pabo.Calendar.DateItem();
            this.lblConsultent = new System.Windows.Forms.Label();
            this.lblPRNO = new System.Windows.Forms.Label();
            this.dgSummary = new System.Windows.Forms.DataGridView();
            this.OPDIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPRNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Video = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlSummry = new System.Windows.Forms.Panel();
            this.btnTomorrow = new System.Windows.Forms.Button();
            this.btnNextTomorrow = new System.Windows.Forms.Button();
            this.pnlVitalSigns = new System.Windows.Forms.Panel();
            this.txtRespiratoryRate = new System.Windows.Forms.TextBox();
            this.txtHeartRate = new System.Windows.Forms.TextBox();
            this.lblRespiratoryRate = new System.Windows.Forms.Label();
            this.lblHeartRate = new System.Windows.Forms.Label();
            this.btnVSSave = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnVitalSignClose = new System.Windows.Forms.Button();
            this.lblBSAUnit = new System.Windows.Forms.Label();
            this.txtBMIStatus = new System.Windows.Forms.TextBox();
            this.lblBMIUnit = new System.Windows.Forms.Label();
            this.lblVitalSigns = new System.Windows.Forms.Label();
            this.lblHeadCircumUnit = new System.Windows.Forms.Label();
            this.lblPulseUnit = new System.Windows.Forms.Label();
            this.lblBPUnit = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblPulse = new System.Windows.Forms.Label();
            this.lblBSA = new System.Windows.Forms.Label();
            this.lblBMI = new System.Windows.Forms.Label();
            this.lblBMIStatus = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblBP = new System.Windows.Forms.Label();
            this.lblHeadCircum = new System.Windows.Forms.Label();
            this.lblTemp = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblConsulted = new System.Windows.Forms.Label();
            this.lblHold = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblOPDID = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPatient)).BeginInit();
            this.panel3.SuspendLayout();
            this.pnlRegisterSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatientReg)).BeginInit();
            this.cmsPatientReg.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnlPaymentMode.SuspendLayout();
            this.pnlMaritalStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel5.SuspendLayout();
            this.pnlChangeAppoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppointment)).BeginInit();
            this.cmsAppointment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSummary)).BeginInit();
            this.pnlSummry.SuspendLayout();
            this.pnlVitalSigns.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(312, 35);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(231, 20);
            this.txtName.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtName, "First, Middle and Last Name");
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtAddress
            // 
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Location = new System.Drawing.Point(85, 121);
            this.txtAddress.MaxLength = 255;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(177, 20);
            this.txtAddress.TabIndex = 10;
            this.toolTip1.SetToolTip(this.txtAddress, "Home address");
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
            this.txtAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // dtpDateBirth
            // 
            this.dtpDateBirth.CustomFormat = "dd-MM-yyyy";
            this.dtpDateBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateBirth.Location = new System.Drawing.Point(601, 37);
            this.dtpDateBirth.Name = "dtpDateBirth";
            this.dtpDateBirth.Size = new System.Drawing.Size(80, 20);
            this.dtpDateBirth.TabIndex = 1;
            this.toolTip1.SetToolTip(this.dtpDateBirth, "Date Of Birth");
            this.dtpDateBirth.ValueChanged += new System.EventHandler(this.dtpDateBirth_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbMale);
            this.panel1.Controls.Add(this.rbFemale);
            this.panel1.Location = new System.Drawing.Point(88, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 24);
            this.panel1.TabIndex = 3;
            this.toolTip1.SetToolTip(this.panel1, "Select Gender");
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Checked = true;
            this.rbMale.Location = new System.Drawing.Point(36, 3);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(48, 17);
            this.rbMale.TabIndex = 1;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "Male";
            this.rbMale.UseVisualStyleBackColor = true;
            this.rbMale.CheckedChanged += new System.EventHandler(this.rbMale_CheckedChanged);
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(89, 2);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(59, 17);
            this.rbFemale.TabIndex = 0;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseVisualStyleBackColor = true;
            this.rbFemale.CheckedChanged += new System.EventHandler(this.rbFemale_CheckedChanged);
            // 
            // cboCity
            // 
            this.cboCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCity.FormattingEnabled = true;
            this.cboCity.Location = new System.Drawing.Point(344, 122);
            this.cboCity.Name = "cboCity";
            this.cboCity.Size = new System.Drawing.Size(168, 21);
            this.cboCity.TabIndex = 11;
            this.toolTip1.SetToolTip(this.cboCity, "Select City");
            this.cboCity.SelectedIndexChanged += new System.EventHandler(this.cboCity_SelectedIndexChanged);
            // 
            // txtPhone
            // 
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Location = new System.Drawing.Point(549, 6);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(85, 20);
            this.txtPhone.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtPhone, "Phone Number");
            this.txtPhone.Visible = false;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // txtCell
            // 
            this.txtCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCell.Location = new System.Drawing.Point(96, 94);
            this.txtCell.MaxLength = 15;
            this.txtCell.Name = "txtCell";
            this.txtCell.Size = new System.Drawing.Size(166, 20);
            this.txtCell.TabIndex = 6;
            this.toolTip1.SetToolTip(this.txtCell, "Cell Number");
            this.txtCell.TextChanged += new System.EventHandler(this.txtCell_TextChanged);
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(449, 34);
            this.dtpFrom.MaxDate = new System.DateTime(2060, 12, 31, 0, 0, 0, 0);
            this.dtpFrom.MinDate = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(92, 20);
            this.dtpFrom.TabIndex = 3;
            this.toolTip1.SetToolTip(this.dtpFrom, "End Date for search");
            this.dtpFrom.Value = new System.DateTime(2021, 4, 6, 0, 0, 0, 0);
            this.dtpFrom.ValueChanged += new System.EventHandler(this.dtpFrom_ValueChanged);
            this.dtpFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // dtpTo
            // 
            this.dtpTo.Checked = false;
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(571, 34);
            this.dtpTo.MaxDate = new System.DateTime(2060, 12, 2, 0, 0, 0, 0);
            this.dtpTo.MinDate = new System.DateTime(2000, 10, 1, 0, 0, 0, 0);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(92, 20);
            this.dtpTo.TabIndex = 2;
            this.toolTip1.SetToolTip(this.dtpTo, "Start Date for Search");
            this.dtpTo.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            this.dtpTo.ValueChanged += new System.EventHandler(this.dtpTo_ValueChanged);
            this.dtpTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // txtSearchPRNO
            // 
            this.txtSearchPRNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchPRNO.Location = new System.Drawing.Point(73, 36);
            this.txtSearchPRNO.Name = "txtSearchPRNO";
            this.txtSearchPRNO.Size = new System.Drawing.Size(100, 20);
            this.txtSearchPRNO.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtSearchPRNO, "MR No. for Search");
            this.txtSearchPRNO.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtSearchPRNO_MaskInputRejected);
            this.txtSearchPRNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // txtSearchPhone
            // 
            this.txtSearchPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchPhone.Location = new System.Drawing.Point(264, 61);
            this.txtSearchPhone.Name = "txtSearchPhone";
            this.txtSearchPhone.Size = new System.Drawing.Size(113, 20);
            this.txtSearchPhone.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtSearchPhone, "Cell number or Phone Number for Search according to selection");
            this.txtSearchPhone.TextChanged += new System.EventHandler(this.txtSearchPhone_TextChanged);
            this.txtSearchPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // txtSearchName
            // 
            this.txtSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchName.Location = new System.Drawing.Point(218, 34);
            this.txtSearchName.Name = "txtSearchName";
            this.txtSearchName.Size = new System.Drawing.Size(186, 20);
            this.txtSearchName.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtSearchName, "Name for Search");
            this.txtSearchName.TextChanged += new System.EventHandler(this.txtSearchName_TextChanged);
            this.txtSearchName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            this.txtSearchName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // btnPatientSearch
            // 
            this.btnPatientSearch.AccessibleDescription = "";
            this.btnPatientSearch.AccessibleName = "";
            this.btnPatientSearch.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnPatientSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPatientSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPatientSearch.ForeColor = System.Drawing.Color.Azure;
            this.btnPatientSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnPatientSearch.Image")));
            this.btnPatientSearch.Location = new System.Drawing.Point(682, 42);
            this.btnPatientSearch.Name = "btnPatientSearch";
            this.btnPatientSearch.Size = new System.Drawing.Size(35, 35);
            this.btnPatientSearch.TabIndex = 6;
            this.btnPatientSearch.Tag = "Search";
            this.btnPatientSearch.Text = "&Find";
            this.btnPatientSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolTip1.SetToolTip(this.btnPatientSearch, "Find Patient according to Seleted Critaria");
            this.btnPatientSearch.UseVisualStyleBackColor = false;
            this.btnPatientSearch.Click += new System.EventHandler(this.btnPatientSearch_Click);
            // 
            // btnDoctorsCashReport
            // 
            this.btnDoctorsCashReport.Location = new System.Drawing.Point(54, 1);
            this.btnDoctorsCashReport.Name = "btnDoctorsCashReport";
            this.btnDoctorsCashReport.Size = new System.Drawing.Size(271, 26);
            this.btnDoctorsCashReport.TabIndex = 43;
            this.btnDoctorsCashReport.Text = "Daily Cash Report";
            this.toolTip1.SetToolTip(this.btnDoctorsCashReport, "Docotor Base cash Report");
            this.btnDoctorsCashReport.UseVisualStyleBackColor = true;
            this.btnDoctorsCashReport.Click += new System.EventHandler(this.btnDoctorsCashReport_Click);
            // 
            // picPatient
            // 
            this.picPatient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picPatient.Location = new System.Drawing.Point(687, 21);
            this.picPatient.Name = "picPatient";
            this.picPatient.Size = new System.Drawing.Size(100, 100);
            this.picPatient.TabIndex = 163;
            this.picPatient.TabStop = false;
            this.toolTip1.SetToolTip(this.picPatient, "Double Click to select Patient Picture");
            this.picPatient.Click += new System.EventHandler(this.picPatient_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Location = new System.Drawing.Point(327, 6);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(45, 45);
            this.btnAccept.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnAccept, "Change Appointment Date or Consultanat");
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteAll.BackgroundImage")));
            this.btnDeleteAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteAll.Location = new System.Drawing.Point(768, 4);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(36, 36);
            this.btnDeleteAll.TabIndex = 18;
            this.toolTip1.SetToolTip(this.btnDeleteAll, "Delete All appointment for Selceted Date");
            this.btnDeleteAll.UseVisualStyleBackColor = true;
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            // 
            // txtBSA
            // 
            this.txtBSA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSA.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBSA.Location = new System.Drawing.Point(124, 304);
            this.txtBSA.Name = "txtBSA";
            this.txtBSA.ReadOnly = true;
            this.txtBSA.Size = new System.Drawing.Size(77, 21);
            this.txtBSA.TabIndex = 13;
            this.txtBSA.TabStop = false;
            this.txtBSA.Tag = "Notset";
            this.toolTip1.SetToolTip(this.txtBSA, "Patient Body surface Area (BSA)");
            this.txtBSA.TextChanged += new System.EventHandler(this.txtBSA_TextChanged);
            // 
            // txtBMI
            // 
            this.txtBMI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBMI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBMI.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBMI.Location = new System.Drawing.Point(124, 280);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.ReadOnly = true;
            this.txtBMI.Size = new System.Drawing.Size(77, 21);
            this.txtBMI.TabIndex = 10;
            this.txtBMI.TabStop = false;
            this.txtBMI.Tag = "Notset";
            this.toolTip1.SetToolTip(this.txtBMI, "Patient  Body Mass Index (BMI)");
            this.txtBMI.TextChanged += new System.EventHandler(this.txtBMI_TextChanged);
            // 
            // txtBP
            // 
            this.txtBP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBP.HidePromptOnLeave = true;
            this.txtBP.Location = new System.Drawing.Point(124, 84);
            this.txtBP.Mask = "000/000";
            this.txtBP.Name = "txtBP";
            this.txtBP.Size = new System.Drawing.Size(77, 21);
            this.txtBP.SkipLiterals = false;
            this.txtBP.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtBP, "Patient  Blood Pressure (BP )");
            this.txtBP.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtBP_MaskInputRejected);
            this.txtBP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // cboHeightUnit
            // 
            this.cboHeightUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHeightUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHeightUnit.FormattingEnabled = true;
            this.cboHeightUnit.Items.AddRange(new object[] {
            "Feet",
            "Inches"});
            this.cboHeightUnit.Location = new System.Drawing.Point(203, 179);
            this.cboHeightUnit.Name = "cboHeightUnit";
            this.cboHeightUnit.Size = new System.Drawing.Size(64, 23);
            this.cboHeightUnit.TabIndex = 7;
            this.toolTip1.SetToolTip(this.cboHeightUnit, "Unit for Height");
            this.cboHeightUnit.SelectedIndexChanged += new System.EventHandler(this.cboHeightUnit_SelectedIndexChanged);
            // 
            // cboWeightUnit
            // 
            this.cboWeightUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWeightUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboWeightUnit.FormattingEnabled = true;
            this.cboWeightUnit.Items.AddRange(new object[] {
            "Kg",
            "Pounds"});
            this.cboWeightUnit.Location = new System.Drawing.Point(203, 155);
            this.cboWeightUnit.Name = "cboWeightUnit";
            this.cboWeightUnit.Size = new System.Drawing.Size(64, 23);
            this.cboWeightUnit.TabIndex = 5;
            this.toolTip1.SetToolTip(this.cboWeightUnit, "Unit for Weight");
            this.cboWeightUnit.SelectedIndexChanged += new System.EventHandler(this.cboWeightUnit_SelectedIndexChanged);
            // 
            // cboTempUnit
            // 
            this.cboTempUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTempUnit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboTempUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTempUnit.FormattingEnabled = true;
            this.cboTempUnit.Items.AddRange(new object[] {
            "F",
            "C"});
            this.cboTempUnit.Location = new System.Drawing.Point(203, 131);
            this.cboTempUnit.Name = "cboTempUnit";
            this.cboTempUnit.Size = new System.Drawing.Size(64, 23);
            this.cboTempUnit.TabIndex = 3;
            this.toolTip1.SetToolTip(this.cboTempUnit, "Unit for Temprature");
            this.cboTempUnit.SelectedIndexChanged += new System.EventHandler(this.cboTempUnit_SelectedIndexChanged);
            // 
            // txtHeight
            // 
            this.txtHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight.Location = new System.Drawing.Point(124, 180);
            this.txtHeight.MaxLength = 4;
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(77, 21);
            this.txtHeight.TabIndex = 6;
            this.toolTip1.SetToolTip(this.txtHeight, "Patient  Height");
            this.txtHeight.TextChanged += new System.EventHandler(this.txtHeight_TextChanged);
            this.txtHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            this.txtHeight.Leave += new System.EventHandler(this.txtWeight_Leave);
            // 
            // txtWeight
            // 
            this.txtWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(124, 156);
            this.txtWeight.MaxLength = 3;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(77, 21);
            this.txtWeight.TabIndex = 4;
            this.toolTip1.SetToolTip(this.txtWeight, "Patient Weight");
            this.txtWeight.TextChanged += new System.EventHandler(this.txtWeight_TextChanged);
            this.txtWeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            this.txtWeight.Leave += new System.EventHandler(this.txtWeight_Leave);
            // 
            // txtPulse
            // 
            this.txtPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPulse.Location = new System.Drawing.Point(124, 108);
            this.txtPulse.MaxLength = 3;
            this.txtPulse.Name = "txtPulse";
            this.txtPulse.Size = new System.Drawing.Size(77, 21);
            this.txtPulse.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtPulse, "Patient Pulse");
            this.txtPulse.TextChanged += new System.EventHandler(this.txtPulse_TextChanged);
            this.txtPulse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // txtTemp
            // 
            this.txtTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTemp.Location = new System.Drawing.Point(124, 132);
            this.txtTemp.MaxLength = 3;
            this.txtTemp.Name = "txtTemp";
            this.txtTemp.Size = new System.Drawing.Size(77, 21);
            this.txtTemp.TabIndex = 2;
            this.toolTip1.SetToolTip(this.txtTemp, "Patient Temprature");
            this.txtTemp.TextChanged += new System.EventHandler(this.txtTemp_TextChanged);
            this.txtTemp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // txtHeadCircum
            // 
            this.txtHeadCircum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeadCircum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeadCircum.Location = new System.Drawing.Point(124, 204);
            this.txtHeadCircum.MaxLength = 4;
            this.txtHeadCircum.Name = "txtHeadCircum";
            this.txtHeadCircum.Size = new System.Drawing.Size(77, 21);
            this.txtHeadCircum.TabIndex = 8;
            this.toolTip1.SetToolTip(this.txtHeadCircum, "Patient Head Circumfrences");
            this.txtHeadCircum.TextChanged += new System.EventHandler(this.txtHeadCircum_TextChanged);
            this.txtHeadCircum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // txtVSPRNO
            // 
            this.txtVSPRNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVSPRNO.Enabled = false;
            this.txtVSPRNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVSPRNO.Location = new System.Drawing.Point(124, 46);
            this.txtVSPRNO.Name = "txtVSPRNO";
            this.txtVSPRNO.Size = new System.Drawing.Size(102, 23);
            this.txtVSPRNO.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtVSPRNO, "PR No. for Search");
            this.txtVSPRNO.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtVSPRNO_MaskInputRejected);
            // 
            // button9
            // 
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.Location = new System.Drawing.Point(73, 62);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(26, 22);
            this.button9.TabIndex = 186;
            this.toolTip1.SetToolTip(this.button9, "Remove Mask");
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button12
            // 
            this.button12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button12.BackgroundImage")));
            this.button12.Location = new System.Drawing.Point(77, 63);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(20, 20);
            this.button12.TabIndex = 188;
            this.button12.Text = "System.Drawing.Bitmap";
            this.toolTip1.SetToolTip(this.button12, "Add Mask");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Visible = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.Location = new System.Drawing.Point(83, 36);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 20);
            this.button2.TabIndex = 190;
            this.button2.Text = "System.Drawing.Bitmap";
            this.toolTip1.SetToolTip(this.button2, "Add Mask");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(63, 34);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 22);
            this.button3.TabIndex = 189;
            this.toolTip1.SetToolTip(this.button3, "Remove Mask");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblPatientID
            // 
            this.lblPatientID.AutoSize = true;
            this.lblPatientID.Location = new System.Drawing.Point(127, 448);
            this.lblPatientID.Name = "lblPatientID";
            this.lblPatientID.Size = new System.Drawing.Size(61, 13);
            this.lblPatientID.TabIndex = 36;
            this.lblPatientID.Text = "lblPatientID";
            this.lblPatientID.Visible = false;
            this.lblPatientID.Click += new System.EventHandler(this.lblPatientID_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pnlRegisterSearch);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Location = new System.Drawing.Point(1, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(819, 230);
            this.panel3.TabIndex = 0;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // pnlRegisterSearch
            // 
            this.pnlRegisterSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRegisterSearch.Controls.Add(this.pictureBox3);
            this.pnlRegisterSearch.Controls.Add(this.button12);
            this.pnlRegisterSearch.Controls.Add(this.button9);
            this.pnlRegisterSearch.Controls.Add(this.ShowMask);
            this.pnlRegisterSearch.Controls.Add(this.RemoveMask);
            this.pnlRegisterSearch.Controls.Add(this.label3);
            this.pnlRegisterSearch.Controls.Add(this.lblPatientSearchHeading);
            this.pnlRegisterSearch.Controls.Add(this.pictureBox1);
            this.pnlRegisterSearch.Controls.Add(this.lblTo);
            this.pnlRegisterSearch.Controls.Add(this.dtpFrom);
            this.pnlRegisterSearch.Controls.Add(this.dtpTo);
            this.pnlRegisterSearch.Controls.Add(this.panel11);
            this.pnlRegisterSearch.Controls.Add(this.dgPatientReg);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchPRNO);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchPhone);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchName);
            this.pnlRegisterSearch.Controls.Add(this.lblSearchName);
            this.pnlRegisterSearch.Controls.Add(this.btnPatientSearch);
            this.pnlRegisterSearch.Controls.Add(this.lblFrom);
            this.pnlRegisterSearch.Controls.Add(this.lblSearchPRNO);
            this.pnlRegisterSearch.Controls.Add(this.btnCol);
            this.pnlRegisterSearch.Controls.Add(this.btnExpend);
            this.pnlRegisterSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlRegisterSearch.Location = new System.Drawing.Point(2, 1);
            this.pnlRegisterSearch.Name = "pnlRegisterSearch";
            this.pnlRegisterSearch.Size = new System.Drawing.Size(815, 226);
            this.pnlRegisterSearch.TabIndex = 2;
            this.pnlRegisterSearch.Tag = "";
            this.pnlRegisterSearch.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlRegisterSearch_Paint);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(379, 61);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(25, 21);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 132;
            this.pictureBox3.TabStop = false;
            // 
            // ShowMask
            // 
            this.ShowMask.AutoSize = true;
            this.ShowMask.Location = new System.Drawing.Point(559, 4);
            this.ShowMask.Name = "ShowMask";
            this.ShowMask.Size = new System.Drawing.Size(63, 13);
            this.ShowMask.TabIndex = 132;
            this.ShowMask.TabStop = true;
            this.ShowMask.Text = "Show Mask";
            this.ShowMask.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ShowMask.Visible = false;
            this.ShowMask.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ShowMask_LinkClicked);
            // 
            // RemoveMask
            // 
            this.RemoveMask.AutoSize = true;
            this.RemoveMask.Location = new System.Drawing.Point(628, 5);
            this.RemoveMask.Name = "RemoveMask";
            this.RemoveMask.Size = new System.Drawing.Size(76, 13);
            this.RemoveMask.TabIndex = 131;
            this.RemoveMask.TabStop = true;
            this.RemoveMask.Text = "Remove Mask";
            this.RemoveMask.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RemoveMask.Visible = false;
            this.RemoveMask.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RemoveMask_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(183, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 130;
            this.label3.Text = "Whatsapp No :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblPatientSearchHeading
            // 
            this.lblPatientSearchHeading.AutoSize = true;
            this.lblPatientSearchHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientSearchHeading.Location = new System.Drawing.Point(40, 4);
            this.lblPatientSearchHeading.Name = "lblPatientSearchHeading";
            this.lblPatientSearchHeading.Size = new System.Drawing.Size(128, 20);
            this.lblPatientSearchHeading.TabIndex = 126;
            this.lblPatientSearchHeading.Text = "Patient Search";
            this.lblPatientSearchHeading.Click += new System.EventHandler(this.lblPatientSearchHeading_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 35);
            this.pictureBox1.TabIndex = 125;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(547, 38);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(26, 13);
            this.lblTo.TabIndex = 123;
            this.lblTo.Text = "To :";
            this.lblTo.Click += new System.EventHandler(this.lblTo_Click);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.rbSPhone);
            this.panel11.Controls.Add(this.rbSCell);
            this.panel11.Location = new System.Drawing.Point(514, 13);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(152, 21);
            this.panel11.TabIndex = 4;
            this.panel11.Visible = false;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.panel11_Paint);
            // 
            // rbSPhone
            // 
            this.rbSPhone.AutoSize = true;
            this.rbSPhone.Location = new System.Drawing.Point(4, 1);
            this.rbSPhone.Name = "rbSPhone";
            this.rbSPhone.Size = new System.Drawing.Size(76, 17);
            this.rbSPhone.TabIndex = 1;
            this.rbSPhone.Text = "Phone No.";
            this.rbSPhone.UseVisualStyleBackColor = true;
            this.rbSPhone.CheckedChanged += new System.EventHandler(this.rbSPhone_CheckedChanged);
            // 
            // rbSCell
            // 
            this.rbSCell.AutoSize = true;
            this.rbSCell.Checked = true;
            this.rbSCell.Location = new System.Drawing.Point(86, 1);
            this.rbSCell.Name = "rbSCell";
            this.rbSCell.Size = new System.Drawing.Size(62, 17);
            this.rbSCell.TabIndex = 0;
            this.rbSCell.TabStop = true;
            this.rbSCell.Text = "Cell No.";
            this.rbSCell.UseVisualStyleBackColor = true;
            this.rbSCell.CheckedChanged += new System.EventHandler(this.rbSCell_CheckedChanged);
            // 
            // dgPatientReg
            // 
            this.dgPatientReg.AllowUserToAddRows = false;
            this.dgPatientReg.AllowUserToDeleteRows = false;
            this.dgPatientReg.AllowUserToOrderColumns = true;
            this.dgPatientReg.AllowUserToResizeColumns = false;
            this.dgPatientReg.AllowUserToResizeRows = false;
            this.dgPatientReg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPatientReg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPatientReg.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgPatientReg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatientReg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgPatientReg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPatientReg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientID,
            this.Email,
            this.SerialNo,
            this.PRNO,
            this.PatientName,
            this.Gender,
            this.DOB,
            this.Age,
            this.PhoneNo,
            this.CellNo,
            this.City,
            this.Address,
            this.MaritalStatus,
            this.Panel,
            this.EmployNo});
            this.dgPatientReg.ContextMenuStrip = this.cmsPatientReg;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPatientReg.DefaultCellStyle = dataGridViewCellStyle24;
            this.dgPatientReg.EnableHeadersVisualStyles = false;
            this.dgPatientReg.Location = new System.Drawing.Point(0, 88);
            this.dgPatientReg.MultiSelect = false;
            this.dgPatientReg.Name = "dgPatientReg";
            this.dgPatientReg.ReadOnly = true;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatientReg.RowHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgPatientReg.RowHeadersWidth = 20;
            this.dgPatientReg.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPatientReg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPatientReg.Size = new System.Drawing.Size(816, 134);
            this.dgPatientReg.TabIndex = 33;
            this.dgPatientReg.TabStop = false;
            this.dgPatientReg.VirtualMode = true;
            this.dgPatientReg.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPatientReg_CellContentClick);
            this.dgPatientReg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPatientReg_CellDoubleClick);
            this.dgPatientReg.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPatientReg_ColumnHeaderMouseClick);
            this.dgPatientReg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgPatientReg_MouseDown);
            // 
            // PatientID
            // 
            this.PatientID.DataPropertyName = "PatientID";
            this.PatientID.HeaderText = "Patient ID";
            this.PatientID.Name = "PatientID";
            this.PatientID.ReadOnly = true;
            this.PatientID.Visible = false;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Visible = false;
            // 
            // SerialNo
            // 
            this.SerialNo.DataPropertyName = "Serial";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SerialNo.DefaultCellStyle = dataGridViewCellStyle15;
            this.SerialNo.HeaderText = "S #";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            // 
            // PRNO
            // 
            this.PRNO.DataPropertyName = "PRNO";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PRNO.DefaultCellStyle = dataGridViewCellStyle16;
            this.PRNO.HeaderText = "MR No.";
            this.PRNO.Name = "PRNO";
            this.PRNO.ReadOnly = true;
            // 
            // PatientName
            // 
            this.PatientName.DataPropertyName = "Name";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PatientName.DefaultCellStyle = dataGridViewCellStyle17;
            this.PatientName.HeaderText = "Name";
            this.PatientName.Name = "PatientName";
            this.PatientName.ReadOnly = true;
            this.PatientName.Width = 200;
            // 
            // Gender
            // 
            this.Gender.DataPropertyName = "Gender";
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            this.Gender.Visible = false;
            // 
            // DOB
            // 
            this.DOB.DataPropertyName = "DOB";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.Format = "dd/MM/yyyy";
            this.DOB.DefaultCellStyle = dataGridViewCellStyle18;
            this.DOB.HeaderText = "D.O.B";
            this.DOB.Name = "DOB";
            this.DOB.ReadOnly = true;
            this.DOB.Visible = false;
            this.DOB.Width = 75;
            // 
            // Age
            // 
            this.Age.DataPropertyName = "Age";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Age.DefaultCellStyle = dataGridViewCellStyle19;
            this.Age.HeaderText = "Age";
            this.Age.MaxInputLength = 10;
            this.Age.Name = "Age";
            this.Age.ReadOnly = true;
            // 
            // PhoneNo
            // 
            this.PhoneNo.DataPropertyName = "PhoneNo";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PhoneNo.DefaultCellStyle = dataGridViewCellStyle20;
            this.PhoneNo.HeaderText = "Phone No.";
            this.PhoneNo.Name = "PhoneNo";
            this.PhoneNo.ReadOnly = true;
            this.PhoneNo.Visible = false;
            this.PhoneNo.Width = 80;
            // 
            // CellNo
            // 
            this.CellNo.DataPropertyName = "CellNo";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CellNo.DefaultCellStyle = dataGridViewCellStyle21;
            this.CellNo.HeaderText = "Whatsapp No.";
            this.CellNo.Name = "CellNo";
            this.CellNo.ReadOnly = true;
            this.CellNo.Width = 160;
            // 
            // City
            // 
            this.City.DataPropertyName = "Cityname";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.City.DefaultCellStyle = dataGridViewCellStyle22;
            this.City.HeaderText = "City";
            this.City.Name = "City";
            this.City.ReadOnly = true;
            this.City.Visible = false;
            this.City.Width = 135;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Address.DefaultCellStyle = dataGridViewCellStyle23;
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            this.Address.Width = 260;
            // 
            // MaritalStatus
            // 
            this.MaritalStatus.DataPropertyName = "MaritalStatus";
            this.MaritalStatus.HeaderText = "MaritalStatus";
            this.MaritalStatus.Name = "MaritalStatus";
            this.MaritalStatus.ReadOnly = true;
            this.MaritalStatus.Visible = false;
            // 
            // Panel
            // 
            this.Panel.DataPropertyName = "PanelName";
            this.Panel.HeaderText = "Panel";
            this.Panel.Name = "Panel";
            this.Panel.ReadOnly = true;
            this.Panel.Visible = false;
            // 
            // EmployNo
            // 
            this.EmployNo.DataPropertyName = "EmployNo";
            this.EmployNo.HeaderText = "EmployNo";
            this.EmployNo.Name = "EmployNo";
            this.EmployNo.ReadOnly = true;
            this.EmployNo.Visible = false;
            // 
            // cmsPatientReg
            // 
            this.cmsPatientReg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAppointment,
            this.miEmailAllVisit});
            this.cmsPatientReg.Name = "cmsPatientReg";
            this.cmsPatientReg.Size = new System.Drawing.Size(169, 48);
            this.cmsPatientReg.Opening += new System.ComponentModel.CancelEventHandler(this.cmsPatientReg_Opening);
            // 
            // miAppointment
            // 
            this.miAppointment.Image = ((System.Drawing.Image)(resources.GetObject("miAppointment.Image")));
            this.miAppointment.Name = "miAppointment";
            this.miAppointment.Size = new System.Drawing.Size(168, 22);
            this.miAppointment.Text = "Appointment";
            this.miAppointment.Click += new System.EventHandler(this.miAppointment_Click);
            // 
            // miEmailAllVisit
            // 
            this.miEmailAllVisit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLastFiveVisit,
            this.miLastTenVisit,
            this.miAllVisit,
            this.miLastVisit});
            this.miEmailAllVisit.Name = "miEmailAllVisit";
            this.miEmailAllVisit.Size = new System.Drawing.Size(168, 22);
            this.miEmailAllVisit.Text = "Email Patient Visit";
            this.miEmailAllVisit.Click += new System.EventHandler(this.miEmailAllVisit_Click_1);
            // 
            // miLastFiveVisit
            // 
            this.miLastFiveVisit.Name = "miLastFiveVisit";
            this.miLastFiveVisit.Size = new System.Drawing.Size(144, 22);
            this.miLastFiveVisit.Text = "Last Five Visit";
            this.miLastFiveVisit.Click += new System.EventHandler(this.miLastFiveVisit_Click);
            // 
            // miLastTenVisit
            // 
            this.miLastTenVisit.Name = "miLastTenVisit";
            this.miLastTenVisit.Size = new System.Drawing.Size(144, 22);
            this.miLastTenVisit.Text = "Last Ten Visit";
            this.miLastTenVisit.Click += new System.EventHandler(this.miLastTenVisit_Click);
            // 
            // miAllVisit
            // 
            this.miAllVisit.Name = "miAllVisit";
            this.miAllVisit.Size = new System.Drawing.Size(144, 22);
            this.miAllVisit.Text = "All Visit";
            this.miAllVisit.Click += new System.EventHandler(this.miAllVisit_Click);
            // 
            // miLastVisit
            // 
            this.miLastVisit.Name = "miLastVisit";
            this.miLastVisit.Size = new System.Drawing.Size(144, 22);
            this.miLastVisit.Text = "Last Visit";
            this.miLastVisit.Click += new System.EventHandler(this.miLastVisit_Click);
            // 
            // lblSearchName
            // 
            this.lblSearchName.AutoSize = true;
            this.lblSearchName.Location = new System.Drawing.Point(179, 38);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(44, 13);
            this.lblSearchName.TabIndex = 3;
            this.lblSearchName.Text = "Name : ";
            this.lblSearchName.Click += new System.EventHandler(this.lblSearchName_Click);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(410, 38);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(36, 13);
            this.lblFrom.TabIndex = 124;
            this.lblFrom.Text = "From :";
            this.lblFrom.Click += new System.EventHandler(this.lblFrom_Click);
            // 
            // lblSearchPRNO
            // 
            this.lblSearchPRNO.AutoSize = true;
            this.lblSearchPRNO.Location = new System.Drawing.Point(12, 40);
            this.lblSearchPRNO.Name = "lblSearchPRNO";
            this.lblSearchPRNO.Size = new System.Drawing.Size(50, 13);
            this.lblSearchPRNO.TabIndex = 0;
            this.lblSearchPRNO.Text = "MR No. :";
            this.lblSearchPRNO.Click += new System.EventHandler(this.lblSearchPRNO_Click);
            // 
            // btnCol
            // 
            this.btnCol.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCol.Image = ((System.Drawing.Image)(resources.GetObject("btnCol.Image")));
            this.btnCol.Location = new System.Drawing.Point(782, 3);
            this.btnCol.Name = "btnCol";
            this.btnCol.Size = new System.Drawing.Size(25, 25);
            this.btnCol.TabIndex = 129;
            this.btnCol.UseVisualStyleBackColor = true;
            this.btnCol.Click += new System.EventHandler(this.btnCol_Click);
            // 
            // btnExpend
            // 
            this.btnExpend.Image = ((System.Drawing.Image)(resources.GetObject("btnExpend.Image")));
            this.btnExpend.Location = new System.Drawing.Point(782, 4);
            this.btnExpend.Name = "btnExpend";
            this.btnExpend.Size = new System.Drawing.Size(25, 25);
            this.btnExpend.TabIndex = 128;
            this.btnExpend.Tag = "0";
            this.btnExpend.UseVisualStyleBackColor = true;
            this.btnExpend.Visible = false;
            this.btnExpend.Click += new System.EventHandler(this.btnExpend_Click);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.button2);
            this.panel7.Controls.Add(this.button3);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Controls.Add(this.txtRPRNo);
            this.panel7.Controls.Add(this.picPatient);
            this.panel7.Controls.Add(this.pnlPaymentMode);
            this.panel7.Controls.Add(this.btnPrintReceipt);
            this.panel7.Controls.Add(this.txtEmpNo);
            this.panel7.Controls.Add(this.pnlMaritalStatus);
            this.panel7.Controls.Add(this.lblPatientInfo);
            this.panel7.Controls.Add(this.btnSave);
            this.panel7.Controls.Add(this.btnClear);
            this.panel7.Controls.Add(this.lblPanel);
            this.panel7.Controls.Add(this.cboPanel);
            this.panel7.Controls.Add(this.txtEmail);
            this.panel7.Controls.Add(this.pictureBox2);
            this.panel7.Controls.Add(this.dtpDateBirth);
            this.panel7.Controls.Add(this.txtAge);
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.lblAge);
            this.panel7.Controls.Add(this.txtAddress);
            this.panel7.Controls.Add(this.txtCell);
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Controls.Add(this.txtName);
            this.panel7.Controls.Add(this.txtPhone);
            this.panel7.Controls.Add(this.cboCity);
            this.panel7.Controls.Add(this.lblAddress);
            this.panel7.Controls.Add(this.lblGender);
            this.panel7.Controls.Add(this.lblPhone);
            this.panel7.Controls.Add(this.lblCell);
            this.panel7.Controls.Add(this.lblName);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.lblEmpNo);
            this.panel7.Controls.Add(this.lblEmail);
            this.panel7.Controls.Add(this.lblDOB);
            this.panel7.Controls.Add(this.lblCity);
            this.panel7.Controls.Add(this.lblMaritalStatus);
            this.panel7.Controls.Add(this.lblPayment);
            this.panel7.Location = new System.Drawing.Point(1, 32);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(815, 195);
            this.panel7.TabIndex = 0;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(513, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 167;
            this.button1.Text = "Add City";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 166;
            this.label4.Text = "MR No. :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Red;
            this.panel4.Location = new System.Drawing.Point(259, 35);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(4, 20);
            this.panel4.TabIndex = 146;
            this.panel4.Tag = "no";
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // txtRPRNo
            // 
            this.txtRPRNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtRPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRPRNo.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtRPRNo.Location = new System.Drawing.Point(106, 35);
            this.txtRPRNo.Name = "txtRPRNo";
            this.txtRPRNo.Size = new System.Drawing.Size(154, 20);
            this.txtRPRNo.TabIndex = 165;
            this.txtRPRNo.TabStop = false;
            this.txtRPRNo.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtRPRNo_MaskInputRejected);
            // 
            // pnlPaymentMode
            // 
            this.pnlPaymentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPaymentMode.Controls.Add(this.txtRefNo);
            this.pnlPaymentMode.Controls.Add(this.lblRefNo);
            this.pnlPaymentMode.Controls.Add(this.rbCredit);
            this.pnlPaymentMode.Controls.Add(this.rbDebit);
            this.pnlPaymentMode.Controls.Add(this.rbCash);
            this.pnlPaymentMode.Controls.Add(this.txtPayment);
            this.pnlPaymentMode.Controls.Add(this.label1);
            this.pnlPaymentMode.Location = new System.Drawing.Point(87, 149);
            this.pnlPaymentMode.Name = "pnlPaymentMode";
            this.pnlPaymentMode.Size = new System.Drawing.Size(589, 27);
            this.pnlPaymentMode.TabIndex = 12;
            this.pnlPaymentMode.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPaymentMode_Paint);
            // 
            // txtRefNo
            // 
            this.txtRefNo.Enabled = false;
            this.txtRefNo.Location = new System.Drawing.Point(412, 2);
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(152, 20);
            this.txtRefNo.TabIndex = 1;
            this.txtRefNo.TextChanged += new System.EventHandler(this.txtRefNo_TextChanged);
            // 
            // lblRefNo
            // 
            this.lblRefNo.AutoSize = true;
            this.lblRefNo.Enabled = false;
            this.lblRefNo.Location = new System.Drawing.Point(334, 5);
            this.lblRefNo.Name = "lblRefNo";
            this.lblRefNo.Size = new System.Drawing.Size(73, 13);
            this.lblRefNo.TabIndex = 152;
            this.lblRefNo.Text = "Reference # :";
            this.lblRefNo.Click += new System.EventHandler(this.lblRefNo_Click);
            // 
            // rbCredit
            // 
            this.rbCredit.AutoSize = true;
            this.rbCredit.Location = new System.Drawing.Point(249, 4);
            this.rbCredit.Name = "rbCredit";
            this.rbCredit.Size = new System.Drawing.Size(73, 17);
            this.rbCredit.TabIndex = 2;
            this.rbCredit.Text = "Jazz Cash";
            this.rbCredit.UseVisualStyleBackColor = true;
            this.rbCredit.CheckedChanged += new System.EventHandler(this.rbPaymentMode_CheckedChanged);
            // 
            // rbDebit
            // 
            this.rbDebit.AutoSize = true;
            this.rbDebit.Location = new System.Drawing.Point(153, 4);
            this.rbDebit.Name = "rbDebit";
            this.rbDebit.Size = new System.Drawing.Size(90, 17);
            this.rbDebit.TabIndex = 1;
            this.rbDebit.Text = "Pre-Paid Card";
            this.rbDebit.UseVisualStyleBackColor = true;
            this.rbDebit.CheckedChanged += new System.EventHandler(this.rbPaymentMode_CheckedChanged);
            // 
            // rbCash
            // 
            this.rbCash.AutoSize = true;
            this.rbCash.Checked = true;
            this.rbCash.Location = new System.Drawing.Point(8, 4);
            this.rbCash.Name = "rbCash";
            this.rbCash.Size = new System.Drawing.Size(49, 17);
            this.rbCash.TabIndex = 0;
            this.rbCash.TabStop = true;
            this.rbCash.Text = "Cash";
            this.rbCash.UseVisualStyleBackColor = true;
            this.rbCash.CheckedChanged += new System.EventHandler(this.rbPaymentMode_CheckedChanged);
            // 
            // txtPayment
            // 
            this.txtPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayment.Location = new System.Drawing.Point(67, 2);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(53, 20);
            this.txtPayment.TabIndex = 0;
            this.txtPayment.Text = "20";
            this.txtPayment.TextChanged += new System.EventHandler(this.txtPayment_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 149;
            this.label1.Text = "Rs";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnPrintReceipt
            // 
            this.btnPrintReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintReceipt.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnPrintReceipt.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintReceipt.Image")));
            this.btnPrintReceipt.Location = new System.Drawing.Point(682, 136);
            this.btnPrintReceipt.Name = "btnPrintReceipt";
            this.btnPrintReceipt.Size = new System.Drawing.Size(40, 40);
            this.btnPrintReceipt.TabIndex = 14;
            this.btnPrintReceipt.Tag = "no";
            this.btnPrintReceipt.Text = "Print Receipt";
            this.btnPrintReceipt.UseVisualStyleBackColor = true;
            this.btnPrintReceipt.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // txtEmpNo
            // 
            this.txtEmpNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmpNo.Enabled = false;
            this.txtEmpNo.Location = new System.Drawing.Point(654, 9);
            this.txtEmpNo.Name = "txtEmpNo";
            this.txtEmpNo.Size = new System.Drawing.Size(10, 20);
            this.txtEmpNo.TabIndex = 9;
            this.txtEmpNo.Visible = false;
            this.txtEmpNo.TextChanged += new System.EventHandler(this.txtEmpNo_TextChanged);
            // 
            // pnlMaritalStatus
            // 
            this.pnlMaritalStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaritalStatus.Controls.Add(this.rbMarried);
            this.pnlMaritalStatus.Controls.Add(this.rbSingle);
            this.pnlMaritalStatus.Location = new System.Drawing.Point(344, 61);
            this.pnlMaritalStatus.Name = "pnlMaritalStatus";
            this.pnlMaritalStatus.Size = new System.Drawing.Size(168, 24);
            this.pnlMaritalStatus.TabIndex = 4;
            this.pnlMaritalStatus.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMaritalStatus_Paint);
            // 
            // rbMarried
            // 
            this.rbMarried.AutoSize = true;
            this.rbMarried.Location = new System.Drawing.Point(82, 3);
            this.rbMarried.Name = "rbMarried";
            this.rbMarried.Size = new System.Drawing.Size(60, 17);
            this.rbMarried.TabIndex = 1;
            this.rbMarried.Text = "Married";
            this.rbMarried.UseVisualStyleBackColor = true;
            this.rbMarried.CheckedChanged += new System.EventHandler(this.rbMarried_CheckedChanged);
            // 
            // rbSingle
            // 
            this.rbSingle.AutoSize = true;
            this.rbSingle.Checked = true;
            this.rbSingle.Location = new System.Drawing.Point(15, 3);
            this.rbSingle.Name = "rbSingle";
            this.rbSingle.Size = new System.Drawing.Size(54, 17);
            this.rbSingle.TabIndex = 0;
            this.rbSingle.TabStop = true;
            this.rbSingle.Text = "Single";
            this.rbSingle.UseVisualStyleBackColor = true;
            this.rbSingle.CheckedChanged += new System.EventHandler(this.rbSingle_CheckedChanged);
            // 
            // lblPatientInfo
            // 
            this.lblPatientInfo.AutoSize = true;
            this.lblPatientInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientInfo.Location = new System.Drawing.Point(61, 7);
            this.lblPatientInfo.Name = "lblPatientInfo";
            this.lblPatientInfo.Size = new System.Drawing.Size(163, 20);
            this.lblPatientInfo.TabIndex = 159;
            this.lblPatientInfo.Text = "Patient Information";
            this.lblPatientInfo.Click += new System.EventHandler(this.lblPatientInfo_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(725, 136);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(40, 40);
            this.btnSave.TabIndex = 13;
            this.btnSave.Tag = "Save";
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(768, 136);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(40, 40);
            this.btnClear.TabIndex = 12;
            this.btnClear.Tag = "clear";
            this.btnClear.Text = "&Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblPanel
            // 
            this.lblPanel.AutoSize = true;
            this.lblPanel.Location = new System.Drawing.Point(166, 15);
            this.lblPanel.Name = "lblPanel";
            this.lblPanel.Size = new System.Drawing.Size(40, 13);
            this.lblPanel.TabIndex = 155;
            this.lblPanel.Text = "Panel :";
            this.lblPanel.Click += new System.EventHandler(this.lblPanel_Click);
            // 
            // cboPanel
            // 
            this.cboPanel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPanel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPanel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPanel.FormattingEnabled = true;
            this.cboPanel.Location = new System.Drawing.Point(384, 8);
            this.cboPanel.Name = "cboPanel";
            this.cboPanel.Size = new System.Drawing.Size(28, 21);
            this.cboPanel.TabIndex = 8;
            this.cboPanel.Visible = false;
            this.cboPanel.SelectedIndexChanged += new System.EventHandler(this.cboPanel_SelectedIndexChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Location = new System.Drawing.Point(345, 95);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(168, 20);
            this.txtEmail.TabIndex = 7;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 35);
            this.pictureBox2.TabIndex = 151;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // txtAge
            // 
            this.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAge.Location = new System.Drawing.Point(584, 63);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(32, 20);
            this.txtAge.TabIndex = 2;
            this.txtAge.TextChanged += new System.EventHandler(this.txtAge_TextChanged);
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(551, 65);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(32, 13);
            this.lblAge.TabIndex = 150;
            this.lblAge.Text = "Age :";
            this.lblAge.Click += new System.EventHandler(this.lblAge_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(542, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 20);
            this.panel2.TabIndex = 145;
            this.panel2.Tag = "no";
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(19, 123);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 13);
            this.lblAddress.TabIndex = 22;
            this.lblAddress.Text = "Address :";
            this.lblAddress.Click += new System.EventHandler(this.lblAddress_Click);
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(22, 67);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(48, 13);
            this.lblGender.TabIndex = 5;
            this.lblGender.Text = "Gender :";
            this.lblGender.Click += new System.EventHandler(this.lblGender_Click);
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(479, 8);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(64, 13);
            this.lblPhone.TabIndex = 2;
            this.lblPhone.Text = "Phone No. :";
            this.lblPhone.Visible = false;
            this.lblPhone.Click += new System.EventHandler(this.lblPhone_Click);
            // 
            // lblCell
            // 
            this.lblCell.AutoSize = true;
            this.lblCell.Location = new System.Drawing.Point(21, 96);
            this.lblCell.Name = "lblCell";
            this.lblCell.Size = new System.Drawing.Size(79, 13);
            this.lblCell.TabIndex = 1;
            this.lblCell.Text = "Whatsapp No :";
            this.lblCell.Click += new System.EventHandler(this.lblCell_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(269, 39);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name :";
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(618, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 158;
            this.label6.Text = "Y";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // lblEmpNo
            // 
            this.lblEmpNo.AutoSize = true;
            this.lblEmpNo.Enabled = false;
            this.lblEmpNo.Location = new System.Drawing.Point(419, 11);
            this.lblEmpNo.Name = "lblEmpNo";
            this.lblEmpNo.Size = new System.Drawing.Size(60, 17);
            this.lblEmpNo.TabIndex = 162;
            this.lblEmpNo.Text = "Panel No. :";
            this.lblEmpNo.UseCompatibleTextRendering = true;
            this.lblEmpNo.Visible = false;
            this.lblEmpNo.Click += new System.EventHandler(this.lblEmpNo_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(268, 97);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 153;
            this.lblEmail.Text = "Email :";
            this.lblEmail.Click += new System.EventHandler(this.lblEmail_Click);
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(551, 39);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(42, 13);
            this.lblDOB.TabIndex = 3;
            this.lblDOB.Text = "D.O.B :";
            this.lblDOB.Click += new System.EventHandler(this.lblDOB_Click);
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(269, 122);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(30, 13);
            this.lblCity.TabIndex = 24;
            this.lblCity.Text = "City :";
            this.lblCity.Click += new System.EventHandler(this.lblCity_Click);
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = true;
            this.lblMaritalStatus.Location = new System.Drawing.Point(267, 67);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(77, 13);
            this.lblMaritalStatus.TabIndex = 161;
            this.lblMaritalStatus.Text = "Marital Status :";
            this.lblMaritalStatus.Click += new System.EventHandler(this.lblMaritalStatus_Click);
            // 
            // lblPayment
            // 
            this.lblPayment.AutoSize = true;
            this.lblPayment.Location = new System.Drawing.Point(21, 155);
            this.lblPayment.Name = "lblPayment";
            this.lblPayment.Size = new System.Drawing.Size(54, 13);
            this.lblPayment.TabIndex = 164;
            this.lblPayment.Tag = "display";
            this.lblPayment.Text = "Payment :";
            this.lblPayment.Click += new System.EventHandler(this.lblPayment_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.pnlChangeAppoint);
            this.panel5.Controls.Add(this.dgAppointment);
            this.panel5.Controls.Add(this.cboConsultent);
            this.panel5.Controls.Add(this.btnSetting);
            this.panel5.Controls.Add(this.btnAppointment);
            this.panel5.Controls.Add(this.lblSelectedDate);
            this.panel5.Controls.Add(this.lblAppointment);
            this.panel5.Controls.Add(this.mcAppointment);
            this.panel5.Controls.Add(this.lblConsultent);
            this.panel5.Controls.Add(this.lblPRNO);
            this.panel5.Controls.Add(this.btnDeleteAll);
            this.panel5.Location = new System.Drawing.Point(1, 231);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(820, 463);
            this.panel5.TabIndex = 1;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // pnlChangeAppoint
            // 
            this.pnlChangeAppoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlChangeAppoint.Controls.Add(this.label2);
            this.pnlChangeAppoint.Controls.Add(this.dtpAppointDate);
            this.pnlChangeAppoint.Controls.Add(this.cboChangeConultant);
            this.pnlChangeAppoint.Controls.Add(this.btnAccept);
            this.pnlChangeAppoint.Controls.Add(this.btnChangeExit);
            this.pnlChangeAppoint.Location = new System.Drawing.Point(285, 2);
            this.pnlChangeAppoint.Name = "pnlChangeAppoint";
            this.pnlChangeAppoint.Size = new System.Drawing.Size(534, 61);
            this.pnlChangeAppoint.TabIndex = 17;
            this.pnlChangeAppoint.Visible = false;
            this.pnlChangeAppoint.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlChangeAppoint_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Consultant :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // dtpAppointDate
            // 
            this.dtpAppointDate.Location = new System.Drawing.Point(95, 33);
            this.dtpAppointDate.Name = "dtpAppointDate";
            this.dtpAppointDate.Size = new System.Drawing.Size(230, 20);
            this.dtpAppointDate.TabIndex = 3;
            this.dtpAppointDate.ValueChanged += new System.EventHandler(this.dtpAppointDate_ValueChanged);
            // 
            // cboChangeConultant
            // 
            this.cboChangeConultant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboChangeConultant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboChangeConultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChangeConultant.FormattingEnabled = true;
            this.cboChangeConultant.Location = new System.Drawing.Point(95, 6);
            this.cboChangeConultant.Name = "cboChangeConultant";
            this.cboChangeConultant.Size = new System.Drawing.Size(230, 21);
            this.cboChangeConultant.TabIndex = 2;
            this.cboChangeConultant.SelectedIndexChanged += new System.EventHandler(this.cboChangeConultant_SelectedIndexChanged);
            // 
            // btnChangeExit
            // 
            this.btnChangeExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnChangeExit.BackgroundImage")));
            this.btnChangeExit.Location = new System.Drawing.Point(481, 7);
            this.btnChangeExit.Name = "btnChangeExit";
            this.btnChangeExit.Size = new System.Drawing.Size(25, 25);
            this.btnChangeExit.TabIndex = 0;
            this.btnChangeExit.UseVisualStyleBackColor = true;
            this.btnChangeExit.Click += new System.EventHandler(this.btnChangeExit_Click);
            // 
            // dgAppointment
            // 
            this.dgAppointment.AllowUserToAddRows = false;
            this.dgAppointment.AllowUserToDeleteRows = false;
            this.dgAppointment.AllowUserToOrderColumns = true;
            this.dgAppointment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAppointment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgAppointment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.APatientID,
            this.APRNO,
            this.AName,
            this.ATokenNo,
            this.AppointmentID,
            this.AppointmentDate,
            this.AppointmentStartTime,
            this.AppointmentEndTime,
            this.AppointmentType,
            this.PersonID,
            this.VitalSign,
            this.opdid});
            this.dgAppointment.ContextMenuStrip = this.cmsAppointment;
            this.dgAppointment.EnableHeadersVisualStyles = false;
            this.dgAppointment.Location = new System.Drawing.Point(285, 66);
            this.dgAppointment.MultiSelect = false;
            this.dgAppointment.Name = "dgAppointment";
            this.dgAppointment.ReadOnly = true;
            this.dgAppointment.RowHeadersVisible = false;
            this.dgAppointment.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgAppointment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAppointment.Size = new System.Drawing.Size(534, 405);
            this.dgAppointment.TabIndex = 15;
            this.dgAppointment.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAppointment_CellClick);
            this.dgAppointment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAppointment_CellContentClick);
            this.dgAppointment.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAppointment_CellDoubleClick);
            this.dgAppointment.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgAppointment_MouseDown);
            // 
            // APatientID
            // 
            this.APatientID.DataPropertyName = "PatientID";
            this.APatientID.HeaderText = "PatientID";
            this.APatientID.Name = "APatientID";
            this.APatientID.ReadOnly = true;
            this.APatientID.Visible = false;
            // 
            // APRNO
            // 
            this.APRNO.DataPropertyName = "PRNo";
            this.APRNO.FillWeight = 60.51777F;
            this.APRNO.HeaderText = "PR No.";
            this.APRNO.Name = "APRNO";
            this.APRNO.ReadOnly = true;
            this.APRNO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AName
            // 
            this.AName.DataPropertyName = "Name";
            this.AName.FillWeight = 123.6393F;
            this.AName.HeaderText = "Name";
            this.AName.Name = "AName";
            this.AName.ReadOnly = true;
            this.AName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ATokenNo
            // 
            this.ATokenNo.DataPropertyName = "TokkenNo";
            this.ATokenNo.FillWeight = 15F;
            this.ATokenNo.HeaderText = "Queue #";
            this.ATokenNo.MinimumWidth = 2;
            this.ATokenNo.Name = "ATokenNo";
            this.ATokenNo.ReadOnly = true;
            this.ATokenNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ATokenNo.Visible = false;
            // 
            // AppointmentID
            // 
            this.AppointmentID.DataPropertyName = "AppointmentID";
            this.AppointmentID.HeaderText = "AppointmentID";
            this.AppointmentID.Name = "AppointmentID";
            this.AppointmentID.ReadOnly = true;
            this.AppointmentID.Visible = false;
            // 
            // AppointmentDate
            // 
            this.AppointmentDate.DataPropertyName = "AppointmentDate";
            this.AppointmentDate.FillWeight = 44.10345F;
            this.AppointmentDate.HeaderText = "AppointmentDate";
            this.AppointmentDate.Name = "AppointmentDate";
            this.AppointmentDate.ReadOnly = true;
            // 
            // AppointmentStartTime
            // 
            this.AppointmentStartTime.DataPropertyName = "AppointmentStartTime";
            this.AppointmentStartTime.HeaderText = "AppointmentStartTime";
            this.AppointmentStartTime.MinimumWidth = 2;
            this.AppointmentStartTime.Name = "AppointmentStartTime";
            this.AppointmentStartTime.ReadOnly = true;
            this.AppointmentStartTime.Visible = false;
            // 
            // AppointmentEndTime
            // 
            this.AppointmentEndTime.DataPropertyName = "AppointmentEndTime";
            this.AppointmentEndTime.FillWeight = 9.771574F;
            this.AppointmentEndTime.HeaderText = "AppointmentEndTime";
            this.AppointmentEndTime.Name = "AppointmentEndTime";
            this.AppointmentEndTime.ReadOnly = true;
            this.AppointmentEndTime.Visible = false;
            // 
            // AppointmentType
            // 
            this.AppointmentType.DataPropertyName = "AppointmentType";
            this.AppointmentType.FillWeight = 131.6591F;
            this.AppointmentType.HeaderText = "AppointmentType";
            this.AppointmentType.Name = "AppointmentType";
            this.AppointmentType.ReadOnly = true;
            this.AppointmentType.Visible = false;
            // 
            // PersonID
            // 
            this.PersonID.DataPropertyName = "PersonID";
            this.PersonID.HeaderText = "PersonID";
            this.PersonID.Name = "PersonID";
            this.PersonID.ReadOnly = true;
            this.PersonID.Visible = false;
            // 
            // VitalSign
            // 
            this.VitalSign.DataPropertyName = "VitalSign";
            this.VitalSign.FillWeight = 15.30885F;
            this.VitalSign.HeaderText = "Vital Sign";
            this.VitalSign.Name = "VitalSign";
            this.VitalSign.ReadOnly = true;
            this.VitalSign.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VitalSign.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // opdid
            // 
            this.opdid.DataPropertyName = "opdid";
            this.opdid.HeaderText = "opdid";
            this.opdid.Name = "opdid";
            this.opdid.ReadOnly = true;
            this.opdid.Visible = false;
            // 
            // cmsAppointment
            // 
            this.cmsAppointment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDown,
            this.miUp,
            this.miChangeAppoint,
            this.miCancelAppoint});
            this.cmsAppointment.Name = "cmsAppointment";
            this.cmsAppointment.Size = new System.Drawing.Size(236, 92);
            this.cmsAppointment.Opening += new System.ComponentModel.CancelEventHandler(this.cmsAppointment_Opening);
            // 
            // miDown
            // 
            this.miDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.miDown.Image = ((System.Drawing.Image)(resources.GetObject("miDown.Image")));
            this.miDown.Name = "miDown";
            this.miDown.Size = new System.Drawing.Size(235, 22);
            this.miDown.Text = "Down : Move Patient in Queue";
            this.miDown.Click += new System.EventHandler(this.miDown_Click);
            // 
            // miUp
            // 
            this.miUp.Image = ((System.Drawing.Image)(resources.GetObject("miUp.Image")));
            this.miUp.Name = "miUp";
            this.miUp.Size = new System.Drawing.Size(235, 22);
            this.miUp.Text = "Up : Move Patient in Queue";
            this.miUp.Click += new System.EventHandler(this.miUp_Click);
            // 
            // miChangeAppoint
            // 
            this.miChangeAppoint.Image = ((System.Drawing.Image)(resources.GetObject("miChangeAppoint.Image")));
            this.miChangeAppoint.Name = "miChangeAppoint";
            this.miChangeAppoint.Size = new System.Drawing.Size(235, 22);
            this.miChangeAppoint.Text = "Change Appointment";
            this.miChangeAppoint.Click += new System.EventHandler(this.miChangeAppoint_Click);
            // 
            // miCancelAppoint
            // 
            this.miCancelAppoint.Name = "miCancelAppoint";
            this.miCancelAppoint.Size = new System.Drawing.Size(235, 22);
            this.miCancelAppoint.Text = "Cancel Appointment";
            this.miCancelAppoint.Click += new System.EventHandler(this.miCancelAppoint_Click);
            // 
            // cboConsultent
            // 
            this.cboConsultent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboConsultent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboConsultent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConsultent.FormattingEnabled = true;
            this.cboConsultent.Location = new System.Drawing.Point(63, 10);
            this.cboConsultent.Name = "cboConsultent";
            this.cboConsultent.Size = new System.Drawing.Size(147, 21);
            this.cboConsultent.TabIndex = 131;
            this.cboConsultent.SelectedIndexChanged += new System.EventHandler(this.cboConsultent_SelectedIndexChanged);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(248, 0);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(35, 35);
            this.btnSetting.TabIndex = 130;
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnAppointment
            // 
            this.btnAppointment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAppointment.BackgroundImage")));
            this.btnAppointment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAppointment.Location = new System.Drawing.Point(211, 2);
            this.btnAppointment.Name = "btnAppointment";
            this.btnAppointment.Size = new System.Drawing.Size(35, 33);
            this.btnAppointment.TabIndex = 19;
            this.btnAppointment.UseVisualStyleBackColor = true;
            this.btnAppointment.Click += new System.EventHandler(this.btnAppointment_Click);
            // 
            // lblSelectedDate
            // 
            this.lblSelectedDate.AutoSize = true;
            this.lblSelectedDate.Location = new System.Drawing.Point(480, 17);
            this.lblSelectedDate.Name = "lblSelectedDate";
            this.lblSelectedDate.Size = new System.Drawing.Size(35, 13);
            this.lblSelectedDate.TabIndex = 16;
            this.lblSelectedDate.Text = "label2";
            this.lblSelectedDate.Click += new System.EventHandler(this.lblSelectedDate_Click);
            // 
            // lblAppointment
            // 
            this.lblAppointment.AutoSize = true;
            this.lblAppointment.Location = new System.Drawing.Point(641, 15);
            this.lblAppointment.Name = "lblAppointment";
            this.lblAppointment.Size = new System.Drawing.Size(0, 13);
            this.lblAppointment.TabIndex = 8;
            this.lblAppointment.Tag = "display";
            this.lblAppointment.Click += new System.EventHandler(this.lblAppointment_Click);
            // 
            // mcAppointment
            // 
            this.mcAppointment.ActiveMonth.Month = 2;
            this.mcAppointment.ActiveMonth.Year = 2020;
            this.mcAppointment.AllowDrop = true;
            this.mcAppointment.Culture = new System.Globalization.CultureInfo("en-US");
            this.mcAppointment.Dates.AddRange(new Pabo.Calendar.DateItem[] {
            this.dateItem1,
            this.dateItem2,
            this.dateItem3});
            this.mcAppointment.Footer.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Footer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.mcAppointment.Footer.Format = Pabo.Calendar.mcTodayFormat.Long;
            this.mcAppointment.Header.BackColor1 = System.Drawing.Color.RoyalBlue;
            this.mcAppointment.Header.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Header.TextColor = System.Drawing.Color.White;
            this.mcAppointment.ImageList = null;
            this.mcAppointment.Location = new System.Drawing.Point(1, 36);
            this.mcAppointment.MaxDate = new System.DateTime(2060, 1, 28, 0, 0, 0, 0);
            this.mcAppointment.MinDate = new System.DateTime(2020, 2, 20, 0, 0, 0, 0);
            this.mcAppointment.Month.BackgroundImage = null;
            this.mcAppointment.Month.BorderStyles.Normal = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.mcAppointment.Month.Colors.BackColor1 = System.Drawing.Color.Lavender;
            this.mcAppointment.Month.Colors.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.mcAppointment.Month.Colors.Days.BackColor1 = System.Drawing.Color.Azure;
            this.mcAppointment.Month.Colors.Disabled.BackColor1 = System.Drawing.Color.Silver;
            this.mcAppointment.Month.Colors.Disabled.BackColor2 = System.Drawing.Color.DarkGray;
            this.mcAppointment.Month.Colors.Selected.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.mcAppointment.Month.Colors.Weekend.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Month.Colors.Weekend.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.Month.DateAlign = Pabo.Calendar.mcItemAlign.TopRight;
            this.mcAppointment.Month.DateFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.mcAppointment.Month.TextAlign = Pabo.Calendar.mcItemAlign.Center;
            this.mcAppointment.Month.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.mcAppointment.Name = "mcAppointment";
            this.mcAppointment.SelectButton = System.Windows.Forms.MouseButtons.None;
            this.mcAppointment.SelectionMode = Pabo.Calendar.mcSelectionMode.One;
            this.mcAppointment.SelectTrailingDates = false;
            this.mcAppointment.ShowTrailingDates = false;
            this.mcAppointment.Size = new System.Drawing.Size(282, 424);
            this.mcAppointment.TabIndex = 14;
            this.mcAppointment.Weekdays.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Weekdays.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Weekdays.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.Weekdays.TextColor = System.Drawing.Color.Black;
            this.mcAppointment.Weeknumbers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Weeknumbers.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.MonthChanged += new Pabo.Calendar.MonthChangedEventHandler(this.mcAppointment_MonthChanged);
            this.mcAppointment.DayClick += new Pabo.Calendar.DayClickEventHandler(this.mcAppointment_DayClick);
            this.mcAppointment.DaySelected += new Pabo.Calendar.DaySelectedEventHandler(this.mcAppointment_DaySelected);
            // 
            // dateItem1
            // 
            this.dateItem1.BackColor1 = System.Drawing.Color.Empty;
            this.dateItem1.BackColor2 = System.Drawing.Color.White;
            this.dateItem1.BackgroundImage = null;
            this.dateItem1.BoldedDate = false;
            this.dateItem1.Date = new System.DateTime(2020, 6, 6, 0, 0, 0, 0);
            this.dateItem1.DateColor = System.Drawing.Color.Empty;
            this.dateItem1.Enabled = true;
            this.dateItem1.GradientMode = Pabo.Calendar.mcGradientMode.None;
            this.dateItem1.Image = null;
            this.dateItem1.ImageListIndex = -1;
            this.dateItem1.Pattern = Pabo.Calendar.mcDayInfoRecurrence.None;
            this.dateItem1.Range = new System.DateTime(2020, 6, 15, 0, 0, 0, 0);
            this.dateItem1.Tag = null;
            this.dateItem1.Text = "";
            this.dateItem1.TextColor = System.Drawing.Color.Empty;
            this.dateItem1.Weekend = false;
            // 
            // dateItem2
            // 
            this.dateItem2.BackColor1 = System.Drawing.Color.Empty;
            this.dateItem2.BackColor2 = System.Drawing.Color.White;
            this.dateItem2.BackgroundImage = null;
            this.dateItem2.BoldedDate = false;
            this.dateItem2.Date = new System.DateTime(2020, 6, 7, 0, 0, 0, 0);
            this.dateItem2.DateColor = System.Drawing.Color.Empty;
            this.dateItem2.Enabled = true;
            this.dateItem2.GradientMode = Pabo.Calendar.mcGradientMode.None;
            this.dateItem2.Image = null;
            this.dateItem2.ImageListIndex = -1;
            this.dateItem2.Pattern = Pabo.Calendar.mcDayInfoRecurrence.None;
            this.dateItem2.Range = new System.DateTime(2020, 6, 15, 0, 0, 0, 0);
            this.dateItem2.Tag = null;
            this.dateItem2.Text = "";
            this.dateItem2.TextColor = System.Drawing.Color.Empty;
            this.dateItem2.Weekend = false;
            // 
            // dateItem3
            // 
            this.dateItem3.BackColor1 = System.Drawing.Color.Empty;
            this.dateItem3.BackColor2 = System.Drawing.Color.White;
            this.dateItem3.BackgroundImage = null;
            this.dateItem3.BoldedDate = false;
            this.dateItem3.Date = new System.DateTime(2020, 6, 8, 0, 0, 0, 0);
            this.dateItem3.DateColor = System.Drawing.Color.Empty;
            this.dateItem3.Enabled = true;
            this.dateItem3.GradientMode = Pabo.Calendar.mcGradientMode.None;
            this.dateItem3.Image = null;
            this.dateItem3.ImageListIndex = -1;
            this.dateItem3.Pattern = Pabo.Calendar.mcDayInfoRecurrence.None;
            this.dateItem3.Range = new System.DateTime(2020, 6, 15, 0, 0, 0, 0);
            this.dateItem3.Tag = null;
            this.dateItem3.Text = "";
            this.dateItem3.TextColor = System.Drawing.Color.Empty;
            this.dateItem3.Weekend = false;
            // 
            // lblConsultent
            // 
            this.lblConsultent.AutoSize = true;
            this.lblConsultent.Location = new System.Drawing.Point(0, 13);
            this.lblConsultent.Name = "lblConsultent";
            this.lblConsultent.Size = new System.Drawing.Size(63, 13);
            this.lblConsultent.TabIndex = 13;
            this.lblConsultent.Text = "Consultant :";
            this.lblConsultent.Click += new System.EventHandler(this.lblConsultent_Click);
            // 
            // lblPRNO
            // 
            this.lblPRNO.AutoSize = true;
            this.lblPRNO.Location = new System.Drawing.Point(34, 283);
            this.lblPRNO.Name = "lblPRNO";
            this.lblPRNO.Size = new System.Drawing.Size(48, 13);
            this.lblPRNO.TabIndex = 0;
            this.lblPRNO.Text = "lblPRNO";
            this.lblPRNO.Visible = false;
            this.lblPRNO.Click += new System.EventHandler(this.lblPRNO_Click);
            // 
            // dgSummary
            // 
            this.dgSummary.AllowDrop = true;
            this.dgSummary.AllowUserToAddRows = false;
            this.dgSummary.AllowUserToDeleteRows = false;
            this.dgSummary.AllowUserToResizeColumns = false;
            this.dgSummary.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OPDIDD,
            this.SPRNo,
            this.SPName,
            this.SAge,
            this.SStatus,
            this.Video});
            this.dgSummary.EnableHeadersVisualStyles = false;
            this.dgSummary.Location = new System.Drawing.Point(1, 127);
            this.dgSummary.MultiSelect = false;
            this.dgSummary.Name = "dgSummary";
            this.dgSummary.ReadOnly = true;
            this.dgSummary.RowHeadersVisible = false;
            this.dgSummary.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSummary.Size = new System.Drawing.Size(361, 560);
            this.dgSummary.TabIndex = 40;
            this.dgSummary.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSummary_CellClick);
            this.dgSummary.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSummary_CellContentClick);
            // 
            // OPDIDD
            // 
            this.OPDIDD.HeaderText = "OPD Id";
            this.OPDIDD.Name = "OPDIDD";
            this.OPDIDD.ReadOnly = true;
            this.OPDIDD.Visible = false;
            this.OPDIDD.Width = 5;
            // 
            // SPRNo
            // 
            this.SPRNo.DataPropertyName = "PRNo";
            this.SPRNo.HeaderText = "MR No.";
            this.SPRNo.Name = "SPRNo";
            this.SPRNo.ReadOnly = true;
            this.SPRNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SPName
            // 
            this.SPName.DataPropertyName = "PName";
            this.SPName.HeaderText = "Name";
            this.SPName.Name = "SPName";
            this.SPName.ReadOnly = true;
            this.SPName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SPName.Width = 160;
            // 
            // SAge
            // 
            this.SAge.DataPropertyName = "Age";
            this.SAge.HeaderText = "Age";
            this.SAge.Name = "SAge";
            this.SAge.ReadOnly = true;
            this.SAge.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SAge.Width = 50;
            // 
            // SStatus
            // 
            this.SStatus.DataPropertyName = "Status";
            this.SStatus.HeaderText = "Status";
            this.SStatus.Name = "SStatus";
            this.SStatus.ReadOnly = true;
            this.SStatus.Visible = false;
            // 
            // Video
            // 
            this.Video.DataPropertyName = "Video";
            this.Video.HeaderText = "Video";
            this.Video.Name = "Video";
            this.Video.ReadOnly = true;
            // 
            // pnlSummry
            // 
            this.pnlSummry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSummry.Controls.Add(this.btnTomorrow);
            this.pnlSummry.Controls.Add(this.btnNextTomorrow);
            this.pnlSummry.Controls.Add(this.pnlVitalSigns);
            this.pnlSummry.Controls.Add(this.btnDoctorsCashReport);
            this.pnlSummry.Controls.Add(this.panel8);
            this.pnlSummry.Controls.Add(this.label7);
            this.pnlSummry.Controls.Add(this.dgSummary);
            this.pnlSummry.Location = new System.Drawing.Point(822, 2);
            this.pnlSummry.Name = "pnlSummry";
            this.pnlSummry.Size = new System.Drawing.Size(367, 692);
            this.pnlSummry.TabIndex = 41;
            this.pnlSummry.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSummry_Paint);
            // 
            // btnTomorrow
            // 
            this.btnTomorrow.Location = new System.Drawing.Point(54, 28);
            this.btnTomorrow.Name = "btnTomorrow";
            this.btnTomorrow.Size = new System.Drawing.Size(271, 26);
            this.btnTomorrow.TabIndex = 45;
            this.btnTomorrow.Text = "Reminder Email for Tomorrow  Appointment";
            this.btnTomorrow.UseVisualStyleBackColor = true;
            this.btnTomorrow.Click += new System.EventHandler(this.btnTomorrow_Click);
            // 
            // btnNextTomorrow
            // 
            this.btnNextTomorrow.Location = new System.Drawing.Point(55, 54);
            this.btnNextTomorrow.Name = "btnNextTomorrow";
            this.btnNextTomorrow.Size = new System.Drawing.Size(270, 26);
            this.btnNextTomorrow.TabIndex = 44;
            this.btnNextTomorrow.Text = "Reminder Email for Next to Tomorrow Appointment";
            this.btnNextTomorrow.UseVisualStyleBackColor = true;
            this.btnNextTomorrow.Click += new System.EventHandler(this.btnNextTomorrow_Click);
            // 
            // pnlVitalSigns
            // 
            this.pnlVitalSigns.BackColor = System.Drawing.Color.Lavender;
            this.pnlVitalSigns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVitalSigns.Controls.Add(this.txtRespiratoryRate);
            this.pnlVitalSigns.Controls.Add(this.txtHeartRate);
            this.pnlVitalSigns.Controls.Add(this.lblRespiratoryRate);
            this.pnlVitalSigns.Controls.Add(this.lblHeartRate);
            this.pnlVitalSigns.Controls.Add(this.btnVSSave);
            this.pnlVitalSigns.Controls.Add(this.txtVSPRNO);
            this.pnlVitalSigns.Controls.Add(this.label8);
            this.pnlVitalSigns.Controls.Add(this.btnVitalSignClose);
            this.pnlVitalSigns.Controls.Add(this.lblBSAUnit);
            this.pnlVitalSigns.Controls.Add(this.txtBMIStatus);
            this.pnlVitalSigns.Controls.Add(this.lblBMIUnit);
            this.pnlVitalSigns.Controls.Add(this.lblVitalSigns);
            this.pnlVitalSigns.Controls.Add(this.txtBSA);
            this.pnlVitalSigns.Controls.Add(this.txtBMI);
            this.pnlVitalSigns.Controls.Add(this.txtBP);
            this.pnlVitalSigns.Controls.Add(this.cboHeightUnit);
            this.pnlVitalSigns.Controls.Add(this.cboWeightUnit);
            this.pnlVitalSigns.Controls.Add(this.cboTempUnit);
            this.pnlVitalSigns.Controls.Add(this.txtHeight);
            this.pnlVitalSigns.Controls.Add(this.txtWeight);
            this.pnlVitalSigns.Controls.Add(this.txtPulse);
            this.pnlVitalSigns.Controls.Add(this.txtTemp);
            this.pnlVitalSigns.Controls.Add(this.txtHeadCircum);
            this.pnlVitalSigns.Controls.Add(this.lblHeadCircumUnit);
            this.pnlVitalSigns.Controls.Add(this.lblPulseUnit);
            this.pnlVitalSigns.Controls.Add(this.lblBPUnit);
            this.pnlVitalSigns.Controls.Add(this.lblHeight);
            this.pnlVitalSigns.Controls.Add(this.lblPulse);
            this.pnlVitalSigns.Controls.Add(this.lblBSA);
            this.pnlVitalSigns.Controls.Add(this.lblBMI);
            this.pnlVitalSigns.Controls.Add(this.lblBMIStatus);
            this.pnlVitalSigns.Controls.Add(this.lblWeight);
            this.pnlVitalSigns.Controls.Add(this.lblBP);
            this.pnlVitalSigns.Controls.Add(this.lblHeadCircum);
            this.pnlVitalSigns.Controls.Add(this.lblTemp);
            this.pnlVitalSigns.Location = new System.Drawing.Point(4, 295);
            this.pnlVitalSigns.Name = "pnlVitalSigns";
            this.pnlVitalSigns.Size = new System.Drawing.Size(358, 364);
            this.pnlVitalSigns.TabIndex = 0;
            this.pnlVitalSigns.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlVitalSigns_Paint);
            // 
            // txtRespiratoryRate
            // 
            this.txtRespiratoryRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRespiratoryRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespiratoryRate.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtRespiratoryRate.Location = new System.Drawing.Point(124, 253);
            this.txtRespiratoryRate.Name = "txtRespiratoryRate";
            this.txtRespiratoryRate.Size = new System.Drawing.Size(77, 21);
            this.txtRespiratoryRate.TabIndex = 149;
            this.txtRespiratoryRate.TabStop = false;
            this.txtRespiratoryRate.Tag = "Notset";
            this.txtRespiratoryRate.TextChanged += new System.EventHandler(this.txtRespiratoryRate_TextChanged);
            this.txtRespiratoryRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // txtHeartRate
            // 
            this.txtHeartRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeartRate.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtHeartRate.Location = new System.Drawing.Point(124, 228);
            this.txtHeartRate.Name = "txtHeartRate";
            this.txtHeartRate.Size = new System.Drawing.Size(77, 21);
            this.txtHeartRate.TabIndex = 148;
            this.txtHeartRate.TabStop = false;
            this.txtHeartRate.Tag = "Notset";
            this.txtHeartRate.TextChanged += new System.EventHandler(this.txtHeartRate_TextChanged);
            this.txtHeartRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidation_KeyPress);
            // 
            // lblRespiratoryRate
            // 
            this.lblRespiratoryRate.AutoSize = true;
            this.lblRespiratoryRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespiratoryRate.ForeColor = System.Drawing.Color.Black;
            this.lblRespiratoryRate.Location = new System.Drawing.Point(19, 257);
            this.lblRespiratoryRate.Name = "lblRespiratoryRate";
            this.lblRespiratoryRate.Size = new System.Drawing.Size(104, 15);
            this.lblRespiratoryRate.TabIndex = 147;
            this.lblRespiratoryRate.Tag = "NotSet";
            this.lblRespiratoryRate.Text = "Respiratory Rate :";
            this.lblRespiratoryRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRespiratoryRate.Click += new System.EventHandler(this.lblRespiratoryRate_Click);
            // 
            // lblHeartRate
            // 
            this.lblHeartRate.AutoSize = true;
            this.lblHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblHeartRate.Location = new System.Drawing.Point(51, 234);
            this.lblHeartRate.Name = "lblHeartRate";
            this.lblHeartRate.Size = new System.Drawing.Size(72, 15);
            this.lblHeartRate.TabIndex = 146;
            this.lblHeartRate.Tag = "NotSet";
            this.lblHeartRate.Text = "Heart Rate :";
            this.lblHeartRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHeartRate.Click += new System.EventHandler(this.lblHeartRate_Click);
            // 
            // btnVSSave
            // 
            this.btnVSSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnVSSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVSSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVSSave.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnVSSave.Image = ((System.Drawing.Image)(resources.GetObject("btnVSSave.Image")));
            this.btnVSSave.Location = new System.Drawing.Point(222, 0);
            this.btnVSSave.Name = "btnVSSave";
            this.btnVSSave.Size = new System.Drawing.Size(40, 40);
            this.btnVSSave.TabIndex = 11;
            this.btnVSSave.Tag = "Save";
            this.btnVSSave.Text = "&Save";
            this.btnVSSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnVSSave.UseVisualStyleBackColor = false;
            this.btnVSSave.Click += new System.EventHandler(this.btnVSSave_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(28, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 15);
            this.label8.TabIndex = 140;
            this.label8.Text = "MR No. :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // btnVitalSignClose
            // 
            this.btnVitalSignClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnVitalSignClose.BackgroundImage")));
            this.btnVitalSignClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnVitalSignClose.Location = new System.Drawing.Point(262, 0);
            this.btnVitalSignClose.Name = "btnVitalSignClose";
            this.btnVitalSignClose.Size = new System.Drawing.Size(40, 40);
            this.btnVitalSignClose.TabIndex = 139;
            this.btnVitalSignClose.UseVisualStyleBackColor = true;
            this.btnVitalSignClose.Click += new System.EventHandler(this.btnVitalSignClose_Click);
            // 
            // lblBSAUnit
            // 
            this.lblBSAUnit.AutoSize = true;
            this.lblBSAUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSAUnit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBSAUnit.Location = new System.Drawing.Point(203, 306);
            this.lblBSAUnit.Name = "lblBSAUnit";
            this.lblBSAUnit.Size = new System.Drawing.Size(37, 15);
            this.lblBSAUnit.TabIndex = 138;
            this.lblBSAUnit.Tag = "NotSet";
            this.lblBSAUnit.Text = "m*m";
            this.lblBSAUnit.Click += new System.EventHandler(this.lblBSAUnit_Click);
            // 
            // txtBMIStatus
            // 
            this.txtBMIStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBMIStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBMIStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBMIStatus.Location = new System.Drawing.Point(124, 328);
            this.txtBMIStatus.Name = "txtBMIStatus";
            this.txtBMIStatus.ReadOnly = true;
            this.txtBMIStatus.Size = new System.Drawing.Size(77, 21);
            this.txtBMIStatus.TabIndex = 136;
            this.txtBMIStatus.TabStop = false;
            this.txtBMIStatus.Tag = "Notset";
            this.txtBMIStatus.TextChanged += new System.EventHandler(this.txtBMIStatus_TextChanged);
            // 
            // lblBMIUnit
            // 
            this.lblBMIUnit.AutoSize = true;
            this.lblBMIUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMIUnit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMIUnit.Location = new System.Drawing.Point(203, 282);
            this.lblBMIUnit.Name = "lblBMIUnit";
            this.lblBMIUnit.Size = new System.Drawing.Size(58, 15);
            this.lblBMIUnit.TabIndex = 14;
            this.lblBMIUnit.Tag = "NotSet";
            this.lblBMIUnit.Text = "Kg/m*m";
            this.lblBMIUnit.Click += new System.EventHandler(this.lblBMIUnit_Click);
            // 
            // lblVitalSigns
            // 
            this.lblVitalSigns.AutoSize = true;
            this.lblVitalSigns.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblVitalSigns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVitalSigns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVitalSigns.Location = new System.Drawing.Point(115, 2);
            this.lblVitalSigns.Name = "lblVitalSigns";
            this.lblVitalSigns.Size = new System.Drawing.Size(92, 18);
            this.lblVitalSigns.TabIndex = 9;
            this.lblVitalSigns.Tag = "self";
            this.lblVitalSigns.Text = "Vital Signs :";
            this.lblVitalSigns.Click += new System.EventHandler(this.lblVitalSigns_Click);
            // 
            // lblHeadCircumUnit
            // 
            this.lblHeadCircumUnit.AutoSize = true;
            this.lblHeadCircumUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadCircumUnit.Location = new System.Drawing.Point(203, 207);
            this.lblHeadCircumUnit.Name = "lblHeadCircumUnit";
            this.lblHeadCircumUnit.Size = new System.Drawing.Size(24, 15);
            this.lblHeadCircumUnit.TabIndex = 6;
            this.lblHeadCircumUnit.Text = "cm";
            this.lblHeadCircumUnit.Click += new System.EventHandler(this.lblHeadCircumUnit_Click);
            // 
            // lblPulseUnit
            // 
            this.lblPulseUnit.AutoSize = true;
            this.lblPulseUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPulseUnit.Location = new System.Drawing.Point(203, 110);
            this.lblPulseUnit.Name = "lblPulseUnit";
            this.lblPulseUnit.Size = new System.Drawing.Size(62, 15);
            this.lblPulseUnit.TabIndex = 5;
            this.lblPulseUnit.Text = "Pulse/Min";
            this.lblPulseUnit.Click += new System.EventHandler(this.lblPulseUnit_Click);
            // 
            // lblBPUnit
            // 
            this.lblBPUnit.AutoSize = true;
            this.lblBPUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBPUnit.Location = new System.Drawing.Point(203, 86);
            this.lblBPUnit.Name = "lblBPUnit";
            this.lblBPUnit.Size = new System.Drawing.Size(48, 15);
            this.lblBPUnit.TabIndex = 4;
            this.lblBPUnit.Text = "mm/Hg";
            this.lblBPUnit.Click += new System.EventHandler(this.lblBPUnit_Click);
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(74, 182);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(49, 15);
            this.lblHeight.TabIndex = 9;
            this.lblHeight.Text = "Height :";
            this.lblHeight.Click += new System.EventHandler(this.lblHeight_Click);
            // 
            // lblPulse
            // 
            this.lblPulse.AutoSize = true;
            this.lblPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPulse.Location = new System.Drawing.Point(79, 110);
            this.lblPulse.Name = "lblPulse";
            this.lblPulse.Size = new System.Drawing.Size(44, 15);
            this.lblPulse.TabIndex = 3;
            this.lblPulse.Text = "Pulse :";
            this.lblPulse.Click += new System.EventHandler(this.lblPulse_Click);
            // 
            // lblBSA
            // 
            this.lblBSA.AutoSize = true;
            this.lblBSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSA.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBSA.Location = new System.Drawing.Point(82, 306);
            this.lblBSA.Name = "lblBSA";
            this.lblBSA.Size = new System.Drawing.Size(41, 15);
            this.lblBSA.TabIndex = 11;
            this.lblBSA.Tag = "NotSet";
            this.lblBSA.Text = "BSA :";
            this.lblBSA.Click += new System.EventHandler(this.lblBSA_Click);
            // 
            // lblBMI
            // 
            this.lblBMI.AutoSize = true;
            this.lblBMI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMI.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMI.Location = new System.Drawing.Point(83, 282);
            this.lblBMI.Name = "lblBMI";
            this.lblBMI.Size = new System.Drawing.Size(40, 15);
            this.lblBMI.TabIndex = 10;
            this.lblBMI.Tag = "NotSet";
            this.lblBMI.Text = "BMI :";
            this.lblBMI.Click += new System.EventHandler(this.lblBMI_Click);
            // 
            // lblBMIStatus
            // 
            this.lblBMIStatus.AutoSize = true;
            this.lblBMIStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMIStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMIStatus.Location = new System.Drawing.Point(39, 330);
            this.lblBMIStatus.Name = "lblBMIStatus";
            this.lblBMIStatus.Size = new System.Drawing.Size(84, 15);
            this.lblBMIStatus.TabIndex = 134;
            this.lblBMIStatus.Tag = "NotSet";
            this.lblBMIStatus.Text = "BMI Status :";
            this.lblBMIStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBMIStatus.Click += new System.EventHandler(this.lblBMIStatus_Click);
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(72, 158);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(51, 15);
            this.lblWeight.TabIndex = 8;
            this.lblWeight.Text = "Weight :";
            this.lblWeight.Click += new System.EventHandler(this.lblWeight_Click);
            // 
            // lblBP
            // 
            this.lblBP.AutoSize = true;
            this.lblBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBP.Location = new System.Drawing.Point(94, 86);
            this.lblBP.Name = "lblBP";
            this.lblBP.Size = new System.Drawing.Size(29, 15);
            this.lblBP.TabIndex = 2;
            this.lblBP.Text = "BP :";
            this.lblBP.Click += new System.EventHandler(this.lblBP_Click);
            // 
            // lblHeadCircum
            // 
            this.lblHeadCircum.AutoSize = true;
            this.lblHeadCircum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadCircum.Location = new System.Drawing.Point(35, 207);
            this.lblHeadCircum.Name = "lblHeadCircum";
            this.lblHeadCircum.Size = new System.Drawing.Size(88, 15);
            this.lblHeadCircum.TabIndex = 7;
            this.lblHeadCircum.Text = "Head Circum. :";
            this.lblHeadCircum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHeadCircum.Click += new System.EventHandler(this.lblHeadCircum_Click);
            // 
            // lblTemp
            // 
            this.lblTemp.AutoSize = true;
            this.lblTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp.Location = new System.Drawing.Point(78, 134);
            this.lblTemp.Name = "lblTemp";
            this.lblTemp.Size = new System.Drawing.Size(45, 15);
            this.lblTemp.TabIndex = 1;
            this.lblTemp.Text = "Temp :";
            this.lblTemp.Click += new System.EventHandler(this.lblTemp_Click);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.lblConsulted);
            this.panel8.Controls.Add(this.lblHold);
            this.panel8.Controls.Add(this.lblWait);
            this.panel8.Location = new System.Drawing.Point(1, 99);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(361, 29);
            this.panel8.TabIndex = 42;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // lblConsulted
            // 
            this.lblConsulted.AutoSize = true;
            this.lblConsulted.BackColor = System.Drawing.Color.Azure;
            this.lblConsulted.Location = new System.Drawing.Point(282, 7);
            this.lblConsulted.Name = "lblConsulted";
            this.lblConsulted.Size = new System.Drawing.Size(54, 13);
            this.lblConsulted.TabIndex = 2;
            this.lblConsulted.Text = "Consulted";
            this.lblConsulted.Click += new System.EventHandler(this.lblConsulted_Click);
            // 
            // lblHold
            // 
            this.lblHold.AutoSize = true;
            this.lblHold.BackColor = System.Drawing.Color.SeaShell;
            this.lblHold.Location = new System.Drawing.Point(169, 7);
            this.lblHold.Name = "lblHold";
            this.lblHold.Size = new System.Drawing.Size(29, 13);
            this.lblHold.TabIndex = 1;
            this.lblHold.Text = "Hold";
            this.lblHold.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHold.Click += new System.EventHandler(this.lblHold_Click);
            // 
            // lblWait
            // 
            this.lblWait.AutoSize = true;
            this.lblWait.BackColor = System.Drawing.Color.White;
            this.lblWait.Location = new System.Drawing.Point(32, 7);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(43, 13);
            this.lblWait.TabIndex = 0;
            this.lblWait.Text = "Waiting";
            this.lblWait.Click += new System.EventHandler(this.lblWait_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(127, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 20);
            this.label7.TabIndex = 41;
            this.label7.Text = "Today Summary";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblOPDID
            // 
            this.lblOPDID.AutoSize = true;
            this.lblOPDID.Location = new System.Drawing.Point(488, 526);
            this.lblOPDID.Name = "lblOPDID";
            this.lblOPDID.Size = new System.Drawing.Size(51, 13);
            this.lblOPDID.TabIndex = 42;
            this.lblOPDID.Text = "lblOPDID";
            this.lblOPDID.Visible = false;
            this.lblOPDID.Click += new System.EventHandler(this.lblOPDID_Click);
            // 
            // frmPatientReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 696);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pnlSummry);
            this.Controls.Add(this.lblPatientID);
            this.Controls.Add(this.lblOPDID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPatientReg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.Text = "Patient Registration";
            this.Load += new System.EventHandler(this.frmPatientReg_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPatient)).EndInit();
            this.panel3.ResumeLayout(false);
            this.pnlRegisterSearch.ResumeLayout(false);
            this.pnlRegisterSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatientReg)).EndInit();
            this.cmsPatientReg.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.pnlPaymentMode.ResumeLayout(false);
            this.pnlPaymentMode.PerformLayout();
            this.pnlMaritalStatus.ResumeLayout(false);
            this.pnlMaritalStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pnlChangeAppoint.ResumeLayout(false);
            this.pnlChangeAppoint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppointment)).EndInit();
            this.cmsAppointment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSummary)).EndInit();
            this.pnlSummry.ResumeLayout(false);
            this.pnlSummry.PerformLayout();
            this.pnlVitalSigns.ResumeLayout(false);
            this.pnlVitalSigns.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblPatientID;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.DataGridView dgPatientReg;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblCell;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtCell;
        private System.Windows.Forms.DateTimePicker dtpDateBirth;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.ComboBox cboCity;
			private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPayment;
        private System.Windows.Forms.Panel pnlRegisterSearch;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.MaskedTextBox txtSearchPRNO;
        private System.Windows.Forms.TextBox txtSearchPhone;
        private System.Windows.Forms.TextBox txtSearchName;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Button btnPatientSearch;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblSearchPRNO;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPanel;
        private System.Windows.Forms.ComboBox cboPanel;
      private System.Windows.Forms.Label lblAppointment;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label6;
			private System.Windows.Forms.Label lblPatientInfo;
        private System.Windows.Forms.Label lblPatientSearchHeading;
			private System.Windows.Forms.Panel pnlPaymentMode;
			private System.Windows.Forms.RadioButton rbCredit;
			private System.Windows.Forms.RadioButton rbDebit;
			private System.Windows.Forms.RadioButton rbCash;
			private System.Windows.Forms.TextBox txtRefNo;
			private System.Windows.Forms.Label lblRefNo;
			private System.Windows.Forms.Label lblConsultent;
			private System.Windows.Forms.TextBox txtEmpNo;
			private System.Windows.Forms.Label lblEmpNo;
			private System.Windows.Forms.Label lblMaritalStatus;
			private System.Windows.Forms.Panel pnlMaritalStatus;
			private System.Windows.Forms.RadioButton rbMarried;
			private System.Windows.Forms.RadioButton rbSingle;
			private System.Windows.Forms.Label lblPRNO;
			private System.Windows.Forms.DataGridView dgSummary;
			private System.Windows.Forms.Panel pnlSummry;
        private System.Windows.Forms.Label label7;
			private System.Windows.Forms.Panel panel8;
			private System.Windows.Forms.Label lblConsulted;
			private System.Windows.Forms.Label lblHold;
        private System.Windows.Forms.Label lblWait;
			private System.Windows.Forms.Label lblOPDID;
			private Pabo.Calendar.MonthCalendar mcAppointment;
        private System.Windows.Forms.Button btnPrintReceipt;
        private System.Windows.Forms.Button btnDoctorsCashReport;
        private System.Windows.Forms.Button btnExpend;
        private System.Windows.Forms.Button btnCol;
        private System.Windows.Forms.DataGridView dgAppointment;
        private System.Windows.Forms.PictureBox picPatient;
        private System.Windows.Forms.Label lblPayment;
        private System.Windows.Forms.ContextMenuStrip cmsAppointment;
        private System.Windows.Forms.ToolStripMenuItem miDown;
        private System.Windows.Forms.ToolStripMenuItem miUp;
        private System.Windows.Forms.ContextMenuStrip cmsPatientReg;
      private System.Windows.Forms.ToolStripMenuItem miAppointment;
        private System.Windows.Forms.ToolStripMenuItem miEmailAllVisit;
      private System.Windows.Forms.Label lblSelectedDate;
        private System.Windows.Forms.ToolStripMenuItem miChangeAppoint;
        private System.Windows.Forms.Panel pnlChangeAppoint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpAppointDate;
        private System.Windows.Forms.ComboBox cboChangeConultant;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnChangeExit;
        private System.Windows.Forms.ToolStripMenuItem miCancelAppoint;
      private System.Windows.Forms.Button btnDeleteAll;
      private System.Windows.Forms.Button btnAppointment;
      private System.Windows.Forms.ToolStripMenuItem miLastFiveVisit;
      private System.Windows.Forms.ToolStripMenuItem miLastTenVisit;
      private System.Windows.Forms.ToolStripMenuItem miAllVisit;
      private System.Windows.Forms.Button btnSetting;
      private System.Windows.Forms.ToolStripMenuItem miLastVisit;
      private System.Windows.Forms.ComboBox cboConsultent;
      private System.Windows.Forms.Button btnTomorrow;
        private System.Windows.Forms.Button btnNextTomorrow;
        private System.Windows.Forms.Panel pnlVitalSigns;
        private System.Windows.Forms.Label lblBSAUnit;
        private System.Windows.Forms.Label lblBMIUnit;
        private System.Windows.Forms.Label lblVitalSigns;
        private System.Windows.Forms.TextBox txtBSA;
        private System.Windows.Forms.TextBox txtBMI;
        private System.Windows.Forms.MaskedTextBox txtBP;
        private System.Windows.Forms.ComboBox cboHeightUnit;
        private System.Windows.Forms.ComboBox cboWeightUnit;
        private System.Windows.Forms.ComboBox cboTempUnit;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.TextBox txtPulse;
        private System.Windows.Forms.TextBox txtTemp;
        private System.Windows.Forms.TextBox txtHeadCircum;
        private System.Windows.Forms.Label lblHeadCircumUnit;
        private System.Windows.Forms.Label lblPulseUnit;
        private System.Windows.Forms.Label lblBPUnit;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblPulse;
        private System.Windows.Forms.Label lblBSA;
        private System.Windows.Forms.Label lblBMI;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblBP;
        private System.Windows.Forms.Label lblHeadCircum;
        private System.Windows.Forms.Label lblTemp;
        private System.Windows.Forms.TextBox txtBMIStatus;
        private System.Windows.Forms.Label lblBMIStatus;
        private System.Windows.Forms.Button btnVitalSignClose;
        private System.Windows.Forms.MaskedTextBox txtVSPRNO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnVSSave;
        private System.Windows.Forms.TextBox txtRespiratoryRate;
        private System.Windows.Forms.TextBox txtHeartRate;
        private System.Windows.Forms.Label lblRespiratoryRate;
        private System.Windows.Forms.Label lblHeartRate;
        private Pabo.Calendar.DateItem dateItem1;
        private Pabo.Calendar.DateItem dateItem2;
        private Pabo.Calendar.DateItem dateItem3;
        private System.Windows.Forms.RadioButton rbSPhone;
        private System.Windows.Forms.RadioButton rbSCell;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.MaskedTextBox txtRPRNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.LinkLabel RemoveMask;
        private System.Windows.Forms.LinkLabel ShowMask;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn DOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaritalStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Panel;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployNo;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewTextBoxColumn APatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn APRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn AName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ATokenNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentEndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonID;
        private System.Windows.Forms.DataGridViewLinkColumn VitalSign;
        private System.Windows.Forms.DataGridViewTextBoxColumn opdid;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPDIDD;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPRNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn SStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Video;
    }
}