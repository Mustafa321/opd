namespace OPD
{
    partial class frmOPD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOPD));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle129 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle130 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle124 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle125 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle126 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle127 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle128 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle131 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle142 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle143 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle132 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle133 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle134 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle135 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle136 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle137 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle138 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle139 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle140 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle141 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle144 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle146 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle147 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle145 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblOPDID = new System.Windows.Forms.Label();
            this.lblPatientID = new System.Windows.Forms.Label();
            this.txtPRNO = new System.Windows.Forms.MaskedTextBox();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.pnlVisit = new System.Windows.Forms.Panel();
            this.pnlNotes = new System.Windows.Forms.Panel();
            this.btnNotesDetail = new System.Windows.Forms.Button();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.btnStickyNotes = new System.Windows.Forms.Button();
            this.lblNotes = new System.Windows.Forms.Label();
            this.lblTextNotes = new System.Windows.Forms.Label();
            this.pnlAllerAR = new System.Windows.Forms.Panel();
            this.dgAR = new System.Windows.Forms.DataGridView();
            this.AREnteredON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgAllergies = new System.Windows.Forms.DataGridView();
            this.AllergEnteredON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Allergies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAllergARExpend = new System.Windows.Forms.Button();
            this.lblAllergARTabHeading = new System.Windows.Forms.Label();
            this.pnlProcAdmission = new System.Windows.Forms.Panel();
            this.btnAddAdmProc = new System.Windows.Forms.Button();
            this.dgAdmission = new System.Windows.Forms.DataGridView();
            this.AdmEnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Admission = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgProcedure = new System.Windows.Forms.DataGridView();
            this.ProcEnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.procedure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnProcAdmissionExp = new System.Windows.Forms.Button();
            this.lblAdmissionProcedure = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pnlTotalVisit = new System.Windows.Forms.Panel();
            this.lblVisit = new System.Windows.Forms.Label();
            this.lblVisitCount = new System.Windows.Forms.Label();
            this.pnlSelPatientDetail = new System.Windows.Forms.Panel();
            this.lblPatientVideoCount = new System.Windows.Forms.Label();
            this.lblPatientDocCount = new System.Windows.Forms.Label();
            this.lblPatientPicCount = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlSelPatientHead = new System.Windows.Forms.Panel();
            this.lblPatientDetail = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblWaitingQueue = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pnlWaitingList = new System.Windows.Forms.Panel();
            this.pnlPayment = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.dtpFollowUpDate = new System.Windows.Forms.DateTimePicker();
            this.lblFollowUpDate = new System.Windows.Forms.Label();
            this.lblPayment = new System.Windows.Forms.Label();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnNews = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlMedHist = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlVitalSignHist = new System.Windows.Forms.Panel();
            this.RemoveMask = new System.Windows.Forms.LinkLabel();
            this.ShowMask = new System.Windows.Forms.LinkLabel();
            this.btnVitalSignHistClose = new System.Windows.Forms.Button();
            this.btnLabsTest = new System.Windows.Forms.Button();
            this.btnInvestigation = new System.Windows.Forms.Button();
            this.btnPresentComplaint = new System.Windows.Forms.Button();
            this.dgComplaints = new System.Windows.Forms.DataGridView();
            this.ComplaintsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDComplaints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelComp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompDiseaseIDRef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompImgDisease = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmsComplaints = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miComplaintsRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miComplaintDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miCompStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.miComplaintICDMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSigns = new System.Windows.Forms.Button();
            this.btnPlans = new System.Windows.Forms.Button();
            this.btnDiagnosis = new System.Windows.Forms.Button();
            this.btnHistory = new System.Windows.Forms.Button();
            this.pnlInstructions = new System.Windows.Forms.Panel();
            this.btnMedicine = new System.Windows.Forms.Button();
            this.dgInvestigation = new System.Windows.Forms.DataGridView();
            this.InvestigatioName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDInvestigation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelInvest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsInvestigation = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miInvestigationRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miInvestigationDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miInvestStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.dgMedicine = new System.Windows.Forms.DataGridView();
            this.MedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Medname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsMed = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAllergies = new System.Windows.Forms.ToolStripMenuItem();
            this.miAR = new System.Windows.Forms.ToolStripMenuItem();
            this.dgPlans = new System.Windows.Forms.DataGridView();
            this.PlansName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDPlans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelPlan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPlans = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miPlansRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miPlansDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miPlanStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.dgDiagnosis = new System.Windows.Forms.DataGridView();
            this.DiagnosisName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDDiagnosis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelDiagnosis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiagnosisDiseaseRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiagnoImgDisease = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmsDiagnosis = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miDiagnosisRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miDiagnosisDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miDiagnosisStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.miDiagnosisICDMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.dgHistory = new System.Windows.Forms.DataGridView();
            this.HistoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDHistory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelHist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HistDiseaseIDRef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImgHistAvail = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmsHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miHistoryRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miHistoryDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miHistoryStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.miHistICDMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.dgSigns = new System.Windows.Forms.DataGridView();
            this.SignsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefIDSigns = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelSign = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SignDiseaseIDRef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SignImgDisease = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmsSigns = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSignsRename = new System.Windows.Forms.ToolStripMenuItem();
            this.miSignsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miSignStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.miSignsICDMoreInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMedHistGrid = new System.Windows.Forms.Panel();
            this.btnMedHistClose = new System.Windows.Forms.Button();
            this.lblHeadingMedHist = new System.Windows.Forms.Label();
            this.dgMedHist = new System.Windows.Forms.DataGridView();
            this.MHVisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MHOPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MHMedicineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MHMedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MHDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDGList = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnListPnl = new System.Windows.Forms.Button();
            this.dgList = new System.Windows.Forms.DataGridView();
            this.PName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ListDisRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsItem = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.SavePatient = new System.Windows.Forms.Label();
            this.pnlVitalSigns = new System.Windows.Forms.Panel();
            this.VideoOption = new System.Windows.Forms.CheckBox();
            this.lblBSAUnit = new System.Windows.Forms.Label();
            this.lblBMIUnit = new System.Windows.Forms.Label();
            this.txtRespiratoryRate = new System.Windows.Forms.TextBox();
            this.txtHeartRate = new System.Windows.Forms.TextBox();
            this.lblRespiratoryRate = new System.Windows.Forms.Label();
            this.lblHeartRate = new System.Windows.Forms.Label();
            this.btnVitalSignHist = new System.Windows.Forms.Button();
            this.btnPreviousVisit = new System.Windows.Forms.Button();
            this.txtBSAStatus = new System.Windows.Forms.TextBox();
            this.txtBMIStatus = new System.Windows.Forms.TextBox();
            this.lblVitalSigns = new System.Windows.Forms.Label();
            this.txtBSA = new System.Windows.Forms.TextBox();
            this.txtBMI = new System.Windows.Forms.TextBox();
            this.txtBP = new System.Windows.Forms.MaskedTextBox();
            this.cboHeightUnit = new System.Windows.Forms.ComboBox();
            this.cboWeightUnit = new System.Windows.Forms.ComboBox();
            this.cboTempUnit = new System.Windows.Forms.ComboBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.txtPulse = new System.Windows.Forms.TextBox();
            this.txtTemp = new System.Windows.Forms.TextBox();
            this.lblPulseUnit = new System.Windows.Forms.Label();
            this.lblBPUnit = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblPulse = new System.Windows.Forms.Label();
            this.lblBSA = new System.Windows.Forms.Label();
            this.lblBMI = new System.Windows.Forms.Label();
            this.lblBSAStatus = new System.Windows.Forms.Label();
            this.lblBMIStatus = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblBP = new System.Windows.Forms.Label();
            this.lblTemp = new System.Windows.Forms.Label();
            this.ttOPD = new System.Windows.Forms.ToolTip(this.components);
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.txtSearchPRNO = new System.Windows.Forms.MaskedTextBox();
            this.txtSearchPhone = new System.Windows.Forms.TextBox();
            this.txtSearchName = new System.Windows.Forms.TextBox();
            this.txtHName = new System.Windows.Forms.TextBox();
            this.cboHCity = new System.Windows.Forms.ComboBox();
            this.dtpHFrom = new System.Windows.Forms.DateTimePicker();
            this.panel9 = new System.Windows.Forms.Panel();
            this.rbHSCell = new System.Windows.Forms.RadioButton();
            this.dtpHTo = new System.Windows.Forms.DateTimePicker();
            this.txtHSPRNO = new System.Windows.Forms.MaskedTextBox();
            this.txtHSPhone = new System.Windows.Forms.TextBox();
            this.txtHSName = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rbHMale = new System.Windows.Forms.RadioButton();
            this.rbHFemale = new System.Windows.Forms.RadioButton();
            this.txtHAddress = new System.Windows.Forms.TextBox();
            this.txtHCell = new System.Windows.Forms.TextBox();
            this.txtHPhone = new System.Windows.Forms.TextBox();
            this.txtHAge = new System.Windows.Forms.TextBox();
            this.dtpHDOB = new System.Windows.Forms.DateTimePicker();
            this.txtCName = new System.Windows.Forms.TextBox();
            this.cboCCity = new System.Windows.Forms.ComboBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.rbCFemale = new System.Windows.Forms.RadioButton();
            this.rbCMale = new System.Windows.Forms.RadioButton();
            this.txtCAddress = new System.Windows.Forms.TextBox();
            this.txtCCell = new System.Windows.Forms.TextBox();
            this.txtCPhone = new System.Windows.Forms.TextBox();
            this.txtCAge = new System.Windows.Forms.TextBox();
            this.dtpCDOB = new System.Windows.Forms.DateTimePicker();
            this.dtpCTo = new System.Windows.Forms.DateTimePicker();
            this.dtpCFrom = new System.Windows.Forms.DateTimePicker();
            this.txtCSPRNO = new System.Windows.Forms.MaskedTextBox();
            this.txtCSPhone = new System.Windows.Forms.TextBox();
            this.txtCSName = new System.Windows.Forms.TextBox();
            this.txtPatientAge = new System.Windows.Forms.TextBox();
            this.txtPatientCell = new System.Windows.Forms.TextBox();
            this.txtPatientName = new System.Windows.Forms.TextBox();
            this.dtpPatientDOB = new System.Windows.Forms.DateTimePicker();
            this.cboPatientCity = new System.Windows.Forms.ComboBox();
            this.txtRPRNo = new System.Windows.Forms.MaskedTextBox();
            this.pnlPatientGender = new System.Windows.Forms.Panel();
            this.rbPatientFemale = new System.Windows.Forms.RadioButton();
            this.rbPatientMale = new System.Windows.Forms.RadioButton();
            this.txtPatientPhone = new System.Windows.Forms.TextBox();
            this.txtPatientAddress = new System.Windows.Forms.TextBox();
            this.txtCPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtHPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtRegEmail = new System.Windows.Forms.TextBox();
            this.txtHEmail = new System.Windows.Forms.TextBox();
            this.cboHPanel = new System.Windows.Forms.ComboBox();
            this.cboCPanel = new System.Windows.Forms.ComboBox();
            this.txtCEmail = new System.Windows.Forms.TextBox();
            this.txtSearchByPRNo = new System.Windows.Forms.MaskedTextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.btnPatientSave = new System.Windows.Forms.Button();
            this.btnRegisterPatient = new System.Windows.Forms.Button();
            this.btnNewPatient = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnPatientSearch = new System.Windows.Forms.Button();
            this.btnConsultedSerach = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.btnCUpdate = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnConsultedSearch = new System.Windows.Forms.Button();
            this.btnHoldSerach = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.rbHSPhone = new System.Windows.Forms.RadioButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ViewPatientDocument = new System.Windows.Forms.ToolStripButton();
            this.miDoc = new System.Windows.Forms.ToolStripButton();
            this.miPicture = new System.Windows.Forms.ToolStripButton();
            this.miVideo = new System.Windows.Forms.ToolStripButton();
            this.miEmail = new System.Windows.Forms.ToolStripButton();
            this.miSMS = new System.Windows.Forms.ToolStripButton();
            this.Notifications = new System.Windows.Forms.ToolStripButton();
            this.miVideoCall = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miControlePnl = new System.Windows.Forms.ToolStripDropDownButton();
            this.miOption = new System.Windows.Forms.ToolStripMenuItem();
            this.SynchPatients = new System.Windows.Forms.ToolStripMenuItem();
            this.SendPatientDetailToServer = new System.Windows.Forms.ToolStripMenuItem();
            this.miNewAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.miInstructions = new System.Windows.Forms.ToolStripMenuItem();
            this.miChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.miPanelInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.dataAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miDosageInstruction = new System.Windows.Forms.ToolStripMenuItem();
            this.backupDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMonthlySummary = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddDisease = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.miHoldSave = new System.Windows.Forms.ToolStripButton();
            this.miConsultedSave = new System.Windows.Forms.ToolStripButton();
            this.miPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.miCancel = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.llblDailyCashReport = new System.Windows.Forms.LinkLabel();
            this.llblSearchByPRNO = new System.Windows.Forms.LinkLabel();
            this.lkbPayRep = new System.Windows.Forms.LinkLabel();
            this.lblMode = new System.Windows.Forms.Label();
            this.lklTdaySummary = new System.Windows.Forms.LinkLabel();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTotalPatient = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSelect = new System.Windows.Forms.Label();
            this.llblRefershPatient = new System.Windows.Forms.LinkLabel();
            this.pnlRegister = new System.Windows.Forms.Panel();
            this.pnlLegend = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblRegCount = new System.Windows.Forms.Label();
            this.pnlRPatient = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPatientName = new System.Windows.Forms.Label();
            this.lblPatientCity = new System.Windows.Forms.Label();
            this.lblPatientAddress = new System.Windows.Forms.Label();
            this.lblRPRNo = new System.Windows.Forms.Label();
            this.lblPatientPhone = new System.Windows.Forms.Label();
            this.lblRDOB = new System.Windows.Forms.Label();
            this.lblRegEmail = new System.Windows.Forms.Label();
            this.lblPatientAge = new System.Windows.Forms.Label();
            this.lblPatientCell = new System.Windows.Forms.Label();
            this.lblPatientGender = new System.Windows.Forms.Label();
            this.dgRegisterPatient = new System.Windows.Forms.DataGridView();
            this.PatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RSNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RPanel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ROPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlRegisterSearch = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbSCell = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblSearchPRNO = new System.Windows.Forms.Label();
            this.pbPatient = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pnlHold = new System.Windows.Forms.Panel();
            this.lblHoldCount = new System.Windows.Forms.Label();
            this.pnlHPatient = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblHPatientID = new System.Windows.Forms.Label();
            this.btnHoldUpdate = new System.Windows.Forms.Button();
            this.lblHName = new System.Windows.Forms.Label();
            this.lblHPRNo = new System.Windows.Forms.Label();
            this.lblHEmail = new System.Windows.Forms.Label();
            this.lblHCity = new System.Windows.Forms.Label();
            this.lblHDOB = new System.Windows.Forms.Label();
            this.lblHAge = new System.Windows.Forms.Label();
            this.lblHAddress = new System.Windows.Forms.Label();
            this.lblHPhone = new System.Windows.Forms.Label();
            this.lblHCell = new System.Windows.Forms.Label();
            this.lblHPanel = new System.Windows.Forms.Label();
            this.lblHGender = new System.Windows.Forms.Label();
            this.dgHold = new System.Windows.Forms.DataGridView();
            this.HPatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPanel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HEnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HDOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoldVisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HCityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHoldSearch = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnHSearch = new System.Windows.Forms.Button();
            this.lblHSName = new System.Windows.Forms.Label();
            this.lblHSPRNO = new System.Windows.Forms.Label();
            this.lblHTo = new System.Windows.Forms.Label();
            this.lblHFrom = new System.Windows.Forms.Label();
            this.pbHold = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pnlConsult = new System.Windows.Forms.Panel();
            this.lblConsultCount = new System.Windows.Forms.Label();
            this.lblCPatientCount = new System.Windows.Forms.Label();
            this.pnlCPatient = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblCAgeT = new System.Windows.Forms.Label();
            this.lblCNameT = new System.Windows.Forms.Label();
            this.lblCEmail = new System.Windows.Forms.Label();
            this.lblCDOBT = new System.Windows.Forms.Label();
            this.lblCCityT = new System.Windows.Forms.Label();
            this.lblCCellT = new System.Windows.Forms.Label();
            this.lblCAddressT = new System.Windows.Forms.Label();
            this.lblCGenderT = new System.Windows.Forms.Label();
            this.lblCPRNo = new System.Windows.Forms.Label();
            this.pbConsulted = new System.Windows.Forms.PictureBox();
            this.dgConsulted = new System.Windows.Forms.DataGridView();
            this.CPatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPanel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPatientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CDOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CVisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CCityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlConsultedSearch = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.rbCSPhone = new System.Windows.Forms.RadioButton();
            this.rbCSCell = new System.Windows.Forms.RadioButton();
            this.lblCPhoneT = new System.Windows.Forms.Label();
            this.lblCPanel = new System.Windows.Forms.Label();
            this.lblCPatientID = new System.Windows.Forms.Label();
            this.lblCSName = new System.Windows.Forms.Label();
            this.lblCTo = new System.Windows.Forms.Label();
            this.lblCFrom = new System.Windows.Forms.Label();
            this.lblCSPRNO = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.miRegistration = new System.Windows.Forms.ToolStripButton();
            this.miHold = new System.Windows.Forms.ToolStripButton();
            this.miConsulted = new System.Windows.Forms.ToolStripButton();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.pnlSerachByPRNo = new System.Windows.Forms.Panel();
            this.btnRegNewPateint = new System.Windows.Forms.Button();
            this.btnSearchPrnoClose = new System.Windows.Forms.Button();
            this.lblPRNo = new System.Windows.Forms.Label();
            this.timerVoice = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn4 = new System.Windows.Forms.DataGridViewImageColumn();
            this.pnlAppointment = new System.Windows.Forms.Panel();
            this.dgAppoinment = new System.Windows.Forms.DataGridView();
            this.TokkenNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppPRNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppPName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppoinmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppointmentDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppOPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppPatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AppVitalSign = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mcAppointment = new Pabo.Calendar.MonthCalendar();
            this.btnAppointControl = new System.Windows.Forms.Button();
            this.lblAppointment = new System.Windows.Forms.Label();
            this.btnAppointment = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlInfo.SuspendLayout();
            this.pnlNotes.SuspendLayout();
            this.pnlAllerAR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllergies)).BeginInit();
            this.pnlProcAdmission.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdmission)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProcedure)).BeginInit();
            this.pnlTotalVisit.SuspendLayout();
            this.pnlSelPatientDetail.SuspendLayout();
            this.pnlSelPatientHead.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlPayment.SuspendLayout();
            this.panel16.SuspendLayout();
            this.pnlMedHist.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlVitalSignHist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgComplaints)).BeginInit();
            this.cmsComplaints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvestigation)).BeginInit();
            this.cmsInvestigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMedicine)).BeginInit();
            this.cmsMed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPlans)).BeginInit();
            this.cmsPlans.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagnosis)).BeginInit();
            this.cmsDiagnosis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHistory)).BeginInit();
            this.cmsHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSigns)).BeginInit();
            this.cmsSigns.SuspendLayout();
            this.pnlMedHistGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMedHist)).BeginInit();
            this.pnlDGList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.cmsItem.SuspendLayout();
            this.pnlVitalSigns.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pnlPatientGender.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlRegister.SuspendLayout();
            this.pnlLegend.SuspendLayout();
            this.pnlRPatient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRegisterPatient)).BeginInit();
            this.pnlRegisterSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPatient)).BeginInit();
            this.pnlHold.SuspendLayout();
            this.pnlHPatient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHold)).BeginInit();
            this.pnlHoldSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHold)).BeginInit();
            this.pnlConsult.SuspendLayout();
            this.pnlCPatient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConsulted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsulted)).BeginInit();
            this.pnlConsultedSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel12.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.pnlSerachByPRNo.SuspendLayout();
            this.pnlAppointment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppoinment)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOPDID
            // 
            this.lblOPDID.AutoSize = true;
            this.lblOPDID.Location = new System.Drawing.Point(172, 131);
            this.lblOPDID.Name = "lblOPDID";
            this.lblOPDID.Size = new System.Drawing.Size(0, 13);
            this.lblOPDID.TabIndex = 1;
            this.lblOPDID.Visible = false;
            // 
            // lblPatientID
            // 
            this.lblPatientID.AutoSize = true;
            this.lblPatientID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientID.Location = new System.Drawing.Point(148, 128);
            this.lblPatientID.Name = "lblPatientID";
            this.lblPatientID.Size = new System.Drawing.Size(0, 13);
            this.lblPatientID.TabIndex = 112;
            this.lblPatientID.Visible = false;
            // 
            // txtPRNO
            // 
            this.txtPRNO.BackColor = System.Drawing.Color.White;
            this.txtPRNO.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtPRNO.Location = new System.Drawing.Point(37, 370);
            this.txtPRNO.Mask = "aa-aa-aaaaaa";
            this.txtPRNO.Name = "txtPRNO";
            this.txtPRNO.ReadOnly = true;
            this.txtPRNO.Size = new System.Drawing.Size(73, 17);
            this.txtPRNO.TabIndex = 110;
            this.txtPRNO.TabStop = false;
            this.ttOPD.SetToolTip(this.txtPRNO, "PR No. for Selected Patient");
            this.txtPRNO.Visible = false;
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.LightGray;
            this.pnlInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInfo.Controls.Add(this.pnlVisit);
            this.pnlInfo.Controls.Add(this.pnlNotes);
            this.pnlInfo.Controls.Add(this.pnlAllerAR);
            this.pnlInfo.Controls.Add(this.pnlProcAdmission);
            this.pnlInfo.Controls.Add(this.pnlTotalVisit);
            this.pnlInfo.Controls.Add(this.pnlSelPatientDetail);
            this.pnlInfo.Controls.Add(this.pnlSelPatientHead);
            this.pnlInfo.Controls.Add(this.panel3);
            this.pnlInfo.Controls.Add(this.lblVersion);
            this.pnlInfo.Controls.Add(this.pnlWaitingList);
            this.pnlInfo.Controls.Add(this.pnlPayment);
            this.pnlInfo.Controls.Add(this.btnAbout);
            this.pnlInfo.Controls.Add(this.btnNews);
            this.pnlInfo.Controls.Add(this.panel16);
            this.pnlInfo.Controls.Add(this.pnlMedHist);
            this.pnlInfo.Controls.Add(this.txtPRNO);
            this.pnlInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlInfo.Location = new System.Drawing.Point(817, 214);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(197, 488);
            this.pnlInfo.TabIndex = 118;
            this.pnlInfo.Tag = "self";
            this.pnlInfo.Visible = false;
            // 
            // pnlVisit
            // 
            this.pnlVisit.AutoScroll = true;
            this.pnlVisit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlVisit.BackColor = System.Drawing.Color.LightGray;
            this.pnlVisit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlVisit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlVisit.Location = new System.Drawing.Point(1, 352);
            this.pnlVisit.Name = "pnlVisit";
            this.pnlVisit.Size = new System.Drawing.Size(193, 112);
            this.pnlVisit.TabIndex = 2;
            this.pnlVisit.Tag = "self";
            this.pnlVisit.Visible = false;
            // 
            // pnlNotes
            // 
            this.pnlNotes.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlNotes.BackgroundImage = global::OPD.Properties.Resources.notes;
            this.pnlNotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNotes.Controls.Add(this.btnNotesDetail);
            this.pnlNotes.Controls.Add(this.txtNotes);
            this.pnlNotes.Controls.Add(this.btnStickyNotes);
            this.pnlNotes.Controls.Add(this.lblNotes);
            this.pnlNotes.Controls.Add(this.lblTextNotes);
            this.pnlNotes.Location = new System.Drawing.Point(1, 114);
            this.pnlNotes.Name = "pnlNotes";
            this.pnlNotes.Size = new System.Drawing.Size(193, 25);
            this.pnlNotes.TabIndex = 133;
            this.pnlNotes.Tag = "med";
            // 
            // btnNotesDetail
            // 
            this.btnNotesDetail.BackgroundImage = global::OPD.Properties.Resources.notesHistory;
            this.btnNotesDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotesDetail.Location = new System.Drawing.Point(147, 0);
            this.btnNotesDetail.Name = "btnNotesDetail";
            this.btnNotesDetail.Size = new System.Drawing.Size(22, 22);
            this.btnNotesDetail.TabIndex = 17;
            this.btnNotesDetail.Tag = "1";
            this.ttOPD.SetToolTip(this.btnNotesDetail, "Detail  view of text and voice Notes");
            this.btnNotesDetail.UseVisualStyleBackColor = true;
            this.btnNotesDetail.Click += new System.EventHandler(this.btnNotesDetail_Click);
            // 
            // txtNotes
            // 
            this.txtNotes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNotes.Location = new System.Drawing.Point(1, 37);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNotes.Size = new System.Drawing.Size(190, 333);
            this.txtNotes.TabIndex = 16;
            // 
            // btnStickyNotes
            // 
            this.btnStickyNotes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStickyNotes.BackgroundImage")));
            this.btnStickyNotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStickyNotes.Location = new System.Drawing.Point(169, 0);
            this.btnStickyNotes.Name = "btnStickyNotes";
            this.btnStickyNotes.Size = new System.Drawing.Size(22, 22);
            this.btnStickyNotes.TabIndex = 15;
            this.btnStickyNotes.Tag = "1";
            this.btnStickyNotes.UseVisualStyleBackColor = true;
            this.btnStickyNotes.Click += new System.EventHandler(this.btnStickyNotes_Click);
            // 
            // lblNotes
            // 
            this.lblNotes.AutoSize = true;
            this.lblNotes.BackColor = System.Drawing.Color.Transparent;
            this.lblNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotes.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblNotes.Location = new System.Drawing.Point(39, 4);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(44, 15);
            this.lblNotes.TabIndex = 14;
            this.lblNotes.Tag = "transdisplay";
            this.lblNotes.Text = "Notes";
            this.lblNotes.Click += new System.EventHandler(this.lblNotes_Click);
            // 
            // lblTextNotes
            // 
            this.lblTextNotes.AutoSize = true;
            this.lblTextNotes.BackColor = System.Drawing.Color.Transparent;
            this.lblTextNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextNotes.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblTextNotes.Location = new System.Drawing.Point(1, 23);
            this.lblTextNotes.Name = "lblTextNotes";
            this.lblTextNotes.Size = new System.Drawing.Size(46, 15);
            this.lblTextNotes.TabIndex = 19;
            this.lblTextNotes.Tag = "transdisplay";
            this.lblTextNotes.Text = "Text : ";
            // 
            // pnlAllerAR
            // 
            this.pnlAllerAR.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlAllerAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlAllerAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAllerAR.Controls.Add(this.dgAR);
            this.pnlAllerAR.Controls.Add(this.dgAllergies);
            this.pnlAllerAR.Controls.Add(this.btnAllergARExpend);
            this.pnlAllerAR.Controls.Add(this.lblAllergARTabHeading);
            this.pnlAllerAR.Location = new System.Drawing.Point(1, 140);
            this.pnlAllerAR.Name = "pnlAllerAR";
            this.pnlAllerAR.Size = new System.Drawing.Size(193, 25);
            this.pnlAllerAR.TabIndex = 143;
            this.pnlAllerAR.Tag = "med";
            this.ttOPD.SetToolTip(this.pnlAllerAR, "Allergies and Adverse Reaction");
            // 
            // dgAR
            // 
            this.dgAR.AllowUserToAddRows = false;
            this.dgAR.AllowUserToDeleteRows = false;
            this.dgAR.AllowUserToOrderColumns = true;
            this.dgAR.AllowUserToResizeColumns = false;
            this.dgAR.AllowUserToResizeRows = false;
            this.dgAR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle90.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAR.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle90;
            this.dgAR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AREnteredON,
            this.AR});
            this.dgAR.Location = new System.Drawing.Point(1, 181);
            this.dgAR.Name = "dgAR";
            this.dgAR.RowHeadersVisible = false;
            this.dgAR.Size = new System.Drawing.Size(189, 159);
            this.dgAR.TabIndex = 17;
            // 
            // AREnteredON
            // 
            this.AREnteredON.DataPropertyName = "EnteredOn";
            this.AREnteredON.FillWeight = 40F;
            this.AREnteredON.HeaderText = "Date";
            this.AREnteredON.Name = "AREnteredON";
            // 
            // AR
            // 
            this.AR.DataPropertyName = "AllergARName";
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AR.DefaultCellStyle = dataGridViewCellStyle91;
            this.AR.FillWeight = 60F;
            this.AR.HeaderText = "Adv. Reaction";
            this.AR.Name = "AR";
            // 
            // dgAllergies
            // 
            this.dgAllergies.AllowUserToAddRows = false;
            this.dgAllergies.AllowUserToDeleteRows = false;
            this.dgAllergies.AllowUserToOrderColumns = true;
            this.dgAllergies.AllowUserToResizeColumns = false;
            this.dgAllergies.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle92.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle92.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle92.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle92.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle92.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle92.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAllergies.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle92;
            this.dgAllergies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAllergies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AllergEnteredON,
            this.Allergies});
            this.dgAllergies.Location = new System.Drawing.Point(1, 24);
            this.dgAllergies.Name = "dgAllergies";
            this.dgAllergies.RowHeadersVisible = false;
            this.dgAllergies.Size = new System.Drawing.Size(189, 158);
            this.dgAllergies.TabIndex = 16;
            // 
            // AllergEnteredON
            // 
            this.AllergEnteredON.DataPropertyName = "EnteredOn";
            this.AllergEnteredON.FillWeight = 40F;
            this.AllergEnteredON.HeaderText = "Date";
            this.AllergEnteredON.Name = "AllergEnteredON";
            // 
            // Allergies
            // 
            this.Allergies.DataPropertyName = "AllergARName";
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Allergies.DefaultCellStyle = dataGridViewCellStyle93;
            this.Allergies.FillWeight = 60F;
            this.Allergies.HeaderText = "Allergies";
            this.Allergies.Name = "Allergies";
            // 
            // btnAllergARExpend
            // 
            this.btnAllergARExpend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAllergARExpend.BackgroundImage")));
            this.btnAllergARExpend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAllergARExpend.Location = new System.Drawing.Point(169, 0);
            this.btnAllergARExpend.Name = "btnAllergARExpend";
            this.btnAllergARExpend.Size = new System.Drawing.Size(22, 22);
            this.btnAllergARExpend.TabIndex = 15;
            this.btnAllergARExpend.Tag = "1";
            this.ttOPD.SetToolTip(this.btnAllergARExpend, "Allergies and Adverse Reaction");
            this.btnAllergARExpend.UseVisualStyleBackColor = true;
            this.btnAllergARExpend.Click += new System.EventHandler(this.btnAllergARExpend_Click);
            // 
            // lblAllergARTabHeading
            // 
            this.lblAllergARTabHeading.AutoSize = true;
            this.lblAllergARTabHeading.BackColor = System.Drawing.Color.Transparent;
            this.lblAllergARTabHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllergARTabHeading.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblAllergARTabHeading.Location = new System.Drawing.Point(4, 5);
            this.lblAllergARTabHeading.Name = "lblAllergARTabHeading";
            this.lblAllergARTabHeading.Size = new System.Drawing.Size(165, 13);
            this.lblAllergARTabHeading.TabIndex = 14;
            this.lblAllergARTabHeading.Tag = "transdisplay";
            this.lblAllergARTabHeading.Text = "Allergies and Adv. Reaction";
            this.ttOPD.SetToolTip(this.lblAllergARTabHeading, "Allergies and Adverse Reaction");
            // 
            // pnlProcAdmission
            // 
            this.pnlProcAdmission.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlProcAdmission.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlProcAdmission.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProcAdmission.Controls.Add(this.btnAddAdmProc);
            this.pnlProcAdmission.Controls.Add(this.dgAdmission);
            this.pnlProcAdmission.Controls.Add(this.dgProcedure);
            this.pnlProcAdmission.Controls.Add(this.btnProcAdmissionExp);
            this.pnlProcAdmission.Controls.Add(this.lblAdmissionProcedure);
            this.pnlProcAdmission.Controls.Add(this.label22);
            this.pnlProcAdmission.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlProcAdmission.Location = new System.Drawing.Point(1, 302);
            this.pnlProcAdmission.Name = "pnlProcAdmission";
            this.pnlProcAdmission.Size = new System.Drawing.Size(193, 25);
            this.pnlProcAdmission.TabIndex = 144;
            this.pnlProcAdmission.Tag = "med";
            this.ttOPD.SetToolTip(this.pnlProcAdmission, "Total visit for Selected Patient");
            // 
            // btnAddAdmProc
            // 
            this.btnAddAdmProc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddAdmProc.BackgroundImage")));
            this.btnAddAdmProc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddAdmProc.Location = new System.Drawing.Point(0, 0);
            this.btnAddAdmProc.Name = "btnAddAdmProc";
            this.btnAddAdmProc.Size = new System.Drawing.Size(25, 23);
            this.btnAddAdmProc.TabIndex = 119;
            this.ttOPD.SetToolTip(this.btnAddAdmProc, "Add Procedure and Admission");
            this.btnAddAdmProc.UseVisualStyleBackColor = true;
            this.btnAddAdmProc.Click += new System.EventHandler(this.btnAddAdmProc_Click);
            // 
            // dgAdmission
            // 
            this.dgAdmission.AllowUserToAddRows = false;
            this.dgAdmission.AllowUserToDeleteRows = false;
            this.dgAdmission.AllowUserToOrderColumns = true;
            this.dgAdmission.AllowUserToResizeColumns = false;
            this.dgAdmission.AllowUserToResizeRows = false;
            this.dgAdmission.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAdmission.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle94.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle94.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle94.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle94.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle94.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAdmission.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle94;
            this.dgAdmission.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAdmission.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AdmEnteredOn,
            this.Admission});
            this.dgAdmission.Location = new System.Drawing.Point(1, 105);
            this.dgAdmission.Name = "dgAdmission";
            this.dgAdmission.RowHeadersVisible = false;
            this.dgAdmission.Size = new System.Drawing.Size(189, 75);
            this.dgAdmission.TabIndex = 118;
            // 
            // AdmEnteredOn
            // 
            this.AdmEnteredOn.DataPropertyName = "EnteredOn";
            this.AdmEnteredOn.FillWeight = 40F;
            this.AdmEnteredOn.HeaderText = "Date";
            this.AdmEnteredOn.Name = "AdmEnteredOn";
            // 
            // Admission
            // 
            this.Admission.DataPropertyName = "ProcAdmName";
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Admission.DefaultCellStyle = dataGridViewCellStyle95;
            this.Admission.FillWeight = 60F;
            this.Admission.HeaderText = "Admission";
            this.Admission.Name = "Admission";
            // 
            // dgProcedure
            // 
            this.dgProcedure.AllowUserToAddRows = false;
            this.dgProcedure.AllowUserToDeleteRows = false;
            this.dgProcedure.AllowUserToOrderColumns = true;
            this.dgProcedure.AllowUserToResizeColumns = false;
            this.dgProcedure.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle96.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle96.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle96.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle96.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle96.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProcedure.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle96;
            this.dgProcedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProcedure.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProcEnteredOn,
            this.procedure});
            this.dgProcedure.Location = new System.Drawing.Point(1, 29);
            this.dgProcedure.Name = "dgProcedure";
            this.dgProcedure.RowHeadersVisible = false;
            this.dgProcedure.Size = new System.Drawing.Size(189, 75);
            this.dgProcedure.TabIndex = 117;
            // 
            // ProcEnteredOn
            // 
            this.ProcEnteredOn.DataPropertyName = "EnteredOn";
            this.ProcEnteredOn.FillWeight = 40F;
            this.ProcEnteredOn.HeaderText = "Date";
            this.ProcEnteredOn.Name = "ProcEnteredOn";
            // 
            // procedure
            // 
            this.procedure.DataPropertyName = "ProcAdmName";
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.procedure.DefaultCellStyle = dataGridViewCellStyle97;
            this.procedure.FillWeight = 60F;
            this.procedure.HeaderText = "Procedures";
            this.procedure.Name = "procedure";
            // 
            // btnProcAdmissionExp
            // 
            this.btnProcAdmissionExp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProcAdmissionExp.BackgroundImage")));
            this.btnProcAdmissionExp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProcAdmissionExp.Location = new System.Drawing.Point(169, 0);
            this.btnProcAdmissionExp.Name = "btnProcAdmissionExp";
            this.btnProcAdmissionExp.Size = new System.Drawing.Size(22, 22);
            this.btnProcAdmissionExp.TabIndex = 17;
            this.btnProcAdmissionExp.Tag = "1";
            this.ttOPD.SetToolTip(this.btnProcAdmissionExp, "View Admission and Procedure");
            this.btnProcAdmissionExp.UseVisualStyleBackColor = true;
            this.btnProcAdmissionExp.Click += new System.EventHandler(this.btnProcAdmissionExp_Click);
            // 
            // lblAdmissionProcedure
            // 
            this.lblAdmissionProcedure.AutoSize = true;
            this.lblAdmissionProcedure.BackColor = System.Drawing.Color.Transparent;
            this.lblAdmissionProcedure.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblAdmissionProcedure.Location = new System.Drawing.Point(22, 5);
            this.lblAdmissionProcedure.Name = "lblAdmissionProcedure";
            this.lblAdmissionProcedure.Size = new System.Drawing.Size(150, 13);
            this.lblAdmissionProcedure.TabIndex = 3;
            this.lblAdmissionProcedure.Tag = "transdisplay";
            this.lblAdmissionProcedure.Text = "Admission and Procedure";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(167, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 13);
            this.label22.TabIndex = 116;
            this.label22.Tag = "transdisplay";
            // 
            // pnlTotalVisit
            // 
            this.pnlTotalVisit.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlTotalVisit.BackgroundImage = global::OPD.Properties.Resources.psychiatric_records_198525_1229308553;
            this.pnlTotalVisit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTotalVisit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalVisit.Controls.Add(this.lblVisit);
            this.pnlTotalVisit.Controls.Add(this.lblVisitCount);
            this.pnlTotalVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlTotalVisit.Location = new System.Drawing.Point(1, 328);
            this.pnlTotalVisit.Name = "pnlTotalVisit";
            this.pnlTotalVisit.Size = new System.Drawing.Size(193, 25);
            this.pnlTotalVisit.TabIndex = 130;
            this.pnlTotalVisit.Tag = "med";
            this.ttOPD.SetToolTip(this.pnlTotalVisit, "Total visit for Selected Patient");
            this.pnlTotalVisit.Visible = false;
            // 
            // lblVisit
            // 
            this.lblVisit.AutoSize = true;
            this.lblVisit.BackColor = System.Drawing.Color.Transparent;
            this.lblVisit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblVisit.Location = new System.Drawing.Point(39, 5);
            this.lblVisit.Name = "lblVisit";
            this.lblVisit.Size = new System.Drawing.Size(81, 13);
            this.lblVisit.TabIndex = 3;
            this.lblVisit.Tag = "transdisplay";
            this.lblVisit.Text = "Patient Visits";
            // 
            // lblVisitCount
            // 
            this.lblVisitCount.AutoSize = true;
            this.lblVisitCount.Location = new System.Drawing.Point(163, 5);
            this.lblVisitCount.Name = "lblVisitCount";
            this.lblVisitCount.Size = new System.Drawing.Size(0, 13);
            this.lblVisitCount.TabIndex = 116;
            this.lblVisitCount.Tag = "transdisplay";
            // 
            // pnlSelPatientDetail
            // 
            this.pnlSelPatientDetail.AutoScroll = true;
            this.pnlSelPatientDetail.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlSelPatientDetail.BackColor = System.Drawing.Color.LightGray;
            this.pnlSelPatientDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSelPatientDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSelPatientDetail.Controls.Add(this.lblPatientVideoCount);
            this.pnlSelPatientDetail.Controls.Add(this.lblPatientDocCount);
            this.pnlSelPatientDetail.Controls.Add(this.lblPatientPicCount);
            this.pnlSelPatientDetail.Controls.Add(this.label20);
            this.pnlSelPatientDetail.Controls.Add(this.label18);
            this.pnlSelPatientDetail.Controls.Add(this.label4);
            this.pnlSelPatientDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSelPatientDetail.Location = new System.Drawing.Point(1, 242);
            this.pnlSelPatientDetail.Name = "pnlSelPatientDetail";
            this.pnlSelPatientDetail.Size = new System.Drawing.Size(193, 59);
            this.pnlSelPatientDetail.TabIndex = 3;
            this.pnlSelPatientDetail.Tag = "self";
            this.pnlSelPatientDetail.Visible = false;
            // 
            // lblPatientVideoCount
            // 
            this.lblPatientVideoCount.AutoSize = true;
            this.lblPatientVideoCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPatientVideoCount.Location = new System.Drawing.Point(104, 42);
            this.lblPatientVideoCount.Name = "lblPatientVideoCount";
            this.lblPatientVideoCount.Size = new System.Drawing.Size(0, 13);
            this.lblPatientVideoCount.TabIndex = 2;
            this.lblPatientVideoCount.Tag = "trans";
            this.ttOPD.SetToolTip(this.lblPatientVideoCount, "View Video Detail");
            this.lblPatientVideoCount.Click += new System.EventHandler(this.lblPatientVideoCount_Click);
            // 
            // lblPatientDocCount
            // 
            this.lblPatientDocCount.AutoSize = true;
            this.lblPatientDocCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPatientDocCount.Location = new System.Drawing.Point(104, 23);
            this.lblPatientDocCount.Name = "lblPatientDocCount";
            this.lblPatientDocCount.Size = new System.Drawing.Size(0, 13);
            this.lblPatientDocCount.TabIndex = 1;
            this.lblPatientDocCount.Tag = "trans";
            this.ttOPD.SetToolTip(this.lblPatientDocCount, "View Document Detail");
            this.lblPatientDocCount.Click += new System.EventHandler(this.lblPatientDocCount_Click);
            // 
            // lblPatientPicCount
            // 
            this.lblPatientPicCount.AutoSize = true;
            this.lblPatientPicCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPatientPicCount.Location = new System.Drawing.Point(104, 4);
            this.lblPatientPicCount.Name = "lblPatientPicCount";
            this.lblPatientPicCount.Size = new System.Drawing.Size(0, 13);
            this.lblPatientPicCount.TabIndex = 0;
            this.lblPatientPicCount.Tag = "trans";
            this.ttOPD.SetToolTip(this.lblPatientPicCount, "View Picture detail");
            this.lblPatientPicCount.Click += new System.EventHandler(this.lblPatientPicCount_Click);
            // 
            // label20
            // 
            this.label20.Image = global::OPD.Properties.Resources.videoIcon;
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(22, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 16);
            this.label20.TabIndex = 5;
            this.label20.Tag = "trans";
            this.label20.Text = "Video : ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttOPD.SetToolTip(this.label20, "View Video Detail");
            this.label20.Click += new System.EventHandler(this.lblPatientVideoCount_Click);
            // 
            // label18
            // 
            this.label18.Image = global::OPD.Properties.Resources.DocIcon;
            this.label18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label18.Location = new System.Drawing.Point(22, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 16);
            this.label18.TabIndex = 4;
            this.label18.Tag = "trans";
            this.label18.Text = "Document : ";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttOPD.SetToolTip(this.label18, "View Document Detail");
            this.label18.Click += new System.EventHandler(this.lblPatientDocCount_Click);
            // 
            // label4
            // 
            this.label4.Image = global::OPD.Properties.Resources.picIcon;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(22, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 3;
            this.label4.Tag = "trans";
            this.label4.Text = "Images : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttOPD.SetToolTip(this.label4, "View Picture detail");
            this.label4.Click += new System.EventHandler(this.lblPatientPicCount_Click);
            // 
            // pnlSelPatientHead
            // 
            this.pnlSelPatientHead.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlSelPatientHead.BackgroundImage = global::OPD.Properties.Resources.Patientdetail;
            this.pnlSelPatientHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSelPatientHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSelPatientHead.Controls.Add(this.lblPatientDetail);
            this.pnlSelPatientHead.Controls.Add(this.label16);
            this.pnlSelPatientHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSelPatientHead.Location = new System.Drawing.Point(1, 218);
            this.pnlSelPatientHead.Name = "pnlSelPatientHead";
            this.pnlSelPatientHead.Size = new System.Drawing.Size(193, 25);
            this.pnlSelPatientHead.TabIndex = 134;
            this.pnlSelPatientHead.Tag = "med";
            this.ttOPD.SetToolTip(this.pnlSelPatientHead, "Total visit for Selected Patient");
            this.pnlSelPatientHead.Visible = false;
            // 
            // lblPatientDetail
            // 
            this.lblPatientDetail.AutoSize = true;
            this.lblPatientDetail.BackColor = System.Drawing.Color.Transparent;
            this.lblPatientDetail.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblPatientDetail.Location = new System.Drawing.Point(39, 5);
            this.lblPatientDetail.Name = "lblPatientDetail";
            this.lblPatientDetail.Size = new System.Drawing.Size(114, 13);
            this.lblPatientDetail.TabIndex = 3;
            this.lblPatientDetail.Tag = "transdisplay";
            this.lblPatientDetail.Text = "Patient Documents";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(167, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 116;
            this.label16.Tag = "transdisplay";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel3.BackgroundImage = global::OPD.Properties.Resources.move_participant_to_waiting;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblWaitingQueue);
            this.panel3.Location = new System.Drawing.Point(1, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(193, 25);
            this.panel3.TabIndex = 129;
            this.panel3.Tag = "med";
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // lblWaitingQueue
            // 
            this.lblWaitingQueue.AutoSize = true;
            this.lblWaitingQueue.BackColor = System.Drawing.Color.Transparent;
            this.lblWaitingQueue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblWaitingQueue.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblWaitingQueue.Location = new System.Drawing.Point(39, 5);
            this.lblWaitingQueue.Name = "lblWaitingQueue";
            this.lblWaitingQueue.Size = new System.Drawing.Size(91, 13);
            this.lblWaitingQueue.TabIndex = 0;
            this.lblWaitingQueue.Tag = "transdisplay";
            this.lblWaitingQueue.Text = "Waiting Queue";
            this.lblWaitingQueue.Click += new System.EventHandler(this.lblWaitingQueue_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Location = new System.Drawing.Point(142, 385);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(29, 9);
            this.lblVersion.TabIndex = 126;
            this.lblVersion.Text = "label15";
            // 
            // pnlWaitingList
            // 
            this.pnlWaitingList.BackColor = System.Drawing.Color.LightGray;
            this.pnlWaitingList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlWaitingList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitingList.Location = new System.Drawing.Point(1, 51);
            this.pnlWaitingList.Name = "pnlWaitingList";
            this.pnlWaitingList.Size = new System.Drawing.Size(193, 62);
            this.pnlWaitingList.TabIndex = 125;
            this.pnlWaitingList.Tag = "self";
            // 
            // pnlPayment
            // 
            this.pnlPayment.BackColor = System.Drawing.Color.LightGray;
            this.pnlPayment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPayment.Controls.Add(this.label17);
            this.pnlPayment.Controls.Add(this.txtPayment);
            this.pnlPayment.Controls.Add(this.dtpFollowUpDate);
            this.pnlPayment.Controls.Add(this.lblFollowUpDate);
            this.pnlPayment.Controls.Add(this.lblPayment);
            this.pnlPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPayment.Location = new System.Drawing.Point(1, 1);
            this.pnlPayment.Name = "pnlPayment";
            this.pnlPayment.Size = new System.Drawing.Size(193, 25);
            this.pnlPayment.TabIndex = 124;
            this.pnlPayment.Tag = "self";
            this.pnlPayment.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(157, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 13);
            this.label17.TabIndex = 159;
            this.label17.Tag = "transdisplay";
            this.label17.Text = "Rs.";
            // 
            // txtPayment
            // 
            this.txtPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayment.Location = new System.Drawing.Point(104, 1);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(45, 20);
            this.txtPayment.TabIndex = 118;
            this.txtPayment.Text = "500";
            this.ttOPD.SetToolTip(this.txtPayment, "Selcted visit Payment in Rs.");
            this.txtPayment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayment_KeyPress);
            // 
            // dtpFollowUpDate
            // 
            this.dtpFollowUpDate.CustomFormat = "dd-MM-yyyy";
            this.dtpFollowUpDate.Enabled = false;
            this.dtpFollowUpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFollowUpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFollowUpDate.Location = new System.Drawing.Point(96, 23);
            this.dtpFollowUpDate.Name = "dtpFollowUpDate";
            this.dtpFollowUpDate.Size = new System.Drawing.Size(95, 20);
            this.dtpFollowUpDate.TabIndex = 1;
            this.dtpFollowUpDate.Tag = "0";
            this.ttOPD.SetToolTip(this.dtpFollowUpDate, "Follow up Date");
            this.dtpFollowUpDate.Visible = false;
            this.dtpFollowUpDate.ValueChanged += new System.EventHandler(this.dtpFollowUpDate_ValueChanged);
            this.dtpFollowUpDate.Leave += new System.EventHandler(this.dtpFollowUpDate_Leave);
            // 
            // lblFollowUpDate
            // 
            this.lblFollowUpDate.AutoSize = true;
            this.lblFollowUpDate.BackColor = System.Drawing.Color.Transparent;
            this.lblFollowUpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFollowUpDate.Location = new System.Drawing.Point(8, 27);
            this.lblFollowUpDate.Name = "lblFollowUpDate";
            this.lblFollowUpDate.Size = new System.Drawing.Size(86, 13);
            this.lblFollowUpDate.TabIndex = 0;
            this.lblFollowUpDate.Tag = "transdisplay";
            this.lblFollowUpDate.Text = "Follow Up Date :";
            this.lblFollowUpDate.Visible = false;
            this.lblFollowUpDate.Click += new System.EventHandler(this.lblFollowUpDate_Click);
            // 
            // lblPayment
            // 
            this.lblPayment.AutoSize = true;
            this.lblPayment.BackColor = System.Drawing.Color.Transparent;
            this.lblPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPayment.Location = new System.Drawing.Point(13, 4);
            this.lblPayment.Name = "lblPayment";
            this.lblPayment.Size = new System.Drawing.Size(73, 13);
            this.lblPayment.TabIndex = 158;
            this.lblPayment.Tag = "transdisplay";
            this.lblPayment.Text = "Paid Amount :";
            // 
            // btnAbout
            // 
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Location = new System.Drawing.Point(151, 465);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(43, 20);
            this.btnAbout.TabIndex = 123;
            this.btnAbout.TabStop = false;
            this.btnAbout.Tag = "noset";
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            // 
            // btnNews
            // 
            this.btnNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNews.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnNews.Location = new System.Drawing.Point(108, 465);
            this.btnNews.Name = "btnNews";
            this.btnNews.Size = new System.Drawing.Size(43, 20);
            this.btnNews.TabIndex = 122;
            this.btnNews.TabStop = false;
            this.btnNews.Tag = "noset";
            this.btnNews.Text = "News";
            this.btnNews.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel16.BackgroundImage = global::OPD.Properties.Resources.VSgraph35;
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label19);
            this.panel16.Location = new System.Drawing.Point(1, 166);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(193, 25);
            this.panel16.TabIndex = 132;
            this.panel16.Tag = "med";
            this.panel16.Click += new System.EventHandler(this.panel16_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label19.Location = new System.Drawing.Point(39, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 13);
            this.label19.TabIndex = 0;
            this.label19.Tag = "transdisplay";
            this.label19.Text = "Vital Sign Chart";
            this.label19.Click += new System.EventHandler(this.panel16_Click);
            // 
            // pnlMedHist
            // 
            this.pnlMedHist.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlMedHist.BackgroundImage = global::OPD.Properties.Resources.medication_schedule_35px;
            this.pnlMedHist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlMedHist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMedHist.Controls.Add(this.label15);
            this.pnlMedHist.Location = new System.Drawing.Point(1, 192);
            this.pnlMedHist.Name = "pnlMedHist";
            this.pnlMedHist.Size = new System.Drawing.Size(193, 25);
            this.pnlMedHist.TabIndex = 131;
            this.pnlMedHist.Tag = "med";
            this.pnlMedHist.Click += new System.EventHandler(this.label15_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label15.Location = new System.Drawing.Point(39, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 0;
            this.label15.Tag = "transdisplay";
            this.label15.Text = "Rx / Medication";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.btnLabsTest);
            this.pnlMain.Controls.Add(this.btnInvestigation);
            this.pnlMain.Controls.Add(this.btnPresentComplaint);
            this.pnlMain.Controls.Add(this.dgComplaints);
            this.pnlMain.Controls.Add(this.btnSigns);
            this.pnlMain.Controls.Add(this.btnPlans);
            this.pnlMain.Controls.Add(this.btnDiagnosis);
            this.pnlMain.Controls.Add(this.btnHistory);
            this.pnlMain.Controls.Add(this.pnlInstructions);
            this.pnlMain.Controls.Add(this.btnMedicine);
            this.pnlMain.Controls.Add(this.dgInvestigation);
            this.pnlMain.Controls.Add(this.dgMedicine);
            this.pnlMain.Controls.Add(this.dgPlans);
            this.pnlMain.Controls.Add(this.dgDiagnosis);
            this.pnlMain.Controls.Add(this.dgHistory);
            this.pnlMain.Controls.Add(this.dgSigns);
            this.pnlMain.Controls.Add(this.pnlMedHistGrid);
            this.pnlMain.Controls.Add(this.pnlDGList);
            this.pnlMain.Controls.Add(this.pnlVitalSignHist);
            this.pnlMain.Location = new System.Drawing.Point(2, 214);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pnlMain.Size = new System.Drawing.Size(814, 488);
            this.pnlMain.TabIndex = 119;
            this.pnlMain.TabStop = true;
            this.pnlMain.Tag = "top";
            this.pnlMain.Visible = false;
            // 
            // pnlVitalSignHist
            // 
            this.pnlVitalSignHist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVitalSignHist.Controls.Add(this.RemoveMask);
            this.pnlVitalSignHist.Controls.Add(this.ShowMask);
            this.pnlVitalSignHist.Controls.Add(this.btnVitalSignHistClose);
            this.pnlVitalSignHist.Location = new System.Drawing.Point(2, -1);
            this.pnlVitalSignHist.Name = "pnlVitalSignHist";
            this.pnlVitalSignHist.Size = new System.Drawing.Size(811, 488);
            this.pnlVitalSignHist.TabIndex = 141;
            this.pnlVitalSignHist.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlVitalSignHist_Paint);
            // 
            // RemoveMask
            // 
            this.RemoveMask.AutoSize = true;
            this.RemoveMask.Location = new System.Drawing.Point(462, 110);
            this.RemoveMask.Name = "RemoveMask";
            this.RemoveMask.Size = new System.Drawing.Size(76, 13);
            this.RemoveMask.TabIndex = 12;
            this.RemoveMask.TabStop = true;
            this.RemoveMask.Text = "Remove Mask";
            this.RemoveMask.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RemoveMask.Visible = false;
            this.RemoveMask.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RemoveMask_LinkClicked_1);
            this.RemoveMask.Click += new System.EventHandler(this.RemoveMask_LinkClicked);
            // 
            // ShowMask
            // 
            this.ShowMask.AutoSize = true;
            this.ShowMask.Location = new System.Drawing.Point(464, 94);
            this.ShowMask.Name = "ShowMask";
            this.ShowMask.Size = new System.Drawing.Size(63, 13);
            this.ShowMask.TabIndex = 13;
            this.ShowMask.TabStop = true;
            this.ShowMask.Text = "Show Mask";
            this.ShowMask.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ShowMask.Visible = false;
            this.ShowMask.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ShowMask_LinkClicked);
            // 
            // btnVitalSignHistClose
            // 
            this.btnVitalSignHistClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVitalSignHistClose.Image = ((System.Drawing.Image)(resources.GetObject("btnVitalSignHistClose.Image")));
            this.btnVitalSignHistClose.Location = new System.Drawing.Point(3, 1);
            this.btnVitalSignHistClose.Name = "btnVitalSignHistClose";
            this.btnVitalSignHistClose.Size = new System.Drawing.Size(26, 22);
            this.btnVitalSignHistClose.TabIndex = 13;
            this.ttOPD.SetToolTip(this.btnVitalSignHistClose, "Close List of Selectesd Parameter");
            this.btnVitalSignHistClose.UseVisualStyleBackColor = true;
            this.btnVitalSignHistClose.Click += new System.EventHandler(this.btnVitalSignHistClose_Click);
            // 
            // btnLabsTest
            // 
            this.btnLabsTest.BackgroundImage = global::OPD.Properties.Resources.icon_lab;
            this.btnLabsTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLabsTest.Location = new System.Drawing.Point(490, 168);
            this.btnLabsTest.Name = "btnLabsTest";
            this.btnLabsTest.Size = new System.Drawing.Size(25, 25);
            this.btnLabsTest.TabIndex = 142;
            this.ttOPD.SetToolTip(this.btnLabsTest, "Add Investigation");
            this.btnLabsTest.UseVisualStyleBackColor = true;
            this.btnLabsTest.Visible = false;
            this.btnLabsTest.Click += new System.EventHandler(this.btnLabsTest_Click);
            // 
            // btnInvestigation
            // 
            this.btnInvestigation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInvestigation.Image = ((System.Drawing.Image)(resources.GetObject("btnInvestigation.Image")));
            this.btnInvestigation.Location = new System.Drawing.Point(514, 168);
            this.btnInvestigation.Name = "btnInvestigation";
            this.btnInvestigation.Size = new System.Drawing.Size(25, 25);
            this.btnInvestigation.TabIndex = 10;
            this.ttOPD.SetToolTip(this.btnInvestigation, "Add Investigation");
            this.btnInvestigation.UseVisualStyleBackColor = true;
            this.btnInvestigation.Click += new System.EventHandler(this.btnInvestigation_Click);
            // 
            // btnPresentComplaint
            // 
            this.btnPresentComplaint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPresentComplaint.Image = ((System.Drawing.Image)(resources.GetObject("btnPresentComplaint.Image")));
            this.btnPresentComplaint.Location = new System.Drawing.Point(516, 2);
            this.btnPresentComplaint.Name = "btnPresentComplaint";
            this.btnPresentComplaint.Size = new System.Drawing.Size(25, 25);
            this.btnPresentComplaint.TabIndex = 4;
            this.ttOPD.SetToolTip(this.btnPresentComplaint, "Add Presenting Complaints");
            this.btnPresentComplaint.UseVisualStyleBackColor = true;
            this.btnPresentComplaint.Click += new System.EventHandler(this.btnPresentComplaint_Click);
            // 
            // dgComplaints
            // 
            this.dgComplaints.AllowUserToResizeColumns = false;
            this.dgComplaints.AllowUserToResizeRows = false;
            this.dgComplaints.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle98.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle98.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle98.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgComplaints.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle98;
            this.dgComplaints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgComplaints.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ComplaintsName,
            this.PrefIDComplaints,
            this.DelComp,
            this.CompDiseaseIDRef,
            this.CompImgDisease});
            this.dgComplaints.ContextMenuStrip = this.cmsComplaints;
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle99.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle99.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle99.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle99.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgComplaints.DefaultCellStyle = dataGridViewCellStyle99;
            this.dgComplaints.EnableHeadersVisualStyles = false;
            this.dgComplaints.Location = new System.Drawing.Point(270, 1);
            this.dgComplaints.MultiSelect = false;
            this.dgComplaints.Name = "dgComplaints";
            this.dgComplaints.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle100.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle100.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgComplaints.RowHeadersDefaultCellStyle = dataGridViewCellStyle100;
            this.dgComplaints.RowHeadersVisible = false;
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgComplaints.RowsDefaultCellStyle = dataGridViewCellStyle101;
            this.dgComplaints.RowTemplate.Height = 25;
            this.dgComplaints.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgComplaints.Size = new System.Drawing.Size(271, 170);
            this.dgComplaints.TabIndex = 3;
            this.dgComplaints.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgComplaints.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgComplaints.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgComplaints.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgComplaints.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // ComplaintsName
            // 
            this.ComplaintsName.FillWeight = 95F;
            this.ComplaintsName.HeaderText = "Presenting Complaints";
            this.ComplaintsName.Name = "ComplaintsName";
            // 
            // PrefIDComplaints
            // 
            this.PrefIDComplaints.HeaderText = "PrefID";
            this.PrefIDComplaints.Name = "PrefIDComplaints";
            this.PrefIDComplaints.ReadOnly = true;
            this.PrefIDComplaints.Visible = false;
            // 
            // DelComp
            // 
            this.DelComp.HeaderText = "DelComp";
            this.DelComp.Name = "DelComp";
            this.DelComp.Visible = false;
            // 
            // CompDiseaseIDRef
            // 
            this.CompDiseaseIDRef.HeaderText = "CompDiseaseIDRef";
            this.CompDiseaseIDRef.Name = "CompDiseaseIDRef";
            this.CompDiseaseIDRef.Visible = false;
            // 
            // CompImgDisease
            // 
            this.CompImgDisease.FillWeight = 5F;
            this.CompImgDisease.HeaderText = "";
            this.CompImgDisease.Image = global::OPD.Properties.Resources.NA1;
            this.CompImgDisease.Name = "CompImgDisease";
            // 
            // cmsComplaints
            // 
            this.cmsComplaints.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miComplaintsRename,
            this.miComplaintDelete,
            this.miCompStatus,
            this.miComplaintICDMoreInfo});
            this.cmsComplaints.Name = "cmsComplaints";
            this.cmsComplaints.Size = new System.Drawing.Size(169, 92);
            // 
            // miComplaintsRename
            // 
            this.miComplaintsRename.Enabled = false;
            this.miComplaintsRename.Name = "miComplaintsRename";
            this.miComplaintsRename.Size = new System.Drawing.Size(168, 22);
            this.miComplaintsRename.Text = "Rename Heading";
            this.miComplaintsRename.Click += new System.EventHandler(this.miComplaintsRename_Click);
            // 
            // miComplaintDelete
            // 
            this.miComplaintDelete.Name = "miComplaintDelete";
            this.miComplaintDelete.Size = new System.Drawing.Size(168, 22);
            this.miComplaintDelete.Text = "Delete";
            this.miComplaintDelete.Click += new System.EventHandler(this.miComplaintDelete_Click);
            // 
            // miCompStatus
            // 
            this.miCompStatus.Name = "miCompStatus";
            this.miCompStatus.Size = new System.Drawing.Size(168, 22);
            this.miCompStatus.Text = "Status";
            // 
            // miComplaintICDMoreInfo
            // 
            this.miComplaintICDMoreInfo.Name = "miComplaintICDMoreInfo";
            this.miComplaintICDMoreInfo.Size = new System.Drawing.Size(168, 22);
            this.miComplaintICDMoreInfo.Text = "More Information";
            this.miComplaintICDMoreInfo.Click += new System.EventHandler(this.miICDMoreInfo_Click);
            // 
            // btnSigns
            // 
            this.btnSigns.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSigns.Image = ((System.Drawing.Image)(resources.GetObject("btnSigns.Image")));
            this.btnSigns.Location = new System.Drawing.Point(785, 2);
            this.btnSigns.Name = "btnSigns";
            this.btnSigns.Size = new System.Drawing.Size(25, 25);
            this.btnSigns.TabIndex = 6;
            this.ttOPD.SetToolTip(this.btnSigns, "Add Signs");
            this.btnSigns.UseVisualStyleBackColor = true;
            this.btnSigns.Click += new System.EventHandler(this.btnSigns_Click);
            // 
            // btnPlans
            // 
            this.btnPlans.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlans.Image = ((System.Drawing.Image)(resources.GetObject("btnPlans.Image")));
            this.btnPlans.Location = new System.Drawing.Point(785, 168);
            this.btnPlans.Name = "btnPlans";
            this.btnPlans.Size = new System.Drawing.Size(25, 25);
            this.btnPlans.TabIndex = 12;
            this.ttOPD.SetToolTip(this.btnPlans, "Add Future Plans");
            this.btnPlans.UseVisualStyleBackColor = true;
            this.btnPlans.Click += new System.EventHandler(this.btnPlans_Click);
            // 
            // btnDiagnosis
            // 
            this.btnDiagnosis.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDiagnosis.BackgroundImage")));
            this.btnDiagnosis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiagnosis.Location = new System.Drawing.Point(245, 168);
            this.btnDiagnosis.Name = "btnDiagnosis";
            this.btnDiagnosis.Size = new System.Drawing.Size(25, 25);
            this.btnDiagnosis.TabIndex = 8;
            this.ttOPD.SetToolTip(this.btnDiagnosis, "Add Diagnosis");
            this.btnDiagnosis.UseVisualStyleBackColor = true;
            this.btnDiagnosis.Click += new System.EventHandler(this.btnDiagnosis_Click);
            // 
            // btnHistory
            // 
            this.btnHistory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHistory.BackgroundImage")));
            this.btnHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHistory.Location = new System.Drawing.Point(243, 2);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(25, 25);
            this.btnHistory.TabIndex = 2;
            this.ttOPD.SetToolTip(this.btnHistory, "Add History");
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // pnlInstructions
            // 
            this.pnlInstructions.AutoScroll = true;
            this.pnlInstructions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInstructions.CausesValidation = false;
            this.pnlInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlInstructions.Location = new System.Drawing.Point(440, 343);
            this.pnlInstructions.Name = "pnlInstructions";
            this.pnlInstructions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pnlInstructions.Size = new System.Drawing.Size(371, 142);
            this.pnlInstructions.TabIndex = 0;
            this.ttOPD.SetToolTip(this.pnlInstructions, "Instruction(Hidayyat) for Patient");
            // 
            // btnMedicine
            // 
            this.btnMedicine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMedicine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnMedicine.Image")));
            this.btnMedicine.Location = new System.Drawing.Point(409, 344);
            this.btnMedicine.Name = "btnMedicine";
            this.btnMedicine.Size = new System.Drawing.Size(25, 25);
            this.btnMedicine.TabIndex = 14;
            this.ttOPD.SetToolTip(this.btnMedicine, "Selection of Medicine and its Dosage");
            this.btnMedicine.UseVisualStyleBackColor = true;
            this.btnMedicine.Click += new System.EventHandler(this.btnMedicine_Click);
            // 
            // dgInvestigation
            // 
            this.dgInvestigation.AllowUserToResizeColumns = false;
            this.dgInvestigation.AllowUserToResizeRows = false;
            this.dgInvestigation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgInvestigation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgInvestigation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvestigation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvestigatioName,
            this.PrefIDInvestigation,
            this.DelInvest});
            this.dgInvestigation.ContextMenuStrip = this.cmsInvestigation;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgInvestigation.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgInvestigation.EnableHeadersVisualStyles = false;
            this.dgInvestigation.Location = new System.Drawing.Point(270, 172);
            this.dgInvestigation.MultiSelect = false;
            this.dgInvestigation.Name = "dgInvestigation";
            this.dgInvestigation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgInvestigation.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgInvestigation.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgInvestigation.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgInvestigation.RowTemplate.Height = 25;
            this.dgInvestigation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInvestigation.Size = new System.Drawing.Size(271, 170);
            this.dgInvestigation.TabIndex = 9;
            this.dgInvestigation.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgInvestigation.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgInvestigation.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgInvestigation.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgInvestigation.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // InvestigatioName
            // 
            this.InvestigatioName.HeaderText = "Investigation";
            this.InvestigatioName.Name = "InvestigatioName";
            // 
            // PrefIDInvestigation
            // 
            this.PrefIDInvestigation.HeaderText = "PrefID";
            this.PrefIDInvestigation.Name = "PrefIDInvestigation";
            this.PrefIDInvestigation.ReadOnly = true;
            this.PrefIDInvestigation.Visible = false;
            // 
            // DelInvest
            // 
            this.DelInvest.HeaderText = "DelInvest";
            this.DelInvest.Name = "DelInvest";
            this.DelInvest.Visible = false;
            // 
            // cmsInvestigation
            // 
            this.cmsInvestigation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miInvestigationRename,
            this.miInvestigationDelete,
            this.miInvestStatus});
            this.cmsInvestigation.Name = "cmsInvestigation";
            this.cmsInvestigation.Size = new System.Drawing.Size(166, 70);
            // 
            // miInvestigationRename
            // 
            this.miInvestigationRename.Name = "miInvestigationRename";
            this.miInvestigationRename.Size = new System.Drawing.Size(165, 22);
            this.miInvestigationRename.Text = "Rename Heading";
            this.miInvestigationRename.Click += new System.EventHandler(this.miInvestigationRename_Click);
            // 
            // miInvestigationDelete
            // 
            this.miInvestigationDelete.Name = "miInvestigationDelete";
            this.miInvestigationDelete.Size = new System.Drawing.Size(165, 22);
            this.miInvestigationDelete.Text = "Delete";
            this.miInvestigationDelete.Click += new System.EventHandler(this.miInvestigationDelete_Click);
            // 
            // miInvestStatus
            // 
            this.miInvestStatus.Name = "miInvestStatus";
            this.miInvestStatus.Size = new System.Drawing.Size(165, 22);
            this.miInvestStatus.Text = "Status";
            // 
            // dgMedicine
            // 
            this.dgMedicine.AllowUserToAddRows = false;
            this.dgMedicine.AllowUserToOrderColumns = true;
            this.dgMedicine.AllowUserToResizeColumns = false;
            this.dgMedicine.AllowUserToResizeRows = false;
            this.dgMedicine.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMedicine.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgMedicine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMedicine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MedID,
            this.Mode,
            this.Medname,
            this.MedDosage});
            this.dgMedicine.ContextMenuStrip = this.cmsMed;
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle102.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle102.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle102.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle102.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle102.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMedicine.DefaultCellStyle = dataGridViewCellStyle102;
            this.dgMedicine.EnableHeadersVisualStyles = false;
            this.dgMedicine.Location = new System.Drawing.Point(1, 343);
            this.dgMedicine.MultiSelect = false;
            this.dgMedicine.Name = "dgMedicine";
            this.dgMedicine.ReadOnly = true;
            this.dgMedicine.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle103.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle103.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle103.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMedicine.RowHeadersDefaultCellStyle = dataGridViewCellStyle103;
            this.dgMedicine.RowHeadersVisible = false;
            this.dgMedicine.RowTemplate.Height = 28;
            this.dgMedicine.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMedicine.Size = new System.Drawing.Size(438, 142);
            this.dgMedicine.TabIndex = 13;
            this.dgMedicine.Tag = "NotSet";
            this.ttOPD.SetToolTip(this.dgMedicine, "Doble Click to select Medicine and its Dosage");
            this.dgMedicine.DoubleClick += new System.EventHandler(this.dgMedicine_DoubleClick);
            this.dgMedicine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgMedicine_MouseDown);
            // 
            // MedID
            // 
            this.MedID.HeaderText = "MedID";
            this.MedID.Name = "MedID";
            this.MedID.ReadOnly = true;
            this.MedID.Visible = false;
            // 
            // Mode
            // 
            dataGridViewCellStyle14.NullValue = "U";
            this.Mode.DefaultCellStyle = dataGridViewCellStyle14;
            this.Mode.HeaderText = "Mode";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Visible = false;
            // 
            // Medname
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Medname.DefaultCellStyle = dataGridViewCellStyle15;
            this.Medname.FillWeight = 45F;
            this.Medname.HeaderText = "Medicine Name";
            this.Medname.Name = "Medname";
            this.Medname.ReadOnly = true;
            // 
            // MedDosage
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedDosage.DefaultCellStyle = dataGridViewCellStyle16;
            this.MedDosage.FillWeight = 55F;
            this.MedDosage.HeaderText = "Dosage";
            this.MedDosage.Name = "MedDosage";
            this.MedDosage.ReadOnly = true;
            // 
            // cmsMed
            // 
            this.cmsMed.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAllergies,
            this.miAR});
            this.cmsMed.Name = "cmsMed";
            this.cmsMed.Size = new System.Drawing.Size(166, 48);
            // 
            // miAllergies
            // 
            this.miAllergies.Name = "miAllergies";
            this.miAllergies.Size = new System.Drawing.Size(165, 22);
            this.miAllergies.Text = "Allergies";
            this.miAllergies.Click += new System.EventHandler(this.miAllergies_Click);
            // 
            // miAR
            // 
            this.miAR.Name = "miAR";
            this.miAR.Size = new System.Drawing.Size(165, 22);
            this.miAR.Text = "Adverse Reaction";
            this.miAR.Click += new System.EventHandler(this.miAR_Click);
            // 
            // dgPlans
            // 
            this.dgPlans.AllowUserToResizeColumns = false;
            this.dgPlans.AllowUserToResizeRows = false;
            this.dgPlans.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPlans.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgPlans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPlans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlansName,
            this.PrefIDPlans,
            this.DelPlan});
            this.dgPlans.ContextMenuStrip = this.cmsPlans;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPlans.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgPlans.EnableHeadersVisualStyles = false;
            this.dgPlans.Location = new System.Drawing.Point(542, 172);
            this.dgPlans.MultiSelect = false;
            this.dgPlans.Name = "dgPlans";
            this.dgPlans.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPlans.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgPlans.RowHeadersVisible = false;
            dataGridViewCellStyle104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgPlans.RowsDefaultCellStyle = dataGridViewCellStyle104;
            this.dgPlans.RowTemplate.Height = 25;
            this.dgPlans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPlans.Size = new System.Drawing.Size(269, 170);
            this.dgPlans.TabIndex = 11;
            this.dgPlans.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgPlans.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgPlans.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgPlans.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgPlans.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // PlansName
            // 
            this.PlansName.HeaderText = "Plans";
            this.PlansName.Name = "PlansName";
            // 
            // PrefIDPlans
            // 
            this.PrefIDPlans.HeaderText = "PrefID";
            this.PrefIDPlans.Name = "PrefIDPlans";
            this.PrefIDPlans.ReadOnly = true;
            this.PrefIDPlans.Visible = false;
            // 
            // DelPlan
            // 
            this.DelPlan.HeaderText = "DelPlan";
            this.DelPlan.Name = "DelPlan";
            this.DelPlan.Visible = false;
            // 
            // cmsPlans
            // 
            this.cmsPlans.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPlansRename,
            this.miPlansDelete,
            this.miPlanStatus});
            this.cmsPlans.Name = "cmsPlans";
            this.cmsPlans.Size = new System.Drawing.Size(166, 70);
            // 
            // miPlansRename
            // 
            this.miPlansRename.Name = "miPlansRename";
            this.miPlansRename.Size = new System.Drawing.Size(165, 22);
            this.miPlansRename.Text = "Rename Heading";
            this.miPlansRename.Click += new System.EventHandler(this.miPlansRename_Click);
            // 
            // miPlansDelete
            // 
            this.miPlansDelete.Name = "miPlansDelete";
            this.miPlansDelete.Size = new System.Drawing.Size(165, 22);
            this.miPlansDelete.Text = "Delete";
            this.miPlansDelete.Click += new System.EventHandler(this.miPlansDelete_Click);
            // 
            // miPlanStatus
            // 
            this.miPlanStatus.Name = "miPlanStatus";
            this.miPlanStatus.Size = new System.Drawing.Size(165, 22);
            this.miPlanStatus.Text = "Status";
            // 
            // dgDiagnosis
            // 
            this.dgDiagnosis.AllowUserToResizeColumns = false;
            this.dgDiagnosis.AllowUserToResizeRows = false;
            this.dgDiagnosis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDiagnosis.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgDiagnosis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDiagnosis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DiagnosisName,
            this.PrefIDDiagnosis,
            this.DelDiagnosis,
            this.DiagnosisDiseaseRefID,
            this.DiagnoImgDisease});
            this.dgDiagnosis.ContextMenuStrip = this.cmsDiagnosis;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDiagnosis.DefaultCellStyle = dataGridViewCellStyle24;
            this.dgDiagnosis.EnableHeadersVisualStyles = false;
            this.dgDiagnosis.Location = new System.Drawing.Point(1, 172);
            this.dgDiagnosis.MultiSelect = false;
            this.dgDiagnosis.Name = "dgDiagnosis";
            this.dgDiagnosis.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDiagnosis.RowHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgDiagnosis.RowHeadersVisible = false;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgDiagnosis.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this.dgDiagnosis.RowTemplate.Height = 25;
            this.dgDiagnosis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiagnosis.Size = new System.Drawing.Size(268, 170);
            this.dgDiagnosis.TabIndex = 7;
            this.dgDiagnosis.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgDiagnosis.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgDiagnosis.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgDiagnosis.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgDiagnosis.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // DiagnosisName
            // 
            this.DiagnosisName.FillWeight = 95F;
            this.DiagnosisName.HeaderText = "Diagnosis";
            this.DiagnosisName.Name = "DiagnosisName";
            // 
            // PrefIDDiagnosis
            // 
            this.PrefIDDiagnosis.HeaderText = "PrefID";
            this.PrefIDDiagnosis.Name = "PrefIDDiagnosis";
            this.PrefIDDiagnosis.ReadOnly = true;
            this.PrefIDDiagnosis.Visible = false;
            // 
            // DelDiagnosis
            // 
            this.DelDiagnosis.HeaderText = "DelDiagno";
            this.DelDiagnosis.Name = "DelDiagnosis";
            this.DelDiagnosis.Visible = false;
            // 
            // DiagnosisDiseaseRefID
            // 
            this.DiagnosisDiseaseRefID.HeaderText = "DiagnosisDiseaseRefID";
            this.DiagnosisDiseaseRefID.Name = "DiagnosisDiseaseRefID";
            this.DiagnosisDiseaseRefID.Visible = false;
            // 
            // DiagnoImgDisease
            // 
            this.DiagnoImgDisease.FillWeight = 5F;
            this.DiagnoImgDisease.HeaderText = "";
            this.DiagnoImgDisease.Image = global::OPD.Properties.Resources.NA1;
            this.DiagnoImgDisease.Name = "DiagnoImgDisease";
            // 
            // cmsDiagnosis
            // 
            this.cmsDiagnosis.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDiagnosisRename,
            this.miDiagnosisDelete,
            this.miDiagnosisStatus,
            this.miDiagnosisICDMoreInfo});
            this.cmsDiagnosis.Name = "cmsDiagnosis";
            this.cmsDiagnosis.Size = new System.Drawing.Size(169, 92);
            // 
            // miDiagnosisRename
            // 
            this.miDiagnosisRename.Name = "miDiagnosisRename";
            this.miDiagnosisRename.Size = new System.Drawing.Size(168, 22);
            this.miDiagnosisRename.Text = "Rename Heading";
            this.miDiagnosisRename.Click += new System.EventHandler(this.miDiagnosisRename_Click);
            // 
            // miDiagnosisDelete
            // 
            this.miDiagnosisDelete.Name = "miDiagnosisDelete";
            this.miDiagnosisDelete.Size = new System.Drawing.Size(168, 22);
            this.miDiagnosisDelete.Text = "Delete";
            this.miDiagnosisDelete.Click += new System.EventHandler(this.miDiagnosisDelete_Click);
            // 
            // miDiagnosisStatus
            // 
            this.miDiagnosisStatus.Name = "miDiagnosisStatus";
            this.miDiagnosisStatus.Size = new System.Drawing.Size(168, 22);
            this.miDiagnosisStatus.Text = "Status";
            // 
            // miDiagnosisICDMoreInfo
            // 
            this.miDiagnosisICDMoreInfo.Name = "miDiagnosisICDMoreInfo";
            this.miDiagnosisICDMoreInfo.Size = new System.Drawing.Size(168, 22);
            this.miDiagnosisICDMoreInfo.Text = "More Information";
            this.miDiagnosisICDMoreInfo.Click += new System.EventHandler(this.miICDMoreInfo_Click);
            // 
            // dgHistory
            // 
            this.dgHistory.AllowUserToResizeColumns = false;
            this.dgHistory.AllowUserToResizeRows = false;
            this.dgHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dgHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HistoryName,
            this.PrefIDHistory,
            this.DelHist,
            this.HistDiseaseIDRef,
            this.ImgHistAvail});
            this.dgHistory.ContextMenuStrip = this.cmsHistory;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgHistory.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgHistory.EnableHeadersVisualStyles = false;
            this.dgHistory.Location = new System.Drawing.Point(1, 1);
            this.dgHistory.MultiSelect = false;
            this.dgHistory.Name = "dgHistory";
            this.dgHistory.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHistory.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dgHistory.RowHeadersVisible = false;
            this.dgHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgHistory.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgHistory.RowTemplate.Height = 25;
            this.dgHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgHistory.Size = new System.Drawing.Size(268, 170);
            this.dgHistory.TabIndex = 1;
            this.dgHistory.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgHistory.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgHistory.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgHistory.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgHistory.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // HistoryName
            // 
            this.HistoryName.FillWeight = 95F;
            this.HistoryName.HeaderText = "History";
            this.HistoryName.Name = "HistoryName";
            // 
            // PrefIDHistory
            // 
            this.PrefIDHistory.HeaderText = "PrefID";
            this.PrefIDHistory.Name = "PrefIDHistory";
            this.PrefIDHistory.Visible = false;
            // 
            // DelHist
            // 
            this.DelHist.HeaderText = "Del";
            this.DelHist.Name = "DelHist";
            this.DelHist.Visible = false;
            // 
            // HistDiseaseIDRef
            // 
            this.HistDiseaseIDRef.HeaderText = "HistDiseaseIDRef";
            this.HistDiseaseIDRef.Name = "HistDiseaseIDRef";
            this.HistDiseaseIDRef.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HistDiseaseIDRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.HistDiseaseIDRef.Visible = false;
            // 
            // ImgHistAvail
            // 
            this.ImgHistAvail.FillWeight = 5F;
            this.ImgHistAvail.HeaderText = "";
            this.ImgHistAvail.Image = global::OPD.Properties.Resources.NA1;
            this.ImgHistAvail.Name = "ImgHistAvail";
            // 
            // cmsHistory
            // 
            this.cmsHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miHistoryRename,
            this.miHistoryDelete,
            this.miHistoryStatus,
            this.miHistICDMoreInfo});
            this.cmsHistory.Name = "cmsHistory";
            this.cmsHistory.Size = new System.Drawing.Size(169, 92);
            // 
            // miHistoryRename
            // 
            this.miHistoryRename.Name = "miHistoryRename";
            this.miHistoryRename.Size = new System.Drawing.Size(168, 22);
            this.miHistoryRename.Text = "Rename Heading";
            this.miHistoryRename.Click += new System.EventHandler(this.miHistoryRename_Click);
            // 
            // miHistoryDelete
            // 
            this.miHistoryDelete.Name = "miHistoryDelete";
            this.miHistoryDelete.Size = new System.Drawing.Size(168, 22);
            this.miHistoryDelete.Text = "Delete";
            this.miHistoryDelete.Click += new System.EventHandler(this.miHistoryDelete_Click);
            // 
            // miHistoryStatus
            // 
            this.miHistoryStatus.Name = "miHistoryStatus";
            this.miHistoryStatus.Size = new System.Drawing.Size(168, 22);
            this.miHistoryStatus.Text = "Status";
            // 
            // miHistICDMoreInfo
            // 
            this.miHistICDMoreInfo.Name = "miHistICDMoreInfo";
            this.miHistICDMoreInfo.Size = new System.Drawing.Size(168, 22);
            this.miHistICDMoreInfo.Text = "More Information";
            this.miHistICDMoreInfo.Click += new System.EventHandler(this.miICDMoreInfo_Click);
            // 
            // dgSigns
            // 
            this.dgSigns.AllowUserToResizeColumns = false;
            this.dgSigns.AllowUserToResizeRows = false;
            this.dgSigns.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSigns.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgSigns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSigns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SignsName,
            this.PrefIDSigns,
            this.DelSign,
            this.SignDiseaseIDRef,
            this.SignImgDisease});
            this.dgSigns.ContextMenuStrip = this.cmsSigns;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSigns.DefaultCellStyle = dataGridViewCellStyle32;
            this.dgSigns.EnableHeadersVisualStyles = false;
            this.dgSigns.Location = new System.Drawing.Point(542, 1);
            this.dgSigns.MultiSelect = false;
            this.dgSigns.Name = "dgSigns";
            this.dgSigns.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSigns.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgSigns.RowHeadersVisible = false;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgSigns.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.dgSigns.RowTemplate.Height = 25;
            this.dgSigns.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSigns.Size = new System.Drawing.Size(269, 170);
            this.dgSigns.TabIndex = 5;
            this.dgSigns.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgParameter_CellEndEdit);
            this.dgSigns.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrameter_CellMouseEnter);
            this.dgSigns.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_EditingControlShowing);
            this.dgSigns.Click += new System.EventHandler(this.dgParameter_Click);
            this.dgSigns.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgParameter_MouseDown);
            // 
            // SignsName
            // 
            this.SignsName.FillWeight = 95F;
            this.SignsName.HeaderText = "Signs";
            this.SignsName.Name = "SignsName";
            // 
            // PrefIDSigns
            // 
            this.PrefIDSigns.HeaderText = "PrefID";
            this.PrefIDSigns.Name = "PrefIDSigns";
            this.PrefIDSigns.ReadOnly = true;
            this.PrefIDSigns.Visible = false;
            // 
            // DelSign
            // 
            this.DelSign.HeaderText = "DelSign";
            this.DelSign.Name = "DelSign";
            this.DelSign.Visible = false;
            // 
            // SignDiseaseIDRef
            // 
            this.SignDiseaseIDRef.HeaderText = "SignDiseaseIDRef";
            this.SignDiseaseIDRef.Name = "SignDiseaseIDRef";
            this.SignDiseaseIDRef.Visible = false;
            // 
            // SignImgDisease
            // 
            this.SignImgDisease.FillWeight = 5F;
            this.SignImgDisease.HeaderText = "";
            this.SignImgDisease.Image = global::OPD.Properties.Resources.NA1;
            this.SignImgDisease.Name = "SignImgDisease";
            // 
            // cmsSigns
            // 
            this.cmsSigns.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSignsRename,
            this.miSignsDelete,
            this.miSignStatus,
            this.miSignsICDMoreInfo});
            this.cmsSigns.Name = "cmsSigns";
            this.cmsSigns.Size = new System.Drawing.Size(169, 92);
            // 
            // miSignsRename
            // 
            this.miSignsRename.Name = "miSignsRename";
            this.miSignsRename.Size = new System.Drawing.Size(168, 22);
            this.miSignsRename.Text = "Rename Heading";
            this.miSignsRename.Click += new System.EventHandler(this.miSignsRename_Click);
            // 
            // miSignsDelete
            // 
            this.miSignsDelete.Name = "miSignsDelete";
            this.miSignsDelete.Size = new System.Drawing.Size(168, 22);
            this.miSignsDelete.Text = "Delete";
            this.miSignsDelete.Click += new System.EventHandler(this.miSignsDelete_Click);
            // 
            // miSignStatus
            // 
            this.miSignStatus.Name = "miSignStatus";
            this.miSignStatus.Size = new System.Drawing.Size(168, 22);
            this.miSignStatus.Text = "Status";
            // 
            // miSignsICDMoreInfo
            // 
            this.miSignsICDMoreInfo.Name = "miSignsICDMoreInfo";
            this.miSignsICDMoreInfo.Size = new System.Drawing.Size(168, 22);
            this.miSignsICDMoreInfo.Text = "More Information";
            this.miSignsICDMoreInfo.Click += new System.EventHandler(this.miICDMoreInfo_Click);
            // 
            // pnlMedHistGrid
            // 
            this.pnlMedHistGrid.Controls.Add(this.btnMedHistClose);
            this.pnlMedHistGrid.Controls.Add(this.lblHeadingMedHist);
            this.pnlMedHistGrid.Controls.Add(this.dgMedHist);
            this.pnlMedHistGrid.Location = new System.Drawing.Point(270, 2);
            this.pnlMedHistGrid.Name = "pnlMedHistGrid";
            this.pnlMedHistGrid.Size = new System.Drawing.Size(541, 337);
            this.pnlMedHistGrid.TabIndex = 140;
            this.pnlMedHistGrid.Visible = false;
            // 
            // btnMedHistClose
            // 
            this.btnMedHistClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMedHistClose.Image = ((System.Drawing.Image)(resources.GetObject("btnMedHistClose.Image")));
            this.btnMedHistClose.Location = new System.Drawing.Point(513, 1);
            this.btnMedHistClose.Name = "btnMedHistClose";
            this.btnMedHistClose.Size = new System.Drawing.Size(26, 22);
            this.btnMedHistClose.TabIndex = 12;
            this.ttOPD.SetToolTip(this.btnMedHistClose, "Close List of Selectesd Parameter");
            this.btnMedHistClose.UseVisualStyleBackColor = true;
            this.btnMedHistClose.Click += new System.EventHandler(this.btnMedHistClose_Click);
            // 
            // lblHeadingMedHist
            // 
            this.lblHeadingMedHist.AutoSize = true;
            this.lblHeadingMedHist.Location = new System.Drawing.Point(228, 5);
            this.lblHeadingMedHist.Name = "lblHeadingMedHist";
            this.lblHeadingMedHist.Size = new System.Drawing.Size(85, 13);
            this.lblHeadingMedHist.TabIndex = 1;
            this.lblHeadingMedHist.Tag = "transdisplay";
            this.lblHeadingMedHist.Text = "Medicine History";
            // 
            // dgMedHist
            // 
            this.dgMedHist.AllowUserToAddRows = false;
            this.dgMedHist.AllowUserToDeleteRows = false;
            this.dgMedHist.AllowUserToResizeColumns = false;
            this.dgMedHist.AllowUserToResizeRows = false;
            this.dgMedHist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMedHist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dgMedHist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgMedHist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MHVisitDate,
            this.MHOPDID,
            this.MHMedicineID,
            this.MHMedName,
            this.MHDosage});
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMedHist.DefaultCellStyle = dataGridViewCellStyle38;
            this.dgMedHist.Location = new System.Drawing.Point(3, 23);
            this.dgMedHist.MultiSelect = false;
            this.dgMedHist.Name = "dgMedHist";
            this.dgMedHist.ReadOnly = true;
            this.dgMedHist.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle105.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle105.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle105.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle105.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle105.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMedHist.RowHeadersDefaultCellStyle = dataGridViewCellStyle105;
            this.dgMedHist.RowHeadersVisible = false;
            this.dgMedHist.RowTemplate.Height = 35;
            this.dgMedHist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMedHist.Size = new System.Drawing.Size(535, 312);
            this.dgMedHist.TabIndex = 0;
            this.dgMedHist.VirtualMode = true;
            // 
            // MHVisitDate
            // 
            this.MHVisitDate.DataPropertyName = "VisitDate";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle36.NullValue = null;
            this.MHVisitDate.DefaultCellStyle = dataGridViewCellStyle36;
            this.MHVisitDate.HeaderText = "Visit Date";
            this.MHVisitDate.Name = "MHVisitDate";
            this.MHVisitDate.ReadOnly = true;
            this.MHVisitDate.Visible = false;
            // 
            // MHOPDID
            // 
            this.MHOPDID.DataPropertyName = "OPDID";
            this.MHOPDID.HeaderText = "OPDID";
            this.MHOPDID.Name = "MHOPDID";
            this.MHOPDID.ReadOnly = true;
            this.MHOPDID.Visible = false;
            // 
            // MHMedicineID
            // 
            this.MHMedicineID.DataPropertyName = "MedicineID";
            this.MHMedicineID.HeaderText = "MedicineID";
            this.MHMedicineID.Name = "MHMedicineID";
            this.MHMedicineID.ReadOnly = true;
            this.MHMedicineID.Visible = false;
            // 
            // MHMedName
            // 
            this.MHMedName.DataPropertyName = "MedicineName";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.MHMedName.DefaultCellStyle = dataGridViewCellStyle37;
            this.MHMedName.FillWeight = 45F;
            this.MHMedName.HeaderText = "Medicine Name";
            this.MHMedName.Name = "MHMedName";
            this.MHMedName.ReadOnly = true;
            // 
            // MHDosage
            // 
            this.MHDosage.DataPropertyName = "MedicineDosage";
            this.MHDosage.FillWeight = 55F;
            this.MHDosage.HeaderText = "Dosage";
            this.MHDosage.Name = "MHDosage";
            this.MHDosage.ReadOnly = true;
            // 
            // pnlDGList
            // 
            this.pnlDGList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDGList.Controls.Add(this.txtSearch);
            this.pnlDGList.Controls.Add(this.btnListPnl);
            this.pnlDGList.Controls.Add(this.dgList);
            this.pnlDGList.Controls.Add(this.SavePatient);
            this.pnlDGList.Location = new System.Drawing.Point(271, 1);
            this.pnlDGList.Name = "pnlDGList";
            this.pnlDGList.Size = new System.Drawing.Size(270, 341);
            this.pnlDGList.TabIndex = 4;
            this.pnlDGList.Tag = "";
            this.pnlDGList.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Location = new System.Drawing.Point(1, 1);
            this.txtSearch.MaxLength = 255;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSearch.Size = new System.Drawing.Size(239, 20);
            this.txtSearch.TabIndex = 12;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnListPnl
            // 
            this.btnListPnl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnListPnl.Image = ((System.Drawing.Image)(resources.GetObject("btnListPnl.Image")));
            this.btnListPnl.Location = new System.Drawing.Point(242, 1);
            this.btnListPnl.Name = "btnListPnl";
            this.btnListPnl.Size = new System.Drawing.Size(26, 22);
            this.btnListPnl.TabIndex = 11;
            this.ttOPD.SetToolTip(this.btnListPnl, "Close List of Selectesd Parameter");
            this.btnListPnl.UseVisualStyleBackColor = true;
            this.btnListPnl.Click += new System.EventHandler(this.btnListPnl_Click);
            // 
            // dgList
            // 
            this.dgList.AllowUserToAddRows = false;
            this.dgList.AllowUserToDeleteRows = false;
            this.dgList.AllowUserToResizeColumns = false;
            this.dgList.AllowUserToResizeRows = false;
            this.dgList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle106.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle106.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle106;
            this.dgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PName,
            this.PrefID,
            this.ListDisRefID});
            this.dgList.ContextMenuStrip = this.cmsItem;
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle107.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle107.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle107.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle107.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle107.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgList.DefaultCellStyle = dataGridViewCellStyle107;
            this.dgList.Location = new System.Drawing.Point(1, 23);
            this.dgList.MultiSelect = false;
            this.dgList.Name = "dgList";
            this.dgList.ReadOnly = true;
            this.dgList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle108.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle108.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle108.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle108.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle108.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgList.RowHeadersDefaultCellStyle = dataGridViewCellStyle108;
            this.dgList.RowHeadersVisible = false;
            this.dgList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle109.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgList.RowsDefaultCellStyle = dataGridViewCellStyle109;
            this.dgList.RowTemplate.Height = 27;
            this.dgList.Size = new System.Drawing.Size(266, 315);
            this.dgList.TabIndex = 0;
            this.ttOPD.SetToolTip(this.dgList, "List of Selected Parameter");
            this.dgList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgList_CellDoubleClick);
            this.dgList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgList_MouseDown);
            // 
            // PName
            // 
            this.PName.DataPropertyName = "Name";
            this.PName.HeaderText = "Name";
            this.PName.Name = "PName";
            this.PName.ReadOnly = true;
            // 
            // PrefID
            // 
            this.PrefID.DataPropertyName = "PrefID";
            this.PrefID.HeaderText = "PrefID";
            this.PrefID.Name = "PrefID";
            this.PrefID.ReadOnly = true;
            this.PrefID.Visible = false;
            // 
            // ListDisRefID
            // 
            this.ListDisRefID.DataPropertyName = "DiseaseIDref";
            this.ListDisRefID.HeaderText = "";
            this.ListDisRefID.Name = "ListDisRefID";
            this.ListDisRefID.ReadOnly = true;
            this.ListDisRefID.Visible = false;
            // 
            // cmsItem
            // 
            this.cmsItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miItemDelete});
            this.cmsItem.Name = "cmsItem";
            this.cmsItem.Size = new System.Drawing.Size(108, 26);
            // 
            // miItemDelete
            // 
            this.miItemDelete.Name = "miItemDelete";
            this.miItemDelete.Size = new System.Drawing.Size(107, 22);
            this.miItemDelete.Text = "Delete";
            this.miItemDelete.Click += new System.EventHandler(this.miItemDelete_Click);
            // 
            // SavePatient
            // 
            this.SavePatient.AutoSize = true;
            this.SavePatient.Location = new System.Drawing.Point(43, 57);
            this.SavePatient.Name = "SavePatient";
            this.SavePatient.Size = new System.Drawing.Size(65, 13);
            this.SavePatient.TabIndex = 10;
            this.SavePatient.Text = "SavePatient";
            this.SavePatient.Visible = false;
            // 
            // pnlVitalSigns
            // 
            this.pnlVitalSigns.BackColor = System.Drawing.Color.Lavender;
            this.pnlVitalSigns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVitalSigns.Controls.Add(this.VideoOption);
            this.pnlVitalSigns.Controls.Add(this.lblBSAUnit);
            this.pnlVitalSigns.Controls.Add(this.lblBMIUnit);
            this.pnlVitalSigns.Controls.Add(this.txtRespiratoryRate);
            this.pnlVitalSigns.Controls.Add(this.txtHeartRate);
            this.pnlVitalSigns.Controls.Add(this.lblRespiratoryRate);
            this.pnlVitalSigns.Controls.Add(this.lblHeartRate);
            this.pnlVitalSigns.Controls.Add(this.btnVitalSignHist);
            this.pnlVitalSigns.Controls.Add(this.btnPreviousVisit);
            this.pnlVitalSigns.Controls.Add(this.txtBSAStatus);
            this.pnlVitalSigns.Controls.Add(this.txtBMIStatus);
            this.pnlVitalSigns.Controls.Add(this.lblVitalSigns);
            this.pnlVitalSigns.Controls.Add(this.txtBSA);
            this.pnlVitalSigns.Controls.Add(this.txtBMI);
            this.pnlVitalSigns.Controls.Add(this.txtBP);
            this.pnlVitalSigns.Controls.Add(this.cboHeightUnit);
            this.pnlVitalSigns.Controls.Add(this.cboWeightUnit);
            this.pnlVitalSigns.Controls.Add(this.cboTempUnit);
            this.pnlVitalSigns.Controls.Add(this.txtHeight);
            this.pnlVitalSigns.Controls.Add(this.txtWeight);
            this.pnlVitalSigns.Controls.Add(this.txtPulse);
            this.pnlVitalSigns.Controls.Add(this.txtTemp);
            this.pnlVitalSigns.Controls.Add(this.lblPulseUnit);
            this.pnlVitalSigns.Controls.Add(this.lblBPUnit);
            this.pnlVitalSigns.Controls.Add(this.lblHeight);
            this.pnlVitalSigns.Controls.Add(this.lblPulse);
            this.pnlVitalSigns.Controls.Add(this.lblBSA);
            this.pnlVitalSigns.Controls.Add(this.lblBMI);
            this.pnlVitalSigns.Controls.Add(this.lblBSAStatus);
            this.pnlVitalSigns.Controls.Add(this.lblBMIStatus);
            this.pnlVitalSigns.Controls.Add(this.lblWeight);
            this.pnlVitalSigns.Controls.Add(this.lblBP);
            this.pnlVitalSigns.Controls.Add(this.lblTemp);
            this.pnlVitalSigns.Location = new System.Drawing.Point(2, 167);
            this.pnlVitalSigns.Name = "pnlVitalSigns";
            this.pnlVitalSigns.Size = new System.Drawing.Size(1012, 47);
            this.pnlVitalSigns.TabIndex = 1;
            this.pnlVitalSigns.Visible = false;
            // 
            // VideoOption
            // 
            this.VideoOption.AutoSize = true;
            this.VideoOption.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VideoOption.Checked = true;
            this.VideoOption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VideoOption.Location = new System.Drawing.Point(118, 26);
            this.VideoOption.Name = "VideoOption";
            this.VideoOption.Size = new System.Drawing.Size(114, 17);
            this.VideoOption.TabIndex = 146;
            this.VideoOption.Text = "Video Consultation";
            this.VideoOption.UseVisualStyleBackColor = true;
            // 
            // lblBSAUnit
            // 
            this.lblBSAUnit.AutoSize = true;
            this.lblBSAUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSAUnit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBSAUnit.Location = new System.Drawing.Point(935, 28);
            this.lblBSAUnit.Name = "lblBSAUnit";
            this.lblBSAUnit.Size = new System.Drawing.Size(37, 15);
            this.lblBSAUnit.TabIndex = 138;
            this.lblBSAUnit.Tag = "NotSet";
            this.lblBSAUnit.Text = "m*m";
            // 
            // lblBMIUnit
            // 
            this.lblBMIUnit.AutoSize = true;
            this.lblBMIUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMIUnit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMIUnit.Location = new System.Drawing.Point(935, 6);
            this.lblBMIUnit.Name = "lblBMIUnit";
            this.lblBMIUnit.Size = new System.Drawing.Size(58, 15);
            this.lblBMIUnit.TabIndex = 14;
            this.lblBMIUnit.Tag = "NotSet";
            this.lblBMIUnit.Text = "Kg/m*m";
            // 
            // txtRespiratoryRate
            // 
            this.txtRespiratoryRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRespiratoryRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespiratoryRate.ForeColor = System.Drawing.Color.Black;
            this.txtRespiratoryRate.Location = new System.Drawing.Point(769, 24);
            this.txtRespiratoryRate.Name = "txtRespiratoryRate";
            this.txtRespiratoryRate.Size = new System.Drawing.Size(44, 21);
            this.txtRespiratoryRate.TabIndex = 145;
            this.txtRespiratoryRate.TabStop = false;
            this.txtRespiratoryRate.Tag = "Notset";
            // 
            // txtHeartRate
            // 
            this.txtHeartRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeartRate.ForeColor = System.Drawing.Color.Black;
            this.txtHeartRate.Location = new System.Drawing.Point(769, 2);
            this.txtHeartRate.Name = "txtHeartRate";
            this.txtHeartRate.Size = new System.Drawing.Size(44, 21);
            this.txtHeartRate.TabIndex = 144;
            this.txtHeartRate.TabStop = false;
            this.txtHeartRate.Tag = "Notset";
            // 
            // lblRespiratoryRate
            // 
            this.lblRespiratoryRate.AutoSize = true;
            this.lblRespiratoryRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespiratoryRate.ForeColor = System.Drawing.Color.Black;
            this.lblRespiratoryRate.Location = new System.Drawing.Point(659, 27);
            this.lblRespiratoryRate.Name = "lblRespiratoryRate";
            this.lblRespiratoryRate.Size = new System.Drawing.Size(104, 15);
            this.lblRespiratoryRate.TabIndex = 143;
            this.lblRespiratoryRate.Tag = "NotSet";
            this.lblRespiratoryRate.Text = "Respiratory Rate :";
            this.lblRespiratoryRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHeartRate
            // 
            this.lblHeartRate.AutoSize = true;
            this.lblHeartRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeartRate.ForeColor = System.Drawing.Color.Black;
            this.lblHeartRate.Location = new System.Drawing.Point(660, 5);
            this.lblHeartRate.Name = "lblHeartRate";
            this.lblHeartRate.Size = new System.Drawing.Size(72, 15);
            this.lblHeartRate.TabIndex = 142;
            this.lblHeartRate.Tag = "NotSet";
            this.lblHeartRate.Text = "Heart Rate :";
            this.lblHeartRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnVitalSignHist
            // 
            this.btnVitalSignHist.Location = new System.Drawing.Point(965, 2);
            this.btnVitalSignHist.Name = "btnVitalSignHist";
            this.btnVitalSignHist.Size = new System.Drawing.Size(2, 2);
            this.btnVitalSignHist.TabIndex = 141;
            this.btnVitalSignHist.UseVisualStyleBackColor = true;
            this.btnVitalSignHist.Visible = false;
            this.btnVitalSignHist.Click += new System.EventHandler(this.btnVitalSignHist_Click);
            // 
            // btnPreviousVisit
            // 
            this.btnPreviousVisit.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnPreviousVisit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPreviousVisit.BackgroundImage")));
            this.btnPreviousVisit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPreviousVisit.Location = new System.Drawing.Point(0, 18);
            this.btnPreviousVisit.Name = "btnPreviousVisit";
            this.btnPreviousVisit.Size = new System.Drawing.Size(33, 27);
            this.btnPreviousVisit.TabIndex = 139;
            this.ttOPD.SetToolTip(this.btnPreviousVisit, "Patient Last visit information");
            this.btnPreviousVisit.UseVisualStyleBackColor = false;
            this.btnPreviousVisit.Click += new System.EventHandler(this.btnPreviousVisit_Click);
            // 
            // txtBSAStatus
            // 
            this.txtBSAStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBSAStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSAStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBSAStatus.Location = new System.Drawing.Point(934, 23);
            this.txtBSAStatus.Name = "txtBSAStatus";
            this.txtBSAStatus.ReadOnly = true;
            this.txtBSAStatus.Size = new System.Drawing.Size(10, 21);
            this.txtBSAStatus.TabIndex = 137;
            this.txtBSAStatus.TabStop = false;
            this.txtBSAStatus.Tag = "Notset";
            this.txtBSAStatus.Visible = false;
            // 
            // txtBMIStatus
            // 
            this.txtBMIStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBMIStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBMIStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBMIStatus.Location = new System.Drawing.Point(934, 1);
            this.txtBMIStatus.Name = "txtBMIStatus";
            this.txtBMIStatus.ReadOnly = true;
            this.txtBMIStatus.Size = new System.Drawing.Size(10, 21);
            this.txtBMIStatus.TabIndex = 136;
            this.txtBMIStatus.TabStop = false;
            this.txtBMIStatus.Tag = "Notset";
            this.txtBMIStatus.Visible = false;
            // 
            // lblVitalSigns
            // 
            this.lblVitalSigns.AutoSize = true;
            this.lblVitalSigns.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblVitalSigns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVitalSigns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVitalSigns.Location = new System.Drawing.Point(1, 1);
            this.lblVitalSigns.Name = "lblVitalSigns";
            this.lblVitalSigns.Size = new System.Drawing.Size(85, 17);
            this.lblVitalSigns.TabIndex = 9;
            this.lblVitalSigns.Tag = "self";
            this.lblVitalSigns.Text = "Vital Signs :";
            // 
            // txtBSA
            // 
            this.txtBSA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSA.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBSA.Location = new System.Drawing.Point(886, 25);
            this.txtBSA.Name = "txtBSA";
            this.txtBSA.ReadOnly = true;
            this.txtBSA.Size = new System.Drawing.Size(46, 21);
            this.txtBSA.TabIndex = 13;
            this.txtBSA.TabStop = false;
            this.txtBSA.Tag = "Notset";
            this.ttOPD.SetToolTip(this.txtBSA, "Patient Body surface Area (BSA)");
            // 
            // txtBMI
            // 
            this.txtBMI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBMI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBMI.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtBMI.Location = new System.Drawing.Point(886, 3);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.ReadOnly = true;
            this.txtBMI.Size = new System.Drawing.Size(46, 21);
            this.txtBMI.TabIndex = 12;
            this.txtBMI.TabStop = false;
            this.txtBMI.Tag = "Notset";
            this.ttOPD.SetToolTip(this.txtBMI, "Patient  Body Mass Index (BMI)");
            // 
            // txtBP
            // 
            this.txtBP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBP.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBP.HidePromptOnLeave = true;
            this.txtBP.Location = new System.Drawing.Point(339, 1);
            this.txtBP.Mask = "000/000";
            this.txtBP.Name = "txtBP";
            this.txtBP.Size = new System.Drawing.Size(67, 21);
            this.txtBP.SkipLiterals = false;
            this.txtBP.TabIndex = 2;
            this.ttOPD.SetToolTip(this.txtBP, "Patient  Blood Pressure (BP )");
            this.txtBP.Enter += new System.EventHandler(this.txtBP_Enter);
            this.txtBP.Leave += new System.EventHandler(this.txtBP_Leave);
            // 
            // cboHeightUnit
            // 
            this.cboHeightUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHeightUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHeightUnit.FormattingEnabled = true;
            this.cboHeightUnit.Items.AddRange(new object[] {
            "Feet",
            "Inches"});
            this.cboHeightUnit.Location = new System.Drawing.Point(588, 21);
            this.cboHeightUnit.Name = "cboHeightUnit";
            this.cboHeightUnit.Size = new System.Drawing.Size(64, 23);
            this.cboHeightUnit.TabIndex = 8;
            this.ttOPD.SetToolTip(this.cboHeightUnit, "Unit for Height");
            this.cboHeightUnit.TextChanged += new System.EventHandler(this.cboHeightUnit_TextChanged);
            // 
            // cboWeightUnit
            // 
            this.cboWeightUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWeightUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboWeightUnit.FormattingEnabled = true;
            this.cboWeightUnit.Items.AddRange(new object[] {
            "Kg",
            "Pounds"});
            this.cboWeightUnit.Location = new System.Drawing.Point(408, 21);
            this.cboWeightUnit.Name = "cboWeightUnit";
            this.cboWeightUnit.Size = new System.Drawing.Size(64, 23);
            this.cboWeightUnit.TabIndex = 6;
            this.ttOPD.SetToolTip(this.cboWeightUnit, "Unit for Weight");
            this.cboWeightUnit.TextChanged += new System.EventHandler(this.cboWeightUnit_TextChanged);
            // 
            // cboTempUnit
            // 
            this.cboTempUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTempUnit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboTempUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTempUnit.FormattingEnabled = true;
            this.cboTempUnit.Items.AddRange(new object[] {
            "F",
            "C"});
            this.cboTempUnit.Location = new System.Drawing.Point(224, 1);
            this.cboTempUnit.Name = "cboTempUnit";
            this.cboTempUnit.Size = new System.Drawing.Size(64, 23);
            this.cboTempUnit.TabIndex = 1;
            this.ttOPD.SetToolTip(this.cboTempUnit, "Unit for Temprature");
            // 
            // txtHeight
            // 
            this.txtHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeight.Location = new System.Drawing.Point(528, 23);
            this.txtHeight.MaxLength = 4;
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(58, 21);
            this.txtHeight.TabIndex = 7;
            this.ttOPD.SetToolTip(this.txtHeight, "Patient  Height");
            this.txtHeight.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHeight.Leave += new System.EventHandler(this.txtWeight_Leave);
            // 
            // txtWeight
            // 
            this.txtWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(339, 23);
            this.txtWeight.MaxLength = 3;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(67, 21);
            this.txtWeight.TabIndex = 5;
            this.ttOPD.SetToolTip(this.txtWeight, "Patient Weight");
            this.txtWeight.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtWeight.Leave += new System.EventHandler(this.txtWeight_Leave);
            // 
            // txtPulse
            // 
            this.txtPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPulse.Location = new System.Drawing.Point(528, 1);
            this.txtPulse.MaxLength = 3;
            this.txtPulse.Name = "txtPulse";
            this.txtPulse.Size = new System.Drawing.Size(58, 21);
            this.txtPulse.TabIndex = 3;
            this.ttOPD.SetToolTip(this.txtPulse, "Patient Pulse");
            this.txtPulse.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPulse.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtTemp
            // 
            this.txtTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTemp.Location = new System.Drawing.Point(163, 1);
            this.txtTemp.MaxLength = 3;
            this.txtTemp.Name = "txtTemp";
            this.txtTemp.Size = new System.Drawing.Size(58, 21);
            this.txtTemp.TabIndex = 0;
            this.ttOPD.SetToolTip(this.txtTemp, "Patient Temprature");
            this.txtTemp.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtTemp.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // lblPulseUnit
            // 
            this.lblPulseUnit.AutoSize = true;
            this.lblPulseUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPulseUnit.Location = new System.Drawing.Point(585, 4);
            this.lblPulseUnit.Name = "lblPulseUnit";
            this.lblPulseUnit.Size = new System.Drawing.Size(62, 15);
            this.lblPulseUnit.TabIndex = 5;
            this.lblPulseUnit.Text = "Pulse/Min";
            // 
            // lblBPUnit
            // 
            this.lblBPUnit.AutoSize = true;
            this.lblBPUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBPUnit.Location = new System.Drawing.Point(405, 4);
            this.lblBPUnit.Name = "lblBPUnit";
            this.lblBPUnit.Size = new System.Drawing.Size(48, 15);
            this.lblBPUnit.TabIndex = 4;
            this.lblBPUnit.Text = "mm/Hg";
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(476, 26);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(49, 15);
            this.lblHeight.TabIndex = 9;
            this.lblHeight.Text = "Height :";
            // 
            // lblPulse
            // 
            this.lblPulse.AutoSize = true;
            this.lblPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPulse.Location = new System.Drawing.Point(481, 4);
            this.lblPulse.Name = "lblPulse";
            this.lblPulse.Size = new System.Drawing.Size(44, 15);
            this.lblPulse.TabIndex = 3;
            this.lblPulse.Text = "Pulse :";
            // 
            // lblBSA
            // 
            this.lblBSA.AutoSize = true;
            this.lblBSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSA.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBSA.Location = new System.Drawing.Point(843, 28);
            this.lblBSA.Name = "lblBSA";
            this.lblBSA.Size = new System.Drawing.Size(41, 15);
            this.lblBSA.TabIndex = 11;
            this.lblBSA.Tag = "NotSet";
            this.lblBSA.Text = "BSA :";
            // 
            // lblBMI
            // 
            this.lblBMI.AutoSize = true;
            this.lblBMI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMI.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMI.Location = new System.Drawing.Point(844, 6);
            this.lblBMI.Name = "lblBMI";
            this.lblBMI.Size = new System.Drawing.Size(40, 15);
            this.lblBMI.TabIndex = 10;
            this.lblBMI.Tag = "NotSet";
            this.lblBMI.Text = "BMI :";
            // 
            // lblBSAStatus
            // 
            this.lblBSAStatus.AutoSize = true;
            this.lblBSAStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBSAStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBSAStatus.Location = new System.Drawing.Point(815, 26);
            this.lblBSAStatus.Name = "lblBSAStatus";
            this.lblBSAStatus.Size = new System.Drawing.Size(85, 15);
            this.lblBSAStatus.TabIndex = 135;
            this.lblBSAStatus.Tag = "NotSet";
            this.lblBSAStatus.Text = "BSA Status :";
            this.lblBSAStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBSAStatus.Visible = false;
            // 
            // lblBMIStatus
            // 
            this.lblBMIStatus.AutoSize = true;
            this.lblBMIStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBMIStatus.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblBMIStatus.Location = new System.Drawing.Point(816, 4);
            this.lblBMIStatus.Name = "lblBMIStatus";
            this.lblBMIStatus.Size = new System.Drawing.Size(84, 15);
            this.lblBMIStatus.TabIndex = 134;
            this.lblBMIStatus.Tag = "NotSet";
            this.lblBMIStatus.Text = "BMI Status :";
            this.lblBMIStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBMIStatus.Visible = false;
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(285, 26);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(51, 15);
            this.lblWeight.TabIndex = 8;
            this.lblWeight.Text = "Weight :";
            // 
            // lblBP
            // 
            this.lblBP.AutoSize = true;
            this.lblBP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBP.Location = new System.Drawing.Point(307, 4);
            this.lblBP.Name = "lblBP";
            this.lblBP.Size = new System.Drawing.Size(29, 15);
            this.lblBP.TabIndex = 2;
            this.lblBP.Text = "BP :";
            // 
            // lblTemp
            // 
            this.lblTemp.AutoSize = true;
            this.lblTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemp.Location = new System.Drawing.Point(115, 4);
            this.lblTemp.Name = "lblTemp";
            this.lblTemp.Size = new System.Drawing.Size(45, 15);
            this.lblTemp.TabIndex = 1;
            this.lblTemp.Text = "Temp :";
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(735, 5);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(94, 20);
            this.dtpFrom.TabIndex = 5;
            this.ttOPD.SetToolTip(this.dtpFrom, "End Date for search");
            this.dtpFrom.Enter += new System.EventHandler(this.dtpTo_Enter);
            this.dtpFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchPhone_KeyPress);
            this.dtpFrom.Leave += new System.EventHandler(this.dtpTo_Leave);
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(605, 5);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(94, 20);
            this.dtpTo.TabIndex = 4;
            this.ttOPD.SetToolTip(this.dtpTo, "Start Date for Search");
            this.dtpTo.Enter += new System.EventHandler(this.dtpTo_Enter);
            this.dtpTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchPhone_KeyPress);
            this.dtpTo.Leave += new System.EventHandler(this.dtpTo_Leave);
            // 
            // txtSearchPRNO
            // 
            this.txtSearchPRNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchPRNO.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtSearchPRNO.Location = new System.Drawing.Point(66, 4);
            this.txtSearchPRNO.Name = "txtSearchPRNO";
            this.txtSearchPRNO.Size = new System.Drawing.Size(98, 20);
            this.txtSearchPRNO.TabIndex = 0;
            this.ttOPD.SetToolTip(this.txtSearchPRNO, "Remove Mask");
            this.txtSearchPRNO.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtSearchPRNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchPhone_KeyPress);
            this.txtSearchPRNO.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // txtSearchPhone
            // 
            this.txtSearchPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchPhone.Location = new System.Drawing.Point(455, 5);
            this.txtSearchPhone.Name = "txtSearchPhone";
            this.txtSearchPhone.Size = new System.Drawing.Size(87, 20);
            this.txtSearchPhone.TabIndex = 3;
            this.ttOPD.SetToolTip(this.txtSearchPhone, "Cell number or Phone Number for Search according to selection");
            this.txtSearchPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtSearchPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchPhone_KeyPress);
            this.txtSearchPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtSearchName
            // 
            this.txtSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchName.Location = new System.Drawing.Point(232, 4);
            this.txtSearchName.Name = "txtSearchName";
            this.txtSearchName.Size = new System.Drawing.Size(142, 20);
            this.txtSearchName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtSearchName, "Name for Search");
            this.txtSearchName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtSearchName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchPhone_KeyPress);
            this.txtSearchName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtSearchName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtHName
            // 
            this.txtHName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHName.Location = new System.Drawing.Point(186, 3);
            this.txtHName.MaxLength = 50;
            this.txtHName.Name = "txtHName";
            this.txtHName.Size = new System.Drawing.Size(138, 20);
            this.txtHName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtHName, "Name");
            this.txtHName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtHName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // cboHCity
            // 
            this.cboHCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHCity.FormattingEnabled = true;
            this.cboHCity.Location = new System.Drawing.Point(730, 28);
            this.cboHCity.Name = "cboHCity";
            this.cboHCity.Size = new System.Drawing.Size(98, 21);
            this.cboHCity.Sorted = true;
            this.cboHCity.TabIndex = 10;
            this.ttOPD.SetToolTip(this.cboHCity, "City");
            // 
            // dtpHFrom
            // 
            this.dtpHFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpHFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHFrom.Location = new System.Drawing.Point(608, 4);
            this.dtpHFrom.Name = "dtpHFrom";
            this.dtpHFrom.Size = new System.Drawing.Size(85, 20);
            this.dtpHFrom.TabIndex = 4;
            this.ttOPD.SetToolTip(this.dtpHFrom, "End date for Patient Date wise search");
            this.dtpHFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHSPRNO_KeyPress);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.rbHSCell);
            this.panel9.Location = new System.Drawing.Point(781, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(39, 24);
            this.panel9.TabIndex = 2;
            this.ttOPD.SetToolTip(this.panel9, "Select Phone number or cell number  for Search");
            this.panel9.Visible = false;
            // 
            // rbHSCell
            // 
            this.rbHSCell.AutoSize = true;
            this.rbHSCell.Location = new System.Drawing.Point(80, 3);
            this.rbHSCell.Name = "rbHSCell";
            this.rbHSCell.Size = new System.Drawing.Size(62, 17);
            this.rbHSCell.TabIndex = 0;
            this.rbHSCell.Text = "Cell No.";
            this.rbHSCell.UseVisualStyleBackColor = true;
            this.rbHSCell.Visible = false;
            // 
            // dtpHTo
            // 
            this.dtpHTo.CustomFormat = "dd/MM/yyyy";
            this.dtpHTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHTo.Location = new System.Drawing.Point(735, 4);
            this.dtpHTo.Name = "dtpHTo";
            this.dtpHTo.Size = new System.Drawing.Size(96, 20);
            this.dtpHTo.TabIndex = 5;
            this.ttOPD.SetToolTip(this.dtpHTo, "Start date for Patient Date wise search");
            this.dtpHTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHSPRNO_KeyPress);
            // 
            // txtHSPRNO
            // 
            this.txtHSPRNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSPRNO.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtHSPRNO.Location = new System.Drawing.Point(67, 5);
            this.txtHSPRNO.Name = "txtHSPRNO";
            this.txtHSPRNO.Size = new System.Drawing.Size(76, 20);
            this.txtHSPRNO.TabIndex = 0;
            this.ttOPD.SetToolTip(this.txtHSPRNO, "Patient PR No. for Search");
            this.txtHSPRNO.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtHSPRNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHSPRNO_KeyPress);
            this.txtHSPRNO.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // txtHSPhone
            // 
            this.txtHSPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSPhone.Location = new System.Drawing.Point(409, 4);
            this.txtHSPhone.Name = "txtHSPhone";
            this.txtHSPhone.Size = new System.Drawing.Size(87, 20);
            this.txtHSPhone.TabIndex = 3;
            this.ttOPD.SetToolTip(this.txtHSPhone, "Phone Number or Cell number for Patient Search");
            this.txtHSPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHSPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHSPRNO_KeyPress);
            this.txtHSPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtHSName
            // 
            this.txtHSName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSName.Location = new System.Drawing.Point(186, 4);
            this.txtHSName.Name = "txtHSName";
            this.txtHSName.Size = new System.Drawing.Size(138, 20);
            this.txtHSName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtHSName, "Patient Name  for Search ");
            this.txtHSName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHSName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHSPRNO_KeyPress);
            this.txtHSName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtHSName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.rbHMale);
            this.panel8.Controls.Add(this.rbHFemale);
            this.panel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(371, 1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(110, 23);
            this.panel8.TabIndex = 2;
            this.ttOPD.SetToolTip(this.panel8, "Gender");
            // 
            // rbHMale
            // 
            this.rbHMale.AutoSize = true;
            this.rbHMale.Checked = true;
            this.rbHMale.Location = new System.Drawing.Point(3, 3);
            this.rbHMale.Name = "rbHMale";
            this.rbHMale.Size = new System.Drawing.Size(48, 17);
            this.rbHMale.TabIndex = 0;
            this.rbHMale.TabStop = true;
            this.rbHMale.Text = "Male";
            this.rbHMale.UseVisualStyleBackColor = true;
            // 
            // rbHFemale
            // 
            this.rbHFemale.AutoSize = true;
            this.rbHFemale.Location = new System.Drawing.Point(51, 3);
            this.rbHFemale.Name = "rbHFemale";
            this.rbHFemale.Size = new System.Drawing.Size(59, 17);
            this.rbHFemale.TabIndex = 1;
            this.rbHFemale.Text = "Female";
            this.rbHFemale.UseVisualStyleBackColor = true;
            // 
            // txtHAddress
            // 
            this.txtHAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHAddress.Location = new System.Drawing.Point(536, 28);
            this.txtHAddress.MaxLength = 255;
            this.txtHAddress.Name = "txtHAddress";
            this.txtHAddress.Size = new System.Drawing.Size(154, 20);
            this.txtHAddress.TabIndex = 9;
            this.ttOPD.SetToolTip(this.txtHAddress, "Address");
            this.txtHAddress.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHAddress.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtHAddress.MouseHover += new System.EventHandler(this.txtPatientAddress_MouseHover);
            this.txtHAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtHCell
            // 
            this.txtHCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHCell.Location = new System.Drawing.Point(81, 28);
            this.txtHCell.MaxLength = 15;
            this.txtHCell.Name = "txtHCell";
            this.txtHCell.Size = new System.Drawing.Size(149, 20);
            this.txtHCell.TabIndex = 5;
            this.ttOPD.SetToolTip(this.txtHCell, "Cell Number");
            this.txtHCell.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHCell.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtHPhone
            // 
            this.txtHPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHPhone.Location = new System.Drawing.Point(798, 1);
            this.txtHPhone.MaxLength = 15;
            this.txtHPhone.Name = "txtHPhone";
            this.txtHPhone.Size = new System.Drawing.Size(10, 20);
            this.txtHPhone.TabIndex = 6;
            this.ttOPD.SetToolTip(this.txtHPhone, "Phone Number");
            this.txtHPhone.Visible = false;
            this.txtHPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtHAge
            // 
            this.txtHAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHAge.Location = new System.Drawing.Point(652, 3);
            this.txtHAge.MaxLength = 3;
            this.txtHAge.Name = "txtHAge";
            this.txtHAge.Size = new System.Drawing.Size(82, 20);
            this.txtHAge.TabIndex = 4;
            this.ttOPD.SetToolTip(this.txtHAge, "Age");
            this.txtHAge.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHAge.Leave += new System.EventHandler(this.txtHAge_Leave);
            // 
            // dtpHDOB
            // 
            this.dtpHDOB.CustomFormat = "dd-MM-yyyy";
            this.dtpHDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHDOB.Location = new System.Drawing.Point(536, 3);
            this.dtpHDOB.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpHDOB.Name = "dtpHDOB";
            this.dtpHDOB.Size = new System.Drawing.Size(81, 20);
            this.dtpHDOB.TabIndex = 3;
            this.ttOPD.SetToolTip(this.dtpHDOB, "Date of Birth");
            this.dtpHDOB.Leave += new System.EventHandler(this.dtpHDOB_Leave);
            // 
            // txtCName
            // 
            this.txtCName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCName.Location = new System.Drawing.Point(179, 3);
            this.txtCName.MaxLength = 50;
            this.txtCName.Name = "txtCName";
            this.txtCName.Size = new System.Drawing.Size(145, 20);
            this.txtCName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtCName, "Name");
            this.txtCName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtCName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // cboCCity
            // 
            this.cboCCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCCity.FormattingEnabled = true;
            this.cboCCity.Location = new System.Drawing.Point(730, 28);
            this.cboCCity.Name = "cboCCity";
            this.cboCCity.Size = new System.Drawing.Size(98, 21);
            this.cboCCity.TabIndex = 10;
            this.ttOPD.SetToolTip(this.cboCCity, "City");
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.rbCFemale);
            this.panel10.Controls.Add(this.rbCMale);
            this.panel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.Location = new System.Drawing.Point(371, 1);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(110, 23);
            this.panel10.TabIndex = 2;
            this.ttOPD.SetToolTip(this.panel10, "Gender");
            // 
            // rbCFemale
            // 
            this.rbCFemale.AutoSize = true;
            this.rbCFemale.Location = new System.Drawing.Point(51, 3);
            this.rbCFemale.Name = "rbCFemale";
            this.rbCFemale.Size = new System.Drawing.Size(59, 17);
            this.rbCFemale.TabIndex = 0;
            this.rbCFemale.Text = "Female";
            this.rbCFemale.UseVisualStyleBackColor = true;
            // 
            // rbCMale
            // 
            this.rbCMale.AutoSize = true;
            this.rbCMale.Checked = true;
            this.rbCMale.Location = new System.Drawing.Point(3, 3);
            this.rbCMale.Name = "rbCMale";
            this.rbCMale.Size = new System.Drawing.Size(48, 17);
            this.rbCMale.TabIndex = 0;
            this.rbCMale.TabStop = true;
            this.rbCMale.Text = "Male";
            this.rbCMale.UseVisualStyleBackColor = true;
            // 
            // txtCAddress
            // 
            this.txtCAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAddress.Location = new System.Drawing.Point(536, 28);
            this.txtCAddress.MaxLength = 255;
            this.txtCAddress.Name = "txtCAddress";
            this.txtCAddress.Size = new System.Drawing.Size(154, 20);
            this.txtCAddress.TabIndex = 9;
            this.ttOPD.SetToolTip(this.txtCAddress, "Address");
            this.txtCAddress.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCAddress.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtCAddress.MouseHover += new System.EventHandler(this.txtPatientAddress_MouseHover);
            this.txtCAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtCCell
            // 
            this.txtCCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCell.Location = new System.Drawing.Point(75, 27);
            this.txtCCell.MaxLength = 15;
            this.txtCCell.Name = "txtCCell";
            this.txtCCell.Size = new System.Drawing.Size(98, 20);
            this.txtCCell.TabIndex = 5;
            this.ttOPD.SetToolTip(this.txtCCell, "Cell number");
            this.txtCCell.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCCell.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtCPhone
            // 
            this.txtCPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPhone.Location = new System.Drawing.Point(3, 0);
            this.txtCPhone.MaxLength = 15;
            this.txtCPhone.Name = "txtCPhone";
            this.txtCPhone.Size = new System.Drawing.Size(16, 20);
            this.txtCPhone.TabIndex = 6;
            this.ttOPD.SetToolTip(this.txtCPhone, "Phone Number");
            this.txtCPhone.Visible = false;
            this.txtCPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtCAge
            // 
            this.txtCAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAge.Location = new System.Drawing.Point(652, 3);
            this.txtCAge.MaxLength = 3;
            this.txtCAge.Name = "txtCAge";
            this.txtCAge.Size = new System.Drawing.Size(73, 20);
            this.txtCAge.TabIndex = 4;
            this.ttOPD.SetToolTip(this.txtCAge, "Age");
            this.txtCAge.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCAge.Leave += new System.EventHandler(this.txtCAge_Leave);
            // 
            // dtpCDOB
            // 
            this.dtpCDOB.CustomFormat = "dd-MM-yyyy";
            this.dtpCDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCDOB.Location = new System.Drawing.Point(536, 3);
            this.dtpCDOB.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpCDOB.Name = "dtpCDOB";
            this.dtpCDOB.Size = new System.Drawing.Size(81, 20);
            this.dtpCDOB.TabIndex = 3;
            this.ttOPD.SetToolTip(this.dtpCDOB, "Patient Date of Birth");
            this.dtpCDOB.ValueChanged += new System.EventHandler(this.dtpCDOB_ValueChanged);
            this.dtpCDOB.Leave += new System.EventHandler(this.dtpCDOB_Leave);
            // 
            // dtpCTo
            // 
            this.dtpCTo.CustomFormat = "dd/MM/yyyy";
            this.dtpCTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCTo.Location = new System.Drawing.Point(735, 4);
            this.dtpCTo.Name = "dtpCTo";
            this.dtpCTo.Size = new System.Drawing.Size(96, 20);
            this.dtpCTo.TabIndex = 5;
            this.ttOPD.SetToolTip(this.dtpCTo, "End Date Search");
            this.dtpCTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCSPRNO_KeyPress);
            // 
            // dtpCFrom
            // 
            this.dtpCFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpCFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCFrom.Location = new System.Drawing.Point(608, 4);
            this.dtpCFrom.Name = "dtpCFrom";
            this.dtpCFrom.Size = new System.Drawing.Size(85, 20);
            this.dtpCFrom.TabIndex = 4;
            this.ttOPD.SetToolTip(this.dtpCFrom, "Start date for Search");
            this.dtpCFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCSPRNO_KeyPress);
            // 
            // txtCSPRNO
            // 
            this.txtCSPRNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCSPRNO.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtCSPRNO.Location = new System.Drawing.Point(62, 4);
            this.txtCSPRNO.Name = "txtCSPRNO";
            this.txtCSPRNO.Size = new System.Drawing.Size(80, 20);
            this.txtCSPRNO.TabIndex = 0;
            this.ttOPD.SetToolTip(this.txtCSPRNO, "PR No. for Search ");
            this.txtCSPRNO.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCSPRNO_MaskInputRejected);
            this.txtCSPRNO.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtCSPRNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCSPRNO_KeyPress);
            this.txtCSPRNO.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // txtCSPhone
            // 
            this.txtCSPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCSPhone.Location = new System.Drawing.Point(452, 5);
            this.txtCSPhone.Name = "txtCSPhone";
            this.txtCSPhone.Size = new System.Drawing.Size(87, 20);
            this.txtCSPhone.TabIndex = 3;
            this.ttOPD.SetToolTip(this.txtCSPhone, "Patient Phone no. / Cell No. for Search");
            this.txtCSPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCSPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCSPRNO_KeyPress);
            this.txtCSPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtCSName
            // 
            this.txtCSName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCSName.Location = new System.Drawing.Point(182, 4);
            this.txtCSName.Name = "txtCSName";
            this.txtCSName.Size = new System.Drawing.Size(145, 20);
            this.txtCSName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtCSName, "Patient Name for Search");
            this.txtCSName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCSName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCSPRNO_KeyPress);
            this.txtCSName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtCSName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtPatientAge
            // 
            this.txtPatientAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientAge.Location = new System.Drawing.Point(654, 3);
            this.txtPatientAge.MaxLength = 3;
            this.txtPatientAge.Name = "txtPatientAge";
            this.txtPatientAge.Size = new System.Drawing.Size(63, 20);
            this.txtPatientAge.TabIndex = 4;
            this.ttOPD.SetToolTip(this.txtPatientAge, "Age");
            this.txtPatientAge.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPatientAge.Leave += new System.EventHandler(this.txtPatientAge_Leave);
            // 
            // txtPatientCell
            // 
            this.txtPatientCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientCell.Location = new System.Drawing.Point(79, 27);
            this.txtPatientCell.MaxLength = 15;
            this.txtPatientCell.Name = "txtPatientCell";
            this.txtPatientCell.Size = new System.Drawing.Size(125, 20);
            this.txtPatientCell.TabIndex = 5;
            this.ttOPD.SetToolTip(this.txtPatientCell, "Cell Number");
            this.txtPatientCell.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPatientCell.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtPatientName
            // 
            this.txtPatientName.BackColor = System.Drawing.SystemColors.Window;
            this.txtPatientName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientName.Location = new System.Drawing.Point(200, 3);
            this.txtPatientName.MaxLength = 50;
            this.txtPatientName.Name = "txtPatientName";
            this.txtPatientName.Size = new System.Drawing.Size(123, 20);
            this.txtPatientName.TabIndex = 1;
            this.ttOPD.SetToolTip(this.txtPatientName, "Patient Name");
            this.txtPatientName.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPatientName.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtPatientName.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // dtpPatientDOB
            // 
            this.dtpPatientDOB.CustomFormat = "dd-MM-yyyy";
            this.dtpPatientDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPatientDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPatientDOB.Location = new System.Drawing.Point(538, 3);
            this.dtpPatientDOB.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpPatientDOB.Name = "dtpPatientDOB";
            this.dtpPatientDOB.Size = new System.Drawing.Size(81, 20);
            this.dtpPatientDOB.TabIndex = 3;
            this.ttOPD.SetToolTip(this.dtpPatientDOB, "Date of Birth");
            this.dtpPatientDOB.ValueChanged += new System.EventHandler(this.dtpPatientDOB_ValueChanged);
            this.dtpPatientDOB.Enter += new System.EventHandler(this.dtpTo_Enter);
            this.dtpPatientDOB.Leave += new System.EventHandler(this.dtpTo_Leave);
            // 
            // cboPatientCity
            // 
            this.cboPatientCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPatientCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPatientCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPatientCity.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboPatientCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPatientCity.FormattingEnabled = true;
            this.cboPatientCity.Location = new System.Drawing.Point(730, 28);
            this.cboPatientCity.Name = "cboPatientCity";
            this.cboPatientCity.Size = new System.Drawing.Size(98, 21);
            this.cboPatientCity.TabIndex = 10;
            this.ttOPD.SetToolTip(this.cboPatientCity, "City");
            // 
            // txtRPRNo
            // 
            this.txtRPRNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtRPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRPRNo.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtRPRNo.Location = new System.Drawing.Point(65, 4);
            this.txtRPRNo.Name = "txtRPRNo";
            this.txtRPRNo.Size = new System.Drawing.Size(92, 20);
            this.txtRPRNo.TabIndex = 0;
            this.txtRPRNo.TabStop = false;
            this.ttOPD.SetToolTip(this.txtRPRNo, "PR No.");
            this.txtRPRNo.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtRPRNo.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // pnlPatientGender
            // 
            this.pnlPatientGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPatientGender.Controls.Add(this.rbPatientFemale);
            this.pnlPatientGender.Controls.Add(this.rbPatientMale);
            this.pnlPatientGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPatientGender.Location = new System.Drawing.Point(383, 1);
            this.pnlPatientGender.Name = "pnlPatientGender";
            this.pnlPatientGender.Size = new System.Drawing.Size(103, 23);
            this.pnlPatientGender.TabIndex = 2;
            this.ttOPD.SetToolTip(this.pnlPatientGender, "Gender");
            // 
            // rbPatientFemale
            // 
            this.rbPatientFemale.AutoSize = true;
            this.rbPatientFemale.Location = new System.Drawing.Point(47, 3);
            this.rbPatientFemale.Name = "rbPatientFemale";
            this.rbPatientFemale.Size = new System.Drawing.Size(59, 17);
            this.rbPatientFemale.TabIndex = 1;
            this.rbPatientFemale.Text = "Female";
            this.rbPatientFemale.UseVisualStyleBackColor = true;
            // 
            // rbPatientMale
            // 
            this.rbPatientMale.AutoSize = true;
            this.rbPatientMale.Checked = true;
            this.rbPatientMale.Location = new System.Drawing.Point(3, 3);
            this.rbPatientMale.Name = "rbPatientMale";
            this.rbPatientMale.Size = new System.Drawing.Size(48, 17);
            this.rbPatientMale.TabIndex = 0;
            this.rbPatientMale.TabStop = true;
            this.rbPatientMale.Text = "Male";
            this.rbPatientMale.UseVisualStyleBackColor = true;
            // 
            // txtPatientPhone
            // 
            this.txtPatientPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientPhone.Location = new System.Drawing.Point(242, 26);
            this.txtPatientPhone.MaxLength = 15;
            this.txtPatientPhone.Name = "txtPatientPhone";
            this.txtPatientPhone.Size = new System.Drawing.Size(17, 20);
            this.txtPatientPhone.TabIndex = 6;
            this.ttOPD.SetToolTip(this.txtPatientPhone, "Phone Number");
            this.txtPatientPhone.Visible = false;
            this.txtPatientPhone.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPatientPhone.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtPatientAddress
            // 
            this.txtPatientAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientAddress.Location = new System.Drawing.Point(536, 28);
            this.txtPatientAddress.MaxLength = 255;
            this.txtPatientAddress.Name = "txtPatientAddress";
            this.txtPatientAddress.Size = new System.Drawing.Size(154, 20);
            this.txtPatientAddress.TabIndex = 9;
            this.ttOPD.SetToolTip(this.txtPatientAddress, "Address");
            this.txtPatientAddress.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtPatientAddress.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            this.txtPatientAddress.MouseHover += new System.EventHandler(this.txtPatientAddress_MouseHover);
            this.txtPatientAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // txtCPRNo
            // 
            this.txtCPRNo.BackColor = System.Drawing.Color.White;
            this.txtCPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCPRNo.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtCPRNo.Location = new System.Drawing.Point(62, 3);
            this.txtCPRNo.Name = "txtCPRNo";
            this.txtCPRNo.Size = new System.Drawing.Size(80, 20);
            this.txtCPRNo.TabIndex = 0;
            this.txtCPRNo.TabStop = false;
            this.ttOPD.SetToolTip(this.txtCPRNo, "PR No.");
            this.txtCPRNo.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtCPRNo.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // txtHPRNo
            // 
            this.txtHPRNo.BackColor = System.Drawing.Color.White;
            this.txtHPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHPRNo.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtHPRNo.Location = new System.Drawing.Point(66, 3);
            this.txtHPRNo.Name = "txtHPRNo";
            this.txtHPRNo.Size = new System.Drawing.Size(76, 20);
            this.txtHPRNo.TabIndex = 0;
            this.txtHPRNo.TabStop = false;
            this.ttOPD.SetToolTip(this.txtHPRNo, "PR No.");
            this.txtHPRNo.Enter += new System.EventHandler(this.txtSearchPRNO_Enter);
            this.txtHPRNo.Leave += new System.EventHandler(this.txtSearchPRNO_Leave);
            // 
            // txtRegEmail
            // 
            this.txtRegEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegEmail.Location = new System.Drawing.Point(323, 28);
            this.txtRegEmail.Name = "txtRegEmail";
            this.txtRegEmail.Size = new System.Drawing.Size(142, 20);
            this.txtRegEmail.TabIndex = 7;
            this.ttOPD.SetToolTip(this.txtRegEmail, "Email Address");
            this.txtRegEmail.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtRegEmail.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtHEmail
            // 
            this.txtHEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHEmail.Location = new System.Drawing.Point(332, 29);
            this.txtHEmail.Name = "txtHEmail";
            this.txtHEmail.Size = new System.Drawing.Size(138, 20);
            this.txtHEmail.TabIndex = 7;
            this.ttOPD.SetToolTip(this.txtHEmail, "Email Address");
            this.txtHEmail.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtHEmail.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // cboHPanel
            // 
            this.cboHPanel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHPanel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHPanel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHPanel.FormattingEnabled = true;
            this.cboHPanel.Location = new System.Drawing.Point(795, 2);
            this.cboHPanel.Name = "cboHPanel";
            this.cboHPanel.Size = new System.Drawing.Size(11, 21);
            this.cboHPanel.TabIndex = 8;
            this.ttOPD.SetToolTip(this.cboHPanel, "Panel");
            this.cboHPanel.Visible = false;
            this.cboHPanel.MouseHover += new System.EventHandler(this.cboPanel_MouseHover);
            // 
            // cboCPanel
            // 
            this.cboCPanel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCPanel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCPanel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCPanel.FormattingEnabled = true;
            this.cboCPanel.Location = new System.Drawing.Point(5, 1);
            this.cboCPanel.Name = "cboCPanel";
            this.cboCPanel.Size = new System.Drawing.Size(11, 21);
            this.cboCPanel.TabIndex = 8;
            this.ttOPD.SetToolTip(this.cboCPanel, "Panel");
            this.cboCPanel.Visible = false;
            this.cboCPanel.MouseHover += new System.EventHandler(this.cboPanel_MouseHover);
            // 
            // txtCEmail
            // 
            this.txtCEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCEmail.Location = new System.Drawing.Point(261, 29);
            this.txtCEmail.Name = "txtCEmail";
            this.txtCEmail.Size = new System.Drawing.Size(145, 20);
            this.txtCEmail.TabIndex = 7;
            this.ttOPD.SetToolTip(this.txtCEmail, "Email Address");
            this.txtCEmail.Enter += new System.EventHandler(this.txtVitalSign_Enter);
            this.txtCEmail.Leave += new System.EventHandler(this.txtVitalSign_Leave);
            // 
            // txtSearchByPRNo
            // 
            this.txtSearchByPRNo.BackColor = System.Drawing.Color.White;
            this.txtSearchByPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchByPRNo.Culture = new System.Globalization.CultureInfo("en-GB");
            this.txtSearchByPRNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchByPRNo.Location = new System.Drawing.Point(26, 29);
            this.txtSearchByPRNo.Name = "txtSearchByPRNo";
            this.txtSearchByPRNo.Size = new System.Drawing.Size(174, 29);
            this.txtSearchByPRNo.TabIndex = 0;
            this.ttOPD.SetToolTip(this.txtSearchByPRNo, "PR No.");
            this.txtSearchByPRNo.TextChanged += new System.EventHandler(this.txtSerachPRNo_TextChanged);
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.Location = new System.Drawing.Point(5, 32);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(20, 20);
            this.button14.TabIndex = 188;
            this.button14.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button14, "Add Mask");
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Visible = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.Location = new System.Drawing.Point(1, 29);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(26, 22);
            this.button13.TabIndex = 187;
            this.ttOPD.SetToolTip(this.button13, "Remove Mask");
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Visible = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button9
            // 
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.Location = new System.Drawing.Point(38, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(26, 22);
            this.button9.TabIndex = 185;
            this.ttOPD.SetToolTip(this.button9, "Remove Mask");
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button12
            // 
            this.button12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button12.BackgroundImage")));
            this.button12.Location = new System.Drawing.Point(44, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(20, 20);
            this.button12.TabIndex = 187;
            this.button12.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button12, "Add Mask");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Visible = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnPatientSave
            // 
            this.btnPatientSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnPatientSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPatientSave.BackgroundImage")));
            this.btnPatientSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPatientSave.FlatAppearance.BorderSize = 0;
            this.btnPatientSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.btnPatientSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnPatientSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 0.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPatientSave.ForeColor = System.Drawing.Color.Transparent;
            this.btnPatientSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPatientSave.Location = new System.Drawing.Point(830, 8);
            this.btnPatientSave.Name = "btnPatientSave";
            this.btnPatientSave.Size = new System.Drawing.Size(35, 35);
            this.btnPatientSave.TabIndex = 11;
            this.btnPatientSave.Tag = "Save";
            this.btnPatientSave.Text = "Save";
            this.btnPatientSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttOPD.SetToolTip(this.btnPatientSave, "Save or Update Patuient Information");
            this.btnPatientSave.UseVisualStyleBackColor = false;
            this.btnPatientSave.Click += new System.EventHandler(this.btnPatientSave_Click);
            // 
            // btnRegisterPatient
            // 
            this.btnRegisterPatient.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegisterPatient.BackgroundImage")));
            this.btnRegisterPatient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRegisterPatient.Location = new System.Drawing.Point(93, 2);
            this.btnRegisterPatient.Name = "btnRegisterPatient";
            this.btnRegisterPatient.Size = new System.Drawing.Size(45, 40);
            this.btnRegisterPatient.TabIndex = 0;
            this.ttOPD.SetToolTip(this.btnRegisterPatient, "Click here to search a patient  ");
            this.btnRegisterPatient.UseVisualStyleBackColor = true;
            this.btnRegisterPatient.Click += new System.EventHandler(this.btnRegisterPatient_Click);
            // 
            // btnNewPatient
            // 
            this.btnNewPatient.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewPatient.BackgroundImage")));
            this.btnNewPatient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewPatient.Location = new System.Drawing.Point(93, 45);
            this.btnNewPatient.Name = "btnNewPatient";
            this.btnNewPatient.Size = new System.Drawing.Size(45, 40);
            this.btnNewPatient.TabIndex = 1;
            this.ttOPD.SetToolTip(this.btnNewPatient, "Click here to register a patient  ");
            this.btnNewPatient.UseVisualStyleBackColor = true;
            this.btnNewPatient.Click += new System.EventHandler(this.btnNewPatient_Click);
            // 
            // button11
            // 
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.Location = new System.Drawing.Point(38, 3);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(26, 22);
            this.button11.TabIndex = 186;
            this.ttOPD.SetToolTip(this.button11, "Remove Mask");
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.Location = new System.Drawing.Point(42, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(20, 20);
            this.button5.TabIndex = 183;
            this.button5.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button5, "Add Mask");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnPatientSearch
            // 
            this.btnPatientSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPatientSearch.BackgroundImage")));
            this.btnPatientSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPatientSearch.Location = new System.Drawing.Point(842, 3);
            this.btnPatientSearch.Name = "btnPatientSearch";
            this.btnPatientSearch.Size = new System.Drawing.Size(22, 23);
            this.btnPatientSearch.TabIndex = 6;
            this.ttOPD.SetToolTip(this.btnPatientSearch, "Find Patient according to Seleted Critaria");
            this.btnPatientSearch.UseVisualStyleBackColor = true;
            this.btnPatientSearch.Click += new System.EventHandler(this.btnPatientSearch_Click);
            // 
            // btnConsultedSerach
            // 
            this.btnConsultedSerach.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConsultedSerach.BackgroundImage")));
            this.btnConsultedSerach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnConsultedSerach.Location = new System.Drawing.Point(96, 2);
            this.btnConsultedSerach.Name = "btnConsultedSerach";
            this.btnConsultedSerach.Size = new System.Drawing.Size(45, 40);
            this.btnConsultedSerach.TabIndex = 0;
            this.ttOPD.SetToolTip(this.btnConsultedSerach, "Click here to search a consulted patient  ");
            this.btnConsultedSerach.UseVisualStyleBackColor = true;
            this.btnConsultedSerach.Click += new System.EventHandler(this.btnConsultedSerach_Click);
            // 
            // button10
            // 
            this.button10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button10.BackgroundImage")));
            this.button10.Location = new System.Drawing.Point(35, 5);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(20, 20);
            this.button10.TabIndex = 186;
            this.button10.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button10, "Add Mask");
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(35, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(26, 22);
            this.button8.TabIndex = 184;
            this.ttOPD.SetToolTip(this.button8, "Remove Mask");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnCUpdate
            // 
            this.btnCUpdate.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCUpdate.BackgroundImage")));
            this.btnCUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCUpdate.FlatAppearance.BorderSize = 0;
            this.btnCUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.btnCUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnCUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F);
            this.btnCUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCUpdate.Location = new System.Drawing.Point(830, 8);
            this.btnCUpdate.Name = "btnCUpdate";
            this.btnCUpdate.Size = new System.Drawing.Size(35, 35);
            this.btnCUpdate.TabIndex = 11;
            this.btnCUpdate.Tag = "update";
            this.btnCUpdate.Text = "Update";
            this.btnCUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttOPD.SetToolTip(this.btnCUpdate, "Update Selected Patient");
            this.btnCUpdate.UseVisualStyleBackColor = false;
            this.btnCUpdate.Click += new System.EventHandler(this.btnCUpdate_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button7.BackgroundImage")));
            this.button7.Location = new System.Drawing.Point(37, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 184;
            this.button7.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button7, "Add Mask");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(35, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 22);
            this.button4.TabIndex = 184;
            this.ttOPD.SetToolTip(this.button4, "Remove Mask");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnConsultedSearch
            // 
            this.btnConsultedSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConsultedSearch.BackgroundImage")));
            this.btnConsultedSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsultedSearch.Location = new System.Drawing.Point(842, 2);
            this.btnConsultedSearch.Name = "btnConsultedSearch";
            this.btnConsultedSearch.Size = new System.Drawing.Size(23, 23);
            this.btnConsultedSearch.TabIndex = 6;
            this.ttOPD.SetToolTip(this.btnConsultedSearch, "Find Patient on Selected Critaria");
            this.btnConsultedSearch.UseVisualStyleBackColor = true;
            this.btnConsultedSearch.Click += new System.EventHandler(this.btnConsultedSearch_Click);
            // 
            // btnHoldSerach
            // 
            this.btnHoldSerach.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHoldSerach.BackgroundImage")));
            this.btnHoldSerach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnHoldSerach.Location = new System.Drawing.Point(96, 2);
            this.btnHoldSerach.Name = "btnHoldSerach";
            this.btnHoldSerach.Size = new System.Drawing.Size(45, 40);
            this.btnHoldSerach.TabIndex = 0;
            this.ttOPD.SetToolTip(this.btnHoldSerach, "Click here to search a Hold patient  ");
            this.btnHoldSerach.UseVisualStyleBackColor = true;
            this.btnHoldSerach.Click += new System.EventHandler(this.btnHoldSerach_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(38, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 22);
            this.button3.TabIndex = 185;
            this.ttOPD.SetToolTip(this.button3, "Remove Mask");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.Location = new System.Drawing.Point(41, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 26);
            this.button2.TabIndex = 183;
            this.button2.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button2, "Add Mask");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button6
            // 
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(40, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(26, 22);
            this.button6.TabIndex = 186;
            this.ttOPD.SetToolTip(this.button6, "Remove Mask");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.btnChangeExit_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.Location = new System.Drawing.Point(41, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 182;
            this.button1.Text = "System.Drawing.Bitmap";
            this.ttOPD.SetToolTip(this.button1, "Add Mask");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbHSPhone
            // 
            this.rbHSPhone.AutoCheck = false;
            this.rbHSPhone.AutoSize = true;
            this.rbHSPhone.Checked = true;
            this.rbHSPhone.Location = new System.Drawing.Point(788, 2);
            this.rbHSPhone.Name = "rbHSPhone";
            this.rbHSPhone.Size = new System.Drawing.Size(32, 17);
            this.rbHSPhone.TabIndex = 1;
            this.rbHSPhone.TabStop = true;
            this.rbHSPhone.Text = "P";
            this.rbHSPhone.UseVisualStyleBackColor = true;
            this.rbHSPhone.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.ViewPatientDocument,
            this.miDoc,
            this.miPicture,
            this.miVideo,
            this.miEmail,
            this.miSMS,
            this.Notifications,
            this.miVideoCall,
            this.toolStripSeparator4,
            this.miControlePnl,
            this.toolStripSeparator11,
            this.miHoldSave,
            this.miConsultedSave,
            this.miPrint,
            this.toolStripSeparator14,
            this.miCancel});
            this.toolStrip1.Location = new System.Drawing.Point(408, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(602, 54);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.ForeColor = System.Drawing.Color.Transparent;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 54);
            // 
            // ViewPatientDocument
            // 
            this.ViewPatientDocument.BackColor = System.Drawing.Color.LightSlateGray;
            this.ViewPatientDocument.Enabled = false;
            this.ViewPatientDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ViewPatientDocument.Image = ((System.Drawing.Image)(resources.GetObject("ViewPatientDocument.Image")));
            this.ViewPatientDocument.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ViewPatientDocument.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ViewPatientDocument.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ViewPatientDocument.Name = "ViewPatientDocument";
            this.ViewPatientDocument.Size = new System.Drawing.Size(68, 51);
            this.ViewPatientDocument.Text = "Document";
            this.ViewPatientDocument.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ViewPatientDocument.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ViewPatientDocument.ToolTipText = "View Patient Documents";
            this.ViewPatientDocument.Click += new System.EventHandler(this.ViewPatientDocument_Click);
            // 
            // miDoc
            // 
            this.miDoc.BackColor = System.Drawing.Color.LightSlateGray;
            this.miDoc.Enabled = false;
            this.miDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.miDoc.Image = ((System.Drawing.Image)(resources.GetObject("miDoc.Image")));
            this.miDoc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miDoc.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miDoc.Name = "miDoc";
            this.miDoc.Size = new System.Drawing.Size(68, 51);
            this.miDoc.Text = "Document";
            this.miDoc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miDoc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.miDoc.Click += new System.EventHandler(this.miDoc_Click);
            // 
            // miPicture
            // 
            this.miPicture.BackColor = System.Drawing.Color.LightSlateGray;
            this.miPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miPicture.Enabled = false;
            this.miPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miPicture.ForeColor = System.Drawing.Color.Black;
            this.miPicture.Image = ((System.Drawing.Image)(resources.GetObject("miPicture.Image")));
            this.miPicture.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miPicture.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miPicture.Name = "miPicture";
            this.miPicture.Size = new System.Drawing.Size(51, 51);
            this.miPicture.Text = "Images";
            this.miPicture.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miPicture.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miPicture.ToolTipText = "Patient Picture Collection";
            this.miPicture.Click += new System.EventHandler(this.miPicture_Click);
            // 
            // miVideo
            // 
            this.miVideo.BackColor = System.Drawing.Color.LightSlateGray;
            this.miVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miVideo.Enabled = false;
            this.miVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miVideo.ForeColor = System.Drawing.Color.Black;
            this.miVideo.Image = ((System.Drawing.Image)(resources.GetObject("miVideo.Image")));
            this.miVideo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miVideo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miVideo.Name = "miVideo";
            this.miVideo.Size = new System.Drawing.Size(43, 51);
            this.miVideo.Text = "&Video";
            this.miVideo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miVideo.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miVideo.ToolTipText = "Collection of Videos for Selected  Patient";
            this.miVideo.Click += new System.EventHandler(this.miVideo_Click);
            // 
            // miEmail
            // 
            this.miEmail.BackColor = System.Drawing.Color.LightSlateGray;
            this.miEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miEmail.Enabled = false;
            this.miEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miEmail.ForeColor = System.Drawing.Color.Black;
            this.miEmail.Image = ((System.Drawing.Image)(resources.GetObject("miEmail.Image")));
            this.miEmail.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miEmail.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miEmail.Name = "miEmail";
            this.miEmail.Size = new System.Drawing.Size(45, 51);
            this.miEmail.Text = "E&mail ";
            this.miEmail.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miEmail.Click += new System.EventHandler(this.miEmail_Click);
            // 
            // miSMS
            // 
            this.miSMS.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSMS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSMS.Enabled = false;
            this.miSMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miSMS.ForeColor = System.Drawing.Color.Black;
            this.miSMS.Image = ((System.Drawing.Image)(resources.GetObject("miSMS.Image")));
            this.miSMS.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSMS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSMS.Name = "miSMS";
            this.miSMS.Size = new System.Drawing.Size(39, 51);
            this.miSMS.Text = "&SMS";
            this.miSMS.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSMS.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSMS.Click += new System.EventHandler(this.miSMS_Click);
            // 
            // Notifications
            // 
            this.Notifications.BackColor = System.Drawing.Color.LightSlateGray;
            this.Notifications.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Notifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Notifications.ForeColor = System.Drawing.Color.Black;
            this.Notifications.Image = global::OPD.Properties.Resources.Notification;
            this.Notifications.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Notifications.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Notifications.Name = "Notifications";
            this.Notifications.Size = new System.Drawing.Size(101, 51);
            this.Notifications.Text = "Notifications (0)";
            this.Notifications.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Notifications.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.Notifications.Click += new System.EventHandler(this.Notifications_Click);
            // 
            // miVideoCall
            // 
            this.miVideoCall.BackColor = System.Drawing.Color.LightSlateGray;
            this.miVideoCall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miVideoCall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miVideoCall.ForeColor = System.Drawing.Color.Black;
            this.miVideoCall.Image = ((System.Drawing.Image)(resources.GetObject("miVideoCall.Image")));
            this.miVideoCall.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miVideoCall.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miVideoCall.Name = "miVideoCall";
            this.miVideoCall.Size = new System.Drawing.Size(64, 51);
            this.miVideoCall.Text = "&VideoCall";
            this.miVideoCall.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miVideoCall.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miVideoCall.Click += new System.EventHandler(this.miVideoCall_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 54);
            // 
            // miControlePnl
            // 
            this.miControlePnl.BackColor = System.Drawing.Color.LightSlateGray;
            this.miControlePnl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miControlePnl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOption,
            this.SynchPatients,
            this.SendPatientDetailToServer,
            this.miNewAccount,
            this.miInstructions,
            this.miChangePassword,
            this.miPanelInfo,
            this.dataAnalysisToolStripMenuItem,
            this.miDosageInstruction,
            this.backupDataToolStripMenuItem,
            this.restoreDatabaseToolStripMenuItem,
            this.tsmiMonthlySummary,
            this.miAddDisease});
            this.miControlePnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miControlePnl.ForeColor = System.Drawing.Color.Black;
            this.miControlePnl.Image = ((System.Drawing.Image)(resources.GetObject("miControlePnl.Image")));
            this.miControlePnl.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miControlePnl.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miControlePnl.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.miControlePnl.Name = "miControlePnl";
            this.miControlePnl.Size = new System.Drawing.Size(96, 51);
            this.miControlePnl.Text = "Control Panel";
            this.miControlePnl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miControlePnl.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miControlePnl.Click += new System.EventHandler(this.miControlePnl_Click);
            // 
            // miOption
            // 
            this.miOption.BackColor = System.Drawing.Color.LightSlateGray;
            this.miOption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miOption.ForeColor = System.Drawing.Color.Black;
            this.miOption.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miOption.Name = "miOption";
            this.miOption.Size = new System.Drawing.Size(273, 46);
            this.miOption.Text = "Setting";
            this.miOption.Click += new System.EventHandler(this.miOption_Click);
            // 
            // SynchPatients
            // 
            this.SynchPatients.BackColor = System.Drawing.Color.LightSlateGray;
            this.SynchPatients.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SynchPatients.ForeColor = System.Drawing.Color.Black;
            this.SynchPatients.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SynchPatients.Name = "SynchPatients";
            this.SynchPatients.Size = new System.Drawing.Size(273, 46);
            this.SynchPatients.Text = "Save Patient to Server";
            this.SynchPatients.Click += new System.EventHandler(this.SynchPatients_Click);
            // 
            // SendPatientDetailToServer
            // 
            this.SendPatientDetailToServer.BackColor = System.Drawing.Color.LightSlateGray;
            this.SendPatientDetailToServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SendPatientDetailToServer.ForeColor = System.Drawing.Color.Black;
            this.SendPatientDetailToServer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SendPatientDetailToServer.Name = "SendPatientDetailToServer";
            this.SendPatientDetailToServer.Size = new System.Drawing.Size(273, 46);
            this.SendPatientDetailToServer.Text = "Send Patient Details To Server";
            this.SendPatientDetailToServer.Click += new System.EventHandler(this.SendPatientDetailToServer_Click);
            // 
            // miNewAccount
            // 
            this.miNewAccount.BackColor = System.Drawing.Color.LightSlateGray;
            this.miNewAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miNewAccount.Enabled = false;
            this.miNewAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miNewAccount.ForeColor = System.Drawing.Color.Black;
            this.miNewAccount.Image = ((System.Drawing.Image)(resources.GetObject("miNewAccount.Image")));
            this.miNewAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miNewAccount.Name = "miNewAccount";
            this.miNewAccount.Size = new System.Drawing.Size(273, 46);
            this.miNewAccount.Text = "&New User";
            this.miNewAccount.Click += new System.EventHandler(this.miNewAccount_Click);
            // 
            // miInstructions
            // 
            this.miInstructions.BackColor = System.Drawing.Color.LightSlateGray;
            this.miInstructions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miInstructions.ForeColor = System.Drawing.Color.Black;
            this.miInstructions.Image = ((System.Drawing.Image)(resources.GetObject("miInstructions.Image")));
            this.miInstructions.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miInstructions.Name = "miInstructions";
            this.miInstructions.Size = new System.Drawing.Size(273, 46);
            this.miInstructions.Text = "Ins&tructions";
            this.miInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.miInstructions.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miInstructions.Click += new System.EventHandler(this.miInstructions_Click);
            // 
            // miChangePassword
            // 
            this.miChangePassword.BackColor = System.Drawing.Color.LightSlateGray;
            this.miChangePassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miChangePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miChangePassword.ForeColor = System.Drawing.Color.Black;
            this.miChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("miChangePassword.Image")));
            this.miChangePassword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miChangePassword.Name = "miChangePassword";
            this.miChangePassword.Size = new System.Drawing.Size(273, 46);
            this.miChangePassword.Text = "Change Pass&word";
            this.miChangePassword.Click += new System.EventHandler(this.miChangePassword_Click);
            // 
            // miPanelInfo
            // 
            this.miPanelInfo.BackColor = System.Drawing.Color.LightSlateGray;
            this.miPanelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miPanelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miPanelInfo.ForeColor = System.Drawing.Color.Black;
            this.miPanelInfo.Image = ((System.Drawing.Image)(resources.GetObject("miPanelInfo.Image")));
            this.miPanelInfo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miPanelInfo.Name = "miPanelInfo";
            this.miPanelInfo.Size = new System.Drawing.Size(273, 46);
            this.miPanelInfo.Text = "Panel";
            this.miPanelInfo.Click += new System.EventHandler(this.miPanelInfo_Click);
            // 
            // dataAnalysisToolStripMenuItem
            // 
            this.dataAnalysisToolStripMenuItem.BackColor = System.Drawing.Color.LightSlateGray;
            this.dataAnalysisToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dataAnalysisToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAnalysisToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.dataAnalysisToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dataAnalysisToolStripMenuItem.Image")));
            this.dataAnalysisToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.dataAnalysisToolStripMenuItem.Name = "dataAnalysisToolStripMenuItem";
            this.dataAnalysisToolStripMenuItem.Size = new System.Drawing.Size(273, 46);
            this.dataAnalysisToolStripMenuItem.Text = "Data Analysis";
            this.dataAnalysisToolStripMenuItem.Click += new System.EventHandler(this.dataAnalysisToolStripMenuItem_Click);
            // 
            // miDosageInstruction
            // 
            this.miDosageInstruction.BackColor = System.Drawing.Color.LightSlateGray;
            this.miDosageInstruction.Image = ((System.Drawing.Image)(resources.GetObject("miDosageInstruction.Image")));
            this.miDosageInstruction.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miDosageInstruction.Name = "miDosageInstruction";
            this.miDosageInstruction.Size = new System.Drawing.Size(273, 46);
            this.miDosageInstruction.Text = "Dosage Instruction";
            this.miDosageInstruction.Click += new System.EventHandler(this.miDosageInstruction_Click);
            // 
            // backupDataToolStripMenuItem
            // 
            this.backupDataToolStripMenuItem.BackColor = System.Drawing.Color.LightSlateGray;
            this.backupDataToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("backupDataToolStripMenuItem.Image")));
            this.backupDataToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.backupDataToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.backupDataToolStripMenuItem.Name = "backupDataToolStripMenuItem";
            this.backupDataToolStripMenuItem.Size = new System.Drawing.Size(273, 46);
            this.backupDataToolStripMenuItem.Text = "Database Backup";
            this.backupDataToolStripMenuItem.Click += new System.EventHandler(this.backupDataToolStripMenuItem_Click);
            // 
            // restoreDatabaseToolStripMenuItem
            // 
            this.restoreDatabaseToolStripMenuItem.BackColor = System.Drawing.Color.LightSlateGray;
            this.restoreDatabaseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("restoreDatabaseToolStripMenuItem.Image")));
            this.restoreDatabaseToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.restoreDatabaseToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.restoreDatabaseToolStripMenuItem.Name = "restoreDatabaseToolStripMenuItem";
            this.restoreDatabaseToolStripMenuItem.Size = new System.Drawing.Size(273, 46);
            this.restoreDatabaseToolStripMenuItem.Text = "Restore Database";
            this.restoreDatabaseToolStripMenuItem.Click += new System.EventHandler(this.restoreDatabaseToolStripMenuItem_Click);
            // 
            // tsmiMonthlySummary
            // 
            this.tsmiMonthlySummary.BackColor = System.Drawing.Color.LightSlateGray;
            this.tsmiMonthlySummary.Name = "tsmiMonthlySummary";
            this.tsmiMonthlySummary.Size = new System.Drawing.Size(273, 46);
            this.tsmiMonthlySummary.Text = "Monthly Summary";
            this.tsmiMonthlySummary.Click += new System.EventHandler(this.tsmiMonthlySummary_Click);
            // 
            // miAddDisease
            // 
            this.miAddDisease.BackColor = System.Drawing.Color.LightSlateGray;
            this.miAddDisease.Name = "miAddDisease";
            this.miAddDisease.Size = new System.Drawing.Size(273, 46);
            this.miAddDisease.Text = "ICD-10";
            this.miAddDisease.Click += new System.EventHandler(this.miAddDisease_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 54);
            // 
            // miHoldSave
            // 
            this.miHoldSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miHoldSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miHoldSave.Enabled = false;
            this.miHoldSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miHoldSave.ForeColor = System.Drawing.Color.Black;
            this.miHoldSave.Image = ((System.Drawing.Image)(resources.GetObject("miHoldSave.Image")));
            this.miHoldSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miHoldSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miHoldSave.Name = "miHoldSave";
            this.miHoldSave.Size = new System.Drawing.Size(39, 51);
            this.miHoldSave.Text = "&Hold";
            this.miHoldSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miHoldSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miHoldSave.ToolTipText = "To hold Patient for some reason";
            this.miHoldSave.Click += new System.EventHandler(this.miHoldSave_Click);
            // 
            // miConsultedSave
            // 
            this.miConsultedSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miConsultedSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miConsultedSave.Enabled = false;
            this.miConsultedSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miConsultedSave.ForeColor = System.Drawing.Color.Black;
            this.miConsultedSave.Image = ((System.Drawing.Image)(resources.GetObject("miConsultedSave.Image")));
            this.miConsultedSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miConsultedSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miConsultedSave.Name = "miConsultedSave";
            this.miConsultedSave.Size = new System.Drawing.Size(67, 51);
            this.miConsultedSave.Text = "&Consulted";
            this.miConsultedSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miConsultedSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miConsultedSave.ToolTipText = "To Add Patient in Consulted List";
            this.miConsultedSave.Click += new System.EventHandler(this.miConsultedSave_Click);
            // 
            // miPrint
            // 
            this.miPrint.BackColor = System.Drawing.Color.LightSlateGray;
            this.miPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miPrint.Enabled = false;
            this.miPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miPrint.ForeColor = System.Drawing.Color.Black;
            this.miPrint.Image = ((System.Drawing.Image)(resources.GetObject("miPrint.Image")));
            this.miPrint.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miPrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miPrint.Name = "miPrint";
            this.miPrint.Size = new System.Drawing.Size(39, 51);
            this.miPrint.Text = "&Print";
            this.miPrint.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miPrint.ToolTipText = "Print Patient Perscription";
            this.miPrint.Click += new System.EventHandler(this.miPrint_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 54);
            // 
            // miCancel
            // 
            this.miCancel.BackColor = System.Drawing.Color.LightSlateGray;
            this.miCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miCancel.ForeColor = System.Drawing.Color.White;
            this.miCancel.Image = ((System.Drawing.Image)(resources.GetObject("miCancel.Image")));
            this.miCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miCancel.ImageTransparentColor = System.Drawing.Color.AliceBlue;
            this.miCancel.Name = "miCancel";
            this.miCancel.Size = new System.Drawing.Size(44, 51);
            this.miCancel.Text = "&Logout";
            this.miCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miCancel.ToolTipText = "Log Out ";
            this.miCancel.Click += new System.EventHandler(this.miCancel_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.llblDailyCashReport);
            this.panel1.Controls.Add(this.llblSearchByPRNO);
            this.panel1.Controls.Add(this.lkbPayRep);
            this.panel1.Controls.Add(this.lblMode);
            this.panel1.Controls.Add(this.lklTdaySummary);
            this.panel1.Controls.Add(this.lblTotalAmount);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lblTotalPatient);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lblSelect);
            this.panel1.Location = new System.Drawing.Point(2, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 20);
            this.panel1.TabIndex = 134;
            this.panel1.Tag = "med";
            // 
            // llblDailyCashReport
            // 
            this.llblDailyCashReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.llblDailyCashReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llblDailyCashReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llblDailyCashReport.Location = new System.Drawing.Point(538, -1);
            this.llblDailyCashReport.Name = "llblDailyCashReport";
            this.llblDailyCashReport.Size = new System.Drawing.Size(124, 20);
            this.llblDailyCashReport.TabIndex = 11;
            this.llblDailyCashReport.TabStop = true;
            this.llblDailyCashReport.Text = "Daily Cash Report";
            this.llblDailyCashReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.llblDailyCashReport.Click += new System.EventHandler(this.llblDailyCashReport_Click);
            // 
            // llblSearchByPRNO
            // 
            this.llblSearchByPRNO.AutoSize = true;
            this.llblSearchByPRNO.Location = new System.Drawing.Point(453, 2);
            this.llblSearchByPRNO.Name = "llblSearchByPRNO";
            this.llblSearchByPRNO.Size = new System.Drawing.Size(77, 13);
            this.llblSearchByPRNO.TabIndex = 9;
            this.llblSearchByPRNO.TabStop = true;
            this.llblSearchByPRNO.Text = "Search Patient";
            this.llblSearchByPRNO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llblSearchByPRNO.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblSearchByPRNO_LinkClicked);
            // 
            // lkbPayRep
            // 
            this.lkbPayRep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lkbPayRep.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lkbPayRep.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkbPayRep.Image = ((System.Drawing.Image)(resources.GetObject("lkbPayRep.Image")));
            this.lkbPayRep.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lkbPayRep.LinkColor = System.Drawing.Color.Blue;
            this.lkbPayRep.Location = new System.Drawing.Point(172, -1);
            this.lkbPayRep.Name = "lkbPayRep";
            this.lkbPayRep.Size = new System.Drawing.Size(139, 20);
            this.lkbPayRep.TabIndex = 8;
            this.lkbPayRep.TabStop = true;
            this.lkbPayRep.Text = "Payment Slip";
            this.lkbPayRep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lkbPayRep.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbPayRep_LinkClicked);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMode.ForeColor = System.Drawing.Color.Black;
            this.lblMode.Location = new System.Drawing.Point(315, 2);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(137, 15);
            this.lblMode.TabIndex = 7;
            this.lblMode.Tag = "transdisplay";
            this.lblMode.Text = "Mode :  Registration";
            // 
            // lklTdaySummary
            // 
            this.lklTdaySummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lklTdaySummary.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lklTdaySummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lklTdaySummary.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lklTdaySummary.LinkColor = System.Drawing.Color.Blue;
            this.lklTdaySummary.Location = new System.Drawing.Point(6, -1);
            this.lklTdaySummary.Name = "lklTdaySummary";
            this.lklTdaySummary.Size = new System.Drawing.Size(139, 20);
            this.lklTdaySummary.TabIndex = 6;
            this.lklTdaySummary.TabStop = true;
            this.lklTdaySummary.Text = "Waiting Queue";
            this.lklTdaySummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lklTdaySummary.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklTdaySummary_LinkClicked);
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.Location = new System.Drawing.Point(945, 3);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(0, 13);
            this.lblTotalAmount.TabIndex = 3;
            this.lblTotalAmount.Tag = "display";
            this.lblTotalAmount.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(789, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 2;
            this.label7.Tag = "display";
            this.label7.Text = "Total Amount : ";
            this.label7.Visible = false;
            // 
            // lblTotalPatient
            // 
            this.lblTotalPatient.AutoSize = true;
            this.lblTotalPatient.Location = new System.Drawing.Point(790, 4);
            this.lblTotalPatient.Name = "lblTotalPatient";
            this.lblTotalPatient.Size = new System.Drawing.Size(0, 13);
            this.lblTotalPatient.TabIndex = 1;
            this.lblTotalPatient.Tag = "display";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(670, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 0;
            this.label5.Tag = "display";
            this.label5.Text = "Total Patient : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(906, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 4;
            this.label10.Tag = "display";
            this.label10.Text = "Rs.  ";
            this.label10.Visible = false;
            // 
            // lblSelect
            // 
            this.lblSelect.AutoSize = true;
            this.lblSelect.Location = new System.Drawing.Point(253, 2);
            this.lblSelect.Name = "lblSelect";
            this.lblSelect.Size = new System.Drawing.Size(13, 13);
            this.lblSelect.TabIndex = 6;
            this.lblSelect.Tag = "1";
            this.lblSelect.Text = "0";
            this.lblSelect.Visible = false;
            // 
            // llblRefershPatient
            // 
            this.llblRefershPatient.AutoSize = true;
            this.llblRefershPatient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.llblRefershPatient.Location = new System.Drawing.Point(147, 92);
            this.llblRefershPatient.Name = "llblRefershPatient";
            this.llblRefershPatient.Size = new System.Drawing.Size(51, 13);
            this.llblRefershPatient.TabIndex = 9;
            this.llblRefershPatient.TabStop = true;
            this.llblRefershPatient.Text = "Refresh";
            this.llblRefershPatient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llblRefershPatient.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblRefershPatient_LinkClicked);
            // 
            // pnlRegister
            // 
            this.pnlRegister.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRegister.Controls.Add(this.llblRefershPatient);
            this.pnlRegister.Controls.Add(this.pnlLegend);
            this.pnlRegister.Controls.Add(this.lblRegCount);
            this.pnlRegister.Controls.Add(this.pnlRPatient);
            this.pnlRegister.Controls.Add(this.btnRegisterPatient);
            this.pnlRegister.Controls.Add(this.btnNewPatient);
            this.pnlRegister.Controls.Add(this.dgRegisterPatient);
            this.pnlRegister.Controls.Add(this.pnlRegisterSearch);
            this.pnlRegister.Controls.Add(this.pbPatient);
            this.pnlRegister.Location = new System.Drawing.Point(2, 77);
            this.pnlRegister.Name = "pnlRegister";
            this.pnlRegister.Size = new System.Drawing.Size(1012, 88);
            this.pnlRegister.TabIndex = 0;
            this.pnlRegister.Tag = "";
            // 
            // pnlLegend
            // 
            this.pnlLegend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLegend.Controls.Add(this.label9);
            this.pnlLegend.Controls.Add(this.label13);
            this.pnlLegend.Controls.Add(this.label6);
            this.pnlLegend.Controls.Add(this.label12);
            this.pnlLegend.Controls.Add(this.label8);
            this.pnlLegend.Controls.Add(this.label11);
            this.pnlLegend.Location = new System.Drawing.Point(686, 86);
            this.pnlLegend.Name = "pnlLegend";
            this.pnlLegend.Size = new System.Drawing.Size(321, 19);
            this.pnlLegend.TabIndex = 151;
            this.pnlLegend.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Lavender;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(272, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 15);
            this.label9.TabIndex = 152;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(228, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 151;
            this.label13.Text = "Wait :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 146;
            this.label6.Text = "Consulted : ";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Aquamarine;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Location = new System.Drawing.Point(191, 1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 15);
            this.label12.TabIndex = 150;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(147, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 147;
            this.label8.Text = "Hold : ";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.LightBlue;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Location = new System.Drawing.Point(95, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 15);
            this.label11.TabIndex = 149;
            this.label11.Text = "     ";
            // 
            // lblRegCount
            // 
            this.lblRegCount.AutoSize = true;
            this.lblRegCount.Location = new System.Drawing.Point(3, 91);
            this.lblRegCount.Name = "lblRegCount";
            this.lblRegCount.Size = new System.Drawing.Size(0, 13);
            this.lblRegCount.TabIndex = 152;
            this.lblRegCount.Tag = "display";
            // 
            // pnlRPatient
            // 
            this.pnlRPatient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRPatient.Controls.Add(this.button9);
            this.pnlRPatient.Controls.Add(this.button12);
            this.pnlRPatient.Controls.Add(this.pictureBox2);
            this.pnlRPatient.Controls.Add(this.panel15);
            this.pnlRPatient.Controls.Add(this.panel14);
            this.pnlRPatient.Controls.Add(this.panel2);
            this.pnlRPatient.Controls.Add(this.txtPatientName);
            this.pnlRPatient.Controls.Add(this.txtRegEmail);
            this.pnlRPatient.Controls.Add(this.txtPatientPhone);
            this.pnlRPatient.Controls.Add(this.pnlPatientGender);
            this.pnlRPatient.Controls.Add(this.txtRPRNo);
            this.pnlRPatient.Controls.Add(this.txtPatientAddress);
            this.pnlRPatient.Controls.Add(this.btnPatientSave);
            this.pnlRPatient.Controls.Add(this.cboPatientCity);
            this.pnlRPatient.Controls.Add(this.txtPatientCell);
            this.pnlRPatient.Controls.Add(this.dtpPatientDOB);
            this.pnlRPatient.Controls.Add(this.txtPatientAge);
            this.pnlRPatient.Controls.Add(this.lblPatientName);
            this.pnlRPatient.Controls.Add(this.lblPatientCity);
            this.pnlRPatient.Controls.Add(this.lblPatientAddress);
            this.pnlRPatient.Controls.Add(this.lblRPRNo);
            this.pnlRPatient.Controls.Add(this.lblPatientPhone);
            this.pnlRPatient.Controls.Add(this.lblRDOB);
            this.pnlRPatient.Controls.Add(this.lblRegEmail);
            this.pnlRPatient.Controls.Add(this.lblPatientAge);
            this.pnlRPatient.Controls.Add(this.lblPatientCell);
            this.pnlRPatient.Controls.Add(this.lblPatientGender);
            this.pnlRPatient.Location = new System.Drawing.Point(140, 32);
            this.pnlRPatient.Name = "pnlRPatient";
            this.pnlRPatient.Size = new System.Drawing.Size(867, 53);
            this.pnlRPatient.TabIndex = 2;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(208, 26);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 21);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Red;
            this.panel15.Location = new System.Drawing.Point(716, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(4, 20);
            this.panel15.TabIndex = 158;
            this.panel15.Tag = "Self";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Red;
            this.panel14.Location = new System.Drawing.Point(486, 1);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(4, 23);
            this.panel14.TabIndex = 157;
            this.panel14.Tag = "Self";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(322, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 20);
            this.panel2.TabIndex = 144;
            this.panel2.Tag = "Self";
            // 
            // lblPatientName
            // 
            this.lblPatientName.AutoSize = true;
            this.lblPatientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientName.Location = new System.Drawing.Point(155, 7);
            this.lblPatientName.Name = "lblPatientName";
            this.lblPatientName.Size = new System.Drawing.Size(44, 13);
            this.lblPatientName.TabIndex = 140;
            this.lblPatientName.Text = "Name : ";
            // 
            // lblPatientCity
            // 
            this.lblPatientCity.AutoSize = true;
            this.lblPatientCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientCity.Location = new System.Drawing.Point(700, 31);
            this.lblPatientCity.Name = "lblPatientCity";
            this.lblPatientCity.Size = new System.Drawing.Size(30, 13);
            this.lblPatientCity.TabIndex = 142;
            this.lblPatientCity.Text = "City :";
            // 
            // lblPatientAddress
            // 
            this.lblPatientAddress.AutoSize = true;
            this.lblPatientAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientAddress.Location = new System.Drawing.Point(483, 31);
            this.lblPatientAddress.Name = "lblPatientAddress";
            this.lblPatientAddress.Size = new System.Drawing.Size(51, 13);
            this.lblPatientAddress.TabIndex = 134;
            this.lblPatientAddress.Text = "Address :";
            // 
            // lblRPRNo
            // 
            this.lblRPRNo.AutoSize = true;
            this.lblRPRNo.Location = new System.Drawing.Point(-3, 8);
            this.lblRPRNo.Name = "lblRPRNo";
            this.lblRPRNo.Size = new System.Drawing.Size(53, 13);
            this.lblRPRNo.TabIndex = 145;
            this.lblRPRNo.Text = "MR No. : ";
            // 
            // lblPatientPhone
            // 
            this.lblPatientPhone.AutoSize = true;
            this.lblPatientPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientPhone.Location = new System.Drawing.Point(266, 30);
            this.lblPatientPhone.Name = "lblPatientPhone";
            this.lblPatientPhone.Size = new System.Drawing.Size(14, 13);
            this.lblPatientPhone.TabIndex = 136;
            this.lblPatientPhone.Text = "P";
            this.lblPatientPhone.Visible = false;
            this.lblPatientPhone.Click += new System.EventHandler(this.lblPatientPhone_Click);
            // 
            // lblRDOB
            // 
            this.lblRDOB.AutoSize = true;
            this.lblRDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRDOB.Location = new System.Drawing.Point(500, 7);
            this.lblRDOB.Name = "lblRDOB";
            this.lblRDOB.Size = new System.Drawing.Size(36, 13);
            this.lblRDOB.TabIndex = 139;
            this.lblRDOB.Text = "DOB :";
            // 
            // lblRegEmail
            // 
            this.lblRegEmail.AutoSize = true;
            this.lblRegEmail.Location = new System.Drawing.Point(271, 31);
            this.lblRegEmail.Name = "lblRegEmail";
            this.lblRegEmail.Size = new System.Drawing.Size(41, 13);
            this.lblRegEmail.TabIndex = 146;
            this.lblRegEmail.Text = "Email : ";
            // 
            // lblPatientAge
            // 
            this.lblPatientAge.AutoSize = true;
            this.lblPatientAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientAge.Location = new System.Drawing.Point(619, 7);
            this.lblPatientAge.Name = "lblPatientAge";
            this.lblPatientAge.Size = new System.Drawing.Size(32, 13);
            this.lblPatientAge.TabIndex = 138;
            this.lblPatientAge.Text = "Age :";
            // 
            // lblPatientCell
            // 
            this.lblPatientCell.AutoSize = true;
            this.lblPatientCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientCell.Location = new System.Drawing.Point(-1, 30);
            this.lblPatientCell.Name = "lblPatientCell";
            this.lblPatientCell.Size = new System.Drawing.Size(76, 13);
            this.lblPatientCell.TabIndex = 3;
            this.lblPatientCell.Text = "Whatsapp No.";
            // 
            // lblPatientGender
            // 
            this.lblPatientGender.AutoSize = true;
            this.lblPatientGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientGender.Location = new System.Drawing.Point(331, 7);
            this.lblPatientGender.Name = "lblPatientGender";
            this.lblPatientGender.Size = new System.Drawing.Size(48, 13);
            this.lblPatientGender.TabIndex = 137;
            this.lblPatientGender.Text = "Gender :";
            // 
            // dgRegisterPatient
            // 
            this.dgRegisterPatient.AllowUserToAddRows = false;
            this.dgRegisterPatient.AllowUserToDeleteRows = false;
            this.dgRegisterPatient.AllowUserToResizeRows = false;
            this.dgRegisterPatient.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRegisterPatient.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgRegisterPatient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgRegisterPatient.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRegisterPatient.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.dgRegisterPatient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgRegisterPatient.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientID,
            this.RSNO,
            this.RPanel,
            this.PEmail,
            this.Status,
            this.PRNO,
            this.ROPDID,
            this.EnteredBy,
            this.PatientName,
            this.Gender,
            this.DOB,
            this.RAge,
            this.PhoneNo,
            this.CellNo,
            this.Address,
            this.cityname});
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle116.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle116.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle116.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle116.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle116.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgRegisterPatient.DefaultCellStyle = dataGridViewCellStyle116;
            this.dgRegisterPatient.Enabled = false;
            this.dgRegisterPatient.EnableHeadersVisualStyles = false;
            this.dgRegisterPatient.Location = new System.Drawing.Point(2, 105);
            this.dgRegisterPatient.MultiSelect = false;
            this.dgRegisterPatient.Name = "dgRegisterPatient";
            this.dgRegisterPatient.ReadOnly = true;
            dataGridViewCellStyle117.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle117.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle117.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle117.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle117.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRegisterPatient.RowHeadersDefaultCellStyle = dataGridViewCellStyle117;
            this.dgRegisterPatient.RowHeadersWidth = 30;
            this.dgRegisterPatient.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgRegisterPatient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRegisterPatient.ShowRowErrors = false;
            this.dgRegisterPatient.Size = new System.Drawing.Size(1007, 514);
            this.dgRegisterPatient.TabIndex = 143;
            this.dgRegisterPatient.TabStop = false;
            this.dgRegisterPatient.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRegisterPatient_CellDoubleClick);
            this.dgRegisterPatient.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgRegisterPatient_ColumnHeaderMouseClick);
            // 
            // PatientID
            // 
            this.PatientID.DataPropertyName = "PatientID";
            this.PatientID.HeaderText = "Patient ID";
            this.PatientID.Name = "PatientID";
            this.PatientID.ReadOnly = true;
            this.PatientID.Visible = false;
            // 
            // RSNO
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RSNO.DefaultCellStyle = dataGridViewCellStyle45;
            this.RSNO.FillWeight = 5F;
            this.RSNO.HeaderText = "S No.";
            this.RSNO.Name = "RSNO";
            this.RSNO.ReadOnly = true;
            // 
            // RPanel
            // 
            this.RPanel.DataPropertyName = "PanelName";
            this.RPanel.HeaderText = "Panel";
            this.RPanel.Name = "RPanel";
            this.RPanel.ReadOnly = true;
            this.RPanel.Visible = false;
            // 
            // PEmail
            // 
            this.PEmail.DataPropertyName = "Email";
            this.PEmail.HeaderText = "Email";
            this.PEmail.Name = "PEmail";
            this.PEmail.ReadOnly = true;
            this.PEmail.Visible = false;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // PRNO
            // 
            this.PRNO.DataPropertyName = "PRNO";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PRNO.DefaultCellStyle = dataGridViewCellStyle46;
            this.PRNO.FillWeight = 10F;
            this.PRNO.HeaderText = "PR No.";
            this.PRNO.Name = "PRNO";
            this.PRNO.ReadOnly = true;
            // 
            // ROPDID
            // 
            this.ROPDID.DataPropertyName = "ROPDID";
            this.ROPDID.HeaderText = "OPDID";
            this.ROPDID.Name = "ROPDID";
            this.ROPDID.ReadOnly = true;
            this.ROPDID.Visible = false;
            // 
            // EnteredBy
            // 
            this.EnteredBy.DataPropertyName = "EnteredBy";
            this.EnteredBy.FillWeight = 0.01F;
            this.EnteredBy.HeaderText = "EnteredBy";
            this.EnteredBy.MinimumWidth = 2;
            this.EnteredBy.Name = "EnteredBy";
            this.EnteredBy.ReadOnly = true;
            this.EnteredBy.Visible = false;
            // 
            // PatientName
            // 
            this.PatientName.DataPropertyName = "PatientName";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PatientName.DefaultCellStyle = dataGridViewCellStyle47;
            this.PatientName.FillWeight = 15F;
            this.PatientName.HeaderText = "Name";
            this.PatientName.Name = "PatientName";
            this.PatientName.ReadOnly = true;
            // 
            // Gender
            // 
            this.Gender.DataPropertyName = "Gender";
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Gender.DefaultCellStyle = dataGridViewCellStyle110;
            this.Gender.FillWeight = 10F;
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            // 
            // DOB
            // 
            this.DOB.DataPropertyName = "DOB";
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle111.Format = "dd/MM/yyyy";
            this.DOB.DefaultCellStyle = dataGridViewCellStyle111;
            this.DOB.HeaderText = "DOB";
            this.DOB.Name = "DOB";
            this.DOB.ReadOnly = true;
            this.DOB.Visible = false;
            // 
            // RAge
            // 
            this.RAge.DataPropertyName = "Age";
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RAge.DefaultCellStyle = dataGridViewCellStyle112;
            this.RAge.FillWeight = 10F;
            this.RAge.HeaderText = "Age";
            this.RAge.Name = "RAge";
            this.RAge.ReadOnly = true;
            // 
            // PhoneNo
            // 
            this.PhoneNo.DataPropertyName = "PhoneNo";
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PhoneNo.DefaultCellStyle = dataGridViewCellStyle113;
            this.PhoneNo.FillWeight = 10F;
            this.PhoneNo.HeaderText = "Phone No.";
            this.PhoneNo.Name = "PhoneNo";
            this.PhoneNo.ReadOnly = true;
            // 
            // CellNo
            // 
            this.CellNo.DataPropertyName = "CellNo";
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CellNo.DefaultCellStyle = dataGridViewCellStyle114;
            this.CellNo.FillWeight = 10F;
            this.CellNo.HeaderText = "Cell No.";
            this.CellNo.Name = "CellNo";
            this.CellNo.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Address.DefaultCellStyle = dataGridViewCellStyle115;
            this.Address.FillWeight = 20F;
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            // 
            // cityname
            // 
            this.cityname.DataPropertyName = "cityname";
            this.cityname.FillWeight = 10F;
            this.cityname.HeaderText = "City";
            this.cityname.Name = "cityname";
            this.cityname.ReadOnly = true;
            // 
            // pnlRegisterSearch
            // 
            this.pnlRegisterSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRegisterSearch.Controls.Add(this.button11);
            this.pnlRegisterSearch.Controls.Add(this.button5);
            this.pnlRegisterSearch.Controls.Add(this.pictureBox1);
            this.pnlRegisterSearch.Controls.Add(this.dtpFrom);
            this.pnlRegisterSearch.Controls.Add(this.dtpTo);
            this.pnlRegisterSearch.Controls.Add(this.rbSCell);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchPRNO);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchPhone);
            this.pnlRegisterSearch.Controls.Add(this.txtSearchName);
            this.pnlRegisterSearch.Controls.Add(this.btnPatientSearch);
            this.pnlRegisterSearch.Controls.Add(this.lblCity);
            this.pnlRegisterSearch.Controls.Add(this.lblSearchName);
            this.pnlRegisterSearch.Controls.Add(this.lblTo);
            this.pnlRegisterSearch.Controls.Add(this.lblFrom);
            this.pnlRegisterSearch.Controls.Add(this.lblSearchPRNO);
            this.pnlRegisterSearch.Enabled = false;
            this.pnlRegisterSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlRegisterSearch.Location = new System.Drawing.Point(140, 1);
            this.pnlRegisterSearch.Name = "pnlRegisterSearch";
            this.pnlRegisterSearch.Size = new System.Drawing.Size(867, 30);
            this.pnlRegisterSearch.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(542, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 159;
            this.pictureBox1.TabStop = false;
            // 
            // rbSCell
            // 
            this.rbSCell.AutoSize = true;
            this.rbSCell.Location = new System.Drawing.Point(378, 8);
            this.rbSCell.Name = "rbSCell";
            this.rbSCell.Size = new System.Drawing.Size(76, 13);
            this.rbSCell.TabIndex = 0;
            this.rbSCell.Text = "Whatsapp No.";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(284, 11);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(41, 13);
            this.lblCity.TabIndex = 138;
            this.lblCity.Text = "label14";
            this.lblCity.Visible = false;
            // 
            // lblSearchName
            // 
            this.lblSearchName.AutoSize = true;
            this.lblSearchName.Location = new System.Drawing.Point(171, 9);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(44, 13);
            this.lblSearchName.TabIndex = 3;
            this.lblSearchName.Text = "Name : ";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(703, 9);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(26, 13);
            this.lblTo.TabIndex = 123;
            this.lblTo.Text = "To :";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(568, 9);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(36, 13);
            this.lblFrom.TabIndex = 124;
            this.lblFrom.Text = "From :";
            // 
            // lblSearchPRNO
            // 
            this.lblSearchPRNO.AutoSize = true;
            this.lblSearchPRNO.Location = new System.Drawing.Point(-2, 8);
            this.lblSearchPRNO.Name = "lblSearchPRNO";
            this.lblSearchPRNO.Size = new System.Drawing.Size(53, 13);
            this.lblSearchPRNO.TabIndex = 0;
            this.lblSearchPRNO.Text = "MR No. : ";
            // 
            // pbPatient
            // 
            this.pbPatient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbPatient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPatient.Image = ((System.Drawing.Image)(resources.GetObject("pbPatient.Image")));
            this.pbPatient.ImageLocation = "";
            this.pbPatient.Location = new System.Drawing.Point(5, 3);
            this.pbPatient.Name = "pbPatient";
            this.pbPatient.Size = new System.Drawing.Size(80, 80);
            this.pbPatient.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPatient.TabIndex = 141;
            this.pbPatient.TabStop = false;
            this.pbPatient.Click += new System.EventHandler(this.pbPatient_DoubleClick);
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(200, 100);
            this.panel11.TabIndex = 0;
            // 
            // pnlHold
            // 
            this.pnlHold.BackColor = System.Drawing.Color.Lavender;
            this.pnlHold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHold.Controls.Add(this.lblHoldCount);
            this.pnlHold.Controls.Add(this.btnHoldSerach);
            this.pnlHold.Controls.Add(this.pnlHPatient);
            this.pnlHold.Controls.Add(this.dgHold);
            this.pnlHold.Controls.Add(this.pnlHoldSearch);
            this.pnlHold.Controls.Add(this.pbHold);
            this.pnlHold.Controls.Add(this.label25);
            this.pnlHold.Location = new System.Drawing.Point(2, 77);
            this.pnlHold.Name = "pnlHold";
            this.pnlHold.Size = new System.Drawing.Size(1012, 88);
            this.pnlHold.TabIndex = 0;
            this.pnlHold.Tag = "";
            this.pnlHold.Visible = false;
            // 
            // lblHoldCount
            // 
            this.lblHoldCount.AutoSize = true;
            this.lblHoldCount.Location = new System.Drawing.Point(3, 91);
            this.lblHoldCount.Name = "lblHoldCount";
            this.lblHoldCount.Size = new System.Drawing.Size(0, 13);
            this.lblHoldCount.TabIndex = 155;
            this.lblHoldCount.Tag = "display";
            // 
            // pnlHPatient
            // 
            this.pnlHPatient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHPatient.Controls.Add(this.button3);
            this.pnlHPatient.Controls.Add(this.button2);
            this.pnlHPatient.Controls.Add(this.pictureBox5);
            this.pnlHPatient.Controls.Add(this.rbHSPhone);
            this.pnlHPatient.Controls.Add(this.txtHPhone);
            this.pnlHPatient.Controls.Add(this.panel9);
            this.pnlHPatient.Controls.Add(this.panel13);
            this.pnlHPatient.Controls.Add(this.cboHPanel);
            this.pnlHPatient.Controls.Add(this.panel6);
            this.pnlHPatient.Controls.Add(this.panel4);
            this.pnlHPatient.Controls.Add(this.txtHName);
            this.pnlHPatient.Controls.Add(this.lblHPatientID);
            this.pnlHPatient.Controls.Add(this.txtHEmail);
            this.pnlHPatient.Controls.Add(this.txtHPRNo);
            this.pnlHPatient.Controls.Add(this.dtpHDOB);
            this.pnlHPatient.Controls.Add(this.txtHAge);
            this.pnlHPatient.Controls.Add(this.cboHCity);
            this.pnlHPatient.Controls.Add(this.txtHCell);
            this.pnlHPatient.Controls.Add(this.txtHAddress);
            this.pnlHPatient.Controls.Add(this.btnHoldUpdate);
            this.pnlHPatient.Controls.Add(this.panel8);
            this.pnlHPatient.Controls.Add(this.lblHName);
            this.pnlHPatient.Controls.Add(this.lblHPRNo);
            this.pnlHPatient.Controls.Add(this.lblHEmail);
            this.pnlHPatient.Controls.Add(this.lblHCity);
            this.pnlHPatient.Controls.Add(this.lblHDOB);
            this.pnlHPatient.Controls.Add(this.lblHAge);
            this.pnlHPatient.Controls.Add(this.lblHAddress);
            this.pnlHPatient.Controls.Add(this.lblHPhone);
            this.pnlHPatient.Controls.Add(this.lblHCell);
            this.pnlHPatient.Controls.Add(this.lblHPanel);
            this.pnlHPatient.Controls.Add(this.lblHGender);
            this.pnlHPatient.Enabled = false;
            this.pnlHPatient.Location = new System.Drawing.Point(142, 32);
            this.pnlHPatient.Name = "pnlHPatient";
            this.pnlHPatient.Size = new System.Drawing.Size(867, 53);
            this.pnlHPatient.TabIndex = 2;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(230, 27);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(25, 21);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 180;
            this.pictureBox5.TabStop = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Red;
            this.panel13.Location = new System.Drawing.Point(732, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(4, 19);
            this.panel13.TabIndex = 178;
            this.panel13.Tag = "Self";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Red;
            this.panel6.Location = new System.Drawing.Point(480, 1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(4, 23);
            this.panel6.TabIndex = 177;
            this.panel6.Tag = "Self";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Red;
            this.panel4.Location = new System.Drawing.Point(320, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(4, 20);
            this.panel4.TabIndex = 156;
            this.panel4.Tag = "Self";
            // 
            // lblHPatientID
            // 
            this.lblHPatientID.AutoSize = true;
            this.lblHPatientID.Location = new System.Drawing.Point(207, 9);
            this.lblHPatientID.Name = "lblHPatientID";
            this.lblHPatientID.Size = new System.Drawing.Size(69, 13);
            this.lblHPatientID.TabIndex = 154;
            this.lblHPatientID.Text = "lblHPatientID";
            this.lblHPatientID.Visible = false;
            // 
            // btnHoldUpdate
            // 
            this.btnHoldUpdate.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnHoldUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHoldUpdate.BackgroundImage")));
            this.btnHoldUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHoldUpdate.FlatAppearance.BorderSize = 0;
            this.btnHoldUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.btnHoldUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SkyBlue;
            this.btnHoldUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 1F);
            this.btnHoldUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnHoldUpdate.Location = new System.Drawing.Point(830, 8);
            this.btnHoldUpdate.Name = "btnHoldUpdate";
            this.btnHoldUpdate.Size = new System.Drawing.Size(35, 35);
            this.btnHoldUpdate.TabIndex = 11;
            this.btnHoldUpdate.Tag = "update";
            this.btnHoldUpdate.Text = "Update";
            this.btnHoldUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnHoldUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnHoldUpdate.UseVisualStyleBackColor = false;
            this.btnHoldUpdate.Click += new System.EventHandler(this.btnHoldUpdate_Click);
            // 
            // lblHName
            // 
            this.lblHName.AutoSize = true;
            this.lblHName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHName.Location = new System.Drawing.Point(145, 6);
            this.lblHName.Name = "lblHName";
            this.lblHName.Size = new System.Drawing.Size(41, 13);
            this.lblHName.TabIndex = 150;
            this.lblHName.Text = "Name :";
            // 
            // lblHPRNo
            // 
            this.lblHPRNo.AutoSize = true;
            this.lblHPRNo.Location = new System.Drawing.Point(-3, 6);
            this.lblHPRNo.Name = "lblHPRNo";
            this.lblHPRNo.Size = new System.Drawing.Size(53, 13);
            this.lblHPRNo.TabIndex = 158;
            this.lblHPRNo.Tag = "";
            this.lblHPRNo.Text = "MR No. : ";
            // 
            // lblHEmail
            // 
            this.lblHEmail.AutoSize = true;
            this.lblHEmail.Location = new System.Drawing.Point(271, 35);
            this.lblHEmail.Name = "lblHEmail";
            this.lblHEmail.Size = new System.Drawing.Size(38, 13);
            this.lblHEmail.TabIndex = 176;
            this.lblHEmail.Text = "Email :";
            // 
            // lblHCity
            // 
            this.lblHCity.AutoSize = true;
            this.lblHCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHCity.Location = new System.Drawing.Point(700, 31);
            this.lblHCity.Name = "lblHCity";
            this.lblHCity.Size = new System.Drawing.Size(30, 13);
            this.lblHCity.TabIndex = 152;
            this.lblHCity.Text = "City :";
            // 
            // lblHDOB
            // 
            this.lblHDOB.AutoSize = true;
            this.lblHDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHDOB.Location = new System.Drawing.Point(498, 7);
            this.lblHDOB.Name = "lblHDOB";
            this.lblHDOB.Size = new System.Drawing.Size(36, 13);
            this.lblHDOB.TabIndex = 149;
            this.lblHDOB.Text = "DOB :";
            this.lblHDOB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHAge
            // 
            this.lblHAge.AutoSize = true;
            this.lblHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAge.Location = new System.Drawing.Point(617, 7);
            this.lblHAge.Name = "lblHAge";
            this.lblHAge.Size = new System.Drawing.Size(32, 13);
            this.lblHAge.TabIndex = 148;
            this.lblHAge.Text = "Age :";
            // 
            // lblHAddress
            // 
            this.lblHAddress.AutoSize = true;
            this.lblHAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAddress.Location = new System.Drawing.Point(483, 31);
            this.lblHAddress.Name = "lblHAddress";
            this.lblHAddress.Size = new System.Drawing.Size(51, 13);
            this.lblHAddress.TabIndex = 144;
            this.lblHAddress.Text = "Address :";
            // 
            // lblHPhone
            // 
            this.lblHPhone.AutoSize = true;
            this.lblHPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHPhone.Location = new System.Drawing.Point(45, 35);
            this.lblHPhone.Name = "lblHPhone";
            this.lblHPhone.Size = new System.Drawing.Size(0, 13);
            this.lblHPhone.TabIndex = 146;
            this.lblHPhone.Visible = false;
            // 
            // lblHCell
            // 
            this.lblHCell.AutoSize = true;
            this.lblHCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHCell.Location = new System.Drawing.Point(1, 31);
            this.lblHCell.Name = "lblHCell";
            this.lblHCell.Size = new System.Drawing.Size(76, 13);
            this.lblHCell.TabIndex = 3;
            this.lblHCell.Text = "Whatsapp No.";
            // 
            // lblHPanel
            // 
            this.lblHPanel.AutoSize = true;
            this.lblHPanel.Location = new System.Drawing.Point(321, 31);
            this.lblHPanel.Name = "lblHPanel";
            this.lblHPanel.Size = new System.Drawing.Size(0, 13);
            this.lblHPanel.TabIndex = 163;
            this.lblHPanel.Visible = false;
            // 
            // lblHGender
            // 
            this.lblHGender.AutoSize = true;
            this.lblHGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHGender.Location = new System.Drawing.Point(324, 7);
            this.lblHGender.Name = "lblHGender";
            this.lblHGender.Size = new System.Drawing.Size(48, 13);
            this.lblHGender.TabIndex = 147;
            this.lblHGender.Text = "Gender :";
            // 
            // dgHold
            // 
            this.dgHold.AllowUserToAddRows = false;
            this.dgHold.AllowUserToDeleteRows = false;
            this.dgHold.AllowUserToResizeRows = false;
            this.dgHold.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgHold.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgHold.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgHold.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle118.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle118.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle118.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle118.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle118.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle118.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHold.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle118;
            this.dgHold.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgHold.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HPatientID,
            this.HSNo,
            this.HPanel,
            this.HPRNO,
            this.OPDID,
            this.HEnteredBy,
            this.HPatientName,
            this.HGender,
            this.HDOB,
            this.HAge,
            this.HoldVisitDate,
            this.HPhone,
            this.HCell,
            this.HEmail,
            this.HCityName,
            this.HAddress});
            dataGridViewCellStyle129.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle129.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle129.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle129.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle129.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle129.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle129.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgHold.DefaultCellStyle = dataGridViewCellStyle129;
            this.dgHold.EnableHeadersVisualStyles = false;
            this.dgHold.Location = new System.Drawing.Point(2, 106);
            this.dgHold.MultiSelect = false;
            this.dgHold.Name = "dgHold";
            this.dgHold.ReadOnly = true;
            dataGridViewCellStyle130.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle130.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle130.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle130.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle130.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle130.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHold.RowHeadersDefaultCellStyle = dataGridViewCellStyle130;
            this.dgHold.RowHeadersWidth = 30;
            this.dgHold.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgHold.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgHold.Size = new System.Drawing.Size(1007, 514);
            this.dgHold.TabIndex = 153;
            this.dgHold.TabStop = false;
            this.dgHold.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgHold_CellDoubleClick);
            this.dgHold.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgHold_ColumnHeaderMouseClick);
            // 
            // HPatientID
            // 
            this.HPatientID.DataPropertyName = "PatientID";
            this.HPatientID.HeaderText = "Patient ID";
            this.HPatientID.Name = "HPatientID";
            this.HPatientID.ReadOnly = true;
            this.HPatientID.Visible = false;
            // 
            // HSNo
            // 
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HSNo.DefaultCellStyle = dataGridViewCellStyle119;
            this.HSNo.FillWeight = 5F;
            this.HSNo.HeaderText = "S No.";
            this.HSNo.Name = "HSNo";
            this.HSNo.ReadOnly = true;
            // 
            // HPanel
            // 
            this.HPanel.DataPropertyName = "PanelName";
            this.HPanel.HeaderText = "Panel";
            this.HPanel.Name = "HPanel";
            this.HPanel.ReadOnly = true;
            this.HPanel.Visible = false;
            // 
            // HPRNO
            // 
            this.HPRNO.DataPropertyName = "PRNO";
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HPRNO.DefaultCellStyle = dataGridViewCellStyle120;
            this.HPRNO.FillWeight = 10F;
            this.HPRNO.HeaderText = "PR No.";
            this.HPRNO.Name = "HPRNO";
            this.HPRNO.ReadOnly = true;
            // 
            // OPDID
            // 
            this.OPDID.DataPropertyName = "OPDID";
            this.OPDID.HeaderText = "OPDID";
            this.OPDID.Name = "OPDID";
            this.OPDID.ReadOnly = true;
            this.OPDID.Visible = false;
            // 
            // HEnteredBy
            // 
            this.HEnteredBy.DataPropertyName = "EnteredBy";
            this.HEnteredBy.HeaderText = "EnteredBy";
            this.HEnteredBy.Name = "HEnteredBy";
            this.HEnteredBy.ReadOnly = true;
            this.HEnteredBy.Visible = false;
            // 
            // HPatientName
            // 
            this.HPatientName.DataPropertyName = "PatientName";
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.HPatientName.DefaultCellStyle = dataGridViewCellStyle121;
            this.HPatientName.FillWeight = 15F;
            this.HPatientName.HeaderText = "Name";
            this.HPatientName.Name = "HPatientName";
            this.HPatientName.ReadOnly = true;
            // 
            // HGender
            // 
            this.HGender.DataPropertyName = "Gender";
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HGender.DefaultCellStyle = dataGridViewCellStyle122;
            this.HGender.FillWeight = 10F;
            this.HGender.HeaderText = "Gender";
            this.HGender.Name = "HGender";
            this.HGender.ReadOnly = true;
            // 
            // HDOB
            // 
            this.HDOB.DataPropertyName = "DOB";
            dataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle123.Format = "dd/MM/yyyy";
            this.HDOB.DefaultCellStyle = dataGridViewCellStyle123;
            this.HDOB.HeaderText = "DOB";
            this.HDOB.Name = "HDOB";
            this.HDOB.ReadOnly = true;
            this.HDOB.Visible = false;
            // 
            // HAge
            // 
            this.HAge.DataPropertyName = "Age";
            dataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HAge.DefaultCellStyle = dataGridViewCellStyle124;
            this.HAge.FillWeight = 10F;
            this.HAge.HeaderText = "Age";
            this.HAge.Name = "HAge";
            this.HAge.ReadOnly = true;
            // 
            // HoldVisitDate
            // 
            this.HoldVisitDate.DataPropertyName = "VisitDate";
            dataGridViewCellStyle125.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle125.Format = "dd-MM-yyyy";
            this.HoldVisitDate.DefaultCellStyle = dataGridViewCellStyle125;
            this.HoldVisitDate.FillWeight = 10F;
            this.HoldVisitDate.HeaderText = "Visit Date";
            this.HoldVisitDate.Name = "HoldVisitDate";
            this.HoldVisitDate.ReadOnly = true;
            // 
            // HPhone
            // 
            this.HPhone.DataPropertyName = "PhoneNo";
            dataGridViewCellStyle126.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HPhone.DefaultCellStyle = dataGridViewCellStyle126;
            this.HPhone.FillWeight = 10F;
            this.HPhone.HeaderText = "Phone No.";
            this.HPhone.Name = "HPhone";
            this.HPhone.ReadOnly = true;
            // 
            // HCell
            // 
            this.HCell.DataPropertyName = "CellNo";
            dataGridViewCellStyle127.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HCell.DefaultCellStyle = dataGridViewCellStyle127;
            this.HCell.FillWeight = 10F;
            this.HCell.HeaderText = "Cell No.";
            this.HCell.Name = "HCell";
            this.HCell.ReadOnly = true;
            // 
            // HEmail
            // 
            this.HEmail.DataPropertyName = "Email";
            this.HEmail.HeaderText = "Email";
            this.HEmail.Name = "HEmail";
            this.HEmail.ReadOnly = true;
            this.HEmail.Visible = false;
            // 
            // HCityName
            // 
            this.HCityName.DataPropertyName = "cityname";
            this.HCityName.HeaderText = "CityName";
            this.HCityName.Name = "HCityName";
            this.HCityName.ReadOnly = true;
            this.HCityName.Visible = false;
            // 
            // HAddress
            // 
            this.HAddress.DataPropertyName = "Address";
            dataGridViewCellStyle128.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.HAddress.DefaultCellStyle = dataGridViewCellStyle128;
            this.HAddress.FillWeight = 20F;
            this.HAddress.HeaderText = "Address";
            this.HAddress.Name = "HAddress";
            this.HAddress.ReadOnly = true;
            // 
            // pnlHoldSearch
            // 
            this.pnlHoldSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHoldSearch.Controls.Add(this.button6);
            this.pnlHoldSearch.Controls.Add(this.button1);
            this.pnlHoldSearch.Controls.Add(this.pictureBox6);
            this.pnlHoldSearch.Controls.Add(this.label1);
            this.pnlHoldSearch.Controls.Add(this.dtpHFrom);
            this.pnlHoldSearch.Controls.Add(this.dtpHTo);
            this.pnlHoldSearch.Controls.Add(this.txtHSPRNO);
            this.pnlHoldSearch.Controls.Add(this.txtHSPhone);
            this.pnlHoldSearch.Controls.Add(this.txtHSName);
            this.pnlHoldSearch.Controls.Add(this.btnHSearch);
            this.pnlHoldSearch.Controls.Add(this.lblHSName);
            this.pnlHoldSearch.Controls.Add(this.lblHSPRNO);
            this.pnlHoldSearch.Controls.Add(this.lblHTo);
            this.pnlHoldSearch.Controls.Add(this.lblHFrom);
            this.pnlHoldSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlHoldSearch.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pnlHoldSearch.Location = new System.Drawing.Point(142, 1);
            this.pnlHoldSearch.Name = "pnlHoldSearch";
            this.pnlHoldSearch.Size = new System.Drawing.Size(867, 30);
            this.pnlHoldSearch.TabIndex = 1;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(497, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(25, 21);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 180;
            this.pictureBox6.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(333, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 129;
            this.label1.Text = "Whatsapp No.";
            // 
            // btnHSearch
            // 
            this.btnHSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHSearch.BackgroundImage")));
            this.btnHSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHSearch.Location = new System.Drawing.Point(842, 2);
            this.btnHSearch.Name = "btnHSearch";
            this.btnHSearch.Size = new System.Drawing.Size(23, 23);
            this.btnHSearch.TabIndex = 6;
            this.btnHSearch.UseVisualStyleBackColor = true;
            this.btnHSearch.Click += new System.EventHandler(this.btnHSearch_Click);
            // 
            // lblHSName
            // 
            this.lblHSName.AutoSize = true;
            this.lblHSName.Location = new System.Drawing.Point(142, 8);
            this.lblHSName.Name = "lblHSName";
            this.lblHSName.Size = new System.Drawing.Size(44, 13);
            this.lblHSName.TabIndex = 3;
            this.lblHSName.Text = "Name : ";
            // 
            // lblHSPRNO
            // 
            this.lblHSPRNO.AutoSize = true;
            this.lblHSPRNO.Location = new System.Drawing.Point(-2, 8);
            this.lblHSPRNO.Name = "lblHSPRNO";
            this.lblHSPRNO.Size = new System.Drawing.Size(44, 13);
            this.lblHSPRNO.TabIndex = 0;
            this.lblHSPRNO.Text = "MR No.";
            // 
            // lblHTo
            // 
            this.lblHTo.AutoSize = true;
            this.lblHTo.Location = new System.Drawing.Point(707, 7);
            this.lblHTo.Name = "lblHTo";
            this.lblHTo.Size = new System.Drawing.Size(26, 13);
            this.lblHTo.TabIndex = 127;
            this.lblHTo.Text = "To :";
            // 
            // lblHFrom
            // 
            this.lblHFrom.AutoSize = true;
            this.lblHFrom.Location = new System.Drawing.Point(572, 8);
            this.lblHFrom.Name = "lblHFrom";
            this.lblHFrom.Size = new System.Drawing.Size(36, 13);
            this.lblHFrom.TabIndex = 128;
            this.lblHFrom.Text = "From :";
            // 
            // pbHold
            // 
            this.pbHold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbHold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbHold.Image = ((System.Drawing.Image)(resources.GetObject("pbHold.Image")));
            this.pbHold.ImageLocation = "";
            this.pbHold.Location = new System.Drawing.Point(5, 3);
            this.pbHold.Name = "pbHold";
            this.pbHold.Size = new System.Drawing.Size(80, 80);
            this.pbHold.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHold.TabIndex = 151;
            this.pbHold.TabStop = false;
            this.pbHold.Click += new System.EventHandler(this.pbPatient_DoubleClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(684, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "label25";
            this.label25.Visible = false;
            // 
            // pnlConsult
            // 
            this.pnlConsult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConsult.Controls.Add(this.lblConsultCount);
            this.pnlConsult.Controls.Add(this.lblCPatientCount);
            this.pnlConsult.Controls.Add(this.btnConsultedSerach);
            this.pnlConsult.Controls.Add(this.pnlCPatient);
            this.pnlConsult.Controls.Add(this.pbConsulted);
            this.pnlConsult.Controls.Add(this.dgConsulted);
            this.pnlConsult.Controls.Add(this.pnlConsultedSearch);
            this.pnlConsult.Location = new System.Drawing.Point(2, 77);
            this.pnlConsult.Name = "pnlConsult";
            this.pnlConsult.Size = new System.Drawing.Size(1012, 88);
            this.pnlConsult.TabIndex = 0;
            this.pnlConsult.Tag = "";
            this.pnlConsult.Visible = false;
            // 
            // lblConsultCount
            // 
            this.lblConsultCount.AutoSize = true;
            this.lblConsultCount.Location = new System.Drawing.Point(3, 91);
            this.lblConsultCount.Name = "lblConsultCount";
            this.lblConsultCount.Size = new System.Drawing.Size(0, 13);
            this.lblConsultCount.TabIndex = 167;
            this.lblConsultCount.Tag = "display";
            // 
            // lblCPatientCount
            // 
            this.lblCPatientCount.AutoSize = true;
            this.lblCPatientCount.Location = new System.Drawing.Point(3, 91);
            this.lblCPatientCount.Name = "lblCPatientCount";
            this.lblCPatientCount.Size = new System.Drawing.Size(0, 13);
            this.lblCPatientCount.TabIndex = 166;
            this.lblCPatientCount.Tag = "display";
            // 
            // pnlCPatient
            // 
            this.pnlCPatient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCPatient.Controls.Add(this.button10);
            this.pnlCPatient.Controls.Add(this.button8);
            this.pnlCPatient.Controls.Add(this.pictureBox3);
            this.pnlCPatient.Controls.Add(this.panel18);
            this.pnlCPatient.Controls.Add(this.panel17);
            this.pnlCPatient.Controls.Add(this.panel5);
            this.pnlCPatient.Controls.Add(this.txtCName);
            this.pnlCPatient.Controls.Add(this.txtCPRNo);
            this.pnlCPatient.Controls.Add(this.btnCUpdate);
            this.pnlCPatient.Controls.Add(this.dtpCDOB);
            this.pnlCPatient.Controls.Add(this.txtCEmail);
            this.pnlCPatient.Controls.Add(this.txtCAge);
            this.pnlCPatient.Controls.Add(this.cboCCity);
            this.pnlCPatient.Controls.Add(this.panel10);
            this.pnlCPatient.Controls.Add(this.txtCAddress);
            this.pnlCPatient.Controls.Add(this.lblCAgeT);
            this.pnlCPatient.Controls.Add(this.txtCCell);
            this.pnlCPatient.Controls.Add(this.lblCNameT);
            this.pnlCPatient.Controls.Add(this.lblCEmail);
            this.pnlCPatient.Controls.Add(this.lblCDOBT);
            this.pnlCPatient.Controls.Add(this.lblCCityT);
            this.pnlCPatient.Controls.Add(this.lblCCellT);
            this.pnlCPatient.Controls.Add(this.lblCAddressT);
            this.pnlCPatient.Controls.Add(this.lblCGenderT);
            this.pnlCPatient.Controls.Add(this.lblCPRNo);
            this.pnlCPatient.Enabled = false;
            this.pnlCPatient.Location = new System.Drawing.Point(142, 32);
            this.pnlCPatient.Name = "pnlCPatient";
            this.pnlCPatient.Size = new System.Drawing.Size(867, 53);
            this.pnlCPatient.TabIndex = 2;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(174, 27);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(25, 21);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 179;
            this.pictureBox3.TabStop = false;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Red;
            this.panel18.Location = new System.Drawing.Point(724, 3);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(4, 20);
            this.panel18.TabIndex = 178;
            this.panel18.Tag = "Self";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Red;
            this.panel17.Location = new System.Drawing.Point(480, 1);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(4, 23);
            this.panel17.TabIndex = 157;
            this.panel17.Tag = "Self";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Red;
            this.panel5.Location = new System.Drawing.Point(320, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(4, 20);
            this.panel5.TabIndex = 174;
            this.panel5.Tag = "Self";
            // 
            // lblCAgeT
            // 
            this.lblCAgeT.AutoSize = true;
            this.lblCAgeT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCAgeT.Location = new System.Drawing.Point(617, 7);
            this.lblCAgeT.Name = "lblCAgeT";
            this.lblCAgeT.Size = new System.Drawing.Size(32, 13);
            this.lblCAgeT.TabIndex = 168;
            this.lblCAgeT.Text = "Age :";
            // 
            // lblCNameT
            // 
            this.lblCNameT.AutoSize = true;
            this.lblCNameT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNameT.Location = new System.Drawing.Point(139, 7);
            this.lblCNameT.Name = "lblCNameT";
            this.lblCNameT.Size = new System.Drawing.Size(41, 13);
            this.lblCNameT.TabIndex = 170;
            this.lblCNameT.Text = "Name :";
            // 
            // lblCEmail
            // 
            this.lblCEmail.AutoSize = true;
            this.lblCEmail.Location = new System.Drawing.Point(224, 32);
            this.lblCEmail.Name = "lblCEmail";
            this.lblCEmail.Size = new System.Drawing.Size(38, 13);
            this.lblCEmail.TabIndex = 176;
            this.lblCEmail.Text = "Email :";
            // 
            // lblCDOBT
            // 
            this.lblCDOBT.AutoSize = true;
            this.lblCDOBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCDOBT.Location = new System.Drawing.Point(498, 7);
            this.lblCDOBT.Name = "lblCDOBT";
            this.lblCDOBT.Size = new System.Drawing.Size(36, 13);
            this.lblCDOBT.TabIndex = 169;
            this.lblCDOBT.Text = "DOB :";
            // 
            // lblCCityT
            // 
            this.lblCCityT.AutoSize = true;
            this.lblCCityT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCCityT.Location = new System.Drawing.Point(700, 31);
            this.lblCCityT.Name = "lblCCityT";
            this.lblCCityT.Size = new System.Drawing.Size(30, 13);
            this.lblCCityT.TabIndex = 171;
            this.lblCCityT.Text = "City :";
            // 
            // lblCCellT
            // 
            this.lblCCellT.AutoSize = true;
            this.lblCCellT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCCellT.Location = new System.Drawing.Point(4, 29);
            this.lblCCellT.Name = "lblCCellT";
            this.lblCCellT.Size = new System.Drawing.Size(76, 13);
            this.lblCCellT.TabIndex = 3;
            this.lblCCellT.Text = "Whatsapp No.";
            // 
            // lblCAddressT
            // 
            this.lblCAddressT.AutoSize = true;
            this.lblCAddressT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCAddressT.Location = new System.Drawing.Point(483, 31);
            this.lblCAddressT.Name = "lblCAddressT";
            this.lblCAddressT.Size = new System.Drawing.Size(51, 13);
            this.lblCAddressT.TabIndex = 165;
            this.lblCAddressT.Text = "Address :";
            // 
            // lblCGenderT
            // 
            this.lblCGenderT.AutoSize = true;
            this.lblCGenderT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCGenderT.Location = new System.Drawing.Point(324, 7);
            this.lblCGenderT.Name = "lblCGenderT";
            this.lblCGenderT.Size = new System.Drawing.Size(48, 13);
            this.lblCGenderT.TabIndex = 167;
            this.lblCGenderT.Text = "Gender :";
            // 
            // lblCPRNo
            // 
            this.lblCPRNo.AutoSize = true;
            this.lblCPRNo.Location = new System.Drawing.Point(-3, 7);
            this.lblCPRNo.Name = "lblCPRNo";
            this.lblCPRNo.Size = new System.Drawing.Size(53, 13);
            this.lblCPRNo.TabIndex = 176;
            this.lblCPRNo.Tag = "";
            this.lblCPRNo.Text = "MR No. : ";
            // 
            // pbConsulted
            // 
            this.pbConsulted.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbConsulted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbConsulted.Image = ((System.Drawing.Image)(resources.GetObject("pbConsulted.Image")));
            this.pbConsulted.ImageLocation = "";
            this.pbConsulted.Location = new System.Drawing.Point(5, 3);
            this.pbConsulted.Name = "pbConsulted";
            this.pbConsulted.Size = new System.Drawing.Size(80, 80);
            this.pbConsulted.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbConsulted.TabIndex = 163;
            this.pbConsulted.TabStop = false;
            this.pbConsulted.Click += new System.EventHandler(this.pbPatient_DoubleClick);
            // 
            // dgConsulted
            // 
            this.dgConsulted.AllowUserToAddRows = false;
            this.dgConsulted.AllowUserToDeleteRows = false;
            this.dgConsulted.AllowUserToResizeRows = false;
            this.dgConsulted.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgConsulted.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgConsulted.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgConsulted.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle131.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle131.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle131.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle131.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle131.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle131.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle131.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConsulted.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle131;
            this.dgConsulted.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgConsulted.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CPatientID,
            this.CSNo,
            this.CPanel,
            this.CEmail,
            this.COPDID,
            this.CPRNO,
            this.CEnteredBy,
            this.CPatientName,
            this.CGender,
            this.CDOB,
            this.CAge,
            this.CVisitDate,
            this.CPhone,
            this.CCell,
            this.CCityName,
            this.CAddress});
            this.dgConsulted.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle142.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle142.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle142.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle142.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle142.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle142.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle142.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgConsulted.DefaultCellStyle = dataGridViewCellStyle142;
            this.dgConsulted.EnableHeadersVisualStyles = false;
            this.dgConsulted.Location = new System.Drawing.Point(2, 106);
            this.dgConsulted.MultiSelect = false;
            this.dgConsulted.Name = "dgConsulted";
            this.dgConsulted.ReadOnly = true;
            dataGridViewCellStyle143.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle143.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle143.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle143.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle143.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle143.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConsulted.RowHeadersDefaultCellStyle = dataGridViewCellStyle143;
            this.dgConsulted.RowHeadersWidth = 30;
            this.dgConsulted.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgConsulted.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConsulted.Size = new System.Drawing.Size(1007, 514);
            this.dgConsulted.TabIndex = 164;
            this.dgConsulted.TabStop = false;
            this.dgConsulted.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgConsulted_CellDoubleClick);
            this.dgConsulted.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgConsulted_ColumnHeaderMouseClick);
            // 
            // CPatientID
            // 
            this.CPatientID.DataPropertyName = "PatientID";
            this.CPatientID.HeaderText = "Patient ID";
            this.CPatientID.Name = "CPatientID";
            this.CPatientID.ReadOnly = true;
            this.CPatientID.Visible = false;
            // 
            // CSNo
            // 
            dataGridViewCellStyle132.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CSNo.DefaultCellStyle = dataGridViewCellStyle132;
            this.CSNo.FillWeight = 5F;
            this.CSNo.HeaderText = "S No.";
            this.CSNo.Name = "CSNo";
            this.CSNo.ReadOnly = true;
            // 
            // CPanel
            // 
            this.CPanel.DataPropertyName = "PanelName";
            this.CPanel.HeaderText = "Panel";
            this.CPanel.Name = "CPanel";
            this.CPanel.ReadOnly = true;
            this.CPanel.Visible = false;
            // 
            // CEmail
            // 
            this.CEmail.DataPropertyName = "Email";
            this.CEmail.HeaderText = "Email";
            this.CEmail.Name = "CEmail";
            this.CEmail.ReadOnly = true;
            this.CEmail.Visible = false;
            // 
            // COPDID
            // 
            this.COPDID.DataPropertyName = "opdid";
            this.COPDID.HeaderText = "OPDID";
            this.COPDID.Name = "COPDID";
            this.COPDID.ReadOnly = true;
            this.COPDID.Visible = false;
            // 
            // CPRNO
            // 
            this.CPRNO.DataPropertyName = "PRNO";
            dataGridViewCellStyle133.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CPRNO.DefaultCellStyle = dataGridViewCellStyle133;
            this.CPRNO.FillWeight = 10F;
            this.CPRNO.HeaderText = "PR No.";
            this.CPRNO.Name = "CPRNO";
            this.CPRNO.ReadOnly = true;
            // 
            // CEnteredBy
            // 
            this.CEnteredBy.DataPropertyName = "EnteredBy";
            this.CEnteredBy.HeaderText = "EnteredBy";
            this.CEnteredBy.Name = "CEnteredBy";
            this.CEnteredBy.ReadOnly = true;
            this.CEnteredBy.Visible = false;
            // 
            // CPatientName
            // 
            this.CPatientName.DataPropertyName = "PatientName";
            dataGridViewCellStyle134.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CPatientName.DefaultCellStyle = dataGridViewCellStyle134;
            this.CPatientName.FillWeight = 15F;
            this.CPatientName.HeaderText = "Name";
            this.CPatientName.Name = "CPatientName";
            this.CPatientName.ReadOnly = true;
            // 
            // CGender
            // 
            this.CGender.DataPropertyName = "Gender";
            dataGridViewCellStyle135.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CGender.DefaultCellStyle = dataGridViewCellStyle135;
            this.CGender.FillWeight = 10F;
            this.CGender.HeaderText = "Gender";
            this.CGender.Name = "CGender";
            this.CGender.ReadOnly = true;
            // 
            // CDOB
            // 
            this.CDOB.DataPropertyName = "DOB";
            dataGridViewCellStyle136.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle136.Format = "dd/MM/yyyy";
            this.CDOB.DefaultCellStyle = dataGridViewCellStyle136;
            this.CDOB.HeaderText = "DOB";
            this.CDOB.Name = "CDOB";
            this.CDOB.ReadOnly = true;
            this.CDOB.Visible = false;
            // 
            // CAge
            // 
            this.CAge.DataPropertyName = "Age";
            dataGridViewCellStyle137.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CAge.DefaultCellStyle = dataGridViewCellStyle137;
            this.CAge.FillWeight = 10F;
            this.CAge.HeaderText = "Age";
            this.CAge.Name = "CAge";
            this.CAge.ReadOnly = true;
            // 
            // CVisitDate
            // 
            this.CVisitDate.DataPropertyName = "VisitDate";
            dataGridViewCellStyle138.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle138.Format = "dd-MM-yyyy";
            this.CVisitDate.DefaultCellStyle = dataGridViewCellStyle138;
            this.CVisitDate.FillWeight = 10F;
            this.CVisitDate.HeaderText = "Visit Date";
            this.CVisitDate.Name = "CVisitDate";
            this.CVisitDate.ReadOnly = true;
            // 
            // CPhone
            // 
            this.CPhone.DataPropertyName = "PhoneNo";
            dataGridViewCellStyle139.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CPhone.DefaultCellStyle = dataGridViewCellStyle139;
            this.CPhone.FillWeight = 10F;
            this.CPhone.HeaderText = "Phone No.";
            this.CPhone.Name = "CPhone";
            this.CPhone.ReadOnly = true;
            // 
            // CCell
            // 
            this.CCell.DataPropertyName = "CellNo";
            dataGridViewCellStyle140.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CCell.DefaultCellStyle = dataGridViewCellStyle140;
            this.CCell.FillWeight = 10F;
            this.CCell.HeaderText = "Cell No.";
            this.CCell.Name = "CCell";
            this.CCell.ReadOnly = true;
            // 
            // CCityName
            // 
            this.CCityName.DataPropertyName = "cityname";
            this.CCityName.HeaderText = "CityName";
            this.CCityName.Name = "CCityName";
            this.CCityName.ReadOnly = true;
            this.CCityName.Visible = false;
            // 
            // CAddress
            // 
            this.CAddress.DataPropertyName = "Address";
            dataGridViewCellStyle141.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CAddress.DefaultCellStyle = dataGridViewCellStyle141;
            this.CAddress.FillWeight = 20F;
            this.CAddress.HeaderText = "Address";
            this.CAddress.Name = "CAddress";
            this.CAddress.ReadOnly = true;
            // 
            // pnlConsultedSearch
            // 
            this.pnlConsultedSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConsultedSearch.Controls.Add(this.button7);
            this.pnlConsultedSearch.Controls.Add(this.button4);
            this.pnlConsultedSearch.Controls.Add(this.pictureBox4);
            this.pnlConsultedSearch.Controls.Add(this.label2);
            this.pnlConsultedSearch.Controls.Add(this.dtpCTo);
            this.pnlConsultedSearch.Controls.Add(this.panel12);
            this.pnlConsultedSearch.Controls.Add(this.dtpCFrom);
            this.pnlConsultedSearch.Controls.Add(this.txtCSPRNO);
            this.pnlConsultedSearch.Controls.Add(this.txtCSPhone);
            this.pnlConsultedSearch.Controls.Add(this.txtCSName);
            this.pnlConsultedSearch.Controls.Add(this.btnConsultedSearch);
            this.pnlConsultedSearch.Controls.Add(this.lblCPatientID);
            this.pnlConsultedSearch.Controls.Add(this.lblCSName);
            this.pnlConsultedSearch.Controls.Add(this.lblCTo);
            this.pnlConsultedSearch.Controls.Add(this.lblCFrom);
            this.pnlConsultedSearch.Controls.Add(this.lblCSPRNO);
            this.pnlConsultedSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlConsultedSearch.Location = new System.Drawing.Point(142, 1);
            this.pnlConsultedSearch.Name = "pnlConsultedSearch";
            this.pnlConsultedSearch.Size = new System.Drawing.Size(867, 30);
            this.pnlConsultedSearch.TabIndex = 1;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(540, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(25, 21);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 180;
            this.pictureBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 129;
            this.label2.Text = "Whatsapp No.";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.rbCSPhone);
            this.panel12.Controls.Add(this.rbCSCell);
            this.panel12.Controls.Add(this.txtCPhone);
            this.panel12.Controls.Add(this.lblCPhoneT);
            this.panel12.Controls.Add(this.cboCPanel);
            this.panel12.Controls.Add(this.lblCPanel);
            this.panel12.Location = new System.Drawing.Point(338, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(23, 24);
            this.panel12.TabIndex = 2;
            this.panel12.Visible = false;
            // 
            // rbCSPhone
            // 
            this.rbCSPhone.AutoCheck = false;
            this.rbCSPhone.AutoSize = true;
            this.rbCSPhone.Checked = true;
            this.rbCSPhone.Location = new System.Drawing.Point(3, 3);
            this.rbCSPhone.Name = "rbCSPhone";
            this.rbCSPhone.Size = new System.Drawing.Size(32, 17);
            this.rbCSPhone.TabIndex = 1;
            this.rbCSPhone.TabStop = true;
            this.rbCSPhone.Text = "P";
            this.rbCSPhone.UseVisualStyleBackColor = true;
            this.rbCSPhone.Visible = false;
            // 
            // rbCSCell
            // 
            this.rbCSCell.AutoSize = true;
            this.rbCSCell.Location = new System.Drawing.Point(2, 0);
            this.rbCSCell.Name = "rbCSCell";
            this.rbCSCell.Size = new System.Drawing.Size(32, 17);
            this.rbCSCell.TabIndex = 0;
            this.rbCSCell.Text = "C";
            this.rbCSCell.UseVisualStyleBackColor = true;
            this.rbCSCell.Visible = false;
            // 
            // lblCPhoneT
            // 
            this.lblCPhoneT.AutoSize = true;
            this.lblCPhoneT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPhoneT.Location = new System.Drawing.Point(3, 5);
            this.lblCPhoneT.Name = "lblCPhoneT";
            this.lblCPhoneT.Size = new System.Drawing.Size(13, 13);
            this.lblCPhoneT.TabIndex = 172;
            this.lblCPhoneT.Text = "p";
            this.lblCPhoneT.Visible = false;
            // 
            // lblCPanel
            // 
            this.lblCPanel.AutoSize = true;
            this.lblCPanel.Location = new System.Drawing.Point(5, 5);
            this.lblCPanel.Name = "lblCPanel";
            this.lblCPanel.Size = new System.Drawing.Size(0, 13);
            this.lblCPanel.TabIndex = 177;
            this.lblCPanel.Visible = false;
            // 
            // lblCPatientID
            // 
            this.lblCPatientID.AutoSize = true;
            this.lblCPatientID.Location = new System.Drawing.Point(56, 6);
            this.lblCPatientID.Name = "lblCPatientID";
            this.lblCPatientID.Size = new System.Drawing.Size(68, 13);
            this.lblCPatientID.TabIndex = 0;
            this.lblCPatientID.Text = "lblCPatientID";
            this.lblCPatientID.Visible = false;
            // 
            // lblCSName
            // 
            this.lblCSName.AutoSize = true;
            this.lblCSName.Location = new System.Drawing.Point(142, 8);
            this.lblCSName.Name = "lblCSName";
            this.lblCSName.Size = new System.Drawing.Size(44, 13);
            this.lblCSName.TabIndex = 3;
            this.lblCSName.Text = "Name : ";
            // 
            // lblCTo
            // 
            this.lblCTo.AutoSize = true;
            this.lblCTo.Location = new System.Drawing.Point(707, 8);
            this.lblCTo.Name = "lblCTo";
            this.lblCTo.Size = new System.Drawing.Size(26, 13);
            this.lblCTo.TabIndex = 127;
            this.lblCTo.Text = "To :";
            // 
            // lblCFrom
            // 
            this.lblCFrom.AutoSize = true;
            this.lblCFrom.Location = new System.Drawing.Point(572, 8);
            this.lblCFrom.Name = "lblCFrom";
            this.lblCFrom.Size = new System.Drawing.Size(36, 13);
            this.lblCFrom.TabIndex = 128;
            this.lblCFrom.Text = "From :";
            // 
            // lblCSPRNO
            // 
            this.lblCSPRNO.AutoSize = true;
            this.lblCSPRNO.Location = new System.Drawing.Point(-3, 8);
            this.lblCSPRNO.Name = "lblCSPRNO";
            this.lblCSPRNO.Size = new System.Drawing.Size(50, 13);
            this.lblCSPRNO.TabIndex = 0;
            this.lblCSPRNO.Text = "MR No. :";
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip2.CanOverflow = false;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miRegistration,
            this.miHold,
            this.miConsulted});
            this.toolStrip2.Location = new System.Drawing.Point(-1, -2);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip2.Size = new System.Drawing.Size(283, 55);
            this.toolStrip2.TabIndex = 9;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // miRegistration
            // 
            this.miRegistration.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRegistration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRegistration.Enabled = false;
            this.miRegistration.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRegistration.ForeColor = System.Drawing.Color.Black;
            this.miRegistration.Image = ((System.Drawing.Image)(resources.GetObject("miRegistration.Image")));
            this.miRegistration.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRegistration.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRegistration.Name = "miRegistration";
            this.miRegistration.Size = new System.Drawing.Size(81, 52);
            this.miRegistration.Text = "Registration";
            this.miRegistration.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRegistration.ToolTipText = "Select New Patient or Registered Patient";
            this.miRegistration.Click += new System.EventHandler(this.miRegistration_Click);
            this.miRegistration.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mi_MouseDown);
            // 
            // miHold
            // 
            this.miHold.BackColor = System.Drawing.Color.LightSlateGray;
            this.miHold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miHold.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miHold.ForeColor = System.Drawing.Color.Black;
            this.miHold.Image = ((System.Drawing.Image)(resources.GetObject("miHold.Image")));
            this.miHold.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miHold.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miHold.ImageTransparentColor = System.Drawing.Color.White;
            this.miHold.Name = "miHold";
            this.miHold.Size = new System.Drawing.Size(75, 52);
            this.miHold.Text = "Hold Queue";
            this.miHold.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miHold.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miHold.ToolTipText = "Click to see Patient on Hold";
            this.miHold.Click += new System.EventHandler(this.miHold_Click);
            this.miHold.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mi_MouseDown);
            // 
            // miConsulted
            // 
            this.miConsulted.BackColor = System.Drawing.Color.LightSlateGray;
            this.miConsulted.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miConsulted.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miConsulted.ForeColor = System.Drawing.Color.Black;
            this.miConsulted.Image = ((System.Drawing.Image)(resources.GetObject("miConsulted.Image")));
            this.miConsulted.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miConsulted.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miConsulted.Name = "miConsulted";
            this.miConsulted.Size = new System.Drawing.Size(67, 52);
            this.miConsulted.Text = "Consulted";
            this.miConsulted.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miConsulted.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miConsulted.ToolTipText = "Click to see Consulted Patient";
            this.miConsulted.Click += new System.EventHandler(this.miConsulted_Click);
            this.miConsulted.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mi_MouseDown);
            // 
            // pnlHead
            // 
            this.pnlHead.BackColor = System.Drawing.Color.DarkGray;
            this.pnlHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.lblTime);
            this.pnlHead.Controls.Add(this.toolStrip2);
            this.pnlHead.Controls.Add(this.toolStrip1);
            this.pnlHead.Location = new System.Drawing.Point(2, 1);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(1012, 54);
            this.pnlHead.TabIndex = 139;
            this.pnlHead.Tag = "top";
            // 
            // lblTime
            // 
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblTime.Location = new System.Drawing.Point(284, 4);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(121, 45);
            this.lblTime.TabIndex = 13;
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlSerachByPRNo
            // 
            this.pnlSerachByPRNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSerachByPRNo.Controls.Add(this.button14);
            this.pnlSerachByPRNo.Controls.Add(this.button13);
            this.pnlSerachByPRNo.Controls.Add(this.btnRegNewPateint);
            this.pnlSerachByPRNo.Controls.Add(this.btnSearchPrnoClose);
            this.pnlSerachByPRNo.Controls.Add(this.lblPRNo);
            this.pnlSerachByPRNo.Controls.Add(this.txtSearchByPRNo);
            this.pnlSerachByPRNo.Location = new System.Drawing.Point(395, 278);
            this.pnlSerachByPRNo.Name = "pnlSerachByPRNo";
            this.pnlSerachByPRNo.Size = new System.Drawing.Size(295, 103);
            this.pnlSerachByPRNo.TabIndex = 142;
            // 
            // btnRegNewPateint
            // 
            this.btnRegNewPateint.Location = new System.Drawing.Point(26, 66);
            this.btnRegNewPateint.Name = "btnRegNewPateint";
            this.btnRegNewPateint.Size = new System.Drawing.Size(174, 23);
            this.btnRegNewPateint.TabIndex = 183;
            this.btnRegNewPateint.Text = "Register New Patient";
            this.btnRegNewPateint.UseVisualStyleBackColor = true;
            this.btnRegNewPateint.Click += new System.EventHandler(this.btnRegNewPateint_Click);
            // 
            // btnSearchPrnoClose
            // 
            this.btnSearchPrnoClose.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchPrnoClose.Image")));
            this.btnSearchPrnoClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSearchPrnoClose.Location = new System.Drawing.Point(206, 34);
            this.btnSearchPrnoClose.Name = "btnSearchPrnoClose";
            this.btnSearchPrnoClose.Size = new System.Drawing.Size(52, 49);
            this.btnSearchPrnoClose.TabIndex = 182;
            this.btnSearchPrnoClose.Text = "Close";
            this.btnSearchPrnoClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSearchPrnoClose.UseVisualStyleBackColor = true;
            this.btnSearchPrnoClose.Click += new System.EventHandler(this.btnSearchPrnoClose_Click);
            // 
            // lblPRNo
            // 
            this.lblPRNo.AutoSize = true;
            this.lblPRNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPRNo.Location = new System.Drawing.Point(30, 13);
            this.lblPRNo.Name = "lblPRNo";
            this.lblPRNo.Size = new System.Drawing.Size(103, 13);
            this.lblPRNo.TabIndex = 180;
            this.lblPRNo.Text = "Search Patient : ";
            // 
            // timerVoice
            // 
            this.timerVoice.Enabled = true;
            this.timerVoice.Interval = 1000;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.FillWeight = 5F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::OPD.Properties.Resources.NA1;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 132;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.FillWeight = 5F;
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = global::OPD.Properties.Resources.NA1;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Width = 13;
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.FillWeight = 5F;
            this.dataGridViewImageColumn3.HeaderText = "";
            this.dataGridViewImageColumn3.Image = global::OPD.Properties.Resources.NA1;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.Width = 13;
            // 
            // dataGridViewImageColumn4
            // 
            this.dataGridViewImageColumn4.FillWeight = 5F;
            this.dataGridViewImageColumn4.HeaderText = "";
            this.dataGridViewImageColumn4.Image = global::OPD.Properties.Resources.NA1;
            this.dataGridViewImageColumn4.Name = "dataGridViewImageColumn4";
            this.dataGridViewImageColumn4.Width = 13;
            // 
            // pnlAppointment
            // 
            this.pnlAppointment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAppointment.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlAppointment.BackgroundImage = global::OPD.Properties.Resources.appointments;
            this.pnlAppointment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlAppointment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAppointment.Controls.Add(this.dgAppoinment);
            this.pnlAppointment.Controls.Add(this.mcAppointment);
            this.pnlAppointment.Controls.Add(this.btnAppointControl);
            this.pnlAppointment.Controls.Add(this.lblAppointment);
            this.pnlAppointment.Controls.Add(this.btnAppointment);
            this.pnlAppointment.Controls.Add(this.label14);
            this.pnlAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlAppointment.Location = new System.Drawing.Point(817, 216);
            this.pnlAppointment.Name = "pnlAppointment";
            this.pnlAppointment.Size = new System.Drawing.Size(195, 24);
            this.pnlAppointment.TabIndex = 141;
            this.pnlAppointment.Tag = "med";
            this.pnlAppointment.Visible = false;
            this.pnlAppointment.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAppointment_Paint);
            // 
            // dgAppoinment
            // 
            this.dgAppoinment.AllowUserToAddRows = false;
            this.dgAppoinment.AllowUserToDeleteRows = false;
            this.dgAppoinment.AllowUserToResizeColumns = false;
            this.dgAppoinment.AllowUserToResizeRows = false;
            this.dgAppoinment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle144.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle144.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle144.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle144.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle144.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle144.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAppoinment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle144;
            this.dgAppoinment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAppoinment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TokkenNo,
            this.AppPRNO,
            this.AppPName,
            this.AppoinmentID,
            this.AppointmentDate,
            this.PersonID,
            this.AppOPDID,
            this.AppPatientID,
            this.AppVitalSign});
            dataGridViewCellStyle146.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle146.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle146.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle146.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle146.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle146.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgAppoinment.DefaultCellStyle = dataGridViewCellStyle146;
            this.dgAppoinment.EnableHeadersVisualStyles = false;
            this.dgAppoinment.Location = new System.Drawing.Point(274, 32);
            this.dgAppoinment.MultiSelect = false;
            this.dgAppoinment.Name = "dgAppoinment";
            this.dgAppoinment.ReadOnly = true;
            dataGridViewCellStyle147.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle147.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle147.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle147.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle147.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle147.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle147.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAppoinment.RowHeadersDefaultCellStyle = dataGridViewCellStyle147;
            this.dgAppoinment.RowHeadersVisible = false;
            this.dgAppoinment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgAppoinment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAppoinment.Size = new System.Drawing.Size(236, 424);
            this.dgAppoinment.TabIndex = 14;
            // 
            // TokkenNo
            // 
            this.TokkenNo.DataPropertyName = "TokkenNo";
            dataGridViewCellStyle145.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TokkenNo.DefaultCellStyle = dataGridViewCellStyle145;
            this.TokkenNo.FillWeight = 10F;
            this.TokkenNo.HeaderText = "No.";
            this.TokkenNo.Name = "TokkenNo";
            this.TokkenNo.ReadOnly = true;
            // 
            // AppPRNO
            // 
            this.AppPRNO.DataPropertyName = "PRNO";
            this.AppPRNO.FillWeight = 40F;
            this.AppPRNO.HeaderText = "PR No.";
            this.AppPRNO.Name = "AppPRNO";
            this.AppPRNO.ReadOnly = true;
            // 
            // AppPName
            // 
            this.AppPName.DataPropertyName = "Name";
            this.AppPName.FillWeight = 50F;
            this.AppPName.HeaderText = "Name";
            this.AppPName.Name = "AppPName";
            this.AppPName.ReadOnly = true;
            // 
            // AppoinmentID
            // 
            this.AppoinmentID.DataPropertyName = "AppointmentID";
            this.AppoinmentID.HeaderText = "AppoinmentID";
            this.AppoinmentID.Name = "AppoinmentID";
            this.AppoinmentID.ReadOnly = true;
            this.AppoinmentID.Visible = false;
            // 
            // AppointmentDate
            // 
            this.AppointmentDate.DataPropertyName = "AppointmentDate";
            this.AppointmentDate.HeaderText = "AppointmentDate";
            this.AppointmentDate.Name = "AppointmentDate";
            this.AppointmentDate.ReadOnly = true;
            this.AppointmentDate.Visible = false;
            // 
            // PersonID
            // 
            this.PersonID.DataPropertyName = "PersonID";
            this.PersonID.HeaderText = "PersonID";
            this.PersonID.Name = "PersonID";
            this.PersonID.ReadOnly = true;
            this.PersonID.Visible = false;
            // 
            // AppOPDID
            // 
            this.AppOPDID.DataPropertyName = "OPDID";
            this.AppOPDID.HeaderText = "OPDID";
            this.AppOPDID.Name = "AppOPDID";
            this.AppOPDID.ReadOnly = true;
            this.AppOPDID.Visible = false;
            // 
            // AppPatientID
            // 
            this.AppPatientID.DataPropertyName = "PatientID";
            this.AppPatientID.HeaderText = "Patient";
            this.AppPatientID.Name = "AppPatientID";
            this.AppPatientID.ReadOnly = true;
            this.AppPatientID.Visible = false;
            // 
            // AppVitalSign
            // 
            this.AppVitalSign.DataPropertyName = "vitalsign";
            this.AppVitalSign.HeaderText = "AppVitalSign";
            this.AppVitalSign.Name = "AppVitalSign";
            this.AppVitalSign.ReadOnly = true;
            this.AppVitalSign.Visible = false;
            // 
            // mcAppointment
            // 
            this.mcAppointment.ActiveMonth.Month = 1;
            this.mcAppointment.ActiveMonth.Year = 2020;
            this.mcAppointment.Culture = new System.Globalization.CultureInfo("en-US");
            this.mcAppointment.Footer.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Footer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.mcAppointment.Footer.Format = Pabo.Calendar.mcTodayFormat.Long;
            this.mcAppointment.Header.BackColor1 = System.Drawing.Color.RoyalBlue;
            this.mcAppointment.Header.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Header.TextColor = System.Drawing.Color.White;
            this.mcAppointment.ImageList = null;
            this.mcAppointment.Location = new System.Drawing.Point(-3, 29);
            this.mcAppointment.MaxDate = new System.DateTime(2060, 1, 28, 0, 0, 0, 0);
            this.mcAppointment.MinDate = new System.DateTime(2020, 1, 1, 0, 0, 0, 0);
            this.mcAppointment.Month.BackgroundImage = null;
            this.mcAppointment.Month.BorderStyles.Normal = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.mcAppointment.Month.Colors.BackColor1 = System.Drawing.Color.Lavender;
            this.mcAppointment.Month.Colors.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.mcAppointment.Month.Colors.Days.BackColor1 = System.Drawing.Color.Azure;
            this.mcAppointment.Month.Colors.Disabled.BackColor1 = System.Drawing.Color.Silver;
            this.mcAppointment.Month.Colors.Disabled.BackColor2 = System.Drawing.Color.DarkGray;
            this.mcAppointment.Month.Colors.Selected.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.mcAppointment.Month.Colors.Weekend.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Month.Colors.Weekend.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.Month.DateAlign = Pabo.Calendar.mcItemAlign.TopRight;
            this.mcAppointment.Month.DateFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.mcAppointment.Month.TextAlign = Pabo.Calendar.mcItemAlign.Center;
            this.mcAppointment.Month.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.mcAppointment.Name = "mcAppointment";
            this.mcAppointment.SelectionMode = Pabo.Calendar.mcSelectionMode.One;
            this.mcAppointment.SelectTrailingDates = false;
            this.mcAppointment.ShowTrailingDates = false;
            this.mcAppointment.Size = new System.Drawing.Size(249, 363);
            this.mcAppointment.TabIndex = 15;
            this.mcAppointment.TabStop = false;
            this.mcAppointment.Weekdays.BackColor1 = System.Drawing.Color.LightSteelBlue;
            this.mcAppointment.Weekdays.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Weekdays.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.Weekdays.TextColor = System.Drawing.Color.Black;
            this.mcAppointment.Weeknumbers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcAppointment.Weeknumbers.GradientMode = Pabo.Calendar.mcGradientMode.Vertical;
            this.mcAppointment.MonthChanged += new Pabo.Calendar.MonthChangedEventHandler(this.mcAppointment_MonthChanged);
            this.mcAppointment.DaySelected += new Pabo.Calendar.DaySelectedEventHandler(this.mcAppointment_DaySelected);
            // 
            // btnAppointControl
            // 
            this.btnAppointControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAppointControl.BackgroundImage")));
            this.btnAppointControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAppointControl.Location = new System.Drawing.Point(169, 0);
            this.btnAppointControl.Name = "btnAppointControl";
            this.btnAppointControl.Size = new System.Drawing.Size(22, 22);
            this.btnAppointControl.TabIndex = 13;
            this.btnAppointControl.Tag = "1";
            this.btnAppointControl.UseVisualStyleBackColor = true;
            this.btnAppointControl.Click += new System.EventHandler(this.btnAppointControl_Click);
            // 
            // lblAppointment
            // 
            this.lblAppointment.AutoSize = true;
            this.lblAppointment.BackColor = System.Drawing.Color.Transparent;
            this.lblAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppointment.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblAppointment.Location = new System.Drawing.Point(45, 4);
            this.lblAppointment.Name = "lblAppointment";
            this.lblAppointment.Size = new System.Drawing.Size(113, 13);
            this.lblAppointment.TabIndex = 8;
            this.lblAppointment.Tag = "transdisplay";
            this.lblAppointment.Text = "Appointment  3 / 8";
            this.lblAppointment.Click += new System.EventHandler(this.lblAppointment_Click);
            // 
            // btnAppointment
            // 
            this.btnAppointment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAppointment.BackgroundImage")));
            this.btnAppointment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAppointment.Location = new System.Drawing.Point(193, 0);
            this.btnAppointment.Name = "btnAppointment";
            this.btnAppointment.Size = new System.Drawing.Size(28, 28);
            this.btnAppointment.TabIndex = 6;
            this.btnAppointment.UseVisualStyleBackColor = true;
            this.btnAppointment.Click += new System.EventHandler(this.btnAppointment_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(271, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 13);
            this.label14.TabIndex = 11;
            this.label14.Tag = "display";
            this.label14.Text = "Token No.      PR No. ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmOPD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 706);
            this.Controls.Add(this.pnlAppointment);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblOPDID);
            this.Controls.Add(this.lblPatientID);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlSerachByPRNo);
            this.Controls.Add(this.pnlConsult);
            this.Controls.Add(this.pnlVitalSigns);
            this.Controls.Add(this.pnlHold);
            this.Controls.Add(this.pnlRegister);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmOPD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HAISAM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOPD_FormClosing);
            this.Load += new System.EventHandler(this.frmOPD_Load);
            this.VisibleChanged += new System.EventHandler(this.frmOPD_VisibleChanged);
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.pnlNotes.ResumeLayout(false);
            this.pnlNotes.PerformLayout();
            this.pnlAllerAR.ResumeLayout(false);
            this.pnlAllerAR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAllergies)).EndInit();
            this.pnlProcAdmission.ResumeLayout(false);
            this.pnlProcAdmission.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdmission)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProcedure)).EndInit();
            this.pnlTotalVisit.ResumeLayout(false);
            this.pnlTotalVisit.PerformLayout();
            this.pnlSelPatientDetail.ResumeLayout(false);
            this.pnlSelPatientDetail.PerformLayout();
            this.pnlSelPatientHead.ResumeLayout(false);
            this.pnlSelPatientHead.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlPayment.ResumeLayout(false);
            this.pnlPayment.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.pnlMedHist.ResumeLayout(false);
            this.pnlMedHist.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlVitalSignHist.ResumeLayout(false);
            this.pnlVitalSignHist.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgComplaints)).EndInit();
            this.cmsComplaints.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvestigation)).EndInit();
            this.cmsInvestigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMedicine)).EndInit();
            this.cmsMed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPlans)).EndInit();
            this.cmsPlans.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagnosis)).EndInit();
            this.cmsDiagnosis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHistory)).EndInit();
            this.cmsHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSigns)).EndInit();
            this.cmsSigns.ResumeLayout(false);
            this.pnlMedHistGrid.ResumeLayout(false);
            this.pnlMedHistGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMedHist)).EndInit();
            this.pnlDGList.ResumeLayout(false);
            this.pnlDGList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.cmsItem.ResumeLayout(false);
            this.pnlVitalSigns.ResumeLayout(false);
            this.pnlVitalSigns.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.pnlPatientGender.ResumeLayout(false);
            this.pnlPatientGender.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlRegister.ResumeLayout(false);
            this.pnlRegister.PerformLayout();
            this.pnlLegend.ResumeLayout(false);
            this.pnlLegend.PerformLayout();
            this.pnlRPatient.ResumeLayout(false);
            this.pnlRPatient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRegisterPatient)).EndInit();
            this.pnlRegisterSearch.ResumeLayout(false);
            this.pnlRegisterSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPatient)).EndInit();
            this.pnlHold.ResumeLayout(false);
            this.pnlHold.PerformLayout();
            this.pnlHPatient.ResumeLayout(false);
            this.pnlHPatient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgHold)).EndInit();
            this.pnlHoldSearch.ResumeLayout(false);
            this.pnlHoldSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHold)).EndInit();
            this.pnlConsult.ResumeLayout(false);
            this.pnlConsult.PerformLayout();
            this.pnlCPatient.ResumeLayout(false);
            this.pnlCPatient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConsulted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsulted)).EndInit();
            this.pnlConsultedSearch.ResumeLayout(false);
            this.pnlConsultedSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlSerachByPRNo.ResumeLayout(false);
            this.pnlSerachByPRNo.PerformLayout();
            this.pnlAppointment.ResumeLayout(false);
            this.pnlAppointment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppoinment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOPDID;
        private System.Windows.Forms.Label lblPatientID;
        private System.Windows.Forms.MaskedTextBox txtPRNO;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlDGList;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnListPnl;
        private System.Windows.Forms.DataGridView dgList;
        private System.Windows.Forms.Label SavePatient;
        private System.Windows.Forms.Button btnSigns;
        private System.Windows.Forms.Button btnPlans;
        private System.Windows.Forms.Button btnInvestigation;
        private System.Windows.Forms.Button btnDiagnosis;
        private System.Windows.Forms.Button btnPresentComplaint;
        private System.Windows.Forms.Button btnHistory;
        private System.Windows.Forms.Button btnMedicine;
        private System.Windows.Forms.DataGridView dgInvestigation;
        private System.Windows.Forms.DataGridView dgMedicine;
        private System.Windows.Forms.DataGridView dgDiagnosis;
        private System.Windows.Forms.DataGridView dgComplaints;
        private System.Windows.Forms.DataGridView dgHistory;
        private System.Windows.Forms.DataGridView dgSigns;
        private System.Windows.Forms.Panel pnlVitalSigns;
        private System.Windows.Forms.TextBox txtBSA;
        private System.Windows.Forms.TextBox txtBMI;
        private System.Windows.Forms.Label lblBSA;
        private System.Windows.Forms.Label lblBMI;
        private System.Windows.Forms.MaskedTextBox txtBP;
        private System.Windows.Forms.ComboBox cboHeightUnit;
        private System.Windows.Forms.ComboBox cboWeightUnit;
        private System.Windows.Forms.ComboBox cboTempUnit;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.TextBox txtPulse;
        private System.Windows.Forms.TextBox txtTemp;
       // private System.Windows.Forms.TextBox txtHeadCircum;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblWeight;
       // private System.Windows.Forms.Label lblHeadCircum;
       // private System.Windows.Forms.Label lblHeadCircumUnit;
        private System.Windows.Forms.Label lblPulseUnit;
        private System.Windows.Forms.Label lblBPUnit;
        private System.Windows.Forms.Label lblPulse;
        private System.Windows.Forms.Label lblBP;
        private System.Windows.Forms.Label lblTemp;
        private System.Windows.Forms.Label lblVitalSigns;
        private System.Windows.Forms.ContextMenuStrip cmsComplaints;
        private System.Windows.Forms.ToolStripMenuItem miComplaintsRename;
        private System.Windows.Forms.ContextMenuStrip cmsHistory;
        private System.Windows.Forms.ToolStripMenuItem miHistoryRename;
        private System.Windows.Forms.ContextMenuStrip cmsDiagnosis;
        private System.Windows.Forms.ToolStripMenuItem miDiagnosisRename;
        private System.Windows.Forms.ContextMenuStrip cmsInvestigation;
        private System.Windows.Forms.ToolStripMenuItem miInvestigationRename;
        private System.Windows.Forms.ContextMenuStrip cmsPlans;
        private System.Windows.Forms.ToolStripMenuItem miPlansRename;
        private System.Windows.Forms.ContextMenuStrip cmsSigns;
        private System.Windows.Forms.ToolStripMenuItem miSignsRename;
        private System.Windows.Forms.ToolStripMenuItem miComplaintDelete;
        private System.Windows.Forms.ToolStripMenuItem miHistoryDelete;
        private System.Windows.Forms.ToolStripMenuItem miDiagnosisDelete;
        private System.Windows.Forms.ToolStripMenuItem miPlansDelete;
        private System.Windows.Forms.ToolStripMenuItem miSignsDelete;
        private System.Windows.Forms.ToolStripMenuItem miInvestigationDelete;
        private System.Windows.Forms.Label lblBMIUnit;
        private System.Windows.Forms.TextBox txtBSAStatus;
        private System.Windows.Forms.TextBox txtBMIStatus;
        private System.Windows.Forms.Label lblBSAStatus;
        private System.Windows.Forms.Label lblBMIStatus;
        private System.Windows.Forms.Label lblBSAUnit;
        private System.Windows.Forms.DateTimePicker dtpFollowUpDate;
        private System.Windows.Forms.Label lblFollowUpDate;
        private System.Windows.Forms.Panel pnlVisit;
        private System.Windows.Forms.ToolStripButton miPicture;
        private System.Windows.Forms.ToolStripButton miVideo;
        private System.Windows.Forms.ToolStripButton miHoldSave;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton miConsultedSave;
        private System.Windows.Forms.ToolStripButton miEmail;
        private System.Windows.Forms.ToolStripButton miSMS;
        private System.Windows.Forms.ToolStripButton miVideoCall;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.TextBox txtPayment;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton miCancel;
        private System.Windows.Forms.Button btnPreviousVisit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTotalPatient;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlRegister;
        private System.Windows.Forms.Button btnRegisterPatient;
        private System.Windows.Forms.Button btnNewPatient;
        private System.Windows.Forms.DataGridView dgRegisterPatient;
        private System.Windows.Forms.Panel pnlRegisterSearch;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Panel panel11;
       // private System.Windows.Forms.RadioButton rbSPhone;
        //private System.Windows.Forms.RadioButton rbSCell;
        private System.Windows.Forms.Label rbSCell;
        private System.Windows.Forms.MaskedTextBox txtSearchPRNO;
        private System.Windows.Forms.TextBox txtSearchPhone;
        private System.Windows.Forms.TextBox txtSearchName;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Button btnPatientSearch;
        private System.Windows.Forms.Label lblSearchPRNO;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.PictureBox pbPatient;
        private System.Windows.Forms.Panel pnlHold;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtHName;
        private System.Windows.Forms.DataGridView dgHold;
        private System.Windows.Forms.ComboBox cboHCity;
        private System.Windows.Forms.Label lblHCity;
        private System.Windows.Forms.Panel pnlHoldSearch;
        private System.Windows.Forms.DateTimePicker dtpHFrom;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton rbHSPhone;
        private System.Windows.Forms.RadioButton rbHSCell;
        private System.Windows.Forms.DateTimePicker dtpHTo;
        private System.Windows.Forms.MaskedTextBox txtHSPRNO;
        private System.Windows.Forms.Label lblHFrom;
        private System.Windows.Forms.TextBox txtHSPhone;
        private System.Windows.Forms.TextBox txtHSName;
        private System.Windows.Forms.Label lblHSName;
        private System.Windows.Forms.Button btnHSearch;
        private System.Windows.Forms.Label lblHSPRNO;
        private System.Windows.Forms.Label lblHTo;
        private System.Windows.Forms.Button btnHoldUpdate;
        private System.Windows.Forms.PictureBox pbHold;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton rbHFemale;
        private System.Windows.Forms.RadioButton rbHMale;
        private System.Windows.Forms.Label lblHAddress;
        private System.Windows.Forms.Label lblHCell;
        private System.Windows.Forms.Label lblHPhone;
        private System.Windows.Forms.Label lblHGender;
        private System.Windows.Forms.Label lblHAge;
        private System.Windows.Forms.Label lblHDOB;
        private System.Windows.Forms.Label lblHName;
        private System.Windows.Forms.TextBox txtHAddress;
        private System.Windows.Forms.TextBox txtHCell;
        private System.Windows.Forms.TextBox txtHPhone;
        private System.Windows.Forms.TextBox txtHAge;
        private System.Windows.Forms.DateTimePicker dtpHDOB;
        private System.Windows.Forms.Label lblHPatientID;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel pnlConsult;
        private System.Windows.Forms.Label lblCPhoneT;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtCName;
        private System.Windows.Forms.ComboBox cboCCity;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton rbCFemale;
        private System.Windows.Forms.RadioButton rbCMale;
        private System.Windows.Forms.TextBox txtCAddress;
        private System.Windows.Forms.TextBox txtCCell;
        private System.Windows.Forms.Label lblCCityT;
        private System.Windows.Forms.Label lblCAddressT;
        private System.Windows.Forms.TextBox txtCPhone;
        private System.Windows.Forms.Label lblCCellT;
        private System.Windows.Forms.Label lblCGenderT;
        private System.Windows.Forms.Label lblCAgeT;
        private System.Windows.Forms.Label lblCDOBT;
        private System.Windows.Forms.Label lblCNameT;
        private System.Windows.Forms.TextBox txtCAge;
        private System.Windows.Forms.DateTimePicker dtpCDOB;
        private System.Windows.Forms.DataGridView dgConsulted;
        private System.Windows.Forms.Panel pnlConsultedSearch;
        private System.Windows.Forms.DateTimePicker dtpCTo;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.RadioButton rbCSPhone;
        private System.Windows.Forms.RadioButton rbCSCell;
        private System.Windows.Forms.DateTimePicker dtpCFrom;
        private System.Windows.Forms.MaskedTextBox txtCSPRNO;
        private System.Windows.Forms.Label lblCFrom;
        private System.Windows.Forms.TextBox txtCSPhone;
        private System.Windows.Forms.TextBox txtCSName;
        private System.Windows.Forms.Label lblCSName;
        private System.Windows.Forms.Button btnConsultedSearch;
        private System.Windows.Forms.Label lblCSPRNO;
        private System.Windows.Forms.Label lblCPatientID;
        private System.Windows.Forms.Label lblCTo;
        private System.Windows.Forms.PictureBox pbConsulted;
        private System.Windows.Forms.Button btnCUpdate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label lblSelect;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton miRegistration;
        private System.Windows.Forms.ToolStripButton miHold;
        private System.Windows.Forms.ToolStripButton miConsulted;
        private System.Windows.Forms.Panel pnlRPatient;
        private System.Windows.Forms.TextBox txtPatientPhone;
        private System.Windows.Forms.Panel pnlPatientGender;
        private System.Windows.Forms.RadioButton rbPatientFemale;
        private System.Windows.Forms.RadioButton rbPatientMale;
        private System.Windows.Forms.Label lblPatientCity;
        private System.Windows.Forms.MaskedTextBox txtRPRNo;
        private System.Windows.Forms.Label lblPatientCell;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblRDOB;
        private System.Windows.Forms.TextBox txtPatientAddress;
        private System.Windows.Forms.Button btnPatientSave;
        private System.Windows.Forms.Label lblPatientAge;
        private System.Windows.Forms.Label lblPatientAddress;
        private System.Windows.Forms.ComboBox cboPatientCity;
        private System.Windows.Forms.DateTimePicker dtpPatientDOB;
        private System.Windows.Forms.Label lblPatientGender;
        private System.Windows.Forms.TextBox txtPatientName;
        private System.Windows.Forms.TextBox txtPatientCell;
        private System.Windows.Forms.TextBox txtPatientAge;
        private System.Windows.Forms.Label lblRPRNo;
        private System.Windows.Forms.Label lblPatientPhone;
        private System.Windows.Forms.Label lblPatientName;
        private System.Windows.Forms.Panel pnlCPatient;
        private System.Windows.Forms.MaskedTextBox txtCPRNo;
        private System.Windows.Forms.Label lblCPRNo;
        private System.Windows.Forms.Panel pnlHPatient;
        private System.Windows.Forms.MaskedTextBox txtHPRNo;
        private System.Windows.Forms.Label lblHPRNo;
        private System.Windows.Forms.LinkLabel lklTdaySummary;
        private System.Windows.Forms.Panel pnlLegend;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnNews;
        private System.Windows.Forms.LinkLabel lkbPayRep;
        private System.Windows.Forms.ToolStripButton miPrint;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtRegEmail;
        private System.Windows.Forms.Label lblRegEmail;
        private System.Windows.Forms.TextBox txtCEmail;
        private System.Windows.Forms.Label lblCEmail;
        private System.Windows.Forms.TextBox txtHEmail;
        private System.Windows.Forms.Label lblHEmail;
        private System.Windows.Forms.Label lblCity;
        //private System.Windows.Forms.ComboBox cboRPanel;
        //private System.Windows.Forms.Label lblRegPanel;
        private System.Windows.Forms.Label lblCPanel;
        private System.Windows.Forms.Label lblHPanel;
        private System.Windows.Forms.ComboBox cboHPanel;
        private System.Windows.Forms.ComboBox cboCPanel;
        private System.Windows.Forms.ToolTip ttOPD;
        private System.Windows.Forms.Panel pnlPayment;
        private System.Windows.Forms.Panel pnlWaitingList;
        private System.Windows.Forms.ContextMenuStrip cmsItem;
        private System.Windows.Forms.ToolStripMenuItem miItemDelete;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.ToolStripButton miDoc;
        private System.Windows.Forms.ToolStripDropDownButton miControlePnl;
        private System.Windows.Forms.ToolStripMenuItem miNewAccount;
        private System.Windows.Forms.ToolStripMenuItem miInstructions;
        private System.Windows.Forms.ToolStripMenuItem miChangePassword;
        private System.Windows.Forms.ToolStripMenuItem miPanelInfo;
        private System.Windows.Forms.ToolStripMenuItem dataAnalysisToolStripMenuItem;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnHoldSerach;
        private System.Windows.Forms.Button btnConsultedSerach;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblCPatientCount;
        private System.Windows.Forms.Label lblHoldCount;
        private System.Windows.Forms.Label lblRegCount;
        private System.Windows.Forms.Label lblConsultCount;
        private System.Windows.Forms.LinkLabel llblRefershPatient;
        private System.Windows.Forms.Panel pnlMedHistGrid;
        private System.Windows.Forms.Button btnMedHistClose;
        private System.Windows.Forms.Label lblHeadingMedHist;
        private System.Windows.Forms.DataGridView dgMedHist;
        private System.Windows.Forms.Button btnVitalSignHist;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel pnlMedHist;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblWaitingQueue;
        private System.Windows.Forms.Panel pnlTotalVisit;
        private System.Windows.Forms.Label lblVisit;
        private System.Windows.Forms.Label lblVisitCount;
        private System.Windows.Forms.Panel pnlAppointment;
        private System.Windows.Forms.DataGridView dgAppoinment;
        private System.Windows.Forms.Button btnAppointControl;
        private System.Windows.Forms.Button btnAppointment;
        private System.Windows.Forms.Label lblAppointment;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem miDosageInstruction;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblPayment;
        private System.Windows.Forms.ToolStripMenuItem backupDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreDatabaseToolStripMenuItem;
        private System.Windows.Forms.LinkLabel llblSearchByPRNO;
        private System.Windows.Forms.Panel pnlSerachByPRNo;
        private System.Windows.Forms.Label lblPRNo;
        private System.Windows.Forms.MaskedTextBox txtSearchByPRNo;
        private System.Windows.Forms.Button btnSearchPrnoClose;
        private System.Windows.Forms.Button btnRegNewPateint;
        private System.Windows.Forms.ToolStripMenuItem miOption;
        private System.Windows.Forms.DataGridViewTextBoxColumn MHVisitDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MHOPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MHMedicineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MHMedName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MHDosage;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CSNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn COPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEnteredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn CDOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn CVisitDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn CCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn CCityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn HEnteredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn HDOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn HAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoldVisitDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn HCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn HEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn HCityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RSNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn RPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn DOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn RAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityname;
        private System.Windows.Forms.Panel pnlNotes;
        private System.Windows.Forms.Button btnStickyNotes;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.TextBox txtNotes;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvVitalSign;  // there is error while logout the application
        private System.Windows.Forms.ToolStripMenuItem tsmiMonthlySummary;
        private System.Windows.Forms.ToolStripMenuItem miAddDisease;
        private Pabo.Calendar.MonthCalendar mcAppointment;
        private System.Windows.Forms.LinkLabel llblDailyCashReport;
        private System.Windows.Forms.Panel pnlSelPatientHead;
        private System.Windows.Forms.Label lblPatientDetail;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel pnlSelPatientDetail;
        private System.Windows.Forms.Label lblPatientVideoCount;
        private System.Windows.Forms.Label lblPatientDocCount;
        private System.Windows.Forms.Label lblPatientPicCount;
        private System.Windows.Forms.ToolStripMenuItem miHistoryStatus;
        private System.Windows.Forms.ToolStripMenuItem miDiagnosisStatus;
        private System.Windows.Forms.ToolStripMenuItem miCompStatus;
        private System.Windows.Forms.ToolStripMenuItem miInvestStatus;
        private System.Windows.Forms.ToolStripMenuItem miPlanStatus;
        private System.Windows.Forms.ToolStripMenuItem miSignStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvestigatioName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDInvestigation;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelInvest;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelHist;
        private System.Windows.Forms.DataGridViewImageColumn ImgHistAvail;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistDiseaseIDRef;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComplaintsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDComplaints;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelComp;
        private System.Windows.Forms.DataGridViewImageColumn CompImgDisease;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompDiseaseIDRef;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiagnosisName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDDiagnosis;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelDiagnosis;
        private System.Windows.Forms.DataGridViewImageColumn DiagnoImgDisease;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiagnosisDiseaseRefID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SignsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDSigns;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelSign;
        private System.Windows.Forms.DataGridViewImageColumn SignImgDisease;
        private System.Windows.Forms.DataGridViewTextBoxColumn SignDiseaseIDRef;
        private System.Windows.Forms.DataGridViewTextBoxColumn PName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ListDisRefID;
        private System.Windows.Forms.ToolStripMenuItem miComplaintICDMoreInfo;
        private System.Windows.Forms.ToolStripMenuItem miHistICDMoreInfo;
        private System.Windows.Forms.ToolStripMenuItem miDiagnosisICDMoreInfo;
        private System.Windows.Forms.ToolStripMenuItem miSignsICDMoreInfo;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn4;
        private System.Windows.Forms.Panel pnlAllerAR;
        private System.Windows.Forms.Button btnAllergARExpend;
        private System.Windows.Forms.Label lblAllergARTabHeading;
        private System.Windows.Forms.DataGridView dgAR;
        private System.Windows.Forms.DataGridView dgAllergies;
        private System.Windows.Forms.ContextMenuStrip cmsMed;
        private System.Windows.Forms.ToolStripMenuItem miAllergies;
        private System.Windows.Forms.ToolStripMenuItem miAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllergEnteredON;
        private System.Windows.Forms.DataGridViewTextBoxColumn Allergies;
        private System.Windows.Forms.Button btnLabsTest;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRespiratoryRate;
        private System.Windows.Forms.TextBox txtHeartRate;
        private System.Windows.Forms.Label lblRespiratoryRate;
        private System.Windows.Forms.Label lblHeartRate;
        private System.Windows.Forms.Panel pnlProcAdmission;
        private System.Windows.Forms.Label lblAdmissionProcedure;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel pnlInstructions;
        private System.Windows.Forms.Button btnProcAdmissionExp;
        private System.Windows.Forms.DataGridView dgAdmission;
        private System.Windows.Forms.DataGridView dgProcedure;
        private System.Windows.Forms.DataGridViewTextBoxColumn AREnteredON;
        private System.Windows.Forms.DataGridViewTextBoxColumn AR;
        private System.Windows.Forms.Button btnAddAdmProc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcEnteredOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn procedure;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdmEnteredOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Admission;
        private System.Windows.Forms.DataGridView dgPlans;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlansName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrefIDPlans;
        private System.Windows.Forms.DataGridViewTextBoxColumn DelPlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn TokkenNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppPRNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppPName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppoinmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppointmentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppOPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppPatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AppVitalSign;
        private System.Windows.Forms.Button btnNotesDetail;
        private System.Windows.Forms.Label lblTextNotes;
        private System.Windows.Forms.Timer timerVoice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Medname;
        private System.Windows.Forms.DataGridViewTextBoxColumn MedDosage;
        private System.Windows.Forms.LinkLabel RemoveMask;
        private System.Windows.Forms.LinkLabel ShowMask;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox VideoOption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlVitalSignHist;
        private System.Windows.Forms.Button btnVitalSignHistClose;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ToolStripMenuItem SynchPatients;
        private System.Windows.Forms.ToolStripMenuItem SendPatientDetailToServer;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ToolStripButton ViewPatientDocument;
        private System.Windows.Forms.ToolStripButton Notifications;
    }
}
