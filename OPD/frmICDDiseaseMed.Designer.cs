namespace OPD
{
    partial class frmICDDiseaseMed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.dgDiseaseMed = new System.Windows.Forms.DataGridView();
            this.SNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Generic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Medicine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiseaseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiseaseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDiseaseName = new System.Windows.Forms.Label();
            this.lblDiseaseCode = new System.Windows.Forms.Label();
            this.txtDiseaseCode = new System.Windows.Forms.TextBox();
            this.txtDiseaseName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiseaseMed)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::OPD.Properties.Resources.Cancel_15;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.Location = new System.Drawing.Point(595, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(25, 25);
            this.btnClose.TabIndex = 1;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgDiseaseMed
            // 
            this.dgDiseaseMed.AllowUserToAddRows = false;
            this.dgDiseaseMed.AllowUserToDeleteRows = false;
            this.dgDiseaseMed.AllowUserToOrderColumns = true;
            this.dgDiseaseMed.AllowUserToResizeColumns = false;
            this.dgDiseaseMed.AllowUserToResizeRows = false;
            this.dgDiseaseMed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDiseaseMed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDiseaseMed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SNO,
            this.Generic,
            this.Medicine,
            this.Nature,
            this.DiseaseCode,
            this.DiseaseName});
            this.dgDiseaseMed.Location = new System.Drawing.Point(0, 27);
            this.dgDiseaseMed.Name = "dgDiseaseMed";
            this.dgDiseaseMed.ReadOnly = true;
            this.dgDiseaseMed.RowHeadersWidth = 20;
            this.dgDiseaseMed.Size = new System.Drawing.Size(619, 239);
            this.dgDiseaseMed.TabIndex = 2;
            // 
            // SNO
            // 
            this.SNO.DataPropertyName = "SNO";
            this.SNO.FillWeight = 5F;
            this.SNO.HeaderText = "S#";
            this.SNO.Name = "SNO";
            this.SNO.ReadOnly = true;
            // 
            // Generic
            // 
            this.Generic.DataPropertyName = "gcname";
            this.Generic.FillWeight = 30F;
            this.Generic.HeaderText = "Generic";
            this.Generic.Name = "Generic";
            this.Generic.ReadOnly = true;
            // 
            // Medicine
            // 
            this.Medicine.DataPropertyName = "Med_name";
            this.Medicine.FillWeight = 40F;
            this.Medicine.HeaderText = "Medicine";
            this.Medicine.Name = "Medicine";
            this.Medicine.ReadOnly = true;
            // 
            // Nature
            // 
            this.Nature.DataPropertyName = "NatureName";
            this.Nature.FillWeight = 25F;
            this.Nature.HeaderText = "Nature";
            this.Nature.Name = "Nature";
            this.Nature.ReadOnly = true;
            // 
            // DiseaseCode
            // 
            this.DiseaseCode.DataPropertyName = "DiseaseCode";
            this.DiseaseCode.HeaderText = "DiseaseCode";
            this.DiseaseCode.Name = "DiseaseCode";
            this.DiseaseCode.ReadOnly = true;
            this.DiseaseCode.Visible = false;
            // 
            // DiseaseName
            // 
            this.DiseaseName.DataPropertyName = "DiseaseName";
            this.DiseaseName.HeaderText = "DiseaseName";
            this.DiseaseName.Name = "DiseaseName";
            this.DiseaseName.ReadOnly = true;
            this.DiseaseName.Visible = false;
            // 
            // lblDiseaseName
            // 
            this.lblDiseaseName.AutoSize = true;
            this.lblDiseaseName.Location = new System.Drawing.Point(166, 7);
            this.lblDiseaseName.Name = "lblDiseaseName";
            this.lblDiseaseName.Size = new System.Drawing.Size(44, 13);
            this.lblDiseaseName.TabIndex = 3;
            this.lblDiseaseName.Text = "Name : ";
            // 
            // lblDiseaseCode
            // 
            this.lblDiseaseCode.AutoSize = true;
            this.lblDiseaseCode.Location = new System.Drawing.Point(5, 7);
            this.lblDiseaseCode.Name = "lblDiseaseCode";
            this.lblDiseaseCode.Size = new System.Drawing.Size(41, 13);
            this.lblDiseaseCode.TabIndex = 4;
            this.lblDiseaseCode.Text = "Code : ";
            // 
            // txtDiseaseCode
            // 
            this.txtDiseaseCode.Location = new System.Drawing.Point(50, 3);
            this.txtDiseaseCode.Name = "txtDiseaseCode";
            this.txtDiseaseCode.ReadOnly = true;
            this.txtDiseaseCode.Size = new System.Drawing.Size(112, 20);
            this.txtDiseaseCode.TabIndex = 5;
            // 
            // txtDiseaseName
            // 
            this.txtDiseaseName.Location = new System.Drawing.Point(214, 3);
            this.txtDiseaseName.Name = "txtDiseaseName";
            this.txtDiseaseName.ReadOnly = true;
            this.txtDiseaseName.Size = new System.Drawing.Size(381, 20);
            this.txtDiseaseName.TabIndex = 6;
            // 
            // frmICDDiseaseMed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 266);
            this.Controls.Add(this.txtDiseaseName);
            this.Controls.Add(this.txtDiseaseCode);
            this.Controls.Add(this.lblDiseaseCode);
            this.Controls.Add(this.lblDiseaseName);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgDiseaseMed);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "frmICDDiseaseMed";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmICDDiseaseMed_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgDiseaseMed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgDiseaseMed;
        private System.Windows.Forms.Label lblDiseaseName;
        private System.Windows.Forms.Label lblDiseaseCode;
        private System.Windows.Forms.TextBox txtDiseaseCode;
        private System.Windows.Forms.TextBox txtDiseaseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Generic;
        private System.Windows.Forms.DataGridViewTextBoxColumn Medicine;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nature;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiseaseCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiseaseName;
    }
}