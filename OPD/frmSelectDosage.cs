using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmSelectDosage : Form
    {
        private frmMedicineSelection objfrmMedicineSelection = null;
        private frmAddFromList objAddfromList = null;

        public frmSelectDosage()
        {
            InitializeComponent();
        }

        public frmSelectDosage(frmMedicineSelection Frm)
        {
            InitializeComponent();
            this.objfrmMedicineSelection = Frm;
        }

        public frmSelectDosage(frmAddFromList Frm)
        {
            InitializeComponent();
            this.objAddfromList = Frm;
        }

        private void frmSelectDosage_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            

              objComp.ApplyStyleToControls(this);
            //cboDayCount.SelectedIndex = 0;
            //cboInstruction.SelectedIndex = 0;
            ////cboQunatity.SelectedIndex = 0;
            //cboType.SelectedIndex = 0;
            
           

            clsBLDBConnection objConnection = new clsBLDBConnection();
            objConnection.Connection_Open();
            //FillCbo(objConnection);

            clsBLDosageInstruction objDosIns = new clsBLDosageInstruction(objConnection);
            objDosIns.PersonID = clsSharedVariables.UserID;

			
                        
            if (clsSharedVariables.Language.Equals("Urdu"))
            {
                objComp.FillComboBox(cboQunatity, objDosIns.GetAll(8), "Name", true);
                objComp.FillComboBox(cboType, objDosIns.GetAll(9), "Name", true);
                objComp.FillComboBox(cboDayCount, objDosIns.GetAll(10), "Name", true);
                objComp.FillComboBoxStartWithEmpty(cboInstruction, objDosIns.GetAll(2), "Description", "DosageInstructionID", true);

            }
            else if (clsSharedVariables.Language.Equals("Farsi"))
            
            {
                objComp.FillComboBox(cboQunatity, objDosIns.GetAll(3), "Name", true);
                objComp.FillComboBox(cboType, objDosIns.GetAll(4), "Name", true);
                objComp.FillComboBox(cboDayCount, objDosIns.GetAll(6), "Name", true);                             
                objComp.FillComboBoxStartWithEmpty(cboInstruction, objDosIns.GetAll(7), "Description", "DosageInstructionID", true);
                chbMorning.Text = @"\Ju";
                chbNoon.Text = "OqB†";
                chbEvening.Text = "Kq";
                chbNight.Text = "LAÌa O³Ë";
                btnSpeedSeletion1.Text = @"Kq OqB† \Ju Ó»ÌŒ ÂÎÃ";
                btnSpeedSeletion2.Text = @"Kq OqB† \Ju Ó»ÌŒ ¹Í";
                btnSpeedSeletion3.Text = @"Kq OqB† \Ju Ó»ÌŒ Ëe";
                btnSpeedSeletion4.Text = @"Kq OqB† \Ju ˆÀ† ÂÎÃ";
                btnSpeedSeletion5.Text = @"Kq OqB† \Ju ˆÀ† ¹Í";
                btnSpeedSeletion6.Text = @"Kq OqB† \Ju ˆÀ† Ëe";
                btnSpeedSeletion7.Text = @"Kq OqB† \Ju ¾ÌnƒÎº ¹Í";
                btnSpeedSeletion8.Text = @"Kq OqB† \Ju ¾ÌnƒÎº Ëe";
                chbMorning.Location = new System.Drawing.Point(57, 9);
                chbNoon.Location = new System.Drawing.Point(47, 39);
                chbNight.Location= new System.Drawing.Point(15, 100);
                chbEvening.Location = new System.Drawing.Point(63, 73);
    
            }

            objComp = null;
            objConnection.Connection_Close();
        }

       //private void FillCbo(clsBLDBConnection objConnection)
       //     {
       //         clsBLDosageInstruction objDosIns = new clsBLDosageInstruction(objConnection);
       //         SComponents objComp = new SComponents();
				

       //         try
       //         {
					
       //             objDosIns.PersonID = clsSharedVariables.UserID;

       //             objComp.FillComboBoxStartWithEmpty(cboInstruction, objDosIns.GetAll(2), "Description", "DosageInstructionID", true);
       //         }
       //         catch { }
       //         finally
       //         {
					

       //             objDosIns = null;
       //             objConnection = null;
       //             objComp = null;
       //         }
       //     }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblFinalDosage.Text = "";
            this.Close();
        }

        private void btnSpeedSeletion1_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion1.Text);
        }

        private void btnSpeedSeletion2_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion2.Text);
        }

        private void btnSpeedSeletion3_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion3.Text);
        }

        private void btnSpeedSeletion4_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion4.Text); 
        }

        private void btnSpeedSeletion5_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion5.Text);
        }

        private void btnSpeedSeletion6_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion6.Text);
        }

        private void btnSpeedSeletion7_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion7.Text);
        }

        private void btnSpeedSeletion8_Click(object sender, EventArgs e)
        {
            SelectComboBoxValues(btnSpeedSeletion8.Text);
        }

        private void SelectComboBoxValues(string str)
        { 
            string[] sarr;
			
            sarr=str.Split(' ');
            cboQunatity.Text = sarr[4];
            cboType.Text  = sarr[3].ToString();
            chbEvening.Checked = true;
            chbMorning.Checked = true;
            chbNoon.Checked = true;
            //chbNight.Checked = true;
        }

        private void CreateDosageStr()
        {
            string str = "";
            
            str += cboInstruction.Text ;
            if (cboDayCount.SelectedIndex != 0)
            {
                str += "( " ;
                str += cboDayCount.Text ;
                str += " ) "; 
            }
            if (chbNight.Checked)
            {
                str += chbNight.Text + " ";
            }
            if (chbEvening.Checked)
            {
                str += chbEvening.Text + " " ;
            }
            if (chbNoon.Checked)
            {
                str +=  chbNoon.Text + " " ;
            }
            if (chbMorning.Checked)
            {
                str += chbMorning.Text + " ";
            }
            str += cboType.Text + " " + cboQunatity.Text;

            lblFinalDosage.Text = str;

        }

        private void cboQunatity_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void cboDayCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void cboInstruction_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void chbMorning_CheckedChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void chbNoon_CheckedChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void chbEvening_CheckedChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chbNight_CheckedChanged(object sender, EventArgs e)
        {
            CreateDosageStr();
        }

        private void frmSelectDosage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objfrmMedicineSelection != null)
            {
                objfrmMedicineSelection.Show();
                if (!this.lblFinalDosage.Text.Trim().Equals(""))
                {
                    string morning = "";
                    string afternoon = "";
                    string evening = "";
                    string night = "";
                   
                    
                    if (chbMorning.Checked==true)
                    {
                        morning = chbMorning.Text;
                    }
                    if (chbNoon.Checked == true)
                    {
                        afternoon = chbNoon.Text;
                    }
                    if (chbEvening.Checked == true)
                    {
                        evening = chbEvening.Text;
                    }
                    if (chbNight.Checked == true)
                    {
                        night = chbNight.Text;
                    }
                    objfrmMedicineSelection.GetDosage(lblFinalDosage.Text.Trim(), morning, afternoon, evening, night, cboDayCount.Text, cboInstruction.Text, cboQunatity.Text);
                }
                this.Dispose();
            }
            else if (objAddfromList != null)
            {
                objAddfromList.Show();
                if (!this.lblFinalDosage.Text.Trim().Equals(""))
                {
                    objAddfromList.SetDosage(lblFinalDosage.Text.Trim());
                }
                this.Dispose();
            }
        }

			private void btnAddDosageInstruction_Click(object sender, EventArgs e)
			{
				frmDosageInstruction objDosIns = new frmDosageInstruction();
				objDosIns.ShowDialog();
				objDosIns.Dispose();
                clsBLDBConnection objConnection = new clsBLDBConnection();
                objConnection.Connection_Open();
                //FillCbo(objConnection);
                objConnection.Connection_Close();
			}
    }
}
