namespace OPD
{
    partial class frmReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.txtKey = new System.Windows.Forms.MaskedTextBox();
          this.btnOK = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.label1 = new System.Windows.Forms.Label();
          this.lblName = new System.Windows.Forms.Label();
          this.txtName = new System.Windows.Forms.TextBox();
          this.txtID = new System.Windows.Forms.TextBox();
          this.lblID = new System.Windows.Forms.Label();
          this.panel2 = new System.Windows.Forms.Panel();
          this.panel1 = new System.Windows.Forms.Panel();
          this.panel3 = new System.Windows.Forms.Panel();
          this.SuspendLayout();
          // 
          // txtKey
          // 
          this.txtKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtKey.HidePromptOnLeave = true;
          this.txtKey.HideSelection = false;
          this.txtKey.Location = new System.Drawing.Point(85, 70);
          this.txtKey.Mask = "AAAAA-AAAAA-AAAAA-AAAAA-AAAAA";
          this.txtKey.Name = "txtKey";
          this.txtKey.PromptChar = ' ';
          this.txtKey.Size = new System.Drawing.Size(294, 21);
          this.txtKey.TabIndex = 2;
          this.txtKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKey_KeyPress);
          // 
          // btnOK
          // 
          this.btnOK.BackColor = System.Drawing.Color.LightSlateGray;
          this.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
          this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.btnOK.Location = new System.Drawing.Point(120, 110);
          this.btnOK.Name = "btnOK";
          this.btnOK.Size = new System.Drawing.Size(75, 23);
          this.btnOK.TabIndex = 3;
          this.btnOK.Text = "&OK";
          this.btnOK.UseVisualStyleBackColor = false;
          this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.BackColor = System.Drawing.Color.LightSlateGray;
          this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
          this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.btnCancel.Location = new System.Drawing.Point(218, 110);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 4;
          this.btnCancel.Text = "&Cancel";
          this.btnCancel.UseVisualStyleBackColor = false;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(50, 74);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(34, 13);
          this.label1.TabIndex = 3;
          this.label1.Tag = "display";
          this.label1.Text = "Key : ";
          // 
          // lblName
          // 
          this.lblName.AutoSize = true;
          this.lblName.Location = new System.Drawing.Point(40, 21);
          this.lblName.Name = "lblName";
          this.lblName.Size = new System.Drawing.Size(44, 13);
          this.lblName.TabIndex = 4;
          this.lblName.Tag = "display";
          this.lblName.Text = "Name : ";
          // 
          // txtName
          // 
          this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtName.Location = new System.Drawing.Point(85, 18);
          this.txtName.Name = "txtName";
          this.txtName.Size = new System.Drawing.Size(294, 20);
          this.txtName.TabIndex = 0;
          this.txtName.Tag = "notset";
          // 
          // txtID
          // 
          this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtID.Location = new System.Drawing.Point(85, 44);
          this.txtID.Name = "txtID";
          this.txtID.Size = new System.Drawing.Size(294, 20);
          this.txtID.TabIndex = 1;
          this.txtID.Tag = "notset";
          // 
          // lblID
          // 
          this.lblID.AutoSize = true;
          this.lblID.Location = new System.Drawing.Point(28, 47);
          this.lblID.Name = "lblID";
          this.lblID.Size = new System.Drawing.Size(56, 13);
          this.lblID.TabIndex = 6;
          this.lblID.Tag = "display";
          this.lblID.Text = "Login ID : ";
          // 
          // panel2
          // 
          this.panel2.BackColor = System.Drawing.Color.Red;
          this.panel2.Location = new System.Drawing.Point(375, 18);
          this.panel2.Name = "panel2";
          this.panel2.Size = new System.Drawing.Size(4, 20);
          this.panel2.TabIndex = 146;
          this.panel2.Tag = "no";
          // 
          // panel1
          // 
          this.panel1.BackColor = System.Drawing.Color.Red;
          this.panel1.Location = new System.Drawing.Point(375, 44);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(4, 20);
          this.panel1.TabIndex = 147;
          this.panel1.Tag = "no";
          // 
          // panel3
          // 
          this.panel3.BackColor = System.Drawing.Color.Red;
          this.panel3.Location = new System.Drawing.Point(375, 70);
          this.panel3.Name = "panel3";
          this.panel3.Size = new System.Drawing.Size(4, 20);
          this.panel3.TabIndex = 148;
          this.panel3.Tag = "no";
          // 
          // frmReg
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(406, 150);
          this.Controls.Add(this.panel3);
          this.Controls.Add(this.panel1);
          this.Controls.Add(this.panel2);
          this.Controls.Add(this.txtID);
          this.Controls.Add(this.lblID);
          this.Controls.Add(this.txtName);
          this.Controls.Add(this.lblName);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOK);
          this.Controls.Add(this.txtKey);
          this.Controls.Add(this.label1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "frmReg";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Registration";
          this.Load += new System.EventHandler(this.frmReg_Load);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txtKey;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
    }
}