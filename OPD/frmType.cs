using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
	public partial class frmType : Form
	{
		private frmOPD MainOPD = null;

		public frmType()
		{
			InitializeComponent();
		}

		public frmType(frmOPD Frm)
		{
			InitializeComponent();
			this.MainOPD = Frm;
		}

		private void frmType_Load(object sender, EventArgs e)
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			SComponents objComp = new SComponents();

			objComp.ApplyStyleToControls(this);
			objConnection.Connection_Open();

			GetLanguage();
			FillGridView(objConnection);

			objConnection.Connection_Close();
			dgType.ClearSelection();
			this.Text = this.Text + "(" + clsSharedVariables.InItCaps(clsSharedVariables.UserName) + ")";

			if (clsSharedVariables.Language.Equals("Eng"))
			{
				this.Size = new Size(868, 704);
				panel6.Visible = false;
				dgType.Location = new Point(1, 99);
				dgType.Size = new Size(853, 431);
				dgType.BringToFront();
                label1.Visible = true;
                label13.Visible = false ;
			}
			else if (clsSharedVariables.Language.Equals("Urdu"))
			{
				this.Size = new Size(868, 704);
				panel6.Visible = true;

				dgType.Location = new Point(1, 341);
				dgType.Size = new Size(853, 189);
				dgType.BringToFront();
                label1.Visible = false ;
                label13.Visible = true ;
			}
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                this.Size = new Size(868, 704);
                panel6.Visible = true;

                dgType.Location = new Point(1, 341);
                dgType.Size = new Size(853, 189);
                dgType.BringToFront();
                label1.Visible = false;
                label13.Visible = true;
            }
			Urdubtn();

			objConnection = null;
			objComp = null;
		}

		private void GetLanguage()
		{
			if (clsSharedVariables.Language.Equals("Eng"))
			{
				rbEnglish.Checked = true;
				btnLangSave_Click(btnLangSave, EventArgs.Empty);
			}
			else if (clsSharedVariables.Language.Equals("Urdu"))
			{
				rbUrdu.Checked = true;
				btnLangSave_Click(btnLangSave, EventArgs.Empty);
			}
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                rbFarsi.Checked = true;
                btnLangSave_Click(btnLangSave, EventArgs.Empty);
            }
		}

		private clsBLType SetBLValues(clsBLType objType)
		{
			if (rbEnglish.Checked)
			{
				objType.Name = "EngInstruction";
				objType.Description = clsSharedVariables.InItCaps(txtDescription.Text);
			}
			if (rbUrdu.Checked)
			{
				objType.Name = "UrduInstruction";
				objType.Description = txtDescription.Text.Replace("\\", "\\\\").Trim();
			}
            if (rbFarsi.Checked)
            {
                objType.Name = "FarsiInstruction";
                objType.Description = txtDescription.Text.Replace("\\", "\\\\").Trim();
            }

			if (chbActive.Checked)
			{
				objType.Active = "1";
			}
			else
			{
				objType.Active = "0";
			}

			if (chbSelect.Checked)
			{
				objType.DefaultVal = "Y";
			}
			else
			{
				objType.DefaultVal = "N";
			}
			objType.EnteredBy = clsSharedVariables.UserID; ;
			objType.EnteredOn = DateTime.Now.Date.ToString("dd/MM/yyyy");
			objType.PersonID = clsSharedVariables.UserID; ;
			objType.ClientID = clsSharedVariables.ClientID; ;

			return objType;
		}

		private void Insert()
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLType objType = new clsBLType(objConnection);

			try
			{
				objType = SetBLValues(objType);
				objConnection.Connection_Open();
				objConnection.Transaction_Begin();
				if (objType.Insert())
				{
					objConnection.Transaction_ComRoll();
					FillGridView(objConnection);
					objConnection.Connection_Close();
					ClearField();
					MessageBox.Show("Instruction Add Successfully ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show(objType.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				objType = null;
				objConnection = null;
			}
		}

		private void UpdateData()
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLType objType = new clsBLType(objConnection);
			try
			{
				objType.TypeID = this.lblTypeID.Text;
				objType = SetBLValues(objType);

				objConnection.Connection_Open();
				objConnection.Transaction_Begin();
				if (objType.Update())
				{
					objConnection.Transaction_ComRoll();
					FillGridView(objConnection);
					objConnection.Connection_Close();
					ClearField();
					MessageBox.Show("Instruction updated successfully ", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show(objType.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				objType = null;
				objConnection = null;
			}
		}

		private void FillGridView(clsBLDBConnection objConnection)
		{
			clsBLType objType = new clsBLType(objConnection);

			objType.PersonID = clsSharedVariables.UserID;
			if (rbUrdu.Checked)
			{
				objType.Name = "UrduInstruction";
			}
			if (rbEnglish.Checked)
			{
				objType.Name = "EngInstruction";
			}
            if (rbFarsi.Checked)
            {
                objType.Name = "FarsiInstruction";
            }
			DataView dvType = objType.GetAll(3);
			//dvType.Sort = "Name";
			this.dgType.DataSource = dvType;

			objType = null;
			dvType = null;
		}

		private void ClearField()
		{
			txtDescription.Text = "";
			chbActive.Checked = true;
			chbSelect.Checked = true;
			dgType.ClearSelection();
			txtDescription.Focus();
			this.lblTypeID.Text = "";
			this.miSave.Tag = "Save";
			this.miSave.Text = "Save";
		}

		private void FillForm(int rowIndex)
		{
			this.miSave.Tag = "Update";
			this.miSave.Text = "Update";

			if (clsSharedVariables.Language.Equals("Eng"))
			{
				txtDescription.Text = clsSharedVariables.InItCaps(dgType.Rows[rowIndex].Cells["Description"].Value.ToString());
			}
            else if (clsSharedVariables.Language.Equals("Urdu"))
			{
				txtDescription.Text = dgType.Rows[rowIndex].Cells["Description"].Value.ToString();
			}
            else if (clsSharedVariables.Language.Equals("Farsi"))
            {
                txtDescription.Text = dgType.Rows[rowIndex].Cells["Description"].Value.ToString();
            }

			lblTypeID.Text = dgType.Rows[rowIndex].Cells["TypeID"].Value.ToString();

			if (dgType.Rows[rowIndex].Cells["Active"].Value.ToString().Equals("1"))
			{
				chbActive.Checked = true;
			}
			else
			{
				chbActive.Checked = false;
			}
			if (dgType.Rows[rowIndex].Cells["DefaultSel"].Value.ToString().Equals("Y"))
			{
				chbSelect.Checked = true;
			}
			else
			{
				chbSelect.Checked = false;
			}
		}

		private void dgType_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex != -1)
			{
				FillForm(e.RowIndex);
			}
		}

		private void SaveLanguage()
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();
			clsBLReferences objRef = new clsBLReferences(objConnection);
			try
			{
				//objPersonal.PersonID = clsSharedVariables.UserID  ;
				if (rbEnglish.Checked)
				{
					objRef.Description = "Eng";
					clsSharedVariables.Language = "Eng";
				}
				else if (rbUrdu.Checked)
				{
					objRef.Description = "Urdu";
					clsSharedVariables.Language = "Urdu";
				}
                else if (rbFarsi.Checked)
                {
                    objRef.Description = "Farsi";
                    clsSharedVariables.Language = "Farsi";
                }

				objRef.ReportRefrence = "InstLang";

				objConnection.Connection_Open();
				objConnection.Transaction_Begin();
				if (objRef.Update())
				{
					objConnection.Transaction_ComRoll();
				}
				else
				{
					MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				objConnection.Connection_Close();
				objRef = null;
				objConnection = null;
			}
		}

		private void miRefresh_Click(object sender, EventArgs e)
		{
			ClearField();
		}

		private void miSave_Click(object sender, EventArgs e)
		{
			if (this.miSave.Tag.Equals("Save"))
			{
				Insert();
			}
			else
			{
				UpdateData();
			}
		}

		private void miExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnLangSave_Click(object sender, EventArgs e)
		{
			clsBLDBConnection objConnection = new clsBLDBConnection();

            SaveLanguage();
			txtDescription.Text = "";
			if (rbEnglish.Checked)
			{
				txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
				this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.Yes;

				dgType.Columns["Description"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
				dgType.Columns["Description"].DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

				panel6.Visible = false;
				dgType.Location = new Point(1, 99);
				dgType.Size = new Size(853, 431);
				dgType.BringToFront();
			}
			else
			{
				txtDescription.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
				this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;

				dgType.Columns["Description"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
				dgType.Columns["Description"].DefaultCellStyle.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
				panel6.Visible = true;
				dgType.Location = new Point(1, 341);
				dgType.Size = new Size(853, 189);
				dgType.BringToFront();
			}
			try
			{
				objConnection.Connection_Open();
				FillGridView(objConnection);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}
			finally
			{
				objConnection.Connection_Close();
				objConnection = null;
			}
		}

		private void frmType_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.MainOPD.Show();
			this.MainOPD.dgMedicineLang();

			this.Dispose();
		}

		private void Urdubtn()
		{
			btn41.Text = "\u0041";
			btn42.Text = "\u0042";
			btn43.Text = "\u0043";
			btn44.Text = "\u0044";
			btn45.Text = "\u0045";
			btn46.Text = "\u0046";
			btn47.Text = "\u0047";
			btn48.Text = "\u0048";
			btn49.Text = "\u0049";
			btn4A.Text = "\u004A";
			btn4B.Text = "\u004B";
			btn4C.Text = "\u004C";
			btn4D.Text = "\u004D";
			btn4E.Text = "\u004E";
			btn4F.Text = "\u004F";
			btn50.Text = "\u0050";
			btn51.Text = "\u0051";
			btn52.Text = "\u0052";
			btn53.Text = "\u0053";
			btn54.Text = "\u0054";
			btn55.Text = "\u0055";
			btn56.Text = "\u0056";
			btn57.Text = "\u0057";
			btn58.Text = "\u0058";
			btn59.Text = "\u0059";
			btn5A.Text = "\u005A";
			btn5C.Text = "\u005C";
			btn5F.Text = "\u005F";
			btn60.Text = "\u0060";
			btn61.Text = "\u0061";
			btn62.Text = "\u0062";
			btn63.Text = "\u0063";
			btn64.Text = "\u0064";
			btn65.Text = "\u0065";
			btn66.Text = "\u0066";
			btn67.Text = "\u0067";
			btn68.Text = "\u0068";
			btn69.Text = "\u0069";
			btn6A.Text = "\u006A";
			btn6B.Text = "\u006B";
			btn6C.Text = "\u006C";
			btn6D.Text = "\u006D";
			btn6E.Text = "\u006E";
			btn6F.Text = "\u006F";
			btn70.Text = "\u0070";
			btn71.Text = "\u0071";
			btn72.Text = "\u0072";
			btn73.Text = "\u0073";
			btn74.Text = "\u0074";
			btn75.Text = "\u0075";
			btn76.Text = "\u0076";
			btn77.Text = "\u0077";
			btn78.Text = "\u0078";
			btn79.Text = "\u0079";
			btn7A.Text = "\u007A";
			btn7B.Text = "\u007B";
			btn7C.Text = "\u007C";
			btn7E.Text = "\u007E";
			btnA1.Text = "\u00A1";

			btnA2.Text = "\u00A2";
			btnA3.Text = "\u00A3";
			btnA4.Text = "\u00A4";
			btnA5.Text = "\u00A5";
			btnA6.Text = "\u00A6";
			btnA7.Text = "\u00A6";
			btnA8.Text = "\u00A8";
			btnA9.Text = "\u00A9";
			btnAA.Text = "\u00AA";
			btnAB.Text = "\u00AB";
			btnAC.Text = "\u00AC";
			btnAD.Text = "\u00AD";
			btnAE.Text = "\u00AE";
			btnAF.Text = "\u00AF";
			btnB0.Text = "\u00B0";
			btnB1.Text = "\u00B1";
			btnB2.Text = "\u00B2";
			btnB3.Text = "\u00B3";
			btnB4.Text = "\u00B4";
			btnB5.Text = "\u00B5";
			btnB6.Text = "\u00B6";
			btnB7.Text = "\u00B7";
			btnB8.Text = "\u00B8";
			btnB9.Text = "\u00B9";
			btnBA.Text = "\u00BA";
			btnBB.Text = "\u00BB";
			btnBC.Text = "\u00BC";
			btnBD.Text = "\u00BD";
			btnBE.Text = "\u00BE";
			btnBF.Text = "\u00BF";

			btnC0.Text = "\u00C0";
			btnC1.Text = "\u00C1";
			btnC2.Text = "\u00C2";
			btnC3.Text = "\u00C3";
			btnC4.Text = "\u00C4";
			btnC5.Text = "\u00C5";
			btnC6.Text = "\u00C6";
			btnC7.Text = "\u00C7";
			btnC8.Text = "\u00C8";
			btnC9.Text = "\u00C9";
			btnCA.Text = "\u00CA";
			btnCB.Text = "\u00CB";
			btnCC.Text = "\u00CC";
			btnCD.Text = "\u00CD";
			btnCE.Text = "\u00CE";
			btnCF.Text = "\u00CF";

			btnD0.Text = "\u00D0";
			btnD1.Text = "\u00D1";
			btnD2.Text = "\u00D2";
			btnD3.Text = "\u00D3";
			btnD4.Text = "\u00D4";
			btnD5.Text = "\u00D5";
			btnD6.Text = "\u00D6";
			btnD7.Text = "\u00D7";
			btnD8.Text = "\u00D8";
			btnD9.Text = "\u00D9";
			btnDA.Text = "\u00DA";
			btnDB.Text = "\u00DA";
			btnDC.Text = "\u00DC";
			btnDD.Text = "\u00DC";
			btnDE.Text = "\u00DF";
			btnDF.Text = "\u00D0";

			btnE0.Text = "\u00E0";
			btnE1.Text = "\u00E1";
			btnE2.Text = "\u00E2";
			btnE3.Text = "\u00E3";
			btnE4.Text = "\u00E4";
			btnE5.Text = "\u00E5";
			btnE6.Text = "\u00E6";
			btnE7.Text = "\u00E7";
			btnE8.Text = "\u00E8";
			btnE9.Text = "\u00E9";
			btnEA.Text = "\u00EA";
			btnEB.Text = "\u00EB";
			btnEC.Text = "\u00EC";
			btnED.Text = "\u00ED";
			btnEE.Text = "\u00EE";
			btnEF.Text = "\u00EF";

			btnF0.Text = "\u00F0";
			btnF1.Text = "\u00F1";

			btn152.Text = "\u0152";
			btn153.Text = "\u0153";
			btn160.Text = "\u0160";
			btn161.Text = "\u0161";
			btn192.Text = "\u0192";
			btn2C6.Text = "\u02C6";
			btn2DC.Text = "\u02DC";
			btn2018.Text = "\u2018";
			btn2019.Text = "\u2019";
			btn201A.Text = "\u201A";
			btn201C.Text = "\u201C";
			btn201D.Text = "\u201D";
			btn201E.Text = "\u201E";
			btn2020.Text = "\u2020";

			btn2021.Text = "\u2021";
			btn2022.Text = "\u2022";
			btn2026.Text = "\u2026";
			btn2030.Text = "\u2030";
			btn2039.Text = "\u2039";
			btn203A.Text = "\u203A";
			btn2122.Text = "\u2122";
			btn2219.Text = "\u2219";
		}

		private void btn_Click(object sender, EventArgs e)
		{
			Button btn = (Button)sender;
			txtDescription.Text = btn.Text + txtDescription.Text;
		//	txtDescription.SelectionStart = txtDescription.Text.Length;
			txtDescription.Focus();
		}

		private void btnBS_Click(object sender, EventArgs e)
		{
			if (txtDescription.Text.Length != 0)
			{
				txtDescription.Text = txtDescription.Text.Substring(1, txtDescription.Text.Length - 1);
				txtDescription.Focus();
				txtDescription.SelectionStart = txtDescription.Text.Length;
			}
		}

		private void btnSpace_Click(object sender, EventArgs e)
		{
			txtDescription.Text = " " + txtDescription.Text;
			txtDescription.Focus();
			txtDescription.SelectionStart = txtDescription.Text.Length;
		}
	}
}