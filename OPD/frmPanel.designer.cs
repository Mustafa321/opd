namespace OPD
{
    partial class frmPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPanel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chbActive = new System.Windows.Forms.CheckBox();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.lblPanelID = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dgPanel = new System.Windows.Forms.DataGridView();
            this.SerialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EnteredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTotalRec = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtFees = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPanel)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(308, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(5, 20);
            this.panel1.TabIndex = 147;
            this.panel1.Tag = "no";
            // 
            // chbActive
            // 
            this.chbActive.AutoSize = true;
            this.chbActive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbActive.Checked = true;
            this.chbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbActive.Location = new System.Drawing.Point(468, 10);
            this.chbActive.Name = "chbActive";
            this.chbActive.Size = new System.Drawing.Size(65, 17);
            this.chbActive.TabIndex = 2;
            this.chbActive.Text = "Active : ";
            this.chbActive.UseVisualStyleBackColor = true;
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.toolStrip1.Location = new System.Drawing.Point(387, -1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(197, 55);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // lblPanelID
            // 
            this.lblPanelID.AutoSize = true;
            this.lblPanelID.Location = new System.Drawing.Point(373, 277);
            this.lblPanelID.Name = "lblPanelID";
            this.lblPanelID.Size = new System.Drawing.Size(45, 13);
            this.lblPanelID.TabIndex = 48;
            this.lblPanelID.Text = "PanelID";
            this.lblPanelID.Visible = false;
            // 
            // dgPanel
            // 
            this.dgPanel.AllowUserToAddRows = false;
            this.dgPanel.AllowUserToDeleteRows = false;
            this.dgPanel.AllowUserToResizeRows = false;
            this.dgPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgPanel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPanel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgPanel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPanel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SerialNo,
            this.PanelID,
            this.PanelName,
            this.Fees,
            this.Active,
            this.EnteredBy,
            this.EnteredOn,
            this.ClientID});
            this.dgPanel.EnableHeadersVisualStyles = false;
            this.dgPanel.Location = new System.Drawing.Point(3, 121);
            this.dgPanel.MultiSelect = false;
            this.dgPanel.Name = "dgPanel";
            this.dgPanel.ReadOnly = true;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPanel.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgPanel.RowHeadersWidth = 25;
            this.dgPanel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPanel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPanel.Size = new System.Drawing.Size(586, 251);
            this.dgPanel.TabIndex = 45;
            this.dgPanel.TabStop = false;
            this.toolTip1.SetToolTip(this.dgPanel, "All add instructions");
            this.dgPanel.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPanel_CellDoubleClick);
            this.dgPanel.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPanel_ColumnHeaderMouseClick);
            // 
            // SerialNo
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SerialNo.DefaultCellStyle = dataGridViewCellStyle10;
            this.SerialNo.HeaderText = "S#";
            this.SerialNo.Name = "SerialNo";
            this.SerialNo.ReadOnly = true;
            this.SerialNo.Width = 50;
            // 
            // PanelID
            // 
            this.PanelID.DataPropertyName = "PanelID";
            this.PanelID.HeaderText = "PanelID";
            this.PanelID.Name = "PanelID";
            this.PanelID.ReadOnly = true;
            this.PanelID.Visible = false;
            // 
            // PanelName
            // 
            this.PanelName.DataPropertyName = "PanelName";
            this.PanelName.HeaderText = "Name";
            this.PanelName.Name = "PanelName";
            this.PanelName.ReadOnly = true;
            this.PanelName.Width = 305;
            // 
            // Fees
            // 
            this.Fees.DataPropertyName = "DefaultFee";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Fees.DefaultCellStyle = dataGridViewCellStyle11;
            this.Fees.HeaderText = "Fees";
            this.Fees.Name = "Fees";
            this.Fees.ReadOnly = true;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "0";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "1";
            // 
            // EnteredBy
            // 
            this.EnteredBy.DataPropertyName = "EnteredBy";
            this.EnteredBy.HeaderText = "EnteredBy";
            this.EnteredBy.Name = "EnteredBy";
            this.EnteredBy.ReadOnly = true;
            this.EnteredBy.Visible = false;
            // 
            // EnteredOn
            // 
            this.EnteredOn.DataPropertyName = "EnteredOn";
            this.EnteredOn.HeaderText = "EnteredOn";
            this.EnteredOn.Name = "EnteredOn";
            this.EnteredOn.ReadOnly = true;
            this.EnteredOn.Visible = false;
            // 
            // ClientID
            // 
            this.ClientID.DataPropertyName = "ClientId";
            this.ClientID.HeaderText = "ClientID";
            this.ClientID.Name = "ClientID";
            this.ClientID.ReadOnly = true;
            this.ClientID.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblTotalRec);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.lblName);
            this.panel3.Controls.Add(this.txtName);
            this.panel3.Controls.Add(this.txtFees);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.chbActive);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(3, 57);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(586, 63);
            this.panel3.TabIndex = 0;
            this.panel3.Tag = "";
            // 
            // lblTotalRec
            // 
            this.lblTotalRec.AutoSize = true;
            this.lblTotalRec.Location = new System.Drawing.Point(50, 37);
            this.lblTotalRec.Name = "lblTotalRec";
            this.lblTotalRec.Size = new System.Drawing.Size(78, 13);
            this.lblTotalRec.TabIndex = 156;
            this.lblTotalRec.Tag = "display";
            this.lblTotalRec.Text = "Total Record : ";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(50, 11);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(44, 13);
            this.lblName.TabIndex = 154;
            this.lblName.Text = "Name : ";
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(97, 8);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(216, 20);
            this.txtName.TabIndex = 0;
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtName_Validating);
            // 
            // txtFees
            // 
            this.txtFees.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFees.Location = new System.Drawing.Point(364, 8);
            this.txtFees.Name = "txtFees";
            this.txtFees.Size = new System.Drawing.Size(63, 20);
            this.txtFees.TabIndex = 1;
            this.txtFees.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFees_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(322, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 151;
            this.label1.Text = "Fees : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(427, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 155;
            this.label2.Text = "Rs";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(3, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(586, 55);
            this.panel2.TabIndex = 49;
            this.panel2.Tag = "top";
            // 
            // frmPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 373);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgPanel);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblPanelID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel Information";
            this.Load += new System.EventHandler(this.frmPanel_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPanel_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPanel)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chbActive;
        private System.Windows.Forms.ToolStripButton miSave;
        private System.Windows.Forms.ToolStripButton miRefresh;
        private System.Windows.Forms.ToolStripButton miExit;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Label lblPanelID;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridView dgPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtFees;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotalRec;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PanelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PanelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fees;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientID;
    }
}