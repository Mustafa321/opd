using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace OPD
{
    public class clsSharedVariables
    {
        public clsSharedVariables()
        {
        }
        public static string UserID = "2";
        public static string UserName = ""; //"Dr. Abdul Azeem";
        public static string ClientID = "1";
        public static string LoginID = ""; //"Ali";
        public static string Email = "TreesSoft@yahoo.com";
        public static string Language = "";
        public static string Version = "Version # H-CM-1.0.4  Date : 10/10/2020";
        public static string RefPersonID = "";
        public static string Payment = "20";
        public static bool AutoPRNO = true ;
        public static bool RenameHeading = true;
        public static bool Appointment = true;
        public static bool WaitingQueue = true;
        public static bool Notes = true;
        public static bool Allergies = true;
        public static bool VitalSign = true;
        public static bool Medication = true;
        public static bool PatientDocuments = true;
        public static bool AdmissionAndProcedure = true;
        public static bool PatientVisits = true;
        public static string SMTP = "";
        public static string Port = "";
        public static string SMTPEmail = "";
        public static string Password = "";
        public static string ExpireDate = "";
        public static int MaxRecTime = 90;//90 Sec
        //public static string DBUserName = "opd";
        //public static string DBPassWord = "trs_haisam_cm";
        //public static string DBServer = "OPD";
        //public static string DBName = "opd";

        public static string DBUserName = "umar";
        public static string DBPassWord = "umar123";
        public static string DBServer = "dns2";
        public static string DBName = "azeem";

        public static string PRNOHeading = "MRN No.";
        public static string PRNOMask = "aa-aa-aaaaaa";

        //public static string Key = "ASWQZ-KJUYH-LOIKJ-UYHBG-LOIKJ";

        public static string InItCaps(string str)
        {
            if(str!=null)
            {
                string str1 = new CultureInfo("en-US").TextInfo.ToTitleCase(str);
                return str1;
            }
            else
            {
                return "";
            }
            
        }
    }
}