﻿namespace OPD
{
    partial class frmPatientDocuments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DocumentGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // DocumentGridView
            // 
            this.DocumentGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DocumentGridView.Location = new System.Drawing.Point(0, -1);
            this.DocumentGridView.Name = "DocumentGridView";
            this.DocumentGridView.Size = new System.Drawing.Size(745, 317);
            this.DocumentGridView.TabIndex = 0;
            this.DocumentGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DocumentGridView_CellClick);
            // 
            // frmPatientDocuments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 318);
            this.Controls.Add(this.DocumentGridView);
            this.Name = "frmPatientDocuments";
            this.Text = "Patient Documents";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPatientDocuments_FormClosed);
            this.Load += new System.EventHandler(this.frmPatientDocuments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DocumentGridView;
    }
}