using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmCashrptRange : Form
    {
      private string _RptName="";

      public string RptName
      {
        get { return _RptName; }
        set { _RptName=value ;}
      }

        public frmCashrptRange()
        {
            InitializeComponent();
        }

        private void frmCashrptRange_Load(object sender, EventArgs e)
        {
          clsBLDBConnection objConnection = new clsBLDBConnection();
          clsBLPersonal objPerson = new clsBLPersonal(objConnection);
          SComponents objComp = new SComponents();

          dtpFrom.Value = DateTime.Now;
          dtpTo.Value = DateTime.Now;
          objComp.ApplyStyleToControls(this);

          if (_RptName.Equals("OPD_001_05"))
          {
            lblConsultant.Visible = false;
            cboConsultant.Visible = false;
          }
          else if (_RptName.Equals("OPD_001_09"))
          {
            lblConsultant.Visible = true ;
            cboConsultant.Visible = true ;
          }
          else if (_RptName.Equals("OPD_001_10"))
          {
            lblConsultant.Visible = true;
            cboConsultant.Visible = true;
          }
          else if (_RptName.Equals("OPD_001_11"))
          {
            lblConsultant.Visible = false;
            cboConsultant.Visible = false;
          }

          objConnection.Connection_Open();
          objComp.FillComboBox(cboConsultant, objPerson.GetAll(6), "Name", "Personid", true);
          objConnection.Connection_Close();


          objComp = null;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
          frmRptBill objDailyCashRpt = new frmRptBill();

          try
          {
            if (dtpTo.Value.CompareTo(dtpFrom.Value) != -1)
            {
              objDailyCashRpt.From = dtpFrom.Value.ToString("yyyy/MM/dd");
              objDailyCashRpt.To = dtpTo.Value.ToString("yyyy/MM/dd");

              objDailyCashRpt.ReportReference = _RptName;

              if (_RptName.Equals("OPD_001_09"))//Detail Pahrmacy Report
              {
                if (!cboConsultant.Text.Equals("Select"))
                {
                  objDailyCashRpt.Consultant = cboConsultant.SelectedValue.ToString();
                }
              }
              if (_RptName.Equals("OPD_001_10"))//Detail Pahrmacy Report
              {
                if (!cboConsultant.Text.Equals("Select"))
                {
                  objDailyCashRpt.Consultant = cboConsultant.SelectedValue.ToString();
                }
              }

              objDailyCashRpt.ShowDialog();
            }
            else
            {
              MessageBox.Show("From Date is not Greater then To Date ", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
          }
          catch (Exception exc)
          {
            MessageBox.Show(exc.Message, "Daily Cash Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          finally
          {
            objDailyCashRpt.Dispose();
          }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}