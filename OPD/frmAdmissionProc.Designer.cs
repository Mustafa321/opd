namespace OPD
{
    partial class frmAdmissionProc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdmissionProc));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtPName = new System.Windows.Forms.TextBox();
            this.txtPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.lblHName = new System.Windows.Forms.Label();
            this.lblHPRNo = new System.Windows.Forms.Label();
            this.lblHAge = new System.Windows.Forms.Label();
            this.lblHGender = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.dgAdmProc = new System.Windows.Forms.DataGridView();
            this.PatientAdmProc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnetredBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblAllergARHeading = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbAdmission = new System.Windows.Forms.RadioButton();
            this.rbProc = new System.Windows.Forms.RadioButton();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chbActive = new System.Windows.Forms.CheckBox();
            this.lblAdmProcID = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdmProc)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tsMain);
            this.panel2.Location = new System.Drawing.Point(12, 11);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(770, 53);
            this.panel2.TabIndex = 106;
            this.panel2.Tag = "top";
            // 
            // tsMain
            // 
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.Transparent;
            this.tsMain.CanOverflow = false;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.tsMain.Location = new System.Drawing.Point(573, -8);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(200, 66);
            this.tsMain.TabIndex = 44;
            this.tsMain.Text = "toolStrip1";
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtGender);
            this.panel3.Controls.Add(this.txtPName);
            this.panel3.Controls.Add(this.txtPRNo);
            this.panel3.Controls.Add(this.txtAge);
            this.panel3.Controls.Add(this.lblHName);
            this.panel3.Controls.Add(this.lblHPRNo);
            this.panel3.Controls.Add(this.lblHAge);
            this.panel3.Controls.Add(this.lblHGender);
            this.panel3.Location = new System.Drawing.Point(12, 70);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(770, 47);
            this.panel3.TabIndex = 107;
            // 
            // txtGender
            // 
            this.txtGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGender.Location = new System.Drawing.Point(521, 12);
            this.txtGender.MaxLength = 3;
            this.txtGender.Name = "txtGender";
            this.txtGender.ReadOnly = true;
            this.txtGender.Size = new System.Drawing.Size(61, 20);
            this.txtGender.TabIndex = 190;
            // 
            // txtPName
            // 
            this.txtPName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPName.Location = new System.Drawing.Point(304, 12);
            this.txtPName.MaxLength = 50;
            this.txtPName.Name = "txtPName";
            this.txtPName.ReadOnly = true;
            this.txtPName.Size = new System.Drawing.Size(145, 20);
            this.txtPName.TabIndex = 179;
            // 
            // txtPRNo
            // 
            this.txtPRNo.BackColor = System.Drawing.Color.White;
            this.txtPRNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRNo.Location = new System.Drawing.Point(149, 12);
            this.txtPRNo.Name = "txtPRNo";
            this.txtPRNo.ReadOnly = true;
            this.txtPRNo.Size = new System.Drawing.Size(73, 20);
            this.txtPRNo.TabIndex = 178;
            this.txtPRNo.TabStop = false;
            // 
            // txtAge
            // 
            this.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAge.Location = new System.Drawing.Point(638, 12);
            this.txtAge.MaxLength = 3;
            this.txtAge.Name = "txtAge";
            this.txtAge.ReadOnly = true;
            this.txtAge.Size = new System.Drawing.Size(38, 20);
            this.txtAge.TabIndex = 182;
            // 
            // lblHName
            // 
            this.lblHName.AutoSize = true;
            this.lblHName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHName.Location = new System.Drawing.Point(257, 16);
            this.lblHName.Name = "lblHName";
            this.lblHName.Size = new System.Drawing.Size(41, 13);
            this.lblHName.TabIndex = 186;
            this.lblHName.Text = "Name :";
            // 
            // lblHPRNo
            // 
            this.lblHPRNo.AutoSize = true;
            this.lblHPRNo.Location = new System.Drawing.Point(70, 16);
            this.lblHPRNo.Name = "lblHPRNo";
            this.lblHPRNo.Size = new System.Drawing.Size(44, 13);
            this.lblHPRNo.TabIndex = 189;
            this.lblHPRNo.Tag = "";
            this.lblHPRNo.Text = "MR No.";
            // 
            // lblHAge
            // 
            this.lblHAge.AutoSize = true;
            this.lblHAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAge.Location = new System.Drawing.Point(600, 16);
            this.lblHAge.Name = "lblHAge";
            this.lblHAge.Size = new System.Drawing.Size(32, 13);
            this.lblHAge.TabIndex = 184;
            this.lblHAge.Text = "Age :";
            // 
            // lblHGender
            // 
            this.lblHGender.AutoSize = true;
            this.lblHGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHGender.Location = new System.Drawing.Point(467, 16);
            this.lblHGender.Name = "lblHGender";
            this.lblHGender.Size = new System.Drawing.Size(48, 13);
            this.lblHGender.TabIndex = 183;
            this.lblHGender.Text = "Gender :";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(87, 126);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(69, 13);
            this.lblDescription.TabIndex = 110;
            this.lblDescription.Text = "Description : ";
            // 
            // dgAdmProc
            // 
            this.dgAdmProc.AllowDrop = true;
            this.dgAdmProc.AllowUserToAddRows = false;
            this.dgAdmProc.AllowUserToDeleteRows = false;
            this.dgAdmProc.AllowUserToOrderColumns = true;
            this.dgAdmProc.AllowUserToResizeColumns = false;
            this.dgAdmProc.AllowUserToResizeRows = false;
            this.dgAdmProc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAdmProc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgAdmProc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAdmProc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientAdmProc,
            this.EnteredOn,
            this.Description,
            this.EnetredBy,
            this.Type,
            this.Active});
            this.dgAdmProc.Location = new System.Drawing.Point(12, 221);
            this.dgAdmProc.MultiSelect = false;
            this.dgAdmProc.Name = "dgAdmProc";
            this.dgAdmProc.ReadOnly = true;
            this.dgAdmProc.RowHeadersWidth = 40;
            this.dgAdmProc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgAdmProc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAdmProc.Size = new System.Drawing.Size(770, 333);
            this.dgAdmProc.TabIndex = 109;
            this.dgAdmProc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAdmProc_CellDoubleClick);
            // 
            // PatientAdmProc
            // 
            this.PatientAdmProc.DataPropertyName = "AdmissionProcID";
            this.PatientAdmProc.HeaderText = "PatientAdmProcID";
            this.PatientAdmProc.Name = "PatientAdmProc";
            this.PatientAdmProc.ReadOnly = true;
            this.PatientAdmProc.Visible = false;
            // 
            // EnteredOn
            // 
            this.EnteredOn.DataPropertyName = "EnteredOn";
            this.EnteredOn.FillWeight = 20F;
            this.EnteredOn.HeaderText = "Date";
            this.EnteredOn.Name = "EnteredOn";
            this.EnteredOn.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Name";
            this.Description.FillWeight = 70F;
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // EnetredBy
            // 
            this.EnetredBy.DataPropertyName = "EnteredBy";
            this.EnetredBy.FillWeight = 45F;
            this.EnetredBy.HeaderText = "EnteredBy";
            this.EnetredBy.Name = "EnetredBy";
            this.EnetredBy.ReadOnly = true;
            this.EnetredBy.Visible = false;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.FillWeight = 40F;
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Visible = false;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "0";
            this.Active.FillWeight = 10F;
            this.Active.HeaderText = "Active";
            this.Active.IndeterminateValue = "";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "1";
            // 
            // lblAllergARHeading
            // 
            this.lblAllergARHeading.AutoSize = true;
            this.lblAllergARHeading.Location = new System.Drawing.Point(121, 196);
            this.lblAllergARHeading.Name = "lblAllergARHeading";
            this.lblAllergARHeading.Size = new System.Drawing.Size(35, 13);
            this.lblAllergARHeading.TabIndex = 108;
            this.lblAllergARHeading.Tag = "display";
            this.lblAllergARHeading.Text = "New :";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbAdmission);
            this.panel1.Controls.Add(this.rbProc);
            this.panel1.Location = new System.Drawing.Point(162, 186);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 29);
            this.panel1.TabIndex = 112;
            // 
            // rbAdmission
            // 
            this.rbAdmission.AutoSize = true;
            this.rbAdmission.Checked = true;
            this.rbAdmission.Location = new System.Drawing.Point(17, 4);
            this.rbAdmission.Name = "rbAdmission";
            this.rbAdmission.Size = new System.Drawing.Size(72, 17);
            this.rbAdmission.TabIndex = 1;
            this.rbAdmission.TabStop = true;
            this.rbAdmission.Text = "Admission";
            this.rbAdmission.UseVisualStyleBackColor = true;
            this.rbAdmission.CheckedChanged += new System.EventHandler(this.rbAdmission_CheckedChanged);
            // 
            // rbProc
            // 
            this.rbProc.AutoSize = true;
            this.rbProc.Location = new System.Drawing.Point(108, 6);
            this.rbProc.Name = "rbProc";
            this.rbProc.Size = new System.Drawing.Size(74, 17);
            this.rbProc.TabIndex = 0;
            this.rbProc.Text = "Procedure";
            this.rbProc.UseVisualStyleBackColor = true;
            this.rbProc.CheckedChanged += new System.EventHandler(this.rbProc_CheckedChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(162, 123);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(527, 57);
            this.txtDescription.TabIndex = 113;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Red;
            this.panel4.Location = new System.Drawing.Point(685, 123);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(3, 58);
            this.panel4.TabIndex = 157;
            this.panel4.Tag = "Self";
            // 
            // chbActive
            // 
            this.chbActive.AutoSize = true;
            this.chbActive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbActive.Checked = true;
            this.chbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbActive.Location = new System.Drawing.Point(397, 195);
            this.chbActive.Name = "chbActive";
            this.chbActive.Size = new System.Drawing.Size(65, 17);
            this.chbActive.TabIndex = 158;
            this.chbActive.Text = "Active : ";
            this.chbActive.UseVisualStyleBackColor = true;
            // 
            // lblAdmProcID
            // 
            this.lblAdmProcID.AutoSize = true;
            this.lblAdmProcID.Location = new System.Drawing.Point(12, 205);
            this.lblAdmProcID.Name = "lblAdmProcID";
            this.lblAdmProcID.Size = new System.Drawing.Size(0, 13);
            this.lblAdmProcID.TabIndex = 159;
            this.lblAdmProcID.Visible = false;
            // 
            // frmAdmissionProc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.lblAdmProcID);
            this.Controls.Add(this.chbActive);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgAdmProc);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblAllergARHeading);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmAdmissionProc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Addmission and Procedure";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAdmissionProc_FormClosing);
            this.Load += new System.EventHandler(this.frmAdmissionProc_Load);
            this.panel2.ResumeLayout(false);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdmProc)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton miSave;
        private System.Windows.Forms.ToolStripButton miRefresh;
        private System.Windows.Forms.ToolStripButton miExit;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtPName;
        private System.Windows.Forms.MaskedTextBox txtPRNo;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label lblHName;
        private System.Windows.Forms.Label lblHPRNo;
        private System.Windows.Forms.Label lblHAge;
        private System.Windows.Forms.Label lblHGender;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.DataGridView dgAdmProc;
        private System.Windows.Forms.Label lblAllergARHeading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbAdmission;
        private System.Windows.Forms.RadioButton rbProc;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chbActive;
        private System.Windows.Forms.Label lblAdmProcID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientAdmProc;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnetredBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
    }
}