using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using OPD_BL;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

namespace OPD
{
    public partial class frmReport : Form
    {
        
        #region "Class Variables"

        private string StrReportReference = "";
        private string _OPDID = "";
        private string _PatientID = "";
        private string _History= "";
        private string _Complaints= "";
        private string _Plans = "";
        private string _Disgnosis = "";
        private string _Signs = "";
        private string _Investigation= "";
        private string _DisplayOption = "";
        private frmOPD ParentForm = null;
        private string _PatientName = "";
        private string _instruction = "";
        private string _PRNo = "";
        private string _TopN = "";
        private string _VisitDate = "";
        private string _Age = "";
        private string _Gender = "";
        public string _Password = "";
        private string _UserName = "";
        private decimal _PrintValue = 1;

        #endregion

        #region "Properties"

        public string ReportReference
        {
            set { StrReportReference = value; }
        }
        public string OPDID
        {
            get { return _OPDID; }
            set { _OPDID = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public decimal PrintValue
        {
            get { return _PrintValue; }
            set { _PrintValue = value; }
        }
        public string Instruction
        {
            get { return _instruction; }
            set { _instruction = value; }
        }
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string VisitDate
        {
            get { return _VisitDate; }
            set { _VisitDate = value; }
        }
        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public string Age
        {
            get { return _Age; }
            set { _Age = value; }
        }
        public string PatientName
        {
            get { return _PatientName; }
            set { _PatientName = value; }
        }

        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }

        public string History
        {
            get { return _History; }
            set { _History = value; }
        }

        public string Complaints
        {
            get { return _Complaints; }
            set { _Complaints = value; }
        }

        public string Plans
        {
            get { return _Plans; }
            set { _Plans  = value; }
        }

        public string Diagnosis
        {
            get { return _Disgnosis ; }
            set { _Disgnosis  = value; }
        }

        public string Investigation
        {
            get { return _Investigation; }
            set { _Investigation= value; }
        }

        public string Signs
        {
            get { return _Signs; }
            set { _Signs = value; }
        }

        public string DisplayOpt
        {
            set { _DisplayOption = value; }
        }

        public string PRNo
        {
            set { _PRNo = value; }
        }

        public string TopN
        {
            set { _TopN = value; }
        }

        #endregion

        public frmReport()
        {
            InitializeComponent();
        }

        public frmReport(frmOPD frm)
        {
            InitializeComponent();
            ParentForm = frm;
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            this.Text += " - (" + clsSharedVariables.UserName + ")";
            objComp.ApplyStyleToControls(this);
            LoadReport();
            //MedicinePerscription();
            objComp = null;
        }
        public async Task<bool> LoadReport()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            ReportDocument repdoc = new ReportDocument();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            clsBLDBConnection objConnection1 = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection1);
            objConnection1.Connection_Open();
            DataView DvMedicine;
            objOPDMed.OPDID = _OPDID;
            DvMedicine = objOPDMed.GetAll(5);
            objConnection1.Connection_Close();
            DataView dvPrefTabl;
            int nCopy = Convert.ToInt16(_PrintValue);
            try
            {
                string strPath = Application.StartupPath + "\\Report\\" + StrReportReference + ".rpt";
                repdoc.Load(strPath);
                           
                //repdoc.Refresh();
                //int j = repdoc.Database.Tables.Count - 1;

                //for (int i = 0; i <= j; i++)
                //{
                //    TableLogOnInfo logOnInfo = new TableLogOnInfo();
                //    ConnectionInfo connectionInfo = new ConnectionInfo();

                //    logOnInfo = repdoc.Database.Tables[i].LogOnInfo;

                //    connectionInfo = logOnInfo.ConnectionInfo;
                //    connectionInfo.ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();
                //    connectionInfo.Password = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
                //    connectionInfo.UserID = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
                //    repdoc.Database.Tables[i].ApplyLogOnInfo(logOnInfo);
                //}
                repdoc.Database.Tables["Medicine"].SetDataSource(DvMedicine.Table);
                objRef.ReportRefrence = StrReportReference;
                objConnection.Connection_Open();
                dvPrefTabl = objRef.GetAll(1);
                objConnection.Connection_Close();
                try
                {
                    repdoc.SetParameterValue("VisitDate", _VisitDate);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Age", _Age);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Gender", _Gender);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Instruction", _instruction);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("UserName", _UserName);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Password", _Password);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("PatientName", _PatientName);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("PRNo", _PRNo);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("OPDID", _OPDID);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("PersonalName", clsSharedVariables.UserName);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("History", _History);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Complaints", _Complaints);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Plans", _Plans);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Signs", _Signs);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Investigation", _Investigation);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Diagnosis", _Disgnosis);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Language", clsSharedVariables.Language);
                }
                catch { }

                try
                {
                    var CompanyInfo= System.Configuration.ConfigurationSettings.AppSettings["CompanyInfo"].ToString();
                    repdoc.SetParameterValue("CompanyInfo", CompanyInfo);
                }
                catch { }


                if (_DisplayOption.Equals("Print"))
                {
                    try
                    {
                        
                      

                      
                        if (!_OPDID.Equals(""))
                        {
                            var pdfName=_PRNo +" "+_PatientName+" "+_VisitDate;
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                                "\\EmailRpt\\" + pdfName + ".pdf");
                            string Path = Application.StartupPath +
                                "\\EmailRpt";
                            string FileName = pdfName + ".pdf";
                           //await SendFileToApi(Path, FileName);
                        }
                        repdoc.PrintToPrinter(nCopy, true, 1, 4);
                    }
                    catch(Exception ex)
                    {
                        repdoc.PrintToPrinter(nCopy, true, 1, 4);
                        this.Close();
                    }

                   
                     this.crystalReportViewer1.ReportSource = repdoc;
                }
                else if (_DisplayOption.Equals("Display"))
                {
                    this.crystalReportViewer1.ReportSource = repdoc;
                }
                else if (_DisplayOption.Equals("Export"))
                {
                    if (!_OPDID.Equals(""))
                    {
                        repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                            "\\EmailRpt\\" + _OPDID + ".pdf");
                    }
                    else
                    {
                        try
                        {
                            repdoc.SetParameterValue("N", _TopN);
                        }
                        catch { }

                        if (_TopN.Equals("1000"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + ".pdf");
                        }
                        else if (_TopN.Equals("10"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + "10.pdf");
                        }
                        else if (_TopN.Equals("5"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + "5.pdf");
                        }
                        repdoc.Dispose();
                        this.crystalReportViewer1.Dispose();
                    }
                    this.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                repdoc = null;
                objRef = null;
                dvPrefTabl = null;
                objConnection = null;
            }
            return true;
        }

        public async Task<string> SendFileToApi(string pathh,string filename)
        {
            var res = "";
            try
            {

                var BaseURL = System.Configuration.ConfigurationSettings.AppSettings["BaseURL"].ToString();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string path = pathh + "\\" + filename;
                    Byte[] bytes = File.ReadAllBytes(path);
                    String fileBase64 = Convert.ToBase64String(bytes);
                    var n = new PatientFile();
                    n.FileName = fileBase64;
                    n.UserName = _UserName;
                    n.VisitDate = _VisitDate;
                    // New code:
                    HttpResponseMessage response = await client.PostAsJsonAsync("api/Patient/SaveOPDDoc", n);
                    if (response.IsSuccessStatusCode)
                    {
                        res = await response.Content.ReadAsAsync<string>();
                        ////////////////////////////////////////////////////////////
                        clsBLDBConnection objConnection1 = new clsBLDBConnection();
                        clsBLOPD objOPD = new clsBLOPD(objConnection1);


                        objConnection1.Connection_Open();
                       
                        objOPD.SetOPDFileName(_OPDID,filename,1);

                    }
                    else
                    {
                        ////////////////////////////////////////////////////////////
                        clsBLDBConnection objConnection1 = new clsBLDBConnection();
                        clsBLOPD objOPD = new clsBLOPD(objConnection1);


                        objConnection1.Connection_Open();

                       objOPD.SetOPDFileName(_OPDID, filename, 0);
                    }
                }

            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        //public void LoadReport()
        //{
        //    clsBLDBConnection objConnection = new clsBLDBConnection();
        //    ReportDocument repdoc = new ReportDocument();
        //    clsBLReferences objRef = new clsBLReferences(objConnection);
        //    DataView dvPrefTabl;
        //    try
        //    {
        //        string strPath = Application.StartupPath + "\\Report\\" + StrReportReference + ".rpt";
        //        repdoc.Load(strPath);
        //        if (!_OPDID.Equals(""))
        //        {
        //            repdoc.RecordSelectionFormula = "{cm_topd.OPDID} = " + _OPDID;




        //            //repdoc.RecordSelectionFormula = "{cm_topd.Status} <> 'W' and {cm_topd.PatientID}= " + _PatientID;

        //            //repdoc.RecordSelectionFormula = "{cm_topd.OPDID} = " + _OPDID + " and {cm_topd.PatientID}={cm_tpatientreg.PatientID} and {cm_tpersonal.PersonID}={cm_tpatientreg.EnteredBy} and {cm_tpersonal.PersonID}= {cm_topd.EnteredBy}";
        //            // and {cm_tpersonal.PersonID}={cm_tpreference.PersonID} and {cm_ttype.PersonID}={cm_tpersonal.PersonID}";
        //        }
        //        if (!_PRNo.Equals(""))
        //        {
        //            repdoc.RecordSelectionFormula = "{cm_topd.Status} <> 'W' and {cm_topd.PatientID}= " + _PatientID;
        //            //repdoc.RecordSelectionFormula = "{cm_topd.OPDID} = " + _OPDID + " and {cm_topd.PatientID}={cm_tpatientreg.PatientID} and {cm_tpersonal.PersonID}={cm_tpatientreg.EnteredBy} and {cm_tpersonal.PersonID}= {cm_topd.EnteredBy}";
        //            // and {cm_tpersonal.PersonID}={cm_tpreference.PersonID} and {cm_ttype.PersonID}={cm_tpersonal.PersonID}";
        //        }
        //        //else and {cm_tpersonal.PersonID}={cm_tpatientreg.EnteredBy} 
        //        //{
        //        //    repdoc.RecordSelectionFormula = "{cm_tpatientreg.patientid} = " + _PatientID;
        //        //}
        //        repdoc.Refresh();
        //        int j = repdoc.Database.Tables.Count - 1;

        //        for (int i = 0; i <= j; i++)
        //        {
        //            TableLogOnInfo logOnInfo = new TableLogOnInfo();
        //            ConnectionInfo connectionInfo = new ConnectionInfo();

        //            logOnInfo = repdoc.Database.Tables[i].LogOnInfo;

        //            connectionInfo = logOnInfo.ConnectionInfo;
        //            connectionInfo.ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();
        //            connectionInfo.Password = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
        //            connectionInfo.UserID = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
        //            repdoc.Database.Tables[i].ApplyLogOnInfo(logOnInfo);
        //        }

        //        objRef.ReportRefrence = StrReportReference;
        //        objConnection.Connection_Open();
        //        dvPrefTabl = objRef.GetAll(1);
        //        objConnection.Connection_Close();
        //        try
        //        {
        //            repdoc.SetParameterValue("OPDID", _OPDID);
        //        }
        //        catch { }
        //        try
        //        {
        //            repdoc.SetParameterValue("PersonalName", clsSharedVariables.UserName);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("History", _History);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("Complaints", _Complaints);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("Plans", _Plans);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("Signs", _Signs);
        //        }
        //        catch { }
        //        try
        //        {
        //            repdoc.SetParameterValue("Investigation", _Investigation);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("Diagnosis", _Disgnosis);
        //        }
        //        catch { }

        //        try
        //        {
        //            repdoc.SetParameterValue("Language", clsSharedVariables.Language);
        //        }
        //        catch { }

        //        if (_DisplayOption.Equals("Print"))
        //        {
        //            this.crystalReportViewer1.ReportSource = repdoc;
        //        }


        //        //if (_DisplayOption.Equals("Print"))
        //        //{
        //        //    try
        //        //    {
        //        //         this.crystalReportViewer1.ReportSource = repdoc;
        //        //        int nCopy = Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings["Print"].ToString());

        //        //        repdoc.PrintToPrinter(nCopy, true, 1, 4);
        //        //    }
        //        //    catch
        //        //    {
        //        //        repdoc.PrintToPrinter(1, true, 1, 4);
        //        //    }

        //        //    this.Close();
        //        //   // this.crystalReportViewer1.ReportSource = repdoc;
        //        //}
        //        else if (_DisplayOption.Equals("Display"))
        //        {
        //            this.crystalReportViewer1.ReportSource = repdoc;
        //        }
        //        else if (_DisplayOption.Equals("Export"))
        //        {
        //            if (!_OPDID.Equals(""))
        //            {
        //                repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
        //                    "\\EmailRpt\\" + _OPDID + ".pdf");
        //            }
        //            else
        //            {
        //                try
        //                {
        //                    repdoc.SetParameterValue("N", _TopN);
        //                }
        //                catch { }

        //                if (_TopN.Equals("1000"))
        //                {
        //                    repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
        //                      "\\EmailRpt\\" + _PRNo + ".pdf");
        //                }
        //                else if (_TopN.Equals("10"))
        //                {
        //                    repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
        //                      "\\EmailRpt\\" + _PRNo + "10.pdf");
        //                }
        //                else if (_TopN.Equals("5"))
        //                {
        //                    repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
        //                      "\\EmailRpt\\" + _PRNo + "5.pdf");
        //                }
        //                repdoc.Dispose();
        //                this.crystalReportViewer1.Dispose();
        //            }
        //            this.Close();
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    finally
        //    {
        //        repdoc = null;
        //        objRef = null;
        //        dvPrefTabl = null;
        //        objConnection = null;
        //    }
        //}
        public void MedicinePerscription()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            ReportDocument repdoc = new ReportDocument();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            DataView DvMedicine;
            clsBLDBConnection objConnection1 = new clsBLDBConnection();
            clsBLOPDMedicine objOPDMed = new clsBLOPDMedicine(objConnection1);
            objConnection1.Connection_Open();
            objOPDMed.OPDID = _OPDID;
            DvMedicine = objOPDMed.GetAll(5);
            objConnection1.Connection_Close();
            DataView dvPrefTabl;
            try
            {
                string strPath = Application.StartupPath + "\\Report\\" + StrReportReference + ".rpt";
                repdoc.Load(strPath);
             
                int j = repdoc.Database.Tables.Count - 1;

                for (int i = 0; i <= j; i++)
                {
                    TableLogOnInfo logOnInfo = new TableLogOnInfo();
                    ConnectionInfo connectionInfo = new ConnectionInfo();

                    logOnInfo = repdoc.Database.Tables[i].LogOnInfo;

                    connectionInfo = logOnInfo.ConnectionInfo;
                    connectionInfo.ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();
                    connectionInfo.Password = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
                    connectionInfo.UserID = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
                    repdoc.Database.Tables[i].ApplyLogOnInfo(logOnInfo);
                }
                repdoc.Database.Tables["Medicine"].SetDataSource(DvMedicine.Table);

                //repdoc.SetDataSource(DvMedicine.Table);
                objRef.ReportRefrence = StrReportReference;
                objConnection.Connection_Open();
                dvPrefTabl = objRef.GetAll(1);
                objConnection.Connection_Close();

                try
                {
                    repdoc.SetParameterValue("PersonalName", clsSharedVariables.UserName);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("PatientName", _PatientName);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("PatientId", _PRNo);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("History", _History);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Complaints", _Complaints);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Plans", _Plans);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Signs", _Signs);
                }
                catch { }
                try
                {
                    repdoc.SetParameterValue("Investigation", _Investigation);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Diagnosis", _Disgnosis);
                }
                catch { }

                try
                {
                    repdoc.SetParameterValue("Language", clsSharedVariables.Language);
                }
                catch { }

                if(_DisplayOption.Equals("Print"))
                {
                    this.crystalReportViewer1.ReportSource = repdoc;
                }


                if (_DisplayOption.Equals("Print"))
                {
                    try
                    {
                        this.crystalReportViewer1.ReportSource = repdoc;
                        int nCopy = Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings["Print"].ToString());

                        repdoc.PrintToPrinter(nCopy, true, 1, 4);
                    }
                    catch
                    {
                        repdoc.PrintToPrinter(1, true, 1, 4);
                    }

                    //this.Close();
                    // this.crystalReportViewer1.ReportSource = repdoc;
                }
                else if (_DisplayOption.Equals("Display"))
                {
                    this.crystalReportViewer1.ReportSource = repdoc;
                }
                else if (_DisplayOption.Equals("Export"))
                {
                    if (!_OPDID.Equals(""))
                    {
                        repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                            "\\EmailRpt\\" + _OPDID + ".pdf");
                    }
                    else
                    {
                        try
                        {
                            repdoc.SetParameterValue("N", _TopN);
                        }
                        catch { }

                        if (_TopN.Equals("1000"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + ".pdf");
                        }
                        else if (_TopN.Equals("10"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + "10.pdf");
                        }
                        else if (_TopN.Equals("5"))
                        {
                            repdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath +
                              "\\EmailRpt\\" + _PRNo + "5.pdf");
                        }
                        repdoc.Dispose();
                        this.crystalReportViewer1.Dispose();
                    }
                    this.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                repdoc = null;
                objRef = null;
                dvPrefTabl = null;
                objConnection = null;
            }
        }

        private void frmReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            ParentForm.Show();
            this.Dispose();
        }
    }
}