using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmDocument : Form
    {
        public frmDocument()
        {
            InitializeComponent();
        }

        # region "Class variables" and "Properties"

        private string _PatientID = "";
        private string _PatientName = "";
        private string _Path = "";

        private frmOPD MainOPD = null;

        //private string _FileName = "";
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        public string PatientName
        {
            set { _PatientName = value; }
        }

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }

        #endregion

        public frmDocument(string PatientID)
        {
            InitializeComponent();
            _PatientID = PatientID;
        }

        public frmDocument(string PatientID, frmOPD Frm)
        {
            InitializeComponent();
            _PatientID = PatientID;
            this.MainOPD = Frm;
        }

        private void frmDocument_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);

            this.Text += " ( " + clsSharedVariables.InItCaps(_PatientName) + " )";
            objComp.ApplyStyleToControls(this);
            try
            {
                objConnection.Connection_Open();
                FillDocType(objConnection);
                FillDatagrid(objConnection, objVdo);
                objConnection.Connection_Close();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objVdo = null;
                objComp = null;
            }
        }

        private void FillDocType(clsBLDBConnection objConnection)
        {
            clsBLDocType objDocType = new clsBLDocType(objConnection);
            SComponents objComp = new SComponents();

            objDocType.Type = "D";
            objComp.FillComboBox(cboDocType, objDocType.GetAll(2), "Name", "DocTypeID", true);

            objDocType = null;
            objComp = null;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            btnClear_click(btnClear, e);
            SelectDoc();
        }

        private void SelectDoc()
        {
            OFDDoc.Filter = "Document (*.Doc;*.DocX;*.Pdf;*.Xls;*.XlsX;*.txt;)|*.Doc;*.DocX;*.Pdf;*.Xls;*.XlsX;*.txt| All files (*.*)|*.*";

            // Allow the user to select multiple images.
            OFDDoc.Multiselect = false;
            OFDDoc.Title = "My Document Browser";
            DialogResult dr = OFDDoc.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtPath.Text = OFDDoc.FileName;
                _Path = OFDDoc.FileName;
                btnSave.Enabled = true;
                btnCopy.Enabled = true;
                txtComment.Text = "";
            }
            else
            {
                btnSave.Enabled = false;
                btnCopy.Enabled = false;
            }

            OFDDoc.Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text.Equals("Save"))
            {
                Insert('P');
            }
            else
            {
                UpdateData('P');
                btnSave.Text = "&Save";
            }

            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            txtComment.Text = "";
            txtPath.Text = "";
            cboDocType.SelectedIndex = 0;
        }

        private void Insert(char Mode)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objDoc = new clsBLPatientPicture(objConnection);
            string msg = "";

            try
            {
                if (!_Path.Equals(""))
                {
                    if (Mode == 'S')
                    {
                        objDoc.SmallPicture = Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path);
                        msg = "Copy File Successfully";
                    }
                    else if (Mode == 'P')
                    {
                        objDoc.SmallPicture = _Path.Replace("\\", "\\\\");
                        msg = "Save Path Successfully";
                    }
                    objDoc = SetBLValues(objDoc, Mode);

                    objConnection.Connection_Open();
                    objConnection.Transaction_Begin();
                    if (objDoc.Insert())
                    {
                        objConnection.Transaction_ComRoll();
                        FillDatagrid(objConnection, objDoc);


                        MessageBox.Show(msg, "Save", MessageBoxButtons.OK, MessageBoxIcon.
            Information);
                    }
                    else
                    {
                        MessageBox.Show(objDoc.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objDoc = null;
                objConnection = null;
                msg = null;
            }
        }

        private void UpdateData(char Mode)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objDoc = new clsBLPatientPicture(objConnection);
            string msg = "";

            try
            {
                objDoc.PictureID = this.lblPictureID.Text;
                if (Mode == 'S')
                {
                    objDoc.SmallPicture = Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path);
                    msg = "Update Copying File Successfully";
                }
                else if (Mode == 'P')
                {
                    objDoc.SmallPicture = _Path.Replace("\\", "\\\\");
                    msg = "Update path Successfully";
                }
                objDoc = SetBLValues(objDoc, Mode);

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objDoc.Update())
                {
                    objConnection.Transaction_ComRoll();
                    FillDatagrid(objConnection, objDoc);
                    objConnection.Connection_Close();

                    MessageBox.Show(msg, "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(objDoc.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objDoc = null;
                objConnection = null;
            }
        }

        private clsBLPatientPicture SetBLValues(clsBLPatientPicture objDoc, int Mode)
        {
            objDoc.PatientID = PatientID;
            if (Mode == 'S')
            {
                objDoc.SavedMethod = "DS";//Document Save
                objDoc.Comment = clsSharedVariables.InItCaps(txtComment.Text.Trim());
            }
            else if (Mode == 'P')
            {
                objDoc.SavedMethod = "DP";//Document Path
                objDoc.Comment = clsSharedVariables.InItCaps(txtComment.Text.Trim());
            }
            if (cboDocType.SelectedIndex != 0)
            {
                objDoc.DocTypeID = cboDocType.SelectedValue.ToString();
            }
            objDoc.EnteredOn=DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            return objDoc;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objDoc = new clsBLPatientPicture(objConnection);

            try
            {
                if (lblMode.Text.Equals("C"))
                {
                    if (!System.IO.File.Exists(_Path.Replace("\\", "")))
                    {
                        System.IO.File.Delete(_Path);
                        //Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(_Path,Microsoft.VisualBasic.FileIO.UIOption
                    }
                }
                objConnection.Connection_Open();
                objDoc.PictureID = lblPictureID.Text;
                objDoc.Delete();
                FillDatagrid(objConnection, objDoc);
                btnDelete.Enabled = false;
                btnOpen.Enabled=false;
                btnSave.Enabled = false;
                btnCopy.Enabled = false;
                txtPath.Text = "";
            }
            finally
            {
                objConnection.Connection_Close();
                btnDelete.Enabled = false;
                btnOpen.Enabled = false;
                objDoc = null;
            }
        }

        private void btnClear_click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            txtPath.Text = "";
            _Path = "";
            txtComment.Text = "";
            btnDelete.Enabled = false;
            btnOpen.Enabled = false;
            dgDoc.ClearSelection();
            btnSave.Text = "Save";
        }

        private void FillDatagrid(clsBLDBConnection objConnection, clsBLPatientPicture objDoc)
        {
            DataView dvDoc;
            try
            {
                objDoc.PatientID = PatientID;
                if (cboDocType.SelectedIndex != 0)
                {
                    objDoc.DocTypeID = cboDocType.SelectedValue.ToString();
                }

                dvDoc = objDoc.GetAll(4);
                dgDoc.DataSource = dvDoc;

                for (int i = 0; i < dgDoc.Rows.Count; i++)
                {
                    dgDoc.Rows[i].Cells["comments"].Value = dgDoc.Rows[i].Cells["comments"].Value.ToString().Trim();
                    if (dgDoc.Rows[i].Cells["SavedMethod"].Value.ToString().Contains("DS"))
                    {
                        dgDoc.Rows[i].Cells["Mode"].Value = "S";
                    }
                    else if (dgDoc.Rows[i].Cells["SavedMethod"].Value.ToString().Contains("DP"))
                    {
                        dgDoc.Rows[i].Cells["Mode"].Value = "P";
                    }
                }
            }
            finally
            {
                dvDoc = null;
            }
        }

        private void dgDoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                btnDelete.Enabled = true;
                btnOpen.Enabled = true ;
                FillHoldPatient(e.RowIndex);
            }
        }

        private void FillHoldPatient(int RowIndex)
        {
            btnSave.Text = "Update";
            btnSave.Enabled = true;
            btnCopy.Enabled = true;

            txtComment.Text = clsSharedVariables.InItCaps(dgDoc.Rows[RowIndex].Cells["Comments"].Value.ToString());
            if (dgDoc.Rows[RowIndex].Cells["Mode"].Value.Equals("S"))
            {
                txtPath.Text = Application.StartupPath + "\\Document\\" + dgDoc.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
                _Path = Application.StartupPath + "\\Document\\" + dgDoc.Rows[RowIndex].Cells["vdopath"].Value.ToString().Replace(":", @":\");
                lblMode.Text = "C";
            }
            else if (dgDoc.Rows[RowIndex].Cells["Mode"].Value.Equals("P"))
            {
                lblMode.Text = "P";
                txtPath.Text = dgDoc.Rows[RowIndex].Cells["vdopath"].Value.ToString();
                _Path = dgDoc.Rows[RowIndex].Cells["vdopath"].Value.ToString();
            }

          

            lblPatientID.Text = dgDoc.Rows[RowIndex].Cells["GridPatientID"].Value.ToString();
            lblPictureID.Text = dgDoc.Rows[RowIndex].Cells["PictureID"].Value.ToString();
            if (dgDoc.Rows[RowIndex].Cells["DocTypeID"].Value.ToString().Equals(""))
            {
                cboDocType.SelectedIndex = 0;
            }
            else
            {
                cboDocType.SelectedValue = dgDoc.Rows[RowIndex].Cells["DocTypeID"].Value.ToString();
            }

            if (System.IO.File.Exists(_Path))
            {
                System.Diagnostics.Process objPro = new System.Diagnostics.Process();
                objPro.StartInfo.FileName = _Path;
                objPro.Start();
                //objPro.WaitForExit();
            }
            else
            {
                MessageBox.Show("File Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtInItCap_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            txt.Text = clsSharedVariables.InItCaps(txt.Text);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (btnSave.Text.Equals("Save"))
            {
                Insert('S');
            }
            else
            {
                UpdateData('S');
                btnSave.Text = "Save";
            }
            if (!Microsoft.VisualBasic.FileIO.FileSystem.FileExists(Application.StartupPath + "\\Document\\" + Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path)))
            {
                Microsoft.VisualBasic.FileIO.FileSystem.CopyFile(_Path, Application.StartupPath + "\\Document\\" + Microsoft.VisualBasic.FileIO.FileSystem.GetName(_Path));
            }
            btnSave.Enabled = false;
            btnCopy.Enabled = false;
            txtComment.Text = "";
            txtPath.Text = "";
            cboDocType.SelectedIndex = 0;
            _Path = "";
        }

        private void frmDocument_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }

        private void btnAddNewType_Click(object sender, EventArgs e)
        {
            frmDocType objDocType = new frmDocType(this);
            this.Hide();
            objDocType.Show();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            #region Run selected file
            try
            {
                if (System.IO.File.Exists(_Path))
                {
                    System.Diagnostics.Process objPro = new System.Diagnostics.Process();
                    objPro.StartInfo.FileName = _Path;
                    objPro.Start();
                    objPro.WaitForExit();
                }
                else
                {
                    MessageBox.Show("File Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void cboDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientPicture objVdo = new clsBLPatientPicture(objConnection);

            try
            {
                objConnection.Connection_Open();
                FillDatagrid(objConnection, objVdo);
                objConnection.Connection_Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                objConnection = null;
                objVdo = null;
            }
        }
    }
}