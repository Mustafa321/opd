using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmSetting : Form
    {
        private frmOPD MainOPD = null;

        public frmSetting()
        {
            InitializeComponent();
        }

        public frmSetting(frmOPD Frm)
        {
            InitializeComponent();
            this.MainOPD = Frm;
        }

        private void frmSetting_Load(object sender, EventArgs e)
        {
            chbAutoPRNO.Checked = clsSharedVariables.AutoPRNO;
            RenameHeading.Checked = clsSharedVariables.RenameHeading;
            Appointment.Checked = clsSharedVariables.Appointment;
            WaitingQueue.Checked = clsSharedVariables.WaitingQueue;
            Notes.Checked = clsSharedVariables.Notes;
            Allergies.Checked = clsSharedVariables.Allergies;
            VitalSign.Checked = clsSharedVariables.VitalSign;
            Medication.Checked = clsSharedVariables.Medication;
            PatientDocuments.Checked = clsSharedVariables.PatientDocuments;
            AdmissionAndProcedure.Checked = clsSharedVariables.AdmissionAndProcedure;
            PatientVisits.Checked = clsSharedVariables.PatientVisits;
          
        }

        private void frmSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.MainOPD.Show();
            this.Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLReferences objRef = new clsBLReferences(objConnection);
            try
            {
                //objRef.Description = chbAutoPRNO.Checked ? "Y" : "N";
                objRef.Description = "Y";

                objRef.ReportRefrence = "AutoPRNO";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.AutoPRNO = chbAutoPRNO.Checked;
                    
                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //Rename checkBox
                objRef.Description = RenameHeading.Checked ? "Y" : "N";

                objRef.ReportRefrence = "RenameHead";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.RenameHeading = RenameHeading.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //Appointment Checkbox
                objRef.Description = Appointment.Checked ? "Y" : "N";

                objRef.ReportRefrence = "Appointment";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.Appointment = Appointment.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //
                //Waiting Queue
                objRef.Description = WaitingQueue.Checked ? "Y" : "N";

                objRef.ReportRefrence = "WaitingQueue";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.WaitingQueue = WaitingQueue.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Notes
                objRef.Description = Notes.Checked ? "Y" : "N";

                objRef.ReportRefrence = "Notes";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.Notes = Notes.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Allergies
                objRef.Description = Allergies.Checked ? "Y" : "N";

                objRef.ReportRefrence = "Allergies";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.Allergies = Allergies.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Vital Sign
                objRef.Description = VitalSign.Checked ? "Y" : "N";

                objRef.ReportRefrence = "VitalSign";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.VitalSign = VitalSign.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Rx/Medication
                objRef.Description = Medication.Checked ? "Y" : "N";

                objRef.ReportRefrence = "Medication";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.Medication = Medication.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Patient Documents
                objRef.Description = PatientDocuments.Checked ? "Y" : "N";

                objRef.ReportRefrence = "PatientDocuments";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.PatientDocuments = PatientDocuments.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Admission And Procedure
                objRef.Description = AdmissionAndProcedure.Checked ? "Y" : "N";

                objRef.ReportRefrence = "AdmissionAndProcedure";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.AdmissionAndProcedure = AdmissionAndProcedure.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                //Patient Visits
                objRef.Description = PatientVisits.Checked ? "Y" : "N";

                objRef.ReportRefrence = "PatientVisits";

                objConnection.Connection_Open();
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    clsSharedVariables.PatientVisits = PatientVisits.Checked;

                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ////////////////////////////////////
                objRef.Description = !txtMaxRecTime.Text.Equals("")? txtMaxRecTime.Text:"90";

                objRef.ReportRefrence = "MaxRecTime";
                objConnection.Transaction_Begin();
                if (objRef.Update())
                {
                    objConnection.Transaction_ComRoll();
                    if(!txtMaxRecTime.Text.Equals(""))
                    {
                        clsSharedVariables.MaxRecTime = Convert.ToInt16(txtMaxRecTime.Text);
                    }
                    else
                    {
                    clsSharedVariables.MaxRecTime = 90;
                    }
                }
                else
                {
                    MessageBox.Show(objRef.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                this.Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();
                objRef = null;
                objConnection = null;
            }
        }

        private void txtMaxRecTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            string tmp = "0123456789\b";
            e.Handled = !(tmp.Contains(e.KeyChar.ToString()));
        }
    }
}