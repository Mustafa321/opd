using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OPD
{
    public partial class frmReg : Form
    {
        private string _Validation = "";

        public string Validation
        {
            get { return _Validation; }
        }

        public frmReg()
        {
            InitializeComponent();
        }

        private void frmReg_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);

            objComp = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _Validation = "";
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string Info = "";
            string[] InfoArr;
            if (txtKey.Text.Length != 29)
            {
                MessageBox.Show("InValid Key", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (System.IO.File.Exists("License.lic"))
            {
                try
                {
                    Info = System.IO.File.ReadAllText("License.lic");
                    InfoArr = DecryptKey(Info.Trim()).Split('\n'); 
                    if (InfoArr.Length == 5)
                    {
                        if (!InfoArr[0].ToUpper().Contains(txtName.Text.Trim().ToUpper()))
                        {
                            MessageBox.Show("InValid User Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtName.Text = "";
                        }
                        else if (!InfoArr[1].ToUpper().Contains(txtID.Text.Trim().ToUpper()))
                        {
                            MessageBox.Show("InValid User ID ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtID.Text = "";
                        }
                        else if (!InfoArr[2].ToUpper().Contains(txtKey.Text.ToUpper()))
                        {
                            MessageBox.Show("InValid Key", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtKey.Text = "";
                        }
                        else if (DateTime.Compare(DateTime.ParseExact(InfoArr[4].Substring(9, 10), "dd/MM/yyyy", null), DateTime.Now) == -1)
                        {
                            MessageBox.Show("Expired License", "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtKey.Text = "";
                        }
                        else
                        {
                            MessageBox.Show("Registration Complete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            _Validation =txtKey.Text;
                            clsSharedVariables.UserName = txtName.Text;
                            clsSharedVariables.LoginID = txtID.Text;
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid License", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtKey.Text = "";
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtKey.Text = "";
                }
            }
            else
            {
                MessageBox.Show("License File Is Not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtKey.Text = "";
            }
        }

        private string DecryptKey(string Info)
        {
            string tmpInfo = "";
            for (int i = Info.Length - 1; i >= 0; i--)
            {
                tmpInfo = tmpInfo + (char)(Info[i] + 10);
            }

            return tmpInfo;
        }

        private void txtKey_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnOK_Click(null, EventArgs.Empty);
            }
        }
    }
}