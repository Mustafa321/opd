using System;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace OPD
{
    /// <summary>
    /// Summary description for Components.
    /// </summary>
    public class SComponentsMain
    {
        //private Color FormColor = Color.FromArgb(226, 232, 237);
        //private Color PanelColorLight = Color.WhiteSmoke;
        //private Color PanelColorDark = Color.FromArgb(189, 216, 234);
        //private Color HeadingColor = Color.FromArgb(25, 100, 155);

        private Color FormColor = Color.WhiteSmoke;
        //private Color PanelColorLight = Color.White ;
        private Color PanelColorLight = Color.Lavender;
        //private Color PanelColorMedium = Color.Gainsboro;
        private Color PanelColorMedium = Color.LightSteelBlue;

        //private Color SelfColor = Color.FromArgb(235, 252, 222);
        private Color SelfColor = Color.Gainsboro;
        //private Color PanelColorDark = Color.LightBlue;
        private Color PanelColorDark = Color.DarkGray;
        private Color HeadingColor = Color.FromArgb(25, 80, 185);
        private Color BtnColor = Color.LightSlateGray;

        private PaintEventArgs _Pea;

        int i_StandardHeight = 0;
        int i_StandardWidth = 0;
        int i_PresentHeight = Screen.PrimaryScreen.WorkingArea.Height;
        int i_PresentWidth = Screen.PrimaryScreen.WorkingArea.Width;
        float f_HeightRatio = new float();
        float f_WidthRatio = new float();

        public SComponentsMain(int H,int W)
        {
            i_StandardHeight = H;
            i_StandardWidth = W;

            f_HeightRatio = (float)((float)i_PresentHeight / (float)i_StandardHeight);
            f_WidthRatio = (float)((float)i_PresentWidth / (float)i_StandardWidth);
        }

        public SComponentsMain(PaintEventArgs pea)
        {
            this._Pea = pea;
        }

        # region "Applying style to controls"

        /// <summary>
        /// Currently it can apply style to only Buttons, DataGrids, TextBoxes & Form
        /// </summary>
        /// <param name="f"></param>
        public void ApplyStyleToControls(Form f)
        {
            f.BackColor = FormColor;

            // root level controls
            foreach (Control c1 in f.Controls)
            {
                if (c1.GetType() == typeof(System.Windows.Forms.GroupBox) || c1.GetType() == typeof(System.Windows.Forms.Panel))
                {
                    if (c1.GetType() == typeof(System.Windows.Forms.GroupBox))
                    {
                        c1.BackColor = FormColor;
                        GroupBox gb = (GroupBox)c1;
                        ApplyStyleToGroupBox(gb, FormColor);
                    }
                    else if (c1.GetType() == typeof(System.Windows.Forms.Panel))
                    {
                        Panel pnl = (Panel)c1;
                        ApplyStyleToPanel(pnl);
                    }
                }
                else if (c1.GetType() == typeof(System.Windows.Forms.TabControl))
                {
                    TabControl tc = (TabControl)c1;
                    tc.BackColor = FormColor;

                    for (int index = 0; index < tc.TabCount; index++)
                    {
                        tc.TabPages[index].BackColor = FormColor;
                        tc.TabPages[index].Font = new Font("Verdana", float.Parse("9"), FontStyle.Bold, GraphicsUnit.Point);
                        tc.TabPages[index].ForeColor = Color.Black;

                        foreach (Control c2 in tc.TabPages[index].Controls)
                        {
                            if (c2.GetType() == typeof(System.Windows.Forms.GroupBox) || c2.GetType() == typeof(System.Windows.Forms.Panel))
                            {
                                foreach (Control c3 in c2.Controls)
                                {
                                    c2.BackColor = FormColor;
                                    ApplyStyleToControl(c3, FormColor);
                                }

                                if (c2.GetType() == typeof(System.Windows.Forms.GroupBox))
                                {
                                    GroupBox gb = (GroupBox)c2;

                                    if (!gb.Tag.ToString().Equals("NotSet"))
                                    {
                                        gb.Font = new Font("Verdana", float.Parse("8"), FontStyle.Bold, GraphicsUnit.Point);
                                        gb.ForeColor = Color.FromArgb(25, 100, 155);
                                    }
                                }
                            }
                            else
                            {
                                c1.BackColor = FormColor;
                                ApplyStyleToControl(c2, FormColor);
                            }
                        }
                    }
                }
                else
                {
                    ApplyStyleToControl(c1, FormColor);
                }
            }
        }

        private void ApplyStyleToGroupBox(GroupBox gb, Color col)
        {
            if (gb.Tag != null && gb.Tag.ToString().Equals("NotSet"))
            {
                gb.Font = new Font("Verdana", float.Parse("8"), FontStyle.Bold, GraphicsUnit.Point);
                gb.ForeColor = Color.FromArgb(25, 100, 155);
                gb.ForeColor = Color.Black;

            }
            gb.Height = Convert.ToInt32(gb.Height * f_HeightRatio);
            gb.Width = Convert.ToInt32(gb.Width * f_WidthRatio);
            gb.Location = new Point(Convert.ToInt32(gb.Location.X * f_WidthRatio), Convert.ToInt32(gb.Location.Y * f_HeightRatio));
            gb.Font = new Font(gb.Font.FontFamily, gb.Font.Size * f_HeightRatio, gb.Font.Style, gb.Font.Unit, ((byte)(0)));

            foreach (Control c2 in gb.Controls)
            {
                ApplyStyleToControl(c2, FormColor);

                if (c2.GetType() == typeof(System.Windows.Forms.GroupBox))
                {
                    ApplyStyleToGroupBox((GroupBox)c2, col);
                }
            }
        }

        private void ApplyStyleToPanel(Panel pnl)
        {
            pnl.Height = Convert.ToInt32(pnl.Height * f_HeightRatio);
            pnl.Width = Convert.ToInt32(pnl.Width * f_WidthRatio);
            pnl.Location = new Point(Convert.ToInt32(pnl.Location.X * f_WidthRatio), Convert.ToInt32(pnl.Location.Y * f_HeightRatio));
            pnl.Font = new Font(pnl.Font.FontFamily, pnl.Font.Size * f_HeightRatio, pnl.Font.Style, pnl.Font.Unit, ((byte)(0)));

            if (pnl.Tag == null || pnl.Tag.Equals(""))
            {
                pnl.BackColor = PanelColorLight;
                foreach (Control c2 in pnl.Controls)
                {
                    ApplyStyleToControl(c2, PanelColorLight);

                    if (c2.GetType() == typeof(System.Windows.Forms.Panel))
                    {
                        ApplyStyleToPanel((Panel)c2);
                    }
                }
            }
            else if (pnl.Tag.ToString().Equals("top"))
            {
                pnl.BackColor = PanelColorDark;
                foreach (Control c2 in pnl.Controls)
                {
                    ApplyStyleToControl(c2, PanelColorDark);

                    if (c2.GetType() == typeof(System.Windows.Forms.Panel))
                    {
                        ApplyStyleToPanel((Panel)c2);
                    }
                }
            }
            else if (pnl.Tag.ToString().Equals("med"))
            {
                pnl.BackColor = PanelColorMedium;

                foreach (Control c2 in pnl.Controls)
                {
                    ApplyStyleToControl(c2, PanelColorMedium);

                    if (c2.GetType() == typeof(System.Windows.Forms.Panel))
                    {
                        ApplyStyleToPanel((Panel)c2);
                    }
                }
            }
            else if (pnl.Tag.ToString().Equals("self"))
            {
                pnl.BackColor = SelfColor;
                foreach (Control c2 in pnl.Controls)
                {
                    ApplyStyleToControl(c2, PanelColorMedium);

                    if (c2.GetType() == typeof(System.Windows.Forms.Panel))
                    {
                        ApplyStyleToPanel((Panel)c2);
                    }
                }
            }
        }

        private void ApplyStyleToControl(Control c, Color col)
        {
            if (c.GetType() == typeof(System.Windows.Forms.Button))
            {
                Button btn = (Button)c;

                if (btn.Tag == null)
                {
                    btn.Font = new Font("Verdana", float.Parse("9"), FontStyle.Bold, GraphicsUnit.Point);
                    btn.BackColor = BtnColor;
                    btn.ForeColor = Color.Black;
                }
                else if (btn.Tag.Equals("urdu"))
                {
                    btn.Font = new Font("AlKatib1", 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    btn.BackColor = Color.Transparent;
                    btn.ForeColor = Color.Black;
                }
                else if (btn.Tag.Equals("notset"))
                {
                    btn.BackColor = BtnColor;
                    btn.ForeColor = Color.Black;
                }
                btn.Height = Convert.ToInt32(btn.Height * f_HeightRatio);
                btn.Width = Convert.ToInt32(btn.Width * f_WidthRatio);
                btn.Location = new Point(Convert.ToInt32(btn.Location.X * f_WidthRatio), Convert.ToInt32(btn.Location.Y * f_HeightRatio));
                btn.Font = new Font(btn.Font.FontFamily, btn.Font.Size * f_HeightRatio, btn.Font.Style, btn.Font.Unit, ((byte)(0)));

            }
            else if (c.GetType() == typeof(System.Windows.Forms.DataGridView))
            {
                DataGridView dg = (DataGridView)c;


                dg.BackgroundColor = PanelColorLight;
                dg.ForeColor = Color.Black;
                dg.GridColor = Color.DimGray;
                
                //dg.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(43, 78, 15);
                //dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(235, 241, 230);
                dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
                dg.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSteelBlue;
                dg.RowsDefaultCellStyle.BackColor = Color.White;

                //if (dg.Tag == null)
                {
                    dg.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", float.Parse("9"), FontStyle.Bold, GraphicsUnit.Point);
                    dg.Font = new Font("Arial", float.Parse("9"), GraphicsUnit.Point);
                }
                //dg.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(235, 252, 222);
                dg.AlternatingRowsDefaultCellStyle.BackColor = Color.Lavender;

                dg.RowsDefaultCellStyle.SelectionBackColor = Color.Gainsboro;
                dg.RowsDefaultCellStyle.SelectionForeColor = Color.Black;

                //typeof(DataGridView).InvokeMember("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty, null, dg, new object[] { true }); 
                //dg.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                //dg.SuspendLayout();
                dg.Height = Convert.ToInt32(dg.Height * f_HeightRatio);
                dg.Width = Convert.ToInt32(dg.Width * f_WidthRatio);
                dg.Location = new Point(Convert.ToInt32(dg.Location.X * f_WidthRatio), Convert.ToInt32(dg.Location.Y * f_HeightRatio));
                dg.Font = new Font(dg.Font.FontFamily, dg.Font.Size * f_HeightRatio, dg.Font.Style, dg.Font.Unit);

            }
            else if (c.GetType() == typeof(System.Windows.Forms.TextBox))
            {
                TextBox tb = (TextBox)c;
                if (tb.Tag == null)
                {
                    tb.Font = new Font("Verdana", float.Parse("8.25"), GraphicsUnit.Point);
                    tb.ForeColor = Color.Black;
                }
                else if (tb.Tag.Equals("urdu"))
                {
                    tb.Font = new Font("AlKatib1", 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    tb.ForeColor = Color.Black;
                }
                tb.BackColor = Color.White;
                tb.BorderStyle = BorderStyle.FixedSingle;
                tb.Height = Convert.ToInt32(tb.Height * f_HeightRatio);

                tb.Width = Convert.ToInt32(tb.Width * f_WidthRatio);
                tb.Location = new Point(Convert.ToInt32(tb.Location.X * f_WidthRatio), Convert.ToInt32(tb.Location.Y * f_HeightRatio));
                tb.Font = new Font(tb.Font.FontFamily, tb.Font.Size * f_HeightRatio, tb.Font.Style, tb.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.CheckBox))
            {
                CheckBox cb = (CheckBox)c;
                //Font cbFont = new Font("Verdana", float.Parse("9"), GraphicsUnit.Point);

                if (cb.Tag == null)
                {
                    cb.Font = new Font("Arial", float.Parse("9"), GraphicsUnit.Point);
                    cb.TextAlign = ContentAlignment.MiddleLeft;
                }
                else if (cb.Tag.Equals("urdu"))
                {
                    cb.Font = new Font("AlKatib1", 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    cb.TextAlign = ContentAlignment.MiddleRight;
                }
                else if (cb.Tag.Equals("none"))
                {
                    cb.BackColor = col;
                    cb.TextAlign = ContentAlignment.MiddleLeft;
                }
                else if (cb.Tag.Equals("trans"))
                {
                    cb.BackColor = Color.Transparent;
                    cb.TextAlign = ContentAlignment.MiddleLeft;
                }

                cb.Height = Convert.ToInt32(cb.Height * f_HeightRatio);

                cb.Width = Convert.ToInt32(cb.Width * f_WidthRatio);
                cb.Location = new Point(Convert.ToInt32(cb.Location.X * f_WidthRatio), Convert.ToInt32(cb.Location.Y * f_HeightRatio));
                cb.Font = new Font(cb.Font.FontFamily, cb.Font.Size * f_HeightRatio, cb.Font.Style, cb.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.Label))
            {
                Label lbl = (Label)c;

                if (lbl.Tag == null)
                {
                    lbl.ForeColor = Color.Black;
                    lbl.BackColor = col;
                }
                else if (lbl.Tag.ToString().Equals("display"))
                {
                    lbl.Font = new Font("Arial", float.Parse("9"), FontStyle.Bold, GraphicsUnit.Point);
                    lbl.ForeColor = Color.Black;
                    lbl.BackColor = col;
                }
                else if (lbl.Tag.ToString().Equals("heading"))
                {
                    lbl.ForeColor = Color.FromArgb(25, 100, 155);
                    lbl.Font = new Font("Arial", float.Parse("12"), FontStyle.Bold, GraphicsUnit.Point);
                    lbl.BackColor = col;
                }
                else if (lbl.Tag.ToString().Equals("trans"))
                {
                    lbl.ForeColor = Color.Black;
                }
                else if (lbl.Tag.ToString().Equals("transdisplay"))
                {
                    lbl.Font = new Font("Arial", float.Parse("9"), FontStyle.Bold, GraphicsUnit.Point);
                    lbl.ForeColor = Color.Black;
                }
                else if (lbl.Tag.ToString().Equals("transheading"))
                {
                    lbl.ForeColor = Color.FromArgb(25, 100, 155);
                    lbl.Font = new Font("Arial", float.Parse("12"), FontStyle.Bold, GraphicsUnit.Point);
                }
                else if (lbl.Tag.ToString().Equals("urdu"))
                {
                    lbl.ForeColor = Color.FromArgb(25, 100, 155);
                    lbl.Font = new Font("AlKatib1", 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
                lbl.Font = new Font(lbl.Font.FontFamily, lbl.Font.Size * f_HeightRatio, lbl.Font.Style, lbl.Font.Unit, ((byte)(0)));
                lbl.Height = Convert.ToInt32(lbl.Height * f_HeightRatio);
                lbl.Width = Convert.ToInt32(lbl.Width * f_WidthRatio);
                lbl.Location = new Point(Convert.ToInt32(lbl.Location.X * f_WidthRatio), Convert.ToInt32(lbl.Location.Y * f_HeightRatio));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.RadioButton))
            {
                RadioButton rbtn = (RadioButton)c;

                if (rbtn.Tag == null)
                {
                    rbtn.BackColor = col;
                }
                else if (rbtn.Tag.Equals("self"))
                {
                    rbtn.BackColor = Color.Transparent;
                }
                rbtn.Font = new Font("Arial", float.Parse("8"), FontStyle.Regular, GraphicsUnit.Point);
                rbtn.ForeColor = Color.Black;
                rbtn.TextAlign = ContentAlignment.TopLeft;
                if (rbtn.Name.Equals("rbPatientMale"))
                {
                    rbtn.ForeColor = Color.Black;
                }
                rbtn.Height = Convert.ToInt32(rbtn.Height * f_HeightRatio);
                rbtn.Width = Convert.ToInt32(rbtn.Width * f_WidthRatio);
                rbtn.Location = new Point(Convert.ToInt32(rbtn.Location.X * f_WidthRatio), Convert.ToInt32(rbtn.Location.Y * f_HeightRatio));
                rbtn.Font = new Font(rbtn.Font.FontFamily, rbtn.Font.Size * f_HeightRatio, rbtn.Font.Style, rbtn.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.ComboBox))
            {
                ComboBox cmb = (ComboBox)c;

                if (cmb.Tag == null)
                {
                    cmb.Font = new Font("Verdana", float.Parse("9"), FontStyle.Regular, GraphicsUnit.Point);
                }
                else if (cmb.Tag.ToString().Equals("urdu"))
                {
                    cmb.Font = new Font("AlKatib1", 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }

                cmb.ForeColor = Color.Black;
                cmb.Height = Convert.ToInt32(cmb.Height * f_HeightRatio);
                cmb.Width = Convert.ToInt32(cmb.Width * f_WidthRatio);
                cmb.Location = new Point(Convert.ToInt32(cmb.Location.X * f_WidthRatio), Convert.ToInt32(cmb.Location.Y * f_HeightRatio));
                cmb.Font = new Font(cmb.Font.FontFamily, cmb.Font.Size * f_HeightRatio, cmb.Font.Style, cmb.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.LinkLabel))
            {
                LinkLabel llbl = (LinkLabel)c;
                llbl.Height = Convert.ToInt32(llbl.Height * f_HeightRatio);
                llbl.Width = Convert.ToInt32(llbl.Width * f_WidthRatio);
                llbl.Location = new Point(Convert.ToInt32(llbl.Location.X * f_WidthRatio), Convert.ToInt32(llbl.Location.Y * f_HeightRatio));
                llbl.Font = new Font(llbl.Font.FontFamily, llbl.Font.Size * f_HeightRatio, llbl.Font.Style, llbl.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.ToolStrip))
            {
                ToolStrip ts = (ToolStrip)c;
                ts.Height = Convert.ToInt32(ts.Height * f_HeightRatio);
                ts.Width = Convert.ToInt32(ts.Width * f_WidthRatio);
                ts.Location = new Point(Convert.ToInt32(ts.Location.X * f_WidthRatio), Convert.ToInt32(ts.Location.Y * f_HeightRatio));
                ts.Font = new Font(ts.Font.FontFamily, ts.Font.Size * f_HeightRatio, ts.Font.Style, ts.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.PictureBox))
            {
                PictureBox picb = (PictureBox)c;
                picb.Height = Convert.ToInt32(picb.Height * f_HeightRatio);
                picb.Width = Convert.ToInt32(picb.Width * f_WidthRatio);
                picb.Location = new Point(Convert.ToInt32(picb.Location.X * f_WidthRatio), Convert.ToInt32(picb.Location.Y * f_HeightRatio));
                picb.Font = new Font(picb.Font.FontFamily, picb.Font.Size * f_HeightRatio, picb.Font.Style, picb.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.DateTimePicker))
            {
                DateTimePicker dtp = (DateTimePicker)c;
                dtp.Height = Convert.ToInt32(dtp.Height * f_HeightRatio);
                dtp.Width = Convert.ToInt32(dtp.Width * f_WidthRatio);
                dtp.Location = new Point(Convert.ToInt32(dtp.Location.X * f_WidthRatio), Convert.ToInt32(dtp.Location.Y * f_HeightRatio));
                dtp.Font = new Font(dtp.Font.FontFamily, dtp.Font.Size * f_HeightRatio, dtp.Font.Style, dtp.Font.Unit, ((byte)(0)));
            }
            else if (c.GetType() == typeof(System.Windows.Forms.MaskedTextBox))
            {
                MaskedTextBox mtxt = (MaskedTextBox)c;
                mtxt.Height = Convert.ToInt32(mtxt.Height * f_HeightRatio);
                mtxt.Width = Convert.ToInt32(mtxt.Width * f_WidthRatio);
                mtxt.Location = new Point(Convert.ToInt32(mtxt.Location.X * f_WidthRatio), Convert.ToInt32(mtxt.Location.Y * f_HeightRatio));
                mtxt.Font = new Font(mtxt.Font.FontFamily, mtxt.Font.Size * f_HeightRatio, mtxt.Font.Style, mtxt.Font.Unit, ((byte)(0)));
            }
        }

        public void ApplyStyleToDGTableStyle(DataGridTableStyle dgts)
        {
            //Font headerFont = new Font("Arial", float.Parse("10"), FontStyle.Bold, GraphicsUnit.Point);

            //dgts.AlternatingBackColor = Color.FromArgb(104, 186, 23);
            //dgts.ForeColor = Color.Black;
            //dgts.BackColor = Color.White;
            //dgts.GridLineColor = Color.DimGray;
            //dgts.GridLineStyle = DataGridLineStyle.Solid;
            //dgts.HeaderBackColor = Color.FromArgb(152, 172, 197);
            //dgts.HeaderFont = headerFont;
            //dgts.HeaderForeColor = Color.Black;
            //dgts.SelectionBackColor = Color.Silver;
            //dgts.SelectionForeColor = Color.Black;
        }

        # endregion

        #region "Fill ComboBox Methods"
        /// <summary>
        /// Bind DataView with Windows.Forms.ComboBox
        /// </summary>
        /// <param name="cmb">Reference of ComboBox</param>
        /// <param name="dv">DataView to bind with the ComboBox</param>
        /// <param name="strTextField">Column name to Display (Note: it is case sensitive, so if you have not alias the column header in the query then write in Upper case)</param>
        /// <param name="strValueField">Column name to assign value (Note: it is case sensitive, so if you have not alias the column header in the query then write in Upper case)</param>
        /// <param name="sort">true for Sorting</param>
        /// <param name="isSelect">true, if you want to add Select row</param>
        public void FillComboBox(ComboBox cmb, DataView dv, string strTextField, string strValueField, bool isSelect)
        {
            if (isSelect)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(strValueField);
                dt.Columns.Add(strTextField);

                DataRow dr = dt.NewRow();
                dr[strValueField] = "-1";
                dr[strTextField] = "Select";
                dt.Rows.Add(dr);

                foreach (DataRow drMain in dv.Table.Rows)
                {
                    dr = dt.NewRow();
                    dr[strValueField] = drMain[strValueField];
                    dr[strTextField] = drMain[strTextField];
                    dt.Rows.Add(dr);
                }

                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dt;
            }
            else
            {
                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dv;
            }
        }

        /// <summary>
        /// Fill ComboBox, when there is no need of Value field in ComboBox
        /// </summary>
        /// <param name="cmb">ComboBox reference</param>
        /// <param name="dv">DataView to load in Combo Box</param>
        /// <param name="strTextField">Display Member field</param>
        /// <param name="isSelect">true, if you want to add Select row</param>
        public void FillComboBox(ComboBox cmb, DataView dv, string strTextField, bool isSelect)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(strTextField);
            dt.Columns.Add("ValueField");

            if (isSelect)
            {
                DataRow dr = dt.NewRow();
                dr[strTextField] = "Select";
                dr["ValueField"] = "-1";
                dt.Rows.Add(dr);

                foreach (DataRow drMain in dv.Table.Rows)
                {
                    dr = dt.NewRow();
                    dr[strTextField] = drMain[strTextField];
                    dr["ValueField"] = drMain[strTextField];
                    dt.Rows.Add(dr);
                }

                cmb.DisplayMember = strTextField;
                cmb.ValueMember = "ValueField";
                cmb.DataSource = dt.DefaultView;
            }
            else
            {
                cmb.ValueMember = strTextField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dv;
            }
        }

        public void FillComboBox(ComboBox cmb, DataView dv, string strTextField, string strValueField, bool isSelect, bool isAll)
        {
            if (isSelect || isAll)
            {
                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add(strValueField);
                dt.Columns.Add(strTextField);

                if (isSelect)
                {
                    dr = dt.NewRow();
                    dr[strValueField] = "-1";
                    dr[strTextField] = "Select";
                    dt.Rows.Add(dr);
                }

                if (isAll)
                {
                    dr = dt.NewRow();
                    dr[strValueField] = "-2";
                    dr[strTextField] = "All";
                    dt.Rows.Add(dr);
                }

                foreach (DataRow drMain in dv.Table.Rows)
                {
                    dr = dt.NewRow();
                    dr[strValueField] = drMain[strValueField];
                    dr[strTextField] = drMain[strTextField];
                    dt.Rows.Add(dr);
                }

                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dt;
            }
            else
            {
                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dv;
            }
        }

        public void FillComboBoxStartWithEmpty(ComboBox cmb, DataView dv, string strTextField, string strValueField, bool isSelect)
        {
            if (isSelect)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(strValueField);
                dt.Columns.Add(strTextField);

                DataRow dr = dt.NewRow();
                dr[strValueField] = "-1";
                dr[strTextField] = "";
                dt.Rows.Add(dr);

                foreach (DataRow drMain in dv.Table.Rows)
                {
                    dr = dt.NewRow();
                    dr[strValueField] = drMain[strValueField];
                    dr[strTextField] = drMain[strTextField];
                    dt.Rows.Add(dr);
                }

                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dt;
            }
            else
            {
                cmb.ValueMember = strValueField;
                cmb.DisplayMember = strTextField;
                cmb.DataSource = dv;
            }
        }

        # endregion
    }
}