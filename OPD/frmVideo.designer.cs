namespace OPD
{
    partial class frmVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVideo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblPictureID = new System.Windows.Forms.Label();
            this.lblComment = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtComment = new System.Windows.Forms.TextBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCopy = new System.Windows.Forms.Button();
            this.lblMode = new System.Windows.Forms.Label();
            this.dgVdo = new System.Windows.Forms.DataGridView();
            this.EnteredDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PictureID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VdoPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SavedMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoctypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OFDVdo = new System.Windows.Forms.OpenFileDialog();
            this.lblPatientID = new System.Windows.Forms.Label();
            this.btnAddNewType = new System.Windows.Forms.Button();
            this.cboDocType = new System.Windows.Forms.ComboBox();
            this.lblDocType = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Player = new AxMediaPlayer.AxMediaPlayer();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVdo)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClear.Location = new System.Drawing.Point(18, 311);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(63, 60);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "&Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnClear, "Clear Selection");
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblPictureID
            // 
            this.lblPictureID.AutoSize = true;
            this.lblPictureID.Location = new System.Drawing.Point(396, 200);
            this.lblPictureID.Name = "lblPictureID";
            this.lblPictureID.Size = new System.Drawing.Size(61, 13);
            this.lblPictureID.TabIndex = 11;
            this.lblPictureID.Text = "lblPictureID";
            this.lblPictureID.Visible = false;
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Location = new System.Drawing.Point(3, 530);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(60, 13);
            this.lblComment.TabIndex = 19;
            this.lblComment.Text = "Comment : ";
            // 
            // txtComment
            // 
            this.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComment.Location = new System.Drawing.Point(6, 546);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComment.Size = new System.Drawing.Size(574, 44);
            this.txtComment.TabIndex = 18;
            this.toolTip1.SetToolTip(this.txtComment, "Comment Related to Image");
            this.txtComment.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNew.Location = new System.Drawing.Point(18, 71);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(63, 60);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "&New";
            this.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnNew, "Select New Image for Save");
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSave.Location = new System.Drawing.Point(18, 151);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(63, 60);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnSave, "Save New Image");
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.Enabled = false;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.Location = new System.Drawing.Point(18, 391);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(63, 60);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnDelete, "Delete Selected Image");
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnCopy);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.lblMode);
            this.panel1.Location = new System.Drawing.Point(712, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 524);
            this.panel1.TabIndex = 16;
            this.panel1.Tag = "top";
            // 
            // btnCopy
            // 
            this.btnCopy.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCopy.Enabled = false;
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.Location = new System.Drawing.Point(18, 231);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(63, 60);
            this.btnCopy.TabIndex = 4;
            this.btnCopy.Text = "Co&py";
            this.btnCopy.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(16, 485);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(44, 13);
            this.lblMode.TabIndex = 23;
            this.lblMode.Text = "lblMode";
            this.lblMode.Visible = false;
            // 
            // dgVdo
            // 
            this.dgVdo.AllowUserToAddRows = false;
            this.dgVdo.AllowUserToDeleteRows = false;
            this.dgVdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVdo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgVdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVdo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EnteredDate,
            this.PictureID,
            this.PatientID1,
            this.Mode,
            this.VdoPath,
            this.Comments,
            this.DocTypeID,
            this.SavedMethod,
            this.DocType,
            this.DoctypeName});
            this.dgVdo.EnableHeadersVisualStyles = false;
            this.dgVdo.Location = new System.Drawing.Point(6, 591);
            this.dgVdo.MultiSelect = false;
            this.dgVdo.Name = "dgVdo";
            this.dgVdo.ReadOnly = true;
            this.dgVdo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVdo.Size = new System.Drawing.Size(806, 116);
            this.dgVdo.TabIndex = 21;
            this.dgVdo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVdo_CellDoubleClick);
            // 
            // EnteredDate
            // 
            this.EnteredDate.DataPropertyName = "EnteredoN";
            this.EnteredDate.FillWeight = 15F;
            this.EnteredDate.HeaderText = "Date";
            this.EnteredDate.Name = "EnteredDate";
            this.EnteredDate.ReadOnly = true;
            // 
            // PictureID
            // 
            this.PictureID.DataPropertyName = "pictureiD";
            this.PictureID.HeaderText = "PictureID";
            this.PictureID.Name = "PictureID";
            this.PictureID.ReadOnly = true;
            this.PictureID.Visible = false;
            // 
            // PatientID1
            // 
            this.PatientID1.DataPropertyName = "PatientID";
            this.PatientID1.HeaderText = "PatientID";
            this.PatientID1.Name = "PatientID1";
            this.PatientID1.ReadOnly = true;
            this.PatientID1.Visible = false;
            // 
            // Mode
            // 
            this.Mode.HeaderText = "Mode";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Visible = false;
            // 
            // VdoPath
            // 
            this.VdoPath.DataPropertyName = "SmallPicture";
            this.VdoPath.FillWeight = 40F;
            this.VdoPath.HeaderText = "Location";
            this.VdoPath.Name = "VdoPath";
            this.VdoPath.ReadOnly = true;
            this.VdoPath.Visible = false;
            // 
            // Comments
            // 
            this.Comments.DataPropertyName = "comment";
            this.Comments.FillWeight = 70F;
            this.Comments.HeaderText = "Comments";
            this.Comments.Name = "Comments";
            this.Comments.ReadOnly = true;
            // 
            // DocTypeID
            // 
            this.DocTypeID.DataPropertyName = "DocTypeID";
            this.DocTypeID.HeaderText = "DocTypeID";
            this.DocTypeID.Name = "DocTypeID";
            this.DocTypeID.ReadOnly = true;
            this.DocTypeID.Visible = false;
            // 
            // SavedMethod
            // 
            this.SavedMethod.DataPropertyName = "SavedMethod";
            this.SavedMethod.HeaderText = "SavedMethod";
            this.SavedMethod.Name = "SavedMethod";
            this.SavedMethod.ReadOnly = true;
            this.SavedMethod.Visible = false;
            // 
            // DocType
            // 
            this.DocType.DataPropertyName = "Type";
            this.DocType.FillWeight = 15F;
            this.DocType.HeaderText = "DocType";
            this.DocType.Name = "DocType";
            this.DocType.ReadOnly = true;
            this.DocType.Visible = false;
            // 
            // DoctypeName
            // 
            this.DoctypeName.DataPropertyName = "DocTypeName";
            this.DoctypeName.FillWeight = 20F;
            this.DoctypeName.HeaderText = "Type";
            this.DoctypeName.Name = "DoctypeName";
            this.DoctypeName.ReadOnly = true;
            // 
            // OFDVdo
            // 
            this.OFDVdo.FileName = "openFileDialog1";
            // 
            // lblPatientID
            // 
            this.lblPatientID.AutoSize = true;
            this.lblPatientID.Location = new System.Drawing.Point(460, 271);
            this.lblPatientID.Name = "lblPatientID";
            this.lblPatientID.Size = new System.Drawing.Size(61, 13);
            this.lblPatientID.TabIndex = 22;
            this.lblPatientID.Text = "lblPatientID";
            this.lblPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPatientID.Visible = false;
            // 
            // btnAddNewType
            // 
            this.btnAddNewType.Location = new System.Drawing.Point(766, 567);
            this.btnAddNewType.Name = "btnAddNewType";
            this.btnAddNewType.Size = new System.Drawing.Size(46, 23);
            this.btnAddNewType.TabIndex = 37;
            this.btnAddNewType.Text = "Add";
            this.btnAddNewType.UseVisualStyleBackColor = true;
            this.btnAddNewType.Click += new System.EventHandler(this.btnAddNewType_Click);
            // 
            // cboDocType
            // 
            this.cboDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDocType.FormattingEnabled = true;
            this.cboDocType.Location = new System.Drawing.Point(586, 546);
            this.cboDocType.Name = "cboDocType";
            this.cboDocType.Size = new System.Drawing.Size(226, 21);
            this.cboDocType.TabIndex = 36;
            this.cboDocType.SelectedIndexChanged += new System.EventHandler(this.cboDocType_SelectedIndexChanged);
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.Location = new System.Drawing.Point(583, 530);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(92, 13);
            this.lblDocType.TabIndex = 35;
            this.lblDocType.Text = "Document Type : ";
            // 
            // panel2
            // 
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.Player);
            this.panel2.Location = new System.Drawing.Point(6, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(705, 524);
            this.panel2.TabIndex = 38;
            // 
            // Player
            // 
            this.Player.Location = new System.Drawing.Point(1, -1);
            this.Player.Name = "Player";
            this.Player.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Player.OcxState")));
            this.Player.Size = new System.Drawing.Size(703, 521);
            this.Player.TabIndex = 0;
            // 
            // frmVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 713);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnAddNewType);
            this.Controls.Add(this.cboDocType);
            this.Controls.Add(this.lblDocType);
            this.Controls.Add(this.dgVdo);
            this.Controls.Add(this.lblPictureID);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblPatientID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmVideo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patient Video";
            this.Load += new System.EventHandler(this.frmVideo_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVideo_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVdo)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Player)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblPictureID;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgVdo;
        private System.Windows.Forms.OpenFileDialog OFDVdo;
        private System.Windows.Forms.Label lblPatientID;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Button btnAddNewType;
        private System.Windows.Forms.ComboBox cboDocType;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PictureID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn VdoPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SavedMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DoctypeName;
        private AxMediaPlayer.AxMediaPlayer Player;
        
        
    }
}