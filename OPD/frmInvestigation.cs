using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmInvestigation : Form
    {
        public frmInvestigation()
        {
            InitializeComponent();
        }
        private void frmInvestigation_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();

            objComp.ApplyStyleToControls(this);
            FillListGridView("6");//Investigation
            //dgList.Columns["Pname"].HeaderText = "Investigation"; 

            objComp = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FillListGridView(string TypeID)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPreference objPreference = new clsBLPreference(objConnection);

            objPreference.TypeID = TypeID;
            objPreference.PersonID = clsSharedVariables.UserID;
            objConnection.Connection_Open();
            dgLeftInvestigation.DataSource = objPreference.GetAll(2);
            objConnection.Connection_Close();

            objPreference = null;
            objConnection = null;

        }

    }
}
