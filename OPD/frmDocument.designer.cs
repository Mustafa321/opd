namespace OPD
{
  partial class frmDocument
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocument));
        this.dgDoc = new System.Windows.Forms.DataGridView();
        this.PictureID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.EnteredDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.GridPatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.VdoPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.Comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SavedMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DoctypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.DocTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.btnCopy = new System.Windows.Forms.Button();
        this.OFDDoc = new System.Windows.Forms.OpenFileDialog();
        this.lblPatientID = new System.Windows.Forms.Label();
        this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
        this.txtComment = new System.Windows.Forms.TextBox();
        this.btnClear = new System.Windows.Forms.Button();
        this.btnNew = new System.Windows.Forms.Button();
        this.btnDelete = new System.Windows.Forms.Button();
        this.btnSave = new System.Windows.Forms.Button();
        this.btnOpen = new System.Windows.Forms.Button();
        this.lblComment = new System.Windows.Forms.Label();
        this.lblPictureID = new System.Windows.Forms.Label();
        this.panel1 = new System.Windows.Forms.Panel();
        this.lblMode = new System.Windows.Forms.Label();
        this.panel2 = new System.Windows.Forms.Panel();
        this.btnAddNewType = new System.Windows.Forms.Button();
        this.cboDocType = new System.Windows.Forms.ComboBox();
        this.lblDocType = new System.Windows.Forms.Label();
        this.lblPath = new System.Windows.Forms.Label();
        this.txtPath = new System.Windows.Forms.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.dgDoc)).BeginInit();
        this.panel1.SuspendLayout();
        this.panel2.SuspendLayout();
        this.SuspendLayout();
        // 
        // dgDoc
        // 
        this.dgDoc.AllowUserToAddRows = false;
        this.dgDoc.AllowUserToDeleteRows = false;
        this.dgDoc.AllowUserToResizeColumns = false;
        this.dgDoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dgDoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        this.dgDoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dgDoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PictureID,
            this.EnteredDate,
            this.Mode,
            this.GridPatientID,
            this.VdoPath,
            this.Comments,
            this.SavedMethod,
            this.TYPE,
            this.DoctypeID,
            this.DocTypeName});
        this.dgDoc.EnableHeadersVisualStyles = false;
        this.dgDoc.Location = new System.Drawing.Point(1, 72);
        this.dgDoc.MultiSelect = false;
        this.dgDoc.Name = "dgDoc";
        this.dgDoc.ReadOnly = true;
        this.dgDoc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        this.dgDoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.dgDoc.Size = new System.Drawing.Size(884, 381);
        this.dgDoc.TabIndex = 28;
        this.dgDoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDoc_CellDoubleClick);
        // 
        // PictureID
        // 
        this.PictureID.DataPropertyName = "pictureiD";
        this.PictureID.HeaderText = "PictureID";
        this.PictureID.Name = "PictureID";
        this.PictureID.ReadOnly = true;
        this.PictureID.Visible = false;
        // 
        // EnteredDate
        // 
        this.EnteredDate.DataPropertyName = "EnteredON";
        this.EnteredDate.FillWeight = 15F;
        this.EnteredDate.HeaderText = "Date";
        this.EnteredDate.Name = "EnteredDate";
        this.EnteredDate.ReadOnly = true;
        // 
        // Mode
        // 
        this.Mode.HeaderText = "Mode";
        this.Mode.Name = "Mode";
        this.Mode.ReadOnly = true;
        this.Mode.Visible = false;
        // 
        // GridPatientID
        // 
        this.GridPatientID.DataPropertyName = "patientID";
        this.GridPatientID.HeaderText = "PatientId";
        this.GridPatientID.Name = "GridPatientID";
        this.GridPatientID.ReadOnly = true;
        this.GridPatientID.Visible = false;
        // 
        // VdoPath
        // 
        this.VdoPath.DataPropertyName = "SmallPicture";
        this.VdoPath.FillWeight = 40F;
        this.VdoPath.HeaderText = "Location";
        this.VdoPath.Name = "VdoPath";
        this.VdoPath.ReadOnly = true;
        this.VdoPath.Visible = false;
        // 
        // Comments
        // 
        this.Comments.DataPropertyName = "comment";
        this.Comments.FillWeight = 70F;
        this.Comments.HeaderText = "Comments";
        this.Comments.Name = "Comments";
        this.Comments.ReadOnly = true;
        // 
        // SavedMethod
        // 
        this.SavedMethod.DataPropertyName = "SavedMethod";
        this.SavedMethod.HeaderText = "SavedMethod";
        this.SavedMethod.Name = "SavedMethod";
        this.SavedMethod.ReadOnly = true;
        this.SavedMethod.Visible = false;
        // 
        // TYPE
        // 
        this.TYPE.DataPropertyName = "Type";
        this.TYPE.HeaderText = "DocType";
        this.TYPE.Name = "TYPE";
        this.TYPE.ReadOnly = true;
        this.TYPE.Visible = false;
        // 
        // DoctypeID
        // 
        this.DoctypeID.DataPropertyName = "DocTypeID";
        this.DoctypeID.HeaderText = "DocTypeID";
        this.DoctypeID.Name = "DoctypeID";
        this.DoctypeID.ReadOnly = true;
        this.DoctypeID.Visible = false;
        // 
        // DocTypeName
        // 
        this.DocTypeName.DataPropertyName = "DocTypeName";
        this.DocTypeName.FillWeight = 15F;
        this.DocTypeName.HeaderText = "Document Type";
        this.DocTypeName.Name = "DocTypeName";
        this.DocTypeName.ReadOnly = true;
        // 
        // btnCopy
        // 
        this.btnCopy.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnCopy.Enabled = false;
        this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnCopy.Location = new System.Drawing.Point(17, 196);
        this.btnCopy.Name = "btnCopy";
        this.btnCopy.Size = new System.Drawing.Size(65, 65);
        this.btnCopy.TabIndex = 4;
        this.btnCopy.Text = "Co&py";
        this.btnCopy.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.btnCopy.UseVisualStyleBackColor = false;
        this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
        // 
        // OFDDoc
        // 
        this.OFDDoc.FileName = "openFileDialog1";
        // 
        // lblPatientID
        // 
        this.lblPatientID.AutoSize = true;
        this.lblPatientID.Location = new System.Drawing.Point(535, 280);
        this.lblPatientID.Name = "lblPatientID";
        this.lblPatientID.Size = new System.Drawing.Size(61, 13);
        this.lblPatientID.TabIndex = 29;
        this.lblPatientID.Text = "lblPatientID";
        this.lblPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        this.lblPatientID.Visible = false;
        // 
        // txtComment
        // 
        this.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
        this.txtComment.Location = new System.Drawing.Point(69, 23);
        this.txtComment.Multiline = true;
        this.txtComment.Name = "txtComment";
        this.txtComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtComment.Size = new System.Drawing.Size(815, 48);
        this.txtComment.TabIndex = 25;
        this.toolTip1.SetToolTip(this.txtComment, "Comment Related to Image");
        this.txtComment.Validating += new System.ComponentModel.CancelEventHandler(this.txtInItCap_Validating);
        // 
        // btnClear
        // 
        this.btnClear.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
        this.btnClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.btnClear.Location = new System.Drawing.Point(17, 267);
        this.btnClear.Name = "btnClear";
        this.btnClear.Size = new System.Drawing.Size(65, 65);
        this.btnClear.TabIndex = 3;
        this.btnClear.Text = "&Clear";
        this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.toolTip1.SetToolTip(this.btnClear, "Clear Selection");
        this.btnClear.UseVisualStyleBackColor = false;
        this.btnClear.Click += new System.EventHandler(this.btnClear_click);
        // 
        // btnNew
        // 
        this.btnNew.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
        this.btnNew.Location = new System.Drawing.Point(17, 51);
        this.btnNew.Name = "btnNew";
        this.btnNew.Size = new System.Drawing.Size(65, 65);
        this.btnNew.TabIndex = 2;
        this.btnNew.Text = "&New";
        this.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.toolTip1.SetToolTip(this.btnNew, "Select New Image for Save");
        this.btnNew.UseVisualStyleBackColor = false;
        this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
        // 
        // btnDelete
        // 
        this.btnDelete.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnDelete.Enabled = false;
        this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
        this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.btnDelete.Location = new System.Drawing.Point(17, 338);
        this.btnDelete.Name = "btnDelete";
        this.btnDelete.Size = new System.Drawing.Size(65, 65);
        this.btnDelete.TabIndex = 1;
        this.btnDelete.Text = "&Delete";
        this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.toolTip1.SetToolTip(this.btnDelete, "Delete Selected Image");
        this.btnDelete.UseVisualStyleBackColor = false;
        this.btnDelete.Click += new System.EventHandler(this.btnCancel_Click);
        // 
        // btnSave
        // 
        this.btnSave.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnSave.Enabled = false;
        this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
        this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.btnSave.Location = new System.Drawing.Point(17, 122);
        this.btnSave.Name = "btnSave";
        this.btnSave.Size = new System.Drawing.Size(65, 65);
        this.btnSave.TabIndex = 0;
        this.btnSave.Text = "&Save";
        this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.toolTip1.SetToolTip(this.btnSave, "Save New Image");
        this.btnSave.UseVisualStyleBackColor = false;
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        // 
        // btnOpen
        // 
        this.btnOpen.BackColor = System.Drawing.Color.LightSlateGray;
        this.btnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        this.btnOpen.Enabled = false;
        this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.Image")));
        this.btnOpen.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
        this.btnOpen.Location = new System.Drawing.Point(17, 372);
        this.btnOpen.Name = "btnOpen";
        this.btnOpen.Size = new System.Drawing.Size(65, 65);
        this.btnOpen.TabIndex = 24;
        this.btnOpen.Text = "&Open";
        this.btnOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        this.toolTip1.SetToolTip(this.btnOpen, "Delete Selected Image");
        this.btnOpen.UseVisualStyleBackColor = false;
        this.btnOpen.Visible = false;
        this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
        // 
        // lblComment
        // 
        this.lblComment.AutoSize = true;
        this.lblComment.Location = new System.Drawing.Point(10, 25);
        this.lblComment.Name = "lblComment";
        this.lblComment.Size = new System.Drawing.Size(57, 13);
        this.lblComment.TabIndex = 26;
        this.lblComment.Text = "Comment :";
        // 
        // lblPictureID
        // 
        this.lblPictureID.AutoSize = true;
        this.lblPictureID.Location = new System.Drawing.Point(471, 209);
        this.lblPictureID.Name = "lblPictureID";
        this.lblPictureID.Size = new System.Drawing.Size(61, 13);
        this.lblPictureID.TabIndex = 23;
        this.lblPictureID.Text = "lblPictureID";
        this.lblPictureID.Visible = false;
        // 
        // panel1
        // 
        this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel1.Controls.Add(this.btnOpen);
        this.panel1.Controls.Add(this.btnCopy);
        this.panel1.Controls.Add(this.btnClear);
        this.panel1.Controls.Add(this.btnNew);
        this.panel1.Controls.Add(this.btnDelete);
        this.panel1.Controls.Add(this.btnSave);
        this.panel1.Controls.Add(this.lblMode);
        this.panel1.Location = new System.Drawing.Point(891, 1);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(100, 456);
        this.panel1.TabIndex = 24;
        this.panel1.Tag = "top";
        // 
        // lblMode
        // 
        this.lblMode.AutoSize = true;
        this.lblMode.Location = new System.Drawing.Point(16, 525);
        this.lblMode.Name = "lblMode";
        this.lblMode.Size = new System.Drawing.Size(44, 13);
        this.lblMode.TabIndex = 23;
        this.lblMode.Text = "lblMode";
        this.lblMode.Visible = false;
        // 
        // panel2
        // 
        this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.panel2.Controls.Add(this.btnAddNewType);
        this.panel2.Controls.Add(this.cboDocType);
        this.panel2.Controls.Add(this.lblDocType);
        this.panel2.Controls.Add(this.lblPath);
        this.panel2.Controls.Add(this.txtPath);
        this.panel2.Controls.Add(this.lblComment);
        this.panel2.Controls.Add(this.dgDoc);
        this.panel2.Controls.Add(this.txtComment);
        this.panel2.Location = new System.Drawing.Point(2, 1);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(888, 456);
        this.panel2.TabIndex = 30;
        // 
        // btnAddNewType
        // 
        this.btnAddNewType.Location = new System.Drawing.Point(836, 0);
        this.btnAddNewType.Name = "btnAddNewType";
        this.btnAddNewType.Size = new System.Drawing.Size(46, 23);
        this.btnAddNewType.TabIndex = 34;
        this.btnAddNewType.Text = "Add";
        this.btnAddNewType.UseVisualStyleBackColor = true;
        this.btnAddNewType.Click += new System.EventHandler(this.btnAddNewType_Click);
        // 
        // cboDocType
        // 
        this.cboDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.cboDocType.FormattingEnabled = true;
        this.cboDocType.Location = new System.Drawing.Point(661, 1);
        this.cboDocType.Name = "cboDocType";
        this.cboDocType.Size = new System.Drawing.Size(176, 21);
        this.cboDocType.TabIndex = 33;
        this.cboDocType.SelectedIndexChanged += new System.EventHandler(this.cboDocType_SelectedIndexChanged);
        // 
        // lblDocType
        // 
        this.lblDocType.AutoSize = true;
        this.lblDocType.Location = new System.Drawing.Point(571, 5);
        this.lblDocType.Name = "lblDocType";
        this.lblDocType.Size = new System.Drawing.Size(92, 13);
        this.lblDocType.TabIndex = 32;
        this.lblDocType.Text = "Document Type : ";
        // 
        // lblPath
        // 
        this.lblPath.AutoSize = true;
        this.lblPath.Location = new System.Drawing.Point(32, 5);
        this.lblPath.Name = "lblPath";
        this.lblPath.Size = new System.Drawing.Size(35, 13);
        this.lblPath.TabIndex = 30;
        this.lblPath.Text = "Path :";
        // 
        // txtPath
        // 
        this.txtPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.txtPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
        this.txtPath.Location = new System.Drawing.Point(69, 1);
        this.txtPath.Name = "txtPath";
        this.txtPath.ReadOnly = true;
        this.txtPath.Size = new System.Drawing.Size(496, 21);
        this.txtPath.TabIndex = 29;
        // 
        // frmDocument
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.ClientSize = new System.Drawing.Size(992, 458);
        this.Controls.Add(this.panel2);
        this.Controls.Add(this.lblPatientID);
        this.Controls.Add(this.lblPictureID);
        this.Controls.Add(this.panel1);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        this.MaximizeBox = false;
        this.Name = "frmDocument";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Document";
        this.Load += new System.EventHandler(this.frmDocument_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDocument_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dgDoc)).EndInit();
        this.panel1.ResumeLayout(false);
        this.panel1.PerformLayout();
        this.panel2.ResumeLayout(false);
        this.panel2.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgDoc;
    private System.Windows.Forms.Button btnCopy;
    private System.Windows.Forms.OpenFileDialog OFDDoc;
    private System.Windows.Forms.Label lblPatientID;
    private System.Windows.Forms.ToolTip toolTip1;
    private System.Windows.Forms.TextBox txtComment;
    private System.Windows.Forms.Button btnClear;
    private System.Windows.Forms.Button btnNew;
    private System.Windows.Forms.Button btnDelete;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Label lblComment;
    private System.Windows.Forms.Label lblPictureID;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label lblMode;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.TextBox txtPath;
      private System.Windows.Forms.Label lblPath;
      private System.Windows.Forms.ComboBox cboDocType;
      private System.Windows.Forms.Label lblDocType;
      private System.Windows.Forms.Button btnAddNewType;
      private System.Windows.Forms.Button btnOpen;
      private System.Windows.Forms.DataGridViewTextBoxColumn PictureID;
      private System.Windows.Forms.DataGridViewTextBoxColumn EnteredDate;
      private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
      private System.Windows.Forms.DataGridViewTextBoxColumn GridPatientID;
      private System.Windows.Forms.DataGridViewTextBoxColumn VdoPath;
      private System.Windows.Forms.DataGridViewTextBoxColumn Comments;
      private System.Windows.Forms.DataGridViewTextBoxColumn SavedMethod;
      private System.Windows.Forms.DataGridViewTextBoxColumn TYPE;
      private System.Windows.Forms.DataGridViewTextBoxColumn DoctypeID;
      private System.Windows.Forms.DataGridViewTextBoxColumn DocTypeName;
  }
}