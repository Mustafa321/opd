namespace OPD
{
  partial class frmPhrmacyMed
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhrmacyMed));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgPatient = new System.Windows.Forms.DataGridView();
            this.dgOPDMed = new System.Windows.Forms.DataGridView();
            this.MOPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MedDosage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deliver = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NotAvail = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnNotAvail = new System.Windows.Forms.Button();
            this.btnMedDetailCost = new System.Windows.Forms.Button();
            this.btnMedCost = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miRefresh = new System.Windows.Forms.ToolStripButton();
            this.miExit = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboConsultant = new System.Windows.Forms.ComboBox();
            this.txtPRNo = new System.Windows.Forms.MaskedTextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtCellNo = new System.Windows.Forms.TextBox();
            this.txtPhoneNo = new System.Windows.Forms.TextBox();
            this.lblPRNo = new System.Windows.Forms.Label();
            this.lblPhoneNo = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCellNo = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblConsultant = new System.Windows.Forms.Label();
            this.lblMed = new System.Windows.Forms.Label();
            this.lblTitalPatient = new System.Windows.Forms.Label();
            this.lbldgMedHeading = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.nudDiscount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.PatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgeGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConsultantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgPatient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgOPDMed)).BeginInit();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiscount)).BeginInit();
            this.SuspendLayout();
            // 
            // dgPatient
            // 
            this.dgPatient.AllowUserToAddRows = false;
            this.dgPatient.AllowUserToDeleteRows = false;
            this.dgPatient.AllowUserToResizeColumns = false;
            this.dgPatient.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatient.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPatient.ColumnHeadersHeight = 22;
            this.dgPatient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPatient.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientID,
            this.PRNo,
            this.OPDID,
            this.PName,
            this.AgeGender,
            this.PhoneNo,
            this.CellNo,
            this.Address,
            this.ConsultantName});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPatient.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgPatient.Location = new System.Drawing.Point(11, 123);
            this.dgPatient.MultiSelect = false;
            this.dgPatient.Name = "dgPatient";
            this.dgPatient.ReadOnly = true;
            this.dgPatient.RowHeadersWidth = 30;
            this.dgPatient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPatient.Size = new System.Drawing.Size(991, 334);
            this.dgPatient.TabIndex = 0;
            this.dgPatient.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPatient_CellClick);
            // 
            // dgOPDMed
            // 
            this.dgOPDMed.AllowDrop = true;
            this.dgOPDMed.AllowUserToAddRows = false;
            this.dgOPDMed.AllowUserToDeleteRows = false;
            this.dgOPDMed.AllowUserToResizeColumns = false;
            this.dgOPDMed.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgOPDMed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgOPDMed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOPDMed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MOPDID,
            this.MedID,
            this.MedName,
            this.MedDosage,
            this.Quantity,
            this.UnitPrice,
            this.Price,
            this.Deliver,
            this.NotAvail});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgOPDMed.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgOPDMed.Location = new System.Drawing.Point(11, 492);
            this.dgOPDMed.MultiSelect = false;
            this.dgOPDMed.Name = "dgOPDMed";
            this.dgOPDMed.RowHeadersWidth = 30;
            this.dgOPDMed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgOPDMed.RowTemplate.Height = 30;
            this.dgOPDMed.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgOPDMed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgOPDMed.Size = new System.Drawing.Size(992, 170);
            this.dgOPDMed.TabIndex = 2;
            this.dgOPDMed.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOPDMed_CellContentClick);
            this.dgOPDMed.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOPDMed_CellEndEdit);
            this.dgOPDMed.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOPDMed_CellEnter);
            this.dgOPDMed.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgOPDMed_EditingControlShowing);
            // 
            // MOPDID
            // 
            this.MOPDID.DataPropertyName = "OPDID";
            this.MOPDID.HeaderText = "OPDID";
            this.MOPDID.Name = "MOPDID";
            this.MOPDID.Visible = false;
            // 
            // MedID
            // 
            this.MedID.DataPropertyName = "MedicineID";
            this.MedID.HeaderText = "MedID";
            this.MedID.Name = "MedID";
            this.MedID.Visible = false;
            // 
            // MedName
            // 
            this.MedName.DataPropertyName = "MedicineName";
            this.MedName.HeaderText = "Name";
            this.MedName.Name = "MedName";
            this.MedName.ReadOnly = true;
            this.MedName.Width = 200;
            // 
            // MedDosage
            // 
            this.MedDosage.DataPropertyName = "MedicineDosage";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("AlKatib1", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MedDosage.DefaultCellStyle = dataGridViewCellStyle6;
            this.MedDosage.HeaderText = "Dosage";
            this.MedDosage.Name = "MedDosage";
            this.MedDosage.ReadOnly = true;
            this.MedDosage.Width = 280;
            // 
            // Quantity
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = "0";
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle7;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MaxInputLength = 5;
            this.Quantity.Name = "Quantity";
            // 
            // UnitPrice
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.NullValue = "0";
            this.UnitPrice.DefaultCellStyle = dataGridViewCellStyle8;
            this.UnitPrice.HeaderText = "Unit Price";
            this.UnitPrice.MaxInputLength = 8;
            this.UnitPrice.Name = "UnitPrice";
            // 
            // Price
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = "0";
            this.Price.DefaultCellStyle = dataGridViewCellStyle9;
            this.Price.HeaderText = "Price";
            this.Price.MaxInputLength = 10;
            this.Price.Name = "Price";
            // 
            // Deliver
            // 
            this.Deliver.DataPropertyName = "Deliver";
            this.Deliver.FalseValue = "0";
            this.Deliver.HeaderText = "Deliver";
            this.Deliver.IndeterminateValue = "";
            this.Deliver.Name = "Deliver";
            this.Deliver.TrueValue = "1";
            this.Deliver.Width = 80;
            // 
            // NotAvail
            // 
            this.NotAvail.DataPropertyName = "Avail";
            this.NotAvail.FalseValue = "0";
            this.NotAvail.HeaderText = "Available";
            this.NotAvail.Name = "NotAvail";
            this.NotAvail.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NotAvail.TrueValue = "1";
            this.NotAvail.Width = 95;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnNotAvail);
            this.panel2.Controls.Add(this.btnMedDetailCost);
            this.panel2.Controls.Add(this.btnMedCost);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(11, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(992, 55);
            this.panel2.TabIndex = 50;
            this.panel2.Tag = "top";
            // 
            // btnNotAvail
            // 
            this.btnNotAvail.Location = new System.Drawing.Point(181, 0);
            this.btnNotAvail.Name = "btnNotAvail";
            this.btnNotAvail.Size = new System.Drawing.Size(82, 53);
            this.btnNotAvail.TabIndex = 59;
            this.btnNotAvail.Text = "Not Available";
            this.btnNotAvail.UseVisualStyleBackColor = true;
            this.btnNotAvail.Click += new System.EventHandler(this.btnNotAvail_Click);
            // 
            // btnMedDetailCost
            // 
            this.btnMedDetailCost.Location = new System.Drawing.Point(93, 0);
            this.btnMedDetailCost.Name = "btnMedDetailCost";
            this.btnMedDetailCost.Size = new System.Drawing.Size(82, 53);
            this.btnMedDetailCost.TabIndex = 57;
            this.btnMedDetailCost.Text = "Detail Medicine Cost";
            this.btnMedDetailCost.UseVisualStyleBackColor = true;
            this.btnMedDetailCost.Click += new System.EventHandler(this.btnMedDetailCost_Click);
            // 
            // btnMedCost
            // 
            this.btnMedCost.Location = new System.Drawing.Point(3, 0);
            this.btnMedCost.Name = "btnMedCost";
            this.btnMedCost.Size = new System.Drawing.Size(84, 53);
            this.btnMedCost.TabIndex = 58;
            this.btnMedCost.Text = "Medicine Cost";
            this.btnMedCost.UseVisualStyleBackColor = true;
            this.btnMedCost.Click += new System.EventHandler(this.btnMedCost_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miRefresh,
            this.miExit});
            this.toolStrip1.Location = new System.Drawing.Point(792, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(199, 55);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // miSave
            // 
            this.miSave.AutoSize = false;
            this.miSave.BackColor = System.Drawing.Color.LightSlateGray;
            this.miSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miSave.ForeColor = System.Drawing.Color.Black;
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(65, 52);
            this.miSave.Tag = "Save";
            this.miSave.Text = "&Save";
            this.miSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miSave.ToolTipText = "Save Instructions";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.AutoSize = false;
            this.miRefresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.miRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miRefresh.ForeColor = System.Drawing.Color.Black;
            this.miRefresh.Image = ((System.Drawing.Image)(resources.GetObject("miRefresh.Image")));
            this.miRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(65, 52);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miRefresh.ToolTipText = "Refresh All Fields";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // miExit
            // 
            this.miExit.AutoSize = false;
            this.miExit.BackColor = System.Drawing.Color.LightSlateGray;
            this.miExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.miExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.miExit.ForeColor = System.Drawing.Color.Black;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(65, 52);
            this.miExit.Text = "&Exit";
            this.miExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.miExit.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.miExit.ToolTipText = "Exit ";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cboConsultant);
            this.panel1.Controls.Add(this.txtPRNo);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.txtCellNo);
            this.panel1.Controls.Add(this.txtPhoneNo);
            this.panel1.Controls.Add(this.lblPRNo);
            this.panel1.Controls.Add(this.lblPhoneNo);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.lblCellNo);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.lblConsultant);
            this.panel1.Location = new System.Drawing.Point(11, 71);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(992, 49);
            this.panel1.TabIndex = 51;
            this.panel1.Tag = "";
            // 
            // cboConsultant
            // 
            this.cboConsultant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboConsultant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboConsultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConsultant.FormattingEnabled = true;
            this.cboConsultant.Location = new System.Drawing.Point(776, 13);
            this.cboConsultant.Name = "cboConsultant";
            this.cboConsultant.Size = new System.Drawing.Size(164, 21);
            this.cboConsultant.TabIndex = 12;
            // 
            // txtPRNo
            // 
            this.txtPRNo.Location = new System.Drawing.Point(69, 13);
            this.txtPRNo.Mask = "aa-aa-aaaaaa";
            this.txtPRNo.Name = "txtPRNo";
            this.txtPRNo.Size = new System.Drawing.Size(76, 20);
            this.txtPRNo.TabIndex = 11;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(202, 14);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(164, 20);
            this.txtName.TabIndex = 7;
            // 
            // txtCellNo
            // 
            this.txtCellNo.Location = new System.Drawing.Point(607, 14);
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.Size = new System.Drawing.Size(100, 20);
            this.txtCellNo.TabIndex = 6;
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Location = new System.Drawing.Point(444, 14);
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(94, 20);
            this.txtPhoneNo.TabIndex = 5;
            this.txtPhoneNo.Visible = false;
            // 
            // lblPRNo
            // 
            this.lblPRNo.AutoSize = true;
            this.lblPRNo.Location = new System.Drawing.Point(0, 17);
            this.lblPRNo.Name = "lblPRNo";
            this.lblPRNo.Size = new System.Drawing.Size(44, 13);
            this.lblPRNo.TabIndex = 4;
            this.lblPRNo.Text = "MR No.";
            // 
            // lblPhoneNo
            // 
            this.lblPhoneNo.AutoSize = true;
            this.lblPhoneNo.Location = new System.Drawing.Point(374, 17);
            this.lblPhoneNo.Name = "lblPhoneNo";
            this.lblPhoneNo.Size = new System.Drawing.Size(67, 13);
            this.lblPhoneNo.TabIndex = 3;
            this.lblPhoneNo.Text = "Phone No. : ";
            this.lblPhoneNo.Visible = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(157, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(44, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name : ";
            // 
            // lblCellNo
            // 
            this.lblCellNo.AutoSize = true;
            this.lblCellNo.Location = new System.Drawing.Point(551, 17);
            this.lblCellNo.Name = "lblCellNo";
            this.lblCellNo.Size = new System.Drawing.Size(85, 13);
            this.lblCellNo.TabIndex = 1;
            this.lblCellNo.Text = "Whatsapp No. : ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(943, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(47, 47);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblConsultant
            // 
            this.lblConsultant.AutoSize = true;
            this.lblConsultant.Location = new System.Drawing.Point(711, 17);
            this.lblConsultant.Name = "lblConsultant";
            this.lblConsultant.Size = new System.Drawing.Size(66, 13);
            this.lblConsultant.TabIndex = 13;
            this.lblConsultant.Text = "Consultant : ";
            // 
            // lblMed
            // 
            this.lblMed.AutoSize = true;
            this.lblMed.Location = new System.Drawing.Point(16, 520);
            this.lblMed.Name = "lblMed";
            this.lblMed.Size = new System.Drawing.Size(35, 13);
            this.lblMed.TabIndex = 10;
            this.lblMed.Text = "label2";
            this.lblMed.Visible = false;
            // 
            // lblTitalPatient
            // 
            this.lblTitalPatient.AutoSize = true;
            this.lblTitalPatient.Location = new System.Drawing.Point(12, 123);
            this.lblTitalPatient.Name = "lblTitalPatient";
            this.lblTitalPatient.Size = new System.Drawing.Size(35, 13);
            this.lblTitalPatient.TabIndex = 9;
            this.lblTitalPatient.Text = "label1";
            this.lblTitalPatient.Visible = false;
            // 
            // lbldgMedHeading
            // 
            this.lbldgMedHeading.AutoSize = true;
            this.lbldgMedHeading.Location = new System.Drawing.Point(12, 533);
            this.lbldgMedHeading.Name = "lbldgMedHeading";
            this.lbldgMedHeading.Size = new System.Drawing.Size(141, 13);
            this.lbldgMedHeading.TabIndex = 52;
            this.lbldgMedHeading.Text = "List of Medicine for Patient : ";
            this.lbldgMedHeading.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 470);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Prescribed Medicine : ";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.ForeColor = System.Drawing.Color.Red;
            this.lblTotalAmount.Location = new System.Drawing.Point(786, 671);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(79, 13);
            this.lblTotalAmount.TabIndex = 54;
            this.lblTotalAmount.Tag = "red";
            this.lblTotalAmount.Text = "Total Amount : ";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandTotal.ForeColor = System.Drawing.Color.Red;
            this.txtGrandTotal.Location = new System.Drawing.Point(874, 668);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(100, 20);
            this.txtGrandTotal.TabIndex = 55;
            this.txtGrandTotal.Tag = "red";
            this.txtGrandTotal.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(980, 671);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 56;
            this.label2.Tag = "red";
            this.label2.Text = "Rs.";
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Location = new System.Drawing.Point(633, 671);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(58, 13);
            this.lblDiscount.TabIndex = 57;
            this.lblDiscount.Text = "Discount : ";
            // 
            // nudDiscount
            // 
            this.nudDiscount.Location = new System.Drawing.Point(697, 669);
            this.nudDiscount.Name = "nudDiscount";
            this.nudDiscount.Size = new System.Drawing.Size(45, 20);
            this.nudDiscount.TabIndex = 58;
            this.nudDiscount.ValueChanged += new System.EventHandler(this.nudDiscount_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(746, 671);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "%";
            // 
            // PatientID
            // 
            this.PatientID.DataPropertyName = "PatientID";
            dataGridViewCellStyle2.NullValue = "0";
            this.PatientID.DefaultCellStyle = dataGridViewCellStyle2;
            this.PatientID.HeaderText = "PatientID";
            this.PatientID.Name = "PatientID";
            this.PatientID.ReadOnly = true;
            this.PatientID.Visible = false;
            // 
            // PRNo
            // 
            this.PRNo.DataPropertyName = "MR No.";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PRNo.DefaultCellStyle = dataGridViewCellStyle3;
            this.PRNo.HeaderText = "PR No.";
            this.PRNo.Name = "PRNo";
            this.PRNo.ReadOnly = true;
            this.PRNo.Width = 90;
            // 
            // OPDID
            // 
            this.OPDID.DataPropertyName = "OPDID";
            this.OPDID.HeaderText = "OPDID";
            this.OPDID.Name = "OPDID";
            this.OPDID.ReadOnly = true;
            this.OPDID.Visible = false;
            // 
            // PName
            // 
            this.PName.DataPropertyName = "Name";
            this.PName.HeaderText = "Name";
            this.PName.Name = "PName";
            this.PName.ReadOnly = true;
            this.PName.Width = 175;
            // 
            // AgeGender
            // 
            this.AgeGender.DataPropertyName = "AgeGender";
            this.AgeGender.HeaderText = "";
            this.AgeGender.Name = "AgeGender";
            this.AgeGender.ReadOnly = true;
            this.AgeGender.Width = 90;
            // 
            // PhoneNo
            // 
            this.PhoneNo.DataPropertyName = "PhoneNo";
            this.PhoneNo.HeaderText = "Phone No.";
            this.PhoneNo.Name = "PhoneNo";
            this.PhoneNo.ReadOnly = true;
            this.PhoneNo.Width = 90;
            // 
            // CellNo
            // 
            this.CellNo.DataPropertyName = "CellNo";
            this.CellNo.HeaderText = "Cell No.";
            this.CellNo.Name = "CellNo";
            this.CellNo.ReadOnly = true;
            this.CellNo.Width = 90;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            this.Address.Width = 245;
            // 
            // ConsultantName
            // 
            this.ConsultantName.DataPropertyName = "ConsultantName";
            this.ConsultantName.HeaderText = "Consultant";
            this.ConsultantName.Name = "ConsultantName";
            this.ConsultantName.ReadOnly = true;
            this.ConsultantName.Width = 175;
            // 
            // frmPhrmacyMed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 700);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudDiscount);
            this.Controls.Add(this.lblDiscount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtGrandTotal);
            this.Controls.Add(this.lblTotalAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbldgMedHeading);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblMed);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblTitalPatient);
            this.Controls.Add(this.dgOPDMed);
            this.Controls.Add(this.dgPatient);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmPhrmacyMed";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Medeicine Request";
            this.Load += new System.EventHandler(this.frmPhrmacyMed_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPatient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgOPDMed)).EndInit();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiscount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgPatient;
    private System.Windows.Forms.DataGridView dgOPDMed;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton miSave;
    private System.Windows.Forms.ToolStripButton miRefresh;
    private System.Windows.Forms.ToolStripButton miExit;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.TextBox txtName;
    private System.Windows.Forms.Label lblPRNo;
    private System.Windows.Forms.Label lblPhoneNo;
    private System.Windows.Forms.Label lblName;
    private System.Windows.Forms.Label lblCellNo;
    private System.Windows.Forms.Button btnSearch;
    private System.Windows.Forms.Label lblMed;
    private System.Windows.Forms.Label lblTitalPatient;
    private System.Windows.Forms.MaskedTextBox txtPRNo;
    private System.Windows.Forms.Label lbldgMedHeading;
    private System.Windows.Forms.Label lblConsultant;
    private System.Windows.Forms.ComboBox cboConsultant;
    private System.Windows.Forms.TextBox txtCellNo;
    private System.Windows.Forms.TextBox txtPhoneNo;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lblTotalAmount;
    private System.Windows.Forms.TextBox txtGrandTotal;
      private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btnMedDetailCost;
    private System.Windows.Forms.Button btnMedCost;
    private System.Windows.Forms.Button btnNotAvail;
    private System.Windows.Forms.DataGridViewTextBoxColumn MOPDID;
    private System.Windows.Forms.DataGridViewTextBoxColumn MedID;
    private System.Windows.Forms.DataGridViewTextBoxColumn MedName;
    private System.Windows.Forms.DataGridViewTextBoxColumn MedDosage;
    private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
    private System.Windows.Forms.DataGridViewTextBoxColumn UnitPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn Price;
    private System.Windows.Forms.DataGridViewCheckBoxColumn Deliver;
    private System.Windows.Forms.DataGridViewCheckBoxColumn NotAvail;
    private System.Windows.Forms.Label lblDiscount;
    private System.Windows.Forms.NumericUpDown nudDiscount;
    private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgeGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConsultantName;
    }
}