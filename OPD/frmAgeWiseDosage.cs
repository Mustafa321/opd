using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmAgeWiseDosage : Form
    {
        private string _MedID = "";
        private string _MedName = "";
        
        public string MedID 
        {
            set { _MedID = value; }
        }
        
        public string MedName
        {
            set { _MedName= value; }
        }

        public frmAgeWiseDosage()
        {
            InitializeComponent();
        }

        private void frmAgeWiseDosage_Load(object sender, EventArgs e)
        {
            txtMedName.Text = _MedName;
            GetAgeWiseDosage();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void GetAgeWiseDosage()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLMedicine objMed = new clsBLMedicine(objConnection);
            DataView dv = new DataView();
            try
            {
                objMed.MedicineID = _MedID;
                objConnection.Connection_Open();
                dv = objMed.GetAll(14);
                dgAgewiseDosage.DataSource = dv;
                dgAgewiseDosage.ClearSelection();
            }
            catch { }
            //GenSerNo(dgAgewiseDosage, "ADSNo");
            finally
            {
                objConnection.Connection_Close();
                objMed = null;
                objConnection = null;
            }
        }
    }
}