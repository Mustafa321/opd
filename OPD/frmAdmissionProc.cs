using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OPD_BL;

namespace OPD
{
    public partial class frmAdmissionProc : Form
    {
        private string _PatientID = "";
        private frmOPD objOPD = null;
        private bool _Flag = false;
        
        public string PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }
        }
        
        public frmAdmissionProc()
        {
            InitializeComponent();
        }

        public frmAdmissionProc(frmOPD objOPD)
        {
            InitializeComponent();
            this.objOPD = objOPD;
        }

        private void frmAdmissionProc_Load(object sender, EventArgs e)
        {
            SComponents objComp = new SComponents();
            this.txtPRNo.Mask= System.Configuration.ConfigurationSettings.AppSettings["MRmask"].ToString();
            objComp.ApplyStyleToControls(this);

            FillPatientInfo();
            FillDG();
            
            objComp = null;
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAdmissionProc_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objOPD != null)
            {
                objOPD.Show();
                this.Dispose();
            }
        }

        private void FillPatientInfo()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLPatientReg objPatientReg = new clsBLPatientReg(objConnection);
            DataView dv = new DataView();

            objPatientReg.PatientID = _PatientID;
            objConnection.Connection_Open();
            dv = objPatientReg.GetAll(16);
            objConnection.Connection_Close();

            if (dv.Table.Rows.Count != 0)
            {
                txtPRNo.Text = dv.Table.Rows[0]["prno"].ToString();
                txtPName.Text = dv.Table.Rows[0]["PName"].ToString();
                txtGender.Text = dv.Table.Rows[0]["gender"].ToString().Equals("M") ? "Male" : "Female";
                txtAge.Text = dv.Table.Rows[0]["age"].ToString();
            }

            objPatientReg = null;
            objConnection = null;
        }

        private void FillDG()
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAdmissionProc objAdmProc = new clsBLAdmissionProc(objConnection);
            DataView dv=new DataView();
            try
            {

                if (rbAdmission.Checked)
                {
                    objAdmProc.Type = "A";//Admission
                }
                else if (rbProc.Checked)
                {
                    objAdmProc.Type = "P";//Procedure
                }
                objAdmProc.PatientID = _PatientID;
                objConnection.Connection_Open();
                dv = objAdmProc.GetAll(1);
                dgAdmProc.DataSource = dv;
            }
            catch { }
            finally
            {
                objConnection.Connection_Close();
                objAdmProc = null;
                objConnection = null;
            }
        }

        private void ClearGridRows(DataGridView dg)
        {
            try
            {
                DataView dv = (DataView)dg.DataSource;

                if (dv != null)
                {
                    dv.Table.Rows.Clear();
                    dg.DataSource = dv;
                }
            }
            catch
            {
                DataTable dt = (DataTable)dg.DataSource;

                if (dt != null)
                {
                    dt.Rows.Clear();
                    dg.DataSource = dt;
                }
            }
            //while (dg.Rows.Count > 0)
            //{
            //  dg.Rows.RemoveAt(0);
            //}
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            ClearField();
            FillDG();
        }
        private void ClearField()
        {
            txtDescription.Text = "";
            lblAdmProcID.Text = "";
            chbActive.Checked = true;
            miSave.Tag = "Save";
        }
        private void miSave_Click(object sender, EventArgs e)
        {
            clsBLDBConnection objConnection = new clsBLDBConnection();
            clsBLAdmissionProc objAdmissionProc = new clsBLAdmissionProc(objConnection);

            try
            {
                objConnection.Connection_Open();
                objConnection.Transaction_Begin();

                if (txtDescription.Text.Trim().Equals(""))
                {
                    MessageBox.Show("Please Enter Description", "Insertion ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                objAdmissionProc.Name = txtDescription.Text.Trim();
                objAdmissionProc.Type = rbAdmission.Checked ? "A" : "P";
                objAdmissionProc.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy");
                objAdmissionProc.EnteredBy = clsSharedVariables.UserID;
                objAdmissionProc.Active = chbActive.Checked ? "1" : "0";
                objAdmissionProc.PatientID= _PatientID;

                if (miSave.Tag.ToString().Equals("Save"))
                {
                    if (objAdmissionProc.Insert())
                    {
                        MessageBox.Show("Insertion Successfully", "Insert", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(objAdmissionProc.ErrorMessage, "Insertion ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else 
                {
                    objAdmissionProc.AdmissionProcID = lblAdmProcID.Text;
                    if (objAdmissionProc.Update())
                    {
                        MessageBox.Show("Update Successfully", "Update ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(objAdmissionProc.ErrorMessage, "Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    miSave.Tag = "Save";

                }
                objConnection.Transaction_ComRoll();

                FillDG();
                ClearField();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Insertion ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                objConnection.Connection_Close();

                objConnection = null;
                objAdmissionProc = null;
            }
        }

        private void Update(clsBLOPDMedAlerAdver objOPDMedAlerAdver, int RowIdx)
        {
            objOPDMedAlerAdver.Active = "N";
            objOPDMedAlerAdver.ID = dgAdmProc.Rows[RowIdx].Cells["PatientAllergAR"].Value.ToString();
            objOPDMedAlerAdver.EnteredOn = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");

            objOPDMedAlerAdver.Update();
        }

        private void rbAdmission_CheckedChanged(object sender, EventArgs e)
        {
            FillDG();
        }

        private void rbProc_CheckedChanged(object sender, EventArgs e)
        {
            FillDG();
        }

        private void dgAdmProc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;

            if (index != -1)
            {
                lblAdmProcID.Text = dgAdmProc.Rows[index].Cells["PatientAdmProc"].Value.ToString();
                txtDescription.Text = dgAdmProc.Rows[index].Cells["Description"].Value.ToString();
                if (dgAdmProc.Rows[index].Cells["Active"].Value.ToString().Equals("1"))
                {
                    chbActive.Checked = true;
                }
                else
                {
                    chbActive.Checked = false;
                }
                miSave.Tag = "Update";
            }
        }
    }
}