namespace OPD
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbAutoPRNO = new System.Windows.Forms.GroupBox();
            this.chbAutoPRNO = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMaxRecTime = new System.Windows.Forms.Label();
            this.txtMaxRecTime = new System.Windows.Forms.TextBox();
            this.lblMaxRecTimeSecHeading = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RenameHeading = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.WaitingQueue = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Appointment = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Allergies = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Notes = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Medication = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.VitalSign = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.AdmissionAndProcedure = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.PatientDocuments = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.PatientVisits = new System.Windows.Forms.CheckBox();
            this.gbAutoPRNO.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAutoPRNO
            // 
            this.gbAutoPRNO.Controls.Add(this.chbAutoPRNO);
            this.gbAutoPRNO.Location = new System.Drawing.Point(12, 12);
            this.gbAutoPRNO.Name = "gbAutoPRNO";
            this.gbAutoPRNO.Size = new System.Drawing.Size(310, 41);
            this.gbAutoPRNO.TabIndex = 0;
            this.gbAutoPRNO.TabStop = false;
            this.gbAutoPRNO.Text = "Auto Genrated PR No.";
            this.gbAutoPRNO.Visible = false;
            // 
            // chbAutoPRNO
            // 
            this.chbAutoPRNO.AutoSize = true;
            this.chbAutoPRNO.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbAutoPRNO.Location = new System.Drawing.Point(6, 18);
            this.chbAutoPRNO.Name = "chbAutoPRNO";
            this.chbAutoPRNO.Size = new System.Drawing.Size(86, 17);
            this.chbAutoPRNO.TabIndex = 0;
            this.chbAutoPRNO.Text = "Auto PR No.";
            this.chbAutoPRNO.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(247, 344);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "OK";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(329, 344);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblMaxRecTime
            // 
            this.lblMaxRecTime.AutoSize = true;
            this.lblMaxRecTime.Location = new System.Drawing.Point(344, 274);
            this.lblMaxRecTime.Name = "lblMaxRecTime";
            this.lblMaxRecTime.Size = new System.Drawing.Size(117, 13);
            this.lblMaxRecTime.TabIndex = 3;
            this.lblMaxRecTime.Text = "Max Recoeding Time : ";
            // 
            // txtMaxRecTime
            // 
            this.txtMaxRecTime.Location = new System.Drawing.Point(464, 271);
            this.txtMaxRecTime.Name = "txtMaxRecTime";
            this.txtMaxRecTime.Size = new System.Drawing.Size(100, 20);
            this.txtMaxRecTime.TabIndex = 4;
            this.txtMaxRecTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxRecTime_KeyPress);
            // 
            // lblMaxRecTimeSecHeading
            // 
            this.lblMaxRecTimeSecHeading.AutoSize = true;
            this.lblMaxRecTimeSecHeading.Location = new System.Drawing.Point(570, 274);
            this.lblMaxRecTimeSecHeading.Name = "lblMaxRecTimeSecHeading";
            this.lblMaxRecTimeSecHeading.Size = new System.Drawing.Size(26, 13);
            this.lblMaxRecTimeSecHeading.TabIndex = 5;
            this.lblMaxRecTimeSecHeading.Text = "Sec";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RenameHeading);
            this.groupBox1.Location = new System.Drawing.Point(328, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 41);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enable Rename Heading";
            // 
            // RenameHeading
            // 
            this.RenameHeading.AutoSize = true;
            this.RenameHeading.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RenameHeading.Location = new System.Drawing.Point(6, 18);
            this.RenameHeading.Name = "RenameHeading";
            this.RenameHeading.Size = new System.Drawing.Size(109, 17);
            this.RenameHeading.TabIndex = 0;
            this.RenameHeading.Text = "Rename Heading";
            this.RenameHeading.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WaitingQueue);
            this.groupBox2.Location = new System.Drawing.Point(328, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 41);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Waiting Queue";
            // 
            // WaitingQueue
            // 
            this.WaitingQueue.AutoSize = true;
            this.WaitingQueue.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.WaitingQueue.Location = new System.Drawing.Point(6, 18);
            this.WaitingQueue.Name = "WaitingQueue";
            this.WaitingQueue.Size = new System.Drawing.Size(127, 17);
            this.WaitingQueue.TabIndex = 0;
            this.WaitingQueue.Text = "Show Waiting Queue";
            this.WaitingQueue.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Appointment);
            this.groupBox3.Location = new System.Drawing.Point(12, 59);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 41);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Appointment";
            // 
            // Appointment
            // 
            this.Appointment.AutoSize = true;
            this.Appointment.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Appointment.Location = new System.Drawing.Point(6, 18);
            this.Appointment.Name = "Appointment";
            this.Appointment.Size = new System.Drawing.Size(115, 17);
            this.Appointment.TabIndex = 0;
            this.Appointment.Text = "Show Appointment";
            this.Appointment.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Allergies);
            this.groupBox4.Location = new System.Drawing.Point(328, 106);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(310, 41);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Allergies and Adv. Reaction";
            // 
            // Allergies
            // 
            this.Allergies.AutoSize = true;
            this.Allergies.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Allergies.Location = new System.Drawing.Point(6, 18);
            this.Allergies.Name = "Allergies";
            this.Allergies.Size = new System.Drawing.Size(187, 17);
            this.Allergies.TabIndex = 0;
            this.Allergies.Text = "Show Allergies and Adv. Reaction";
            this.Allergies.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Notes);
            this.groupBox5.Location = new System.Drawing.Point(12, 106);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(310, 41);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Notes";
            // 
            // Notes
            // 
            this.Notes.AutoSize = true;
            this.Notes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Notes.Location = new System.Drawing.Point(6, 18);
            this.Notes.Name = "Notes";
            this.Notes.Size = new System.Drawing.Size(84, 17);
            this.Notes.TabIndex = 0;
            this.Notes.Text = "Show Notes";
            this.Notes.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Medication);
            this.groupBox6.Location = new System.Drawing.Point(328, 153);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(310, 41);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Rx / Medication";
            // 
            // Medication
            // 
            this.Medication.AutoSize = true;
            this.Medication.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Medication.Location = new System.Drawing.Point(6, 18);
            this.Medication.Name = "Medication";
            this.Medication.Size = new System.Drawing.Size(132, 17);
            this.Medication.TabIndex = 0;
            this.Medication.Text = "Show Rx / Medication";
            this.Medication.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.VitalSign);
            this.groupBox7.Location = new System.Drawing.Point(12, 153);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(310, 41);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Vital Sign Chart";
            // 
            // VitalSign
            // 
            this.VitalSign.AutoSize = true;
            this.VitalSign.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VitalSign.Location = new System.Drawing.Point(6, 18);
            this.VitalSign.Name = "VitalSign";
            this.VitalSign.Size = new System.Drawing.Size(128, 17);
            this.VitalSign.TabIndex = 0;
            this.VitalSign.Text = "Show Vital Sign Chart";
            this.VitalSign.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.AdmissionAndProcedure);
            this.groupBox8.Location = new System.Drawing.Point(328, 209);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(310, 41);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Admission and Procedure";
            // 
            // AdmissionAndProcedure
            // 
            this.AdmissionAndProcedure.AutoSize = true;
            this.AdmissionAndProcedure.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AdmissionAndProcedure.Location = new System.Drawing.Point(6, 18);
            this.AdmissionAndProcedure.Name = "AdmissionAndProcedure";
            this.AdmissionAndProcedure.Size = new System.Drawing.Size(176, 17);
            this.AdmissionAndProcedure.TabIndex = 0;
            this.AdmissionAndProcedure.Text = "Show Admission and Procedure";
            this.AdmissionAndProcedure.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.PatientDocuments);
            this.groupBox9.Location = new System.Drawing.Point(12, 209);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(310, 41);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Patient Documents";
            // 
            // PatientDocuments
            // 
            this.PatientDocuments.AutoSize = true;
            this.PatientDocuments.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PatientDocuments.Location = new System.Drawing.Point(6, 18);
            this.PatientDocuments.Name = "PatientDocuments";
            this.PatientDocuments.Size = new System.Drawing.Size(146, 17);
            this.PatientDocuments.TabIndex = 0;
            this.PatientDocuments.Text = "Show Patient Documents";
            this.PatientDocuments.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.PatientVisits);
            this.groupBox10.Location = new System.Drawing.Point(12, 256);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(310, 41);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Patient Visits";
            // 
            // PatientVisits
            // 
            this.PatientVisits.AutoSize = true;
            this.PatientVisits.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PatientVisits.Location = new System.Drawing.Point(6, 18);
            this.PatientVisits.Name = "PatientVisits";
            this.PatientVisits.Size = new System.Drawing.Size(116, 17);
            this.PatientVisits.TabIndex = 0;
            this.PatientVisits.Text = "Show Patient Visits";
            this.PatientVisits.UseVisualStyleBackColor = true;
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 412);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblMaxRecTimeSecHeading);
            this.Controls.Add(this.txtMaxRecTime);
            this.Controls.Add(this.lblMaxRecTime);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbAutoPRNO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OPD Setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSetting_FormClosing);
            this.Load += new System.EventHandler(this.frmSetting_Load);
            this.gbAutoPRNO.ResumeLayout(false);
            this.gbAutoPRNO.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAutoPRNO;
        private System.Windows.Forms.CheckBox chbAutoPRNO;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMaxRecTime;
        private System.Windows.Forms.TextBox txtMaxRecTime;
        private System.Windows.Forms.Label lblMaxRecTimeSecHeading;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox RenameHeading;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox WaitingQueue;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox Appointment;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox Allergies;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox Notes;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox Medication;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox VitalSign;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox AdmissionAndProcedure;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox PatientDocuments;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox PatientVisits;
    }
}