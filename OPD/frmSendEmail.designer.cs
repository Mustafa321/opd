namespace OPD
{
    partial class frmSendEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSendEmail));
          this.lblSendT0 = new System.Windows.Forms.Label();
          this.lblSubject = new System.Windows.Forms.Label();
          this.lblFrom = new System.Windows.Forms.Label();
          this.txtTo = new System.Windows.Forms.TextBox();
          this.txtFrom = new System.Windows.Forms.TextBox();
          this.txtSubject = new System.Windows.Forms.TextBox();
          this.txtDescription = new System.Windows.Forms.TextBox();
          this.lblDescription = new System.Windows.Forms.Label();
          this.btnOk = new System.Windows.Forms.Button();
          this.lblAttachment = new System.Windows.Forms.Label();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // lblSendT0
          // 
          this.lblSendT0.AutoSize = true;
          this.lblSendT0.Location = new System.Drawing.Point(53, 17);
          this.lblSendT0.Name = "lblSendT0";
          this.lblSendT0.Size = new System.Drawing.Size(29, 13);
          this.lblSendT0.TabIndex = 0;
          this.lblSendT0.Tag = "display";
          this.lblSendT0.Text = "To : ";
          // 
          // lblSubject
          // 
          this.lblSubject.AutoSize = true;
          this.lblSubject.Location = new System.Drawing.Point(30, 69);
          this.lblSubject.Name = "lblSubject";
          this.lblSubject.Size = new System.Drawing.Size(52, 13);
          this.lblSubject.TabIndex = 1;
          this.lblSubject.Tag = "display";
          this.lblSubject.Text = "Subject : ";
          // 
          // lblFrom
          // 
          this.lblFrom.AutoSize = true;
          this.lblFrom.Location = new System.Drawing.Point(43, 43);
          this.lblFrom.Name = "lblFrom";
          this.lblFrom.Size = new System.Drawing.Size(39, 13);
          this.lblFrom.TabIndex = 2;
          this.lblFrom.Tag = "display";
          this.lblFrom.Text = "From : ";
          // 
          // txtTo
          // 
          this.txtTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtTo.Location = new System.Drawing.Point(84, 14);
          this.txtTo.Name = "txtTo";
          this.txtTo.Size = new System.Drawing.Size(298, 20);
          this.txtTo.TabIndex = 3;
          // 
          // txtFrom
          // 
          this.txtFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtFrom.Location = new System.Drawing.Point(84, 40);
          this.txtFrom.Name = "txtFrom";
          this.txtFrom.Size = new System.Drawing.Size(298, 20);
          this.txtFrom.TabIndex = 4;
          // 
          // txtSubject
          // 
          this.txtSubject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtSubject.Location = new System.Drawing.Point(84, 66);
          this.txtSubject.Name = "txtSubject";
          this.txtSubject.Size = new System.Drawing.Size(298, 20);
          this.txtSubject.TabIndex = 5;
          this.txtSubject.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
          // 
          // txtDescription
          // 
          this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.txtDescription.Location = new System.Drawing.Point(84, 92);
          this.txtDescription.Multiline = true;
          this.txtDescription.Name = "txtDescription";
          this.txtDescription.Size = new System.Drawing.Size(389, 166);
          this.txtDescription.TabIndex = 6;
          this.txtDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Validating);
          // 
          // lblDescription
          // 
          this.lblDescription.AutoSize = true;
          this.lblDescription.Location = new System.Drawing.Point(23, 95);
          this.lblDescription.Name = "lblDescription";
          this.lblDescription.Size = new System.Drawing.Size(59, 13);
          this.lblDescription.TabIndex = 7;
          this.lblDescription.Tag = "display";
          this.lblDescription.Text = "Message : ";
          // 
          // btnOk
          // 
          this.btnOk.BackColor = System.Drawing.Color.LightSlateGray;
          this.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
          this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
          this.btnOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnOk.Location = new System.Drawing.Point(408, 8);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(50, 52);
          this.btnOk.TabIndex = 9;
          this.btnOk.Tag = "notset";
          this.btnOk.Text = "&Send";
          this.btnOk.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
          this.btnOk.UseVisualStyleBackColor = false;
          this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
          // 
          // lblAttachment
          // 
          this.lblAttachment.AutoSize = true;
          this.lblAttachment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
          this.lblAttachment.Location = new System.Drawing.Point(425, 70);
          this.lblAttachment.Name = "lblAttachment";
          this.lblAttachment.Size = new System.Drawing.Size(10, 13);
          this.lblAttachment.TabIndex = 10;
          this.lblAttachment.Text = " ";
          this.lblAttachment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
          // 
          // pictureBox1
          // 
          this.pictureBox1.BackColor = System.Drawing.Color.LightSlateGray;
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(395, 64);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(25, 25);
          this.pictureBox1.TabIndex = 11;
          this.pictureBox1.TabStop = false;
          // 
          // frmSendEmail
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(492, 273);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.lblAttachment);
          this.Controls.Add(this.btnOk);
          this.Controls.Add(this.txtDescription);
          this.Controls.Add(this.txtSubject);
          this.Controls.Add(this.txtFrom);
          this.Controls.Add(this.txtTo);
          this.Controls.Add(this.lblFrom);
          this.Controls.Add(this.lblSubject);
          this.Controls.Add(this.lblSendT0);
          this.Controls.Add(this.lblDescription);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.MaximizeBox = false;
          this.Name = "frmSendEmail";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "Email Patient Prescription";
          this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSendEmail_FormClosed);
          this.Load += new System.EventHandler(this.frmSendEmail_Load);
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSendT0;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnOk;
        public System.Windows.Forms.TextBox txtTo;
        public System.Windows.Forms.TextBox txtFrom;
        public System.Windows.Forms.TextBox txtSubject;
        public System.Windows.Forms.TextBox txtDescription;
        public System.Windows.Forms.Label lblAttachment;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}