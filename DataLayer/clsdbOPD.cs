using System;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;


namespace DataLayer
{
	public class clsdbOPD : Iinterface
	{
		public clsdbOPD()
		{
		}

		# region "Class_Variables"

		private string StrQuery = null;		

		# endregion

		#region "Properties"

		public string Query
		{
			get { return StrQuery; }
			set { StrQuery = value; }
		}

		# endregion

		# region "Data_Methods"
        
        /// <summary>
        /// For MySql DataBase 
        /// </summary>
        /// <returns></returns>
		public OdbcCommand Insert()
        {
			OdbcCommand objCommand = new OdbcCommand();
            
			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;
            
			return objCommand;
		}

		public OdbcCommand Update ( )
        {
			OdbcCommand objCommand = new OdbcCommand();

            objCommand.CommandText = this.Query;
            objCommand.CommandType = CommandType.Text;

            return objCommand;
        }

		public OdbcCommand Delete ( )
        {
			OdbcCommand objCommand = new OdbcCommand();

            objCommand.CommandText = this.Query;
            objCommand.CommandType = CommandType.Text;

            return objCommand;
        }

		public OdbcCommand Get_All ( )
        {
			OdbcCommand objCommand = new OdbcCommand();
            objCommand.CommandText = this.Query;
            objCommand.CommandType = CommandType.Text;

            return objCommand;
        }

		public OdbcCommand Get_Single ( )
        {
			OdbcCommand objCommand = new OdbcCommand();

            objCommand.CommandText = this.Query;
            objCommand.CommandType = CommandType.Text;

            return objCommand;
        }

		public OdbcCommand Get_Max ( )
        {
			OdbcCommand objCommand = new OdbcCommand();
            
			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}
       
        /// <summary>
        /// For Oracle Databse
        /// </summary>
        /// <returns></returns>

		public OleDbCommand Ora_Insert ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		public OleDbCommand Ora_Get_Max ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		public OleDbCommand Ora_Update ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		public OleDbCommand Ora_Delete ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		public OleDbCommand Ora_Get_All ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		public OleDbCommand Ora_Get_Single ( )
		{
			OleDbCommand objCommand = new OleDbCommand();

			objCommand.CommandText = this.Query;
			objCommand.CommandType = CommandType.Text;

			return objCommand;
		}

		#endregion
	}
}