using System;
using System.Data;
using System.Data.Odbc;
using System.Configuration;
using System.Data.OleDb;

namespace DataLayer
{
    /// <summary>
    /// Summary description for clsdbconnection.
    /// </summary>
    public class clsdbconnection
    {
        private string _Flag = "";

        public string Flag
        {
            get { return _Flag; }
            set { _Flag = value; }
        }

        public clsdbconnection()
        {
        }

        #region Enumerations

        public enum ConnectionType
        {
            MySQL = 1,
            SQLSERVER = 2,
            Oracle = 3
        }

        #endregion

        #region "Properties"

        /// <summary>
        /// Get SQL Server Connection
        /// </summary>

        public OdbcConnection Odbc_SQL_Connection
        {
            get { return Dbconnection(clsdbconnection.ConnectionType.MySQL); }
        }

        //public OleDbConnection Oledb_Oracle_Connection
        //{
        //    get { return DbconnectionOracle(clsdbconnection.ConnectionType.Oracle); }
        //}

        #endregion

        #region "Connection String"

        private OdbcConnection Dbconnection(ConnectionType ConnectToDB)
        {
            string StrConnection = null;
            var ServerName = System.Configuration.ConfigurationSettings.AppSettings["DbServerName"].ToString();
            var ServerPort = System.Configuration.ConfigurationSettings.AppSettings["DbServerPort"].ToString();
            var DbName = System.Configuration.ConfigurationSettings.AppSettings["DBName"].ToString();
            var DBuserName = System.Configuration.ConfigurationSettings.AppSettings["DbUserName"].ToString();
            var DBPassword = System.Configuration.ConfigurationSettings.AppSettings["DbPassword"].ToString();
            var DNS = System.Configuration.ConfigurationSettings.AppSettings["DbDNS"].ToString();

            if (ConnectToDB == ConnectionType.MySQL)
            {

                StrConnection = "DATABASE="+ DbName + ";DSN="+ DNS + ";OPTION=0;PORT="+ ServerPort + ";SERVER="+ ServerName + ";UID="+ DBuserName + ";PWD="+ DBPassword + "";

            }
            else if (ConnectToDB == ConnectionType.SQLSERVER)
            {
                StrConnection = "";
            }
            OdbcConnection Conn = new OdbcConnection(StrConnection);
            try
            {
                Conn.Open();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return Conn;
        }

        //private OleDbConnection DbconnectionOracle ( ConnectionType ConnectToDB)
        //{
        //    //string StrConnection = null;

        //    //if (ConnectToDB == ConnectionType.Oracle)
        //    //{
        //    //    StrConnection = "Provider=MSDAORA.1;User ID=whims2;Data Source=HIMS;Password=whims2";
        //    //}
        //    //else if (ConnectToDB == ConnectionType.SQLSERVER)
        //    //{
        //    //    StrConnection = "";
        //    //}
        //    //OleDbConnection Conn = new OleDbConnection(StrConnection);
        //    //try
        //    //{
        //    //    Conn.Open();
        //    //}
        //    //catch (Exception Exc)
        //    //{
        //    //    string err = Exc.Message;
        //    //}

        //    //return Conn;
        //    string StrConnection = null;

        //    if (ConnectToDB == ConnectionType.Oracle)
        //    {
        //        try
        //        {
        //            if (_Flag.Equals("0"))
        //            {
        //                //trConnection = ConfigurationSettings.AppSettings["ConnectionString"].ToString();
        //                StrConnection = "Provider=MSDAORA.1;User ID=whims2;Data Source=HIMS;Password=whims2";
        //            }
        //            else if (_Flag.Equals("1"))
        //            {
        //                StrConnection = "Provider=MSDAORA.1;User ID=whims;Data Source=HIMS;Password=whims";
        //            }
        //        }
        //        catch
        //        {
        //            StrConnection = "Provider=MSDAORA.1;User ID=whims;Data Source=HIMS;Password=whims";
        //        }

        //        //StrConnection = "Provider=MSDAORA.1;User ID=whims;Data Source=HIMS;Password=whims";
        //        //StrConnection = "Provider='OraOLEDB.Oracle.1';User ID=hims;Password=hims;Data Source=hims;Extended Properties=;Persist Security Info=False";
        //    }

        //    OleDbConnection Conn = new OleDbConnection(StrConnection);
        //    //Conn.ConnectionString = StrConnection;
        //    Conn.Open();
        //    return Conn;

        //}

        #endregion
    }
}