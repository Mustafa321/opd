using System;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;

namespace DataLayer
{
	public class clsoperation
	{
		private string strMsg = "";
		private string StrOperationError;

        protected OdbcConnection Conn;
        protected OdbcCommand ObjCmd;
        protected OdbcTransaction DbTrans;

		protected OleDbConnection Ora_Conn;
		protected OleDbCommand Ora_ObjCmd;
		protected OleDbTransaction Ora_DbTrans;

        protected clsdbconnection Objconn = new clsdbconnection();

		public clsoperation()
		{
		}		

		#region "Properties"
		
		public string OperationError
		{
			get	{ return StrOperationError; }
			set { StrOperationError = value; }
		}

		public string StrMsg
		{
			set { strMsg = value; }
		}

        /// <summary>
        /// For Mysql
        /// </summary>
		public OdbcConnection GetConnection
        {
			get { return Conn; }
		}

		public OdbcTransaction DBTransaction
        {
			get { return DbTrans; }
		}
        /// <summary>
        /// For Oracle
        /// </summary>
		public OleDbConnection GetOraConnection
		{
			get { return Ora_Conn; }
		}

		public OleDbTransaction OraDBTransaction
		{
			get { return Ora_DbTrans; }
		}



		#endregion

		#region "Enumeration"

		public enum Get_PKey : byte
		{
			Yes = 1,
			No = 2
		}
		#endregion

		#region "Error_Transction"
        /// <summary>
        /// For Mysql
        /// </summary>
        public void Open_Connection()
        {
            // Get Database connection
			Conn = Objconn.Odbc_SQL_Connection;
        }

  		public void Start_Transaction()
		{
			// Declare Transaction Variable by beginning a new transaction
			DbTrans = Conn.BeginTransaction();
		}

        public void End_Transaction()
		{
			if (strMsg.Equals("Error"))
			{
				// Rollback transaction
				DbTrans.Rollback();
			}
			else
			{
				// commit transaction
				DbTrans.Commit();
			}
		}

        public void Close_Connection()
        {
            Conn.Close();
            Conn.Dispose();
            Conn = null;
        }
        
        /// <summary>
        /// For Oracle
        /// </summary>
        //public void Open_Ora_Connection (string  flag )
        //{
        //    // Get Database connection
        //    Objconn.Flag= flag;
        //    Ora_Conn = Objconn.Oledb_Oracle_Connection;
        //}

		public void Start_Ora_Transaction ( )
		{
			// Declare Transaction Variable by beginning a new transaction
			Ora_DbTrans = Ora_Conn.BeginTransaction();
		}

		public void End_Ora_Transaction ( )
		{
			if (strMsg.Equals("Error"))
			{
				// Rollback transaction
				Ora_DbTrans.Rollback();
			}
			else
			{
				// commit transaction
				Ora_DbTrans.Commit();
			}
		}

		public void Close_Ora_Connection ( )
		{
			Ora_Conn.Close();
			Ora_Conn.Dispose();
			Ora_Conn = null;
		}

		#endregion

		#region DataAccess

	    /// <summary>
	    /// For MYsql
	    /// </summary>
		public string DataTrigger_Insert(Iinterface Entity)
		{
			try
			{
                ObjCmd = Entity.Insert();
                ObjCmd.Connection = Conn;
                ObjCmd.Transaction = DbTrans;
                ObjCmd.ExecuteNonQuery();
			}
			catch(Exception e)
			{	
				OperationError = (e.Message) + "Sorry, Record can not be inserted.";
				strMsg = "Error";
			}
		
			return strMsg;
		}

        public string DataTrigger_Update(Iinterface Entity)
        {
            try
            {
                ObjCmd = Entity.Update();
                ObjCmd.Connection = Conn;
                ObjCmd.Transaction = DbTrans;
                ObjCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                OperationError = (e.Message) + "Sorry, Record can not be updated.";
                strMsg = "Error";
            }

            return strMsg;
        }

		public string DataTrigger_Delete(Iinterface Entity)
		{
            try
            {
                ObjCmd = Entity.Delete();
                ObjCmd.Connection = Conn;
                ObjCmd.Transaction = DbTrans;
                
                ObjCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                OperationError = (e.Message) + "Sorry, Record can not be deleted.";
                strMsg = "Error";
            }

			return strMsg;
		}

		public DataView DataTrigger_Get_All(Iinterface Entity)
		{			
            ObjCmd = Entity.Get_All();
			ObjCmd.Connection = Conn;

            try { ObjCmd.Transaction = DbTrans; }
            catch { }

			OdbcDataAdapter da = new OdbcDataAdapter(ObjCmd);
			DataSet DS = new DataSet();
			da.Fill(DS);

			DataView DV = new DataView(DS.Tables[0]);
			
			return DV;
		}
		
		public DataView DataTrigger_Get_Single(Iinterface Entity)
		{
			// Get Database connection
			Conn = Objconn.Odbc_SQL_Connection;
	
			ObjCmd = Entity.Get_Single();
			ObjCmd.Connection = Conn;
				
			// Dataadapter
			OdbcDataAdapter da = new OdbcDataAdapter(ObjCmd);
            
			// fill dataset
			DataSet DS = new DataSet();
			da.Fill(DS);

            //ObjCmd.Connection.Close();
            //ObjCmd.Connection.Dispose();
            //ObjCmd.Connection = null;

			// Create the DataView Object
			DataView DV = new DataView(DS.Tables[0]);
				
			return DV;
		}

		public string DataTrigger_Get_Max(Iinterface Entity)
		{
			try
			{
                ObjCmd = Entity.Get_Max();
                ObjCmd.Connection = Conn;

                try { ObjCmd.Transaction = DbTrans; }
                catch { }

				OdbcDataAdapter da = new OdbcDataAdapter(ObjCmd);
				DataSet DS = new DataSet();
				da.Fill(DS);

				return DS.Tables[0].Rows[0]["MaxID"].ToString();
			}
			catch(Exception e)
			{
				OperationError = (e.Message) + "Sorry, Maximum Primary Key can not be extracted.";
				strMsg = "Error";
			}
			
			return strMsg;
		}

        /// <summary>
        /// For Oracle
        /// </summary>
		public string DataTrigger_Ora_Insert ( Iinterface Entity )
		{
			try
			{
				Ora_ObjCmd = Entity.Ora_Insert();
				Ora_ObjCmd.Connection = Ora_Conn;
				Ora_ObjCmd.Transaction = Ora_DbTrans;
				Ora_ObjCmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				OperationError = (e.Message) + "Sorry, Record can not be inserted.";
				strMsg = "Error";
			}

			return strMsg;
		}

		public string DataTrigger_Ora_Update ( Iinterface Entity )
		{
			try
			{
				Ora_ObjCmd = Entity.Ora_Update();
				Ora_ObjCmd.Connection = Ora_Conn;
				Ora_ObjCmd.Transaction = Ora_DbTrans;
				Ora_ObjCmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				OperationError = (e.Message) + "Sorry, Record can not be updated.";
				strMsg = "Error";
			}

			return strMsg;
		}
		
		public string DataTrigger_Ora_Delete ( Iinterface Entity )
		{
			Ora_ObjCmd = Entity.Ora_Delete();
			Ora_ObjCmd.Transaction = Ora_DbTrans;

			try
			{
				Ora_ObjCmd.Connection = Ora_Conn;
				Ora_ObjCmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				OperationError = (e.Message) + "Sorry, Record can not be deleted.";
				strMsg = "Error";
			}

			return strMsg;
		}

		public DataView DataTrigger_Ora_Get_All ( Iinterface Entity )
		{
			Ora_ObjCmd = Entity.Ora_Get_All();
			Ora_ObjCmd.Connection = Ora_Conn;

			try { Ora_ObjCmd.Transaction = Ora_DbTrans; }
			catch { }

			OleDbDataAdapter da = new OleDbDataAdapter(Ora_ObjCmd);
			DataSet DS = new DataSet();
			da.Fill(DS);

			DataView DV = new DataView(DS.Tables[0]);

			return DV;
		}

        //public DataView DataTrigger_Ora_Get_Single ( Iinterface Entity )
        //{
        //    // Get Database connection
        //    Ora_Conn = Objconn.Oledb_Oracle_Connection;

        //    Ora_ObjCmd = Entity.Ora_Get_Single();
        //    Ora_ObjCmd.Connection = Ora_Conn;

        //    // Dataadapter
        //    OleDbDataAdapter da = new OleDbDataAdapter(Ora_ObjCmd);

        //    // fill dataset
        //    DataSet DS = new DataSet();
        //    da.Fill(DS);

        //    //Ora_ObjCmd.Connection.Close();
        //    //Ora_ObjCmd.Connection.Disposed();
        //    //Ora_ObjCmd.Connection = null;

        //    // Create the DataView Object
        //    DataView DV = new DataView(DS.Tables[0]);

        //    return DV;
        //}

		public string DataTrigger_Ora_Get_Max ( Iinterface Entity )
		{
			try
			{
				Ora_ObjCmd = Entity.Ora_Get_Max();
				Ora_ObjCmd.Connection = Ora_Conn;

				try { Ora_ObjCmd.Transaction = Ora_DbTrans; }
				catch { }

				OleDbDataAdapter da = new OleDbDataAdapter(Ora_ObjCmd);
				DataSet DS = new DataSet();
				da.Fill(DS);

				return DS.Tables[0].Rows[0]["MaxID"].ToString();
			}
			catch (Exception e)
			{
				OperationError = (e.Message) + "Sorry, Maximum Primary Key can not be extracted.";
				strMsg = "Error";
			}

			return strMsg;
		}
		#endregion
	}
}