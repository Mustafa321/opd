using System;
using System.Data.Odbc;
using System.Data.OleDb;

namespace DataLayer
{
	/// <summary>
	/// Summary description for IInterface.
	/// </summary>
	 public interface Iinterface
	{
		#region "Methods"

		 OdbcCommand Insert ( );

		 OdbcCommand Update ( );

		 OdbcCommand Delete ( );

		 OdbcCommand Get_All ( );

		 OdbcCommand Get_Single ( );

		 OdbcCommand Get_Max ( );

		 OleDbCommand Ora_Insert ( );

		 OleDbCommand Ora_Update ( );

		 OleDbCommand Ora_Delete ( );

		 OleDbCommand Ora_Get_All ( );

		 OleDbCommand Ora_Get_Single ( );

		 OleDbCommand Ora_Get_Max ( );
        
		#endregion
	 }
}
